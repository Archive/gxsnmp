/*
 * GXSNMP -- An snmp mangament application
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <time.h>
#include <stdio.h>
#ifdef HAVE_GETOPT_LONG
#include <getopt.h>
#endif HAVE_GETOPT_LONG
#include <strings.h>
#include <signal.h>

#include "collector.h" 

void mrtg_init (GHashTable *config)
{
}

#if 0
FILE *of;

void flush_output() {

  fflush(of);
  fclose(of);
  of = fopen(outfile,"a");
}

void init_output() {
  of = fopen(outfile,"a");
}

int format(char *buffer, long *num, SNMP_OBJECT *vars) {
  int i, res;

  res = 0;
  switch (vars->type){
    case SNMP_INTEGER:
    case SNMP_GAUGE:
    case SNMP_TIMETICKS:
    case SNMP_COUNTER:
      res = 1;
      *num = vars->syntax.lng;
      break;
    case SNMP_OCTETSTR:
    case SNMP_OPAQUE:
      strncpy(buffer,&vars->syntax.bufchr[0], vars->syntax_len);
      buffer[vars->syntax_len]='\0';
      break;
    case SNMP_OBJECTID:
      buffer[0]=0;
      for (i=0;i<vars->syntax_len/sizeof(glong);i++)
        sprintf(buffer+strlen(buffer),".%d",vars->syntax.bufint[i]);
      break;
    case SNMP_IPADDR:
      sprintf(buffer,"%d.%d.%d.%d",
              vars->syntax.bufchr[0],
              vars->syntax.bufchr[1],
              vars->syntax.bufchr[2],
              vars->syntax.bufchr[3]);
      break;
    case SNMP_NULL:
      sprintf(buffer,"NULL");
      break;
  }
  return res;
}

void output(struct _oid *myoid, SNMP_OBJECT *vars, time_t now) {
  char buffer[1024];
  char buffer1[1024];
  int i, type;
  long l;
  double value;
  int duration;

  type = format(buffer1,&l,vars);
  i=1;
  if (myoid->duration) {
    if (myoid->last == 0) {
      i=0;
    } else {
      duration = now - myoid->last;
      if (duration > 2* myoid->duration) i=0;
    }
    myoid->last = now;
#if 0
    if (i && type) {
      if (l < myoid->value) {
        i=0;
      } else {
        value = ((double) l - (double) myoid->value) / (double) duration;
      }
    }
#endif
    value = (double) l;
    if (type) myoid->value = l;
  } else {
    if (type) value = (double) l;
  }
  if (!i) return;    

  buffer[0] = 0;
  for(i=0;i<myoid->name_length-1;i++)
    sprintf(buffer+strlen(buffer),".%d", myoid->name[i]);

  if (type) {
    fprintf(of, "%d|%s|%s|%d|%lf|%d\n", 
              now, 
              myoid->host->name, 
              buffer, 
              myoid->name[myoid->name_length-1], 
              value,
              duration);
    if (debug)
      printf("%d|%s|%s|%d|%lf|%d\n", 
              now, 
              myoid->host->name, 
              buffer, 
              myoid->name[myoid->name_length-1], 
              value,
              duration);

  } else { 
    fprintf(of, "%d|%s|%s|%d|%s|%d\n",
              now, 
              myoid->host->name, 
              buffer, 
              myoid->name[myoid->name_length-1], 
              buffer1,
              duration);
    if (debug)
      printf("%d|%s|%s|%d|%s|%d\n",
              now, 
              myoid->host->name, 
              buffer, 
              myoid->name[myoid->name_length-1], 
              buffer1,
              duration);
  }
}
#endif
