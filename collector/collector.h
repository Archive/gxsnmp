#include <config.h>

#include <time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <glib.h>

/*
 * Configuration is stored in a cascaded hash table. Each element of the 
 * table can be either a string, numeric or again a hash table. 
 */

enum {
  SLIST_NUM,
  SLIST_STRING,
  SLIST_LIST
};

struct slist {
  int   type;
  union value {
    int          number;
    gchar      * string;
    GHashTable * list;
  } value;
};

/*
 * Global flag for debugging 
 */

int debug;

/*
 * Forward references for internal module init functions
 */

void snmp_init (GHashTable *config);
void mrtg_init (GHashTable *config);

/* 
 * Register function for host specific setup per module.
 * This is implemented as a callback cb_host_init.
 */

typedef void (*cb_host_init)(GHashTable *config, char *hostname ());

void register_host_function(char *module, cb_host_init cb);

/*
 * Structures for passing data between different modules.
 * The payload is the data to be used by the next module in the chain.
 * The meta data is some descriptive data like hostname, IP address, origin,
 * SNMP table instance, table index, SNMP type, and whatever the providing
 * module thinks could be useful later.
 */

struct property {
  char *name;
  int   type;
  union value;
};

struct data {
  GSList payload;
  GSList meta;
}; 
