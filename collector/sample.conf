# Set default values valid for all collected variables unless overridden

modules {
  internal {
    snmp;
    trapd;
    scanner;
    scannerv6;
    mrtg;
    exec;
    trap;
  };
  external pager = "/opt/gxsnmp/modules/sms.so";
};
 
snmp config {
  community = "public";
#  domain    = 2;
  domain    = "IPX";
  port      = 161;
  retries   = 20;
  timeout   = 5;
  defer     = 3600;
  interval  = 900;
};

trapd config {
  transport ip   = "AF_INET";
  transport ipv6 = "AF_INET6";
  transport ipx  = "AF_IPX";
};

scanner config {
  timeout = 900;
  retries = 5;
};

scanner6 config {
  timeout = 900;
  retries = 5;
};

mrtg config {
  database         = "/var/spool/mrtg";
  interval detail  = 900;
  interval hour    = 3600;
  interval day     = 86400;
  template table   = "$hostname-$tablename-$instance.rrd";
  template group   = "$hostname-$groupname.rrd";
};

trap config {
  community = "trap";
  transport = "AF_INET";
  destination control2 {
    name      = "control2.nwe.de";
    community = "test";
    version   = "1";
  };
  destination control1 {
    name      = "control1.regio-info.de";
    version   = "2C";
    transport = "AF_INET6";
  };
};

snmp {
  group system {
    uptime {
      oid = ".1.3.6.1.2.1.1.3";
      type = "counter";
    };
    description oid  = ".1.3.6.1.2.1.1.1";
    description type = "string";
  };
  group cisco {
    busy5  oid   = ".1.3.6.1.4";
    busy5  alarm = 98;
    busy15 oid   = ".1.3.6.1.4";
    type = "gauge";
  };
};

snmp table perf {
  index       = ".1.3.6.1.2.1.2.2.1.2";
  ifoocts oid = ".1.3.6.1.2.1.2.2.1.10";
  ifiocts oid = ".1.3.6.1.2.1.2.2.1.13";
  ifoerrs oid = ".1.3.6.1.2.1.2.2.1.16";
  ifierrs oid = ".1.3.6.1.2.1.2.2.1.19";
  type        = "counter";
};

scanner service telnet {
  type = "tcp";
  port = 23;
};

host alpha {
  name = "alpha.scram.de";
  snmp table perf {
    instances = "1,3-10,31";
    output mrtg;
  };
  snmp group cisco;
};

host test {
  name = "test.scram.de";
  snmp community = "test";
  snmp table perf {
    instances = "1,3";
    output mrtg;
  };
  snmp group cisco {
    output exec {
      alarm   = 98;
      command = "/usr/local/bin/restart.pl %h";
    };
    output mrtg;
  };
};
