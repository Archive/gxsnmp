#include <gnome.h>
#include "gxsnmp.h"

#include "dbapi.h"
#include "ddserver.h"

GxSNMP_GSQLDB_RowHost *
impl_GetRowHost(PortableServer_Servant servant, 
                CORBA_Environment *ev)
{

G_sql *sql;
impl_POA_GxSNMP_GSQLDB *newservant;
gpointer data;
GxSNMP_GSQLDB_RowHost *row;
gchar nulstr[] = "";
gchar nulint = 0;


	newservant = servant;
	sql = newservant->sql;
	row = GxSNMP_GSQLDB_RowHost__alloc();
	memset(row, 0, sizeof(row));

	if(g_sql_next_row(sql) == FALSE){ /*stupid*/
		goto fail;
	}


	/* Move database row to the corba row */
	data = g_sql_field_pos(sql, 0); /* get field data */
	if(!(row->objid = strtol(data,0,0))) data = &nulint;  /* assign to corba field */

	if(!(data = g_sql_field_pos(sql, 1))) data = nulstr;
	row->created = CORBA_string_dup(data);

	if(!(data = g_sql_field_pos(sql, 2))) data = nulstr;
	row->modified = CORBA_string_dup(data);

	if(!(data = g_sql_field_pos(sql, 3))) data = nulstr;
	row->dnsname = CORBA_string_dup(data);

	if(!(data = g_sql_field_pos(sql, 4))) data = nulstr;
	row->name = CORBA_string_dup(data);

	if(!(data = g_sql_field_pos(sql, 5))) data = nulstr;
	row->description = CORBA_string_dup(data);

	if(!(data = g_sql_field_pos(sql, 6))) data = nulstr;
	row->contact = CORBA_string_dup(data);

	if(!(data = g_sql_field_pos(sql, 7))) data = nulstr;
	row->tags = CORBA_string_dup(data);

	return row;

fail:
	return row;
}
