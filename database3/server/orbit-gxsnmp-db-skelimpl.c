#include <stdio.h>
#include "gxsnmp.h"

#include "dbapi.h"
#include "ddserver.h"
#include "table-common.h"

extern G_sql_connection *dbhandler;

/*******************
**** PROTOTYPES ****
********************/

static void impl_TableOpen (
			PortableServer_Servant servant, 
			GxSNMP_GSQLDB_GSQLDBTable *table, 
			CORBA_Environment *ev);

static void impl_TableClose (
			PortableServer_Servant servant, 
			GxSNMP_GSQLDB_GSQLDBTable *table, 
			CORBA_Environment *ev);

GxSNMP_GSQLDB_RowHost * impl_GetRowHost (
			PortableServer_Servant servant, 
			CORBA_Environment *ev);

GxSNMP_GSQLDB_RowInterface * impl_GetRowInterface (
			PortableServer_Servant servant,
			CORBA_Environment *ev);

GxSNMP_GSQLDB_RowMap * impl_GetRowMap (
			PortableServer_Servant servant,
			CORBA_Environment *ev);

GxSNMP_GSQLDB_RowGraph * impl_GetRowGraph (
			PortableServer_Servant servant,
			CORBA_Environment *ev);



/***** EPV STRUCTURES -- GSQLDB *****/

/*** epv structures ***/
static PortableServer_ServantBase__epv GSQLDBbase_epv = {
        NULL,                   /* _private data */
        NULL,                   /* finalize routine */
        NULL                    /* default_POA routine */
};

static POA_GxSNMP_GSQLDB__epv GSQLDBepv = {
        NULL,                   /* _private */
        impl_TableOpen,
        impl_TableClose,
        impl_GetRowHost,
	impl_GetRowInterface,
	impl_GetRowMap,
	impl_GetRowGraph,
};

/*** vepv structures ***/
static POA_GxSNMP_GSQLDB__vepv GSQLDBvepv = {
        &GSQLDBbase_epv,
        &GSQLDBepv
};


/******** FUNCTIONS *******/



impl_POA_GxSNMP_GSQLDB *
impl__GSQLDBCreate (PortableServer_POA poa, CORBA_Environment *ev)
{
	impl_POA_GxSNMP_GSQLDB *newservant;
	PortableServer_ObjectId *objid;

	newservant = g_new0(impl_POA_GxSNMP_GSQLDB, 1);
	newservant->servant.vepv = &GSQLDBvepv;
	newservant->poa = poa;
	newservant->sql = 0;

	POA_GxSNMP_GSQLDB__init ((PortableServer_Servant) newservant, ev);
	objid = PortableServer_POA_activate_object (poa, newservant, ev);
	CORBA_free(objid);

	return newservant;
}


void
impl__destroy (impl_POA_GxSNMP_GSQLDB *servant, CORBA_Environment *ev)
{
	PortableServer_ObjectId *objid;
	
	objid = PortableServer_POA_servant_to_id (servant->poa, servant, ev);
	PortableServer_POA_deactivate_object (servant->poa, objid, ev);

	CORBA_free (objid);

	POA_GxSNMP_GSQLDB__fini ((PortableServer_Servant) servant, ev);

	g_free(servant);
}

/***** GSQLDB interface ******/
static void
impl_TableOpen(PortableServer_Servant servant, 
                GxSNMP_GSQLDB_GSQLDBTable *table, 
                CORBA_Environment *ev)
{

gchar sqlstr[2000];
G_sql *sql;
db_table_descriptor *tabledesc;
impl_POA_GxSNMP_GSQLDB *newservant;

	newservant = servant;

	tabledesc = db_lookup_table(table->table);

	encode_sql(tabledesc, 0, sqlstr, SQL_SELECT_ALL,0, 0); /* WHY? */
	sql = g_sql_query(dbhandler, sqlstr, strlen(sqlstr));
	if(!(sql = g_sql_query(dbhandler, sqlstr, strlen(sqlstr)))){
		goto fail_load;
	}
	newservant->sql = sql;

	return;

fail_load:
	return;

}

static void
impl_TableClose(PortableServer_Servant servant, 
                GxSNMP_GSQLDB_GSQLDBTable *table, 
                CORBA_Environment *ev)
{
impl_POA_GxSNMP_GSQLDB *newservant;

	newservant = servant;

	g_sql_free_query(newservant->sql);
	newservant->sql = 0;

	return;
}
