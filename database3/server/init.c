/*  $Id$
 *
 * Copyright 2000 Larry Liimatainen
 * init.c -- bootstraping table description information
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  init.c
 *
 */

/* Database description
   initialization code
*/

#include <stdio.h>
#include <glib.h>
#include "dbapi.h"
#include "ddserver.h"
#include "table-common.h"

extern G_sql_connection *dbhandler;
extern GList * table_desc;

void db_init_server_side(db_table_descriptor *table)
{
GList *tdgl;
DB_dbfieldconfig *dfc;
gchar sqlstr[2000];
G_sql_query *sql;

  encode_sql(table, 0, sqlstr, SQL_SELECT_ALL, 0, 0);
  if(!(sql = g_sql_query(dbhandler, sqlstr, strlen(sqlstr)))){
              /*FIX: Notify user, else he must timeout */
              return;
  }         
  tdgl = NULL;
  while(g_sql_next_row(sql)){
    dfc = g_malloc(sizeof(DB_dbfieldconfig));

    dfc->rowid = strtol(g_sql_field_pos(sql, 0),0,0);
    dfc->tablen = g_sql_field_pos(sql, 1);
    dfc->fieldn = g_sql_field_pos(sql, 2);
    dfc->fieldt = strtol(g_sql_field_pos(sql, 3),0,0);
    dfc->number = strtol(g_sql_field_pos(sql, 4),0,0);
    tdgl = db_init_tabdes_add(tdgl, dfc);
  }
  g_sql_free_query(sql);
   
  db_init_sort_tabdes(tdgl);
  db_table_descriptor_calc_size(tdgl);
  db_table_descriptor_calc_offset(tdgl);

  table_desc = tdgl;
}

