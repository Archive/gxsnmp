/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <glib.h>
#include <orb/orbit.h>
#include <liboaf/liboaf.h>

#include "gxsnmp.h"

#define G_SQL_INTERFACE
#define __IN_MAIN_C__

#include "debug.h"

#include "dbapi.h"
#include "plugins.h"

/**************/

gboolean io_callback (gint type, GIOChannel *source, gint len, gpointer data);

/**************/


char gxdd_version[] = "$Id$"; /*usefull to 'strings' the binary*/

void debug(){}				/* for gdb */
gint debug_level, dcsock;
gxdbconfig dbconfig;
extern void tranq_check();
GMainLoop    *loop;
GList *tranq = NULL;		/*this list holds incoming SQL transaction requests*/
GList *profiles = NULL;		/*this list holds all connected users profiles */
GList *notiflist = NULL;        /*notification buffer */
G_sql_connection *dbhandler;	/*handle for database */
db_table_descriptor *inittab;

int main(int argc, char **argv)
{
CORBA_ORB orb;
CORBA_Environment ev;
PortableServer_POA poa;
impl_POA_GxSNMP_GSQLDB *servant;
GxSNMP_GSQLDB object;

  
  G_sql dbsinfo;
  
  /*daemonize*/
  /*
    if(fork() != 0) _exit(0);
    if(setsid() == -1) _exit(0);
    if(fork() != 0) _exit(0);
    umask(777);
    if(chdir("/")<0) _exit(0);
    for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
    open("/dev/null",O_RDWR); dup(0); dup(0);
  */
  /***********/

  if(db_load_enviroment()){
    fprintf(stderr,"Couldn't load configuration, can't start!\n");
    exit(0);
  }
  dbsinfo.engine   = dbconfig.dbtype;
  dbsinfo.host     = dbconfig.dbhost;
  dbsinfo.port     = dbconfig.dbport;
  dbsinfo.user     = dbconfig.dbuser;
  dbsinfo.password = dbconfig.dbpass;
  
  db_plugins_load (dbconfig.dbplug);		/*dir contains database plugins */
  db_plugins_start();
  fprintf(stderr, "connect to %s\n",dbconfig.dbhost);
  dbhandler = g_sql_connect(&dbsinfo); /*libgxsnmp function takes a *G_sql and returns a *G_sql_connection */
  if(!dbhandler)
    {
      fprintf(stderr,"Failed to connect to database\n");
      exit(0);
    }
  g_sql_select(dbhandler, "gxsnmp");

  inittab = db_set_initial_table_description();
  db_init_server_side(inittab);

/**** START CORBA INTERFACE ****/

  oaf_init(argc,argv); CORBA_exception_init (&ev);
  orb = oaf_orb_get(); poa = (PortableServer_POA)
  CORBA_ORB_resolve_initial_references (orb, "RootPOA", &ev);
  if (ev._major != CORBA_NO_EXCEPTION) {
    fprintf (stderr, "Error: %s\n", CORBA_exception_id(&ev));
    _exit (1);
  }
  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);
  if (ev._major != CORBA_NO_EXCEPTION) {
    fprintf (stderr, "Error: %s\n", CORBA_exception_id(&ev));
    _exit (-1);
  }
  servant = impl__GSQLDBCreate (poa, &ev);
  object = PortableServer_POA_servant_to_reference (poa, servant, &ev);
  switch(
    oaf_active_server_register( 
    "OAFIID:gxsnmp-db:ee8e6ae0-c604-11d4-9f30-0800208225d2", object)){
      case OAF_REG_SUCCESS:
      break;               
      case OAF_REG_NOT_LISTED:
        fprintf(stderr,"OAF_REG_NOT_LISTED\n");
      break;
      case OAF_REG_ALREADY_ACTIVE:
        fprintf(stderr,"OAF_REG_ALREADY_ACTIVE\n");
      break;
      case OAF_REG_ERROR:
        fprintf(stderr,"OAF_REG_ERROR\n");
      break;
  }
  CORBA_Object_release (object, &ev);
 

/**** START NATIVE INTERFACE ****/
  
  io_connect_as_server (4000, io_callback, NULL);

  /*think queue check in while() will consume CPU,
  app should sleep through select() or if queue is empty,
  and a timer is used, it must be interupted if queue starts
  growing, from beeing empty, when sleeping.   
  */
fprintf(stderr,"gxdd is online\n");
  loop = g_main_new (TRUE);
  while (g_main_is_running (loop))
    {
      g_main_iteration(1);
      tranq_check();
      notification_traverse();
      notification_list_empty();
    }

  /*before giving up control
    notify all connected clients*/
  g_main_destroy (loop);
  CORBA_exception_free (&ev);
  _exit(0);
  return 0;
}

