/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>

#define G_SQL_INTERFACE
#define __IN_MAIN_C__

#include "debug.h"

#include "dbapi.h"

char version[] = "$Id$"; /*usefull to 'strings' the binary*/

gboolean             tcp_new_connection           (GIOChannel     *source,  GIOCondition   condition, gpointer       data);
gboolean             tcp_client_activity          (GIOChannel     *source, GIOCondition   condition, gpointer       data);

void debug(){}				/* for gdb */
gint debug_level, dcsock;
gchar buffer[2000];			/* same */
gxdbconfig dbconfig;
extern void tranq_check();
GMainLoop    *loop;
GList *tranq = NULL;		/*this list holds incoming SQL transaction requests*/
GList *profiles = NULL;		/*this list holds all connected users profiles */
GList *notiflist = NULL;        /*notification buffer */
G_sql_connection *dbhandler;	/*handle for database */
db_table_descriptor *inittab;

int main(int argc, char **argv)
{
  
  G_sql dbsinfo;
  
  /*daemonize*/
  /*
    if(fork() != 0) _exit(0);
    if(setsid() == -1) _exit(0);
    if(fork() != 0) _exit(0);
    umask(777);
    if(chdir("/")<0) _exit(0);
    for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
    open("/dev/null",O_RDWR); dup(0); dup(0);
  */
  /***********/

  if(db_load_enviroment()){
    fprintf(stderr,"Couldn't load configuration, can't start!\n");
    exit(0);
  }
  dbsinfo.engine   = dbconfig.dbtype;
  dbsinfo.host     = dbconfig.dbhost;
  dbsinfo.port     = dbconfig.dbport;
  dbsinfo.user     = dbconfig.dbuser;
  dbsinfo.password = dbconfig.dbpass;
  
  db_plugins_load (dbconfig.dbplug);		/*dir contains database plugins */
  db_plugins_start();
  fprintf(stderr, "connect to %s\n",dbconfig.dbhost);
  dbhandler = g_sql_connect(&dbsinfo); /*libgxsnmp function takes a *G_sql and returns a *G_sql_connection */
  if(!dbhandler)
    {
      fprintf(stderr,"Failed to connect to database\n");
      exit(0);
    }
  g_sql_select(dbhandler, "gxsnmp");

//  db_table_init();
  inittab = db_set_initial_table_description();
  db_init_server_side(inittab);

  
  tcp_socket_init (dbconfig.ddport);

  /*think queue check in while() will consume CPU,
  app should sleep through select() or if queue is empty,
  and a timer is used, it must be interupted if queue starts
  growing, from beeing empty, when sleeping.   
  */

  loop = g_main_new (TRUE);
  while (g_main_is_running (loop))
    {
      g_main_iteration(1);
      tranq_check();
      notification_traverse();
      notification_list_empty();
    }

  /*before giving up control
    notify all connected clients*/
  g_main_destroy (loop);
  return 0;
}

/* Dummy functions -- Kludgy  */
int gxsnmp_network_new (gpointer dummy)
{
  return 0;
}
int gxsnmp_host_new (gpointer dummy)
{
  return 0;
}
