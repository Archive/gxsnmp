#include "orbit-gxsnmp-db.h"
#include "g_sql.h"

typedef struct {
	POA_GxSNMP_GSQLDB servant;
	PortableServer_POA poa;
	G_sql *sql;
} impl_POA_GxSNMP_GSQLDB;

