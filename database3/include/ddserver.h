/* -*- Mode: C -*-
 * $Id$
 * GXSNMP -- A SNMP Management Application.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifndef __GXSNMPDB_TYPES_H__
#define __GXSNMPDB_TYPES_H__

typedef enum {
  PDUMEMTYPE_NULL = 1,
  PDUMEMTYPE_INT, 		/*Packet encode: MemberType Integer */
  PDUMEMTYPE_STR,		/*Packet encode: MemberType ASCIIZ */
  PDUMEMTYPE_DOUBLE
}GxSNMP_PDU_Types;

typedef enum {		/*This is all table-plugins SQL strings it can build */
  SQL_SELECT = 1,		/* Select based on field value */
  SQL_SELECT_PRI,		/* Select a row using primary key */
  SQL_SELECT_ALL,		/* Select all rows */
  SQL_INSERT,			/* Insert a row */
  SQL_UPDATE,			/* Update a row using primary key */
  SQL_DELETE			/* Delete row at primary key */
} GxSNMP_SQL_Types;

typedef enum {		/* Used by API/Daemon in packet to identify action in packet */
  AUTH = 1,
  DISCONNECT,
  NACK,
  NOTIFY,
  ROW_ADD,
  ROW_DELETE,
  ROW_UPDATE,
  ROW_READ,
  TABLE_LOAD,
  TABLE_LOAD_FILTERED,	/* Load a table using memberrecords as a filter, textual, ** OBSOLETE ** */
  RESULT		/* generic result of a database question */
} GxSNMP_APICMD_Types;

typedef enum {		/*Queries that can be used to ask a table plugin about table info*/
  NUM_FIELDS = 1,		/*Number of fields in table, count is from zero*/
  PRI_KEY_FIELD,		/*What is your primary key field number*/
  TABLE_ID,			/*What is your unique table ID */
  FIELD_NAME,			/*return asciiz of field name, needs a numerical pointer to field*/
  TABLE_POINTER			/*given a table identifier(gint) it returns a pointer into db_tables */
} GxSNMP_TABLEINFO_Types;


/***** database I/O control values ******/

#define DBCTL_RW           0x00000001    /* read_write access, reseted we have read_only access */
#define DBCTL_NOTIFY_BRIEF 0x00000002    /* notify brief          */
#define DBCTL_NOTIFY_FULL  0x00000004    /* notify full           */
#define DBCTL_AUTH         0x00000008    /* if set user is authenticated */

/********** profile info ***************/

typedef struct _profile {       /* holds a connected user profile */
  gint value;			/* connection profiles, eg notify read-write */
  GIOChannel *source;		/* Client IO stream */
} profile;

typedef struct _tranrec {      /* holds a transaction record from the Client API */
  gint action;			/* this is what action to perform on table */
  gchar *table;			/* table to access */
  gpointer members;		/* pointer to all MemberRecords */
  gint mlen;			/* length of MemberRecords */
  GIOChannel *source;		/* Client IO stream, its also identifies whitch profile to use*/
  profile *prf;
} tranrec;

/********** Notification *********/

typedef struct _notification {
  profile *prf;		      /* profile triggering notif update */
  db_table_descriptor *table; /* table that got modified */
  gint rowid;                 /* RECORD ID that got modified */
} notification;

/*********************************/

#endif
/* EOF */
