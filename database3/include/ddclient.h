/* -*- Mode: C -*-
 * $Id$
 * GXSNMP -- A SNMP Management Application.
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Common utility functions.
 */

#ifndef __UTIL_H__
#define __UTIL_H__
#include <glib.h>

/* copy a space seperated word 'w' to destination 'dst' from 'src' */
gchar              *wordcpyt                      (gchar       *dst,
						   gchar       *src,
						   gint        w);

gint    dd_connect                (gchar *IP, gint port);
void    dd_disconnect             (GIOChannel *channel);
gint    dd_read_header            (GIOChannel *channel, gchar *buffer, gint mode);
gchar * dd_read_rest              (GIOChannel *channel, gint len, gchar *header);
gint    dd_write                  (GIOChannel *channel, gint datalen, gchar *data);
GIOChannel * db_server_connect_config  (gchar *host, gint port);
gint    db_server_connect         (gchar *host, gint port);
gint    db_server_disconnect      (GIOChannel *channel);

#endif

/* EOF */
