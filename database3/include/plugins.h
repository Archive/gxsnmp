/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- An snmp management application
 *  Copyright (C) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  plugin.h -- Plugin handling code, shamelessly copied from gnumeric :)
 */

#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include <gmodule.h>

/*
**  The different types of plugins.
**
**  All plugins are loaded at the beginning of the main() file.
**
**  Database plugins are started at an early stage of initialization,
**  because they are needed to load the database.
**
**  Application and collector plugins are started after the entire 
**  database has been read into memory and initialized.
*/

enum {
  PLUGIN_DATABASE    = 0x00000001, /* Database plugin */
  PLUGIN_COLLECTOR   = 0x00000002, /* Collector plugin (No GTK used) */
  PLUGIN_APPLICATION = 0x00000004  /* Application plugin (GTK used) */
};

/*
**  Control block for keeping track of the various plugins
*/

typedef struct _PluginData
{
  GModule    * handle;		/* Identifies the loaded module */
  gchar      * filename;        /* Filename of plugin module */
  gchar      * name;		/* Plugin-assigned name of plugin */
  gint         type;            /* Plugin-assigned type of plugin */
  gint         refcount;        /* Reference cound for plugin */
/*
 * Now comes the plugin functions we would like to be there
 *
 * load_plugin   -- Called when we load the plugin(s).
 *
 * unload_plugin -- Called when we unlocad the plugin.
 *
 * start_plugin  -- Called when we want the plugin to do something.
 *
 * plugin_config -- Called when we need a plugin specific config panel.
 *                  The database plugins will use this.
 *
 * plugin_save   -- Called when we want the plugin to save any config/state 
 *                  information for itself.
 * 
 * plugin_load   -- Called when we want the plugin to load any config/state 
 *                  information.
 */
  int       (* load_plugin)	(struct _PluginData *);
  void      (* unload_plugin)  	(struct _PluginData *);
  void      (* start_plugin) 	(struct _PluginData *);
  void      (* plugin_config)   (struct _PluginData *);
  gboolean  (* plugin_save)     (struct _PluginData *);
  gboolean  (* plugin_load)     (struct _PluginData *);
  gpointer   * private;		/* For private use by the plugin */
}
PluginData;

/*
 * Proposed API for the plugins.
 * pf_type  : Type of function that this plugin provides.
 * pf_argc  : The number of args it accepts
 * pf_argv  : The actual arg/value pairs
 * pf_func  : The function to call
 *
 * I don't understand how this would be used ... - jms
 *
 * I envisoned this as a way to implement multiple functions per plugin.
 * Something like a query_funcs would be implemented and that would return
 * A list of functions and types that a given plugin has/provides..
 */

typedef struct _PluginFuncs
{
  gint     pf_type;              /* What type of function is this */
  gint     pf_argc;              /* argument count */
  gchar    *pf_argv;             /* Argument pairs */
  void     (*pf_func) ();
}
PluginFuncs;

extern GList * plugin_list;

/******************************************************************************
**
**  The API for the plugin interface
**
******************************************************************************/

void		db_plugins_load	                   (gchar *);
void            db_plugins_unload	           (void);
void		db_plugins_start                   ();

/*
** These are the minimal functions that need to be defined in the plugin.
*/
#if 0
int		init_plugin	(PluginData  	* pd);
void		cleanup_plugin	(PluginData  	* pd);

 -- Further extending this..
               
void plugin_config      (PluginData    *pd); /* plugin specific config panel */
void plugin_save_config (PluginData    *pd); /* Tell the plugin to save */
void plugin_load_config (PluginData    *pd); /* Tell the plugin to load */
#endif
#endif

/* EOF */

