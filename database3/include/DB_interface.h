/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- An snmp management application
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  Functions for dealing with DB_interface objects.
 */

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include "tables.h"

DB_interface * interface_create  (void);
void           interface_destroy (DB_interface * dbi);
void           interface_add     (DB_interface * dbi);
void           interface_update  (DB_interface * dbi);
void           interface_delete  (DB_interface * dbi);

DB_interface * interface_find_by_rowid (guint rowid);

#endif

/* EOF */
