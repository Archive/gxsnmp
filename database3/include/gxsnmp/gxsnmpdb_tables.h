/*
**  $Id$
**
**  GXSNMP - An snmp management application
** 
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
** 
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
** 
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
** 
**  GxSNMP table definitions
*/

#ifndef __GXSNMPDB_TABLES_H__
#define __GXSNMPDB_TABLES_H__

#include "g_sql.h"
#include "g_sqldb.h"

extern G_sqldb_table * snmp_sqldb;
extern G_sqldb_table * host_sqldb;
extern G_sqldb_table * interface_sqldb;
extern G_sqldb_table * network_sqldb;
extern G_sqldb_table * map_sqldb;
extern G_sqldb_table * graph_sqldb;
extern G_sqldb_table * hinventory_sqldb;
extern G_sqldb_table * hitem_sqldb;

/*
**  The SNMP table is used to store information about SNMP configurations.
*/

typedef struct _DB_snmp
{
  guint           rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gchar         * name;                 /* Name of SNMP configuration */
  guint           version;              /* SNMP version to use */
  guint           port;                 /* SNMP port to contact */
  guint           retries;              /* Retries before failure */
  guint           timeout;              /* Seconds before timeout */
  gchar         * read_c;               /* Read community string */
  gchar         * write_c;              /* Write community string */

  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */
  GList         * DB_interfaces;        /* GList of all interfaces that use
					   this SNMP definition */
  gpointer        unused1;
  gpointer        unused2;
  gpointer        unused3;
}
DB_snmp;

/*
**  The INTERFACE table is used to store information about network interfaces.
*/

typedef struct _DB_interface
{
  guint           rowid;		/* Unique row identifier */
  gchar         * created;		/* Row creation date/time */
  gchar         * modified;		/* Last row modification date/time */
  guint		  host;			/* RowID of host owning interface */  
  guint		  snmp;			/* RowID of snmp Def. for interface */
  guint		  transport;		/* AF_xxx Def. of transport method */
  gchar 	* address;		/* Interface hardware address */
  gchar		* netmask;		/* IPv4 netmask used by interface */
  gchar		* name;			/* Name of interface, e.g. "eth0" */
  gchar         * tags;                 /* Interface status and flags */

  gpointer	  g_sqldb_private;	/* For use by the g_sqldb library */
  struct _DB_host * DB_host;		/* pointer to parent host structure */
  DB_snmp	* DB_snmp;		/* pointer to snmp definition */
  gpointer	  application;		/* Pointer to application-specific
					   data for the interface.  Currently
					   unused. */
  gpointer        unused1;
}
DB_interface;

/*
**  The HOST table is used to store information about hosts
*/

typedef struct _DB_host
{
  guint           rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gchar         * dns_name;             /* Preferred DNS name of host */
  gchar         * name;                 /* Name of host to display on maps */
  gchar         * description;          /* User-provided description of host */
  gchar         * contact;              /* User-provided contact information */
  gchar         * tags;

  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */
  GList         * DB_interfaces;        /* GList of all interfaces assigned to
                                           this host */
  GList         * DB_graphs;            /* GList of graphical representations
                                           of this host */
  gpointer        application;          /* Pointer to application-specific
                                           data for the host.  Currently
                                           unused. */
  gpointer        unused1;
}
DB_host;

/*
**  The NETWORK table is used to store information about networks
*/

typedef struct _DB_network
{
  guint           rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gchar         * address;              /* Starting address of network */
  gchar         * netmask;              /* IPv4 netmask of network */
  guint           speed;                /* Speed of network, in KBytes/Sec */
  gchar         * name;                 /* User-assigned name of network */
  gchar         * description;          /* User-provided Desc. of network */
  gchar         * contact;              /* User-provided contact information */
  gchar         * tags;

  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */
  GList         * DB_graphs;            /* GList of graphical representations */
  gpointer	  application;		/* Pointer to application-specific
					   data for the network.  Currently
					   unused. */
  gpointer        unused1;
  gpointer        unused2;
}
DB_network;

/*
**  The MAP table is used to store information about a map.
*/

typedef struct _DB_map
{
  guint           rowid;        	/* Unique row identifier */
  gchar         * created;      	/* Row creation date/time */
  gchar         * modified;     	/* Last row modification date/time */
  gchar         * name;         	/* Name of this map */
  gchar         * tab;          	/* Name to display in map tab */
  gchar		* description;  	/* User-provided description of map */

  gpointer	  g_sqldb_private;	/* For use by the g_sqldb library */
  GList		* DB_graphs;		/* GList of DB_graph objects
					   displayed on this map */
  gpointer	  application;		/* Pointer to application-specific
					   data for the network.
					   The GUI application uses this
					   field to point to the gxsnmp_map 
					   object. */
  gpointer        unused1;
  gpointer        unused2;
}
DB_map;

/*
**  The GRAPH table is used to store graphical information about items that
**  appear in maps.
*/

enum
{
  DB_GRAPH_INVALID,			
  DB_GRAPH_HOST,			/* Possibilities for the */
  DB_GRAPH_NETWORK,			/* DB_graph.type field */
  DB_GRAPH_WIRE				
};

typedef struct _DB_graph
{
  gint            rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gint            map;                  /* Map this row belongs to */
  gint            type;                 /* Type code of item below */
  gint		  host;			/* Rowid of associated host */
  gint            network;              /* Rowid of associated network */
  gchar         * details;              /* Widget-specific data for item */
  gdouble         x;                    /* TEMP: x location of object */
  gdouble         y;                    /* TEMP: y location of object */
  gchar		* pixmap;		/* TEMP: pixmap filename */
  gchar         * tags;

  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */
  DB_map	* DB_map;		/* Pointer to associated map */
  struct _DB_host	* DB_host;		/* Pointer to associated host */
  DB_network 	* DB_network;		/* Pointer to associated network */
  gpointer        application;          /* Pointer to application-specific
					   data for the map item.
                                           The GUI application uses this
                                           field to point to either the
					   gxsnmp_host object, the
					   gxsnmp_network object, or the
					   gxsnmp_wire object, depending
				 	   on the value of DB_graph.type. */
}
DB_graph;
/****************************************************************************
 * The inventory of hardware on a given node.
 ***************************************************************************/
typedef struct _DB_hinventory 
{
  gint         rowid;                    /* unique row identifier */
  gchar        *created;
  gchar        *modified;
  gchar        *name;
  gint         item;                     /* Rowid of the associated item */
  gint         host;                     /* Rowid of the associated host */
  gchar        *serial;                  /* serial # of this item */
  gchar        *installed;               /* date/time of installation */
  gchar        *removed;                 /* date/time of de-installation */
  gchar        *group;
  gchar        *description;
  gchar        *model;

  gpointer     g_sqldb_private;
}
DB_hinventory;
/****************************************************************************
 * The Hardware item table
 ***************************************************************************/
typedef struct _DB_hitem
{
  gint         rowid;
  gchar        *group;                   /* group of items this item is in*/
  gchar        *created;
  gchar        *modified;
  gchar        *name;
  gchar        *description;
  gchar        *model;
  gchar        *vendor;

  gpointer     g_sqldb_private;
}
DB_hitem;

/****************************************************************************
**
**  The application programming interface for the GXSNMP database
**
****************************************************************************/

#endif
/* EOF */
