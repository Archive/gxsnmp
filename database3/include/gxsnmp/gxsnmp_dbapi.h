/**** Include some DB core files ***/

#include "macro.h"

/**** GxSNMP predefined object API filters ****/

#include "gxsnmpdb_filter.h"

/**** GxSNMP DB TABLES ****/

#include "gxsnmpdb_tables.h"

/**** GxSNMP ADD-ON API ****/

#include "gxsnmpdb_DB_host.h"
#include "gxsnmpdb_DB_interface.h"
#include "gxsnmpdb_DB_graph.h"
#include "gxsnmpdb_DB_hitem.h"
#include "gxsnmpdb_DB_network.h"
#include "gxsnmpdb_DB_snmp.h"
#include "gxsnmpdb_DB_map.h"
#include "gxsnmpdb_DB_hinventory.h"

void            table_load_topology     ();
void            table_load_graphical    ();

