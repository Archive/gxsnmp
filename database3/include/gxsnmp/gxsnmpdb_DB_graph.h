/*
**  $Id$
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Functions for dealing with DB_graph objects.
*/

#ifndef __GRAPH_H__
#define __GRAPH_H__

#include "tables.h"

GList    * graph_load          (void);
DB_graph * graph_create        (void);
void       graph_destroy       (DB_graph * dbg);
void       graph_add           (DB_graph * dbg);
void       graph_update        (DB_graph * dbg);
void       graph_delete        (DB_graph * dbg);
DB_graph * graph_find_by_rowid (guint      rowid);

DB_graph * graph_find_host     (guint      host_rowid,
				guint	   map_rowid);

DB_graph * graph_find_network  (guint      network_rowid, 
				guint 	   map_rowid);

DB_graph * graph_find_wire     (guint      map_rowid, 
				guint 	   host_rowid, 
				guint 	   network_rowid);

#endif

/* EOF */
