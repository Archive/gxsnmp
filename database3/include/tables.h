/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP - An snmp management application
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 * 
 *  Database column definitions.
 * OBJECT DEFINITION IS FOUND IN BOTTOM
 */

#ifndef __TABLES_H__
#define __TABLES_H__

#include "g_sql.h"
#include "g_sqldb.h"

typedef struct _DB_dbfieldconfig
{
  guint           rowid;
  gchar         * tablen;
  gchar         * fieldn;
  gchar         * fieldt;
  guint           number;

  gpointer        unused1;
  gpointer        unused2;
  gpointer        unused3;
  gpointer        unused4;
  gpointer        unused5;
}
DB_dbfieldconfig;

/*
**  The SNMP table is used to store information about SNMP configurations.
*/

/* OBJECT DATABASE DEFINITIONS 
 *
 * This should be a single file, but when everybody includes tables.h
 * I use this for now.
 */

/* This enumeration defines what gxobject.type to
   assign, the type tells us what type gxsobject.obj is */

typedef enum {
  OBJ_DB = 1
} GxSNMP_OBJECT_CONTEXT;

/* database find filter */

typedef enum {
  OBJ_FIND_MULTI = 1,
  OBJ_FIND_SINGLE
} GxSNMP_OBJECT_FILTER;


typedef struct _db_filter  {
  gint method;			/* match method */
  gchar *name;
  GList *entrys;
  gchar **args;			/* a setup of filters */
} db_filter;

typedef struct _db_filter_entry  {
  gchar *field;
  gpointer value;
} db_filter_entry;

/* CONFIGURATION */

typedef struct {
        gchar base[200];
        gchar tabplug[200];		/* table-plugins directory */
        gchar dbplug[200];		/* database plugins */
        char  ddhost[200];		/* database daemon host */
        gint  ddport;			/* database daemon port */
        gchar dbtype[40];
        gchar dbhost[40];
        gint  dbport;
        gchar dbuser[80];
        gchar dbpass[80];
        
}gxdbconfig;

extern gxdbconfig dbconfig;

/*********************** 
 TABLE DESCRIPTOR STRUCT
***********************/
typedef struct _db_table_descriptor {
  gchar    * name;
  guint      size;
  GList    * fields;
} db_table_descriptor;

/**********************
**********************/

/*********************** 
 FIELD DESCRIPTOR STRUCT
***********************/
typedef struct _db_field_descriptor {
  gchar    * name;
  guchar     type;
  guint      offset;
} db_field_descriptor;

/**********************
**********************/

#define TD_GUINT     1
#define TD_GINT      2
#define TD_GSTR      3
#define TD_PGUINT    4    /* not-in-database */
#define TD_PGINT     5    /* not-in-database */
#define TD_PGSTR     6    /* not-in-database */
#define TD_PGL       7    /* not-in-database GList */
#define TD_GDOUBLE   8
#define TD_PGDOUBLE  9    /* not-in-database */

/*********** ddserver response storage struct *********/
typedef struct _db_ddr {
  gint len;			/* length of data */
  gpointer data;		/* raw data from response */
} db_ddr;

#if 0
/****************/
/***** break down of data *********/
/*********** Notification *********/
/*db database daemon result notification */
typedef struct _db_ddr_notif {
  GIOChannel *channel;		/* socket to ddserver notif came from */
  gint type ;                   /* Notification type */
  db_table_descriptor *table;	/* target Table of notification */
  db_field_descriptor *field;	/* target field */
  gint rowid;			/* target Database record */
} db_ddr_notif;

typedef struct _db_notif {
  void (* func)(GIOChannel *channel, 
                gint type,
                guint rowid, 
                gchar *table,
                gpointer data);		/* registered user notification callback */
} db_notif;
#endif

/****************/

typedef enum {          /*This is all table-plugins SQL strings it can build */
  DB_NOTIF_TABLE_CHANGED = 1,    /* db-table has changed */
  DB_NOTIF_TABLE_FIELD_CHANGED,  /* db-field has changed */
  DB_NOTIF_TABLE_RECORD_CHANGED  /* db-record has changed */
} DB_Notification_Types;

#endif
/* EOF */
