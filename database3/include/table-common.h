/**** THIS IS FROM util.c ****/
gchar * db_timestamp ();

/***** End util.c *****/

gint            db_lookup_field_offset  (db_table_descriptor *table, 
                                        gchar *name);
gint            db_lookup_field_type    (db_table_descriptor *table, 
                                        gchar *fieldname);

gboolean        decode_sql              (db_table_descriptor *table,
                                        gpointer packet,
                                        G_sql_query *sql,
                                        gint numfields,
                                        gint *n);

gint            encode_members          (db_table_descriptor *table, 
                                        gpointer packet, 
                                        gpointer data);

gpointer        decode_members          (db_table_descriptor *table, 
                                        gpointer packet, 
                                        gint *n, 
                                        gint plen);

gboolean        encode_sql              (db_table_descriptor *table, 
                                        gpointer packet, 
                                        gchar *sqlstr, 
                                        gint querytype, 
                                        gint *n, 
                                        gint tpl);
