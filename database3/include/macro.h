gint g_db_assign_rowid	(G_sqldb_table *sqldb, 
			gpointer row, gchar *tablename, gchar *fieldname);
gint g_db_timestamp	(gpointer row, gchar *tablename, 
			gchar *fieldname);
gint g_db_add_reference	(G_sqldb_table *sqldb, 
                        gpointer row, gpointer rowid, 
                        gchar *t1, gchar *f1, 
                        gchar *t2, gchar *f2 );
gint g_db_del_reference	(gpointer row, 
                        gchar *t1, gchar *f1, 
                        gchar *t2, gchar *f2 );
