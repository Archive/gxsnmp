gint	get_member	(gint *n, gpointer packet, gpointer buffer);
void	add_guint	(gint *n, gpointer packet, guint data);
void	add_gdouble	(gint *n, gpointer packet, gdouble data);
void	add_asciiz	(gint *n, gpointer packet, gpointer str);
gint	set_pdu_header	(gpointer packet, gint type, gchar *table_name);

typedef union _pdumember {
  gchar string[2049];
  guint integer;
  gdouble dbl;
  } pdumember;
