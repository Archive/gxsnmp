/*  $Id$
 *
 * Copyright 2000 Larry Liimatainen
 * ddclient.c -- client connection routines
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  ddclient.c
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <stdio.h>

#include "tables.h"
#include "g_sqldb.h"
#include "ddserver.h"
#include "ddclient.h"
#include "table-common.h"
#include "pdu.h"

static GList *dbnotiflist;
static GList *dbserverdatalist;


#define HEADER (sizeof(gint) * 2) /* this defines how much to read in the header to retreive total packet length */

void
db_notification_add(gpointer func)
{
  dbnotiflist = g_list_append(dbnotiflist, func);
}

void
db_notification_del(gpointer func)
{
  dbnotiflist = g_list_remove(dbnotiflist, func);
}

gboolean
dd_server_activity(GIOChannel *source, GIOCondition cond, gint len)
{
  guint         num_read = 0;
  gchar        *buffer;
  gint n;
  db_ddr       *result;
  GList *gl;

  union _member {
  gchar string[2049];
  guint integer;
  guint dbl;
  } member;

  gint     datalen;
  gint     rowid;
  gint     type;
  gchar   *table;
  gpointer data;

  void (* func)(GIOChannel *, 
                gint ,
                guint , 
                gchar *,
                gpointer);         /* registered user notification callback */

  buffer = g_malloc(len);
  if(cond == G_IO_IN){
    switch(g_io_channel_read (source, buffer, len, &num_read)){
      case G_IO_ERROR_NONE:
        if(!num_read){
          return TRUE;
        }
        /* Read in rest of data */
        memcpy(&datalen,buffer+sizeof(gint), sizeof(guint)); /* get TPL */
        if(datalen <= 8) return TRUE; /* table is empty */
        if(!(data = dd_read_rest(source, datalen, buffer))){
          fprintf(stderr,"  dd_read_rest() error\n");
          return TRUE;
        }
        memcpy(&n, data, sizeof(gint));
        switch(n){
          case NOTIFY:
            n = sizeof(gint)*2; /*move past NOTIFY+TPL */
            memcpy(&type,  data+n, sizeof(gint));
            n += sizeof(gint);
            memcpy(&rowid, data+n, sizeof(gint));
            n += sizeof(gint);
            get_member(&n, data, &member);
            if(strlen(member.string)){
              table = db_lookup_table(member.string);
            }
            gl = dbnotiflist;
            while(gl){   /* call each registered notifcation callbacks */
              if(gl->data){
                func = gl->data;
                func(source, type, rowid,
                     table, data);
              }
              gl = gl->next;
            }
          break;
          default: /* NORMAL USER DATA */
            result = g_malloc0(sizeof(db_ddr));
            result->len = datalen;
            result->data = data;
            dbserverdatalist = g_list_append(dbserverdatalist, result);
          break;
        }
      break;
      case G_IO_ERROR_AGAIN:
        fprintf(stderr,"ddclient.c: AGAIN CONDITION ");
      break;
      case G_IO_ERROR_INVAL:
        fprintf(stderr,"ddclient.c: INVAL CONDITION ");
      break;
      case G_IO_ERROR_UNKNOWN:
        fprintf(stderr,"ddclient.c: GUNKNOWN CONDITION ");
      break;
      default:
        fprintf(stderr,"ddclient.c: UNKNOWN CONDITION ");
      break;
    }
  }
  g_free(buffer);
  return TRUE;
}

/**
 * dd_read_header:
 *
 * this routine reads just as much it needs
 * on incoming data stream to determine how
 * much it has to read, ie read Total Packet Length
 *
 * RETURN:
 * good: number of bytes readed 
 * fail: -1
 *
 **/
gint 
dd_read_header(GIOChannel *channel, gchar *buffer, gint mode)
{
  gint num_read;

  if(g_io_channel_read(channel, buffer, HEADER, &num_read) == G_IO_ERROR_NONE){
    if(num_read == HEADER){
      return num_read;
    }
  }
  return -1;   
}

/**
 * dd_read_rest:
 *
 **/
gchar *
dd_read_rest(GIOChannel *channel, gint len, gchar *header)
{
char *bulk;
int n=HEADER;
gint num_read;
  
  if (!(bulk = (char *) g_malloc(len+1))) return 0;
  memcpy(bulk, header, HEADER);
  
  if(g_io_channel_read(channel, bulk+n, len-n, &num_read) == G_IO_ERROR_NONE){
    if(num_read == (len-n)) return bulk;
  }  

  g_free(bulk);
  return 0;
}

/**
 * dd_write:
 *
 **/
gint 
dd_write(GIOChannel *channel, gint datalen, gchar *data)
{
  int num_write;
  
  if(g_io_channel_write(channel, data, datalen + 1, &num_write) == G_IO_ERROR_NONE){
    return num_write;
  }
  return 0;
}

db_ddr *
db_write_read(GIOChannel *channel, gint len, gchar *buffer)
{
  gint num_write;  
  GList *gl;
  db_ddr *result;

  if(g_io_channel_write(channel, buffer, len + 1, &num_write) == G_IO_ERROR_NONE){
    /* Now await response */
    dd_server_activity(channel, G_IO_IN, HEADER);
    gl = dbserverdatalist;                       
    if(gl){
      result = gl->data;
      dbserverdatalist = g_list_remove(dbserverdatalist, result);
      return result;                                             
    }
  }
  return 0;
}

/* db_server_connect_config()
*
*
*/
GIOChannel *
db_server_connect_config(gchar *host, gint port)
{
GIOChannel *channel;

  channel = io_connect_as_client_private (host, port, dd_server_activity, HEADER);
  if(!channel) return 0;

  db_set_initial_table_description();
  db_init_client_side(channel);

  return channel;
}

/* db_server_connect()
*
*
*/
GIOChannel *channel;
db_server_connect(gchar *host, gint port)
{
  return io_connect_as_client_private (host, port, dd_server_activity, HEADER);
}

/* db_server_disconnect()
*
*
*/
gint
db_server_disconnect(GIOChannel *channel)
{
  gchar packet[500];
  gint h;

  h = set_pdu_header(packet, DISCONNECT, "");
  memcpy(packet+h-sizeof(gint), &h, sizeof(gint));
  dd_write(channel, h, &packet);

  g_io_channel_close (channel);
  return 0;
}
