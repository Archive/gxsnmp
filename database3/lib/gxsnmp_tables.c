/*
**  $Id$
**
**  GXSNMP - An snmp management application
** 
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
** 
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
** 
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
** 
**  Database tables definitions
*/

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

static
G_sqldb_table snmp_db_struct =
{
  0                  ,                          /* Pointer to G_sql structure */
  0                          ,                   /* Pointer to database name */
  "snmp",                                       /* Pointer to table name */
  0,
  0,
  0,
  sizeof (DB_snmp),                             /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_snmp, g_sqldb_private),   /* Offset of private hndl */
  0                                             /* List of column definitions */
};

G_sqldb_table * snmp_sqldb = &snmp_db_struct;


static
G_sqldb_table host_db_struct =
{
  0                          ,                  /* Pointer to G_sql structure */
  0                          ,                   /* Pointer to database name */
  "host",                                       /* Pointer to table name */
  0,
  0,
  0,
  sizeof (DB_host),                             /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_host, g_sqldb_private),   /* Offset of private handle */
  0                                             /* List of column definitions */
};

G_sqldb_table * host_sqldb = &host_db_struct;

static
G_sqldb_table interface_db_struct =
{
  0                          ,                  /* Pointer to G_sql structure */
  0                          ,                   /* Pointer to database name */
  "interface",                                  /* Pointer to table name */
  0,
  0,
  0,
  sizeof (DB_interface),                        /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_interface, g_sqldb_private), /* Offset of private hndl */
  0                                            /* List of column definitions */
};

G_sqldb_table * interface_sqldb = &interface_db_struct;

G_sqldb_table network_db_struct =
{
  0                          ,                  /* Pointer to G_sql structure */
  0                          ,                  /* Pointer to database name */
  "network",                                    /* Pointer to table name */
  0,
  0,
  0,
  sizeof (DB_network),                          /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_network, g_sqldb_private), /* Offset of private hndl */
  0                                             /* List of column definitions */
};

G_sqldb_table * network_sqldb = &network_db_struct;

static
G_sqldb_table map_db_struct =
{
  0                          ,                  /* Pointer to G_sql structure */
  0                          ,                  /* Pointer to database name */
  "map",                                        /* Pointer to table name */
  0,
  0,
  0,
  sizeof (DB_map),                              /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_map, g_sqldb_private),    /* Offset of private hndl */
  0                                             /* List of column definitions */
};

G_sqldb_table * map_sqldb = &map_db_struct;

static 
G_sqldb_table graph_db_struct =
{
  0                          ,                  /* Pointer to G_sql structure */
  0                          ,                  /* Pointer to database name */
  "graph",                                      /* Pointer to table name */
  0,
  0,
  0,
  sizeof (DB_graph),                            /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_graph, g_sqldb_private),  /* Offset of private handle */
  0                                             /* List of column definitions */
};

G_sqldb_table * graph_sqldb = &graph_db_struct;

static
G_sqldb_table hitem_db_struct =
{
  0                          ,
  0                          ,
  "hitems",
  0,
  0,
  0,
  sizeof (DB_hitem),
  G_STRUCT_OFFSET (DB_hitem, g_sqldb_private),
  0              
};
G_sqldb_table  *hitem_sqldb = &hitem_db_struct;

static
G_sqldb_table hinventory_db_struct =
{
  0                          ,
  0                          ,
  "hinventory",
  0,
  0,
  0,
  sizeof (DB_hitem),
  G_STRUCT_OFFSET (DB_hinventory, g_sqldb_private),
  0              
};
G_sqldb_table  *hinventory_sqldb = &hinventory_db_struct;

