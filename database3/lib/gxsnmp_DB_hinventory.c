/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

/* 
 * DB_hinventory.c containes utility functions related to the creation and 
 * destruction of DB_hinventorys, and functions to manage the DB_hinventory 
 * database.
 */
#include <config.h>
#include <gnome.h>
#include "g_sqldb.h"
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "gxsnmp/gxsnmpdb_DB_interface.h"
#include "debug.h"

/****************************************************************************
 * hinventory_create ()
 ***************************************************************************/
DB_hinventory *
hinventory_create (void)
{
  DB_hinventory *dbi;
  D_FUNC_START;
  dbi           = g_new0 (DB_hinventory, 1);
  if (dbi)
    {
      dbi->rowid = g_sqldb_highest_rowid (hinventory_sqldb, "_rowid") + 1;
      DPRT ("hinventory_create: memory allocated initlizing structre.");
      dbi->created  = g_strdup (_("(not in database)"));
      dbi->modified = g_strdup (dbi->created);
      D_FUNC_END;
      return dbi;
    }
  DPRT ("hinventory_create: unable to alloc memory for new hinventory object.");
  D_FUNC_END;
  g_warning ("hinventory_create unable to allocate memory, expect trouble.\n");
  return NULL;

}
/****************************************************************************
 * hinventory_destroy ()
 ***************************************************************************/
void 
hinventory_destroy (DB_hinventory *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  if (dbi->rowid != 0)
    {
      g_warning ("hinventory_destroy: attempt to destroy an object still in "
		 "use.");
      D_FUNC_END;
      return;
    }
  if (dbi)
    {
      DPRT ("hinventory_destroy: valid structure, freeing data.");
      if (dbi->created)        g_free (dbi->created);
      if (dbi->modified)       g_free (dbi->modified);
      if (dbi->name)           g_free (dbi->name);
      if (dbi->serial)         g_free (dbi->group);
      if (dbi->installed)      g_free (dbi->description);
      if (dbi->removed)        g_free (dbi->model);
      DPRT ("hinventory_destroy: freeing structure.");
      g_free (dbi);
    }
  D_FUNC_END;
}
/****************************************************************************
 * hinventory_add ()
 ***************************************************************************/
void
hinventory_add (DB_hinventory *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  if (dbi->rowid != 0)
    {
      g_warning ("Attempted to re add a hinventory with a rowid of 0");
      return ;
    }
  dbi->rowid = g_sqldb_highest_rowid (hinventory_sqldb, "_rowid") + 1;
  if (dbi->created) 
    g_free (dbi->created);
  dbi->created = db_timestamp ();
  if (dbi->modified) 
    g_free (dbi->modified);
  dbi->modified = g_strdup (dbi->created);

  g_sqldb_row_add (hinventory_sqldb, dbi);
  D_FUNC_END;
}
/****************************************************************************
 * hinventory_update ()
 ***************************************************************************/
void 
hinventory_update (DB_hinventory *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  g_assert (dbi->rowid != 0);
  if (dbi->modified) 
    g_free (dbi->modified);
  dbi->modified = db_timestamp ();
  g_sqldb_row_update (hinventory_sqldb, dbi);
  D_FUNC_END;
}
/****************************************************************************
 * hinventory_delete ()
 * 
 * This really will be a rarely used function as inventory items will only
 * be deleted when the host that they were in gets deleted.
 ***************************************************************************/
void
hinventory_delete (DB_hinventory *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  g_return_if_fail (dbi->rowid != 0);
  g_assert (g_sqldb_row_delete (hinventory_sqldb, dbi) == TRUE);
  dbi->rowid = 0;
  D_FUNC_END;
}
/****************************************************************************
 * hinventory_list ()
 ***************************************************************************/
GList *
hinventory_list (void)
{
  return g_sqldb_table_list (hinventory_sqldb);
}
/****************************************************************************
 * hinventory_find_by_rowid ()
 ***************************************************************************/
DB_hinventory *
hinventory_find_by_rowid (guint rowid)
{
  g_return_val_if_fail (rowid != 0, NULL);
  return g_sqldb_row_find (hinventory_sqldb, "_rowid", &rowid);
}

/* EOF */

