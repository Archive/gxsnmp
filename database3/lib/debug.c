#include <stdio.h>
#include <glib.h>
#include "dbapi.h"
#include "ddserver.h"
#include "table-common.h"

void
db_debug_dump_table_descriptors(GList *tdgl)
{
  GList *gl;
  db_table_descriptor *tab;
  db_field_descriptor *fie;

  fprintf(stderr,"Printing Table descriptors.\n");
  while(tdgl){
    tab = tdgl->data;
    fprintf(stderr,"Tablename(%s)\n", tab->name);
    gl = tab->fields;                            
    while(gl){       
      fie = gl->data;
      fprintf(stderr,"  fieldname(%s)offset(%d)type(%d)\n",
              fie->name, fie->offset, fie->type);
      gl = gl->next;
    }               
    tdgl = tdgl->next;
  }
  fprintf(stderr,"Printing Table descriptors done\n");
}

