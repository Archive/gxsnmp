/*  $Id$
 *
 * Copyright 2000 Larry Liimatainen
 * dbfilter.c -- database filters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  dbfilter.c
 *
 */


#include <glib.h>
#include <stdio.h>
#include "tables.h"

gint db_filter_setup(db_filter *filter, gchar * name, guint method)
{
  filter->name = name;
  filter->method = method;
  filter->entrys = NULL;
  return 0;
}

gint db_filter_add(db_filter *filter, gchar * field, gpointer value)
{
  db_filter_entry *filent;

  filent = g_malloc0(sizeof(db_filter_entry));
  filent->field = field;  /* field_name: 'name' in HOST table */
  filent->value = value; /* field_value to search for */
  filter->entrys = g_list_append(filter->entrys, filent);

  return 0;
}

/* db_filter_modify()
*
*/
gint db_filter_modify(db_filter *filter, gchar * field, gpointer value)
{
  db_filter_entry *filent;
  GList *gl;

  gl = filter->entrys;
  while(gl){
    filent = gl->data;
    if(!(strcmp(filent->field, field))){
      filent->value = value;
      return 0;
    }
  }

  return -1;
}

gint
db_filter_del(db_filter *filter, gchar * field)
{
  db_filter_entry *filent;
  GList *gl;

  gl = filter->entrys;
  while(gl){
    filent = gl->data;
    if(!(strcmp(filent->field, field))){
      filter->entrys = g_list_remove(filter->entrys, filent);
      return 0;
    }
  }


  return -1;
}
