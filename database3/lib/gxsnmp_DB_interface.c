/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*/

/*
**  DB_interface.c contains utility functions related to the management of
**  DB_interface objects.
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <stdio.h>
#include <glib.h>

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

#include "debug.h"


/******************************************************************************
**
**  Function to create a new DB_interface object, and initialize it with 
**  default values, including the zero rowid which marks it as being
**  "not in the database"
**
******************************************************************************/

DB_interface *
interface_create (void)
{
  DB_interface * dbi;

  D_FUNC_START;
  dbi           = g_new0 (DB_interface, 1);
  if (dbi)
    {
      dbi->rowid = g_sqldb_highest_rowid (interface_sqldb, "_rowid") + 1;
      dbi->created   = g_strdup ("(not in database)");
      dbi->modified  = g_strdup (dbi->created);
      D_FUNC_END;
      return dbi;
    }
  d_print (DEBUG_TRACE, "Returning a NULL structure pointer");
  g_warning ("interface_create() memory allocation failed.");
  D_FUNC_END;
  return NULL;

}

/******************************************************************************
**
**  Function to destroy a DB_interface object.  The rowid must be zero, which
**  marks it as being "not in the database"
**
******************************************************************************/

void
interface_destroy (DB_interface * dbi)
{
  D_FUNC_START;
  g_assert (dbi->rowid == 0);
  if (dbi->created)  g_free (dbi->created);
  if (dbi->modified) g_free (dbi->modified);
  if (dbi->address)  g_free (dbi->address);
  if (dbi->netmask)  g_free (dbi->netmask);
  if (dbi->name)     g_free (dbi->name);
  g_free (dbi);
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to add a DB_interface object to the database.  The rowid must be
**  zero, meaning that the item is not already in the database.
**
******************************************************************************/

void
interface_add (DB_interface * dbi)
{
  D_FUNC_START;
  g_assert (dbi->rowid   == 0);
  g_assert (dbi->DB_host == NULL);
  g_assert (dbi->DB_snmp == NULL);

  dbi->rowid = g_sqldb_highest_rowid (interface_sqldb, "_rowid") + 1;

  g_db_add_reference(host_sqldb, dbi, &dbi->host, "interface", 
     "DB_host", "host", "DB_interfaces");

  g_db_add_reference(snmp_sqldb, dbi, &dbi->snmp, "interface", 
     "DB_snmp", "snmp", "DB_interfaces");


  g_db_timestamp(dbi, "interface", "created");
  if (dbi->modified) g_free (dbi->modified);
  dbi->modified = g_strdup (dbi->created);

  g_sqldb_row_add (interface_sqldb, dbi);
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to update a DB_interface object in the database.  The rowid must be
**  non-zero, meaning that the item is already in the database.
**
******************************************************************************/

void
interface_update (DB_interface * dbi)
{
  D_FUNC_START;
  g_assert (dbi->rowid != 0);
  
  if (dbi->DB_host)				/* If previously assigned */
    if (dbi->DB_host->rowid != dbi->host)	/* to a different host ... */
      {						/* then unassign it */
        g_db_del_reference(dbi,"interface", "DB_host", "host", "DB_interfaces");
      } 

  if (dbi->host)		/* Is the interface assigned to a host? */
    if (!dbi->DB_host)		/* Yes -- add to host GList if necessary */
      {	
        g_db_add_reference(host_sqldb, dbi, &dbi->host, "interface", "DB_host", 
                    "host", "DB_interfaces");
      }

  if (dbi->DB_snmp)                             /* If previously using a */
    if (dbi->DB_snmp->rowid != dbi->snmp)       /* different SNMP setting */
      {                                         /* then unassign it */
        g_db_del_reference(dbi,"interface", "DB_snmp", "snmp", "DB_interfaces");
      }

  if (dbi->snmp)                /* Is the interface assigned SNMP settings */
    if (!dbi->DB_snmp)          /* Yes -- add to snmp GList if necessary */
      {
        g_db_add_reference(snmp_sqldb, dbi, &dbi->snmp, "interface", "DB_snmp", 
                    "snmp", "DB_interfaces");
      }

  g_db_timestamp(dbi, "interface", "modified");

  g_sqldb_row_update (interface_sqldb, dbi);
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to delete a DB_interface object from the database.  The rowid
**  must be non-zero, meaning that the item is already in the database.
**
**  When we finished, we set the rowid to zero, meaning that the item is
**  no longer in the database, and return.  The caller is responsible for
**  making a call to host_destroy to free the memory associated with the
**  DB_interface block.
**
******************************************************************************/

void
interface_delete (DB_interface * dbi)
{
  D_FUNC_START;
  g_assert (dbi->rowid != 0);

  if (dbi->DB_host){                             /* If assigned to a host */
    fprintf(stderr,"\ninterface_delete(): DBI(%u)\n", dbi->rowid);
    g_db_del_reference(dbi,"interface", "DB_host", "host", "DB_interfaces");
  }
  dbi->DB_host = NULL;

  if (dbi->DB_snmp)                             /* If assigned SNMP settings */
    g_db_del_reference(dbi,"interface", "DB_snmp", "snmp", "DB_interfaces");
  dbi->DB_snmp = NULL;
  
  g_assert (g_sqldb_row_delete (interface_sqldb, dbi) == TRUE); 

  dbi->rowid = 0;
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to locate an interface by the database rowid
**
******************************************************************************/

DB_interface *
interface_find_by_rowid (guint rowid)
{
  d_print (DEBUG_TRACE, "\n");
  return g_sqldb_row_find (interface_sqldb, "_rowid", &rowid);
}

/* EOF */

