/* -*- Mode: C -*-
 *  $Id$
 *  Copyright (C) 2000 Larry Liimatainen
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  GxSNMP Predefined filters for loaded database tables.
 */

#include <glib.h>
#include <stdio.h>
#include "tables.h"

/* GXSNMP specific section
*
* here we define functions and filters
* that is used by Gxsnmp.
*
*/

db_filter db_interface_find_by_rowid;
db_filter db_network_find_by_address;
db_filter db_host_find_by_name;
db_filter db_graph_find_wire;
db_filter db_graph_find_network;
db_filter db_graph_find_host;
db_filter db_find_by_rowid;

/* gxsnmp_db_filter_init()
* initiate all gxsnmp filters
* for ready to use.
*/
gint gxsnmp_db_filter_init()
{

const static gint fooint = 0;

/* setup db_host_find_by_name */

  db_filter_setup(&db_host_find_by_name, "host", OBJ_FIND_SINGLE);
  db_filter_add(&db_host_find_by_name, "name", "");

/* setup db_network_find_by_address */

  db_filter_setup(&db_network_find_by_address, "network", OBJ_FIND_SINGLE);
  db_filter_add(&db_network_find_by_address, "network", "");
  db_filter_add(&db_network_find_by_address, "netmask", "");

/* setup db_graph_find_wire */

  db_filter_setup(&db_graph_find_wire, "graph", OBJ_FIND_SINGLE);
  db_filter_add(&db_graph_find_wire, "map", &fooint);
  db_filter_add(&db_graph_find_wire, "network", &fooint);

/* setup db_graph_find_network */

  db_filter_setup(&db_graph_find_network, "graph", OBJ_FIND_SINGLE);
  db_filter_add(&db_graph_find_network, "map", &fooint);
  db_filter_add(&db_graph_find_network, "network", &fooint);

/* setup db_graph_find_host */

  db_filter_setup(&db_graph_find_host, "graph", OBJ_FIND_SINGLE);
  db_filter_add(&db_graph_find_host, "map", &fooint);
  db_filter_add(&db_graph_find_host, "host", &fooint);

/* setup db_find_by_rowid */

  db_filter_setup(&db_find_by_rowid, "noname", OBJ_FIND_SINGLE);
  db_filter_add(&db_find_by_rowid, "_rowid", &fooint);

}

/*EOF*/
