/*  $Id$
 *
 *  Copyright 2000 Larry Liimatainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  dbconfig.c -- configuration files loading
 *
 */

/* configuration code
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#include "tables.h"

gxdbconfig dbconfig;

static void restrap(char *line) 
{
  if(!(strncmp(line,"\"",1)))
  {
    if (!(strncmp(line+(strlen(line)-1),"\"",1)))
    {
      strncpy(line,line+1,strlen(line)-2);
      strncpy(line+strlen(line)-2,"\0",1);
    }   
  }
}
/**
 * db_load_environment:
 *
 * Updated this to do some redumentary searching rather then just reling on
 * an environment variable. Though if the env variable _is_ set it will
 * override everything and be used.
 **/
int
db_load_enviroment()
{
  char line[200],entity[200],value[200];
  char *env;
  FILE *fp;

  
  env = g_getenv("GXDB_BASE");
  if(!env)
    {
      /* Look in the home dir of the user running the server. */
      env = g_strdup_printf("%s/.gxsnmp/gxdb.conf", g_getenv("HOME"));
      if ( !(fp = fopen (env, "r")) )
	{
	  /* I don't like the hardcoded paths like this .... */
	  env = "/usr/share/gxsnmp/conf/gxdb.conf";
	  if (! (fp = fopen (env, "r")) )
	    {
              env = "/etc/gxdb.conf";
              if (! (fp = fopen (env, "r")) )
               {
	         g_warning ("Unable to locate the config file, please insure "
		  	 "the config file is installed somewhere.\n");
	         return -1;
               }
	    }
	}
      fclose (fp);
    }
  else
    {
      /* We are using the environment specified path */
      env = g_strdup_printf("%s/gxdb.conf", env);
    }
  /* At this point we should have a valid file. */
  dbconfig.ddport = 4000; /*default port to use */
  
  if(!(fp = fopen(env, "r")))
    {
      g_warning ("Unable to open config file %s\n", env);
      return -1;
    }
  /* This is some scary stuff here... */
  while(fgets(line,200,fp))
    {
      if(line[0] == '#') continue;
      wordcpyt(entity,line,"=",1);
      wordcpyt(value,line,"=",2);
      restrap(value);
      if(!strcmp("DATABASE_DAEMON_PORT",entity)) dbconfig.ddport = atoi(value);
      else if(!strcmp("TABLE_PLUGINS",entity)) strcpy(dbconfig.tabplug, value);
      else if(!strcmp("DATABASE_PLUGINS",entity)) strcpy(dbconfig.dbplug, value);
      else if(!strcmp("DATABASE_DAEMON_HOST",entity)) strcpy(dbconfig.ddhost, value);
      else if(!strcmp("DATABASE_PORT",entity)) dbconfig.dbport = atoi(value);
      else if(!strcmp("DATABASE_ENGINE",entity)) strcpy(dbconfig.dbtype, value);
      else if(!strcmp("DATABASE_HOST",entity)) strcpy(dbconfig.dbhost, value);
      else if(!strcmp("DATABASE_USER",entity)) strcpy(dbconfig.dbuser, value);
      else if(!strcmp("DATABASE_PASSWORD",entity)) strcpy(dbconfig.dbpass, value);
    }
  
  fclose(fp);
  return 0;
}
