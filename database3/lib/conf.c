/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- A SNMP Management Application.
 *  Copyright 2000 Larry Liimatainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  conf.c -- table configuration loading
 *
 */

#ifdef HAVE_CONFIG
#include "config.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "debug.h"

#include "tables.h"

/****************************************************************************
 * Forward declarations
 ****************************************************************************/
gint     db_table_descriptor_calc_size                (GList *gl);
gint     db_table_descriptor_calc_offset              (GList *gl);

int debug_level;

extern GList * table_desc;
GList * dbfieldconfigdesc;

static
G_sqldb_table dbfieldconfig_db_struct =
{
  0                          ,
  0                          ,
  "dbfieldconfig",
  0,
  sizeof (DB_dbfieldconfig),
  G_STRUCT_OFFSET (DB_dbfieldconfig, unused1),
  0
};
G_sqldb_table  *dbfieldconfig_sqldb = &dbfieldconfig_db_struct;
/**
 * db_init_sort_tabdes:
 * ****UNTESTED*****
 * sort by DB_dbfieldconfig->number
 * located in db_field_descriptor->offset
 **/
void
db_init_sort_tabdes(GList *ottd)
{
  GList               *ttd, *gl;
  db_table_descriptor *table;
  db_field_descriptor *f1, *f2;
  void                **p1, **p2, **p3;

  ttd = ottd;
  while(ttd)
    {
      table = ttd->data;
restart:
      gl = table->fields;
      f2 = 0;
      while(gl)
	{
	  f1 = gl->data;
	  p1 = &(gl->data);
	  if(f2 && f1)
	    {
	      if(f2->offset > f1->offset)
		{
		  p3 = *p1;
		  *p1 = *p2;
		  *p2 = p3;
		  goto restart;
		}
	    }
	  gl = gl->next;
	  p2 = p1;
	  f2 = f1;
	}
      ttd = ttd->next;
    }
}
/**
 * db_init_tabdes_add:
 *
 **/
GList *
db_init_tabdes_add(GList *ottd, DB_dbfieldconfig *dfc)
{
  db_table_descriptor *table;
  db_field_descriptor *field;
  GList               *gl, *ttd;

  ttd = ottd;
  while(ttd)
    {
      table = ttd->data;
      if(strcmp(dfc->tablen, table->name))
	{
	  ttd = ttd->next;
	  continue;
	}
      gl = table->fields;
      while(gl)
	{
	  field = gl->data;
	  if(!strcmp(field->name, dfc->fieldn))
	    return ottd;
	  gl = gl->next;
	}
/*OK here we have a table descriptor ready */
      field         = g_malloc(sizeof(db_field_descriptor));
      field->name   = g_strdup(dfc->fieldn);
      field->type   = dfc->fieldt;
      field->offset = dfc->number;
      table->fields = g_list_append(table->fields, field);

      return ottd;
    }
  
  table         = g_malloc(sizeof(db_table_descriptor));
  table->name   = g_strdup(dfc->tablen);
  table->fields = NULL;
  field         = g_malloc(sizeof(db_field_descriptor));
  field->name   = g_strdup(dfc->fieldn);
  field->type   = dfc->fieldt;
  field->offset = dfc->number;
  table->fields = g_list_append(table->fields, field);
  return g_list_append(ottd, table);
}

/**
 * db_set_initial_table_description:
 *
 * Setup bootstrap config to be able to load in table dbfieldconfig
 * dbfieldconfig contains table information for all used tables.
 *
 **/
db_table_descriptor * 
db_set_initial_table_description()
{
  DB_dbfieldconfig    * fcfg;
  db_table_descriptor * table;
  db_field_descriptor * field;
  GList               *tmp, *tmp2;
  
  table = g_malloc(sizeof(db_table_descriptor));

  table->name   = "dbfieldconfig";
  table->size   = 0;
  table->fields = NULL;

  field         = g_malloc(sizeof(db_field_descriptor));
  field->name   = "_rowid";
  field->type   = TD_GUINT;
  field->offset = 0;
  table->fields = g_list_append(table->fields, field);

  field         = g_malloc(sizeof(db_field_descriptor));
  field->name   = "tablen";
  field->type   = TD_GSTR;
  field->offset = 4;
  table->fields = g_list_append(table->fields, field);

  field         = g_malloc(sizeof(db_field_descriptor));
  field->name   = "fieldn";
  field->type   = TD_GSTR;
  field->offset = 8;
  table->fields = g_list_append(table->fields, field);

  field         = g_malloc(sizeof(db_field_descriptor));
  field->name   = "fieldt";
  field->type   = TD_GUINT;
  field->offset = 12;
  table->fields = g_list_append(table->fields, field);

  field         = g_malloc(sizeof(db_field_descriptor));
  field->name   = "number";
  field->type   = TD_GUINT;
  field->offset = 16;
  table->fields = g_list_append(table->fields, field);

  table_desc = NULL;
  table_desc = g_list_append(table_desc, table);

  if(db_table_descriptor_calc_size(table_desc))
    {
      return NULL;
    }

  if(db_table_descriptor_calc_offset(table_desc))
    {
      return NULL;
    }
  
#ifdef DEBUG
  fprintf(stderr,"\nPrinting initial config.\n");
  tmp = table_desc;
  while(tmp)
    {
      table = tmp->data;
      fprintf(stderr,"  Table: %s\n", table->name);
      tmp2 = table->fields;
      while(tmp2)
	{
	  field = tmp2->data;
	  fprintf(stderr,"    Field: %s\n", field->name);
	  tmp2 = tmp2->next;
	}
      tmp = tmp->next;
    }
  fprintf(stderr,"\nPrinting initial config, done.\n");
#endif
  return table;

}
/**
 * db_table_descriptor_calc_size:
 *
 **/
gint
db_table_descriptor_calc_size(GList *gl)
{
  db_table_descriptor * table;
  db_field_descriptor * field;
  GList               * g2;
  gint i;
  
  while(gl)
    { /* go through all tables */
      table = gl->data;
      g2 = table->fields;
      i = 0;
      while(g2)
	{
	  field = g2->data;
	  switch(field->type)
	    {
	    case TD_GUINT:
	    case TD_GINT:
	    case TD_PGUINT:
	    case TD_PGINT:
//	      i += sizeof(gint);
	      i += sizeof(gint);
	      break;
	    case TD_GSTR:
	    case TD_PGSTR:
	    case TD_PGL:
	      i += sizeof(gpointer);
	      break;
	    case TD_GDOUBLE:
	    case TD_PGDOUBLE:
	      i += sizeof(gdouble);
	      break;
	    default: 
	      fprintf(stderr,"ERROR IN TABLES.CONF: UNKNOWN FIELD TYPE\n");
	      return -1;
	      break;
	    }
	  g2 = g2->next;
	}
      i += sizeof(gpointer) * 5;	/* user struct private pointers,
                                           example last five members in DB_host */
      table->size = i;
#ifdef DEBUG
      fprintf(stderr,"Table: %s, has a calculated size of %u\n",
	      table->name, table->size);
#endif
      gl = gl->next;
  }
  return 0;
}
/**
 * db_table_descriptor_calc_offset:
 *
 **/
gint
db_table_descriptor_calc_offset(GList *gl)
{
  db_table_descriptor * table;
  db_field_descriptor * field;
  GList               * g2;
  gint i,o;
  
  while(gl)
    { /* go through all tables */
      table = gl->data;
      g2 = table->fields;
      i = o = 0;
      while(g2){
	field = g2->data;
	field->offset = o;
#ifdef DEBUG
	fprintf(stderr,"Entry: %s.%s, has offset %u\n",
		table->name, field->name, field->offset);
#endif
	switch(field->type)
	  {
	  case TD_GUINT:
	  case TD_GINT:
	  case TD_PGUINT:
	  case TD_PGINT:
	    o += sizeof(gint);
	    break;
	  case TD_GSTR:
	  case TD_PGSTR:
	  case TD_PGL:
	    o += sizeof(gpointer);
	    break;
	  case TD_GDOUBLE:
	  case TD_PGDOUBLE:
	    o += sizeof(gdouble);
	    break;
	  default: 
	    fprintf(stderr,"ERROR IN TABLES.CONF: UNKNOWN FIELD TYPE\n");
	    return -1;
	    break;
	  }
	g2 = g2->next;
      }
      gl = gl->next;
    }
  return 0;
}
/**
 * new_table:
 *
 * 
 **/
db_table_descriptor *
new_table()
{
  db_table_descriptor * tab;

  if(!(tab = g_malloc0(sizeof(db_table_descriptor))))
    {
      fprintf(stderr,"g_malloc0 failed\n");
      return 0;
    }
  return tab;
}
/**
 * new_field:
 *
 **/
db_field_descriptor *
new_field()
{
  db_field_descriptor * field;

  if(!(field = g_malloc0(sizeof(db_field_descriptor))))
    {
      fprintf(stderr,"g_malloc0 failed\n");
      return 0;
    }
  return field;
}
/**
 * db_init_client_side
 *
 **/
void
db_init_client_side(GIOChannel *channel)
{
  db_table_descriptor *tab;
  db_field_descriptor *fie;
  GList *tdgl, *gl, *gl2, *mgl;
  DB_dbfieldconfig *dfc;

  dbfieldconfig_sqldb->channel = channel;
  g_sqldb_table_load(dbfieldconfig_sqldb);
  mgl = g_sqldb_table_list(dbfieldconfig_sqldb);
  
  tdgl = NULL;
  gl = mgl;
  while(gl)
    {
      dfc = gl->data;
      tdgl = db_init_tabdes_add(tdgl, dfc);
      gl = gl->next;
    }
  
  db_init_sort_tabdes(tdgl);
  
  db_table_descriptor_calc_size(tdgl);
  db_table_descriptor_calc_offset(tdgl);
  
  gl = tdgl;
  while(gl)
    {
      tab = gl->data;
      gl2 = tab->fields;
      while(gl2)
	{
	  fie = gl2->data;
	  gl2 = gl2->next;
	}
      gl = gl->next;
    }
  
  table_desc = tdgl;
}

/* EOF */
