/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- An snmp management application
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

/*
**  graph.c contains utility functions related to the creation and destruction
**  of DB_graph items, and functions used to manage the DB_graph database.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <glib.h>

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

#include "debug.h"

/******************************************************************************
**
**  Function to create a new DB_graph object, and initialize it with 
**  default values, including the zero rowid which marks it as being
**  "not in the database"
**
******************************************************************************/
DB_graph *
graph_create (void)
{
  DB_graph * dbg;

  D_FUNC_START;
  dbg           = g_new0 (DB_graph, 1);
  if (dbg)
    {
      d_print (DEBUG_TRACE, "Memory allocated, initilizing structure");
      dbg->created  = g_strdup ("(not in database)");
      dbg->modified = g_strdup (dbg->created);
      d_print (DEBUG_TRACE, "Returning valid structure pointer");
      D_FUNC_END;
      return dbg;
    }
  d_print (DEBUG_TRACE, "Returning a NULL structure pointer");
  g_warning ("graph_create memory allocation failed, expect trouble.");
  D_FUNC_END;
  return NULL;
}

/******************************************************************************
**
**  Function to destroy a DB_graph object.  The rowid must be zero, which
**  marks it as being "not in the database"
**
******************************************************************************/

void
graph_destroy (DB_graph * dbg)
{
  D_FUNC_START;
  if (dbg)
    {
      g_assert (dbg->rowid == 0);
      d_print (DEBUG_TRACE, "Valid structure freeing data.");
      if (dbg->created)  g_free (dbg->created);
      if (dbg->modified) g_free (dbg->modified);
      if (dbg->details)  g_free (dbg->details);
      if (dbg->pixmap)   g_free (dbg->pixmap);
      d_print (DEBUG_TRACE, "Freeing structure.");
      g_free (dbg);
      dbg = NULL;
    }
  D_FUNC_END;
}

/******************************************************************************
 *
 *  Function to add a DB_graph object to the database.  The rowid must be
 *  zero, meaning that the item is not already in the database.
 *
 *****************************************************************************/

void
graph_add (DB_graph * dbg)
{

static gchar nulstr[] = "";

  D_FUNC_START;
  g_return_if_fail (dbg != NULL);
  g_assert (dbg->rowid == 0);
  g_assert (dbg->DB_map == NULL);
  g_assert (dbg->DB_host == NULL);
  g_assert (dbg->DB_network == NULL);
  
  switch (dbg->type)			/* do some application test */
    {
      case DB_GRAPH_HOST:
	g_assert (dbg->host != 0);
	g_assert (dbg->network == 0);
        break;
      case DB_GRAPH_NETWORK:
	g_assert (dbg->host == 0);
	g_assert (dbg->network != 0);
	break;
      case DB_GRAPH_WIRE:
	g_assert (dbg->host != 0);
	g_assert (dbg->network != 0);
	break;
      default:
	g_assert_not_reached();
    }

/* ok, for application, this row is OK, now we must check so
   the row is ok for functionality, ie pointers can't be zero */
  if(!dbg->details) dbg->details = &nulstr;
  if(!dbg->pixmap) dbg->pixmap = &nulstr;

  g_assert (dbg->map != 0);		/* Must be assigned to a map */

  g_db_add_reference(map_sqldb, dbg, &dbg->map, "graph", "DB_map", "map", "DB_graphs");
  g_db_add_reference(host_sqldb, dbg, &dbg->host, "graph", "DB_host", "host", "DB_graphs");
  g_db_add_reference(network_sqldb, dbg, &dbg->network, 
            "graph", "DB_network", "network", "DB_graphs");

  g_db_timestamp(dbg, "graph", "created");
  if (dbg->modified) g_free (dbg->modified);
  dbg->modified = g_strdup (dbg->created);

  dbg->rowid = g_sqldb_highest_rowid (graph_sqldb, "_rowid") + 1;
  g_sqldb_row_add (graph_sqldb, dbg);
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to update a DB_graph object in the database.  The rowid must be
**  non-zero, meaning that the item is already in the database.
**
******************************************************************************/

void
graph_update (DB_graph * dbg)
{
  D_FUNC_START;
  g_return_if_fail (dbg != NULL);
  g_assert (dbg->rowid != 0);
  switch (dbg->type)
    {
      case DB_GRAPH_HOST:
        g_assert (dbg->host != 0);
        g_assert (dbg->network == 0);
        break;
      case DB_GRAPH_NETWORK:
        g_assert (dbg->host == 0);
        g_assert (dbg->network != 0);
        break;
      case DB_GRAPH_WIRE:
        g_assert (dbg->host != 0);
        g_assert (dbg->network != 0);
        break;
      default:
        g_assert_not_reached();
    }
  g_assert (dbg->map != 0);             /* Must be assigned to a map */

  if (dbg->DB_map)                              /* If previously assigned */
    if (dbg->DB_map->rowid != dbg->map)         /* to a different map ... */
      {                                         /* then unassign it */
        g_db_del_reference(dbg, "graph", "DB_map", "map", "DB_graphs");
      }

  if (!dbg->DB_map)            /* Assign to a new map if necessary */
    {
      g_db_add_reference(map_sqldb, dbg, &dbg->map, "graph", "DB_map",
            "map", "DB_graphs");
    }

  if (dbg->DB_host)                             /* If previously attached to */
    if (dbg->DB_host->rowid != dbg->host)       /* a different host object */
      {                                         /* then unassign it */
        g_db_del_reference(dbg, "graph", "DB_host", "host", "DB_graphs");
      }

  if (dbg->host)                /* Assign graph object to a host? */
    if (!dbg->DB_host)          /* Yes -- add to host GList if necessary */
      {
        g_db_add_reference(host_sqldb, dbg, &dbg->host, "graph", "DB_host",
              "host", "DB_graphs");
      }


  if (dbg->DB_network)                          /* If previously attached to */
    if (dbg->DB_network->rowid != dbg->network) /* a different network object */
      {                                         /* then unassign it */
        g_db_del_reference(dbg, "graph", "DB_network", "network", "DB_graphs");
      }

  if (dbg->network)             /* Assign graph object to a network? */
    if (!dbg->DB_network)       /* Yes -- add to network GList if necessary */
      {
        g_db_add_reference(network_sqldb, dbg, &dbg->network, "graph", "DB_network",
              "network", "DB_graphs");
      }

  g_db_timestamp(dbg, "graph", "modified");

  g_sqldb_row_update (graph_sqldb, dbg);
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to delete a DB_graph object from the database.  The rowid must
**  be non-zero, meaning that the item is already in the database.
**
**  When we finished, we set the rowid to zero, meaning that the item is
**  no longer in the database, and return.  The caller is responsible for
**  making a call to host_destroy to free the memory associated with the
**  DB_graph block.
**
******************************************************************************/

void
graph_delete (DB_graph * dbg)
{
  D_FUNC_START;
  g_assert (dbg->rowid != 0);
  g_return_if_fail (dbg != NULL);
  if (dbg->DB_map)
      g_db_del_reference(dbg, "graph", "DB_map", "map", "DB_graphs");
  dbg->DB_map = NULL;
  if (dbg->DB_host)
      g_db_del_reference(dbg, "graph", "DB_host", "host", "DB_graphs");
  dbg->DB_host = NULL;

  if (dbg->DB_network)
      g_db_del_reference(dbg, "graph", "DB_network", "network", "DB_graphs");
  dbg->DB_network = NULL;

  g_assert (g_sqldb_row_delete (graph_sqldb, dbg) == TRUE);

  dbg->rowid = 0;
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to locate a graph entry by the database rowid
**
******************************************************************************/

DB_graph *
graph_find_by_rowid (guint rowid)
{
  d_print (DEBUG_TRACE, "Find rowid %d\n", rowid);
  return g_sqldb_row_find (graph_sqldb, "_rowid", &rowid);
}

/******************************************************************************
**
**  Function to locate a host graph entry
**
******************************************************************************/

DB_graph *
graph_find_host (guint map_rowid, guint host_rowid)
{
  GList	* gl;
  DB_graph * dbg;
  D_FUNC_START;
  gl = g_sqldb_table_list (graph_sqldb);
  while (gl)
    {
      dbg = (DB_graph *)gl->data;
      if ((dbg->type  == DB_GRAPH_HOST) &&
	  (dbg->map   == map_rowid) &&
	  (dbg->host  == host_rowid))
	{
	  d_print (DEBUG_TRACE, "Host found, returning valid pointer.\n");
	  D_FUNC_END;
	  return dbg;
	}
      gl = gl->next;
    }
  d_print (DEBUG_TRACE, "Host not found, returning NULL pointer.\n");
  D_FUNC_END;
  return NULL;
}


/******************************************************************************
**
**  Function to locate a network graph entry
**
******************************************************************************/

DB_graph *
graph_find_network (guint map_rowid, guint network_rowid)
{
  GList    * gl;
  DB_graph * dbg;
  D_FUNC_START;
  gl = g_sqldb_table_list (graph_sqldb);
  while (gl)
    {
      dbg = (DB_graph *)gl->data;
      if ((dbg->type    == DB_GRAPH_NETWORK) &&
	  (dbg->map     == map_rowid) &&
	  (dbg->network == network_rowid))
	{
	  D_FUNC_END;
	  return dbg;
	}
      gl = gl->next;
    }
  D_FUNC_END;
  return NULL;
}

/******************************************************************************
**
**  Function to locate a wire graph entry
**
******************************************************************************/

DB_graph *
graph_find_wire (guint map_rowid, guint host_rowid, guint network_rowid)
{
  GList    * gl;
  DB_graph * dbg;
  D_FUNC_START;
  gl = g_sqldb_table_list (graph_sqldb);
  while (gl)
    {
      dbg = (DB_graph *)gl->data;
      if ((dbg->type == DB_GRAPH_WIRE) &&
	  (dbg->map  == map_rowid) &&
	  (dbg->host == host_rowid) &&
	  (dbg->network == network_rowid))
	{
	  D_FUNC_END;
	  return dbg;
	}
      gl = gl->next;
    }
  D_FUNC_END;
  return NULL;
}

/* EOF */

