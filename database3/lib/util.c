/*  $Id$
 *
 * Copyright 2000 Larry Liimatainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * util.c -- currently this is a shelter
 *
 */

/*
 * Common utility functions.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <glib.h>
#include <sys/time.h>

#include "debug.h"

/**
 * wordcpyt:
 * 
 * Copy a space seperated word 'w' to destination 'dst' from 'src'
 *
 **/
gchar *
wordcpyt (gchar *dst, gchar *src, gchar *tmp, gint w)
{
static int i, j;
                
  i = j = 0;
  *dst = 0;
  for(w--;w;)           /*count forward w words */
    {
      if ( *(src + i) == 0 || *(src + i) == '\n'){ /* not enough words */
        *dst = 0;
        return 0; /* Leave destination empty */
      }
      if ( *(src + i) == *tmp) w--;
      i++;
    }

  while(  ( *(src + i) != 0 && (*(src + i) != '\n')) 
       && ( *(src + i) != *tmp) )
    {
      *(dst + j) = *(src + i);    /* move each character */
      i++;
      j++;
    }
  *(dst+j) = 0;
  return dst;
}

/**
 * db_timestamp:
 *
 *  Construct a standard database timestamp.
 *
 *  This subroutine stores a timestamp in a common format suitable for
 *  inclusion in database records.
 *
 *  The format is yyyy.mm.dd hh:mm:ss\0, for a total of 20 bytes, including
 *  the NULL.
 *
 *  Return values:
 *
 *  The return value is a pointer to the buffer.
 **/

gchar *
db_timestamp ()
{
  gchar         * buf;
  struct tm     * tim;
  time_t          gm_time;
  D_FUNC_START;
  buf     = g_malloc (20);
  gm_time = time (NULL);
  tim     = localtime (&gm_time);
  strftime (buf, 20, "%Y.%m.%d %H:%M:%S", tim);
  D_FUNC_END;
  return buf;
}

/* EOF */
