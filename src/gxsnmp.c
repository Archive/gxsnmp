/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <smi.h>

#include "gxsnmp_app.h"
#include "gxsnmp_config.h"
#include "gxsnmp_menus.h"
#include "gxsnmp_mib_browser.h"

#define __IN_MAIN_C__
#include "debug.h"
/****************************************************************************
 * Global data (ick!)
 **/
GxSNMPConfig      *current_config = NULL;

/***************************************************************************
 * Local functions.
 **/
static void       session_die             (GnomeClient        *client,
					   gpointer           data);
static gint       save_session            (GnomeClient        *client,
					   gint               phase,
					   GnomeSaveStyle     save_style,
					   gint               is_shutdown,
					   GnomeInteractStyle interact_style,
					   gint               is_fast,
					   gpointer           client_data);
void 		  debug_file_cb		  (poptContext 	      con,
					   enum poptCallbackReason reason,
					   const struct poptOption * opt,
					   const char         *arg, 
					   const void         *data);
/**
 * popt table
 **/
int             debug_level  = 0;
gint            aa_canvas    = 0; /* Default off as the aa_canvas is broke */
gint            test_browser = 0;
GHashTable      *debug_files = NULL;
static gchar    *geometry    = NULL;
static gchar    *dummy       = NULL;
poptContext     pctx;
struct poptOption options[] = {
  { NULL, '\0', POPT_ARG_CALLBACK, debug_file_cb, "NOTNULL", NULL, NULL },
  { "geometry", '\0', POPT_ARG_STRING, &geometry, 0,
    N_("Specifiy the geometry of the main window"),
    N_("GEOMETRY") },
  { "debug-level", '\0', POPT_ARG_INT, &debug_level, 0,
    N_("Specify the level of debugging messages that will be output."),
    N_("DEBUG_LEVEL") },
  { "debug-filename", '\0', POPT_ARG_STRING, &dummy, 0,
    N_("Specify the filename for which debugging messages will be output (can "
       "be specified more than once)."),
    N_("DEBUG_FILENAME") },
  { "aa-canvas", '\0', POPT_ARG_INT, &aa_canvas, 0,
    N_("Enable the antialiased canvas."),
    N_("AA_CANVAS") },
  { "test-browser", '\0', POPT_ARG_INT, &test_browser, 0,
    N_("Test the new browser widget."),
    N_("TEST_BROWSER") },
  { NULL, '\0', 0, NULL, 0 }
};

void
debug_file_cb (poptContext con, enum poptCallbackReason reason,
	       const struct poptOption *opt, const char *arg, 
	       const void *data)
{
  if (!strcmp(opt->longName, "debug-filename"))
    {
      if (!debug_files) 
	debug_files = g_hash_table_new(g_str_hash, g_str_equal);
      g_hash_table_insert(debug_files, arg, data);
    }
}

/**
 * Program entry point
 **/
gint
main (int argc, gchar *argv[])
{
  GSList      *slist;
  GxSNMPApp   *app;
  GtkWidget   *map;
  GtkWidget   *browser;
  GnomeClient *client;
  
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);
  
  gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv, options, 0, &pctx);
  poptFreeContext (pctx);
  /** Initialize the SMI library abort on failure */
  if (smiInit (""))
    {
      g_error ("SMI Lib failed to init!\n");
    }
  /** Get our configuration */
  current_config = gxsnmp_config_init ();
  
  /** Connect to session managment **/
  client = gnome_master_client ();
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself", 
		      GTK_SIGNAL_FUNC (save_session), argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
		      GTK_SIGNAL_FUNC (session_die), NULL);

  /** Finish setting up the SMI library. */
  if (current_config->mib_path)
    smiSetPath (current_config->mib_path);
  slist = current_config->mibs;
  while (slist)
    {
      d_print (DEBUG_DUMP, "smiLoadModule %s\n", (gchar *)slist->data);
      smiLoadModule ((gchar *)slist->data);
      slist = slist->next;
    }
  app = gxsnmp_app_new (geometry);
  map = gxsnmp_map_new ();
  gxsnmp_map_set_geometry ((GxSNMPMap *)map, 6000, 6000);
  gxsnmp_app_add_map (app, (GxSNMPMap *)map, "Default");
  gtk_widget_show_all (GTK_WIDGET (app->app));
#if 0
  if (test_browser)
    { 
      browser = gxsnmp_mib_browser_new();
      gtk_widget_show_all (GTK_WIDGET (browser));
    }
#endif
  gtk_main ();
  gxsnmp_config_destroy (current_config);
  return 0;
}

/** session mangement **/
/** 
 * save_session:
 **/
static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar **argv;
  guint argc;

  argv = g_malloc0(sizeof (gchar *)*4);
  argc = 1;
  
  argv[0] = client_data;
  /* add any addtional state info here. */
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);
  return TRUE;
}
/**
 * session_die
 **/
static void
session_die (GnomeClient *client, gpointer client_data)
{
  gtk_main_quit ();
}

/* EOF */

