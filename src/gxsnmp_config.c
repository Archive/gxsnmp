/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Config handling
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <smi.h>

#include "gxsnmp_config.h"
#include "gxsnmp_util.h"

#include "debug.h"

/****************************************************************************
 * Local functions.
 **/
static GxSNMPConfig        *gxsnmp_default_config             (void);
/****************************************************************************
 * Static info.
 **/
static gchar *default_mibs[] = {
  "RMON-MIB",
  "IP-MIB",
  "TUBS-IBR-LINUX-MIB"
};
/****************************************************************************
 * Module implementation.
 **/
/**
 * gxsnmp_default_config
 *
 * Create and initilize a config block when no configuration can be found
 * or loaded. This should set all configurable options to sane values.
 *
 * Return value: A properly initialized config block. The callee will be
 *               responsible for managing the memory resources.
 **/
static GxSNMPConfig *
gxsnmp_default_config ()
{
  GxSNMPConfig    *ret_val;
  gchar           *path;
  gint            i, j;
  D_FUNC_START;
  ret_val = g_malloc0 (sizeof (GxSNMPConfig));
  /* if the SMI lib has a path, save that as the default. */
  /* assignment intended */
  if ( (path = smiGetPath()) )
    ret_val->mib_path = path;
  j = ELEMENTS (default_mibs);
  for ( i = 0; i < j; i++ )
    {
      d_print (DEBUG_DUMP, "Adding %s mib to the default list \n", 
	       default_mibs[i]);
      ret_val->mibs = g_slist_append (ret_val->mibs, default_mibs[i]);
    }
  return ret_val;
  D_FUNC_END;
}

/****************************************************************************
 * Public functions
 **/
/**
 * gxsnmp_config_init:
 *
 * Initialize and load a config block. If no config file(s) can be found
 * return the default config. The callee will be responsible for the resource 
 * management. The only time this call should fail is on memory allocation
 * failure. Which will cause an abort.
 *
 * Return value: A properly initialized config block.
 **/
GxSNMPConfig  *
gxsnmp_config_init()
{
  GxSNMPConfig  *ret_val;
  D_FUNC_START;
  /* Start with the default config */
  ret_val = gxsnmp_default_config ();

  /* Now load the config bits if possible */


  /* Return what we found. */
  D_FUNC_END;
  return ret_val;
}
/**
 * gxsnmp_config_save:
 * @config
 *
 * Write out the config so that it may be loaded for another session. This 
 * will just write the config. Call gxsnmp_config_destroy when finished with
 * the config block.
 *
 * Return value: TRUE == Config saved.
 *               FALSE == Config not saved.
 **/
gboolean 
gxsnmp_config_save (GxSNMPConfig *config)
{
  D_FUNC_START;
  /* FIXME: Write it */
  D_FUNC_END;
  return FALSE;
}
/**
 * gxsnmp_config_destroy:
 * 
 * Free up the resources used by a config block. The GxSNMPConfig pointer 
 * shall be invalid after this call.
 *
 * Return value: Nothing.
 **/
void
gxsnmp_config_destroy (GxSNMPConfig *config)
{
  D_FUNC_START;
  g_return_if_fail (config != NULL);

  if (config->mib_path)
    g_free (config->mib_path);
  g_slist_free (config->mibs);
  g_free (config);
  config = NULL;
  D_FUNC_END;

};

/* EOF */
