/* -*- Mode: C; tab-width: 4 -*-
 *  $Id$
 *
 *  Copyright 1999, Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 * 
 * debug.h -- misc debug type macros.. If debugging is disabled in any module
 *            that includes this file the macros should expand out to empty
 *            warning free code.
 */
#ifndef __DEBUG_H__
#define __DEBUG_H__
#include <stdio.h>

#ifndef __IN_MAIN_C__
extern int debug_level;
extern GHashTable *debug_files;
#endif
enum {
  DEBUG_TRACE      = 1 << 0,
  DEBUG_DUMP       = 1 << 1,
  DEBUG_DATABASE   = 1 << 2,
  DEBUG_PLUGINS    = 1 << 3,
  DEBUG_QUERIES    = 1 << 4,
  DEBUG_DISCOVERY  = 1 << 5,
  DEBUG_OBJECTS    = 1 << 6
};

#define d_print(level, format, a...) \
        G_STMT_START { \
        if ((debug_level & level) && \
           ((!debug_files) || g_hash_table_lookup(debug_files, __FILE__))) { \
        printf (__FILE__" (%d) : " __PRETTY_FUNCTION__ "() : ", __LINE__); \
        printf (format, ## a); }                                     \
        } G_STMT_END

#define D_FUNC_START  d_print (DEBUG_TRACE, " (start)\n")
#define D_FUNC_END    d_print (DEBUG_TRACE, " (end)\n")

#endif
/* EOF */

