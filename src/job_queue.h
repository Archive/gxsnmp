/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- An snmp managament application
 *  Copyright (C) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  Queue handling definitions/declarations.
 */
#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <glib.h>

/*
 * DOC: gxsnmp job queue routines.
 *
 *  The job queue is a list of callbacks that are to be invoked at 
 *  some specific time in the future.  The event queue is a GList kept
 *  in chronological order, so that the top entry on the event queue
 *  is always the next event to be triggered.
 *
 *  Examples of events that may be added to the event queue are 
 *  polling collectors, checking hosts, discovering the network,
 *  timeouts, etc.
 */

typedef void        (*GXQueueFunc)	         (guint         id, 
						  gpointer 	data);
guint		    add_queue	                 (guint32     	interval,
						  GXQueueFunc 	function,
						  gpointer    	data);
gboolean	    remove_queue 	         (guint         id);
gboolean	    run_queue	                 (gpointer	data);

#endif

/* EOF */


