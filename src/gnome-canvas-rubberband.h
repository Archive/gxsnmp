/* Rubberband object for GnomeCanvas widget
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GNOME_CANVAS_RUBBERBAND_H
#define GNOME_CANVAS_RUBBERBAND_H

#include <libgnome/gnome-defs.h>
/*#include "gnome-canvas.h"*/
#include <libgnomeui/gnome-canvas.h>


BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_RUBBERBAND            (gnome_canvas_rubberband_get_type ())
#define GNOME_CANVAS_RUBBERBAND(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_RUBBERBAND, GnomeCanvasRubberband))
#define GNOME_CANVAS_RUBBERBAND_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_RUBBERBAND, GnomeCanvasRubberbandClass))
#define GNOME_IS_CANVAS_RUBBERBAND(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_RUBBERBAND))
#define GNOME_IS_CANVAS_RUBBERBAND_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_RUBBERBAND))

typedef void (*GnomeCanvasSnapFunc) (double *x, double *y, gpointer data);

typedef struct _GnomeCanvasRubberband GnomeCanvasRubberband;
typedef struct _GnomeCanvasRubberbandClass GnomeCanvasRubberbandClass;

/* Note that this is NOT a canvas item */

struct _GnomeCanvasRubberband {
  GtkObject object;
  
  GnomeCanvasItem* item;

  GnomeCanvasItem* rect;

  /* These are item coordinates for internal use.*/
  double x_origin, y_origin;

  GnomeCanvasSnapFunc       snap_func;
  gpointer                  snap_func_data;

  guint32 last_time;
};

struct _GnomeCanvasRubberbandClass {
  GtkObjectClass parent_class;

  /* Coordinates are all item coordinates. */
  void (* moved)    (GnomeCanvasRubberband* rb, double x1, double y1, double x2, double y2);
  void (* snapped)  (GnomeCanvasRubberband* rb, double x1, double y1, double x2, double y2);
  void (* cancelled)(GnomeCanvasRubberband* rb); 
};

/* Standard Gtk function */
GtkType gnome_canvas_rubberband_get_type (void);

/* The rubberband can only be used in a particular way. You start it on 
   the canvas, get it back, connect to snapped/cancelled; once it's 
   snapped or cancelled, you have to destroy it. There can be only
   one rubberband per canvas at a time, and you can use a rubberband
   only once. You will get either snapped or cancelled, never both. */

/* x and y are world coordinates, since you'll usually start
   with a button event. */

/* Rubberband coordinates are relative to the item you pass in, though the 
   rubberbanding is not confined to that item, cancelled/snapped will return
   coordinates relative to it. */

GnomeCanvasRubberband* 
gnome_canvas_rubberband_new(GnomeCanvasItem*    item,
			    gdouble             x,
			    gdouble             y,
			    guint32             event_time,
			    GnomeCanvasSnapFunc snap_func,
			    gpointer            snap_func_data);

END_GNOME_DECLS

#endif

