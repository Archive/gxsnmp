/*
 *  $Id$
 *  Copyright 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  plugin.h -- Plugin handling code, shamelessly copied from gnumeric :)
 */
#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#ifdef HAVE_GNOME
#include <gnome.h>
#endif
#include <gtk/gtk.h>
#include <gmodule.h>

/*
**  The different types of plugins.
**
**  All plugins are loaded at the beginning of the main() file.
**
**  Database plugins are started at an early stage of initialization,
**  because they are needed to load the database.
**
**  Application and collector plugins are started after the entire 
**  database has been read into memory and initialized.
*/

enum {
  PLUGIN_DATABASE    = 0x00000001, /* Database plugin */
  PLUGIN_COLLECTOR   = 0x00000002, /* Collector plugin (No GTK used) */
  PLUGIN_APPLICATION = 0x00000004  /* Application plugin (GTK used) */
};

/*
**  Control block for keeping track of the various plugins
*/

typedef struct _PluginData
{
  GModule   *handle;		/* Identifies the loaded module */
  gchar     *filename;		/* Filename of plugin module */
  gchar     *name;		/* Plugin-assigned name of plugin */
  gint       type;              /* Plugin-assigned type of plugin */
  /* The following pointers are filled on initial plugin load */
  int     (* load_plugin)	(struct _PluginData *);
  void    (* unload_plugin)  	(struct _PluginData *);
  void    (* start_plugin) 	(struct _PluginData *);
  void    (* configure_plugin)  (struct _PluginData *);
  gchar     *menu_path;         /* The menu path we want to be inserted on */
  gpointer  menu_data;          /* A pointer user_data for the callback */
  void    (* menu_callback)     (GtkWidget          *application,
				 gpointer           user_data);
  gpointer   menu_item;         /* A pointer to the menu item that was added */
  gpointer   app;               /* A pointer back to the app window */
  gint       refcount;		/* Reference count for plugin */
  gpointer * private;		/* For private use by the plugin */
}
PluginData;

extern GList * plugin_list;

/******************************************************************************
**
**  The API for the plugin interface
**
******************************************************************************/

void		plugins_load	(void);
void            plugins_unload	(void);
void		plugins_start   (gint		  type,
				 GtkWidget        *widget);

/*
** These are the minimal functions that need to be defined in the plugin.
*/
/*
int		init_plugin	(PluginData  	* pd);
void		cleanup_plugin	(PluginData  	* pd);
*/
#endif

/* EOF */


