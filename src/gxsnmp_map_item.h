/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifndef __GXSNMP_MAP_ITEM_H__
#define __GXSNMP_MAP_ITEM_H__
#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>
BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros
 **/
#define GXSNMP_TYPE_MAP_ITEM              (gxsnmp_map_item_get_type ())
#define GXSNMP_MAP_ITEM(obj)              (GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MAP_ITEM, GxSNMPMapItem))
#define GXSNMP_MAP_ITEM_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_MAP_ITEM, GxSNMPMapItemClass))
#define GXSNMP_IS_MAP_ITEM(obj)           (GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MAP_ITEM))
#define GXSNMP_IS_MAP_ITEM_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MAP_ITEM))
/****************************************************************************
 * Control Blocks
 **/
typedef struct _GxSNMPMapItem             GxSNMPMapItem;
typedef struct _GxSNMPMapItemClass        GxSNMPMapItemClass;
struct _GxSNMPMapItem
{
  GnomeCanvasItem          item;
  
  gpointer                 app;          /* Application specific pointer */
};
struct _GxSNMPMapItemClass
{
  GnomeCanvasItemClass     class;
  
  void     (* update)      (GxSNMPMapItem    *item, 
			    double           *affine, 
			    ArtSVP           *clip_path,
			    int              flags);
  void     (* realize)     (GxSNMPMapItem    *item);
  void     (* unrealize)   (GxSNMPMapItem    *item);
  void     (* draw)        (GnomeCanvasItem  *item,
			    GdkDrawable      *drawable,
			    int              x,
			    int              y,
			    int              width,
			    int              height);
  void     (* render)      (GnomeCanvasItem  *item,
			    GnomeCanvasBuf   *buf);
  /* methods */
  gboolean (* select)      (GxSNMPMapItem    *item);
  gboolean (* unselect)    (GxSNMPMapItem    *item);

  void     (* edit_item)   (GxSNMPMapItem    *item);
  void     (* popup_menu)  (GxSNMPMapItem    *item,
			    GdkEventButton   *event,
			    gpointer         data);
  gint     (* changed)     (GxSNMPMapItem    *item,
			    gpointer         data);
};
/****************************************************************************
 * Public functions
 **/
GtkType        gxsnmp_map_item_get_type         (void);

END_GNOME_DECLS
#endif
/* EOF */

