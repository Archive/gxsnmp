/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gxsnmp_app.h"
#include "gxsnmp_menus.h"

#include "debug.h"
/****************************************************************************
 * Forward declarations
 **/
static void       not_implemented_cb               (GtkWidget       *widget,
						    gpointer        data);
static void       popup_not_implemented_cb         (GtkWidget       *widget,
						    gpointer        data);
static void       new_app_cb                       (GtkWidget       *widget,
						    gpointer        data);
static void       new_map_cb                       (GtkWidget       *widget,
						    gpointer        data);
static void       close_cb                         (GtkWidget       *widget,
						    gpointer        data);
static void       exit_cb                          (GtkWidget       *widget,
						    gpointer        data);
static void       about_cb                         (GtkWidget       *widget,
						    gpointer        data);
static void       map_zoom_in_cb                   (GtkWidget       *widget,
						    gpointer        data);
static void       map_zoom_out_cb                  (GtkWidget       *widget,
						    gpointer        data);
/****************************************************************************
 * Handles for the popup menus.
 **/
GtkWidget                   *root_menu_popup  = NULL;
/** Menus */
static GnomeUIInfo new_menu [] = {
  GNOMEUIINFO_MENU_NEW_ITEM (N_("_New Window"),
			     N_("Create a new map window"),
			     new_app_cb, NULL),
  GNOMEUIINFO_MENU_NEW_ITEM (N_("New _Map"),
			     N_("Create a new map on the current window"),
			     new_map_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo file_menu [] = {
  GNOMEUIINFO_MENU_NEW_SUBTREE (new_menu),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_PRINT_SETUP_ITEM (not_implemented_cb, NULL),
  GNOMEUIINFO_MENU_PRINT_ITEM (not_implemented_cb, NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_CLOSE_ITEM (close_cb, NULL),
  GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),
  GNOMEUIINFO_END
};
static GnomeUIInfo settings_menu [] = {
  GNOMEUIINFO_ITEM_STOCK (N_("General Settings"),
    N_("This will set the overall application settings."),
    not_implemented_cb, GNOME_STOCK_MENU_PREF),
  GNOMEUIINFO_ITEM_STOCK (N_("Plugin Settings"),
    N_("This will set the plugins that are loaded and the paths that will "
       "searched when loading plugins."),
    not_implemented_cb, GNOME_STOCK_MENU_PREF),
  GNOMEUIINFO_END
};
static GnomeUIInfo tools_menu [] = {
  { GNOME_APP_UI_ITEM, N_("Host Tool"),
    N_("Create a new host on the map."),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME, 
    "gxsnmp/host_tool.png", 0, (GdkModifierType) 0, NULL },
  { GNOME_APP_UI_ITEM, N_("Network Tool"),
    N_("Create a new network on the map."),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME, 
    "gxsnmp/network_tool.png", 0, (GdkModifierType) 0, NULL },
  { GNOME_APP_UI_ITEM, N_("Group Tool"),
    N_("Create a new group of the selected objects."),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME, 
    "gxsnmp/group_tool.png", 0, (GdkModifierType) 0, NULL },
  { GNOME_APP_UI_ITEM, N_("Wire Tool"),
    N_("Link two or more objects on the map."),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME, 
    "gxsnmp/link-nodes.png", 0, (GdkModifierType) 0, NULL },
  GNOMEUIINFO_END
};
static GnomeUIInfo help_menu [] = {
  GNOMEUIINFO_HELP ("gxsnmp"),
  GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo app_menu [] = {
  GNOMEUIINFO_MENU_FILE_TREE (file_menu),
  GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu),
  GNOMEUIINFO_SUBTREE (N_("Tools"), tools_menu),
  GNOMEUIINFO_MENU_HELP_TREE (help_menu),
  GNOMEUIINFO_END
};

/** Toolbars */
static GnomeUIInfo toolbar[] = {
  { GNOME_APP_UI_TOGGLEITEM, NULL, N_("Host Tool"),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "gxsnmp/host_tool.png", 0, (GdkModifierType) 0, NULL },
  { GNOME_APP_UI_TOGGLEITEM, NULL, N_("Network Tool"),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "gxsnmp/network_tool.png", 0, (GdkModifierType) 0, NULL },
  { GNOME_APP_UI_ITEM, NULL, N_("Group Tool"),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "gxsnmp/group_tool.png", 0, (GdkModifierType) 0, NULL },
  { GNOME_APP_UI_TOGGLEITEM, NULL, N_("Wire Tool"),
    not_implemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "gxsnmp/link-nodes.png", 0, (GdkModifierType) 0, NULL },
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(
			 NULL, N_("Prints the current map."),
			 not_implemented_cb, GNOME_STOCK_PIXMAP_PRINT),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(
			 NULL, N_("Exit from GxSNMP."),
			 exit_cb, GNOME_STOCK_PIXMAP_EXIT),
  GNOMEUIINFO_END
};
/****************************************************************************
 * The general map popup.
 **/
static GnomeUIInfo map_popup[] = {
  GNOMEUIINFO_ITEM_STOCK (N_("Zoom in"), NULL,
			  map_zoom_in_cb, GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK (N_("Zoom out"), NULL,
			  map_zoom_out_cb, GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK (N_("Toggle Grid"), NULL, 
			  popup_not_implemented_cb, GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK (N_("Snap to Grid"), NULL, 
			  popup_not_implemented_cb, GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_END
};

void 
gxsnmp_install_menus_and_toolbar (GxSNMPApp *app)
{
  D_FUNC_START;
  gnome_app_create_toolbar_with_data (GNOME_APP (app->app), toolbar, app);
  gnome_app_create_menus_with_data (GNOME_APP (app->app), app_menu, app);
  gnome_app_install_menu_hints (GNOME_APP (app->app), app_menu);
  root_menu_popup = gnome_popup_menu_new (map_popup);
  D_FUNC_END;
}

/**
 * Callback functions
 **/
static void
not_implemented_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget    *dialog;
  GxSNMPApp    *app = (GxSNMPApp *)data;

  dialog = gnome_ok_dialog_parented
    ( _("This functionality has not been implemented as of yet.\n"
	"patches welcome!"),
      GTK_WINDOW (app->app));
}
static void
popup_not_implemented_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget    *dialog;
  GxSNMPApp    *app = (GxSNMPApp *)data;

  dialog = gnome_ok_dialog
    ( _("This functionality has not been implemented as of yet.\n"
	"patches welcome!"));
}
static void
new_app_cb (GtkWidget *widget, gpointer data)
{
  GxSNMPApp   *app;
  app = gxsnmp_app_new (NULL);
  gtk_widget_show_all (GTK_WIDGET (app->app));
}

static void
new_map_cb (GtkWidget *widget, gpointer data)
{
}

static void
close_cb (GtkWidget *widget, gpointer data)
{
  GxSNMPApp   *app;
  
  app = (GxSNMPApp *)data;
  gxsnmp_app_close (app);
}

static void
exit_cb (GtkWidget *widget, gpointer data)
{
  GxSNMPApp   *app;
  gint x, y, w, h;
  app = (GxSNMPApp *)data;
  gdk_window_get_position (app->app->window, &x, &y);
  gdk_window_get_size (app->app->window, &w, &h);
  
  gnome_config_set_int ("/GXSNMP/Geometry/width", w);
  gnome_config_set_int ("/GXSNMP/Geometry/height", h);
  gnome_config_set_int ("/GXSNMP/Geometry/xpos", x);
  gnome_config_set_int ("/GXSNMP/Geometry/ypos", y);
  gnome_config_sync ();

  gtk_main_quit ();
}
static void 
map_zoom_in_cb (GtkWidget *widget, gpointer data)
{
  gxsnmp_map_zoom_in ((GxSNMPMap *)data);
}
static void
map_zoom_out_cb (GtkWidget *widget, gpointer data)
{
  gxsnmp_map_zoom_out ((GxSNMPMap *)data);
}
static void
about_cb (GtkWidget *widget, gpointer data)
{
  static GtkWidget    *dialog = NULL;
  GxSNMPApp           *app;
  D_FUNC_START;
  app = (GxSNMPApp *) data;
  if (dialog != NULL)
    {
      g_assert (GTK_WIDGET_REALIZED (dialog));
      gdk_window_show (dialog->window);
      gdk_window_raise (dialog->window);
    }
  else
    {
      const gchar *authors[] = {
	"Gregory McLean <gregm@gnu.org>",
	"Jochen Friedrich <jochen@gxsnmp.org>",
	"John Schulien <jms@gxsnmp.org>",
	"Larry Liimatainen <remlali@remsa.ericsson.se>",
	"Craig Small <csmall@gxsnmp.org>",
	"And Many Others...",
	NULL
      };
      gchar *logo = gnome_unconditional_pixmap_file ("gxsnmp/logo_h.xpm");
      dialog = gnome_about_new (_("GxSNMP"), VERSION, 
				"(C) 1998, 1999 Gregory McLean & Jochen Friedrich",
				(const gchar **)authors,
				_("GxSNMP is your basic SNMP management "
				  "application. The goal is to allow you to "
				  "better manage your networks."),
				logo);
      g_free (logo);
      gtk_signal_connect (GTK_OBJECT (dialog), "destroy", 
			  (GtkSignalFunc) gtk_widget_destroyed,
			  &dialog);
      if (app->app)
	gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (app->app));
      gtk_widget_show (dialog);
    }
  D_FUNC_END;
}
/****************************************************************************
 * Public Functions.
 **/
gint 
gxsnmp_menu_raise_map_popup (GdkEventButton *event, gpointer data)
{
  D_FUNC_START;
  gnome_popup_menu_do_popup (root_menu_popup, NULL, NULL, event, data);
  D_FUNC_END;
  return TRUE;
}

/* EOF */
