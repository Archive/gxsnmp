/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gxsnmp_app.h"
#include "gxsnmp_menus.h"

#include "debug.h"

/** Keep a list of open application windows **/

static GSList *app_list = NULL;
/****************************************************************************
 * Forward references
 ***************************************************************************/
static gint          delete_event_cb              (GtkWidget         *w,
						   GdkEventAny       *e,
						   gpointer          data);

GxSNMPApp *
gxsnmp_app_new (const gchar *geometry)
{
  GxSNMPApp     *app;
  GtkWidget     *window_vbox;
 
  D_FUNC_START;
  app = g_new (GxSNMPApp, 1);
  app->map_list = NULL;

  app->app = gnome_app_new (PACKAGE, _("GXSNMP Network Management Tool"));
  
  window_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (window_vbox), 2);
  
  gtk_window_set_policy (GTK_WINDOW (app->app), TRUE, TRUE, TRUE);
  gtk_window_set_default_size (GTK_WINDOW (app->app), 1000, 450);
  gtk_window_set_wmclass (GTK_WINDOW (app->app), "gxsnmp", "GxSNMP");

  app->map_notebook = gtk_notebook_new ();
  gtk_box_pack_start_defaults (GTK_BOX (window_vbox), app->map_notebook);
  gtk_notebook_popup_enable (GTK_NOTEBOOK (app->map_notebook));

  gnome_app_set_contents (GNOME_APP (app->app), window_vbox);

  app->appbar = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_NEVER);
  gnome_app_set_statusbar (GNOME_APP (app->app), app->appbar);
  
  gxsnmp_install_menus_and_toolbar (app);
  /** connect the signals **/
  gtk_signal_connect (GTK_OBJECT (app->app), "delete_event",
		      (GtkSignalFunc) delete_event_cb,
		      app);

  /** geometry parsing **/
  if (geometry != NULL)
    {
      gint x, y, w, h;
      if (gnome_parse_geometry (geometry, &x, &y, &w, &h) )
	{
	  if (x != -1)
	    gtk_widget_set_uposition (app->app, x, y);
	  if (w != -1)
	    gtk_window_set_default_size (GTK_WINDOW (app->app), w, h);
	}
      else
	{
	  g_error (_("Unable to parse the geometry string '%s'"), geometry);
	}
    }
  else
    {                              /* Lets see if the config has a geometry */
      gint x, y, w, h;
      w = gnome_config_get_int ("/GXSNMP/Geometry/width");
      h = gnome_config_get_int ("/GXSNMP/Geometry/height");
      if (w != 0 && h != 0)
	gtk_window_set_default_size (GTK_WINDOW (app->app), w, h); 
      x = gnome_config_get_int ("/GXSNMP/Geometry/xpos");
      y = gnome_config_get_int ("/GXSNMP/Geometry/ypos");
      if (x != 0 && y != 0)
	gtk_widget_set_uposition (app->app, x, y);
    }
  app_list = g_slist_prepend (app_list, app);
  D_FUNC_END;
  return app;
}

void
gxsnmp_app_close (GxSNMPApp *app)
{
  D_FUNC_START;
  g_return_if_fail (app != NULL);
  app_list = g_slist_remove (app_list, app);
  if (app_list == NULL)
    {
      gint x, y, w, h;
 
      /* Last window save the position of the window */
      gdk_window_get_position (app->app->window, &x, &y);
      gdk_window_get_size (app->app->window, &w, &h);
      
      gnome_config_set_int ("/GXSNMP/Geometry/width", w);
      gnome_config_set_int ("/GXSNMP/Geometry/height", h);
      gnome_config_set_int ("/GXSNMP/Geometry/xpos", x);
      gnome_config_set_int ("/GXSNMP/Geometry/ypos", y);
      gnome_config_sync ();
    }
  
  gtk_widget_destroy (GTK_WIDGET (app->app));
  app->app = NULL;
  g_free (app);
  if (app_list == NULL)
    {
      D_FUNC_END;
      gtk_main_quit ();
    }
  D_FUNC_END;
}

static gint
delete_event_cb (GtkWidget *window, GdkEventAny *e, gpointer data)
{
  GxSNMPApp   *app = (GxSNMPApp *)data;
  D_FUNC_START;
  gxsnmp_app_close (app);
  D_FUNC_END;
  return TRUE;
}

/** Supporting functions **/
void 
gxsnmp_app_add_map (GxSNMPApp *app, GxSNMPMap *map, const gchar *name)
{
  GtkWidget   *scrolled_window;
  GtkWidget   *frame;
  GtkWidget   *note_tab;
  D_FUNC_START;
  /* cache a pointer to the app window we are being added to */
  map->app = app;
  gnome_canvas_set_scroll_region (GNOME_CANVAS (map), 0, 0, 
				  map->width, map->height);
  scrolled_window = gtk_scrolled_window_new (GTK_LAYOUT (map)->hadjustment, 
					     GTK_LAYOUT (map)->vadjustment);
  d_print (DEBUG_DUMP, "(%d, %d)\n", map->width, map->height);
  d_print (DEBUG_DUMP, "(%d, %d)\n", GNOME_CANVAS (map)->layout.width,
	   GNOME_CANVAS (map)->layout.height);
  gtk_container_add (GTK_CONTAINER (scrolled_window), GTK_WIDGET (map));

  note_tab = gtk_label_new (name);
  gtk_notebook_append_page (GTK_NOTEBOOK (app->map_notebook),
                            scrolled_window, note_tab);
  app->map_list = g_list_append (app->map_list, map);
  gtk_widget_show_all (scrolled_window);
  D_FUNC_END;
}


/* EOF */


