/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gxsnmp_xml_io.h"

#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

#include "debug.h"
/****************************************************************************
 * Local functions
 **/

/****************************************************************************
 * Module implementation.
 **/
/**
 * gxsnmp_xml_write_list:
 *
 * Write the contents of a given list into the a file.
 *
 **/
gboolean 
gxsnmp_xml_write_list (GList *list, const gchar *filename)
{
  xmlNsPtr     gxsnmp_ns;
  xmlDocPtr    doc;
  gint         ret;
  D_FUNC_START;
  g_return_val_if_fail (filename != NULL, FALSE);
  doc = xmlNewDoc ("1.0");
  if (!doc)
    {
      D_FUNC_END;
      g_warning ("Failed to create a new xml doc.\n");
      return FALSE;
    }
  gxsnmp_ns = xmlNewGlobalNs (doc, "http://www.gxsnmp.org/", "gxsnmp");
  doc->root = xmlNewDocNode (doc, gxsnmp_ns, "ListStorage", NULL);
  xmlSetProp (doc->root, "Version", VERSION);
  while (list)
    {
      xmlNewChild (doc->root, gxsnmp_ns, "ListValue", (gchar *)list->data);
      list = list->next;
    }
  ret = xmlSaveFile (filename, doc);
  xmlFreeDoc (doc);
  D_FUNC_END;
  return (ret < 0) ? FALSE : TRUE;
}
/**
 * gxsnmp_xml_write_slist:
 *
 **/
gboolean 
gxsnmp_xml_write_slist (GSList *slist, const gchar *filename)
{
  xmlNsPtr     gxsnmp_ns;
  xmlDocPtr    doc;
  gint         ret;
  D_FUNC_START;
  g_return_val_if_fail (filename != NULL, FALSE);
  doc = xmlNewDoc ("1.0");
  if (!doc)
    {
      D_FUNC_END;
      g_warning ("Failed to create a new xml doc.\n");
      return FALSE;
    }
  gxsnmp_ns = xmlNewGlobalNs (doc, "http://www.gxsnmp.org/", "gxsnmp");
  doc->root = xmlNewDocNode (doc, gxsnmp_ns, "SListStorage", NULL);
  xmlSetProp (doc->root, "Version", VERSION);
  while (slist)
    {
      xmlNewChild (doc->root, gxsnmp_ns, "SListValue", (gchar *)slist->data);
      slist = slist->next;
    }
  ret = xmlSaveFile (filename, doc);
  xmlFreeDoc (doc);
  D_FUNC_END;
  return (ret < 0) ? FALSE : TRUE;
}
/**
 * gxsnmp_xml_read_list:
 *
 **/
gboolean 
gxsnmp_xml_read_list (GList *list, const gchar *filename)
{
  xmlDocPtr    doc;
  xmlNodePtr   cur;
  xmlNsPtr     gxsnmp_ns;
  gchar        *tmp = NULL;

  D_FUNC_START;
  doc = xmlParseFile (filename);
  if (doc == NULL)
    {
      g_warning ("Unable to read list definition file '%s'.",
		 filename);
      D_FUNC_END;
      return FALSE;
    }
  cur = doc->root;
  if (!cur)
    {
      g_warning ("Empty list definition file '%s'.", filename);
      xmlFreeDoc (doc);
      D_FUNC_END;
      return FALSE;
    }
  gxsnmp_ns = xmlSearchNsByHref (doc, doc->root, "http://www.gxsnmp.org/");
  if (doc->root->name == NULL ||
      strcasecmp (doc->root->name, "ListStorage") ||
      gxsnmp_ns == NULL)
    {
      g_warning ("Unrecognized file format.");
      xmlFreeDoc (doc);
      D_FUNC_END;
      return FALSE;
    }
  /* Now this looks like something we wrote lets read it back in */
  cur = cur->childs;
  while (cur)
    {
      if (strcasecmp (cur->name, "ListValue"))
	tmp = xmlNodeListGetString (doc, cur->childs, 1);
      if (tmp)
	list = g_list_append (list, tmp);
      cur = cur->next;
    }
  xmlFreeDoc (doc);
  D_FUNC_END;
  return TRUE;
}

/**
 * gxsnmp_xml_read_slist:
 *
 **/
gboolean 
gxsnmp_xml_read_slist (GSList *slist, const gchar *filename)
{
  xmlDocPtr    doc;
  xmlNodePtr   cur;
  xmlNsPtr     gxsnmp_ns;
  gchar        *tmp = NULL;

  D_FUNC_START;
  doc = xmlParseFile (filename);
  if (doc == NULL)
    {
      g_warning ("Unable to read list definition file '%s'.",
		 filename);
      D_FUNC_END;
      return FALSE;
    }
  cur = doc->root;
  if (!cur)
    {
      g_warning ("Empty list definition file '%s'.", filename);
      xmlFreeDoc (doc);
      D_FUNC_END;
      return FALSE;
    }
  gxsnmp_ns = xmlSearchNsByHref (doc, doc->root, "http://www.gxsnmp.org/");
  if (doc->root->name == NULL ||
      strcasecmp (doc->root->name, "SListStorage") ||
      gxsnmp_ns == NULL)
    {
      g_warning ("Unrecognized file format.");
      xmlFreeDoc (doc);
      D_FUNC_END;
      return FALSE;
    }
  /* Now this looks like something we wrote lets read it back in */
  cur = cur->childs;
  while (cur)
    {
      if (strcasecmp (cur->name, "SListValue"))
	tmp = xmlNodeListGetString (doc, cur->childs, 1);
      if (tmp)
	slist = g_slist_append (slist, tmp);
      cur = cur->next;
    }
  xmlFreeDoc (doc);
  D_FUNC_END;
  return TRUE;
}

/* EOF */




