/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * xml support.
 */ 
#ifndef __GXSNMP_XML_IO_H__
#define __GXSNMP_XML_IO_H__

#include <gnome.h>

/****************************************************************************
 * Public functions.
 **/
gboolean            gxsnmp_xml_write_list          (GList           *list, 
					            const gchar     *filename);
gboolean            gxsnmp_xml_write_slist         (GSList          *slist,
						    const gchar     *filename);
gboolean            gxsnmp_xml_read_list           (GList           *list,
						    const gchar     *filename);
gboolean            gxsnmp_xml_read_slist          (GSList          *slist,
						    const gchar     *filename);

#endif
/* EOF */
