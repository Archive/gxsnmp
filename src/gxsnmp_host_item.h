/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifndef __GXSNMP_HOST_ITEM_H__
#define __GXSNMP_HOST_ITEM_H__
#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

#include "gxsnmp_map_item.h"
BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros
 **/
#define GXSNMP_TYPE_HOST_ITEM              (gxsnmp_host_item_get_type ())
#define GXSNMP_HOST_ITEM(obj)              (GTK_CHECK_CAST ((obj), GXSNMP_TYPE_HOST_ITEM, GxSNMPHostItem))
#define GXSNMP_HOST_ITEM_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_HOST_ITEM, GxSNMPHostItemClass))
#define GXSNMP_IS_HOST_ITEM(obj)           (GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_HOST_ITEM))
#define GXSNMP_IS_HOST_ITEM_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_HOST_ITEM))
/****************************************************************************
 * Control Blocks
 **/
typedef struct _GxSNMPHostItem             GxSNMPHostItem;
typedef struct _GxSNMPHostItemClass        GxSNMPHostItemClass;
struct _GxSNMPHostItem
{
  GnomeCanvasItem          item;

  GdkImlibImage            *im;
  GdkPixmap                *pixmap;
  GdkBitmap                *mask;
  GdkGC                    *gc;        /* Gdk rendering */
  ArtPixBuf                *pixbuf;    /* AA rendering */
  double                   affine[6];

  /* Now our coordinates */
  double                   xpos;  /* Position at the anchor, item relative */
  double                   ypos;
  double                   width; /* Size of the image, item relative */
  double                   height;

  /* Now here is the rendered size */
  int                      cx;
  int                      cy;
  int                      cwidth;
  int                      cheight;
  

  gpointer                 app;             /* Application specific pointer */
  /* Flags */
  guint                    selected    : 1;   /* Render as selected. */
  guint                    sensitive   : 1;   /* We can be moved etc.. */
  guint                    dragging    : 1;   /* We are being dragged around */
  guint                    need_recalc : 1;
};
struct _GxSNMPHostItemClass
{
  GnomeCanvasItemClass       class;
  /* methods */
  gboolean (* select)      (GxSNMPHostItem    *item);
  gboolean (* unselect)    (GxSNMPHostItem    *item);

  void     (* edit_item)   (GxSNMPHostItem    *item);
  void     (* popup_menu)  (GxSNMPHostItem    *item,
			    GdkEventButton   *event,
			    gpointer         data);
  gint     (* changed)     (GxSNMPHostItem    *item,
			    gpointer         data);
};
/****************************************************************************
 * Public functions
 **/
GtkType        gxsnmp_host_item_get_type         (void);

END_GNOME_DECLS
#endif
/* EOF */

