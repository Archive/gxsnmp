/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifndef __GXSNMP_APP_H__
#define __GXSNMP_APP_H__

#include <gnome.h>
#include "gxsnmp_map.h"

typedef struct _GxSNMPApp     GxSNMPApp;

struct _GxSNMPApp {
  GtkWidget    *app;     /* The GnomeApp widget */
  GtkWidget    *appbar;  /* The GnomeAppBar in the GnomeApp for convience */

  GtkWidget    *map_notebook;
  
  GList        *map_list; /* List of active maps on/in this window */
  guint        current_tool;
};

GxSNMPApp              *gxsnmp_app_new               (const gchar     *geo);
void                   gxsnmp_app_close              (GxSNMPApp       *app);
void                   gxsnmp_app_add_map            (GxSNMPApp       *app,
						      GxSNMPMap       *map,
						      const gchar     *name);
#endif

/* EOF */
