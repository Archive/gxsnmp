/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gxsnmp_map_item.h"

#include "debug.h"
/****************************************************************************
 * Forward references
 **/
static void       gxsnmp_map_item_class_init   (GxSNMPMapItemClass *klass);
static void       gxsnmp_map_item_init         (GxSNMPMapItem      *item);
static void       gxsnmp_map_item_destroy      (GtkObject          *object);
static void       gxsnmp_map_item_set_arg      (GtkObject          *object,
						GtkArg             *arg,
						guint              arg_id);
static void       gxsnmp_map_item_get_arg      (GtkObject          *object,
						GtkArg             *arg,
						guint              arg_id);
static void       gxsnmp_map_item_realize      (GnomeCanvasItem    *item);
static void       gxsnmp_map_item_unrealize    (GnomeCanvasItem    *item);
static void       gxsnmp_map_item_draw         (GnomeCanvasItem    *item,
						GdkDrawable        *drawable,
						int                xx,
						int                yy,
						int                width,
						int                height);
static void       gxsnmp_map_item_render       (GnomeCanvasItem    *item,
						GnomeCanvasBuf     *buf);
static void       gxsnmp_map_item_update       (GnomeCanvasItem    *item,
						 double            *affline,
						 ArtSVP            *clip,
						 int               flags);
/****************************************************************************
 * Local definitions
 **/
/** Arguments */
enum {
  ARG_0
};
/** Signals */
enum {
  LAST_SIGNAL
};
/** Parent class */
static GnomeCanvasItemClass         *parent_class;
/**
 * gxsnmp_map_item_get_type:
 *
 * Registers the &GxSNMPMapItem class if necessary, and returns the unique
 * type ID associated to it.
 *
 * Return value: The unique type ID of the &GxSNMPMapItem class.
 **/
GtkType
gxsnmp_map_item_get_type (void)
{
  static GtkType map_item_type = 0;
  D_FUNC_START;
  if (!map_item_type)
    {
      static const GtkTypeInfo map_item_info = {
	"GxSNMPMapItem",
	sizeof (GxSNMPMapItem),
	sizeof (GxSNMPMapItemClass),
	(GtkClassInitFunc) gxsnmp_map_item_class_init,
	(GtkObjectInitFunc) gxsnmp_map_item_init,
	NULL, /* reserved_1 */
	NULL, /* reserved_2 */
	(GtkClassInitFunc) NULL
      };
      map_item_type = gtk_type_unique (gnome_canvas_item_get_type (),
				       &map_item_info);
    }
  D_FUNC_END;
  return map_item_type;
}
/****************************************************************************
 * Class initialization.
 **/
static void
gxsnmp_map_item_class_init (GxSNMPMapItemClass *klass)
{
  GtkObjectClass        *object_class;
  GnomeCanvasItemClass  *item_class;
  D_FUNC_START;
  object_class = (GtkObjectClass *)klass;
  item_class   = (GnomeCanvasItemClass *)klass;
  parent_class = gtk_type_class (gnome_canvas_item_get_type ());

  /* Object methods */
  object_class->destroy  = gxsnmp_map_item_destroy;
  object_class->set_arg  = gxsnmp_map_item_set_arg;
  object_class->get_arg  = gxsnmp_map_item_get_arg;

  item_class->realize    = gxsnmp_map_item_realize;
  item_class->unrealize  = gxsnmp_map_item_unrealize;
  item_class->draw       = gxsnmp_map_item_draw;
  item_class->render     = gxsnmp_map_item_render;
  item_class->update     = gxsnmp_map_item_update;

  D_FUNC_END;
}
/****************************************************************************
 * Object initialization
 **/
static void
gxsnmp_map_item_init (GxSNMPMapItem *item)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * destroy handler.
 **/
static void
gxsnmp_map_item_destroy (GtkObject *object)
{
  D_FUNC_START;
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
  D_FUNC_END;
}
/****************************************************************************
 * set_arg handler.
 **/
static void
gxsnmp_map_item_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * get_arg handler.
 **/
static void
gxsnmp_map_item_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * realize handler.
 **/
static void
gxsnmp_map_item_realize (GnomeCanvasItem *item)
{
  D_FUNC_START;
  if (parent_class->realize)
    (* parent_class->realize) (item);

  D_FUNC_END;
}
/****************************************************************************
 * unrealize handler.
 **/
static void
gxsnmp_map_item_unrealize (GnomeCanvasItem *item)
{
  D_FUNC_START;
  if (parent_class->unrealize)
    (* parent_class->unrealize) (item);
  D_FUNC_END;
}
/****************************************************************************
 * draw handler. (Gdk method)
 **/
static void
gxsnmp_map_item_draw (GnomeCanvasItem *item, GdkDrawable *drawable,
		       int xx, int yy, int width, int height)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * render hanlder. (aa canvas)
 **/
static void
gxsnmp_map_item_render (GnomeCanvasItem *item, GnomeCanvasBuf *buf)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * update handler.
 **/
static void
gxsnmp_map_item_update (GnomeCanvasItem *item, double *affine, ArtSVP *clip,
			 gint flags)
{
  D_FUNC_START;
  
  if (parent_class->update)
    (* parent_class->update) (item, affine, clip, flags);

  D_FUNC_END;
}

/* EOF */


