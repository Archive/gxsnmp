/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Config handling routines.
 */ 
#ifndef __GXSNMP_CONFIG_H__
#define __GXSNMP_CONFIG_H__

#include <gnome.h>

/****************************************************************************
 * Config Macros.
 **/

/****************************************************************************
 * Control blocks.
 **/
typedef struct _GxSNMPConfig             GxSNMPConfig;
struct _GxSNMPConfig
{
  /* Mib Information */
  gchar       *mib_path;         /* The path to search when loading mibs */
  GSList      *mibs;             /* The list of mibs to load. */

  /* Plugin information */        
  gchar       *plugin_path;      /* The path to search when loading plugins */
  GSList      *plugins;          /* A list of plugins to load. */
};
/****************************************************************************
 * Public Functions.
 **/
GxSNMPConfig          *gxsnmp_config_init             (void);
gboolean              gxsnmp_config_save              (GxSNMPConfig   *config);
void                  gxsnmp_config_destroy           (GxSNMPConfig   *config);
#endif
/* EOF */
