/* Handled line types for GnomeCanvas widget
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_HANDLE_LINE_H
#define GNOME_CANVAS_HANDLE_LINE_H

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas-line.h> /* fixme when we go to gnome-libs if we do */
#include "gnome-canvas-handle.h"
#include "gnome-canvas-handled.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_HANDLE_LINE            (gnome_canvas_handle_line_get_type ())
#define GNOME_CANVAS_HANDLE_LINE(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HANDLE_LINE, GnomeCanvasHandleLine))
#define GNOME_CANVAS_HANDLE_LINE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HANDLE_LINE, GnomeCanvasHandleLineClass))
#define GNOME_IS_CANVAS_HANDLE_LINE(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HANDLE_LINE))
#define GNOME_IS_CANVAS_HANDLE_LINE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HANDLE_LINE))

typedef enum {
  GNOME_HANDLE_LINE_LINE, /* Free movement */
  GNOME_HANDLE_LINE_VRULE, /* locked x pos, x1 == x2 */
  GNOME_HANDLE_LINE_HRULE  /* locked y pos  y1 == y2 */
} GnomeHandleLineType;

typedef struct _GnomeCanvasHandleLine GnomeCanvasHandleLine;
typedef struct _GnomeCanvasHandleLineClass GnomeCanvasHandleLineClass;

struct _GnomeCanvasHandleLine {
  GnomeCanvasHandled handled;

  GnomeHandleLineType type;

  double coords[4];
  
  GnomeCanvasLine*  line; /* OK to set characteristics of the line, 
			     except for its position (points) */

  /* don't play with these */
  GnomeCanvasHandle* handle1; /* x1, y1 */
  GnomeCanvasHandle* handle2; /* x2, y2 */
};

struct _GnomeCanvasHandleLineClass {
  GnomeCanvasHandledClass parent_class;

};

/* Standard Gtk function */
GtkType gnome_canvas_handle_line_get_type (void);

END_GNOME_DECLS

#endif

