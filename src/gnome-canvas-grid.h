/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifndef __GNOME_CANVAS_GRID_H__
#define __GNOME_CANVAS_GRID_H__

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>
BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros
 **/
#define GNOME_TYPE_CANVAS_GRID            (gnome_canvas_grid_get_type ())
#define GNOME_CANVAS_GRID(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_GRID, GnomeCanvasGrid))
#define GNOME_CANVAS_GRID_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_GRID))
#define GNOME_IS_CANVAS_GRID(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_GRID))
#define GNOME_IS_CANVAS_GRID_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_GRID))
/****************************************************************************
 * Control blocks for the canvas grid.
 **/
typedef struct _GnomeCanvasGrid        GnomeCanvasGrid;
typedef struct _GnomeCanvasGridClass   GnomeCanvasGridClass;

struct _GnomeCanvasGrid {
  GnomeCanvasItem     item;
  
  GdkGC               *gc;             /* The GC for the non aa canvas */
  GdkBitmap           *stipple;        /* The stipple pattern */
  GdkColor            color;           /* The gdk color */

  guint               line_color;
  gint                x, y;            /* The grid size */

};
struct _GnomeCanvasGridClass {
  GnomeCanvasItemClass parent_class;
};
/****************************************************************************
 * Public API
 **/
GtkType               gnome_canvas_grid_get_type             (void);

END_GNOME_DECLS
#endif
