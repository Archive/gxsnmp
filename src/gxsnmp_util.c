/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Helper functions.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include "gxsnmp_util.h"
#include "debug.h"

/** 
 * match_strval:
 * @val:
 * @value_string:
 *
 * Looks up a numeric value in a list and returns the corresponding 
 * description. Unlike hash tables, the list can be defined staticly in a 
 * .c or .h file.
 *
 * Return value:
 **/
gchar*
match_strval(guint32 val, const value_string *vs) 
{
  gint i = 0;
  
  D_FUNC_START;
  while (vs[i].strptr) 
    {
      if (vs[i].value == val)
	break;
      i++;
    }
  D_FUNC_END;
  return(vs[i].strptr);
}

/* EOF */
