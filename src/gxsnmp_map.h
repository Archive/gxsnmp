/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (C) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *  
 *  The map widget.
 */

#ifndef __GXSNMP_MAP_H__
#define __GXSNMP_MAP_H__

BEGIN_GNOME_DECLS
/****************************************************************************
 *  Standard widget macros
 **/
#define GXSNMP_TYPE_MAP            (gxsnmp_map_get_type())
#define GXSNMP_MAP(obj)            (GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MAP, GxSNMPMap))
#define GXSNMP_MAP_CLASS(klass)    (GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_MAP, GxSNMPMapClass))
#define GXSNMP_IS_MAP(obj)         (GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MAP))
#define GXSNMP_IS_MAP_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MAP))
/**
 *  The mouse states
 *
 **/
typedef enum {
  MOUSE_STATE_NONE,	   /* No mouse buttons depressed */
  MOUSE_STATE_RUBBERBAND,  /* Left button depressed, selecting item(s) */
  MOUSE_STATE_DRAGGING,	   /* Left button depressed, dragging item(s) */
  MOUSE_STATE_MID_SCROLL   /* Drag the canvas itself around */ 
} GxSNMPMapMouseState;

typedef enum {
  MAP_STATE_NONE,
  MAP_STATE_RUBBERBAND,    /* Doing a rubberband. */
  MAP_STATE_MOUSE_SCROLL
} GxSNMPMapState;
/*****************************************************************************
 *  Control blocks for the widget class and for widget instances
 **/
typedef struct _GxSNMPMapClass   GxSNMPMapClass;
typedef struct _GxSNMPMap 	 GxSNMPMap;
struct _GxSNMPMap
{
  GnomeCanvas       canvas;		/* The GnomeCanvas base widget */
  GnomeCanvasItem   *grid;              /* The grid item */
  gchar		    *name;		/* Name of this map */
  gpointer          app;                /* The app window that holds us. */
  GList             *selected;           /* GList of all selected objects */
  gint              active_tool;        /* What tool is active */

  gdouble	    x1, y1;		/* Point where left mouse button */
					/* was clicked and held down */
  gdouble	    x2, y2;		/* Last saved mouse location */
  gboolean          do_snapping;        /* Shall we snap the coordinates? */
  gint              width, height;      /* Width and Height of the canvas */
                                        /* area */
  gdouble           grid_x, grid_y;     /* Grid x/y size */

  /* Autoscroll stuff */
  gint              timer_tag;
  gint              vvalue_diff, hvalue_diff;
  gint              event_last_x, event_last_y;
  
  /* Flags */
  guint             grid_visible : 1;   /* Is the grid visible on this map */
  GxSNMPMapState    map_state;
}; 
struct _GxSNMPMapClass
{
  GnomeCanvasClass canvas_class;   	/* The GnomeCanvas base class */
};

/****************************************************************************
 *
 *  Public widget manipulation functions
 *
 **/
GtkType	           gxsnmp_map_get_type		   (void);
GtkWidget          *gxsnmp_map_new		   (void);
void	           gxsnmp_map_set_name		   (GxSNMPMap 	     *map,
						    gchar	     *name);
void               gxsnmp_map_set_geometry         (GxSNMPMap        *map,
						    gint             width,
						    gint             height);
void               gxsnmp_map_zoom_in              (GxSNMPMap        *map);
void               gxsnmp_map_zoom_out             (GxSNMPMap        *map);
void               gxsnmp_map_toggle_grid          (GxSNMPMap        *map);
void               gxsnmp_map_set_grid_visiblity   (GxSNMPMap        *map,
						    guint            flag);
END_GNOME_DECLS
#endif /* __GXSNMP_MAP_H__ */





