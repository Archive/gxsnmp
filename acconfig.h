/* acconfig.h
   This file is in the public domain.

   Descriptive text for the C preprocessor macros that
   the distributed Autoconf macros can define.
   No software package will use all of them; autoheader copies the ones
   your configure.in uses into your configuration header file templates.

   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  Although this order
   can split up related entries, it makes it easier to check whether
   a given entry is in the file.

   Leave the following blank line there!!  Autoheader needs it.  */
^L

/* These get defined to the version numbers in configure.in */
#define I_AM_GXSNMP
#undef GXSNMP_MAJOR_VERSION
#undef GXSNMP_MINOR_VERSION
#undef GXSNMP_MICRO_VERSION
#undef HAVE_SQL
#undef USE_MYSQL
#undef USE_POSTGRESQL
#undef HAVE_SYS_SELECT_H
#undef NO_FD_SET
#undef VERSION
#undef PACKAGE
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef HAVE_STRUCT_ICMP
#undef HAVE_INET
#undef HAVE_IPX
#undef HAVE_INET6
#undef USE_GLE
#undef USE_GMODULE

^L
/* Leave that blank line there!!  Autoheader needs it.
   If you're adding to this file, keep in mind:
   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  */

