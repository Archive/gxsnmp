/*
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**  API for reading in and accessing configuration file data.
*/
#ifndef _CONFIG_FILE_H_
#define _CONFIG_FILE_H_

typedef struct config_node CONFIG_NODE;  /* An opaque type */

gboolean      config_node_is_number  (CONFIG_NODE * node);

gboolean      config_node_is_string  (CONFIG_NODE * node);

gboolean      config_node_is_list    (CONFIG_NODE * node);

CONFIG_NODE * config_node_new        (gchar       * key);

CONFIG_NODE * config_node_new_number (gchar       * key,
                                      gint          number);

CONFIG_NODE * config_node_new_string (gchar       * key,
                                      gchar       * string);

CONFIG_NODE * config_node_new_list   (gchar       * key);

gboolean      config_node_insert     (CONFIG_NODE * node,
                                      CONFIG_NODE * parent);

gboolean      config_node_remove     (CONFIG_NODE * node,
                                      CONFIG_NODE * parent);

gint          config_node_destroy    (CONFIG_NODE * node);

gint          config_node_get_number (CONFIG_NODE * node);

gchar       * config_node_get_string (CONFIG_NODE * node);

gboolean      config_node_set_number (CONFIG_NODE * node,
				      gint          number);

gboolean      config_node_set_string (CONFIG_NODE * node,
				      gchar       * string);

gboolean      config_node_set_list   (CONFIG_NODE * node);

CONFIG_NODE * config_node_lookup     (CONFIG_NODE * node,
                                      gchar       * key);

void	      config_node_foreach    (CONFIG_NODE * node,
				      GHFunc        function,
				      gpointer      user_data);

CONFIG_NODE * config_load            (gchar       * filename);

gboolean      config_save            (CONFIG_NODE * node,
                                      gchar       * filename);

#endif

/* EOF */
