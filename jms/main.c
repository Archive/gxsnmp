/*
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include <smi.h>
#include <glib.h>
#include <g_snmp.h>

#include "config_file.h"
#include "event.h"

/******************************************************************************
**
**  Static memory
**
******************************************************************************/

GMainLoop   * mainloop;		/* Main program loop structure */
CONFIG_NODE * config = NULL;	/* Internal representation of config file */

/******************************************************************************
**
**  Here is the main program loop 
**
******************************************************************************/

int main (int argc, char *argv[])
{
  config = config_load("collector.conf");

  printf("Start of configuration file:\n");
  config_save(config, NULL);
  printf ("End of configuration file.\n");

/*
**  Initialize SMI.  Call smiInit, then call smiLoadModule to load in the
**  MIB definition for the variables we are interested in accessing.
*/

  smiInit ("gxsnmp");
  smiLoadModule ("RFC-1213");

/*
**  Initialize Jochen's SNMP library
*/

  if (!g_snmp_init (FALSE))
    g_error ("Failed to initialize the SNMP library.");

/*
**  Next, create a main loop.  Create a single "timeout" and set it to go
**  off immediately.  Once the timeout is created, simply run the main loop.
**
**  Control will quickly be transferred to event_scheduler().
*/

  mainloop = g_main_new (TRUE);

  g_timeout_add (0, event_scheduler, 0);

  while (g_main_is_running (mainloop))
    g_main_run (mainloop);

  g_main_destroy (mainloop);
  return (0);
}

