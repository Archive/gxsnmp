/*
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <glib.h>
#include "oidcmp.h"

/******************************************************************************
**
**  Routine to compare a oid, s1, with a reference oid, s2.
**  Returns zero if n1 >= n2, and the first n2 integers of s1 match s2.
**
******************************************************************************/

gint
oidcmp (gulong * s1, gulong * s2, gint n1, gint n2)
{
 if (n1 < n2)
    return -1;
  while (n2--)
    if (*s1++ != *s2++)
      return (*(s1 - 1) < *(s2 - 1)) ? -1 : 1;
  return 0;
}

