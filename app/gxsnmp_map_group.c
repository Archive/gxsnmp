/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The map_group widget is for grouping other map items together.
 * It can be expanded/collasped.
 */
#include <gnome.h>
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "gxsnmp_map_group.h"
#include "menus.h"

/****************************************************************************
 * Arguments for this item 
 ***************************************************************************/

/****************************************************************************
 * Forward references
 ***************************************************************************/
static void      map_group_class_init       (GXsnmp_map_groupClass   *klass);
static void      map_group_init             (GXsnmp_map_group        *group);
static void      map_group_get_arg          (GtkObject               *object,
					     GtkArg                  *arg,
					     guint                   arg_id);
static void      map_group_set_arg          (GtkObject               *object,
					     GtkArg                  *arg,
					     guint                   arg_id);
static void      map_group_destroy          (GtkObject               *object);

/****************************************************************************
 * gxsnmp_map_group_get_type ()
 ***************************************************************************/
static GXsnmp_map_item *parent_class = NULL;
GtkType
gxsnmp_map_group_get_type ()
{
  static GtkType group_type = 0;
  if (!group_type)
    {
      GtkTypeInfo group_info =
      {
	"GXsnmp_map_group",
	sizeof (GXsnmp_map_group),
	sizeof (GXsnmp_map_groupClass),
	(GtkClassInitFunc) map_group_class_init,
	(GtkObjectInitFunc) map_group_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      group_type = gtk_type_unique (gxsnmp_map_item_get_type (), &group_info);
    }
  return group_type;
}
/****************************************************************************
 * The Class initalization subroutine 
 ***************************************************************************/
static void
map_group_class_init (GXsnmp_map_groupClass *klass)
{
  GtkObjectClass       *object_class;
  GXsnmp_map_itemClass *map_item_class;

  object_class   = (GtkObjectClass *)       klass;
  map_item_class = (GXsnmp_map_itemClass *) klass;
  parent_class   = gtk_type_class (gxsnmp_map_item_get_type ());

  /* Arg types */

  /* Object methods */
  object_class->set_arg    = map_group_set_arg;
  object_class->get_arg    = map_group_get_arg;
  object_class->destroy    = map_group_destroy;
  
  /* Class methods */

  /* DB Hooks */
}
/****************************************************************************
 * map_group_set_arg ()
 ***************************************************************************/
static void
map_group_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
}
/****************************************************************************
 * map_group_get_arg ()
 ***************************************************************************/
static void
map_group_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
}
/****************************************************************************
 * map_group_destroy ()
 ***************************************************************************/
static void
map_group_destroy (GtkObject *object)
{
}
/****************************************************************************
 * Widget initialization callback
 ***************************************************************************/
static void
map_group_init (GXsnmp_map_group *map_group)
{
  map_group->text_color = NULL;
  map_group->image      = NULL;
  map_group->text       = NULL;
  map_group->members    = NULL;
  map_group->collapsed  = TRUE;  /* start out collapsed -- for now*/
  map_group->expand_map = NULL;  /* Expand in place. */
};
/****************************************************************************
 *
 * PUBLIC API
 *
 ***************************************************************************/
/****************************************************************************
 * New method 
 ***************************************************************************/
GXsnmp_map_item *
gxsnmp_map_group_new (DB_graph *graph)
{
  GXsnmp_map_group *map_group;
  GXsnmp_map_item  *group_map_item;
  GnomeCanvasGroup *root_group;

  g_return_val_if_fail (graph != NULL, NULL);
  g_return_val_if_fail (graph->DB_map != NULL, NULL);

  root_group = gnome_canvas_root (GNOME_CANVAS (graph->DB_map->application));
  map_group  = GXSNMP_MAP_GROUP (gnome_canvas_item_new 
				 (root_group,
				  gxsnmp_map_group_get_type (),
				  "x", (double)graph->x,
				  "y", (double)graph->y,
				  NULL));
  group_map_item = GXSNMP_MAP_ITEM (map_group);
  
  /* point the DB_graph object and the gxsnmp_map_group object at each other*/
  group_map_item->DB_graph = graph;
  graph->application = map_group;

  return GXSNMP_MAP_ITEM (map_group);
}
