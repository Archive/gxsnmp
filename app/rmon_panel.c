/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Jochen Friedrich & Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * RMON panel
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>
#include "main.h"
#include "rmon_panel.h"
#include "debug.h"

/****************************************************************************
 * Static data 
 ***************************************************************************/
#define HOST_CLIST_COLUMNS 4
static gchar *host_rmon_titles[] = {
  N_("Display Name"), N_("DNS Name"), N_("Contact"), N_("Last Modification")
};
static gint  host_clist_title_widths[] = { 15, 15, 20, 10 };

/****************************************************************************
 * Forward declarations and callback functions  
 ***************************************************************************/
static void      rmon_panel_class_init    (GXsnmp_rmon_panelClass    *klass);
static void      rmon_panel_init          (GXsnmp_rmon_panel         *dialog);
static void      list_dialog_cb           (GnomeDialog               *dialog,
					   gint                       button,
                                           gpointer                   data);
/****************************************************************************
 * gxsnmp_rmon_panel_get_type()
 ***************************************************************************/
GtkType
gxsnmp_rmon_panel_get_type ()
{
  static GtkType list_type = 0;
  if (!list_type)
    {
      GtkTypeInfo list_info = 
      {
	"GXsnmp_rmon_panel",
	sizeof (GXsnmp_rmon_panel),
	sizeof (GXsnmp_rmon_panelClass),
	(GtkClassInitFunc) rmon_panel_class_init,
	(GtkObjectInitFunc) rmon_panel_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      list_type =gtk_type_unique(gnome_dialog_get_type(), &list_info);
    }
  return list_type;
}
/****************************************************************************
 * Class init
 ***************************************************************************/
static void
rmon_panel_class_init (GXsnmp_rmon_panelClass *klass)
{
}
/****************************************************************************
 * Widget init
 ***************************************************************************/
static void
rmon_panel_init (GXsnmp_rmon_panel *dialog)
{
  D_FUNC_START;
  /* Append buttons for close and help */
  gnome_dialog_append_buttons (GNOME_DIALOG (dialog),
                               GNOME_STOCK_BUTTON_CLOSE,
                               GNOME_STOCK_BUTTON_HELP,
                               NULL);
  gtk_signal_connect (GTK_OBJECT(dialog), "clicked",
		      GTK_SIGNAL_FUNC(list_dialog_cb), NULL);
  D_FUNC_END;
}
static void
list_dialog_cb (GnomeDialog *dialog, gint button, gpointer data)
{
  gchar *tmp;
  D_FUNC_START;
  if (button == 0)
    {
      gnome_dialog_close (GNOME_DIALOG (dialog));
    }
  else if (button == 1)
    {
      tmp = gnome_help_file_find_file ("gxsnmp", "rmon-panel.html");
      if (tmp)
	{
	  gnome_help_goto (0, tmp);
	  g_free (tmp);
	}
    }
  D_FUNC_END;
}
/****************************************************************************
 * Public API
 ***************************************************************************/
GtkWidget *
gxsnmp_rmon_panel_new (gpointer data)
{
  GXsnmp_host_list  *dialog;
  D_FUNC_START;
  dialog = gtk_type_new (gxsnmp_rmon_panel_get_type ());
  D_FUNC_END;
  return GTK_WIDGET (dialog);
}

/* EOF */
