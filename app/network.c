/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

/* 
 * network.c containes utility functions related to the creation and 
 * destruction of DB_network items, and functions to manage the DB_network 
 * database.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>
#include "g_sqldb.h"
#include "tables.h"
#include "gxsnmpdb.h"

#include "debug.h"
/****************************************************************************
 * network_create ()
 ***************************************************************************/
DB_network *
network_create (void)
{
  DB_network *dbn;
  D_FUNC_START;
  dbn           = g_new0 (DB_network, 1);
  if (dbn)
    {
      d_print (DEBUG_TRACE, "Memory allocated initlizing structre.\n");
      dbn->created  = g_strdup (_("(not in database)"));
      dbn->modified = g_strdup (dbn->created);
      dbn->DB_graphs= NULL;
      D_FUNC_END;
      return dbn;
    }
  d_print (DEBUG_TRACE, "Unable to alloc memory for new network object.\n");
  D_FUNC_END;
  g_warning ("network_create unable to allocate memory, expect trouble.\n");
  return NULL;

}
/****************************************************************************
 * network_destroy ()
 ***************************************************************************/
void 
network_destroy (DB_network *dbn)
{
  D_FUNC_START;
  g_return_if_fail (dbn != NULL);
  if (dbn->rowid != 0)
    {
      g_warning ("network_destroy: attempt to destroy an object still in "
		 "use.");
      D_FUNC_END;
      return;
    }
  if (dbn)
    {
      d_print (DEBUG_TRACE, "Valid structure, freeing data.\n");
      if (dbn->created)        g_free (dbn->created);
      if (dbn->modified)       g_free (dbn->modified);
      if (dbn->name)           g_free (dbn->name);
      if (dbn->address)        g_free (dbn->address);
      if (dbn->netmask)        g_free (dbn->netmask);
      if (dbn->description)    g_free (dbn->description);
      if (dbn->contact)        g_free (dbn->contact);
      g_free (dbn);
    }
  D_FUNC_END;
}
/****************************************************************************
 * network_add ()
 ***************************************************************************/
void
network_add (DB_network *dbn)
{
  D_FUNC_START;
  g_return_if_fail (dbn != NULL);
  if (dbn->rowid != 0)
    {
      g_warning ("Attempted to re add a network with a rowid of 0");
      return ;
    }
  d_print (DEBUG_DUMP, "Adding new network %s\n", dbn->name);
  dbn->rowid = g_sqldb_highest_rowid (network_sqldb, "_rowid") + 1;
  if (dbn->created) 
    g_free (dbn->created);
  dbn->created = db_timestamp ();
  if (dbn->modified) 
    g_free (dbn->modified);
  dbn->modified = g_strdup (dbn->created);

  g_sqldb_row_add (network_sqldb, dbn);
  D_FUNC_END;
}
/****************************************************************************
 * network_update ()
 ***************************************************************************/
void 
network_update (DB_network *dbn)
{
  D_FUNC_START;
  g_return_if_fail (dbn != NULL);
  g_assert (dbn->rowid != 0);
  d_print (DEBUG_DUMP, "Update network %s\n", dbn->name);
  if (dbn->modified) 
    g_free (dbn->modified);
  dbn->modified = db_timestamp ();
  g_sqldb_row_update (network_sqldb, dbn);
  D_FUNC_END;
}
/****************************************************************************
 * network_delete ()
 ***************************************************************************/
void
network_delete (DB_network *dbn)
{
  GList    *gl;
  DB_graph *dbg;

  D_FUNC_START;
  g_return_if_fail (dbn != NULL);
  g_return_if_fail (dbn->rowid != 0);
  d_print (DEBUG_TRACE, "deleting graph objects.\n");
  while ((gl = dbn->DB_graphs))		/* Assignment intended */
    {
      dbg = (DB_graph *)gl->data;
      graph_delete (dbg); 		/* gl is invalid after this call */
      graph_destroy (dbg);
    }

  dbn->DB_graphs = NULL;
  g_assert (g_sqldb_row_delete (network_sqldb, dbn) == TRUE);
  dbn->rowid = 0;
  D_FUNC_END;
}
/****************************************************************************
 * network_list ()
 ***************************************************************************/
GList *
network_list (void)
{
  d_print (DEBUG_TRACE, "\n");
  return g_sqldb_table_list (network_sqldb);
}
/****************************************************************************
 * network_find_by_rowid ()
 ***************************************************************************/
DB_network *
network_find_by_rowid (guint rowid)
{
  d_print (DEBUG_TRACE, "\n");
  g_return_val_if_fail (rowid != 0, NULL);
  return g_sqldb_row_find (network_sqldb, "_rowid", &rowid);
}

/*
**  Subroutine used to find all network segments assigned to a subnet.
**  In the case of switched ethernet, there may be many segments with
**  the same IP range.  This subroutine returns a GList of them.
*/
/****************************************************************************
 * network_find_by_address ()
 ***************************************************************************/
GList *
network_find_by_address (gchar * address, gchar * netmask)
{
  GList      * gl;
  GList      * result = NULL;
  DB_network * dbn;
  gboolean     matched = FALSE;
  D_FUNC_START;
  g_return_val_if_fail (address != NULL, NULL);
  g_return_val_if_fail (netmask != NULL, NULL);
  gl = g_sqldb_table_list (network_sqldb);
  while (gl)
    {
      matched = FALSE;
      dbn = gl->data;
      if (dbn->address)
        if (!strcmp (address, dbn->address))
	  {
	    if (!netmask && !dbn->netmask)
	      matched = TRUE;
	    if (netmask && dbn->netmask)
	      if (!strcmp (netmask, dbn->netmask))
	        matched = TRUE;
	    if (matched)
	      result = g_list_append (result, dbn);
	  }
      gl = gl->next;
    }
  D_FUNC_END;
  return result;
}
/* EOF */

