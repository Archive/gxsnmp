#include "checkmem.h"
#ifdef __OBJC__
#include <objc/Object.h>
#endif

#define MAX_ALLOC 4097

void domemcheck(void)
{
  int i;
  char *tptr[MAX_ALLOC];
#ifdef __OBJC__
  id iptr[MAX_ALLOC];
#endif
  g_print("domemcheck()\n");
  for(i = 1; i < MAX_ALLOC; i++)
    {
#ifdef __OBJC__
      iptr[i] = [Object new];
#endif
      tptr[i] = g_malloc0(i);
    }
  for(i = 1; i < MAX_ALLOC; i++)
    {
#ifdef __OBJC__
      [iptr[i] free];
#endif
      g_free(tptr[i]);
    }
}

/*eof*/
