/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Network item widget
**
**  A network item is derived from a GXsnmp_map_item object.  
**  The default network item consists of a line, with all hosts on the
**  network connected to that line with the shortest possible wire.
*/

#ifndef __GXSNMP_NETWORK_H__
#define __GXSNMP_NETWORK_H__

BEGIN_GNOME_DECLS

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "gxsnmp_map.h"
#include "gxsnmp_map_item.h"

/*****************************************************************************
 *  Standard widget macros
 ****************************************************************************/
#define GXSNMP_TYPE_NETWORK (gxsnmp_network_get_type())
#define GXSNMP_NETWORK(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_NETWORK, GXsnmp_network)
#define GXSNMP_NETWORK_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_NETWORK, GXsnmp_networkClass)
#define GXSNMP_IS_NETWORK(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_NETWORK)
#define GXSNMP_IS_NETWORK_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_NETWORK)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_network		GXsnmp_network;
typedef struct _GXsnmp_networkClass	GXsnmp_networkClass;

struct _GXsnmp_network
{
  GXsnmp_map_item   	  map_item;	/* Base object */
  GnomeCanvasItem	  *line;	/* The network line */
  GnomeCanvasItem	  *highlight;	/* The network line highlight */
  GnomeCanvasItem	  *shadow;	/* The line shadow */
  GnomeCanvasItem	  *text;	/* The displayed text */
  gchar			  *text_color;	/* Current text color */
  gint                    orientation;
 
  gint                    state;        /* resizing, flashing, etc.. */
  gdouble                 x1, y1;
  gdouble                 x2, y2;
  gdouble		  length;	/* Length of network line */
}; 

struct _GXsnmp_networkClass
{
  GXsnmp_map_itemClass 	  map_item_class;	/* Base class */
};

/****************************************************************************
**
**  Public widget manipulation functions
**
****************************************************************************/
GtkType	          gxsnmp_network_get_type	  (void);
GXsnmp_map_item * gxsnmp_network_new		  (DB_graph * graph);
void              gxsnmp_network_set_display_name (GXsnmp_network    *network,
						   gchar             *name);
END_GNOME_DECLS

#endif /* __GXSNMP_NETWORK_H__ */



