/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

/* 
 * map.c containes utility functions related to the creation and destruction
 * of DB_map items, and functions to manage the DB_map database.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>
#include "g_sqldb.h"
#include "tables.h"
#include "map.h"
#include "gxsnmpdb.h"

#include "debug.h"

extern gint dcsock;

/****************************************************************************
 * map_create
 ***************************************************************************/
DB_map *
map_create (void)
{
  DB_map *dbm;
  D_FUNC_START;
  dbm           = g_new0 (DB_map, 1);
  if (dbm)
    {
      dbm->created  = g_strdup (_("(not in database)"));
      dbm->modified = g_strdup (dbm->created);
      return dbm;
    }
  g_warning ("Memory allocation failed. expect problems.\n");
  D_FUNC_END;
  return NULL;
}
/****************************************************************************
 * map_destroy
 ***************************************************************************/
void 
map_destroy (DB_map *dbm)
{
  D_FUNC_START;
  g_return_if_fail (dbm != NULL);
  if (dbm->rowid != 0)
    {
      g_warning ("map_destroy: attempt to free a map instance that is "
		 "still in use.");
      D_FUNC_END;
      return;
    }
  if (dbm)
    {
      if (dbm->created)        g_free (dbm->created);
      if (dbm->modified)       g_free (dbm->modified);
      if (dbm->name)           g_free (dbm->name);
      if (dbm->tab)            g_free (dbm->tab);
      if (dbm->description)    g_free (dbm->description);
      g_free (dbm);
      dbm = NULL;
    }
  D_FUNC_END;
}
/****************************************************************************
 * map_add ()
 *
 * This function is obsoleted by map table-plugins obj_add()
 *
 ***************************************************************************/
void
map_add (DB_map *dbm)
{
  D_FUNC_START;
  g_return_if_fail (dbm != NULL);
  if (dbm->rowid != 0)
    {
      g_warning ("Attempted to re add a map which is in the DB already.");
      return ;
    }
fprintf(stderr,"1");
  dbm->rowid = g_sqldb_highest_rowid (map_sqldb, "_rowid") + 1;
fprintf(stderr,"rowid is %d\n", dbm->rowid);
  if (dbm->created) 
    g_free (dbm->created);
fprintf(stderr,"2");
  dbm->created = db_timestamp ();
  if (dbm->modified) 
    g_free (dbm->modified);
fprintf(stderr,"3");
  dbm->modified = g_strdup (dbm->created);
  d_print (DEBUG_DUMP, "Adding %s\n", dbm->name);
fprintf(stderr,"4");
  if (g_sqldb_row_add (dcsock, map_sqldb, dbm))
    {
      D_FUNC_END;
      return;
    }
fprintf(stderr,"5");
  dbm->rowid = 0;
  D_FUNC_END;
}
/****************************************************************************
 * map_update 
 ***************************************************************************/
void 
map_update (DB_map *dbm)
{
  D_FUNC_START;
  g_return_if_fail (dbm != NULL);
  g_return_if_fail (dbm->rowid != 0);
  if (dbm->modified) 
    g_free (dbm->modified);
  dbm->modified = db_timestamp ();
  if (g_sqldb_row_update (dcsock, map_sqldb, dbm))
    {
      D_FUNC_END;
      return;
    }
  g_warning ("map_update: failed to update the database..");
  D_FUNC_END;
}
/******************************************************************************
**
**  Function to delete a DB_map object from the database.  The rowid must
**  be non-zero, meaning that the item is already in the database.
**
**  When we delete a DB_map object from the database, we must also delete
**  any DB_graph entries which describe objects on this map.
**
**  When we finished, we set the rowid to zero, meaning that the item is
**  no longer in the database, and return.  The caller is responsible for
**  making a call to map_destroy to free the memory associated with the
**  DB_map block.
**
******************************************************************************/
void
map_delete (DB_map * dbm)
{
  GList        * gl;
  D_FUNC_START;
  g_return_if_fail (dbm != NULL);
  if (dbm->rowid == 0)
    {
      g_warning ("map_delete: attempt to delete a map that is marked as "
		 "deleted already.");
      D_FUNC_END;
      return;
    }

  while ((gl = dbm->DB_graphs))         /* Assignment intended */
    {
      graph_delete  ((DB_graph *) gl->data);
      graph_destroy ((DB_graph *) gl->data);
    }
  g_slist_free (dbm->DB_graphs);
  dbm->DB_graphs = NULL;
  if (g_sqldb_row_delete (dcsock, map_sqldb, dbm))
    {
      dbm->rowid = 0;
      D_FUNC_END;
      return;
    }
  g_warning ("map_delete: failed.");
  D_FUNC_END;
}
/****************************************************************************
 * map_list ()
 ***************************************************************************/
GList *
map_list (void)
{
  return g_sqldb_table_list (map_sqldb);
}
/****************************************************************************
 * map_find_by_rowid ()
 ***************************************************************************/
DB_map *
map_find_by_rowid (guint rowid)
{
  D_FUNC_START;
  g_return_if_fail (rowid != 0);
  D_FUNC_END;
  return g_sqldb_row_find (map_sqldb, "_rowid", &rowid);
}
/* EOF */

