/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * Misc ip specific utilities, currently only IPV4 is supported.
 */
#ifndef __IP_UTILS_H__
#define __IP_UTILS_H__

gulong		cidr_to_mask			(gint          bits);

gint		mask_to_cidr			(gulong        mask);

void		ip_range			(gulong        network,
						 gint          bits,
						 gulong      * range_start,
						 gulong      * range_end);

void		quad_to_parts			(const gchar * quad,
						 gint        * a,
						 gint        * b,
						 gint        * c,
						 gint        * d);

gulong		quad_to_ulong			(const gchar * quad);

gchar	      * ulong_to_sol_quad		(gulong        addr,
						 gchar       * buf,
						 gint          bufsize);

gulong		next_ip_block			(gulong        addr,
						 gint          bits);


gchar	      * host_and_netmask_to_network  	(gchar 	     * address, 
						 gchar 	     * netmask);

#endif
