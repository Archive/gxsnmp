/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The network_dialog widget is used to enter in and edit network data.
 */
#ifndef __GXSNMP_NETWORK_DIALOG_H__
#define __GXSNMP_NETWORK_DIALOG_H__

#include <gnome.h>
#include "gnome-property-dialog.h"
#include "network_panel.h"
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros
 ***************************************************************************/
#define GXSNMP_TYPE_NETWORK_DIALOG      (gxsnmp_network_dialog_get_type ())
#define GXSNMP_NETWORK_DIALOG(obj) \
        GTK_CHECK_CAST ((obj),GXSNMP_TYPE_NETWORK_DIALOG,GXsnmp_network_dialog)
#define GXSNMP_NETWORK_DIALOG_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_NETWORK_DIALOG, \
                              GXsnmp_network_dialogClass)
#define GXSNMP_IS_NETWORK_DIALOG(obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_NETWORK_DIALOG)
#define GXSNMP_IS_NETWORK_DIALOG_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_NETWORK_DIALOG)

/****************************************************************************
 * Control blocks for the widget class and for widget instances 
 ***************************************************************************/

typedef struct _GXsnmp_network_dialog              GXsnmp_network_dialog;
typedef struct _GXsnmp_network_dialogClass         GXsnmp_network_dialogClass;

struct _GXsnmp_network_dialog
{
  GnomePropertyDialog    dialog;
  
  GtkWidget      *table;
  GtkWidget      *network_panel;
  GtkWidget	* graph_panel;		   /* The graph editing panel */
  GtkWidget	* no_graph_panel;	/* Displayed when host not on map */
  GtkWidget	* graph_scroll;		/* Scroll window for graph list */
  GtkWidget	* graph_clist;		/* List of available maps */
  GtkWidget	* graph_show_button;	/* Show on selected map */
  GtkWidget	* graph_hide_button;	/* Hide on selected map */

  DB_network    *selected_network;
  DB_graph      *selected_graph;

  guint         *graph_datalets;
  guint         graph_row;
  gboolean      network_dirty;
  gboolean      graph_dirty;
  gdouble       x, y;

};

struct _GXsnmp_network_dialogClass
{
 GnomePropertyDialogClass  parent_class; 	/* Class base structure */
};

/****************************************************************************
 * Public widget functions
 ***************************************************************************/

GtkType            gxsnmp_network_dialog_get_type     (void);
GtkWidget          *gxsnmp_network_dialog_new         (DB_network  *dbn,
						       gdouble     x,
						       gdouble     y);

END_GNOME_DECLS

#endif
/* EOF */
