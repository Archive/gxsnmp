/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Host item widget
**
**  A host item is derived from a GXsnmp_map_item object
*/

#ifndef __GXSNMP_HOST_H__
#define __GXSNMP_HOST_H__

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

#include "gxsnmp_map.h"
#include "gxsnmp_map_item.h"

BEGIN_GNOME_DECLS

/*****************************************************************************
**
**  Standard widget macros
**
*****************************************************************************/

#define GXSNMP_TYPE_HOST (gxsnmp_host_get_type())

#define GXSNMP_HOST(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_HOST, GXsnmp_host)

#define GXSNMP_HOST_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_HOST, \
			      GXsnmp_hostClass)

#define GXSNMP_IS_HOST(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_HOST)

#define GXSNMP_IS_HOST_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_HOST)

/*****************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*****************************************************************************/

typedef struct _GXsnmp_host		GXsnmp_host;
typedef struct _GXsnmp_hostClass	GXsnmp_hostClass;

struct _GXsnmp_host
{
  GXsnmp_map_item	  map_item;	/* Base object */
  GnomeCanvasItem       * image;        /* The displayed image */
  GnomeCanvasItem       * text;         /* The displayed text */
  gchar                 * text_color;   /* Current text color */
}; 

struct _GXsnmp_hostClass
{
  GXsnmp_map_itemClass 	map_item_class;		/* Base class */
};

/****************************************************************************
**
**  Public widget manipulation functions
**
*****************************************************************************/

GtkType	          gxsnmp_host_get_type	        (void);

GXsnmp_map_item  *gxsnmp_host_new	        (DB_graph      * graph);

void              gxsnmp_host_set_display_name  (GXsnmp_host   * host,
						 gchar         * name);
 
void              gxsnmp_host_set_pixmap        (GXsnmp_host   * host,
						 gchar         * name);

void 		  gxsnmp_host_set_im_pixmap     (GXsnmp_host   * host,
						 GdkImlibImage * im);

void              gxsnmp_host_set_text_color    (GXsnmp_host   * host,
						 gchar         * color);
END_GNOME_DECLS
#endif /* __GXSNMP_HOST_H__ */
