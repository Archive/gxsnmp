/*
 * $Id$
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * About window code.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>

static void                   create_panel                  (void);


/*
 * Local functions
 */

static void
create_panel ()
{
  GtkWidget   *about;
  gchar       *pixmap;

  const gchar       *authors[] = {
    "Gregory McLean",
    "Jochen Friedrich",
    "John Schulien",
    NULL
  };
  pixmap = gnome_unconditional_pixmap_file ("gxsnmp/logo_h.xpm");
  about = gnome_about_new ( "GXSNMP", VERSION,
			    "(C) 1998 Gregory McLean & Jochen Friedrich",
			    (const gchar **) authors,
			    _("GXSNMP is a basic SNMP management "
			      "application that allows you to better "
			      "manage your networks."),
			    pixmap);
  gtk_widget_show (about);
  return ;
}

/*
 * Global functions
 */
void open_about_panel ()
{
  create_panel ();
}

/* EOF */



