/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Functions for dealing with DB_map objects.
 */

#ifndef __MAP_H__
#define __MAP_H__

#include "tables.h"

DB_map            *map_create           (void);
void              map_destroy           (DB_map      *dbm);
void              map_add               (DB_map      *dbm);
void              map_update            (DB_map      *dbm);
void              map_delete            (DB_map      *dbm);
GList             *map_list             (void);
DB_map            *map_find_by_rowid    (guint       rowid);

#endif

/* EOF */
