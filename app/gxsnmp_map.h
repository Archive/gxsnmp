/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**  
**  Network map widget
*/

#ifndef __GXSNMP_MAP_H__
#define __GXSNMP_MAP_H__

BEGIN_GNOME_DECLS

#include "gxsnmp_map_item.h"

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_MAP (gxsnmp_map_get_type())

#define GXSNMP_MAP(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MAP, GXsnmp_map)

#define GXSNMP_MAP_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_MAP, \
			      GXsnmp_mapClass)

#define GXSNMP_IS_MAP(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MAP)

#define GXSNMP_IS_MAP_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MAP)

/*******************************************************************************
**
**  The mouse states
**
*******************************************************************************/

typedef enum {
  MOUSE_STATE_NONE,	   /* No mouse buttons depressed */
  MOUSE_STATE_LEFTCLICK,   /* Left button depressed, x1, y1, x2, y2 stored */
  MOUSE_STATE_RUBBERBAND,  /* Left button depressed, selecting item(s) */
  MOUSE_STATE_DRAGGING,	   /* Left button depressed, dragging item(s) */
  MOUSE_STATE_MID_SCROLL   /* Drag the canvas itself around */ 
} MouseState;
typedef enum {
  MAP_STATE_NONE,
  MAP_STATE_NEED_DRAG,
  MAP_STATE_OVER_ITEM
} MapState;
/*****************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*****************************************************************************/

typedef struct _GXsnmp_mapClass   GXsnmp_mapClass;
typedef struct _GXsnmp_map 	  GXsnmp_map;

struct _GXsnmp_map
{
  GnomeCanvas       canvas;		/* The GnomeCanvas base widget */
  gchar		  * name;		/* Name of this map */

  /* The following two items should be mashed into a bitfield.. */  
  MouseState        mouse_state;        /* The state of the mouse. */
  MapState          map_state;          /* State of the map. */
  GList           * selected;           /* GList of all selected objects */
  GdkBitmap        *stipple;            /* Stipple pattern for the rubberband*/
  GnomeCanvasItem * rubberband;		/* The rubberband item, if created */
  GnomeCanvasItem * grabbed;		/* The currently grabbed item */
  gdouble	    x1, y1;		/* Point where left mouse button */
					/* was clicked and held down */
  gdouble	    x2, y2;		/* Last saved mouse location */
  gboolean          do_snapping;        /* Shall we snap the coordinates? */
  gint              width, height;      /* Width and Height of the canvas */
                                        /* area */
  gdouble           grid_x, grid_y;     /* Grid x/y size */
  DB_map	  * DB_map;		/* Pointer to DB_map structure */
  /* Autoscroll stuff */
  gint              timer_tag;
  gint              vvalue_diff, hvalue_diff;
  gint              event_last_x, event_last_y;
  
}; 

struct _GXsnmp_mapClass
{
  GnomeCanvasClass canvas_class;   	/* The GnomeCanvas base class */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkType	    gxsnmp_map_get_type		(void);
GtkWidget * gxsnmp_map_new		(void);
void	    gxsnmp_map_set_name		(GXsnmp_map 	 * map,
					 gchar		 * map_name);
gint 	    gxsnmp_map_mouse_select     (GXsnmp_map_item * item, 
					 GdkEvent        * event, 
					 gpointer          data);
void        gxsnmp_map_set_geometry     (GXsnmp_map      * map,
					 gint              width,
					 gint              height);
void 	    toggle_snap_menu_cb 	(GtkWidget       * widget, 
					 gpointer 	   data);
void        gxsnmp_map_zoom_in          (GXsnmp_map      * map);
void        gxsnmp_map_zoom_out         (GXsnmp_map      * map);

END_GNOME_DECLS
#endif /* __GXSNMP_MAP_H__ */
