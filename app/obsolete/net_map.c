/*
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Network Map panel. (New and Improved to use the gnome-canvas)
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>
#include <gdk/gdkkeysyms.h>
#include <math.h>
#include "main.h"
#include "net_map.h"
#include "gui.h"
#include "snmp_host.h"
#include "menus.h"     

#include "structs.h"
#include "protos.h"

extern gxsnmp *app_info;
/*
 * A few defines 
 */
#define MAP_DO_SNAP(p)  (GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (p), "snap_to_grid")))
/*
 * Forward declaration
 */
static void                init_map_grid        (void);
static void                delete_map_panel     (GtkWidget        *widget,
						 gpointer         data);
#if 0
static void                close_panel_cb       (GtkWidget        *widget,
						 gpointer         data);
#endif
static gint                canvas_key_press     (GnomeCanvas      *widget,
						 GdkEventKey      *e,
						 gpointer         data);
static gint                canvas_item_event    (GnomeCanvasItem  *item,
						 GdkEvent         *e,
						 gpointer         data);
static gint                canvas_event         (GtkWidget        *widget,
						 GdkEvent         *e,
						 gpointer         data);
static gint                canvas_enter_event   (GtkWidget        *widget,
						 GdkEvent         *e,
						 gpointer         data);
static gint                update_mouse_stat    (GtkWidget        *widget,
						 GdkEventMotion   *e,
						 gpointer         data);
static gint                map_link_event       (GnomeCanvasItem  *widget,
						 GdkEvent         *e,
						 gpointer         data);
static void                setup_item           (GnomeCanvasItem  *item,
						 gpointer         data);
static void                free_imlib_image     (GtkObject        *object,
						 gpointer         data);
static void                map_draw_net_line    (map_node         *node);
static void                map_update_map_link  (map_links        *mlink);
/*
 * Add label callbacks TODO..
 */
#if 0
static void         add_label_ok_cb             (GtkWidget        *widget,
						 gpointer         data);
static void         add_label_can_cb            (GtkWidget        *widget,
#endif						 gpointer         data);
/*
 * Some defines
 * Couple of modes for the canvas
 */
enum {
  ITEM_NONE     = 1 << 1,
  ITEM_DRAG     = 1 << 2,     /* dragging it around */
  ITEM_RESIZE   = 1 << 3,     /* resizing it */
  ITEM_PICK     = 1 << 4,     /* selecting it to link it */
};
/*
 * Module specific stuff
 */
/* Placement stuff shamelessly stolen from gmc */
static char *spot_array;
static int x_spots, y_spots;
static GList    *map_list = NULL;          /* List of open maps */
/*
 * Global variables needed for this panel
 */


/*
 * Local variables for the network map panel
 */
/* The currently active map_panel */

static GtkWidget      *map_panel = NULL;


/*
 * Local functions 
 */

/*
 * Function    : map_get_mode
 * Description : This will retrieve the internal mode of the map canvas
 */
int
map_get_mode (GnomeCanvas *canvas)
{
  GtkWidget   *panel;

  panel = gtk_widget_get_toplevel (GTK_WIDGET (canvas));
  if (panel)
    return GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (panel),
						 "map_mode"));
  return -1;
}
/*
 * Function    : map_set_mode
 * Descripiton : Set the mode of a map canavs
 */
void
map_set_mode (GnomeCanvas *canvas, int mode)
{
  GtkWidget   *panel;

  panel = gtk_widget_get_toplevel (GTK_WIDGET (canvas));
  if (panel)
    gtk_object_set_data (GTK_OBJECT (panel), "map_mode", 
			 GINT_TO_POINTER (mode));
}

/*
 * Idea for autoplacement 
 */
static void 
init_map_grid ()
{
  gint         size;
  GtkWidget    *canvas;
  gdouble      x_1, y_1, x_2, y_2;

  canvas = map_get_canvas_widget (NULL);
  gnome_canvas_get_scroll_region (GNOME_CANVAS (canvas), 
				  &x_1, &y_1, &x_2, &y_2);
  x_spots = x_2 / 24;
  y_spots = y_2 / 24;
  size = (x_spots * y_spots) / 8;
  spot_array = g_malloc (size + 1);
  memset (spot_array, 0, size);
  g_print ("x_spots = %d, y_spots = %d\n", 
	    x_spots, y_spots);
}
static gdouble grid_size = 20.0;   
/*
 * Function    : map_snap_to_grid
 * Description : Simple function to snap items to the grid.
 */
void
map_snap_to_grid (gdouble *x, gdouble *y)
{
  gdouble   topsnap;
  gdouble   bottomsnap;

  *x += grid_size/2.0;
  *y += grid_size/2.0;
  /* Snap the X coord */
  bottomsnap = *x - (fmod (*x, grid_size));
  topsnap    = bottomsnap + grid_size;
  if (*x > bottomsnap)
    *x = bottomsnap;
  if (*x > topsnap)
    *x = topsnap;
  /* Snap the Y coord */
  bottomsnap = *y - (fmod (*y, grid_size));
  topsnap    = bottomsnap + grid_size;
  if (*y > bottomsnap) 
    *y = bottomsnap;
  if (*y > topsnap) 
    *y = topsnap;

}
/* 
 * Function    : create_map_panel
 * Description : This function will create a new map panel, and if parent
 *               is non-null will add it to the 'map_container' widget in
 *               the parent, else it will create a panel with tool bars all
 *               all.
 */
GtkWidget *
create_map_panel (GtkWidget *parent, gchar *title)
{
  GtkWidget        *panel;
  GtkWidget        *window_frame;
  GtkWidget        *map_table;
  GtkWidget        *separator_bar;
  GtkWidget        *logo_widget;
  GtkWidget        *mouse_stat;              /* The mouse x y on the map */
  GtkWidget        *map_stat;                /* Any stat info for the map */
  GtkWidget        *canvas;
  GtkWidget        *scrolled_window;
  GtkWidget        *frame;
  GtkWidget        *led_bar;
  GtkWidget        *s_bar;
  GnomeCanvasGroup *root;
  GdkColor         active;
  GdkColor         inactive;
  gchar            *logo_pixmap_filename;

  if (title)
    panel = gnome_app_new ("GXSNMP", title);
  else
    panel = gnome_app_new ("GXSNMP", _("Network map"));
  gtk_signal_connect (GTK_OBJECT (panel), "delete_event",
		      (GtkSignalFunc) delete_map_panel,
		      NULL);

  window_frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (window_frame), 4);

  map_table = gtk_table_new (8, 7, FALSE);
  gtk_object_set_data (GTK_OBJECT (panel), "map_table", map_table);
  gtk_widget_set_name (GTK_WIDGET (map_table), "net_map_table");
  gtk_container_border_width (GTK_CONTAINER (map_table), 2);
  gtk_container_add (GTK_CONTAINER (window_frame), map_table);
  gtk_object_set_data (GTK_OBJECT (panel), "map_mode", 
		       GINT_TO_POINTER (ITEM_NONE));
  
  separator_bar = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE (map_table), separator_bar, 
		    1, 2, 3, 6,
		    GTK_FILL, GTK_FILL, 3, 0);

  /*
   * The logo
   */
  logo_pixmap_filename = gnome_unconditional_pixmap_file ("logo_v.xpm");
  logo_widget = gnome_pixmap_new_from_file (logo_pixmap_filename);
  gtk_table_attach (GTK_TABLE (map_table), logo_widget,
		    0, 1, 3, 6,
		    GTK_FILL, GTK_FILL, 0, 0);
  /*
   * The canvas for the map.
   */
  gtk_widget_push_visual (gdk_imlib_get_visual ());
  gtk_widget_push_colormap (gdk_imlib_get_colormap ());
  canvas = gnome_canvas_new ();
  gtk_widget_set_name (canvas, "network_map");
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();
  gtk_widget_set_usize (canvas, 1600, 1600);
  gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), 
				  0, 0,
				  1600, 1600);
  gtk_signal_connect (GTK_OBJECT (canvas), "key_press_event",
		      (GtkSignalFunc) canvas_key_press,
		      panel);
  gtk_signal_connect (GTK_OBJECT (canvas), "motion_notify_event",
		      (GtkSignalFunc) update_mouse_stat,
		      panel);
  gtk_signal_connect_after (GTK_OBJECT (canvas), "button_press_event",
		      (GtkSignalFunc) canvas_event,
		      panel);
  gtk_signal_connect (GTK_OBJECT (canvas), "enter_notify_event",
		      (GtkSignalFunc) canvas_enter_event,
		      panel);
  gtk_object_set_data (GTK_OBJECT (panel), "map_canvas", canvas);
  gtk_widget_set_name (GTK_WIDGET (canvas), "map_canvas");
  /*
   * Scrolled area for the canvas...
   */
  scrolled_window = gtk_scrolled_window_new 
    (GTK_LAYOUT (canvas)->hadjustment, GTK_LAYOUT (canvas)->vadjustment);
  //  gtk_frame_set_shadow_type (GTK_FRAME (scrolled_window), GTK_SHADOW_IN);
  gtk_table_attach (GTK_TABLE (map_table), scrolled_window, 
		    3, 6, 4, 5,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_widget_set_usize (scrolled_window, 600, 400);
  /*
  s_bar = gtk_hscrollbar_new (GTK_LAYOUT (canvas)->hadjustment);
  gtk_table_attach (GTK_TABLE (map_table), s_bar,
		    2, 6, 5, 6,
		    GTK_FILL, GTK_FILL, 0, 3);*/
  s_bar = gtk_hruler_new ();
  gtk_widget_set_name (GTK_WIDGET (s_bar), "net_map_ruler");
  gtk_signal_connect_object (GTK_OBJECT (canvas), "motion_notify_event",
			     (GtkSignalFunc) GTK_WIDGET_CLASS (GTK_OBJECT (s_bar)->klass)->motion_notify_event,
			     GTK_OBJECT (s_bar));
  gtk_table_attach (GTK_TABLE (map_table), s_bar,
		    3, 6, 3, 4,
		    GTK_FILL, GTK_FILL, 0, 0);
  gtk_ruler_set_range (GTK_RULER (s_bar), 0, 10, 5, 10);
  /*s_bar = gtk_vscrollbar_new (GTK_LAYOUT (canvas)->vadjustment);
  gtk_table_attach (GTK_TABLE (map_table), s_bar,
		    6, 7, 3, 5,
		    GTK_FILL, GTK_FILL, 3, 0); */
  s_bar = gtk_vruler_new ();
  gtk_widget_set_name (GTK_WIDGET (s_bar), "net_map_ruler");
  gtk_signal_connect_object (GTK_OBJECT (canvas), "motion_notify_event",
			     (GtkSignalFunc) GTK_WIDGET_CLASS (GTK_OBJECT (s_bar)->klass)->motion_notify_event,
			     GTK_OBJECT (s_bar));
  gtk_ruler_set_range (GTK_RULER (s_bar), 0, 10, 5, 10);
  gtk_table_attach (GTK_TABLE (map_table), s_bar,
		    2, 3, 4, 5,
		    GTK_FILL, GTK_FILL, 0, 0);
  gtk_container_add (GTK_CONTAINER (scrolled_window), canvas);
  root = GNOME_CANVAS_GROUP (gnome_canvas_root (GNOME_CANVAS (canvas)));
  gtk_object_set_data (GTK_OBJECT (panel), "map_canvas_root", root);
  GTK_WIDGET_SET_FLAGS (canvas, GTK_CAN_FOCUS);
  gtk_widget_grab_focus (canvas);
  /*
   * Status areas
   */
  mouse_stat = gtk_statusbar_new ();
  gtk_table_attach (GTK_TABLE (map_table), mouse_stat,
		    0, 1, 7, 8,
		    GTK_FILL, GTK_FILL, 2, 0);
  gtk_object_set_data (GTK_OBJECT (panel), "mouse_stat", mouse_stat);
  gtk_object_set_data (GTK_OBJECT (panel), "mouse_stat_id", (int *)
		       gtk_statusbar_get_context_id (GTK_STATUSBAR 
						     (mouse_stat), 
						     "mouse_stat"));
						     
  map_stat = gtk_statusbar_new ();
  gtk_table_attach (GTK_TABLE (map_table), map_stat,
		    3, 6, 7, 8,
		    GTK_FILL, GTK_FILL, 2, 0);  
  gtk_object_set_data (GTK_OBJECT (panel), "map_stat", map_stat);
  gtk_object_set_data (GTK_OBJECT (panel), "map_stat_id", (gint *)
		       gtk_statusbar_get_context_id (GTK_STATUSBAR 
						     (map_stat), 
						     "map_stat"));
  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
  gtk_table_attach (GTK_TABLE (map_table), frame, 
		    1, 3, 7, 8,
		    GTK_FILL, GTK_FILL, 2, 0);
  led_bar = gtk_led_new ();
  gdk_color_parse ("#00F100", &active);
  gdk_color_parse ("#008C00", &inactive);
  gtk_led_set_colors (GTK_LED (led_bar), &active, &inactive);
  gtk_object_set_data (GTK_OBJECT (panel), "act_led", led_bar);
  gtk_container_add (GTK_CONTAINER (frame), led_bar);

  /* Some state info */
  gtk_object_set_data (GTK_OBJECT (panel), "snap_to_grid",
		       GINT_TO_POINTER (TRUE));

  /*
   * Set the gnome_app contents...
   */
  gnome_app_set_contents (GNOME_APP (panel), window_frame);

/*
**  Create the main application menu and toolbar.  See main_menu.c
*/

/*  create_main_menu (panel);
  create_main_toolbar (panel);
*/
  gtk_widget_show_all (panel);
  return panel;
}


/*
 * The Snap-To-Grid callback handler.  Called from main_menu.c
 */
#if 0
void 
toggle_snap_menu_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *parent;

  parent = map_get_active_map ();
  if (parent)
    {
      if (GTK_CHECK_MENU_ITEM (widget)->active)
	  gtk_object_set_data (GTK_OBJECT (parent), "snap_to_grid", 
			       GINT_TO_POINTER (TRUE));
      else
	  gtk_object_set_data (GTK_OBJECT (parent), "snap_to_grid",
			       GINT_TO_POINTER (FALSE));
    }
}
#endif

/*
 * Window Manager delete signal
 */
static void
delete_map_panel (GtkWidget *widget, gpointer data)
{

}

/*
 * Close button callback
 */
/*    FIXME
static void
close_panel_cb (GtkWidget *widget, gpointer data)
{
  destroy_map_panel ();
}
*/
/*
 * Keypress on the canvas
 */
static gint
canvas_key_press (GnomeCanvas *widget, GdkEventKey *e, gpointer data)
{
  /* -- DEAD -- */
}

/*
 * Events on the items in the canvas
 */
static gint
canvas_item_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
  static double     x, y;
  double            x1, y_1;
  GdkCursor         *fleur;
  GtkWidget         *parent;
  int               mode;
  gint              c_width, c_height;
  hosts             *host;
  GnomeCanvas       *canvas;
  net_entry         *network;
  map_node          *node;
#ifdef HAVE_SQL
  static gboolean    sql_update = FALSE;
#endif

  node = (map_node *)data;
  canvas = item->canvas;
  parent = (GTK_WIDGET (canvas)->parent) ? gtk_widget_get_toplevel (GTK_WIDGET (canvas)) : GTK_WIDGET (canvas);
  mode = map_get_mode (canvas);
  c_width = GTK_WIDGET(canvas)->allocation.width;
  c_height = GTK_WIDGET(canvas)->allocation.height;
  switch (event->type) 
    {
    case GDK_BUTTON_PRESS:
      x = event->button.x;
      y = event->button.y;
      switch (event->button.button)
	{
	case 1:
	  if (mode == ITEM_PICK)
	    {
	      map_select_node (NULL, node);
	    }
	  else
	    {
	      fleur = gdk_cursor_new (GDK_FLEUR);
	      gnome_canvas_item_grab (item,
				      GDK_POINTER_MOTION_MASK |
				      GDK_BUTTON_RELEASE_MASK,
				      fleur,
				      event->button.time);
	      gdk_cursor_destroy (fleur);
	      map_select_node (NULL, node);
	      map_set_mode (canvas, ITEM_DRAG);
	    }
	  break;
	case 2:
	  fleur = gdk_cursor_new (GDK_FLEUR);
	  gnome_canvas_item_grab (item,
				  GDK_POINTER_MOTION_MASK |
				  GDK_BUTTON_RELEASE_MASK,
				  fleur,
				  event->button.time);
	  gdk_cursor_destroy (fleur);
	  map_set_mode (canvas, ITEM_RESIZE);
	  break;
	case 3:
	  switch (node->mn_type)
	    {
	    case MAP_NODE_HOST:
	      host = (hosts *)node->mn_data;
	      gtk_object_set_data (GTK_OBJECT 
				   (GNOME_CANVAS_ITEM (item)->canvas),
				   "selected_host", host);
/* OBSOLETE host_menu_right_popup ((int)&event->button, event->button.time); */
	      gtk_signal_emit_stop_by_name (GTK_OBJECT (canvas), 
					    "button_press_event");
	      return TRUE;
	      break;
	    case MAP_NODE_NETWORK:
	      network = (net_entry *)node->mn_data;
/* OBSOLETE do_network_popup_menu ((GdkEventButton *)event, node->mn_data); */
	      gtk_signal_emit_stop_by_name (GTK_OBJECT (canvas),
					    "button_press_event");
	      return TRUE;
	      break;
	    default:
	      break;
	    }
	  break;
	default:
	  break;
	}
      break;
      
    case GDK_MOTION_NOTIFY:
      if ( (mode == ITEM_DRAG) && (event->motion.state & GDK_BUTTON1_MASK))
	{
#ifdef HAVE_SQL
	  sql_update = TRUE;
#endif
	  if (MAP_DO_SNAP (map_get_active_map()))
	    map_snap_to_grid (&event->motion.x, &event->motion.y);
	  gnome_canvas_item_move (item,  event->motion.x  - x, 
				  event->motion.y - y);
          gnome_canvas_item_i2w(item,  &x, &y);
	  map_move_node (node, x, y);
	  x = event->motion.x;
	  y = event->motion.y;
	}
      else
	if ( (mode == ITEM_RESIZE)  
	     && (event->motion.state & GDK_BUTTON1_MASK))
	  {
	    g_print ("Do item resize\n");
	  }
      break;
    case GDK_BUTTON_RELEASE:
      gnome_canvas_item_ungrab (item, event->button.time);
      if (mode != ITEM_PICK)
	{
	  map_unselect_node (NULL, node);
#ifdef HAVE_SQL
	  if (sql_update)
	    {
	      sql_update = FALSE;
	      /* FIXME: 
              map_db_update_entry (&app_info->host_db, "map", node);
	      */
	    }
	  
#endif
	  map_set_mode (canvas, ITEM_NONE);
	}
      break;
    case GDK_KEY_PRESS:
      /* g_print ("Item event got a key press..\n"); */
      break;
    case GDK_KEY_RELEASE:
      /* g_print ("Item event got a key release...\n"); */
    case GDK_ENTER_NOTIFY:
      /* g_print ("Entered an item.\n"); */
      break;
    default:
      break;
    }
  return FALSE;
}

/*
 * General canvas events _not_ over a node item.
 */
static gint 
canvas_event (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  /* -- DEAD --- */
}

/*
 * This is done so the key bindings work consistantly. (without having to 
 * click on the canvas all the time.. Or after moving an item around
 * The canvas looses focus. This appears to work though there may be some
 * ugly side effects.
 */
static gint 
canvas_enter_event (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  /* Set the active panel */
  map_panel = (widget->parent) ? gtk_widget_get_toplevel (widget): widget;
  /* set the keyboard focus back on the canvas */
  gtk_widget_grab_focus (widget);
  return FALSE;
}

static gint
update_mouse_stat (GtkWidget *widget, GdkEventMotion *e, gpointer data)
{
  /* --- DEAD ---- */
}

static gint
map_link_event (GnomeCanvasItem *item, GdkEvent *e, gpointer data)
{
  static gdouble      x,y;
  static int          mode = 0;
  static gint         ix, iy;
  GdkCursor           *fleur;
  GnomeCanvasPoints   *p;
  GtkArg              my_arg;
  gint                link_length;
  gint                best_d2, d2, i, best;
  
  switch (e->type)
    {
    case GDK_BUTTON_PRESS:
      switch (e->button.button)
	{
	case 1:
	  x = e->button.x;
	  y = e->button.y;
	  /* Get the points closest to the click */
	  /* Thanks to raph levein :) */
	  my_arg.name = "points";
	  gtk_object_getv (GTK_OBJECT (item), 1, &my_arg);
	  p = GTK_VALUE_POINTER (my_arg);
	  best_d2 = (x - p->coords[0]) * (x - p->coords[0]) +
	    (y - p->coords[1]) * (y - p->coords[1]);
	  best = 0;
	  for (i = 1; i < p->num_points; i++)
	    {
	      d2 = (x - p->coords[i * 2]) * (x - p->coords[i * 2]) +
		(y - p->coords[i * 2 + 1]) * (y - p->coords[i * 2 + 1]);
	      if (d2 < best_d2)
		{
		  best_d2 = d2;
		  best = i;
		}
	    }
	  ix = best * 2;
	  iy = best * 2 + 1;
	  fleur = gdk_cursor_new (GDK_FLEUR);
	  gnome_canvas_item_grab (item,
				  GDK_POINTER_MOTION_MASK |
				  GDK_BUTTON_RELEASE_MASK,
				  fleur,
				  e->button.time);
	  gdk_cursor_destroy (fleur);
	  mode = mode ^ITEM_DRAG;
	  break;
	case 2:
	  g_print ("Add a point to the link?\n");
	  break;
	case 3:
	  g_print ("do a link menu?\n");
	  gtk_signal_emit_stop_by_name (GTK_OBJECT 
					(GNOME_CANVAS_ITEM (item)->canvas),
					"button_press_event");
	default:
	break;
	}
    case GDK_MOTION_NOTIFY:
      if ( (mode & ITEM_DRAG) && (e->motion.state & GDK_BUTTON1_MASK))
	{
	  my_arg.name = "points";
	  gtk_object_getv (GTK_OBJECT (item), 1, &my_arg);
	  p = GTK_VALUE_POINTER (my_arg);
	  p->coords[ix] = e->motion.x;
	  p->coords[iy] = e->motion.y;
	  gnome_canvas_item_set (item, "points", p, NULL);
	}
      break;
    case GDK_BUTTON_RELEASE:
      gnome_canvas_item_ungrab (item, e->button.time);
      if (mode & ITEM_DRAG)
	mode = mode ^ ITEM_DRAG;
      break;
    case GDK_ENTER_NOTIFY:
      break;
    default:
      break;
    }
  return FALSE;
}


/*
**  Node link callback handler -- called from main_menu.c
*/

void
node_link_cb (GtkWidget *widget, gpointer data)
{
  GnomeCanvas  *canvas;
  GtkWidget    *parent;
  
  parent = (widget->parent) ? gtk_widget_get_toplevel (widget) : widget;
  canvas = GNOME_CANVAS (get_widget (parent, "map_canvas"));
  map_set_mode (canvas, ITEM_PICK);
  gtk_object_set_data (GTK_OBJECT (map_panel), "number_selected", (int *)0);
  map_update_stat (_("Select the first node to link from."));
}

void
open_map_panel ()
{

  if (map_panel)
    {
      if (!GTK_WIDGET_VISIBLE (map_panel))
 	gtk_widget_show (map_panel);
      return;
    }
  else
    {
      map_panel = create_map_panel (NULL, NULL);      /* open the intial map */
      init_map_grid ();
      reset_map_panel ();
    }
}

/*
 * Close/destroy
 */
void
destroy_map_panel ()
{
  if (map_panel)
    {
      gtk_widget_destroy (map_panel);
      map_panel = NULL;
    }
}

/*
 * Hide
 */
void
hide_map_panel ()
{
  g_return_if_fail (map_panel != NULL);

  if (GTK_WIDGET_VISIBLE (map_panel))
    gtk_widget_hide (map_panel);
}

/*
 * reset
 */
void 
reset_map_panel ()
{
  g_return_if_fail (map_panel != NULL);
}

static void
setup_item (GnomeCanvasItem *item, gpointer data)
{
  g_return_if_fail (item != NULL);

  gtk_signal_connect (GTK_OBJECT (item), "event",
		      (GtkSignalFunc) canvas_item_event,
		      data);
}
static void
free_imlib_image (GtkObject *object, gpointer data)
{
  gdk_imlib_destroy_image (data);
}

void
host_pixmap (GnomeCanvasGroup *group, gchar *pix_filename, double x, double y)
{
#if 0    /* Depreciated */

#endif
}
/*
 * Draw the actual network line. 
 * TODO: Update to represent diffrent network types and speeds when 
 *       we get that far.
 */
static void
map_draw_net_line (map_node *node)
{
  GnomeCanvasPoints   *points;
  GnomeCanvasItem     *item;

  points = gnome_canvas_points_new (2);
  points->coords[0] = -90.0;
  points->coords[1] = -2.0;
  points->coords[2] = 90.0;
  points->coords[3] = -2.0;

  item = gnome_canvas_item_new (node->mn_cgroup,
				gnome_canvas_line_get_type (),
				"points", points,
				"fill_color", "#00AA00",
				"width_units", 2.0,
				NULL);
  gtk_object_set_data (GTK_OBJECT (node->mn_cgroup), "line_item", item);
  gnome_canvas_points_free (points);
}

/*
 * Internal Function : map_update_map_link
 * Description       : This function is resposnible for updating any node
 *                     links on the map when nodes are dragged around.
 */
static void
map_update_map_link (map_links *mlink)
{
  GnomeCanvasPoints    *points;
  GnomeCanvasPoints    *p;
  GtkArg               *my_arg;
  double             left_edge, right_edge, top_edge, bottom_edge;
  double             x1, y_1, x2, y2;

  my_arg = g_new0 (GtkArg, 2);
  my_arg[0].name = "points";
  gtk_object_getv (GTK_OBJECT (mlink->ml_item), 1, my_arg);
  p = GTK_VALUE_POINTER (my_arg[0]);
  points = gnome_canvas_points_new (p->num_points);
  gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (mlink->ml_to), 
				&left_edge, &top_edge, 
				&right_edge, &bottom_edge);
  gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (mlink->ml_from), 
				&x1, &y_1, &x2, &y2);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (mlink->ml_from), &x1, &y_1);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (mlink->ml_from), &x2, &y2);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (mlink->ml_to), &left_edge, &top_edge);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (mlink->ml_to), &right_edge, &bottom_edge);
  my_arg[0].name = "x";
  my_arg[1].name = "y";
  gtk_object_getv (GTK_OBJECT (mlink->ml_from), 2, my_arg);
  points->coords[0] = GTK_VALUE_DOUBLE (my_arg[0]);
  points->coords[1] = (y_1 > top_edge) ? y_1 : y2;
  points->coords[2] = (x1 < left_edge) ? left_edge : right_edge;
  gtk_object_getv (GTK_OBJECT (mlink->ml_to), 2, my_arg);
  points->coords[3] = GTK_VALUE_DOUBLE (my_arg[1]);
  gnome_canvas_item_set (mlink->ml_item, 
			 "points", points,
			 NULL);
  g_free (my_arg);
  gnome_canvas_points_free (points);

}
/*
 * Function    : map_add_node
 * Description : This function will do the actual placement and creation
 *               of a new node on the map. This function will also connect
 *               up any host type nodes to the respective network if its
 *               added in the map. NOTE:: When completeing the network
 *               functions and reloading from the database. Load networks
 *               first.
 */
gboolean
map_add_node (NodeType type, gpointer node_data)
{
  GnomeCanvasGroup   *root;
  map_node           *new_node;        /* The node we are adding */
  map_node           *old_node;        /* The node to connect to if any */
  hosts              *host_node;
  net_entry          *net_node;
  old_node = NULL;
  new_node = NULL;

  root = (GnomeCanvasGroup *)gtk_object_get_data (GTK_OBJECT (map_panel),
						  "map_canvas_root");

  if (root)
    {
      new_node = (map_node *)g_malloc (sizeof (map_node));
      new_node->mn_cgroup = GNOME_CANVAS_GROUP (gnome_canvas_item_new 
						(root,
						 gnome_canvas_group_get_type(),
						 NULL));
      new_node->mn_data = node_data;
      new_node->mn_type = type;
      new_node->mn_links = NULL;
      setup_item (GNOME_CANVAS_ITEM (new_node->mn_cgroup), new_node); 
      switch (type)
	{
	case MAP_NODE_HOST:
	  host_node = (hosts *)node_data;
	  host_node->hl_mnode = new_node;
	  host_pixmap (new_node->mn_cgroup, host_node->pixmap_file,
		       0.0, 0.0);
	  map_set_node_label (new_node, host_node->hl_disp, "firebrick");
	  if ( (host_node->hl_flags & GX_AUTOPLACEMENT) &&
	       (host_node->hl_location.x == 0) &&
	       (host_node->hl_location.y == 0) )
	    {
	      gnome_canvas_item_set (GNOME_CANVAS_ITEM (new_node->mn_cgroup),
				     "x", 90.0,
				     "y", 90.0,
				     NULL);

	      /* Auto place it? */
	    }
	  else
	    {
	      if (MAP_DO_SNAP (map_panel))
		map_snap_to_grid (&host_node->hl_location.x, 
				  &host_node->hl_location.y);
	      gnome_canvas_item_set (GNOME_CANVAS_ITEM (new_node->mn_cgroup),
				     "x", host_node->hl_location.x,
				     "y", host_node->hl_location.y,
				     NULL);

	    }
	  if (host_node->hl_flags & GX_AUTOCONNECT)
	    {
/*
	      GList * gl;

	      gl = network_db_find_by_interface_address (host_node);
	      if (gl)       
		{
		  DB_network * dbn;
		  dbn = (DB_network *) gl->data; 
		  old_node = dbn->map_item;
		  map_link_nodes (dbn->map_item, new_node);
		}
*/
	    }
	  break;
	case MAP_NODE_NETWORK:
	  net_node   = (net_entry *)node_data;
	  new_node->mn_name = g_strdup (net_node->nl_name);
	  net_node->nl_mnode = new_node;
	  if (MAP_DO_SNAP (map_get_active_map()))
	    map_snap_to_grid (&net_node->nl_location.x,
			      &net_node->nl_location.y);
	  gnome_canvas_item_set (GNOME_CANVAS_ITEM (new_node->mn_cgroup),
				 "x", net_node->nl_location.x,
				 "y", net_node->nl_location.y,
				 NULL);

	  map_draw_net_line (new_node);
	  map_set_node_label (new_node, net_node->nl_name, "red");
	  break;
	case MAP_NODE_LINK:
	  g_print ("Add a link node to the map.\n");
	  break;
	case MAP_LABEL:
	  break;
	default:
	  break;
	}
      return TRUE;
    }
  return FALSE;
}
/*
 * Function   : map_add_host 
 * Description: This will add a host to the network map at a given x/y 
 *              position. This is just a convience function for the
 *              real node add function.
 * Arguments  : host  -- The hosts structure of the host to add.
 *              x     -- The X position on the map to add it to.
 *              y     -- The Y position on the map to add it to.
 * Returns    : TRUE  -- Host was suscessfully added to the map.
 *              FALSE -- Host was not added to the map. Check app_errno 
 *                       for further info.
 */
gboolean
map_add_host (hosts *host, gdouble x, gdouble y)
{
  g_return_val_if_fail (map_panel != NULL, FALSE);

  host->hl_location.x = x;
  host->hl_location.y = y;
  map_add_node (MAP_NODE_HOST, host);
  return TRUE;
}

/* Function    : map_del_node
 * Description :
 */

/*
 * Function    : map_link_nodes
 * Description : This function will create the link between two nodes
 *               on the map. See the map_links structure in structs.h
 *               to examine what constitutes a link in the context of the 
 *               map.
 */
void
map_link_nodes (map_node *node1, map_node *node2)
{
  /*
   * node2  == the node to connect to. 
   * node1  == the node to connect FROM.
   */
  GnomeCanvasGroup   *to_group;
  GnomeCanvasGroup   *from_group;
  GnomeCanvasItem    *link_item;
  GnomeCanvasPoints  *points;
  GtkArg             *my_arg;
  map_links          *map_link;
  double             left_edge, right_edge, top_edge, bottom_edge;
  double             x1, y_1, x2, y2;
  
  g_return_if_fail (node1 != NULL);
  g_return_if_fail (node2 != NULL);
  /* Both host/other nodes connect in any order */
  if ( (node2->mn_type == MAP_NODE_HOST) &&
       (node1->mn_type == MAP_NODE_HOST) )
    {
      to_group   = node2->mn_cgroup;
      from_group = node1->mn_cgroup;
    }
  else
    {
      /* Always connect _to_ a network */
      to_group = (node2->mn_type == MAP_NODE_NETWORK) ? node2->mn_cgroup :
	                                                node1->mn_cgroup;
      /* From a host */
      from_group = (node1->mn_type == MAP_NODE_HOST) ? node1->mn_cgroup :
	                                               node2->mn_cgroup;
    }
  map_link = (map_links *)g_malloc (sizeof (map_links));
  gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (to_group), 
				&left_edge, &top_edge,
				&right_edge, &bottom_edge);
  gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (from_group), 
				&x1, &y_1, &x2, &y2);
  my_arg = g_new0 (GtkArg, 2);
  my_arg[0].name    = "x";
  my_arg[1].name    = "y";
  gtk_object_getv (GTK_OBJECT (from_group), 2, my_arg);
  points = gnome_canvas_points_new (2);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (from_group), &x1, &y_1);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (from_group), &x2, &y2);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (to_group), &left_edge, &top_edge);
  gnome_canvas_item_i2w (GNOME_CANVAS_ITEM (to_group), &right_edge, &bottom_edge);
  points->coords[0] = GTK_VALUE_DOUBLE (my_arg[0]);
  points->coords[1] = (y_1 > top_edge) ? y_1 : y2;
  points->coords[2] = (x1 < left_edge) ?  left_edge : right_edge;
  gtk_object_getv (GTK_OBJECT (to_group), 2, my_arg);
  points->coords[3] = GTK_VALUE_DOUBLE (my_arg[1]);

  link_item = gnome_canvas_item_new (GNOME_CANVAS_GROUP
				 (GNOME_CANVAS_ITEM (to_group)->canvas->root),
				  gnome_canvas_line_get_type (),
				  "points", points,
				  "fill_color", "black",
				  "width_units", 0.0,
				  NULL);
  gtk_signal_connect (GTK_OBJECT (link_item), "event",
		      (GtkSignalFunc) map_link_event,
		      NULL);
  map_link->ml_from = node1->mn_cgroup;
  map_link->ml_to   = node2->mn_cgroup;
  map_link->ml_item = link_item;
  node1->mn_links   = g_list_append (node1->mn_links, map_link);
  map_link->ml_from = node2->mn_cgroup;
  map_link->ml_to   = node1->mn_cgroup;
  node2->mn_links   = g_list_append (node2->mn_links, map_link);
  g_free (my_arg);
  gnome_canvas_points_free (points);
}

/*
 * Function   : map_del_host
 * Description: This function will delete a host from the map.
 * Arguments  : host  -- The hosts structure describing the host to 
 *                       remove from the map.
 * Returns    : TRUE  -- Host was deleted from the map.
 *              FALSE -- Host was not deleted from the map. Check app_errno
 *                       for further info.
 */
gboolean
map_del_host (hosts *host)
{
  map_node    *node;

  g_return_val_if_fail (host != NULL, FALSE);
  node = host->hl_mnode;
  gtk_object_destroy (GTK_OBJECT (node->mn_cgroup));
  g_free (node);
  return FALSE;
}

/*
 * Function   : map_add_network
 * Description: This function shall add a network to the map at a given
 *              x/y position. This is just a convience wrapper for the
 *              real node add function.
 * Arguments  : network  -- The network structure of the network to add
 *                          to the map.
 *              X        -- The X position on the map.
 *              Y        -- The Y position on the map.
 * Returns    : TRUE     -- Network was added to the map.
 *              FALSE    -- Network was not added to the map. Check app_errno
 *                          for further info.
 *              
 */
gboolean
map_add_network (net_entry *network, gdouble x, gdouble y)
{
  g_return_val_if_fail (map_panel != NULL, FALSE);

  network->nl_location.x = x;
  network->nl_location.y = y;
  map_add_node (MAP_NODE_NETWORK, network);
  return TRUE;
}

/*
 * Function   : map_del_network
 * Description: This function shall delete a network from the map's display.
 * Arguments  : network  -- The network structure of the network to delete
 *                          from the map.
 * Returns    : TRUE     -- Network was deleted from the map.
 *              FALSE    -- Network was not deleted from the map. Check
 *                          app_errno for further info.
 */
gboolean
map_del_network (net_entry *network)
{
  map_node   *node;

  g_return_val_if_fail (network != NULL, FALSE);
  node = network->nl_mnode;
  gtk_object_destroy (GTK_OBJECT (node->mn_cgroup));
  g_free (node);
  return FALSE;
}

/*
 * Function    : map_set_node_label
 * Description : This function will change a given nodes label on the map.
 * Arguments   : node  -- The node pointer of the node.
 *               text  -- A string pointer to the new label text.
 *               color -- The color name to make this text.
 * Returns     : Nothing.
 */
void
map_set_node_label (map_node *node, gchar *text, gchar *color)
{
  GnomeCanvasItem   *item;

  if ( (item = gtk_object_get_data (GTK_OBJECT (node->mn_cgroup),
				    "text_item")) )
    {
      gnome_canvas_item_set (item, "text", text,
			     "fill_color", color,
			     NULL);
      return;
    }
	
  item = gnome_canvas_item_new (node->mn_cgroup,
				gnome_canvas_text_get_type (),
				"text", text,
				"x", 0.0,
				"y", 0.0,
				"fill_color", color,
				"anchor", GTK_ANCHOR_N,
				NULL);
  gtk_object_set_data (GTK_OBJECT (node->mn_cgroup), "text_item", item);

}

/*
 * Function    : map_set_node_label_color 
 * Description : This will change the color of a node's label.
 * Arguments   : node    -- A map_node pointer to the node in question.
 *               color   -- A GdkColor pointer to a previously allocated color.
 *               cl_name -- A gchar pointer to a color name to allocated and
 *                          set on this node.
 * Returns     : Nothing.
 * Notes       : Only pass in either the color or cl_name. Passing both will
 *               only waste your time. It will pick the color pointer over
 *               allocating it internally.
 */
void
map_set_node_label_color (map_node *node, GdkColor *color, gchar *cl_name)
{
  GnomeCanvasItem   *item;

  g_return_if_fail (node != NULL);
  g_return_if_fail (GTK_IS_OBJECT (node->mn_cgroup));
  if ((item = gtk_object_get_data (GTK_OBJECT (node->mn_cgroup), "text_item")))
    {
      if (color)
	gnome_canvas_item_set (item, "fill_color_gdk", color, NULL);
      else
	gnome_canvas_item_set (item, "fill_color", cl_name, NULL);
    }
}
/*
 * Function    : map_zoom_in
 * Description : This function will zoom the map in.
 * Arguments   : canvas -- The GNOME canvas pointer of the map.
 * Returns     : Nothing.
 */
void
map_zoom_in (GnomeCanvas *canvas)
{
  double pix = canvas->pixels_per_unit;

  if (pix < 10.0)
    {
      pix += 0.5;
      gnome_canvas_set_pixels_per_unit (canvas, pix);
    }
}
/*
 * Function    : map_zoom_out
 * Description : This function will zoom the map out.
 * Arguments   : canvas -- The GNOME canvas pointer of the map.
 * Returns     : Nothing.
 */
void
map_zoom_out (GnomeCanvas *canvas)
{
  gdouble pix;

  g_return_if_fail (canvas != NULL);
  pix =  canvas->pixels_per_unit;
  if (pix > 2.0)
    {
      pix -= 0.5;
      gnome_canvas_set_pixels_per_unit (canvas, pix);
    }
}

/*
 * Function    : map_highlight_host
 * Description : This function will highlight a given host on the map.
 * Arguments   : host -- The hosts entry structure pointer to the host.
 * Returns     ; None.
 */
void
map_highlight_host (hosts *host)
{
  GnomeCanvasItem  *item;
  GtkArg           my_arg;
  gboolean         is_active;
  GdkColor         *color;

  g_return_if_fail (host != NULL);

  item = gtk_object_get_data (GTK_OBJECT (host->hl_mnode), "text_item");
  if (item)
    {
      is_active = (gboolean)gtk_object_get_data (GTK_OBJECT (item), 
						   "is_active");
      if (!is_active)
	{
	  my_arg.name = "fill_color_gdk";
	  gtk_object_getv (GTK_OBJECT (item), 1, &my_arg);
	  color = GTK_VALUE_BOXED (my_arg);
	  gtk_object_set_data (GTK_OBJECT (item), "start_color",
			       (GdkColor *)color);
	  gnome_canvas_item_set (item,
				 "fill_color", "blue",
				 NULL);
	  gtk_object_set_data (GTK_OBJECT (item), "is_active", 
			       (gboolean *)TRUE);
	}
    }
}

/*
 * Function    : map_unhighlight_host
 * Description : This will return a given host on the map to its un highlit
 *               look.
 * Arguments   : host    -- The hosts entry structure pointer of the host 
 *                          highlight.
 * Returns     : None.
 */
void
map_unhighlight_host (hosts *host)
{
  GnomeCanvasItem   *item;
  GdkColor          *color;

  g_return_if_fail (host != NULL);

  item = gtk_object_get_data (GTK_OBJECT (host->hl_mnode), "text_item");
  if (item)
    {
      color = (GdkColor *)gtk_object_get_data (GTK_OBJECT (item), 
					      "start_color");
      gnome_canvas_item_set (item,
			     "fill_color_gdk", color,
			     NULL);
      gtk_object_remove_data (GTK_OBJECT (item), "start_color");
      gtk_object_remove_data (GTK_OBJECT (item), "is_active");
    }
}

/*
 * Function    : map_get_active_map 
 * Description : Will return the GtkWidget pointer to the active map panel,
 *               NULL is returned on any failure.
 * Arguments   : None
 * Returns     : The GtkWidget pointer the the active panel, or NULL on failure
 */
GtkWidget *
map_get_active_map ()
{
  /* FIXME : Needs to be written but for now just return the global map_panel
   */
  return map_panel;
}

/*
 * Function    : map_get_canvas_widget 
 * Description : This function will return the canvas widget of the map.
 * Arguments   : panel   -- The GtkWidget pointer to the panel you want
 *                          the canvas widget from. NULL will get the
 *                          active map's canvas.
 * Returns     : canvas gtk widget pointer or null if the map has been 
 *               destroyed.
 */
GtkWidget *
map_get_canvas_widget (GtkWidget *panel)
{
  if (panel)
    return (GtkWidget *)get_widget (panel, "map_canvas");
  else
    return (GtkWidget *)get_widget (map_get_active_map (), "map_canvas");
}

/*
 * Function    : map_get_selected_host 
 * Description : This function will return the hosts pointer to the selected
 *               host.
 * Arguments   : panel   -- The panel of the active map, passing NULL will give
 *                          you the active host on the active panel.
 * Returns     : The hosts entry structure of the selected host or, 
 *               NULL on failure. This will return the last selected
 *               host.
 */
hosts *
map_get_selected_host (GtkWidget *panel)
{
  GtkWidget   *canvas;
  
  if (panel)
    canvas = map_get_canvas_widget (panel);
  else
    canvas = map_get_canvas_widget (map_get_active_map());
  return (hosts *)(gtk_object_get_data (GTK_OBJECT (canvas), "selected_host"));
}
/*
 * Function    : map_get_selected_node 
 * Description : This will return the pointer to the currently selected node
 *               on the active map.
 */
map_node *
map_get_selected_node (GtkWidget *panel)
{
  GtkWidget  *parent;
  
  parent = (panel != NULL) ? panel : map_get_active_map ();
  
  return (map_node *)gtk_object_get_data (GTK_OBJECT (parent),
					  "selected_node");
}

/*
 * Function    : map_update_stat 
 * Description : This will update the status in the map_stat area,
 * Arguments   : varargs format.
 * Returns     : Nothing.
 */
void
map_update_stat (gchar *msg, ...)
{
  va_list   ap;
  gchar     buf[2048];
  GtkWidget *stat_bar;
  gint      statusid;

  va_start (ap, msg);
  vsnprintf (buf, sizeof (buf), msg, ap);
  va_end (ap);
  
  stat_bar = GTK_WIDGET (get_widget (map_panel, "map_stat"));
  if (stat_bar)
    {
      
      statusid = gtk_statusbar_get_context_id (GTK_STATUSBAR (stat_bar), 
					       "map_stat");
      gtk_statusbar_pop (GTK_STATUSBAR (stat_bar), statusid);
      gtk_statusbar_push (GTK_STATUSBAR (stat_bar), statusid, buf);
    }
}
/*
 * Function    : map_select_item 
 * Description : This function will draw a select box around a given item.
 *               A secondary function of this is that it handles the manual
 *               linking of nodes.
 */
void
map_select_node (GtkWidget *panel, map_node *node)
{
  map_node           *node1;
  GtkWidget          *parent;
  int                num_selected;
  double             x1,y_1,x2,y2;
  GnomeCanvasItem    *outline;
  GnomeCanvas        *canvas;
  
  canvas = GNOME_CANVAS (GNOME_CANVAS_ITEM (node->mn_cgroup)->canvas);
  parent = (panel != NULL) ? panel : map_get_active_map ();
  gtk_object_set_data (GTK_OBJECT (parent), "selected_node", node);
  if (map_get_mode (canvas) == ITEM_PICK)
    {
      num_selected = GPOINTER_TO_INT(gtk_object_get_data 
				     (GTK_OBJECT (parent), 
				      "number_selected"));
      switch (num_selected)
	{
	case 0:
	  gtk_object_set_data (GTK_OBJECT (parent), "node_1", node);
	  map_update_stat (_("Select the second node to link to."));
	  num_selected++;
	  gtk_object_set_data (GTK_OBJECT (parent), "number_selected",
			       GINT_TO_POINTER (num_selected));
	  break;
	case 1:
	  node1 = (map_node *)gtk_object_get_data (GTK_OBJECT (parent), 
						   "node_1");
	  if (node1)
	    {
	      map_link_nodes (node1, node);
	      map_unselect_node (NULL, node1);
	      map_unselect_node (NULL, node);
	      gtk_object_remove_data (GTK_OBJECT (parent), "node_1");
	      gtk_object_remove_data (GTK_OBJECT (parent), 
				      "number_selected");
	      map_update_stat (_("Nodes linked."));
	      map_set_mode (canvas, ITEM_NONE);
	    }
	  break;
	default:
	  g_print ("num_selected %d",num_selected);
	  break;
	}
    }
  /*
   * Not picking a node. Selecting it for some other purpose.
   * Anyway draw a box around the node.
   */
  if (node->mn_cgroup)
    {
      x1 = y_1 = x2 = y2 = 0.0;
      gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (node->mn_cgroup), 
				    &x1, &y_1, &x2, &y2);

      x1 -= 5.0;
      y_1 -= 5.0;
      x2 += 5.0;
      y2 += 5.0;
      if (!(gtk_object_get_data (GTK_OBJECT (node->mn_cgroup), "outline")))
	{
	  outline = gnome_canvas_item_new (GNOME_CANVAS_GROUP (node->mn_cgroup),
					   gnome_canvas_rect_get_type (),
					   "x1", x1,
					   "y1", y_1,
					   "x2", x2,
					   "y2", y2,
					   "outline_color", "black",
					   "width_units", 0.0,
					   NULL);
	  gtk_object_set_data (GTK_OBJECT (node->mn_cgroup), "outline", 
			       outline);
	}
    }
}

/*
 * Function    : map_unselect_node
 * Description : This function will unselect a given node. (Much simpler then
 *               that mess for selecting.
 */
void 
map_unselect_node (GtkWidget *panel, map_node *node)
{
  GnomeCanvasItem *item;
  GtkWidget       *parent;
  
  parent = (panel != NULL) ? panel : map_get_active_map ();
  
  item = gtk_object_get_data (GTK_OBJECT (node->mn_cgroup), "outline");
  if (item)
    gtk_object_destroy (GTK_OBJECT (item));
  gtk_object_remove_data (GTK_OBJECT (node->mn_cgroup), "outline");
}
/*
 * Function    : map_move_node 
 * Description :
 */
void
map_move_node (map_node *node, double x, double y)
{
  hosts            *host;
  net_entry        *network;
  GList            *node_links;
  
  network = NULL;
  host = NULL;
  switch (node->mn_type)
    {
    case MAP_NODE_HOST:
      host = (hosts *)node->mn_data;
      host->hl_location.x = x;
      host->hl_location.y = y;
      break;
    case MAP_NODE_NETWORK:
      network = (net_entry *)node->mn_data;
      network->nl_location.x = x;
      network->nl_location.y = y;
      break;
    default:
      break;
    }
  if (node->mn_links)
    {
      node_links = node->mn_links;
      while (node_links)
	{
	  map_update_map_link (node_links->data);
	  node_links = g_list_next (node_links);
	}
    }
}
/* EOF */
