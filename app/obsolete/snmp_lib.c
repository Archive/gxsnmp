/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Snmp support
 */
#ifdef HAVE_CONFIG_H
#include <config.h>               /* pull in the autoconf genereated stuff */
#endif
#include "main.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#ifdef TIME_WITH_SYS_TIME
#include <sys/time.h>
#endif
#include <time.h>
#include <sys/errno.h>
#include <glib.h>
#include "snmp_lib.h"
#include "host.h"
#include "snmp_host.h"
#include "protos.h"

extern int errno;

void 
snmperror ()
{
  char buf[256];

  switch (errno)
    {
    case EPROTONOSUPPORT:
      snprintf (buf, sizeof (buf), "The protocol type or the specified "
		"protocol is not supported within this domain.");
      break;
    case EMFILE:
      snprintf (buf, sizeof (buf), 
		"The per-process descriptor table is full.");
      break;
    case ENFILE:
      snprintf (buf, sizeof (buf),
		"The system file table is full.");
      break;
    case EACCES:
      snprintf (buf, sizeof (buf), "Insuffcient permissions to open snmp"
		" socket or the protocol is denied.");
      break;
    case ENOBUFS:
      snprintf (buf, sizeof (buf), "Insuffcient buffer space is available. "
		"The socket cannot be created until sufficient resources are "
		"freed.");
      break;
    }
  notice_dlg (buf);
}

/*
 * The initialisation
 */

void
snmpinit() 
{
  if (!g_snmp_init(FALSE))
    g_error("Initialisation of SNMP library failed.");
} 

/*
 * Function    : snmp_add_null_var
 * Description : This function will add a variable to the list of OID's to
 *               get from the node.
 */
void
snmp_add_null_var (GSList **objs, GSList **entry, gchar *what,
		   gchar *identifier, gint upd_type, gpointer update)
{
  SNMP_OBJECT     *obj;
  oid_entry       *vars;

  obj = (SNMP_OBJECT *)g_malloc (sizeof (SNMP_OBJECT));

  if (obj)
    {
      obj->type       = SNMP_NULL;
      obj->id_len     = SNMP_SIZE_OBJECTID;
      obj->id         = g_malloc (SNMP_SIZE_OBJECTID);
      
      if (!read_objid (what, obj->id, &obj->id_len))
	{
	  /* Set error condition and return */
	  g_free (obj);
	  return ;
	}
      vars            = (oid_entry *)g_malloc (sizeof (oid_entry));
      vars->oe_idlen  = obj->id_len;
      vars->oe_name   = (gchar *)g_strdup (identifier);
      vars->oe_type   = upd_type;
      vars->oe_update = update;
      g_memmove (vars->oe_id, obj->id, obj->id_len * sizeof (gulong));

      *objs = g_slist_append (*objs, obj);
      *entry = g_slist_append (*entry, vars);
    }
}
/*
 * Function    : snmp_update_func
 * Description : This is intended to be a generic callback function
 *               that will facilitate updating widgets and/or application
 *               variables. This should also be able to call plugins if 
 *               that need arises.
 */
gboolean 
snmp_update_func (host_snmp *host, gpointer magic, SNMP_PDU *spdu,
		  GSList *objs)
{
  gchar           buf[1024];
  hosts           *udp_host;
  GSList          *entry;
  GSList          *myentry;
  GSList          *myobj;
  oid_entry       *oid;
  SNMP_OBJECT     *obj;

  /* FIXME: Figure out what to do with the request stuff */
  udp_host = find_host_by_name (host->name);

  entry = (GSList *)magic;

  if (spdu->request.error_status)
    {
      g_slist_free (entry);
      return TRUE;
    }
  myobj = objs;
  while (myobj)
    {
      obj     = (SNMP_OBJECT *)myobj->data;
      myentry = entry;
      while (myentry)
	{
	  oid = (oid_entry *)myentry->data;
	  if (!memcmp(obj->id, oid->oe_id, oid->oe_idlen * sizeof (gulong)))
	    {
	      switch (oid->oe_type)
		{
		case OID_UPDATE_WIDGET:
		  g_snmp_printf(buf, sizeof(buf), obj);
		  gtk_entry_set_text (GTK_ENTRY (oid->oe_update), buf);
		  break;
		case OID_UPDATE_VARIABLE:
		  /* update some structure/application variable */
		  break;
		}
	    }
	  myentry = g_slist_next (myentry);
	}
      myobj = g_slist_next (myobj);
    }
  map_update_stat (_("SNMP Query to node complete."));
  g_slist_free (entry);
  return TRUE;
}

/* EOF */



