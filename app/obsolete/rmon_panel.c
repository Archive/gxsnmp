/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Jochen Friedrich & Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * RMON panel
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#include <gnome.h>
#endif
#include "main.h"
#include "gui.h"
#include "rmon_panel.h"

extern gxsnmp *app_info;

/*
 * Local variables for the network list panel
 *
 */
static rmon_panel    *rm_panel = NULL;

/*
 * Forward declarations for the network panel
 */
static void           create_rmon_panel        (void);
static gint           delete_rmon_panel        (GtkWidget    *widget,
						GdkEvent     *e,
						gpointer     data);
static void           refresh_rmon_cb          (GtkWidget    *widget,
						gpointer     data);

static char *evc_labels[] = {
  N_("Index"),
  N_("Description"),
  N_("Type"),
  N_("Trap Community"),
  N_("Last Sent"),
  N_("Owner"),
  N_("Status")
};

static void
create_rmon_panel ()
{
  GtkWidget      *frame;
  GtkWidget      *label;
  GtkWidget      *vbox;
  GtkWidget      *notebook;
  int             i;

  rm_panel = (rmon_panel *)g_malloc (sizeof(rmon_panel));

  rm_panel->window = gnome_app_new ("GXSNMP", _("RMON"));
  /*
   * There _HAS_ to be a better way to do this so that the entire
   * clist is _visible_.
   */
  gtk_widget_set_usize (rm_panel->window, 600, 400);

  rm_panel->request = NULL;
  gtk_signal_connect (GTK_OBJECT (rm_panel->window),
		      "delete_event",
		      GTK_SIGNAL_FUNC (delete_rmon_panel),
		      NULL);

  vbox = gtk_vbox_new (FALSE, 4);

  label = gtk_label_new (_("RMON"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.0);
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 2);

  notebook = gtk_notebook_new ();
  gtk_signal_connect (GTK_OBJECT (notebook), "switch_page",
                      GTK_SIGNAL_FUNC (page_switch_cb), NULL); 
  gtk_notebook_set_tab_pos (GTK_NOTEBOOK (notebook), GTK_POS_TOP);
  gtk_box_pack_start (GTK_BOX (vbox), notebook, TRUE, TRUE, 0);

/*  
 * Events page
 */
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 5);

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), frame,
                            notebook_tab (_("Events")));

  for (i=0; i< ELEMENTS (evc_labels); i++)
    {
      evc_labels[i] = strdup(gettext(evc_labels[i]));
    }
  rm_panel->evt_clist = gtk_clist_new_with_titles (ELEMENTS (evc_labels), evc_labels);
  gtk_clist_set_column_width (GTK_CLIST (rm_panel->evt_clist), 0, 40);
  gtk_clist_set_column_width (GTK_CLIST (rm_panel->evt_clist), 1, 110);
  gtk_clist_set_column_width (GTK_CLIST (rm_panel->evt_clist), 2, 40);
  gtk_clist_set_column_width (GTK_CLIST (rm_panel->evt_clist), 3, 90);
  gtk_clist_set_column_width (GTK_CLIST (rm_panel->evt_clist), 4, 90);
  gtk_clist_set_column_width (GTK_CLIST (rm_panel->evt_clist), 5, 100);
  gtk_clist_set_column_width (GTK_CLIST (rm_panel->evt_clist), 6, 40);
  gtk_container_add (GTK_CONTAINER (frame), rm_panel->evt_clist);

/*  
 * Alarm page
 */
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 5);

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), frame,
                            notebook_tab (_("Alarms")));
/*  
 * Capture page
 */
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 5);

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), frame,
                            notebook_tab (_("Capture")));
/*  
 * Statistics page
 */
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 5);

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), frame,
                            notebook_tab (_("Statistics")));
/*  
 * Filter page
 */
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 5);

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), frame,
                            notebook_tab (_("Filter")));

  gnome_app_set_contents (GNOME_APP (rm_panel->window), vbox);

  gtk_widget_show_all (rm_panel->window);
}

static void
reload_event_table (hosts *host)
{
  hosts     *this_host;
  GSList    *objs;
  GSList    *magic;

  this_host = (hosts *)host;

  if (rm_panel->request)
    g_remove_request (rm_panel->request);
  rm_panel->request = NULL;

  if (this_host)
    {
      objs  = NULL;
      magic = NULL;
      update_statusbar (_("Reloading event table from host '%s'..."),
                         this_host->hl_disp);
      add_null_var (&objs, &magic, "rmon.event.eventTable.eventEntry.eventIndex",
                    rm_panel->evt_clist);
      add_null_var (&objs, &magic, "rmon.event.eventTable.eventEntry.eventDescription",
                    rm_panel->evt_clist);
      add_null_var (&objs, &magic, "rmon.event.eventTable.eventEntry.eventType",
                    rm_panel->evt_clist);
      add_null_var (&objs, &magic, "rmon.event.eventTable.eventEntry.eventCommunity",
                    rm_panel->evt_clist);
      add_null_var (&objs, &magic, "rmon.event.eventTable.eventEntry.eventLastTimeSent",
                    rm_panel->evt_clist);
      add_null_var (&objs, &magic, "rmon.event.eventTable.eventEntry.eventOwner",
                    rm_panel->evt_clist);
      add_null_var (&objs, &magic, "rmon.event.eventTable.eventEntry.eventStatus",
                    rm_panel->evt_clist);
      host->hl_snmp.magic = magic;
      host->hl_snmp.done_callback = update_evt_ctable;
      host->hl_snmp.time_callback = destroy_evt_ctable;
      rm_panel->request = g_async_getnext (&host->hl_snmp, objs);
    } else {
      g_warning ("Null pointer alert!\n");
    }
}

void
update_evt_ctable (host_snmp *host, void *magic, SNMP_PDU *spdu, GSList *objs)
{
  char                 buf[1024];              /* Should be big enough? */
  GSList              *entry;
  GSList              *myentry;
  GSList              *myobj;
  GSList              *nobjs;
  oidentry            *var;
  struct _SNMP_OBJECT *obj;
  struct _SNMP_OBJECT *nobj;
  char                *kludge[ELEMENTS (evc_labels)];  /* FIXME */
  int                  i, j;

#ifdef MEM_DEBUG
  DMC ();
#endif

  rm_panel->request = NULL;
  entry = (GSList *)magic;

  if (spdu->request.error_status)
    {
      g_slist_free(entry);
      return;
    }

  nobjs = NULL;
  myobj = objs;
  j     = 0;
  i     = 0;
  while(myobj)
    {
      obj = (struct _SNMP_OBJECT *) myobj->data;
      g_snmp_printf(buf, sizeof(buf), obj);

      myentry = entry;
      i = 0;
      while (myentry)
        {
          var = (oidentry *) myentry->data;
          if (!memcmp(obj->id, var->id, var->id_len * sizeof(gulong)))
            {
              kludge[i] = strdup(buf);
              nobj = g_malloc(sizeof(struct _SNMP_OBJECT));
              nobj->type       = SNMP_NULL;
              nobj->syntax_len = 0;
              nobj->id_len     = obj->id_len;
              g_memmove(nobj->id, obj->id, obj->id_len * sizeof(gulong));
              nobjs  = g_slist_append(nobjs, nobj);
              j++;
            }
          i++;
          myentry = myentry->next;
        }
      myobj = myobj->next;
    }
  if (i != j)
    {
      g_slist_free(nobjs);
      nobjs = NULL;
    }
  if (nobjs)
    {
      var = (oidentry *) entry->data;
      gtk_clist_append (GTK_CLIST (var->name), kludge);
      host->magic = magic;
      host->done_callback = update_evt_ctable;
      host->time_callback = destroy_evt_ctable;
      rm_panel->request = g_async_getnext(host, nobjs);
    }
  else
    {
      g_slist_free(entry);
      update_statusbar (_("Event table reload from host '%s' complete."),
                       host->name);
      rm_panel->request = NULL;
    }
}

void
destroy_evt_ctable (host_snmp *host, void *magic)
{
  GSList              *entry;

  rm_panel->request = NULL;

  entry = (GSList *)magic;
  g_slist_free(entry);
}

/*
 * Callback functions
 */

static void 
refresh_rmon_cb (GtkWidget *widget, gpointer data)
{
}

static gint
delete_rmon_panel (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  if (rm_panel)
    {
      gtk_widget_destroy (rm_panel->window);
      g_free (rm_panel);
      rm_panel = NULL;
    }
  return TRUE;
}
/*
 * Global Standard panel functions.
 */
void
open_rmon_panel ()
{
  if (rm_panel == NULL)
    create_rmon_panel ();
  else if (!GTK_WIDGET_VISIBLE (rm_panel->window))
    gtk_widget_show (rm_panel->window);
  reset_rmon_panel ();
}

void
destroy_rmon_panel ()
{
  if (rm_panel != NULL)
    delete_rmon_panel (NULL, NULL, rm_panel);
}

void
hide_rmon_panel ()
{
  if (rm_panel->window)
    if (GTK_WIDGET_VISIBLE (rm_panel->window))
      gtk_widget_hide (rm_panel->window);
}

void
reset_rmon_panel ()
{
  gtk_clist_clear (GTK_CLIST (rm_panel->evt_clist));
/*  reload_event_table (get_current_host (NULL));  */

  refresh_rmon_cb (NULL, rm_panel);
}

/* EOF */
