/*
**  $Id$
**
**  GXSNMP -- An snmp mangament application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Subroutines for accessing the host database
*/

#include <glib.h>
#include <g_sqldb.h>
#include "main.h"
#include "tables.h"
#include "protos.h"
#include "host_db.h"

extern gxsnmp *app_info;

/******************************************************************************
**
**  Definition of the database-backed columns in the host table
**
******************************************************************************/

static
G_sqldb_column hosts_columns[] = {

  { "_rowid",          G_SQL_INT | G_SQL_KEY | G_SQL_PRIMARY,
                       G_STRUCT_OFFSET (DB_hosts, rowid) },

  { "display_name",    G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_disp) },

  { "hostname",        G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.name) },

  { "net_addr",	       G_SQL_INT,
		       G_STRUCT_OFFSET (DB_hosts, hl_addr.s_addr) },

  { "net_mask",	       G_SQL_INT,
		       G_STRUCT_OFFSET (DB_hosts, hl_netmask.s_addr) },

  { "read_comm",       G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.rcomm) },

  { "write_comm",      G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.wcomm) },

  { "retries",         G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.retries) },

  { "timeout",         G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.timeout) },

  { "snmp_port",       G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.port) },

  { "snmp_domain",     G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.domain) },

  { "snmp_version",    G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.version) },

  { "map_x",           G_SQL_DOUBLE,
                       G_STRUCT_OFFSET (DB_hosts, hl_location.x) },

  { "map_y",           G_SQL_DOUBLE,
                       G_STRUCT_OFFSET (DB_hosts, hl_location.y) },

  { "map_group",       G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_group) },

  { "flags",           G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_flags) },

  { "system_location", G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_sysloc) },

  { "contact_person",  G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_syscon) },

  { "pixmap_file",     G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, pixmap_file) },
  { NULL },
};

G_sqldb_table hosts_db_struct =
{
  NULL,                                       /* Pointer to G_sql structure */
  NULL,                                       /* Pointer to database name */
  "hosts",                                    /* Pointer to table name */
  sizeof (DB_hosts),                           /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_hosts, g_sqldb_private), /* Offset of private handle */
  hosts_columns			      	      /* List of column definitions */
};

G_sqldb_table * hosts_sqldb = &hosts_db_struct;

/*****************************************************************************
**
**  host_db_update_entry ()  --  Update an entry in the host database
**
**  This subroutine updates an existing record in the host database.
**
**  Return Values:
**
**  TRUE  -- The entry was successfully updated
**  FALSE -- An error occurred, and the entry was not updated.
*
*****************************************************************************/

gboolean   
host_db_update_entry (DB_hosts * host)
{
  return g_sqldb_row_update (hosts_sqldb, host);
}

/*****************************************************************************
**
**  host_db_delete_entry ()  --  Delete an entry in the host database.
**
**  Return Values:
**
**  TRUE  -- The entry was successfully deleted.
**  FALSE -- An error occurred, and the entry was not deleted.
**
******************************************************************************/

gboolean   
host_db_delete_entry (DB_hosts * host)
{
  return g_sqldb_row_delete (hosts_sqldb, host);
}
/****************************************************************************
 *
 * host_db_add_entry ()   -- Add an entry to the host database.
 *
 * Return Values:
 *
 * TRUE   -- The entry was successfully added.
 * FALSE  -- An error occured.
 ****************************************************************************/
gboolean
host_db_add_entry (DB_hosts *host)
{
  return g_sqldb_row_add (hosts_sqldb, host);
}

/* EOF */
