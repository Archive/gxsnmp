/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * 'lazy mans' gui functions.
 */
#include "config.h"
#include <gnome.h>

#include "main.h"
#include "gui.h"


int color_map[][3] = {
  {   0,   0,   0 },         /* black */
  {   0,   0, 255 },         /* Blue */
  {   0, 200,   0 },         /* Green */
  { 200,   0,   0 },         /* Red */
  { 255,   0, 255 },         /* Violet */
  { 255, 255,   0 }          /* Yellow */
};

extern gxsnmp *app_info;

GdkPixmap *book_open        = NULL;
GdkPixmap *book_closed      = NULL;
GdkPixmap *book_open_mask   = NULL;
GdkPixmap *book_closed_mask = NULL;

static char * book_open_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c black",
"X      c #808080",
"o      c white",
"                ",
"  ..            ",
" .Xo.    ...    ",
" .Xoo. ..oo.    ",
" .Xooo.Xooo...  ",
" .Xooo.oooo.X.  ",
" .Xooo.Xooo.X.  ",
" .Xooo.oooo.X.  ",
" .Xooo.Xooo.X.  ",
" .Xooo.oooo.X.  ",
"  .Xoo.Xoo..X.  ",
"   .Xo.o..ooX.  ",
"    .X..XXXXX.  ",
"    ..X.......  ",
"     ..         ",
"                "};

static char * book_closed_xpm[] = {
"16 16 6 1",
"       c None s None",
".      c black",
"X      c red",
"o      c yellow",
"O      c #808080",
"#      c white",
"                ",
"       ..       ",
"     ..XX.      ",
"   ..XXXXX.     ",
" ..XXXXXXXX.    ",
".ooXXXXXXXXX.   ",
"..ooXXXXXXXXX.  ",
".X.ooXXXXXXXXX. ",
".XX.ooXXXXXX..  ",
" .XX.ooXXX..#O  ",
"  .XX.oo..##OO. ",
"   .XX..##OO..  ",
"    .X.#OO..    ",
"     ..O..      ",
"      ..        ",
"                "};


GtkWidget *
new_window (char *title, GtkWindowType type, GtkWindowPosition pos, int border)
{
  GtkWidget    *window;

  window = gtk_window_new (type);
  gtk_window_set_title (GTK_WINDOW (window), title);
  gtk_window_position (GTK_WINDOW (window), pos);
  gtk_container_border_width (GTK_CONTAINER (window), border);
  return window;
}

GdkColor
get_color (int which)
{
  GdkColor   color;
  gint       n;

  color.red  = color_map[which][RED_IDX] | (color_map[which][RED_IDX] << 8);
  color.green= color_map[which][GRN_IDX] | (color_map[which][GRN_IDX] << 8);
  color.blue = color_map[which][BLU_IDX] | (color_map[which][BLU_IDX] << 8);
  color.pixel = 0;   /* required */
  n = 0;
  gdk_color_context_get_pixels (app_info->cc,
				&color.red, 
				&color.green, 
				&color.blue,
				1,
				&color.pixel,
				&n);
  return color;
}

/*
 * notebook page switch function.
 * Basic one to handle changing the pixmaps
 */
void
page_switch_cb (GtkWidget *widget, GtkNotebookPage *page, gint page_num)
{
  GtkNotebookPage *oldpage;
  GtkWidget       *pixwid;


  oldpage = GTK_NOTEBOOK (widget)->cur_page;
  if (page == oldpage)
    return;
  /*
  pixwid = ((GtkBoxChild*)(GTK_BOX (page->tab_label)->children->data))->widget;
  gtk_pixmap_set (GTK_PIXMAP (pixwid), book_open, book_open_mask);
  if (oldpage)
    {
      pixwid = ((GtkBoxChild*)(GTK_BOX
			       (oldpage->tab_label)->children->data))->widget;
      gtk_pixmap_set (GTK_PIXMAP (pixwid), book_closed, book_closed_mask);
    }
  */
}

/*
 * Notice dialog. No critical messages, just something the user needs to
 * know.
 */
void
notice_dlg (char *msg, ...)
{
  va_list ap;
  gchar   buf[1024];

  va_start (ap, msg);
  vsnprintf (buf, sizeof(buf), msg, ap);
  va_end (ap);

  gtk_widget_show (gnome_message_box_new (buf, "info",
					  GNOME_STOCK_BUTTON_OK, NULL));
}

/*
 * Do them nifty looking notebook tabs, yeah its a short function but
 * I got tired of writing the same 10-15 lines in every panel.
 */
GtkWidget *
notebook_tab (const char *label)
{
  GtkWidget    *tab_box;
  GtkWidget    *pixmap;
  GtkWidget    *tab_label;
  

  tab_box = gtk_hbox_new (FALSE, 0);
  tab_label = gtk_label_new (label);
  /*  pixmap = gtk_pixmap_new (book_closed, book_closed_mask);
  gtk_box_pack_start (GTK_BOX (tab_box), pixmap, FALSE, TRUE, 0);
  gtk_misc_set_padding (GTK_MISC (pixmap), 3, 1); */
  gtk_misc_set_padding (GTK_MISC (tab_label), 10, 0);
  gtk_box_pack_start (GTK_BOX (tab_box), tab_label, FALSE, TRUE, 0);
  gtk_widget_show_all (tab_box);
  return tab_box;
}

/*
 * Function    : get_widget
 * Description : This function will get a widget by its tag.
 * Arguments   : widget  -- The parent widget that should have 
 *                          this widget contained in it.
 *               name    -- The tag to look for.
 * Returns     : The GtkWidget pointer of the widget or NULL
 *               on failure.
 */
GtkWidget * 
get_widget (GtkWidget *widget, const gchar *name)
{
  GtkWidget   *found;

  g_return_val_if_fail (GTK_IS_OBJECT (widget), NULL);
  if (widget->parent)
    widget = gtk_widget_get_toplevel (widget);
  found = gtk_object_get_data (GTK_OBJECT (widget), name);
  if (!found)
    {
      return NULL;
    }
  return found;
}
/*
 * Function    : gt_build_ui_table
 * Description : This function is a convience function to do the table
 *               layout of the panels in this app.
 */
gboolean 
gt_build_ui_table (GTableDef  *table_info, GtkWidget *table)
{
  GtkWidget    *label;
  GtkWidget    *widget;
  GtkWidget    *parent;
  GTableAttach *attach;
  GtkWidget    *tooltips;
  gint         i;

  g_return_val_if_fail (table_info != NULL, FALSE);
  g_return_val_if_fail (table != NULL, FALSE);

  parent = gtk_widget_get_toplevel (table);
  tooltips = get_widget (parent, "tooltips");
  for (i = 0; table_info[i].entry_type != GT_UI_END; i++)
    {
      if (table_info[i].label_val && table_info[i].entry_type == GT_ENTRY)
	{
	  label = gtk_label_new (table_info[i].label_val);
	  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
	}
      attach = table_info[i].label_attach;
      if (attach && label)
	gtk_table_attach (GTK_TABLE (table), label,
			  attach->left_attach,
			  attach->right_attach,
			  attach->top_attach,
			  attach->bottom_attach,
			  attach->x_flags, attach->y_flags,
			  attach->x_pad, attach->y_pad);
      if (table_info[i].tooltip)
	{
	  if (tooltips && label)
	    gtk_tooltips_set_tip (GTK_TOOLTIPS (tooltips), label, 
				  table_info[i].tooltip, NULL);
	}
      switch (table_info[i].entry_type)
	{
	case GT_ENTRY:
	  widget = gtk_entry_new ();
	  break;
	case GT_FRAME:
	  if (table_info[i].label_val)
	    widget = gtk_frame_new (table_info[i].label_val);
	  else
	    widget = gtk_frame_new (NULL);
	  break;
	case GT_CHECKBUTTON:
	  if (table_info[i].label_val)
	    widget = gtk_check_button_new_with_label (table_info[i].label_val);
	  else
	    widget = gtk_check_button_new ();
	  break;
	default:
	  widget = gtk_entry_new ();
	  break;
	}
      if (table_info[i].entry_tag)
	  gtk_object_set_data (GTK_OBJECT (parent), table_info[i].entry_tag,
			       widget);
      if ( (attach = table_info[i].entry_attach) )
	gtk_table_attach (GTK_TABLE (table), widget,
			  attach->left_attach,
			  attach->right_attach,
			  attach->top_attach,
			  attach->bottom_attach,
			  attach->x_flags, attach->y_flags,
			  attach->x_pad, attach->y_pad);
    }
  gtk_widget_show_all (table);
  return TRUE;
}

void
gt_add_action_buttons (GTCallbacks *callbacks, GtkWidget *b_box)
{
  GtkWidget    *button;
  GtkWidget    *pixmap;
  gint         i;

  button = NULL;                 /* SHUT UP GCC */

  for (i = 0; callbacks[i].button_type != GT_UI_END; i++)
    {
      switch (callbacks[i].button_type)
	{
	case GT_BUTTON_OK:
	  button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
	  break;
	case GT_BUTTON_CAN:
	  button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
	  break;
	case GT_BUTTON_REL:
	  button = gtk_button_new_with_label ("Reload");
	  break;
	case GT_BUTTON_EXIT:
	  pixmap = gnome_stock_pixmap_widget (b_box, GNOME_STOCK_PIXMAP_QUIT);
	  button = gnome_pixmap_button (pixmap, N_("Exit"));
	  break;
	case GT_BUTTON_CLOSE:
	  button = gnome_stock_button (GNOME_STOCK_BUTTON_CLOSE);
	  break;
	case GT_BUTTON_SAVE:
	  pixmap = gnome_stock_pixmap_widget (b_box, GNOME_STOCK_PIXMAP_SAVE);
	  button = gnome_pixmap_button (pixmap, N_("Save"));
	  break;
	case GT_BUTTON_DEFAULT:
	  button = gtk_button_new_with_label (_("Defaults"));
	  break;
	}
      if (callbacks[i].callback_func)
	{
	  gtk_signal_connect (GTK_OBJECT (button), "clicked",
			      GTK_SIGNAL_FUNC (callbacks[i].callback_func),
			      callbacks[i].callback_data);
	}
      if (callbacks[i].button_tag)
	{
	  gtk_object_set_data (GTK_OBJECT (button), callbacks[i].button_tag,
			       button);
	}
      gtk_container_add (GTK_CONTAINER (b_box), button);
    }
}

/* EOF */
