/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place - Suite 330 , Cambridge, MA 02139, USA.
**
**  Subroutines for accessing the host database
*/

#ifndef __HOST_DB_H__
#define __HOST_DB_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib.h>
#include <g_sql.h>
#include <g_sqldb.h>
#include "structs.h"

G_sqldb_table * host_sqldb;

/*******************************************************************************
**
**  In-memory structure of a database-backed host object
**
*******************************************************************************/

typedef struct _hosts DB_hosts;	

/*******************************************************************************
**
**  Subroutines to access the SNMP configuration database
**
*******************************************************************************/

gboolean      host_db_load_table   (G_sql *host_db, gchar *host_database);

gboolean      host_db_add_entry    (DB_hosts * entry);

gboolean      host_db_update_entry (DB_hosts * entry);

gboolean      host_db_delete_entry (DB_hosts * entry);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __HOST_DB_H__ */
/* EOF */
