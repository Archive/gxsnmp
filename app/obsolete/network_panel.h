/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Network list panel definitions
 */
#ifndef __NETWORK_LIST_H__
#define __NETWORK_LIST_H__

#include <gtk/gtk.h>

/*
 * For the network list panel
 */
typedef struct __netwk_panel {
  GtkWidget    *window;
  GtkWidget    *net_clist;
  GtkWidget    *buttons[2];
} netwk_panel;

#define NETLIST_CLOSE   0
#define NETLIST_REFR    1

/*
 * For the add a network panel
 */
typedef struct __net_add__ {
  GtkWidget    *window;
  GtkWidget    *entries[3];
} net_add_panel;

/*
 * Global standard panel functions.
 *
 *  Network List panel
 */
void             open_network_panel            (void);
void             destroy_network_panel         (void);
void             reset_network_panel           (void);
void             hide_network_panel            (void);

/*
 * Add network panel
 */
void            open_add_network_panel         (void);
void            destroy_add_net_panel          (void);
void            reset_add_net_panel            (void);
void            hide_add_net_panel             (void);

#endif

/* EOF */
