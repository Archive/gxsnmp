/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 * 
 * The mib browser panel.
 */

#ifndef __BROWSER_PAN_H__
#define __BROWSER_PAN_H__
#include <gnome.h>

typedef struct __BrowserPanel {
  GtkWidget   *window;
  GtkWidget   *rbutton;
  GtkWidget   *root;           /* root of the tree */
  GtkWidget   *mib_desc;       /* the description of the mib */
  GtkWidget   *mib_type;       /* Type of this mib */
  GtkWidget   *mib_oid;        /* the oid of this mib */
  GtkWidget   *mib_label;      /* the textual representation of a mib */
  GtkWidget   *mib_value;
  GtkWidget   *mib_host;
  GtkWidget   *fbutton;
  GtkWidget   *nbutton;
  GtkWidget   *gbutton;
  GtkWidget   *pbutton;
} browser_panel;

void browser_add_host       (char *host);
void browser_remove_host    (char *host);
/*
 * snmp callbacks
 */
gboolean update_mib_value       (host_snmp *host,
                                 void *magic,
                                 SNMP_PDU *spdu,
                                 GSList *objs);

/*
 * snmp timeout callbacks
 */
void update_mib_timeout         (host_snmp *host,
				 void *magic);

/*
 * Standard panel functions
 */
void       open_browser_panel          (void);
void       hide_browser_panel          (void);
void       destroy_browser_panel       (void);
void       reset_browser_panel         (void);
#endif

