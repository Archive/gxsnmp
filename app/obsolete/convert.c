#include <glib.h>
#include <g_sqldb.h>
#include "main.h"
#include "tables.h"
#include "protos.h"

extern gxsnmp *app_info;

/*
**  Hackish Table used to convert old network database format to new format 
*/

typedef struct _DB_networkz
{
  gint	       rowid;
  gchar      * name;
  gchar      * address;
  gchar      * netmask;
  gint	       x;
  gint         y;
  gpointer     g_sqldb_private;
}
DB_networkz;

typedef struct _hosts DB_hosts;


/******************************************************************************
**
**  Definition of the database-backed columns in the old host table
**
******************************************************************************/

static
G_sqldb_column hosts_columns[] = {

  { "_rowid",          G_SQL_INT | G_SQL_KEY | G_SQL_PRIMARY,
                       G_STRUCT_OFFSET (DB_hosts, rowid) },

  { "display_name",    G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_disp) },

  { "hostname",        G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.name) },

  { "net_addr",	       G_SQL_INT,
		       G_STRUCT_OFFSET (DB_hosts, hl_addr.s_addr) },

  { "net_mask",	       G_SQL_INT,
		       G_STRUCT_OFFSET (DB_hosts, hl_netmask.s_addr) },

  { "read_comm",       G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.rcomm) },

  { "write_comm",      G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.wcomm) },

  { "retries",         G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.retries) },

  { "timeout",         G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.timeout) },

  { "snmp_port",       G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.port) },

  { "snmp_domain",     G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.domain) },

  { "snmp_version",    G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_snmp.version) },

  { "map_x",           G_SQL_DOUBLE,
                       G_STRUCT_OFFSET (DB_hosts, hl_location.x) },

  { "map_y",           G_SQL_DOUBLE,
                       G_STRUCT_OFFSET (DB_hosts, hl_location.y) },

  { "map_group",       G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_group) },

  { "flags",           G_SQL_INT,
                       G_STRUCT_OFFSET (DB_hosts, hl_flags) },

  { "system_location", G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_sysloc) },

  { "contact_person",  G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, hl_syscon) },

  { "pixmap_file",     G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_hosts, pixmap_file) },
  { NULL },
};

G_sqldb_table hosts_db_struct =
{
  NULL,                                       /* Pointer to G_sql structure */
  NULL,                                       /* Pointer to database name */
  "hosts",                                    /* Pointer to table name */
  sizeof (DB_hosts),                           /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_hosts, g_sqldb_private), /* Offset of private handle */
  hosts_columns			      	      /* List of column definitions */
};

G_sqldb_table * hosts_sqldb = &hosts_db_struct;


static
G_sqldb_column networkz_columns[] = {

  { "_rowid",          G_SQL_INT | G_SQL_KEY | G_SQL_PRIMARY,
                       G_STRUCT_OFFSET (DB_networkz, rowid) },

  { "name",            G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_networkz, name) },

  { "address",         G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_networkz, address) },

  { "mask",            G_SQL_STRING,
                       G_STRUCT_OFFSET (DB_networkz, netmask) },

  { "map_x",           G_SQL_INT,
                       G_STRUCT_OFFSET (DB_networkz, x) }, 

  { "map_y",           G_SQL_INT,
                       G_STRUCT_OFFSET (DB_networkz, y) }, 

  { NULL }
};

static
G_sqldb_table networkz_db_struct =
{
  NULL,                                         /* Pointer to G_sql structure */
  NULL,                                         /* Pointer to database name */
  "networks",                                   /* Pointer to table name */
  sizeof (DB_networkz),                         /* Length of a row in bytes */
  G_STRUCT_OFFSET (DB_networkz, g_sqldb_private),   /* Offset of prvt hndl */
  networkz_columns                              /* List of column definitions */
};

G_sqldb_table * networkz_sqldb = &networkz_db_struct;

/******************************************************************************
**
**  Convert database format
**
******************************************************************************/

gchar * host_dbname = "gxsnmp";
extern G_sqldb_table hosts_db_struct;
extern G_sqldb_table * hosts_sqldb;

void
db_convert ()
{
  GList * gl;
  DB_map  * dbm;
  gchar        * timestamp;

  timestamp = db_timestamp ();


  g_print ("Making initial map entry\n");

  dbm = g_new0 (DB_map, 1);
  dbm->rowid 		= 0;	/* Zero is the default map */
  dbm->created 		= g_strdup (timestamp);
  dbm->modified 	= g_strdup (timestamp);
  dbm->name		= g_strdup ("Default");
  dbm->tab    		= g_strdup ("Default");
  dbm->description 	= g_strdup ("Default Map");

  g_sqldb_row_add (map_sqldb, dbm);

  g_print ("Initial map entry created\n");

  g_print ("Reading in old hosts database\n");

  hosts_db_struct.engine   = &gxsnmp_sql_db; 
  hosts_db_struct.database = &host_dbname;
  
  g_sqldb_table_load (hosts_sqldb);
  g_print ("Table loaded\n"); 
  gl = g_sqldb_table_list (hosts_sqldb);
  while (gl)
    {
      DB_hosts	   * hosts;
      DB_interface * dbi;
      DB_host	   * dbh;
      DB_snmp	   * dbs;
      DB_graph	   * dbg;
      int	     dbi_rowid;
      int	     dbh_rowid;
      int            dbs_rowid;
      int            dbg_rowid;

      hosts = (DB_hosts *) gl->data;

      g_print ("Converting entry %s\n", hosts->hl_disp);

      dbi = g_new0 (DB_interface, 1);
      dbh = g_new0 (DB_host, 1);
      dbs = g_new0 (DB_snmp, 1);
      dbg = g_new0 (DB_graph, 1);

      dbi_rowid = g_sqldb_highest_rowid (interface_sqldb, "_rowid") + 1;
      dbh_rowid = g_sqldb_highest_rowid (host_sqldb, "_rowid") + 1;
      dbs_rowid = g_sqldb_highest_rowid (snmp_sqldb, "_rowid") + 1;
      dbg_rowid = g_sqldb_highest_rowid (graph_sqldb, "_rowid") + 1;
    
      dbi->rowid 	= dbi_rowid;
      dbi->created	= strdup (timestamp);
      dbi->modified	= strdup (timestamp);
      dbi->host		= dbh_rowid;
      dbi->snmp		= dbs_rowid;
      dbi->transport	= hosts->hl_snmp.domain;
      dbi->address	= g_strdup (inet_ntoa(hosts->hl_addr));
      dbi->netmask	= g_strdup (inet_ntoa(hosts->hl_netmask));

      dbh->rowid        = dbh_rowid;
      dbh->created      = strdup (timestamp);
      dbh->modified     = strdup (timestamp);
      dbh->dns_name     = g_strdup (hosts->hl_disp);
      dbh->name         = g_strdup (hosts->hl_disp);
      dbh->description  = g_strdup (hosts->hl_sysloc);
      dbh->contact      = g_strdup (hosts->hl_syscon);

      dbs->rowid	= dbs_rowid;
      dbs->created	= strdup (timestamp);    
      dbs->modified	= strdup (timestamp);
      dbs->name	        = g_strdup_printf ("SNMP/%s", hosts->hl_disp);
      dbs->version	= hosts->hl_snmp.version;    
      dbs->port	        = hosts->hl_snmp.port;
      dbs->timeout	= hosts->hl_snmp.timeout;
      dbs->retries	= hosts->hl_snmp.retries;   
      dbs->read_c	= g_strdup (hosts->hl_snmp.rcomm);
      dbs->write_c	= g_strdup (hosts->hl_snmp.wcomm);

      dbg->rowid	= dbg_rowid;
      dbg->created	= g_strdup (timestamp);
      dbg->modified     = g_strdup (timestamp);
      dbg->map	        = 0;	/* Zero is the default map */
      dbg->type	        = DB_GRAPH_HOST;
      dbg->host	        = dbh_rowid;
      dbg->network      = 0;
      dbg->details      = "";
      dbg->x            = hosts->hl_location.x;
      dbg->y            = hosts->hl_location.y;

      g_sqldb_row_add (host_sqldb, dbh);
      g_sqldb_row_add (interface_sqldb, dbi);
      g_sqldb_row_add (snmp_sqldb, dbs);
      g_sqldb_row_add (graph_sqldb, dbg);
      gl = gl->next;
    }
 g_print ("Database converted!\n");       


 g_print ("Reading in old networks database\n");

  networkz_db_struct.engine   = &gxsnmp_sql_db;
  networkz_db_struct.database = &host_dbname;
 
  g_sqldb_table_load (networkz_sqldb);
  g_print ("Table loaded\n");
  gl = g_sqldb_table_list (networkz_sqldb);
  while (gl)
    {
      DB_networkz  * networkz;
      DB_network   * dbn;
      DB_graph     * dbg;
      int            dbn_rowid;
      int            dbg_rowid;
      gchar        * timestamp;

      networkz = (DB_networkz *) gl->data;

      g_print ("Converting entry %s\n", networkz->name);

      dbn = g_new0 (DB_network, 1);
      dbg = g_new0 (DB_graph, 1);

      dbn_rowid = g_sqldb_highest_rowid (network_sqldb, "_rowid") + 1;
      dbg_rowid = g_sqldb_highest_rowid (graph_sqldb, "_rowid") + 1;

      timestamp = db_timestamp ();

      dbn->rowid        = dbn_rowid;
      dbn->created      = strdup (timestamp);
      dbn->modified     = strdup (timestamp);
      dbn->address      = g_strdup (networkz->address);
      dbn->netmask      = g_strdup (networkz->netmask);
      dbn->speed        = 0;                      
      dbn->name         = g_strdup (networkz->name);
      dbn->description  = g_strdup ("");
      dbn->contact	= g_strdup ("");

      dbg->rowid        = dbg_rowid;
      dbg->created      = g_strdup (timestamp);
      dbg->modified     = g_strdup (timestamp);
      dbg->map          = 0;    /* Zero is the default map */
      dbg->type         = DB_GRAPH_NETWORK;
      dbg->host         = 0;          
      dbg->network      = dbn_rowid;
      dbg->details      = "";
      dbg->x            = networkz->x;
      dbg->y            = networkz->y;

      g_sqldb_row_add (network_sqldb, dbn);
      g_sqldb_row_add (graph_sqldb, dbg);

      gl = gl->next;
    }
  g_print ("Database converted!\n");

  g_print ("Quit and restart the application to use the new database entries\n");
}

/* EOF */
