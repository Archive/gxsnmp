/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Jochen Friedrich & Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Network list panel definitions
 */
#ifndef __RMON_PANEL_H__
#define __RMON_PANEL_H__

#include <gtk/gtk.h>

/*
 * For the network list panel
 */
typedef struct __rmon_panel {
  GtkWidget    *window;
  GtkWidget    *evt_clist;
  gpointer      request;
} rmon_panel;

/*
 * Callbacks for snmp queries (if the query timed out)
 */
void 	   update_evt_ctable              (host_snmp *host,
                                           void *magic,
                                           SNMP_PDU *spdu,
                                           GSList *objs);
void       destroy_evt_ctable             (host_snmp *host,
                                           void *magic);

/*
 * Global standard panel functions.
 *
 *  Network List panel
 */
void             open_rmon_panel            (void);
void             destroy_rmon_panel         (void);
void             reset_rmon_panel           (void);
void             hide_rmon_panel            (void);
 
#endif

/* EOF */
