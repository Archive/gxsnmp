/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Some 'lazy mans' functions.
 */
#ifndef __GUI_SUP_H__
#define __GUI_SUP_H__

#include <config.h>
#include <gnome.h>

#include <stdarg.h>

#define RED_IDX     0
#define GRN_IDX     1
#define BLU_IDX     2

#define BLUE_COLOR    1
#define HOST_UP_COLOR 2
#define HOST_DN_COLOR 3

/*
 * I got sick of writing the _same_ table code over and over and over
 * so here is something to make defineing programing panels easier
 */
#define GT_UI_END       -1
enum {
  GT_ENTRY,
  GT_FRAME,
  GT_CHECKBUTTON,
  GT_COMBO,
  GT_SPIN,
  GT_HSLIDE,
  GT_VSLIDE,
  GT_RADIO
};

enum {
  GT_BUTTON_OK,  
  GT_BUTTON_CAN,
  GT_BUTTON_REL,
  GT_BUTTON_EXIT,
  GT_BUTTON_CLOSE,
  GT_BUTTON_SAVE,
  GT_BUTTON_DEFAULT
};

typedef struct _GTableAttach {
  guint     left_attach;
  guint     right_attach;
  guint     top_attach;
  guint     bottom_attach;
  guint     x_flags;
  guint     y_flags;
  guint     x_pad;
  guint     y_pad;
} GTableAttach;

typedef struct _GTableDef {
  gchar         *label_val;
  gchar         *tooltip;
  gchar         *entry_tag;
  guint         entry_type;
  gpointer      entry_data;     
  GTableAttach  *label_attach;
  GTableAttach  *entry_attach;
} GTableDef;

typedef struct _GTCallbacks {
  guint         button_type;
  gpointer      callback_func;
  gpointer      callback_data;
  gchar         *button_tag;
} GTCallbacks;

typedef struct _GTEntryData {
  gboolean      editable;
  gint          max_size;
  gchar         *name;
} GTEntryData;

/* 
 * Handy element counting macro
 */
#define ELEMENTS(x) (sizeof (x) / sizeof (x[0]))

GtkWidget    *new_window            (char              *title,
				     GtkWindowType     type,
				     GtkWindowPosition pos,
				     int               border);
GtkWidget    *new_pixmap            (char            *filename,
				     GdkWindow       *window,
				     GdkColor        *background);
/*GtkWidget    *new_host_widget       (hosts           *hentry); */
GdkColor     get_color              (int             which); 
void         destroy_pixmap_cache   (void);
void         init_pixmap_d          (GtkWidget       *window);
void         page_switch_cb         (GtkWidget       *widget,
				     GtkNotebookPage *page,
   				     gint            page_num);
void         notice_dlg             (gchar *msg, ...)
                                    __attribute__ ((format (printf, 1, 2)));
GtkWidget    *notebook_tab          (const char      *label);
GtkWidget    *get_widget            (GtkWidget       *widget,
				     const gchar     *name);
/*
 * Some specific ui generation funcs. Probably not usefull outside of this
 * app.
 */
gboolean     gt_build_ui_table      (GTableDef       *table_info,
				     GtkWidget       *widget);
void         gt_add_action_buttons  (GTCallbacks     *callbacks,
				     GtkWidget       *b_box);
#endif
/* EOF */








