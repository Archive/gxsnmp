
/*
 * Data structures.
 */

#ifndef __STRUCTS_H
#define __STRUCTS_H
#include <config.h>
#include <gnome.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>

#include <g_snmp.h>
#include <smi.h>

typedef struct _map_location {
  double        x;
  double        y;
  int           width;
  int           height;
} location;
/*
 * Interface list for a given node.
 */
typedef struct _inf_list {
  struct in_addr   if_addr;       /* Ip address for this interface */
  struct in_addr   if_net;        /* Network # that this interface sits on */
  struct in_addr   if_mask;       /* The netmask for this interface */ 
  time_t           if_lastsnmp;   /* last time we did snmp on this interface */
  time_t           if_lastactive; /* Last time of any activity on this int */
  time_t           if_uptime;     /* Interface uptime (snmp derived) */
  time_t           if_lastup;     /* last update of the interface was */
  int              if_flags;      /* Up down disabled etc.. */
  int              if_speed;      /* Speed of this interface */
  char             if_index;      /* Index in the mib for this interface */
  struct _hosts    *if_host;      /* the host entry for this interface */
} inf_list;

/*
 * Flags we set on a host
 */
enum {
  /* Autoplace on the map */
  GX_AUTOPLACEMENT  = 1 << 0,
    /* Autoconnect to other nodes on the map */
  GX_AUTOCONNECT    = 1 << 1,
    /* Host does SNMP */
  GX_SNMPENABLE     = 1 << 2
};

/*
 * The hosts structure
 */
typedef struct _hosts {
  /* --- GENERAL INFORMATION --- */
  char            *hl_disp;       /* what we should display on the map */
  char            *hl_group;      /* Group for the map. */
  struct in_addr   hl_addr;       /* internet address of this host */
  struct in_addr   hl_netmask;    /* netmask for this host. */
  GSList          *hl_if;         /* Interface list for this host*/
  /* --- SNMP INFORMATION --- */
  host_snmp        hl_snmp;       /* snmp information for this host */
  GList            *hl_monitor;   /* A list of mibs to monitor for this host */
  char             *hl_syscon;    /* snmp sysContact as queried from host */
  char             *hl_sysloc;    /* snmp sysLocation as queried from host */
  /* --- MAP INFORMATION --- */
  location         hl_location;   /* Map related location information */
  gint             hl_state;      /* state for this host for the queue */
  gint             hl_flags;      /* Various flags for this host. */
  gchar            *pixmap_file;  /* Filename of the pixmap */

  time_t        hl_lastpong;    /* last response from a ping */
  GtkWidget     *listwidget;    /* the widget on the host list for this host */
  u_long        hl_lastsnmpif;
  time_t        hl_lastsnmp;    /* Last time we did any snmp on this node */
  gpointer      hl_queue_request; /* The queue process has a request running */
  gulong        hl_last_in;       /* last input octets */
  gulong        hl_last_out;      /* Last output octets */
  gulong        hl_ifspeed;       /* speed of the monitored if */
  time_t        hl_queue_last_time; /* Time of the last poll */
  time_t        hl_poll_interval;   /* Poll time in seconds */
  gfloat        hl_in_load;
  gfloat        hl_out_load;
  gfloat        hl_combined_load;
  /* --- SQL Information --- */
  int           rowid;          /* SQL row ID */
  gchar         *hl_modified;         /* Last modification date */
  gchar         *hl_added;            /* date added */
  gpointer	g_sqldb_private;	/* Private data owned by g_sqldb */
  gpointer      map_item;     

  /*
   * A pointer to the map_node structure of this object 
   */
  gpointer      hl_mnode;
} hosts;

/* 
 * The types of nodes supported in the map.
 * Plugins _should_ be able to extend this but I'm not sure how I want to
 * go about this.
 */
typedef enum {
  MAP_NODE_HOST,
  MAP_NODE_NETWORK,
  MAP_LABEL,
  MAP_NODE_LINK
} NodeType;

typedef enum {
  NODE_OK,
  NODE_WARN,
  NODE_CRIT
} NodeFlags;

/*
 * Everything on the map is described as a map node. This encapulates this
 * in a neat structure.
 */
typedef struct _map_node {
  NodeType            mn_type;
  NodeFlags           mn_flags;
  GnomeCanvasGroup    *mn_cgroup;
  gchar               *mn_name;
  gchar               *mn_mgroup;
  GList               *mn_links;
  /*
   * Per node specific data 
   */
  gpointer            mn_data;
} map_node;

/*
 * A simple structure to describe the map links
 */
typedef struct __map_links {
  GnomeCanvasGroup   *ml_from;
  GnomeCanvasGroup   *ml_to;
  GnomeCanvasItem    *ml_item;
} map_links;

typedef struct __net_entry {
  char            *nl_name;
  struct in_addr   nl_net;
  struct in_addr   nl_mask;
  int              nl_type;
  int              nl_speed;
  int              nl_flags;
  char             *nl_group;
  location         nl_location;
  map_node         *nl_mnode;
  gpointer	   map_item;
  int              rowid;          /* SQL Row id */
  gpointer      g_sqldb_private;        /* Private data owned by g_sqldb */

  gchar		  * hash_name;     /* Used exclusively by network_db.c */

} net_entry;
#endif

/* EOF */
