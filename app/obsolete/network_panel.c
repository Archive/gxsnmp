/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Network list panel
 */
#include <config.h>
#include <gnome.h>
#include "main.h"
#include "network_panel.h"
#include "gui.h"

#include "protos.h"

extern gxsnmp *app_info;

/*
 * Local variables for the network list panel
 *
 */
static netwk_panel    *net_list_panel = NULL;

static char *list_labels[] = {
  N_("Network"),
  N_("Broadcast"),
  N_("CIDR"),
  N_("Mask"),
  N_("Available Hosts"),
  N_("Active Hosts")
};

static int col_sizes [ELEMENTS (list_labels)];
#define PADDING 40

/*
 * Forward declarations for the network panel
 */
static void           create_net_list_panel        (void);
static gint           delete_net_list_panel        (GtkWidget    *widget,
						    GdkEvent     *e,
						    gpointer     data);
static void           close_net_list_cb            (GtkWidget    *widget,
						    gpointer     data);
static void           refresh_net_list_cb          (GtkWidget    *widget,
						    gpointer     data);

static void
create_net_list_panel ()
{
  GtkWidget    *button;
  GtkWidget    *frame;
  GtkWidget    *w_box;
  GtkWidget    *table;
  GtkWidget    *bar;
  GtkWidget    *label;
  GtkWidget    *button_box;
  GtkWidget    *clist;
  int          i, tmp;

  net_list_panel = (netwk_panel *)g_malloc (sizeof(netwk_panel));

  net_list_panel->window = gnome_app_new ("GXSNMP", _("Network List"));
  gtk_signal_connect (GTK_OBJECT (net_list_panel->window),
		      "delete_event",
		      GTK_SIGNAL_FUNC (delete_net_list_panel),
		      NULL);
  w_box = gtk_vbox_new (FALSE, 2);

  label = gtk_label_new (_("Known Networks"));
  gtk_box_pack_start (GTK_BOX (w_box), label, FALSE, FALSE, 0);

  bar = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (w_box), bar, FALSE, FALSE, 2);
  table = gtk_table_new (3, 3, FALSE);
  gtk_box_pack_start (GTK_BOX (w_box), table, TRUE, TRUE, 2);

  /*
   * Compute the minimum size needed to display xxx.xxx.xxx.xxx
   * plus some padding. 18 chars... The ip address + a space on either side
   */
  tmp = gdk_text_width (table->style->font,
			"1", 1);
  tmp = tmp * 18;                        
  for ( i = 0; i < ELEMENTS (list_labels); i++)
    {
      list_labels[i] = strdup (gettext(list_labels[i]));
      col_sizes[i] = MAX (tmp, gdk_string_width (label->style->font,
						 list_labels[i]));
#ifdef DEBUG_LAYOUT
      g_print ("Width of col %d is %d\n", i, col_sizes[i]);
#endif
    }
  clist = gtk_clist_new_with_titles 
    (ELEMENTS (list_labels), list_labels);
  gtk_object_set_data (GTK_OBJECT (net_list_panel->window), "net_list",
		       clist);
  gtk_object_set_data (GTK_OBJECT (net_list_panel->window), "clist_items",
		       (gint *)ELEMENTS (list_labels));
  gtk_clist_column_titles_passive (GTK_CLIST (clist));
  tmp = 0;
  for ( i = 0; i < ELEMENTS (list_labels); i++)
    {
      gtk_clist_set_column_width (GTK_CLIST (clist), 
				  i, (col_sizes[i]));
      /*
       * Now we can compute the size of the window (to be able to at least
       * see most of the clist...
       */
      tmp += col_sizes[i];
    }
  gtk_widget_set_usize (net_list_panel->window, tmp + PADDING, 200);
  gtk_table_attach (GTK_TABLE (table), clist, 0, 1, 0, 3,
		    GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 2, 1);
  bar = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 1, 2, 0, 3,
		    GTK_FILL , GTK_FILL | GTK_EXPAND, 2, 1);
  button = gtk_button_new_with_label (_("Edit"));
  gtk_table_attach (GTK_TABLE (table), button,
		    2, 3, 0, 1,
		    GTK_FILL | GTK_SHRINK, GTK_SHRINK, 2, 1);
  button = gtk_button_new_with_label (_("Delete"));
  gtk_table_attach (GTK_TABLE (table), button,
		    2, 3, 1, 2,
		    GTK_FILL | GTK_SHRINK, GTK_SHRINK, 2, 1);
  button = gtk_button_new_with_label (_("Layout #3"));
  gtk_table_attach (GTK_TABLE (table), button,
		    2, 3, 2, 3,
		    GTK_FILL | GTK_SHRINK, GTK_SHRINK, 2, 1);
  bar = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (w_box), bar, FALSE, FALSE, 0);
  frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (w_box), frame, FALSE, FALSE, 0);
  button_box = gtk_hbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (button_box), 2);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (button_box), 3);
  gtk_container_add (GTK_CONTAINER (frame), button_box);
  button = gnome_stock_button (GNOME_STOCK_BUTTON_CLOSE);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (close_net_list_cb),
		      NULL);
  gtk_container_add (GTK_CONTAINER (button_box), button);
  gnome_app_set_contents (GNOME_APP (net_list_panel->window), w_box);
  gtk_widget_show_all (net_list_panel->window);
}

/*
 * Callback functions
 */
static void
close_net_list_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_hide (net_list_panel->window);
}

static void 
refresh_net_list_cb (GtkWidget *widget, gpointer data)
{
}

static gint
delete_net_list_panel (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  if (net_list_panel)
    {
      gtk_widget_destroy (net_list_panel->window);
      g_free (net_list_panel);
      net_list_panel = NULL;
    }
  return TRUE;
}
/*
 * Global Standard panel functions.
 */
void
open_network_panel ()
{
  if (net_list_panel == NULL)
    {
      create_net_list_panel ();
      //      nl_populate_net_clist (net_list_panel->window);
    }
  else if (!GTK_WIDGET_VISIBLE (net_list_panel->window))
    gtk_widget_show (net_list_panel->window);
  reset_network_panel ();
}

void
destroy_network_panel ()
{
  if (net_list_panel != NULL)
    delete_net_list_panel (NULL, NULL, net_list_panel);
}

void
hide_network_panel ()
{
  if (net_list_panel->window)
    if (GTK_WIDGET_VISIBLE (net_list_panel->window))
      gtk_widget_hide (net_list_panel->window);
}

void
reset_network_panel ()
{
  refresh_net_list_cb (NULL, net_list_panel);
}

/*
 * Add network panel
 */
static GtkWidget         *create_add_network_panel      (gchar      *title);
static gint              delete_add_network_panel       (GtkWidget  *widget,
							 GdkEvent   *e,
							 gpointer   data);
/*
 * Callback functions 
 */
static void              add_network_ok_cb              (GtkWidget  *widget,
							 gpointer   data);
static void              add_network_can_cb             (GtkWidget  *widget,
							 gpointer   data);

static GtkWidget  *add_net_panel = NULL;

static GTableAttach add_net_table_label[] = {
  { 0, 1, 0, 1, GTK_FILL, 0, 0, 0 },
  { 0, 1, 1, 2, GTK_FILL, 0, 0, 0 },
  { 2, 3, 1, 2, GTK_FILL, 0, 0, 0 },
  { 0, 1, 2, 3, GTK_FILL, 0, 0, 0 },
  { 2, 3, 2, 3, GTK_FILL, 0, 0, 0 }
};

static GTableAttach add_net_table_entry[] = {
  { 1, 4, 0, 1, GTK_FILL, 0, 0, 0 },
  { 1, 2, 1, 2, GTK_FILL, 0, 0, 0 },
  { 3, 4, 1, 2, GTK_FILL, 0, 0, 0 },
  { 1, 2, 2, 3, GTK_FILL, 0, 0, 0 },
  { 3, 4, 2, 3, GTK_FILL, 0, 0, 0 }
};

static gchar *panel_tags[] = {
  "net_name",
  "net_address",
  "net_speed",
  "net_group",
  "net_mask"
};

static GTableDef add_net_table[] = {
  { N_("Name"),
    N_("This is the name that will be displayed on the network map. If blank "
     "then the network's address will be used."),    
    "net_name", GT_ENTRY, NULL, 
    &add_net_table_label[0], 
    &add_net_table_entry[0] },
  { N_("Address"),
    N_("This Should be the network's address."),
    "net_address", GT_ENTRY, NULL,
    &add_net_table_label[1],
    &add_net_table_entry[1] },
  { N_("Speed"),
    N_("Enter in the speed of the network or leave blank to have the program "
       "attempt to determine it."),
    "net_speed", GT_ENTRY, NULL,
    &add_net_table_label[2],
    &add_net_table_entry[2] },
  { N_("Mask"),
    N_("This should be the network mask for this network, if left blank the "
       "program will attempt to determine it."),
    "net_mask", GT_ENTRY, NULL, 
    &add_net_table_label[3],
    &add_net_table_entry[3] },
  { N_("Group"),
    N_("Select the group of this network for proper placement on the network "
       "map."),
    "net_group", GT_ENTRY, NULL,
    &add_net_table_label[4],
    &add_net_table_entry[4] },
  { NULL, NULL, NULL, GT_UI_END, NULL, NULL }
};
static GTCallbacks add_net_buttons[] = {
  { GT_BUTTON_OK, add_network_ok_cb, NULL, "ok_button" },
  { GT_BUTTON_CAN, add_network_can_cb, NULL, "cancel_button" },
  { GT_UI_END, NULL, NULL, NULL }
};

static GtkWidget *
create_add_network_panel (gchar *title)
{
  GtkWidget     *window_table;
  GtkWidget     *window_frame;
  GtkWidget     *vbox;
  GtkTooltips   *tooltips;

  if (add_net_panel)
    return add_net_panel;
  tooltips = gtk_tooltips_new ();

  add_net_panel = gnome_app_new ("GXSNMP", title);
  gtk_signal_connect (GTK_OBJECT (add_net_panel), "delete_event",
		      (GtkSignalFunc) delete_add_network_panel,
		      NULL);

  gtk_object_set_data (GTK_OBJECT (add_net_panel), "tooltips", tooltips);
  gtk_tooltips_enable (GTK_TOOLTIPS (tooltips));
  window_frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (window_frame), 4);
  window_table = gtk_table_new (3, 5, FALSE);
  gtk_container_border_width (GTK_CONTAINER (window_table), 4);
  gtk_table_set_row_spacings (GTK_TABLE (window_table), 3);
  gtk_table_set_col_spacings (GTK_TABLE (window_table), 3);
  gtk_container_add (GTK_CONTAINER (window_frame), window_table);
  gnome_app_set_contents (GNOME_APP (add_net_panel), window_frame);

  window_frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (window_frame), 4);
  gtk_table_attach (GTK_TABLE (window_table), window_frame,
		    4, 5, 0, 3,
		    GTK_FILL, GTK_FILL, 0, 0);
  vbox = gtk_vbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (vbox), 10);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (vbox), GTK_BUTTONBOX_START);
  gtk_container_add (GTK_CONTAINER (window_frame), vbox);
  gt_add_action_buttons (add_net_buttons, vbox);
  gt_build_ui_table (add_net_table, window_table);
  gtk_widget_show_all (add_net_panel);
  return add_net_panel;
}

static void
clear_add_network_panel ()
{
  GtkWidget   *entry;
  int i;
  
  for (i = 0; i < ELEMENTS (panel_tags); i++)
    {
      entry = get_widget (add_net_panel, panel_tags[i]);
      if (entry)
	{
	  gtk_entry_set_text (GTK_ENTRY (entry), "");
	}
    }
}

/*
 * Callback functions
 */

static gint
delete_add_network_panel (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  if (add_net_panel)
    {
      gtk_widget_destroy (add_net_panel);
      add_net_panel = NULL;
    }
  return TRUE;
}

static void
add_network_ok_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *e_widget;
  net_entry   *network;

  gtk_widget_hide (add_net_panel);
  if (gtk_object_get_data (GTK_OBJECT (add_net_panel), "is_editing"))
    {
      /* 
       * ---------------- Editing a network entry ---------------
       *  All of the hash table housekeeping is now automatically done
       *  by the network_db functions.
       */
      network = gtk_object_get_data (GTK_OBJECT (add_net_panel), "net_entry");
      if ( (e_widget = get_widget (add_net_panel, "net_name")) )
        {
	  g_free (network->nl_name);
	  network->nl_name = gtk_entry_get_text (GTK_ENTRY (e_widget));
	}
      if ( (e_widget = get_widget (add_net_panel, "net_address")) )
	{
	  network->nl_net.s_addr = inet_addr (gtk_entry_get_text 
					      (GTK_ENTRY (e_widget)));
	}
      if ( (e_widget = get_widget (add_net_panel, "net_mask")) )
	{
	  network->nl_mask.s_addr = inet_addr (gtk_entry_get_text
					       (GTK_ENTRY (e_widget)));
	}
/*      network_db_update_entry (&app_info->network_db,
			       app_info->network_database,
			       network); */
    }
  else
    {
      /*
       * ---------------------- Adding a new entry ----------------------
       */
      network = (net_entry *)g_malloc (sizeof (net_entry));
      network->nl_location.x = 200;
      network->nl_location.y = 200;
      if ( (e_widget = get_widget (add_net_panel, "net_name")) )
	{
	 network->nl_name = g_strdup (gtk_entry_get_text(GTK_ENTRY (e_widget)));
	}
      if ( (e_widget = get_widget (add_net_panel, "net_address")) )
	{
	  network->nl_net.s_addr = inet_addr (gtk_entry_get_text (GTK_ENTRY 
							      (e_widget)));
	}
      if ( (e_widget = get_widget (add_net_panel, "net_mask")) )
	{
	  network->nl_mask.s_addr = inet_addr (gtk_entry_get_text (GTK_ENTRY
							    (e_widget)));
	}

/*      map_add_node (MAP_NODE_NETWORK, network);
      network_db_add_entry (&app_info->network_db,
                            app_info->network_database,
                            network); */
    }

}

static void
add_network_can_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_hide (add_net_panel);
}

/*
 * Global panel functions (add network)
 */
void 
open_add_network_panel ()
{
  if (!add_net_panel)
    {
      create_add_network_panel (_("Add a new network"));
      gtk_object_set_data (GTK_OBJECT (add_net_panel), "is_editing",
			   (gboolean *)FALSE);
    }
  else if (!GTK_WIDGET_VISIBLE (add_net_panel))
    {
      gtk_object_set_data (GTK_OBJECT (add_net_panel), "is_editing", 
			   (gboolean *)FALSE);
      gtk_window_set_title (GTK_WINDOW (add_net_panel), 
			    _("Add a new network"));
      clear_add_network_panel ();
      gtk_widget_show (add_net_panel);
    }
}

void 
destroy_add_net_panel ()
{
  
}

void
hide_add_net_panel ()
{
  if (add_net_panel)
    if (GTK_WIDGET_VISIBLE (add_net_panel))
      gtk_widget_hide (add_net_panel);
}

void 
reset_add_net_panel ()
{
}

/*
 * Edit network panel (reuse the add network panel)
 */

void
open_edit_network_panel ()
{
  if (!add_net_panel)
    {
      create_add_network_panel (_("Edit Network"));
      gtk_object_set_data (GTK_OBJECT (add_net_panel), "is_editing",
			   (gboolean *)TRUE);
    }
  else if (!GTK_WIDGET_VISIBLE (add_net_panel))
    {
      gtk_window_set_title (GTK_WINDOW (add_net_panel), _("Edit Network"));
      gtk_object_set_data (GTK_OBJECT (add_net_panel), "is_editing",
			   (gboolean *)TRUE);
      gtk_widget_show (add_net_panel);
    }
}

void
network_panel_do_edit (net_entry *network)
{
  GtkWidget *widget;

  gtk_object_set_data (GTK_OBJECT (add_net_panel), "net_entry", network);
  if ( (widget = get_widget (add_net_panel, "net_name")) )
    gtk_entry_set_text (GTK_ENTRY (widget), network->nl_name);
  if ( (widget = get_widget (add_net_panel, "net_address")) )
    gtk_entry_set_text (GTK_ENTRY (widget), inet_ntoa(network->nl_net));
  if ( (widget = get_widget (add_net_panel, "net_mask")) )
    gtk_entry_set_text (GTK_ENTRY (widget), inet_ntoa(network->nl_mask));
}

/* EOF */
