/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (c) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Adding and editing nodes.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <gnome.h>
#include <g_snmp.h>

#include "structs.h"
#include "main.h"
#include "gui.h"
#include "protos.h"
#include "snmp_host.h"
#include "host_db.h"


extern gxsnmp *app_info;

static GtkWidget *add_host_panel = NULL;
/*
 * Forward declarations
 */
static GtkWidget     *create_add_host_panel     (gchar        *title);


/*
 * Callback functions 
 */
static gint          delete_add_host_panel      (GtkWidget    *widget,
						 GdkEvent     *e,
						 gpointer     data);
static void          add_host_ok_cb             (GtkWidget    *widget,
						 gpointer     data);
static void          add_host_can_cb            (GtkWidget    *widget,
						 gpointer     data);
static void          add_host_enable_snmp_cb    (GtkWidget    *widget,
						 gpointer     data);
static void          add_host_auto_place_cb     (GtkWidget    *widget,
						 gpointer     data);
static void          add_host_pixmap_file_cb    (GtkWidget    *widget,
						 gpointer     data);
static GTCallbacks add_host_buttons[] = {
  { GT_BUTTON_OK, add_host_ok_cb, NULL, "ok_button" },
  { GT_BUTTON_CAN, add_host_can_cb, NULL, "cancel_button" },
  { GT_UI_END, NULL, NULL, NULL }
};

/* 
 * General frame widgets
 */

static gchar *general_tags[] = {
  "hostname",
  "dispname",
  "group",
  "pixmapfile_entry",
  "netmask"
};

/*
 * Map frame widgets 
 */
static gchar *placement_tags[] = {
  "auto_placement",
  "auto_connection",
  "host_xpos",
  "host_ypos"
};

/*
 * SNMP frame widgets
 */
static gchar *snmp_tags[] = {
  "snmp_enable",
  "snmp_transport",
  "snmp_read_comm",
  "snmp_write_comm",
  "snmp_version",
  "snmp_port",
  "snmp_retries",
  "snmp_timeout"
};

static GtkWidget *
create_add_host_panel (gchar *title)
{
  GtkWidget       *window_frame;
  GtkWidget       *window_table;
  GtkWidget       *work_table;
  GtkWidget       *action_box;
  GtkWidget       *event_box;
  GtkWidget       *label;
  GtkWidget       *entry;
  GtkWidget       *wentry;

  GtkTooltips     *tooltips;
  gchar           *node_pixmap_filename;

  if (add_host_panel)
    return add_host_panel;

  add_host_panel = gnome_app_new ("GXSNMP", title);
  gtk_widget_set_name (GTK_WIDGET (add_host_panel), "add_host_panel");
  gtk_signal_connect (GTK_OBJECT (add_host_panel), "delete_event",
		      (GtkSignalFunc) delete_add_host_panel,
		      NULL);
  tooltips = gtk_tooltips_new ();
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "tooltips", tooltips);
  gtk_tooltips_enable (GTK_TOOLTIPS (tooltips));

  window_table = gtk_table_new (3, 3, FALSE);
  gtk_container_border_width (GTK_CONTAINER (window_table), 5);
  gtk_table_set_row_spacings (GTK_TABLE (window_table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (window_table), 4);
  gnome_app_set_contents (GNOME_APP (add_host_panel), window_table);
  /*
   * General settings (host name, display name, group etc.. 
   */
  window_frame = gtk_frame_new (_("General Settings"));
  gtk_table_attach (GTK_TABLE (window_table), window_frame, 0, 1, 0, 1,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_frame_set_shadow_type (GTK_FRAME (window_frame), GTK_SHADOW_IN);
  work_table = gtk_table_new (6, 2, FALSE);
  gtk_container_border_width (GTK_CONTAINER (work_table), 5);
  gtk_table_set_row_spacings (GTK_TABLE (work_table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (work_table), 4);
  gtk_container_add (GTK_CONTAINER (window_frame), work_table);
  /* --- HOSTNAME --- */
  label = gtk_label_new (_("Name"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
  event_box = gtk_event_box_new ();
  gtk_container_add (GTK_CONTAINER (event_box), label);
  gtk_tooltips_set_tip (tooltips, event_box, _("The DNS resolvable hostname "
					       "for this node."), NULL);
  gtk_table_attach (GTK_TABLE (work_table), event_box, 0, 1, 0, 1,
		    GTK_FILL, 0, 0, 0 );
  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "hostname", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0 );
  /* --- NETMASK --- */
  label = gtk_label_new (_("Netmask"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
  event_box = gtk_event_box_new ();
  gtk_container_add (GTK_CONTAINER (event_box), label);
  gtk_tooltips_set_tip (tooltips, event_box, _("The netmask for this host."), 
			NULL);
  gtk_table_attach (GTK_TABLE (work_table), event_box, 0, 1, 1, 2,
		    GTK_FILL, 0, 0, 0);
  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "netmask", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 1, 2, 1, 2,
		    GTK_FILL, 0, 0, 0);
  /* --- DISPLAY NAME --- */
  label = gtk_label_new (_("Display Name"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
  event_box = gtk_event_box_new ();
  gtk_container_add (GTK_CONTAINER (event_box), label);
  gtk_tooltips_set_tip (tooltips, event_box, _("The text for the label that "
					       "will be displayed on the map "
					       "for this node."), NULL);
  gtk_table_attach (GTK_TABLE (work_table), event_box, 0, 1, 2, 3,
		    GTK_FILL, 0, 0, 0);
  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "dispname", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 1, 2, 2, 3,
		    GTK_FILL, 0, 0, 0);
  /* --- MAP GROUP --- */
  label = gtk_label_new (_("Map Group"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
  event_box = gtk_event_box_new ();
  gtk_container_add (GTK_CONTAINER (event_box), label);
  gtk_tooltips_set_tip (tooltips, event_box, _("The map group this node should"
					       " be added to."), NULL);
  gtk_table_attach (GTK_TABLE (work_table), event_box, 0, 1, 3, 4,
		    GTK_FILL, 0, 0, 0);
  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "group", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 1, 2, 3, 4,
		    GTK_FILL, 0, 0, 0);
    /* --- PIXMAP --- */
  label = gtk_label_new (_("Pixmap filename"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
  event_box = gtk_event_box_new ();
  gtk_container_add (GTK_CONTAINER (event_box), label);
  gtk_tooltips_set_tip (tooltips, event_box, _("The filename, including the "
					       "full path, for the pixmap for "
					       "this node on the map."), NULL);
  gtk_table_attach (GTK_TABLE (work_table), event_box, 0, 1, 4, 5,
		    GTK_FILL, 0, 0, 0);
  entry = gnome_pixmap_entry_new ("pixmap", _("Browse"), TRUE);
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "pixmapfile", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 1, 2, 4, 5,
		    GTK_FILL, 0, 0, 0);
  node_pixmap_filename = gnome_unconditional_pixmap_file ("desktop.xpm");
  wentry = gnome_pixmap_entry_gtk_entry (GNOME_PIXMAP_ENTRY (entry));
  gtk_entry_set_text (GTK_ENTRY (wentry), node_pixmap_filename);
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "pixmapfile_entry",
		       wentry);

  /*
   * SNMP Settings frame
   */
  gtk_table_attach (GTK_TABLE (window_table), window_frame,
		    1, 2, 0, 1,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  /*
   * Map placement frame
   */
  window_frame = gtk_frame_new (_("Map Placement"));
  gtk_frame_set_shadow_type (GTK_FRAME (window_frame), GTK_SHADOW_IN);
  gtk_table_attach (GTK_TABLE (window_table), window_frame,
		    0, 1, 1, 2,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  work_table = gtk_table_new (2, 3, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (work_table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (work_table), 4);
  gtk_container_border_width (GTK_CONTAINER (work_table), 5);
  gtk_container_add (GTK_CONTAINER (window_frame), work_table);
  /* -- AUTOPLACEMENT -- */
  entry = gtk_check_button_new_with_label (_("Auto placement"));
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "auto_placement",
		       entry);
  gtk_signal_connect (GTK_OBJECT (entry), "clicked", 
		      (GtkSignalFunc) add_host_auto_place_cb,
		      NULL);
  gtk_table_attach (GTK_TABLE (work_table), entry, 0, 1, 0, 1, 
		    GTK_FILL, 0, 0, 0);
  /* -- X Position -- */
  label = gtk_label_new (_("X position"));
  gtk_table_attach (GTK_TABLE (work_table), label, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);
  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "host_xpos", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 2, 3, 0, 1,
		    GTK_FILL, 0, 0, 0);
  /* -- Auto connection -- */
  entry = gtk_check_button_new_with_label (_("Auto connection"));
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "auto_connection", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 0, 1, 1, 2,
		    GTK_FILL, 0, 0, 0);
  
  /* -- Y Position --*/
  label = gtk_label_new (_("Y position"));
  gtk_table_attach (GTK_TABLE (work_table), label, 1, 2, 1, 2,
		    GTK_FILL, 0, 0, 0);
  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "host_ypos", entry);
  gtk_table_attach (GTK_TABLE (work_table), entry, 2, 3, 1, 2,
		    GTK_FILL, 0, 0, 0);
  /*
   * Database frame
   */
  window_frame = gtk_frame_new (_("Database Settings"));
  gtk_frame_set_shadow_type (GTK_FRAME (window_frame), GTK_SHADOW_IN);
  gtk_table_attach (GTK_TABLE (window_table), window_frame,
		    1, 2, 1, 2,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  work_table = gtk_table_new (3, 3, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (work_table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (work_table), 4);
  gtk_container_border_width (GTK_CONTAINER (work_table), 4);
  gtk_container_add (GTK_CONTAINER (window_frame), work_table);

  /* 
   * ----- Action area frame
   */
  window_frame = gtk_frame_new (NULL);
  gtk_table_attach (GTK_TABLE (window_table), window_frame,
		    2, 3, 0, 2,
		    0, GTK_FILL, 0, 0);
  action_box = gtk_vbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (action_box), 5);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (action_box), GTK_BUTTONBOX_START);
  gtk_container_add (GTK_CONTAINER (window_frame), action_box);

  /* -- Buttons -- */
  gt_add_action_buttons (add_host_buttons, action_box);

  gtk_widget_show_all (add_host_panel);
  return add_host_panel;
}

/* --- GENERAL INFORMATION FRAMES --- */
void 
clear_general_frame (GtkWidget *parent)
{
  GtkWidget   *work_widget;
  int         i;

  for (i = 0; i < ELEMENTS (general_tags); i++)
    {
      work_widget = get_widget (parent, general_tags[i]);
      if (work_widget)
	gtk_entry_set_text (GTK_ENTRY (work_widget), "");
    }
}

void 
set_general_frame (GtkWidget *parent, hosts *host)
{
  GtkWidget   *work_widget;
  int         i;

  for (i = 0; i < ELEMENTS (general_tags); i++)
    {
      work_widget = get_widget (parent, general_tags[i]);
      if (work_widget)
	{
	  switch (i)
	    {
	    case 0:              /* name */
	      gtk_entry_set_text (GTK_ENTRY (work_widget), 
				  host->hl_snmp.name);
	      break;
	    case 1:              /* display name */
	      gtk_entry_set_text (GTK_ENTRY (work_widget), host->hl_disp);
	      break;
	    case 2:             /* group */
	      gtk_entry_set_text (GTK_ENTRY (work_widget), host->hl_group);
	      break;
	    case 3:            /* pixmap filename */

	      break;
	    case 4:            /* netmask */
	      gtk_entry_set_text (GTK_ENTRY (work_widget), 
					     inet_ntoa (host->hl_netmask));
	      
	      break;
	    default:
	      break;
	    }
	}
    }
}

void
get_general_frame (GtkWidget *parent, hosts *host)
{
  GtkWidget  *work_widget;
  int        i;

  if (host)
    {
      for (i = 0; i < ELEMENTS (general_tags); i++)
	{
	  if ( (work_widget = get_widget (parent, general_tags[i])) )
	    {
	      switch (i)
		{
		case 0:
		  if (host->hl_snmp.name)
		    {
		      if (strcmp (host->hl_snmp.name,
				  gtk_entry_get_text (GTK_ENTRY 
						      (work_widget))) != 0)
			{
			  /* We hash on the hostname so we need to re-hash if 
			   * it Changes. 
			   */
			  hl_del_host (host);
			  g_free (host->hl_snmp.name);
			  host->hl_snmp.name = g_strdup (gtk_entry_get_text 
							 (GTK_ENTRY 
							  (work_widget)));
			  hl_add_host (host);
			}
		    }
		  else
		    {   /* must be a new host */
		      host->hl_snmp.name = g_strdup (gtk_entry_get_text
						    (GTK_ENTRY (work_widget)));
		    }
		  break;
		case 1:
		  if (host->hl_disp)
		    {
		    if (strcmp (host->hl_disp, gtk_entry_get_text 
				(GTK_ENTRY (work_widget))) != 0)
		      {
			g_free (host->hl_disp);
			host->hl_disp = g_strdup (gtk_entry_get_text 
						  (GTK_ENTRY (work_widget)));
			map_set_node_label (host->hl_mnode, host->hl_disp,
					    "firebrick");
		      }
		    }
		  else
		    {
		      host->hl_disp = g_strdup (gtk_entry_get_text 
						(GTK_ENTRY (work_widget)));
		    }
		  break;
		case 2:
		  if (host->hl_group)
		    {
		      if (strcmp (host->hl_group, gtk_entry_get_text
				  (GTK_ENTRY (work_widget))) != 0)
			{
			  g_free (host->hl_group);
			  host->hl_group = g_strdup (gtk_entry_get_text
						    (GTK_ENTRY (work_widget)));
			}
		    }
		  else
		    {
		      host->hl_group = g_strdup (gtk_entry_get_text 
						 (GTK_ENTRY (work_widget)));
		    }
		  break;
		case 3:                /* pixmap filename */
		  host->pixmap_file = g_strdup (gtk_entry_get_text 
						(GTK_ENTRY (work_widget)));
		  g_print ("Pixmap filename is %s\n", host->pixmap_file);
		  break;
		case 4:                /* netmask */
		  host->hl_netmask.s_addr = quad_to_ulong 
		    (gtk_entry_get_text	(GTK_ENTRY (work_widget)));
		  
		  break;
		default:
		  break;
		}
	    }
	}
    }
}


/* --- MAP PLACEMENT FRAME --- */
void
clear_map_frame (GtkWidget *parent)
{
  GtkWidget  *work_widget;
  int        i;
  
  for (i = 0; i < ELEMENTS (placement_tags); i++)
    {
      work_widget = get_widget (parent, placement_tags[i]);
      if (work_widget);
      switch (i)
	{
	case 0:
	  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (work_widget), FALSE);
	  break;
	case 1:
	  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (work_widget), FALSE);
	  break;
	case 2:
	  gtk_entry_set_text (GTK_ENTRY (work_widget), "");
	  break;
	case 3:
	  gtk_entry_set_text (GTK_ENTRY (work_widget), "");
	  break;
	default:
	  break;
	}
    }
}

void
set_map_frame (GtkWidget *parent, location *hl_location)
{
  GtkWidget    *work_widget;
  int          i;
  hosts        *host;
  gchar        buf[10];

  host = (hosts *)gtk_object_get_data (GTK_OBJECT (parent), "selected_host");
  for (i = 0; i < ELEMENTS (placement_tags); i++)
    {
      work_widget = get_widget (parent, placement_tags[i]);
      if (work_widget)
	{
	  switch (i)
	    {
	    case 0:
	      if (host)
		{
		  if (host->hl_flags & GX_AUTOPLACEMENT)
		    {
		      gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON 
						   (work_widget),
						   TRUE);
		    }
		  else
		    {
		      gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON 
						   (work_widget),
						   FALSE);
		    }
		}
	      else
		{
		  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON 
					       (work_widget),
					       TRUE);
		}
	      break;
	    case 1:
	      if (host)
		{
		  if (host->hl_flags & GX_AUTOCONNECT)
		    {
		      gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON 
						   (work_widget),
						   TRUE);
		    }
		  else
		    {
		      gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON 
						   (work_widget),
						   FALSE);
		    }
		}
	      else
		{
		  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
					       (work_widget),
					       TRUE);
		}
	      break;
	    case 2:
	      snprintf (buf, sizeof(buf), "%f", hl_location->x);
	      gtk_entry_set_text (GTK_ENTRY (work_widget), buf);
	      break;
	    case 3:
	      snprintf (buf, sizeof(buf), "%f", hl_location->y);
	      gtk_entry_set_text (GTK_ENTRY (work_widget), buf);
	      break;
	    default:
	      break;
	    }
	}
    }
}

void
get_map_frame (GtkWidget *parent, location *hl_location)
{
  GtkWidget   *work_widget;
  int         i;
  hosts       *host;
  double      old_x, old_y;
  
  host = (hosts *)gtk_object_get_data (GTK_OBJECT (parent), "selected_host");
  for (i = 0; i < ELEMENTS (placement_tags); i++)
    {
      old_x = hl_location->x;
      old_y = hl_location->y;
      work_widget = get_widget (parent, placement_tags[i]);
      if (work_widget)
	{
	  switch (i)
	    {
	    case 0:
	      if (host)
		{
		  if (GTK_TOGGLE_BUTTON (work_widget)->active)
		    {
		      if (host->hl_flags & GX_AUTOPLACEMENT)
			break;
		      else
			host->hl_flags |= GX_AUTOPLACEMENT;
		    }
		  else
		    {
		      host->hl_flags ^= GX_AUTOPLACEMENT;
		    }
		}
	      break;
	    case 1:
	      if (host)
		{
		  if (GTK_TOGGLE_BUTTON (work_widget)->active)
		    {
		      if (host->hl_flags & GX_AUTOCONNECT)
			break;
		      else
			host->hl_flags |= GX_AUTOCONNECT;
		    }
		  else
		    host->hl_flags ^= GX_AUTOCONNECT;;
		}
	      break;
	    case 2:
	      if (atof (gtk_entry_get_text (GTK_ENTRY (work_widget))) != old_x)
		{
		  hl_location->x = atof (gtk_entry_get_text (GTK_ENTRY 
							     (work_widget)));
		  host->hl_state |= MAP_UPDATE_NEEDED;
		}
	      break;
	    case 3:
	      if (atof (gtk_entry_get_text (GTK_ENTRY (work_widget))) != old_y)
		{
		  hl_location->y = atof (gtk_entry_get_text (GTK_ENTRY
							     (work_widget)));
		  if (host->hl_state & MAP_UPDATE_NEEDED)
		    host->hl_state |= MAP_UPDATE_NEEDED;
		}
	      break;
	    default:
	      break;
	    }
	}
    }
}

/* 
* Misc support Functions.
 */
void
clear_add_host_panel ()
{
  clear_general_frame (add_host_panel);
  clear_map_frame (add_host_panel);
}

/*
 * Callback funstions
 */
static gint
delete_add_host_panel (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  if (add_host_panel)
    {
      gtk_widget_destroy (add_host_panel);
      add_host_panel = NULL;
    }
  return TRUE;
}

static void 
add_host_ok_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget     *parent;
  hosts         *host;
  map_node      *node;

  if (widget->parent)
    parent = gtk_widget_get_toplevel (widget);
  else
    parent = widget;

  gtk_widget_hide (parent);

  if ( (gtk_object_get_data (GTK_OBJECT (parent), "is_editing")) )
    {
      host = (hosts *)gtk_object_get_data (GTK_OBJECT (parent), 
					   "selected_host");
      
      if (host)
	{
	  get_general_frame (parent, host);
	  get_map_frame (parent, &host->hl_location);
	  node = host->hl_mnode;
	  
	  host_pixmap (node->mn_cgroup, host->pixmap_file, 0.0, 0.0);
	  if (host->hl_state && MAP_UPDATE_NEEDED)
	    {
	      /* FIXME: gnome_canvas_item_move (host->hl_mnode, host->hl_location.x,
		host->hl_location.y); */
	      map_move_node (host->hl_mnode, host->hl_location.x, 
			     host->hl_location.y);
	      host->hl_state &= MAP_UPDATE_NEEDED;
	    }
	}
    }
  else
    {
      g_print ("Adding a new host\n");
      host = (hosts *)g_malloc (sizeof (hosts));
      host->hl_snmp.name  = NULL;
      host->hl_snmp.rcomm = NULL;
      host->hl_snmp.wcomm = NULL;
      host->hl_disp       = NULL;
      host->hl_group      = NULL;
      host->pixmap_file   = NULL;
      host->hl_sysloc     = g_strdup("");
      host->hl_syscon     = g_strdup("");
      host->hl_flags      = 0;
      gtk_object_set_data (GTK_OBJECT (parent), "selected_host", host);
      get_general_frame (parent, host);
      get_map_frame (parent, &host->hl_location);
      hl_add_host (host);
      g_print ("Calling map_add_node...\n");
      map_add_host (host, 0, 0);
    }

  if (gtk_object_get_data (GTK_OBJECT (parent), "is_editing"))
    host_db_update_entry (host);    
  else
    g_sqldb_row_add (host_sqldb, host);

  gtk_object_remove_data (GTK_OBJECT (parent), "selected_host");
}

static void
add_host_can_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget    *parent;

  if (widget->parent)
    parent = gtk_widget_get_toplevel (widget);
  else
    parent = widget;

  gtk_widget_hide (parent);
  gtk_object_remove_data (GTK_OBJECT (parent), "selected_host");
}

static void
add_host_enable_snmp_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget    *parent;
  GtkWidget    *work_widget;
  int          i;
  gboolean     state;

  if (widget->parent)
    parent = gtk_widget_get_toplevel (widget);
  else
    parent = widget;
  if (GTK_TOGGLE_BUTTON (widget)->active)
    state = TRUE;
  else
    state = FALSE;
  for (i = 1; i < ELEMENTS (snmp_tags); i++)
    {
      work_widget = get_widget (parent, snmp_tags[i]);
      if (work_widget)
	gtk_widget_set_sensitive (GTK_WIDGET (work_widget),
				  state);
    }

}

static void
add_host_auto_place_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget    *parent;
  GtkWidget    *work_widget;
  int          i;
  gboolean     state;

  if (widget->parent)
    parent = gtk_widget_get_toplevel (widget);
  else
    parent = widget;
  if (GTK_TOGGLE_BUTTON (widget)->active)
    state = FALSE;
  else
    state = TRUE;
  for (i = 2; i < ELEMENTS (placement_tags); i++)
    {
      work_widget = get_widget (parent, placement_tags[i]);
      if (work_widget)
	gtk_widget_set_sensitive (GTK_WIDGET (work_widget), state);
    }
}

static void
add_host_pixmap_file_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *filename_entry;

  filename_entry = gtk_object_get_data (GTK_OBJECT (data), "pixmapfile_entry");
  g_print ("Add host pixmap filename changed?\n");
  g_print ("%s\n", gtk_entry_get_text (GTK_ENTRY (filename_entry)));
}

/*
 * Global support functions
 */
void
open_add_host_panel ()
{
  if (!add_host_panel)
    {
      create_add_host_panel (_("Add a new host"));
      gtk_object_set_data (GTK_OBJECT (add_host_panel), "is_editing",
			   GINT_TO_POINTER (FALSE));
      clear_add_host_panel ();
    }
  else
    {
      gtk_object_set_data (GTK_OBJECT (add_host_panel), "is_editing", 
			   GINT_TO_POINTER (FALSE));
      if (!GTK_WIDGET_VISIBLE (add_host_panel))
	{
	  gtk_window_set_title (GTK_WINDOW (add_host_panel), 
				_("Add a new host"));
	  gtk_widget_show (add_host_panel);
	  clear_add_host_panel ();
	}
      else
	{
	  gtk_window_set_title (GTK_WINDOW (add_host_panel), _("Add a new host"));
	}
    }
}

void 
destroy_add_host_panel ()
{
  if (add_host_panel)
    {
      gtk_widget_destroy (add_host_panel);
      add_host_panel = NULL;
    }
}

void 
hide_add_host_panel ()
{
  if (add_host_panel)
    if (GTK_WIDGET_VISIBLE (add_host_panel))
      gtk_widget_hide (add_host_panel);
}

void
reset_add_host_panel ()
{
}

/*
 * Editing a host
 */
void
open_edit_host_panel ()
{
  if (!add_host_panel)
    {
      create_add_host_panel (_("Edit a host"));
      gtk_object_set_data (GTK_OBJECT (add_host_panel), "is_editing",
			   (gboolean *)TRUE);
    }
  else
    {
      gtk_object_set_data (GTK_OBJECT (add_host_panel), "is_editing", 
			   (gboolean *)TRUE);
      if (!GTK_WIDGET_VISIBLE (add_host_panel))
	{
	  gtk_window_set_title (GTK_WINDOW (add_host_panel), _("Edit a host"));
	  gtk_widget_show (add_host_panel);
	  
	}
      else
	{
	  gtk_window_set_title (GTK_WINDOW (add_host_panel), _("Edit a host"));
	}
    }
}

void 
destroy_edit_host_panel ()
{
  if (add_host_panel)
    {
      gtk_widget_destroy (add_host_panel);
      add_host_panel = NULL;
    }
}

void
hide_edit_host_panel ()
{
  if (add_host_panel)
    {
      if (GTK_WIDGET_VISIBLE (add_host_panel))
	gtk_widget_hide (add_host_panel);
    }
}
void
edit_host_panel_edit (hosts *host)
{
  gtk_object_set_data (GTK_OBJECT (add_host_panel), "selected_host", host);
  set_general_frame (add_host_panel, host);
  set_map_frame (add_host_panel, &host->hl_location);
}

/* EOF */
