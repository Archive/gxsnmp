/*
**  $Id$
**
**  GXSNMP -- An snmp mangament application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place - Suite 330 , Cambridge, MA 02139, USA.
**
**  Subroutines for accessing the network database
*/

#ifndef __NETWORK_DB_H__
#define __NETWORK_DB_H__

BEGIN_GNOME_DECLS

#include <glib.h>
#include <g_sql.h>
#include "structs.h"

/*******************************************************************************
**
**  Structure of a network object in memory
**
*******************************************************************************/

typedef struct __net_entry DB_networks;	

/*******************************************************************************
**
**  Subroutines to access the SNMP configuration database
**
*******************************************************************************/

gboolean	network_db_load_table   (G_sql		* network_db,
					 gchar		* network_database);

gboolean	network_db_add_entry    (G_sql		* network_db,
					 gchar		* network_database,
					 DB_networks    * entry);

gboolean	network_db_update_entry (G_sql		* network_db,
					 gchar		* network_database,
					 DB_networks    * entry);

gboolean	network_db_delete_entry (G_sql		* network_db,
					 gchar		* network_database,
					 DB_networks    * entry);

gboolean	network_db_unique	(DB_networks	* entry);

GList         * network_db_list_all	(void);

void		network_db_terminate    (void);

END_GNOME_DECLS

#endif /* __NETWORK_DB_H__ */
/* EOF */
