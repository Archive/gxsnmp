/*
 * Definitions for the variables as defined in the MIB
 */
/***********************************************************
	Copyright 1988, 1989 by Carnegie Mellon University

                      All Rights Reserved

Permission to use, copy, modify, and distribute this software and its 
documentation for any purpose and without fee is hereby granted, 
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in 
supporting documentation, and that the name of CMU not be
used in advertising or publicity pertaining to distribution of the
software without specific, written prior permission.  

CMU DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
CMU BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.
******************************************************************/

#ifndef MIB_H
#define MIB_H

#include "g_snmp.h"

#define MIB 1, 3, 6, 1, 2, 1

extern struct tree * Mib;

extern void          init_mib        (void);

extern void          print_variable  (gulong      * objid, 
				      int           objidlen, 
				      SNMP_OBJECT * variable);

extern char        * snmp_new_prefix (char        * prefix);

extern void          print_objid     (gulong 	  * objid, 
				      int 	    objidlen);

extern int           read_objid      (char        * input, 
				      gulong 	  * output, 
				      int 	  * out_len);
		
extern struct tree * get_mib_tree    (gulong      * objid, 
				      gint 	    objidlen);

#endif

/* EOF */

