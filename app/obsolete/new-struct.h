/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Specfic structure definitions
 */

#ifndef __STRUCTS_H__
#define __STRUCTS_H__
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>
#include <netdb.h>
#include <netinet/in.h>

#include <g_snmp.h>

typedef struct _NetAddr           NetAddr;        /* ip/ipx etc address */

typedef struct _MapLocation       MapLocation;    /* Location on the map */
typedef struct _MapNode           MapNode;        /* a node on the map */
typedef struct _MapLink           MapLink;        /* A link on the map */
typedef struct _InfList           InfList;        /* Interface entry */
typedef struct _NodeId            NodeId;         /* Common node information */
typedef struct _DBEntry           DBEntry;        /* Common Database stuff */
typedef struct _Group             Group;          /* Node grouping */
typedef struct _Host              Host;           /* A host node */
typedef struct _Network           Network;        /* A network node */
typedef struct _MonitorMib        MonitorMib;     /* Monitoring */
typedef struct _NodeHealth        NodeHealth;     /* Health reporting */

struct _NetAddr
{
  gint   if_domain;
  gint   if_addrlen;
  gchar  if_addr[1];
};


/* Structure describing the location of a given object on a map */
struct _MapLocation {
  gint              ml_map_id;         /* Id of the map */
  gdouble           ml_xpos;           /* X coordinate of the object */
  gdouble           ml_ypos;           /* Y coordinate of the object */
  gint              ml_width;
  gint              ml_height;
};

/* The node on the map */
struct _MapNode {
  NodeType         mn_type;             /* Node type */
  NodeFlags        mn_flags;
  MapLocation      mn_loc;
  gpointer         mn_cgroup;
  gchar            *mn_name;
  /* Map group */
  gint             mn_mgroup_id;
  GList            *mn_links;           /* All the links to other nodes */
  /* Per node specific data */
  gpointer         mn_data;
};

/* Links on the map */
struct _MapLink {
  gpointer    ml_from;
  gpointer    ml_to;
  gpointer    ml_item;
};

/* Structure describing the interfaces on a node. */
struct _InfList {
  gint             if_type;       /* what kind of interface is this */
  gint             if_state;      /* Up/Down/Admin Down etc.. */
  gint             if_flags;
  gint             if_transport;
  gint             if_speed;
  gint             if_last_response;  /* Last time we got a response */
  NetAddr          if_addr;
  NetAddr          if_mask;
  InfList          *if_far_end;    /* The other end of this interface */
};

/* Structure describing the common entries for each object */
struct _NodeId {
  gchar            *ni_dn_name;    /* display name for this object */
  gchar            *ni_in_name;    /* Internet name (FQDN) */
  gint             ni_grp_id;      /* What group this is a member of */
  gint             ni_transport;   /* Type of addr for this node ipv4 ipv6..*/
  NetAddr          ni_addr;
  NetAddr          ni_mask;
};

/* Structure describing the common db entries for each object in the db */    
struct _DBEntry {
  gint         db_rowid;         /* Unique row id for this object in the db */
  gchar        *db_date_mod;     /* Date this object was created in the db */
  gchar        *db_date_created; /* Date this object was created in the db */
  gchar        *db_created_by;   /* User name of the creator of this object */
  gchar        *db_modified_by;  /* Username of the modifier of this object */
};

/* Structure describing the groups */
struct _Group {
  gint         grp_id;           /* The id of this group */
  gchar        *grp_name;        /* The name of this group */
  GList        grp_members;      /* a list of members of this group */
};

/* The structure that represents an object of the host type */
struct _Host {
  NodeId        *node_id;
  MapLocation   *map_location;
  DBEntry       *db_entry;
};

/* The structure that represents an object of the network type */
struct _Network {
  NodeId        *node_id;
  MapLocation   *map_location;
  DBEntry       *db_entry;
};

/* A entry of mibs to monitor on a node. */
struct _MonitorMib {
  gpointer        foo;
  
};

/* Health of a Node. */
struct _NodeHealth {
  gpointer       foo;
  
};

#endif

/* EOF */

