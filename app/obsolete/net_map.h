/*
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Network Map panel. (New and Improved to use the gnome-canvas)
 */

#ifndef __NET_MAP_H__
#define __NET_MAP_H__
#include <gnome.h>

void          open_map_panel                     (void);
void          destroy_map_panel                  (void);
void          hide_map_panel                     (void);
void          reset_map_panel                    (void);

void 	toggle_snap_menu_cb 		(GtkWidget *widget, gpointer data);
void 	node_link_cb 			(GtkWidget *widget, gpointer data);

#endif
/* EOF */













