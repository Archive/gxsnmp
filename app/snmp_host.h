/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Variables and functions specific to the host panel
 */
#ifndef __SNMP_HOST_H__
#define __SNMP_HOST_H__

#include <sys/types.h>
#include <time.h>
#include <gtk/gtk.h>
#include <netdb.h>
#include <netinet/in.h>

/* 
 * For the add host dialog 
 */
typedef struct _host_panel {
  GtkWidget 	*window;	    /* the window */
  GtkWidget     *buttons[2];        /* The buttons */
  GtkWidget     *entries[8];        /* The text entry widgets */
  GtkWidget     *s_widgets[2];      /* Server page widgets */
} host_panel;

/*
 * Used for both the set and get panels.
 */
typedef struct _set_panel {
  GtkWidget     *window;
  GtkWidget     *entries[3];
  GtkWidget     *buttons[3];
} set_panel;

#define HOST_OK_BUTTON   0
#define HOST_CAN_BUTTON  1

#define HOST_E_NAME   0
#define HOST_E_DISP   1
#define HOST_E_PORT   2
#define HOST_E_RCOM   3
#define HOST_E_WCOM   4
#define HOST_E_VERS   5
#define HOST_E_RETR   6
#define HOST_E_TIME   7

#define HOST_S_POLL   0
#define HOST_S_SERV   1

#define SET_QUERY_BUTTON  0
#define SET_CLEAR_BUTTON  1
#define SET_CLOSE_BUTTON  2

#define SET_MIB_ENTRY     0
#define SET_MIB_HOST      1
#define SET_MIB_RESULT    2

typedef struct __edit_host {
  GtkWidget     *window;
  GtkWidget     *widget[8];
} edit_host;


/*
 * Defines for the following host list
 */
#define HOST_STATE_UNKNOWN   -1
#define HOST_STATE_UP        0x01
#define HOST_STATE_DOWN      0x02
/*
 * Some state defines (which panels need updating etc..)
 */
enum {
  INFO_PANEL_OPEN      = 1 << 0,
  INTERFACE_PANEL_OPEN = 1 << 1,
  MAP_UPDATE_NEEDED    = 1 << 2
};

/*
 * Simple map location cordinates.
 */

#define DEFAULT_COMMUNITY	"public"           /* read */
#define DEFAULT_WCOMMUNITY      "private"          /* write */
#define DEFAULT_PORT		"161"
/* 1 - 300 range */
#define DEFAULT_TIMEOUT		"30"
/* 1 - 32 range */
#define DEFAULT_RETRIES		"3"

/*
 * SQL stuff
 */
#define DEFAULT_DATABASE        "gxsnmp"


/*
 * Global functions defined by this module
 */
/*
 * Exported standard panel functions
 */
void               open_add_host_panel            (void);
void               hide_add_host_panel            (void);
void               destroy_add_host_panel         (void);
void               reset_add_host_panel           (void);

void               open_edit_panel                (void);
void               hide_edit_panel                (void);
void               destroy_edit_panel             (void);
void               reset_edit_panel               (void);

void               open_set_panel                 (void);
void               destroy_set_panel              (void);
void               hide_set_panel                 (void);
void               reset_set_panel                (void);

void               open_get_panel                 (void);
void               destroy_get_panel              (void);
void               hide_get_panel                 (void);
void               reset_get_panel                (void);

void               open_monitor_panel             (void);
void               destroy_monitor_panel          (void);
void               hide_monitor_panel             (void);
void               reset_monitor_panel            (void);

#endif
/* EOF */

