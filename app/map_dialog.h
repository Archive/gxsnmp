/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  The map_dialog widget is used to handle adding/editing maps
 */
#ifndef __GXSNMP_MAP_DIALOG_H__
#define __GXSNMP_MAP_DIALOG_H__

#include <gnome.h>
#include "gnome-property-dialog.h"
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

BEGIN_GNOME_DECLS
/****************************************************************************
 *Standard widget macros.
 ***************************************************************************/
#define GXSNMP_TYPE_MAP_DIALOG (gxsnmp_map_dialog_get_type())
#define GXSNMP_MAP_DIALOG(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MAP_DIALOG, GXsnmp_map_dialog)
#define GXSNMP_MAP_DIALOG_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_MAP_DIALOG, \
                              GXsnmp_map_dialogClass)
#if 0
#define GXSNMP_IS_MAP_DIALOG(obj) \ 
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MAP_DIALOG)
#endif
#define GXSNMP_IS_MAP_DIALOG_CLASS(klass)\
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MAP_DIALOG)
/****************************************************************************
 * Control blocks for the widget class and for widget instances
 ***************************************************************************/
typedef struct _GXsnmp_map_dialog       GXsnmp_map_dialog;
typedef struct _GXsnmp_map_dialogClass  GXsnmp_map_dialogClass;

struct _GXsnmp_map_dialog
{
  GnomePropertyDialog dialog;

  GtkWidget	* map_clist;
  GtkWidget	* map_panel;
  GtkWidget	* scrolled;
  GtkWidget	* add_button;
  GtkWidget	* delete_button;

  DB_map       *selected_map;
  gint         row;
};

struct _GXsnmp_map_dialogClass
{
  GnomePropertyDialogClass    parent_class;
};

/****************************************************************************
 * Public widget manipilation functions
 ***************************************************************************/
GtkType        gxsnmp_map_dialog_get_type             (void);
GtkWidget      *gxsnmp_map_dialog_new                 (gpointer   data);

END_GNOME_DECLS
#endif
/* EOF */
