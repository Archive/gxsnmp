/*
**  $Id$
**  GXSNMP -- An snmp mangament application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Summary dialog control code
*/

#include "main.h"

extern gxsnmp *app_info;

/*******************************************************************************
**
**  Forward declarations and callback functions
**
*******************************************************************************/

static gint close_cb 		(GtkWidget 	*widget,
				 gpointer	 data);

static void clicked_cb          (GtkWidget      *widget,
                                 gpointer        data);

/*******************************************************************************
**
**  Open the summary dialog, creating it if it is not present. 
**
**  The hide() show() logic guarantees that the window pops to the top,
**  regardless of its previous state.  Is this desirable?  A hack?
**
*******************************************************************************/

void 
open_summary_dialog ()
{
  if (!app_info->summary_dialog)
    {
      app_info->summary_dialog = gxsnmp_summary_dialog_new ();

      gtk_signal_connect (GTK_OBJECT (app_info->summary_dialog), "close",
                      GTK_SIGNAL_FUNC (close_cb), NULL);
 
      gtk_signal_connect (GTK_OBJECT (app_info->summary_dialog), "clicked",
                      GTK_SIGNAL_FUNC (clicked_cb), NULL);

      update_summary_dialog ();
    }
  gtk_widget_hide (app_info->summary_dialog);
  gtk_widget_show (app_info->summary_dialog);
}

/*******************************************************************************
**
**  Public function to destroy the summary dialog and clear the widget
**  pointer.
**
*******************************************************************************/

void
destroy_summary_dialog ()
{
  if (app_info->summary_dialog)
    {
      gtk_widget_destroy (app_info->summary_dialog);
      app_info->summary_dialog = NULL;
    }
}

/*******************************************************************************
**
**  Public function to load and display new values in the summary dialog.
**  Call this function whenever one of the status fields are changed.
**
*******************************************************************************/

void update_summary_dialog ()
{
  if (app_info->summary_dialog)
    gxsnmp_summary_dialog_put_data ( 
			GXSNMP_SUMMARY_DIALOG(app_info->summary_dialog), 
			&app_info->summary);
}

/*******************************************************************************
**
**  Callback routine for the summary dialog.  The summary dialog has only
**  one button, the close button, so just destroy the dialog whenever we
**  received a "clicked" signal.
**
*******************************************************************************/

static void
clicked_cb (GtkWidget *widget, gpointer data)
{
  destroy_summary_dialog();
}

/*******************************************************************************
**
**  Callback routine for when the summary dialog window is closed.  Clear
**  the widget pointer so that the next time the dialog window is opened, 
**  gxsnmp knows to create a new one.  Also return TRUE, indicating to
**  gnome_dialog_close that we have destroyed the dialog.
**
*******************************************************************************/

static gint
close_cb  (GtkWidget *widget, gpointer data)
{
  destroy_summary_dialog();
  return TRUE;
}

/* EOF */
