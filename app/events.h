/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The events manager
 */
#ifndef __EVENTS_H__
#define __EVENTS_H__

typedef struct __event_panel {
  GtkWidget   *window;
  GtkWidget   *clist;
} event_panel;

typedef struct __events {
  int     eventid;
  int     category;
  int     sub_category;
  int     stat;                    /* New/Old/Acknowledged etc */
  time_t  timestamp;
  char    *summary;               /* a 20 to 30 char summary for display */
  char    *description;           /* A longer description for the detail */
} app_event;

/*
 * The event categories
 */
#define GX_EVENT_INFO     0x02
#define GX_EVENT_WARN     0x04
#define GX_EVENT_ALRM     0x08

/*
 * The sub categories of the above
 */
/* INFO */
#define GX_INFO_NONE     0x00    /* general information */
#define GX_INFO_ADD      0x01    /* an add action happened */
#define GX_INFO_DEL      0x02    /* a delete action happened */
/* WARN */
#define GX_WARN_RESP     0x00    /* a device didn't respond */
#define GX_WARN_LOAD     0x01    /* device is approaching heavy load */
/* ALRM */
#define GX_ALRM_DOWN     0x00    /* Device is marked down */
#define GX_ALRM_VRFY     0x01    /* couldn't verify servies on device */
#define GX_ALRM_RESO     0x02    /* Over utilization of devices resources */

int add_event              (app_event *event);
int remove_event           (int event_id);

/*
 * Standard panel functions
 */
void              open_event_panel             (void);
void              destroy_event_panel          (void);
void              hide_event_panel             (void);
void              reset_event_panel            (void);

/*
 * Misc functions for the event panel
 */

#endif

/* EOF */






