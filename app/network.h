/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Functions for dealing with DB_network objects.
 */

#ifndef __NET_H__
#define __NET_H__

#include "tables.h"

DB_network      * network_create           (void);
void              network_destroy          (DB_network  * dbn);
void              network_add              (DB_network  * dbn);
void              network_update           (DB_network  * dbh);
void              network_delete           (DB_network  * dbn);
GList           * network_list             (void);
DB_network      * network_find_by_rowid    (guint         rowid);
GList		* network_find_by_address  (gchar	* address,
					    gchar	* netmask);


#endif

/* EOF */
