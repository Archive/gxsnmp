/*
**  $Id$
**
**  GXSNMP -- An snmp management application.
**  Copyright (C) 1998 Gregory Mclean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  The configuration panel 
*/

#include <config.h>
#include "main.h"
#include "config_panel.h"
#include <g_sql.h>

#include "debug.h"

extern gxsnmp *app_info;

/*****************************************************************************
**
**  Local forward references
**
*****************************************************************************/

static void  config_panel_class_init (GXsnmp_config_panelClass *klass);

static void  config_panel_init       (GXsnmp_config_panel      *panel);

static void  changed_cb	     	     (GtkWidget *widget, gpointer data);

static void  apply_cb	     	     (GtkWidget *widget, gint     data);

static gint  close_cb 	     	     (GtkWidget *widget, gpointer data);

static void  create_config_panel     (void);

/*****************************************************************************
**
**  Definitions of the individual notebook pages. 
**  The config_labels[] array specifies the text to be displayed on the tabs,
**  and the enumeration provides integer indices.
**
*****************************************************************************/

#define CONFIGURATION_TABS 5

static gchar *config_labels[] = {
  N_("General"),
  N_("SNMP Defaults"),
  N_("SQL"),
  N_("MENU"),
  N_("Collector"),
  N_("MIBs")
};

enum {
  CONFIG_GENERAL,
  CONFIG_SNMP,
  CONFIG_SQL,
  CONFIG_MENU,
  CONFIG_COLLECTOR,
  CONFIG_MIBS
};

static gchar *configuration_title = N_("GXSNMP Configuration");

/*******************************************************************************
**
**  Local forward references
**
*******************************************************************************/

static void  config_panel_class_init	(GXsnmp_config_panelClass *klass);

static void  config_panel_init		(GXsnmp_config_panel	  *panel);

/*******************************************************************************
**
**  gxsnmp_config_panel_get_type ()
**
*******************************************************************************/

guint
gxsnmp_config_panel_get_type ()
{
  static guint config_type = 0;

  if (!config_type)
    {
      GtkTypeInfo config_info =
      {
	"GXsnmp_config_panel",
	sizeof (GXsnmp_config_panel),
	sizeof (GXsnmp_config_panelClass),
	(GtkClassInitFunc) config_panel_class_init,
	(GtkObjectInitFunc) config_panel_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };
      config_type = gtk_type_unique (gnome_property_box_get_type (), 
				     &config_info);
    }
  return config_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
config_panel_class_init (GXsnmp_config_panelClass *class)
{
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
config_panel_init (GXsnmp_config_panel *panel)
{
  GtkWidget	*label;		/* Used to create the page tabs */
  GtkWidget     *vbox;
  D_FUNC_START;
  /*
  **  Add the general configuration panel page to the notebook
  */
  /*
**  panel->general = gxsnmp_general_panel_new();
**  gtk_signal_connect (GTK_OBJECT(panel->general), "changed",
**                      GTK_SIGNAL_FUNC(changed_cb), panel);
**  gtk_widget_show(panel->general);
**
**  label = gtk_label_new (gettext(config_labels[CONFIG_GENERAL]));
**  gtk_widget_show (label);
**
**  gnome_property_box_append_page(GNOME_PROPERTY_BOX(panel),
**				 panel->general, label);
*/
/*
**  Add the SNMP defaults configuration panel page to the notebook
*/
/*
  panel->snmp = gxsnmp_snmp_panel_new (g_transport_name_list (), 
				       g_transport_model_list (), 
				       app_info->version_names, 
				       app_info->version_values);
  gtk_signal_connect (GTK_OBJECT(panel->snmp), "changed",
		      GTK_SIGNAL_FUNC(changed_cb), panel);
  gtk_widget_show (panel->snmp);

  label = gtk_label_new (gettext(config_labels[CONFIG_SNMP]));
  gtk_widget_show (label);

  gnome_property_box_append_page (GNOME_PROPERTY_BOX(panel),
				  panel->snmp, label);
*/
  /*
   *  Add the SQL configuration panel page to the notebook 
   * All this will be replaced with the database dialog sometime.
   */
  vbox = gtk_vbox_new (FALSE, 4);
  panel->sql_engine = gtk_combo_new ();
  gtk_combo_set_popdown_strings (GTK_COMBO (panel->sql_engine),
				 g_sql_enumerate_backends ());
  d_print (DEBUG_TRACE, "Backends registered %d\n", 
	   g_list_length (g_sql_enumerate_backends ()));
  gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (panel->sql_engine)->entry),
			  FALSE);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (panel->sql_engine)->entry),
		      "changed", (GtkSignalFunc) changed_cb, panel);
  gtk_box_pack_start (GTK_BOX (vbox), panel->sql_engine, TRUE, TRUE, 4);
  /* General sql settings */
  panel->sql = gxsnmp_sql_panel_new (g_sql_enumerate_backends ());
  gtk_signal_connect (GTK_OBJECT(panel->sql), "changed",
                      GTK_SIGNAL_FUNC(changed_cb), panel);
  gtk_box_pack_start (GTK_BOX (vbox), panel->sql, TRUE, TRUE, 4);
  
  gtk_widget_show_all (vbox);

  label = gtk_label_new (gettext(config_labels[CONFIG_SQL]));
  gtk_widget_show(label);

  gnome_property_box_append_page (GNOME_PROPERTY_BOX(panel),
				  vbox, label);

/*
**  Add the MENU configuration panel page to the notebook
*/
	
  panel->menu = gxsnmp_menu_panel_new ();
  gtk_signal_connect (GTK_OBJECT(panel->menu), "changed",
                      GTK_SIGNAL_FUNC(changed_cb), panel);
  gtk_widget_show (panel->menu);

  label = gtk_label_new (gettext(config_labels[CONFIG_MENU]));
  gtk_widget_show(label);

  gnome_property_box_append_page (GNOME_PROPERTY_BOX(panel),
				  panel->menu, label);

/*
**  Add the Collector configuration panel page to the notebook
*/
/*
**  panel->collector = gxsnmp_collector_panel_new();
**  gtk_widget_show (panel->collector);
**
**  label = gtk_label_new (gettext(config_labels[CONFIG_COLLECTOR]));
**  gtk_widget_show(label);
**
**  gnome_property_box_append_page (GNOME_PROPERTY_BOX(panel),
**				  panel->collector, label);
*/
/*
**  Add the MIB configuration panel page to the notebook
*/

  panel->mib = gxsnmp_mib_panel_new();
  gtk_signal_connect (GTK_OBJECT(panel->mib), "changed",
                      GTK_SIGNAL_FUNC(changed_cb), panel);
  gtk_widget_show (panel->mib);

  label = gtk_label_new (gettext(config_labels[CONFIG_MIBS]));
  gtk_widget_show(label);

  gnome_property_box_append_page (GNOME_PROPERTY_BOX(panel),
				  panel->mib, label);

  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function to handle the "changed" signal from the 
**  GXsnmp_config_panel widget.  This function is invoked whenever the 
**  user modifies any data field in any notebook window.  The purpose of 
**  this function is to activate the "Ok" and "Apply" buttons.
**
****************************************************************************/

static void
changed_cb( GtkWidget *widget, gpointer data)
{
  GXsnmp_config_panel *panel;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  panel = GXSNMP_CONFIG_PANEL(data);	/* Pointer to the base structure */
  gnome_property_box_changed (GNOME_PROPERTY_BOX(panel));
  D_FUNC_END;
}

/*******************************************************************************
**
**  Callback function to handle the "apply" signal from the underlying
**  gnome_property_box widget. This function is invoked when the user 
**  presses "Ok" or "Apply."  The purpose of this function is to copy
**  all data from the notebook page widgets to the running configuration,
**  and save the configuration.  
**
*******************************************************************************/

static void
apply_cb (GtkWidget *widget, gint data)
{
  GXsnmp_config_panel  *panel;
  GXsnmp_configuration *c;
  struct _GXsnmp_sql_configuration sql;

  D_FUNC_START;
  if (data == (-1))	/* Only interested in the "Global Apply" callback */
    { 
      panel = GXSNMP_CONFIG_PANEL(app_info->conf_panel);
      c     = &app_info->current_config;

/*
**      gxsnmp_general_panel_get_data (
**			GXSNMP_GENERAL_PANEL(panel->general), &c->general);
*/ 
/*      gxsnmp_snmp_panel_get_data (GXSNMP_SNMP_PANEL(panel->snmp), &c->snmp);
*/
      gxsnmp_sql_panel_get_data  (GXSNMP_SQL_PANEL (panel->sql), &sql);  

      g_memmove (&app_info->general_db, &sql.G_sql, sizeof sql.G_sql);
      app_info->general_database = sql.database;
      app_info->general_db.engine = g_strdup (gtk_entry_get_text
		         (GTK_ENTRY (GTK_COMBO(panel->sql_engine)->entry)));

      gxsnmp_menu_panel_get_data (GXSNMP_MENU_PANEL(panel->menu), &c->menu);

/* FIXME: wretched hack */

/*
**      gxsnmp_collector_panel_get_data (
**                    GXSNMP_COLLECTOR_PANEL(panel->collector), &c->collector);
*/
      gxsnmp_mib_panel_get_data (GXSNMP_MIB_PANEL(panel->mib), &c->mib);

      d_print (DEBUG_TRACE, "Saving configuration.\n");
      save_config();
    }
  D_FUNC_END;
}

/*******************************************************************************
**
**  Callback function to handle the "close" signal from the underlying
**  gnome_dialog widget.  This function is invoked when the user has    
**  selected "Ok" or "Cancel", and the widget is about to be destroyed.
**  The purpose of this function is to zero the widget pointer in the
**  global data area, and return FALSE, indicating to gnome_dialog_close
**  that we have not destroyed the dialog. 
**
*******************************************************************************/

static gint
close_cb (GtkWidget *widget, gpointer data)
{

  D_FUNC_START;
  app_info->conf_panel = NULL;
  D_FUNC_END;
  return FALSE;
}

/*******************************************************************************
**
**  Public function to create a new configuration panel widget
**
*******************************************************************************/

GtkWidget *
gxsnmp_config_panel_new ()
{
  return GTK_WIDGET( gtk_type_new( gxsnmp_config_panel_get_type()));
}

/*******************************************************************************
**
**  Private function to create the configuration panel window
**
*******************************************************************************/

static void
create_config_panel ()
{
  GXsnmp_config_panel  *panel;
  GXsnmp_configuration *config;
  GXsnmp_sql_configuration sql;
  D_FUNC_START;
  
  app_info->conf_panel = gxsnmp_config_panel_new();
  panel  = GXSNMP_CONFIG_PANEL(app_info->conf_panel);
  config = &app_info->current_config;

  gtk_window_set_title (GTK_WINDOW (panel), gettext(configuration_title));

  gtk_signal_connect (GTK_OBJECT (panel), "apply",
                      GTK_SIGNAL_FUNC (apply_cb), NULL);

  gtk_signal_connect (GTK_OBJECT (panel), "close",
                      GTK_SIGNAL_FUNC (close_cb), NULL);

/*
** gxsnmp_general_panel_put_data (
**                GXSNMP_GENERAL_PANEL(panel->general), &config->general);
*/
/*  gxsnmp_snmp_panel_put_data (GXSNMP_SNMP_PANEL(panel->snmp), &config->snmp);
*/
  d_print (DEBUG_TRACE, "Copying default SQL config to the panel\n");
  g_memmove (&sql.G_sql, &app_info->general_db, sizeof (sql.G_sql));
  sql.database = app_info->general_database;

  gxsnmp_sql_panel_put_data (GXSNMP_SQL_PANEL(panel->sql), &sql, 
				g_sql_enumerate_backends ());
/*
**  gxsnmp_collector_panel_put_data (
**                GXSNMP_COLLECTOR_PANEL(panel->collector), &config->collector);
*/

  gxsnmp_menu_panel_put_data (GXSNMP_MENU_PANEL(panel->menu), &config->menu);
  gxsnmp_mib_panel_put_data (GXSNMP_MIB_PANEL(panel->mib), &config->mib);

  gnome_property_box_set_state(GNOME_PROPERTY_BOX(app_info->conf_panel), FALSE);

  gtk_widget_show_all (app_info->conf_panel);
  D_FUNC_END;
}

/*******************************************************************************
**
**  Public function to create and display the configuration window.  This
**  function is called by the "Configuration menu" callback.
**
*******************************************************************************/

void
open_config_panel ()
{
  D_FUNC_START;
  if (!app_info->conf_panel)
    {
      d_print (DEBUG_TRACE, "Create new panel.\n");
      create_config_panel ();
    }
  else
    {
      d_print (DEBUG_TRACE, "Showing old panel.\n");
      if (!GTK_WIDGET_VISIBLE (app_info->conf_panel))
        gtk_widget_show (app_info->conf_panel);
    }
  D_FUNC_END;
}

/* EOF */


