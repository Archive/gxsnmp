/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Wire item widget
**
**  Wire items connect hosts to networks
*/

#ifndef __GXSNMP_WIRE_H__
#define __GXSNMP_WIRE_H__

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

#include "gxsnmp_map.h"
#include "gxsnmp_host.h"
#include "gxsnmp_network.h"
#include "gxsnmp_map_item.h"

BEGIN_GNOME_DECLS

/*****************************************************************************
**
**  Standard widget macros
**
*****************************************************************************/

#define GXSNMP_TYPE_WIRE (gxsnmp_wire_get_type())

#define GXSNMP_WIRE(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_WIRE, GXsnmp_wire)

#define GXSNMP_WIRE_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_WIRE, \
			      GXsnmp_wireClass)

#define GXSNMP_IS_WIRE(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_WIRE)

#define GXSNMP_IS_WIRE_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_WIRE)

/*****************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*****************************************************************************/

typedef enum
{
  WIRE_INCORRECT,	/* Wire is incorrectly placed */
  WIRE_CENTERED,	/* Wire has been centered */
  WIRE_ADJUSTED,	/* Wire has been adjusted to final placement */
} 
WireAdjustState;

typedef struct _GXsnmp_wire		GXsnmp_wire;
typedef struct _GXsnmp_wireClass	GXsnmp_wireClass;

struct _GXsnmp_wire
{
  GXsnmp_map_item         map_item;     /* Base object */
  GnomeCanvasItem	* line;		/* The wire line item */
  GnomeCanvasPoints	* points;	/* Points list for the line */
  
  DB_graph		* DB_graph_host;     /* DB_graph entry for host */
  DB_graph 		* DB_graph_network;  /* DB_graph entry for network */

/* The following three entries are used by the "attach" callback */

  gdouble          	* endpoint;	/* points entry for endpoint */
  gdouble               * nextpoint;	/* points entry for next point */
  WireAdjustState	  adjuststate;  /* Wire adjustment state */

  gchar                 * fill_color;   /* What color to make this wire */
}; 

struct _GXsnmp_wireClass
{
  GXsnmp_map_itemClass    itemClass;
};

/****************************************************************************
**
**  Public widget manipulation functions
**
*****************************************************************************/

GtkType	          gxsnmp_wire_get_type	(void);

GXsnmp_map_item * gxsnmp_wire_new	(DB_graph * graph);

void              gxsnmp_wire_move      (GXsnmp_map_item * item,
					 GXsnmp_wire     * wire);
void              gxsnmp_wire_set_color (GXsnmp_wire     * wire,
					 gchar           * color);
					 
END_GNOME_DECLS
#endif /* __GXSNMP_WIRE_H__ */
