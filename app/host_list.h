/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Definitions for the host list panel
 */
#ifndef __GXSNMP_HOST_LIST_H__
#define __GXSNMP_HOST_LIST_H__
#include <gnome.h>
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "host_panel.h"

#include "snmp_host.h"
BEGIN_GNOME_DECLS

/****************************************************************************
 * Standard widget macros 
 ***************************************************************************/
#define GXSNMP_TYPE_HOST_LIST (gxsnmp_host_list_get_type ())
#define GXSNMP_HOST_LIST(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_HOST_LIST, GXsnmp_host_list)
#define GXSNMP_HOST_LIST_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_HOST_LIST, \
			      GXsnmp_host_listClass)
#define GXSNMP_IS_HOST_LIST (obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_HOST_LIST)
#define GXSNMP_IS_HOST_LIST_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_HOST_LIST)
/****************************************************************************
 * Control blocks for the widget class and for widget instances 
 ***************************************************************************/
typedef struct _GXsnmp_host_list           GXsnmp_host_list;
typedef struct _GXsnmp_host_listClass      GXsnmp_host_listClass;

struct _GXsnmp_host_list
{
  GnomeDialog dialog;

  GtkWidget     *table;
  GtkWidget     *clist;
  GtkWidget	*scrolled;
  GtkWidget     *edit_button;                  /* Edit button */
  GtkWidget     *dele_button;                  /* Delete button */

  DB_host       *selected_host;
};

struct _GXsnmp_host_listClass
{
  GnomeDialogClass    parent_class;
};
/****************************************************************************
 * Public Widget functions
 ***************************************************************************/
GtkType        gxsnmp_host_list_get_type               (void);
GtkWidget      *gxsnmp_host_list_new                   (gpointer);

#endif 

/* EOF */

