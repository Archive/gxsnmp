/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Header file for the main module
 */
#ifndef __MAIN_H__
#define __MAIN_H__
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>
#include <sys/types.h>
#include <sys/stat.h>             /* For 'struct stat' */
#include <sys/errno.h>            /* For the errno declarations */
#include <pwd.h>                  /* For the passwd struct */
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <gtkledbar.h>
#include <g_snmp.h>
#include <smi.h>
#include <g_sql.h>

/*
**  Include the GXsnmp widget library
*/

#include "gxsnmpapp.h"
#include "about.h"
#include "menus.h"
#include "snmp_host.h"
#include "config_panel.h"
#include "host_list.h"
#include "events.h"
#include "fileops.h"
#include "browser.h"
#include "ping.h"
#include "queue.h"
#include "main_panel.h"
#include "rmon_panel.h"

#define ELEMENTS(x) (sizeof (x) / sizeof (x[0]))

/*
 * User interface variables and structures
 */
typedef struct __GxSnmp {

  /* main interface values */

  GtkWidget            *window;
  GtkAccelGroup        *accel_table;
  GdkPixmap            *pixmap;
  GtkWidget            *current_host;
  GdkColorContext      *cc;
  GdkGC                *gc;

/*
**  Database definitions
**
**  Eventually, each database will be able to have its own definition.
**  As a way of getting from here to there, I'm creating a set of
**  #defines as a migration tool.  - jms
**
**  The G_sql type is defined in lib/g_sql.h
*/

  G_sql	 		 general_db;		/* This will go away! */
  gchar		       * general_database;	/* And this will too! */

#define graph_db	 general_db
#define graph_database	 general_database

#define host_db      	 general_db
#define host_database    general_database

#define interface_db     general_db
#define interface_database general_database

#define map_db		 general_db
#define map_database	 general_database

#define network_db   	 general_db
#define	network_database general_database

#define snmp_db		 general_db
#define snmp_database	 general_database

#define wire_db		 general_db
#define wire_database	 general_database

/* application info */
  gxsnmp_config        current_config;

/*
**  Host, network, alarm, event, and task counts are stored in the
**  following structure, which is contained in summary_dialog.h
*/

  GXsnmp_summary_data  summary;			/* See summary_dialog.h */

  gint                 ping_tag;
  GHashTable           *host_hash;
  int                  x,y;
  int                  snmp_running;
  gint                 root_width;               /* width of root window */
  gint                 root_height;              /* height of root window */

/*
**  Linked lists of all available SNMP versions
**  and their associated values.
*/
  GList                *version_names;
  GList                *version_values;

  /* Panels of the application */
  GtkWidget             * host_popup_right;
  GtkWidget             * conf_panel;

  GtkWidget	        * summary_dialog;
  GtkWidget		* database_dialog;
} gxsnmp;

void 		quit_app                (void);
void 		update_statusbar 	(gchar * format, ...) 
					__attribute__ ((format (printf, 1, 2)));
void            app_update_init_status  (gchar *lab1, gchar *lab2);
void            notice_dlg              (gchar * format, ...)
                                        __attribute__ ((format (printf, 1, 2)));
#endif

/* EOF */
