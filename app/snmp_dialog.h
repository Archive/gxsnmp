/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  The snmp_dialog widget is used to manage SNMP configuration settings
*/

#ifndef __GXSNMP_SNMP_DIALOG_H__
#define __GXSNMP_SNMP_DIALOG_H__

#include <gnome.h>
#include "gnome-property-dialog.h"
#include "snmp_panel.h"
#include "interface_panel.h"
#include "snmp_panel.h"
#include "tables.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*****************************************************************************
**
**  Standard widget macros
**
*****************************************************************************/

#define GXSNMP_TYPE_SNMP_DIALOG (gxsnmp_snmp_dialog_get_type())

#define GXSNMP_SNMP_DIALOG(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_SNMP_DIALOG, GXsnmp_snmp_dialog)

#define GXSNMP_SNMP_DIALOG_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_SNMP_DIALOG, \
			      GXsnmp_snmp_dialogClass)

#define GXSNMP_IS_SNMP_DIALOG(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_SNMP_DIALOG)

#define GXSNMP_IS_SNMP_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_SNMP_DIALOG)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_snmp_dialog	GXsnmp_snmp_dialog;
typedef struct _GXsnmp_snmp_dialogClass GXsnmp_snmp_dialogClass;

struct _GXsnmp_snmp_dialog
{
  GnomePropertyDialog  dialog;		/* Base structure */

  GtkWidget     * table;		/* The top level box */
  GtkWidget	* scrolled;		/* The scrolling window */
  GtkWidget	* snmp_clist;		/* The list of SNMP configurations */
  GtkWidget	* name_entry;		/* Entry field for editing name */
  GtkWidget	* snmp_panel;		/* The snmp editing panel */
  GtkWidget	* add_button;		/* The "Add" button */
  GtkWidget	* delete_button;	/* The "Delete" button */

  DB_snmp	* selected_snmp;	/* Current SNMP definition */
  gint		  row;			/* Current row in clist */

};

struct _GXsnmp_snmp_dialogClass
{
  GnomePropertyDialogClass  parent_class; 	/* Class base structure */
};

/*****************************************************************************
**
**  Public widget manipulation functions
**
*****************************************************************************/

guint	    gxsnmp_snmp_dialog_get_type		(void);
GtkWidget * gxsnmp_snmp_dialog_new		(gpointer data);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_SNMP_DIALOG_H__ */

/* EOF */

