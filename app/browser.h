/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 * 
 * The mib browser panel.
 */

#ifndef __BROWSER_H__
#define __BROWSER_H__
#include <gnome.h>

BEGIN_GNOME_DECLS

/****************************************************************************
 * Standard widget macros 
 ***************************************************************************/
#define GXSNMP_TYPE_BROWSER (gxsnmp_browser_get_type ())
#define GXSNMP_BROWSER(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_BROWSER, GXsnmp_browser)
#define GXSNMP_BROWSER_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_BROWSER, \
			      GXsnmp_browserClass)
#define GXSNMP_IS_BROWSER (obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_BROWSER)
#define GXSNMP_IS_BROWSER_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_BROWSER)
/****************************************************************************
 * Control blocks for the widget class and for widget instances 
 ***************************************************************************/
typedef struct _GXsnmp_browser             GXsnmp_browser;
typedef struct _GXsnmp_browserClass        GXsnmp_browserClass;

struct _GXsnmp_browser
{
  GnomeDialog    dialog;

  DB_host       *dbh;            /* host db entry */
  gulong         thismib[SNMP_SIZE_OBJECTID]; /* current MIB instance */
  int            thismib_length; /* Length of current MIB instance */
  gulong         basemib[SNMP_SIZE_OBJECTID]; /* top level MIB instance */
  int            basemib_length; /* Length of top level MIB instamce */
  GtkWidget     *root;           /* root of the MIB tree */
  GtkWidget     *mib_desc;       /* the description of the mib */
  GtkWidget     *mib_type;       /* Type of this mib */
  GtkWidget     *mib_module;     /* Module this oid is defined in */
  GtkWidget     *mib_status;     /* Status of this oid */
  GtkWidget     *mib_oid;        /* the oid of this mib */
  GtkWidget     *mib_label;      /* the textual representation of a mib */
  GtkWidget     *mib_value;      /* value for SNMP read / write operation */
  GtkWidget     *fbutton;        /* FIRST button */
  GtkWidget     *nbutton;        /* NEXT button */
  GtkWidget     *gbutton;        /* GET button */
  GtkWidget     *pbutton;        /* PUT button */
  gpointer       request;        /* SNMP request handle */
};

struct _GXsnmp_browserClass
{
  GnomeDialogClass    parent_class;
};
/****************************************************************************
 * Public Widget functions
 ***************************************************************************/
GtkType        gxsnmp_browser_get_type               (void);
GtkWidget      *gxsnmp_browser_new                   (gpointer);

#endif

/* EOF */
