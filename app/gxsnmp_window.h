/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Network Window Wiget  (Basically the main application)
*/

#ifndef __GXSNMP_WINDOW_H__
#define __GXSNMP_WINDOW_H__

BEGIN_GNOME_DECLS

#include "gxsnmp_map.h"

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_WINDOW (gxsnmp_window_get_type())

#define GXSNMP_WINDOW(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_WINDOW, GXsnmp_window)

#define GXSNMP_WINDOW_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_WINDOW, \
			      GXsnmp_windowClass)

#define GXSNMP_IS_WINDOW(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_WINDOW)

#define GXSNMP_IS_WINDOW_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_WINDOW)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_window		GXsnmp_window;
typedef struct _GXsnmp_windowClass      GXsnmp_windowClass;

struct _GXsnmp_window
{
  GnomeApp	  gnomeapp;		/* The GnomeApp base widget */  

  GtkWidget	* logo;			/* The big side logo */
  GtkWidget	* mouse_status;         /* Mouse status area */
  GtkWidget	* map_statusbar;	/* Map status area */  
  GtkWidget	* activity_led;		/* The activity indicator */
  GtkWidget	* map_notebook;		/* The notebook of maps */
  GList		* map_list;		/* List of active maps */

  /* Moved to the map so each map can be set differently */
  gboolean	  snap_to_grid;		/* Flag:  Snap items to grid */

};

struct _GXsnmp_windowClass
{
  GnomeAppClass parent_class;		/* The GnomeApp base class */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkType	     gxsnmp_window_get_type	       (void);
GtkWidget  * gxsnmp_window_new		       (void);

void	     gxsnmp_window_add_map             (GXsnmp_window   * window, 
					        GXsnmp_map	* map);

GXsnmp_map * gxsnmp_window_get_current_map     (GXsnmp_window 	* window);

void	     gxsnmp_window_set_mouse_statusbar (void);
void	     gxsnmp_window_set_map_statusbar   (void);
void 	     gxsnmp_window_set_activity_led    (void);
void         gxsnmp_window_set_statusbar_text  (GXsnmp_window 	* window, 
						gchar 		* string);


END_GNOME_DECLS

#endif /* __GXSNMP_WINDOW_H__ */
