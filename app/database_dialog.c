/*
**  $Id$
**
**  GXSNMP -- An snmp management application.
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  The configuration panel 
*/
#include <config.h>
#include <gnome.h>
#include "main.h"
#include "database_dialog.h"
#include "gnome-dialog-create-button.h"
#include <g_sql.h>

#define DEBUG_CALLBACKS
#include "debug.h"
extern gxsnmp *app_info;

/******************************************************************************
**
**  Local forward references
**
******************************************************************************/

static void database_dialog_class_init  (GXsnmp_database_dialogClass *klass);
static void database_dialog_init        (GXsnmp_database_dialog      *panel);
static void changed_cb	     	     	(GtkWidget *widget, gpointer data);
static void database_dialog_cb          (GnomeDialog                 *dialog,
					 gint                        button,
					 gpointer                    data);
static void  create_database_dialog  	(void);

/******************************************************************************
**
**  Static Data
**
******************************************************************************/

static gchar * title = N_("GXSNMP Database Configuration");

static gchar * buttons[] = { "Ok", "Cancel", "Help", NULL };
			     
/******************************************************************************
**
**  gxsnmp_database_dialog_get_type ()
**
******************************************************************************/

guint
gxsnmp_database_dialog_get_type ()
{
  static guint widget_type = 0;

  if (!widget_type)
    {
      GtkTypeInfo widget_info =
      {
	"GXsnmp_database_dialog",
	sizeof (GXsnmp_database_dialog),
	sizeof (GXsnmp_database_dialogClass),
	(GtkClassInitFunc) database_dialog_class_init,
	(GtkObjectInitFunc) database_dialog_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };
      widget_type = gtk_type_unique (gnome_dialog_get_type (), 
				     &widget_info);
    }
  return widget_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
database_dialog_class_init (GXsnmp_database_dialogClass *class)
{
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
database_dialog_init (GXsnmp_database_dialog *dialog)
{
  GtkWidget * label;
  GtkWidget * hbox;
  GtkWidget * separator;
  GtkWidget * button_box;
  GtkWidget * button;
  GtkWidget * item;
  GtkWidget * scrolled_window;
  GList     * list;
  gint        c_width, c_height;

  D_FUNC_START;
/*
**  Build the main framework for the whole thing
*/

  dialog->main_table = gtk_table_new (4, 4, FALSE);
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      dialog->main_table, TRUE, TRUE, 0);
  gtk_widget_show (dialog->main_table);
  /* Lets pick up the text width and height and loose the nasty absoulte size 
   * stuff 
   */
  c_width  = gdk_string_width (dialog->main_table->style->font, "xW") / 2;
  c_height = dialog->main_table->style->font->ascent +
             dialog->main_table->style->font->descent;

/*
**  First comes the list of all existing configurations
*/

  label = gtk_label_new (_("Existing Configurations"));
  gtk_table_attach (GTK_TABLE (dialog->main_table), label, 
		    0, 1, 0, 1, 0, 0, 0, 0);
  gtk_widget_show (label);
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_ALWAYS);
  gtk_table_attach (GTK_TABLE (dialog->main_table), scrolled_window,
                    0, 1, 1, 2, 
                    GTK_FILL, GTK_FILL, 4, 4);
 
  dialog->config_list = gtk_list_new ();
  /* 20 x 10 should suffice. */
  gtk_widget_set_usize (GTK_WIDGET (scrolled_window), 
                                    c_width * 20, c_height * 10);
			
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled_window)
					 , dialog->config_list);
  gtk_widget_show_all (scrolled_window);

  list = NULL;

/*
**  FIXME: Stick something in the list for testing purposes ... get rid of this
*/

  item = gtk_list_item_new_with_label ("Default");
  gtk_widget_show (item);
  list = g_list_append (list, item);
  item = gtk_list_item_new_with_label ("Remote");
  gtk_widget_show (item);
  list = g_list_append (list, item);

  gtk_list_insert_items (GTK_LIST (dialog->config_list), list, 0);

/*
**  Now the button box for the configuration selection
*/

  button_box = gtk_hbutton_box_new ();
  gtk_table_attach (GTK_TABLE (dialog->main_table), button_box, 
		    0, 1, 2, 3, 0, 0, 0, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (button_box),
                             gnome_preferences_get_button_layout ());
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (button_box),
                              GNOME_PAD);
  gtk_widget_show (button_box);

  button = gnome_dialog_create_button (GNOME_DIALOG (dialog), "Create", NULL);
  gtk_box_pack_start (GTK_BOX (button_box), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

  button = gnome_dialog_create_button (GNOME_DIALOG (dialog), "Delete", NULL);
  gtk_box_pack_start (GTK_BOX (button_box), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

/*
**  Now a vertical separator 
*/

  separator = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE (dialog->main_table), separator, 1, 2, 0, 3, 
		    0, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_widget_show (separator);

/*
**  Now the "Database Connection" part of the dialog
*/

  label = gtk_label_new (_("Database Connection"));
  gtk_table_attach (GTK_TABLE (dialog->main_table), label,
                    2, 3, 0, 1, 0, 0, 0, 0);
  gtk_widget_show (label);

/*
**  The database-engine combo box, and the engine-specific widget
*/

  dialog->engine_vbox = gtk_vbox_new (FALSE, 4);
  gtk_table_attach (GTK_TABLE (dialog->main_table), dialog->engine_vbox, 
		    2, 3, 1, 2, 0, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_widget_show (dialog->engine_vbox);

  hbox = gtk_hbox_new (FALSE, 4);
  gtk_box_pack_start (GTK_BOX (dialog->engine_vbox), hbox, FALSE, FALSE, 4);
  gtk_widget_show (hbox);

  label = gtk_label_new (_("Engine"));
  gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 4);
  gtk_widget_show (label);

  dialog->engine_combo = gtk_combo_new ();
  gtk_combo_set_popdown_strings (GTK_COMBO (dialog->engine_combo),
				 g_sql_enumerate_backends ());
  gtk_box_pack_start (GTK_BOX (hbox), dialog->engine_combo, TRUE, TRUE, 4);
  gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(dialog->engine_combo)->entry), 
			 FALSE);
/*
  gtk_signal_connect (GTK_OBJECT(GTK_COMBO(dialog->engine)->entry), "changed",
                      (GtkSignalFunc) changed_cb, dialog);
*/
  gtk_widget_show(dialog->engine_combo);

/*
**  FIXME: Add the SQL configuration panel page to the notebook.  Actually,
**  we want to get this widget definition from the backend.
*/
	
  dialog->engine_specific = gxsnmp_sql_panel_new (g_sql_enumerate_backends ());
  gtk_box_pack_start (GTK_BOX (dialog->engine_vbox), dialog->engine_specific,
		      TRUE, TRUE, 0);
  gtk_widget_show (dialog->engine_specific);

/*
**  Now the button box for the database connection section
*/

  button_box = gtk_hbutton_box_new ();
  gtk_table_attach (GTK_TABLE (dialog->main_table), button_box,
                    2, 3, 2, 3, 0, 0, 0, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (button_box),
                             gnome_preferences_get_button_layout ());
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (button_box),
                              GNOME_PAD);
  gtk_widget_show (button_box);

  button = gnome_dialog_create_button (GNOME_DIALOG (dialog), 
				       "Connect", NULL);
  gtk_box_pack_start (GTK_BOX (button_box), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

  button = gnome_dialog_create_button (GNOME_DIALOG (dialog), 
				       "Disconnect", NULL);
  gtk_box_pack_start (GTK_BOX (button_box), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

/*
**  Now a vertical separator
*/

  separator = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE (dialog->main_table), separator, 3, 4, 0, 3,
                    0, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_widget_show (separator);
  gtk_widget_show (separator);

/*
**  Next the list of databases available on the connected server
*/

  label = gtk_label_new (_("Available Databases"));
  gtk_table_attach (GTK_TABLE (dialog->main_table), label,
                    4, 5, 0, 1, 0, 0, 0, 0);
  gtk_widget_show (label);
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_ALWAYS);
  gtk_table_attach (GTK_TABLE (dialog->main_table), scrolled_window,
		    4, 5, 1, 2, GTK_FILL, GTK_FILL, 4, 4);
  
  dialog->database_list = gtk_list_new ();
  gtk_widget_set_usize (GTK_WIDGET (scrolled_window), 
			c_width * 20, c_height * 20);
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled_window),
					 dialog->database_list);
  gtk_widget_show_all (scrolled_window);

/*
**  FIXME: Stick something in the list for testing purposes ... get rid of this
*/

  list = NULL;
  item = gtk_list_item_new_with_label ("gxsnmp");  
  gtk_widget_show (item);
  list = g_list_append (list, item);
  item = gtk_list_item_new_with_label ("other");
  gtk_widget_show (item);
  list = g_list_append (list, item);

  gtk_list_insert_items (GTK_LIST (dialog->database_list), list, 0);

/*
**  Now the button box for the database selection section  
*/

  button_box = gtk_hbutton_box_new ();
  gtk_table_attach (GTK_TABLE (dialog->main_table), button_box,
                    4, 5, 2, 3, 0, 0, 0, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (button_box),
                             gnome_preferences_get_button_layout ());
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (button_box),
                              GNOME_PAD);
  gtk_widget_show (button_box);

  button = gnome_dialog_create_button (GNOME_DIALOG (dialog), "Create", NULL);
  gtk_box_pack_start (GTK_BOX (button_box), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

  button = gnome_dialog_create_button (GNOME_DIALOG (dialog), "Delete", NULL);
  gtk_box_pack_start (GTK_BOX (button_box), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

  /* Now the dialog buttons */
  gnome_dialog_append_buttons (GNOME_DIALOG (dialog), 
			       GNOME_STOCK_BUTTON_APPLY,
			       GNOME_STOCK_BUTTON_CLOSE,
			       GNOME_STOCK_BUTTON_HELP,
			       NULL);
  gtk_signal_connect (GTK_OBJECT (dialog), "clicked", 
		      (GtkSignalFunc) database_dialog_cb, dialog);
  D_FUNC_END;
}
/****************************************************************************
 * Callback functions
 ***************************************************************************/
static void
database_dialog_cb (GnomeDialog *dialog, gint button, gpointer data)
{
  GXsnmp_database_dialog   *gx_dialog;

  D_FUNC_START;
  g_return_if_fail (data != NULL);
  gx_dialog = (GXSNMP_DATABASE_DIALOG (data));
  switch (button)
    {
    case 0:
      g_print ("Button 0\n");
      break;
    case 1:
      g_print ("Button 1\n");
      break;
    case 2:
      g_print ("Button 2\n");
      break;
    case 3:
      g_print ("Button 3\n");
      break;
    case 4:
      g_print ("Button 4\n");
      break;
    case 5:
      g_print ("Button 5\n");
      break;
    case 6:
      g_print ("Apply\n");
      break;
    case 7:                                              /* Close button */
      gnome_dialog_close (GNOME_DIALOG (gx_dialog));
      break;
    case 8:                                              /* HELP button */
      g_print ("Help me! I'm confused!\n");
    default:
      g_print ("Button %d hit, should never happen?\n", button);
      break;
    }
  D_FUNC_END;
}
/****************************************************************************
 * Refresh the database contents.
 ***************************************************************************/
static void
database_refresh_cb (GtkWidget *widget, gpointer data)
{
  GXsnmp_database_dialog   *dialog;
  GXsnmp_sql_configuration sql;
  GXsnmp_configuration     *config;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  dialog = GXSNMP_DATABASE_DIALOG (widget);
  config = &app_info->current_config;
  g_memmove (&sql.G_sql, &app_info->general_db, sizeof (sql.G_sql));
  sql.database = app_info->general_database;
  gxsnmp_sql_panel_put_data (GXSNMP_SQL_PANEL (dialog->engine_specific),
			     &sql, g_sql_enumerate_backends ());
  D_FUNC_END;
}

/*****************************************************************************
**
**  Public function to create a new configuration panel widget
**
*****************************************************************************/

GtkWidget *
gxsnmp_database_dialog_new ()
{
  return GTK_WIDGET (gtk_type_new( gxsnmp_database_dialog_get_type()));
}

/*******************************************************************************
**
**  Private function to create the database dialog window
**
*******************************************************************************/

static void
create_database_dialog ()
{
  app_info->database_dialog = gxsnmp_database_dialog_new ();

  gtk_window_set_title (GTK_WINDOW (app_info->database_dialog), 
			gettext (title));
  gtk_widget_show (app_info->database_dialog);

}

/*******************************************************************************
**
**  Public function to create and display the configuration window.  This
**  function is called by the "Configuration menu" callback.
**
*******************************************************************************/

void
open_database_dialog ()
{

  if (!app_info->database_dialog)
    {
#ifdef DEBUG_CALLBACKS
      g_print("Creating new database dialog\n");
#endif
      create_database_dialog ();
    }
  else
    {
#ifdef DEBUG_CALLBACKS
      g_print("showing old database dialog\n");
#endif
      gtk_widget_hide (app_info->database_dialog);  /* Force the window to */
      gtk_widget_show (app_info->conf_panel);	    /* pop to the top */
    }
  /* In either case... refresh the dialog */
  database_refresh_cb (app_info->database_dialog, NULL);
}

/* EOF */
