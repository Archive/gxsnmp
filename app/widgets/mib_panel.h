/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  This file defines the MENU configuration panel widget
*/

#ifndef __GXSNMP_MIB_PANEL_H__
#define __GXSNMP_MIB_PANEL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_MIB_PANEL (gxsnmp_mib_panel_get_type())

#define GXSNMP_MIB_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MIB_PANEL, GXsnmp_mib_panel)

#define GXSNMP_MIB_PANEL_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_MIB_PANEL, \
			      GXsnmp_mib_panelClass)

#define GXSNMP_IS_MIB_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MIB_PANEL)

#define GXSNMP_IS_MIB_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MIB_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_mib_panel	GXsnmp_mib_panel;
typedef struct _GXsnmp_mib_panelClass  	GXsnmp_mib_panelClass;

struct _GXsnmp_mib_panel
{
  GtkVBox  	  vbox; 		/* The VBox base structure */
  GtkList	 *alist;
  GtkList	 *slist;
};

struct _GXsnmp_mib_panelClass
{
  GtkFrameClass parent_class;		/* The frame class base structure */

  void (* changed) (GXsnmp_mib_panel *panel);  /* The "changed" signal */
};

/*******************************************************************************
**
**  Data structure used to load and unload the panel
**
*******************************************************************************/

typedef struct _GXsnmp_mib_configuration GXsnmp_mib_configuration;

struct _GXsnmp_mib_configuration
{
	GSList * mibs;
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkType	    gxsnmp_mib_panel_get_type	(void);

GtkWidget * gxsnmp_mib_panel_new	();

void        gxsnmp_mib_panel_get_data   (GXsnmp_mib_panel 	  *panel,
					 GXsnmp_mib_configuration *c);

void	    gxsnmp_mib_panel_put_data	(GXsnmp_mib_panel 	  * panel,
					 GXsnmp_mib_configuration * c);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_MIB_PANEL_H__ */
