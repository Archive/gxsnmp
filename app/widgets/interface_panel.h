/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (c) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  interface_panel.h -- Network interface configuration panel
**
**  interface_panel.h implements a widget that is used to enter and edit
**  information about a host network interface.
*/

#ifndef __GXSNMP_INTERFACE_PANEL_H__
#define __GXSNMP_INTERFACE_PANEL_H__
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_INTERFACE_PANEL (gxsnmp_interface_panel_get_type())

#define GXSNMP_INTERFACE_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_INTERFACE_PANEL, \
			GXsnmp_interface_panel)

#define GXSNMP_INTERFACE_PANEL_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_INTERFACE_PANEL, \
			      GXsnmp_interface_panelClass)

#define GXSNMP_IS_INTERFACE_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_INTERFACE_PANEL)

#define GXSNMP_IS_INTERFACE_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_INTERFACE_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_interface_panel	     GXsnmp_interface_panel;
typedef struct _GXsnmp_interface_panelClass  GXsnmp_interface_panelClass;

struct _GXsnmp_interface_panel
{
  GtkVBox    	  dialog;		/* The dialog base structure */

  GtkWidget	* name;			/* Name of the interface */
  GtkWidget	* transport;		/* Transport method */
  GtkWidget	* address;		/* Interface address */
  GtkWidget	* netmask;		/* Netmask associated with interface */
  GtkWidget	* netmask_label;	/* Label to be displayed for netmask */
  GtkTooltips   * tooltips;             /* Tooltips object */
  GtkWidget     * netmask_event;        /* Eventbox for netmask label */
  GtkWidget     * address_event;        /* Eventbox for address label */
  GtkWidget	* created;		/* Creation date of object */
  GtkWidget	* modified;		/* Modification date of object */
  GList         * transport_names;	/* List of possible transport names */
  GList         * transport_values;	/* List of possible transport values */
};

struct _GXsnmp_interface_panelClass
{
  GtkVBoxClass parent_class;		/* The VBox class base structure */

  void (* changed) (GXsnmp_interface_panel *panel);  /* The "changed" signal */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

guint	    gxsnmp_interface_panel_get_type (void);

GtkWidget * gxsnmp_interface_panel_new      ();

void        gxsnmp_interface_panel_put_data (GXsnmp_interface_panel * panel,
                                             DB_interface           * dbi);

gboolean    gxsnmp_interface_panel_get_data (GXsnmp_interface_panel * panel,
					     DB_interface           * dbi);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_INTERFACE_PANEL_H__ */


