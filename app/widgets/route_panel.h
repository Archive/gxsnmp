/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The route_panel widget is used to display and edit route data.
 */
#ifndef __GXSNMP_ROUTE_PANEL_H__
#define __GXSNMP_ROUTE_PANEL_H__

#include <gnome.h>
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "panel_utility.h"

/****************************************************************************
 * Widget macros
 ***************************************************************************/
#define GXSNMP_TYPE_ROUTE_PANEL (gxsnmp_route_panel_get_type ())
#define GXSNMP_ROUTE_PANEL(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_ROUTE_PANEL, \
                        GXsnmp_route_panel)
#define GXSNMP_ROUTE_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_ROUTE_PANEL, \
                              GXsnmp_route_panelCLass)
#define GXSNMP_IS_ROUTE_PANEL(obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_ROUTE_PANEL)
#define GXSNMP_IS_ROUTE_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_ROUTE_PANEL)
/****************************************************************************
 * Control Blocks
 ***************************************************************************/
typedef struct _GXsnmp_route_panel          GXsnmp_route_panel;
typedef struct _GXsnmp_route_panelClass     GXsnmp_route_panelClass;

struct _GXsnmp_route_panel
{
  GtkVBox     vbox;

  GtkWidget   *route_clist;
  GtkWidget   *target_name;
  GtkWidget   *route_count;
  GtkWidget   *filter_entry;

  GSList      *objs;              /* The objects */
  GSList      *magic;

  DB_host     *target;            /* What node to pull the route from */
  DB_snmp     *snmp;              /* Use this snmp config */

  gpointer    snmp_request;       /* pointer to the running snmp request */
};

struct _GXsnmp_route_panelClass
{
  GtkVBoxClass   parent_class;

};
/****************************************************************************
 * Public API
 ***************************************************************************/
GtkType     gxsnmp_route_panel_get_type       (void);
GtkWidget   *gxsnmp_route_panel_new           (DB_host *host);

#endif
/* EOF */
