/*
**  $Id$
**
**  GXSNMP -- An snmp management application.
**  Copyright (C) 1998 Gregory Mclean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  host_panel.c 
**
**  host_panel.c implements a widget that is used to enter and edit
**  general information about a host.  At this point, the general information
**  about a host consists only of the user-assigned host name.  All other
**  information has been moved to the interface and map structures.
*/

#include <gnome.h>
#include "host_panel.h"
#include "panel_utility.h"
#include "debug.h"
#define PANEL_ROWS 6

/*
**  Table of descriptive labels to display as the first column of the table
*/

static char *host_label[] = {
  N_("Host Name"),
  N_("DNS Name"),
  N_("Description"),
  N_("Contact"),
  N_("Created"),
  N_("Modified")
};

/*
**  Table of tooltips to associate with the table columns
*/

static char *host_tooltip[] = {
  N_("The name to display under the map icon."),
  N_("The DNS name to use when accessing the primary interface on this node."),
  N_("Your description of the host."),
  N_("Your contact information for the host."),
  N_("Date and time this host was added to the database."),
  N_("Date and time this host was modified in the database.")
};

/*
**  Signal information
*/

enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

/*******************************************************************************
**
**  Local forward references
**
*******************************************************************************/

static void    host_panel_class_init 	(GXsnmp_host_panelClass  *klass);

static void    host_panel_init       	(GXsnmp_host_panel       *panel);

static void    changed_cb		(GtkWidget		 *widget,
				 	 gpointer		  data);

/*******************************************************************************
**
**  gxsnmp_host_panel_get_type ()
**
*******************************************************************************/

guint
gxsnmp_host_panel_get_type()
{
  static guint gp_type = 0;

  if (!gp_type)
    {
      GtkTypeInfo gp_info =
      {
	"GXsnmp_host_panel",
	sizeof (GXsnmp_host_panel),
	sizeof (GXsnmp_host_panelClass),
	(GtkClassInitFunc) host_panel_class_init,
	(GtkObjectInitFunc) host_panel_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };
      gp_type = gtk_type_unique (gtk_vbox_get_type (), &gp_info);
    }
  return gp_type;
}

/*****************************************************************************
**
**  The class initialization subroutine
**
*****************************************************************************/

static void
host_panel_class_init (GXsnmp_host_panelClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  signals[CHANGED_SIGNAL] =
    gtk_signal_new("changed",
                   GTK_RUN_FIRST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (GXsnmp_host_panelClass, changed),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  class->changed = NULL;
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
host_panel_init (GXsnmp_host_panel *panel)
{
  GtkWidget 	*table;
  GtkTooltips	*tooltips;
  GtkWidget	*label;
  GtkWidget 	*event_box;
  int	     	 i;
  D_FUNC_START;
/*
**  The controls are organized in a PANEL_ROWS by 2 table
*/

  table = gtk_table_new (PANEL_ROWS, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table), 4);
  gtk_container_border_width (GTK_CONTAINER (table), 0);
  gtk_container_add (GTK_CONTAINER (panel), table);

/*
**  Start by constructing all of the label widgets and adding the tooltips.
*/

  tooltips = gtk_tooltips_new ();
  for (i = 0; i < PANEL_ROWS; i++)
    {
      label = gtk_label_new (gettext(host_label[i]));
      gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
      event_box = gtk_event_box_new ();
      gtk_container_add (GTK_CONTAINER (event_box), label);
      gtk_tooltips_set_tip (tooltips, event_box, gettext(host_tooltip[i]), 
			    NULL);
      gtk_table_attach (GTK_TABLE(table), event_box, 0, 1, i, i + 1,
                        GTK_FILL, 0, 0, 0);
    }

/*
**  The first control is "Host Name"
*/

  panel->name = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT(panel->name), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->name, 1, 2, 0, 1,
                    GTK_FILL, 0, 0, 0);

/*
**  The second control is "DNS Name"
*/

  panel->dns_name = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT(panel->dns_name), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->dns_name, 1, 2, 1, 2,
                    GTK_FILL, 0, 0, 0);

/*
**  The third control is "Description"
*/

  panel->description = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT(panel->description), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->description, 1, 2, 2, 3,
                    GTK_FILL, 0, 0, 0);

/*
**  The fourth control is "Contact"
*/

  panel->contact = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT(panel->contact), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->contact, 1, 2, 3, 4,
                    GTK_FILL, 0, 0, 0);

/*
**  The fifth field is "Created"
*/

  panel->created = gtk_entry_new ();
  gtk_widget_set_sensitive (panel->created, FALSE);
  gtk_table_attach (GTK_TABLE (table), panel->created, 1, 2, 4, 5,
                    GTK_FILL, 0, 0, 0);

/*
**  The sixth field is "Modified"
*/

  panel->modified = gtk_entry_new ();
  gtk_widget_set_sensitive (panel->modified, FALSE);
  gtk_table_attach (GTK_TABLE (table), panel->modified, 1, 2, 5, 6,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show_all (table);
  D_FUNC_END;
}

/*******************************************************************************
**
**  Callback function for when any of the child control widgets is changed.
**  Emit a "changed" signal.
**
*******************************************************************************/

static void
changed_cb(GtkWidget *widget, gpointer data)
{
  GXsnmp_host_panel *panel;         /* Pointer to  panel widget */
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  panel = GXSNMP_HOST_PANEL(data);  /* Argument --> panel widget */

  gtk_signal_emit (GTK_OBJECT(panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}

/*******************************************************************************
**
**  Public function to create a new widget
**
*******************************************************************************/

GtkWidget *
gxsnmp_host_panel_new()
{
  return GTK_WIDGET ( gtk_type_new ( gxsnmp_host_panel_get_type()));
}

/*******************************************************************************
**
**  Public function to copy settings from a host object into the widgets.
**
*******************************************************************************/

void
gxsnmp_host_panel_put_data (GXsnmp_host_panel * panel,
			    DB_host           * dbh)
{
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_HOST_PANEL(panel));
  g_return_if_fail (dbh != NULL);

  panel->rowid = dbh->rowid;

  gtk_entry_set_text(GTK_ENTRY(panel->name),        
		     dbh->name        ? dbh->name        : "");
  gtk_entry_set_text(GTK_ENTRY(panel->dns_name),    
		     dbh->dns_name    ? dbh->dns_name    : "");
  gtk_entry_set_text(GTK_ENTRY(panel->description), 
		     dbh->description ? dbh->description : "");
  gtk_entry_set_text(GTK_ENTRY(panel->contact),     
		     dbh->contact     ? dbh->contact     : "");
  gtk_entry_set_text(GTK_ENTRY(panel->created),
                     dbh->created     ? dbh->created     : "");
  gtk_entry_set_text(GTK_ENTRY(panel->modified), 
		     dbh->modified    ? dbh->modified    : "");
  D_FUNC_END;
}

/*******************************************************************************
**
**  Public function to read settings from the panel widgets back into a
**  host object.
**
**  Returns TRUE if something changed, FALSE otherwise.
**
*******************************************************************************/

gboolean
gxsnmp_host_panel_get_data (GXsnmp_host_panel * panel,
			    DB_host           * dbh)
{
  gint       changed = FALSE;
  D_FUNC_START;
  g_return_val_if_fail (panel != NULL, FALSE);
  g_return_val_if_fail (GXSNMP_IS_HOST_PANEL(panel), FALSE);
  g_return_val_if_fail (dbh != NULL, FALSE);
  if (dbh->rowid != panel->rowid)
    {
      DPRT ("gxsnmp_host_panel_get_data: dbh->rowid != panel->rowid");
      g_warning ("gxsnmp_host_panel_get_data: dbh->rowid != panel->rowid");
      D_FUNC_END;
      return FALSE;
    }
  changed |= update_string_field(&dbh->name, 
		gtk_entry_get_text (GTK_ENTRY (panel->name)));

  changed |= update_string_field (&dbh->dns_name,
		gtk_entry_get_text (GTK_ENTRY (panel->dns_name)));

  changed |= update_string_field (&dbh->description,
		gtk_entry_get_text (GTK_ENTRY (panel->description)));

  changed |= update_string_field (&dbh->contact,
		gtk_entry_get_text (GTK_ENTRY (panel->contact)));
  D_FUNC_END;
  return (changed != FALSE);
}

/* EOF */


