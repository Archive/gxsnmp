/*
** $Id$
**
**  GXSNMP -- An snmp mangament application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Summary dialog code
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "summary_dialog.h"  

/*
**  Table of descriptive labels to display as the first column of the table
*/

#define PANEL_ROWS 5

static char *summary_labels[] = {
  N_("Known Hosts"),
  N_("Known Networks"),
  N_("Outstanding Alarms"),
  N_("Pending events"),
  N_("Pending Tasks")
};

static char *window_title = N_("GXSNMP Status Summary");


/*******************************************************************************
**
**  Local forward references
**
*******************************************************************************/

static void summary_dialog_class_init	(GXsnmp_summary_dialogClass *klass);

static void summary_dialog_init		(GXsnmp_summary_dialog	    *dialog);

/*******************************************************************************
**
**  gxsnmp_summary_dialog_get_type ()
**
*******************************************************************************/

guint
gxsnmp_summary_dialog_get_type()
{
  static guint summary_type = 0;

  if (!summary_type)
    {
      GtkTypeInfo summary_info =
      {
        "GXsnmp_summary_dialog",
        sizeof (GXsnmp_summary_dialog),
        sizeof (GXsnmp_summary_dialogClass),
        (GtkClassInitFunc) summary_dialog_class_init,
        (GtkObjectInitFunc) summary_dialog_init,
        (GtkArgSetFunc) NULL,
        (GtkArgGetFunc) NULL
      };
      summary_type = gtk_type_unique (gnome_dialog_get_type(), &summary_info);
    }
  return summary_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
summary_dialog_class_init (GXsnmp_summary_dialogClass *klass)
{
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
summary_dialog_init (GXsnmp_summary_dialog *dialog)
{
  GtkWidget *table;
  GtkWidget *label;
  GtkWidget **widget_list;
  int        i;

  gtk_window_set_title (GTK_WINDOW(dialog), _(window_title));

  gnome_dialog_append_buttons (GNOME_DIALOG (dialog),
			       GNOME_STOCK_BUTTON_CLOSE,
			       NULL);
/*
**  The controls are organized in a PANEL_ROWS by 2 table
*/

  table = gtk_table_new (PANEL_ROWS, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE(table), 4);
  gtk_table_set_col_spacings (GTK_TABLE(table), 4);
  gtk_container_border_width (GTK_CONTAINER(table), 5);
  gtk_widget_show (table);
  gtk_box_pack_start (GTK_BOX( GNOME_DIALOG (dialog)->vbox),
		      table, TRUE, TRUE, 0);

/*
**  Construct each field, one at a time
*/

  widget_list = &dialog->hosts;		/* Start with first widget */

  for (i = 0; i < PANEL_ROWS; i++)
    {
      label = gtk_label_new (gettext(summary_labels[i]));
      gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
      gtk_table_attach (GTK_TABLE(table), label, 0, 1, i, i + 1,
                        GTK_FILL, 0, 0, 0);
      gtk_widget_show (label);
      widget_list[i] = gtk_entry_new ();
      gtk_widget_set_sensitive (widget_list[i], FALSE);
      gtk_table_attach (GTK_TABLE (table), widget_list[i],
                        1, 2, i, (i + 1),
                        GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK,
                        2, 1);
      gtk_widget_show (widget_list[i]);
    }
}

/*******************************************************************************
**
**  Public function to create a new widget
**
*******************************************************************************/

GtkWidget *
gxsnmp_summary_dialog_new()
{
  return GTK_WIDGET ( gtk_type_new ( gxsnmp_summary_dialog_get_type()));
}


/*******************************************************************************
**
**  Public function to load settings into the panel widgets
**
*******************************************************************************/

void
gxsnmp_summary_dialog_put_data (GXsnmp_summary_dialog    *dialog,
                           	GXsnmp_summary_data	 *d)
{

  char buf[16];

  g_return_if_fail (dialog != NULL);
  g_return_if_fail (GXSNMP_IS_SUMMARY_DIALOG(dialog));

  snprintf (buf, sizeof(buf), "%i", d->host_count);
  gtk_entry_set_text(GTK_ENTRY(dialog->hosts),    buf);

  snprintf(buf, sizeof(buf), "%i", d->network_count);
  gtk_entry_set_text(GTK_ENTRY(dialog->networks), buf);

  snprintf (buf, sizeof(buf), "%i", d->alarm_count);
  gtk_entry_set_text(GTK_ENTRY(dialog->alarms),   buf);

  snprintf (buf, sizeof(buf), "%i", d->event_count);
  gtk_entry_set_text(GTK_ENTRY(dialog->events),   buf);

  snprintf (buf, sizeof(buf), "%i", d->task_count);
  gtk_entry_set_text(GTK_ENTRY(dialog->tasks),    buf);
}

/* EOF */
