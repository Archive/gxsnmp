/**
 * gnome_dialog_create_button: Create a pixmap button, but instead
 * of placing the button in the buttonbar, return the widget handle to the
 * caller, so the caller can place the button in the client area.
 *
 * @dialog: &GnomeDialog to add the button to.
 * @button_name: Name of the button, or stock button #define.
 * @pixmap_name: Stock pixmap name.
 *
 **/
GtkWidget *
gnome_dialog_create_button (GnomeDialog * dialog,
                            const gchar * button_name,
                            const gchar * pixmap_name);
