/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  This file defines the SQL configuration panel widget
*/

#ifndef __GXSNMP_SQL_PANEL_H__
#define __GXSNMP_SQL_PANEL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <g_sql.h>

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_SQL_PANEL (gxsnmp_sql_panel_get_type())

#define GXSNMP_SQL_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_SQL_PANEL, GXsnmp_sql_panel)

#define GXSNMP_SQL_PANEL_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_SQL_PANEL, \
			      GXsnmp_sql_panelClass)

#define GXSNMP_IS_SQL_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_SQL_PANEL)

#define GXSNMP_IS_SQL_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_SQL_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_sql_panel	GXsnmp_sql_panel;
typedef struct _GXsnmp_sql_panelClass   GXsnmp_sql_panelClass;

struct _GXsnmp_sql_panel
{
  GtkVBox  	  vbox; 		/* The VBox base structure */
  GtkWidget	* host;			/* The "SQL Host" entry field */
  GtkWidget	* user;			/* The "SQL User" entry field */
  GtkWidget	* password;		/* The "SQL Password" entry field */
  GtkWidget	* port;			/* The "SQL Port" spinbutton */
  GtkWidget     * db_table;             /* Default table/view to use */
};

struct _GXsnmp_sql_panelClass
{
  GtkFrameClass parent_class;		/* The frame class base structure */

  void (* changed) (GXsnmp_sql_panel *panel);  /* The "changed" signal */
};

/*******************************************************************************
**
**  Data structure used to load and unload the panel
**
*******************************************************************************/

typedef struct _GXsnmp_sql_configuration GXsnmp_sql_configuration;

struct _GXsnmp_sql_configuration
{
  struct _G_sql	G_sql;			/* See lib/g_sql.h */
  gchar *	database;		/* SQL database name */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkType	    gxsnmp_sql_panel_get_type	(void);

GtkWidget * gxsnmp_sql_panel_new	(GList 		* sql_engines);

void        gxsnmp_sql_panel_get_data   (GXsnmp_sql_panel 	  *panel,
					 GXsnmp_sql_configuration *c);

void	    gxsnmp_sql_panel_put_data	(GXsnmp_sql_panel 	  * panel,
					 GXsnmp_sql_configuration * c, 
				 	 GList			  * engines);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_SQL_PANEL_H__ */
