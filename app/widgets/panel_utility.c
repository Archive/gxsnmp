/*
**  $Id$
**
**  GXSNMP -- An snmp management application.
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Utility functions for panel manipulation
*/

#include <glib.h>

/******************************************************************************
**
**  Function to update a field.  What a pain!
**
**  DB_val is a pointer to the field pointer in the record.  The contents of
**  the field pointer may be NULL, the field pointer may be zero length.
**
**  new_val is a reference pointer to the new value.  It may not be NULL. It
**  may point to a zero length string however.
**
**  The new_val field is stripped of leading and trailing blanks.  If the
**  result is a zero length string, the zero length string is converted
**  into a NULL.
**
**  If the 
**
**  Returns TRUE if the field changed,  false otherwise.
**
******************************************************************************/

gboolean
update_string_field (gchar ** DB_val, gchar * new_val)
{
  gchar * new_copy;

  new_copy = g_strstrip (g_strdup (new_val)); /* Copy the new value */

  if (!strlen(new_copy))	/* If, after stripping leading and trailing */
    {				/* blanks, the result is zero length, then */
      g_free (new_copy);	/* replace the zero length string with a */
      new_copy = NULL;		/* NULL. */
    }

  if (!*DB_val && !new_copy)	/* If the old value was NULL, and the new */ 
    return FALSE;		/* value is NULL, the field did not change */

  if (!*DB_val)			/* If the old value was NULL, and the new */
    {				/* value is non-zero-length, then replace */
      *DB_val = new_copy;	/* the NULL with the new field value, and */
      return TRUE;		/* indicate that the field changed. */
    }

  if (!new_copy)		/* If the old value was not NULL, and the */
    {				/* new value is NULL, then free the old */
      g_free (*DB_val);		/* data, store NULL as the new value, and */
      *DB_val = NULL;		/* indicate that the field has changed */
      return TRUE;		
    }

  if (!strcmp(*DB_val, new_copy))	/* If the two strings compare equal */
    {
      g_free (new_copy);		/* Get rid of the identical string */
      return FALSE;			/* The field did not change */
    }

  g_free (*DB_val);  		/* Get rid of the old string value */
  *DB_val = new_copy;		/* Store the new string value */
  return TRUE;			/* The field changed */
}

/* EOF */





