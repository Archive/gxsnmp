/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  graph_panel.h -- implements a widget that is used to enter and
**  		     edit information about a map representation of an item
*/

#ifndef __GXSNMP_GRAPH_PANEL_H__
#define __GXSNMP_GRAPH_PANEL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_GRAPH_PANEL (gxsnmp_graph_panel_get_type())

#define GXSNMP_GRAPH_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_GRAPH_PANEL, \
			GXsnmp_graph_panel)

#define GXSNMP_GRAPH_PANEL_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_GRAPH_PANEL, \
			      GXsnmp_graph_panelClass)

#define GXSNMP_IS_GRAPH_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_GRAPH_PANEL)

#define GXSNMP_IS_GRAPH_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_GRAPH_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_graph_panel	 GXsnmp_graph_panel;
typedef struct _GXsnmp_graph_panelClass  GXsnmp_graph_panelClass;

struct _GXsnmp_graph_panel
{
  GtkVBox         vbox;			/* The Vertical Box base structure */
  GtkWidget	* x;			/* X location on map */
  GtkWidget 	* y;			/* Y location on map */
  GtkWidget	* pixmap;		/* Pixmap to display on map */
  GtkWidget	* created;		/* Creation date */
  GtkWidget	* modified;		/* Modification date */
  gint		  rowid;		/* Rowid of entry being edited */
};

struct _GXsnmp_graph_panelClass
{
  GtkVBoxClass parent_class;		/* The VBox class base structure */

  void (* changed) (GXsnmp_graph_panel *panel);  /* "changed" signal */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

guint	    gxsnmp_graph_panel_get_type (void);

GtkWidget * gxsnmp_graph_panel_new (void);

void        gxsnmp_graph_panel_put_data  (GXsnmp_graph_panel * panel,
                            		  DB_graph           * dbg);

gboolean    gxsnmp_graph_panel_get_data  (GXsnmp_graph_panel * panel,
                                          DB_graph           * dbg);

void        gxsnmp_graph_panel_set_state (GXsnmp_graph_panel * panel, 
					  gboolean             state);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_GRAPH_PANEL_H__ */
