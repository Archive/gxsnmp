/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright 1998, 1999 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The map_panel widget is used to enter in and edit map data.
 */
#include <config.h>
#include <gnome.h>
#include "map_panel.h"
#include "panel_utility.h"
#include "debug.h"

#ifndef ELEMENTS
#define ELEMENTS(x) (sizeof (x) / sizeof (x[0]))
#endif
/****************************************************************************
 * Static Data
 ***************************************************************************/
static char *map_label[] = {
  N_("Name"),
  N_("Description"),
  N_("Tab"),
  N_("Created"),
  N_("Modified"),
  N_("Display"),
};
static char *map_tooltip[] = {
  N_("The name of this map."),
  N_("Your description of this map."),
  N_("A short descriptive tag that will be displayed on the notebook tab for "
     "this map."),
  N_("Date and time this network was added to the database."),
  N_("Date and time this network was modified in the database."),
  N_("Check this item if the map should be displayed in the notebook tabs of "
     "the main window.")
};  
/****************************************************************************
 * Signals
 ***************************************************************************/
enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };
/****************************************************************************
 * Forward declarations
 ***************************************************************************/
static void          map_panel_class_init   (GXsnmp_map_panelClass    *klass);
static void          map_panel_init         (GXsnmp_map_panel         *panel);
static void          changed_cb             (GtkWidget                *widget,
					     gpointer                 data);
/****************************************************************************
 * Widget Methods
 ***************************************************************************/
GtkType
gxsnmp_map_panel_get_type ()
{
  static GtkType map_type = 0;
  if (!map_type)
    {
      GtkTypeInfo map_info = 
      {
	"GXsnmp_map_panel",
	sizeof (GXsnmp_map_panel),
	sizeof (GXsnmp_map_panelClass),
	(GtkClassInitFunc) map_panel_class_init,
	(GtkObjectInitFunc) map_panel_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      map_type = gtk_type_unique (gtk_vbox_get_type (), &map_info);
    }
  return map_type;
}
/****************************************************************************
 * Class init function
 ***************************************************************************/
static void
map_panel_class_init (GXsnmp_map_panelClass *klass)
{
  GtkObjectClass    *object_class;
  object_class = (GtkObjectClass *)klass;
  signals[CHANGED_SIGNAL] =
    gtk_signal_new("changed",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET (GXsnmp_map_panelClass, changed),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  klass->changed = NULL;
}
/****************************************************************************
 * Object init
 ***************************************************************************/
static void
map_panel_init (GXsnmp_map_panel *panel)
{
  GtkWidget     *table;
  GtkTooltips   *tooltips;
  GtkWidget     *label;
  GtkWidget     *event_box;
  gint          i;
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  table = gtk_table_new (ELEMENTS (map_label), 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table), 4);
  gtk_container_border_width (GTK_CONTAINER (table), 0);
  gtk_container_add (GTK_CONTAINER (panel), table);
  tooltips = gtk_tooltips_new ();
  for (i = 0; i < ELEMENTS (map_label); i++)
    {
      label = gtk_label_new (gettext(map_label[i]));
      gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
      event_box = gtk_event_box_new ();
      gtk_container_add (GTK_CONTAINER (event_box), label);
      gtk_tooltips_set_tip (tooltips, event_box, 
			    gettext(map_tooltip[i]), NULL);
      gtk_table_attach (GTK_TABLE (table), event_box, 0, 1, i, i+1, 
			GTK_FILL, 0, 0, 0);
    }
  /* entry widgets */
  panel->name = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->name), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->name, 1, 2, 0, 1, 
		    GTK_FILL, 0, 0, 0);
  panel->description = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->description), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->description, 1, 2, 1, 2, 
		    GTK_FILL, 0, 0, 0);
  panel->tab = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->tab), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach(GTK_TABLE(table), panel->tab, 1, 2, 2, 3, GTK_FILL, 0, 0,0);
  panel->created = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->created), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->created, 1, 2, 3, 4,
		    GTK_FILL, 0, 0, 0);
  panel->modified = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->modified), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->modified, 1, 2, 4, 5,
		    GTK_FILL, 0, 0, 0);
  panel->display = gtk_check_button_new ();
  gtk_table_attach (GTK_TABLE (table), panel->display, 1, 2, 5, 6,
		    GTK_FILL, 0, 0, 0);
  /* Finallu show the whole thing */
  gtk_widget_show_all (table);
  D_FUNC_END;
}
/****************************************************************************
 * The 'changed' callback.
 ***************************************************************************/
static void
changed_cb (GtkWidget *widget, gpointer data)
{
  GXsnmp_map_panel *panel;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  panel = GXSNMP_MAP_PANEL (data);
  gtk_signal_emit (GTK_OBJECT (panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}
/****************************************************************************
 * Public functions 
 ***************************************************************************/
GtkWidget *
gxsnmp_map_panel_new ()
{
  return GTK_WIDGET (gtk_type_new (gxsnmp_map_panel_get_type ()));
}
void
gxsnmp_map_panel_put_data (GXsnmp_map_panel *panel, DB_map *dbm)
{
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_MAP_PANEL (panel));
  g_return_if_fail (dbm != NULL);

  panel->rowid = dbm->rowid;
  gtk_entry_set_text (GTK_ENTRY (panel->name),
		      dbm->name               ? dbm->name : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->description),
		      dbm->description        ? dbm->description : "");
  gtk_entry_set_text (GTK_ENTRY (panel->tab),
		      dbm->tab                ? dbm->tab : "");
  gtk_entry_set_text (GTK_ENTRY (panel->created),
		      dbm->created             ? dbm->created : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->modified), 
		      dbm->modified            ? dbm->modified : "");
  D_FUNC_END;
}
gboolean 
gxsnmp_map_panel_get_data (GXsnmp_map_panel *panel, DB_map *dbm)
{
  gint changed = FALSE;
  D_FUNC_START;
  g_return_val_if_fail (panel != NULL, FALSE);
  g_return_val_if_fail (GXSNMP_IS_MAP_PANEL (panel), FALSE);
  g_return_val_if_fail (dbm != NULL, FALSE);
  if (dbm->rowid != panel->rowid)
    {
      DPRT ("gxsnmp_map_panel_get_data: dbm->rowid != panel->rowid ???");
      g_warning ("gxsnmp_map_panel_get_data: dbm->rowid != panel->rowid ???");
      D_FUNC_END;
      return FALSE;
    }
  changed |= update_string_field (&dbm->name,
			 gtk_entry_get_text (GTK_ENTRY (panel->name)));
  changed |= update_string_field (&dbm->description,
			 gtk_entry_get_text (GTK_ENTRY (panel->description)));
  changed |= update_string_field (&dbm->tab,
			 gtk_entry_get_text (GTK_ENTRY (panel->tab)));
  D_FUNC_END;
  return (changed != FALSE);
}
/* EOF */

