/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright 1998,1999 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The network_panel widget is used to enter in and edit network data.
 */
#ifndef __GXSNMP_NETWORK_PANEL_H__
#define __GXSNMP_NETWORK_PANEL_H__
#include <gnome.h>
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
BEGIN_GNOME_DECLS
/****************************************************************************
 * Widget macros
 ***************************************************************************/
#define GXSNMP_TYPE_NETWORK_PANEL (gxsnmp_network_panel_get_type ())
#define GXSNMP_NETWORK_PANEL(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_NETWORK_PANEL, \
			GXsnmp_network_panel)
#define GXSNMP_NETWORK_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_NETWORK_PANEL, \
			      GXsnmp_network_panelClass)
#define GXSNMP_IS_NETWORK_PANEL(obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_NETWORK_PANEL)
#define GXSNMP_IS_NETWORK_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_NETWORK_PANEL)
/****************************************************************************
 * control blocks
 ***************************************************************************/
typedef struct _GXsnmp_network_panel        GXsnmp_network_panel;
typedef struct _GXsnmp_network_panelClass   GXsnmp_network_panelClass;
struct _GXsnmp_network_panel
{
  GtkVBox      vbox;

  GtkWidget    *name;
  GtkWidget    *address;
  GtkWidget    *mask;
  GtkWidget    *speed;
  GtkWidget    *description;
  GtkWidget    *contact;
  GtkWidget    *created;
  GtkWidget    *modified;
  gint         rowid;
};
struct _GXsnmp_network_panelClass
{
  GtkVBoxClass parent_class;

  void (* changed) (GXsnmp_network_panel *panel);
};
/****************************************************************************
 * Public API
 ***************************************************************************/
GtkType         gxsnmp_network_panel_get_type    (void);
GtkWidget       *gxsnmp_network_panel_new        (void);
void            gxsnmp_network_panel_put_data    (GXsnmp_network_panel *panel,
						  DB_network           *dbn);
gboolean        gxsnmp_network_panel_get_data    (GXsnmp_network_panel *panel,
						  DB_network           *dbn);
END_GNOME_DECLS
#endif
/* EOF */
