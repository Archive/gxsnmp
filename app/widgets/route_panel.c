/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The route_panel widget is used to display and edit route data.
 */
#include <gnome.h>
#include "route_panel.h"
#include "panel_utility.h"
#include "debug.h"

/****************************************************************************
 * Static data 
 ***************************************************************************/
static gchar *route_clist_titles[] = {
  N_("Protocol"),
  N_("Destination"),
  N_("Netmask"),
  N_("Gateway"),
  N_("Interface"),
  N_("Metric"),
  N_("Route Age"),
  N_("Type")
};
/****************************************************************************
 * Forward declarations
 ***************************************************************************/
static void        route_panel_class_init   (GXsnmp_route_panelClass *klass);
static void        route_panel_init         (GXsnmp_route_panel      *panel);
/****************************************************************************
 * Widget methods
 ***************************************************************************/
GtkType
gxsnmp_route_panel_get_type ()
{
  static GtkType route_type = 0;
  if (!route_type)
    {
      GtkTypeInfo route_info = 
      {
	"GXsnmp_route_panel",
	sizeof (GXsnmp_route_panel),
	sizeof (GXsnmp_route_panelClass),
	(GtkClassInitFunc) route_panel_class_init,
	(GtkObjectInitFunc) route_panel_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      route_type = gtk_type_unique (gtk_vbox_get_type (), &route_info);
    }
  return route_type;
}
/****************************************************************************
 * Class init function
 ***************************************************************************/
static void 
route_panel_class_init (GXsnmp_route_panelClass *klass)
{
}
/****************************************************************************
 * Object init function
 ***************************************************************************/
#ifndef ELEMENTS
#define ELEMENTS(x) (sizeof (x) / sizeof (x[0]))
#endif
static void
route_panel_init (GXsnmp_route_panel *panel)
{
  GtkWidget    *table;
  GtkWidget    *scrolled;
  gint         i, c_width, c_height;
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  /* 
   * First comes the info/filter table
   */
  table = gtk_table_new (2, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (panel), table, TRUE, TRUE, 0);
  /*
   * Then the actual route clist
   */
  for (i = 0; i < ELEMENTS (route_clist_titles); i++)
    route_clist_titles[i] = g_strdup (gettext(route_clist_titles[i]));
  panel->route_clist = gtk_clist_new_with_titles 
                          (ELEMENTS (route_clist_titles), route_clist_titles);
  gtk_clist_column_titles_passive (GTK_CLIST (panel->route_clist));
  gtk_clist_columns_autosize (GTK_CLIST (panel->route_clist));
  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_ALWAYS);
  gtk_box_pack_start (GTK_BOX (panel), scrolled, FALSE, FALSE, 0);
  gtk_container_add (GTK_CONTAINER (scrolled), panel->route_clist);
  D_FUNC_END;
}

/****************************************************************************
 * Public Widget functions
 ***************************************************************************/
GtkWidget *
gxsnmp_route_panel_new (DB_host *host)
{
  return GTK_WIDGET (gtk_type_new (gxsnmp_route_panel_get_type()));
}

/* EOF */
