/*
**  $Id$
**
**  GXSNMP -- An SNMP management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  snmp_panel.c -- SNMP configuration panel code
**
**  snmp_panel implements a widget that is used to enter/edit SNMP
**  configuration information.  It is used both to enter/edit information
**  about a specific host, and to enter/edit default information.
*/ 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
 
#include <gnome.h>
#include "snmp_panel.h"
#include "panel_utility.h"
#include "debug.h"
/*
**  Table of descriptive labels to display as the first column of the table
*/

#define PANEL_ROWS 6

static gchar *snmp_labels[] = {
  N_("Read Community"),
  N_("Write Community"),
  N_("Version"),
  N_("Port"),
  N_("Timeout"),
  N_("Retries")
};

/*
**  Signal information
*/

enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

/*******************************************************************************
**
**  Local forward references
**
*******************************************************************************/

static void snmp_panel_class_init	(GXsnmp_snmp_panelClass *klass);

static void snmp_panel_init		(GXsnmp_snmp_panel	*panel);

static void changed_cb			(GtkWidget		*widget,
					 gpointer		 data);

/*******************************************************************************
**
**  gxsnmp_snmp_panel_get_type() 
** 
*******************************************************************************/

GtkType
gxsnmp_snmp_panel_get_type ()
{
  static GtkType psc_type = 0;

  if (!psc_type)
    {
      GtkTypeInfo psc_info =
      {
	"GXsnmp_snmp_panel",
	sizeof (GXsnmp_snmp_panel),
	sizeof (GXsnmp_snmp_panelClass),
	(GtkClassInitFunc) snmp_panel_class_init,
	(GtkObjectInitFunc) snmp_panel_init,
        /* reserved 1 */ NULL,
        /* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      psc_type = gtk_type_unique (gtk_vbox_get_type (), &psc_info);
    }
  return psc_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
snmp_panel_class_init (GXsnmp_snmp_panelClass *class)
{

  GtkObjectClass *object_class;

  object_class = GTK_OBJECT_CLASS(class);

  signals[CHANGED_SIGNAL] =
    gtk_signal_new("changed",
                   GTK_RUN_FIRST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (GXsnmp_snmp_panelClass, changed),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  class->changed = NULL;
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
snmp_panel_init (GXsnmp_snmp_panel *panel)
{
  GtkWidget	*table;
  GtkWidget	*label;
  GtkObject     *adjustment;
  int		 i;
  D_FUNC_START;
/*
**  The controls are organized in a PANEL_ROWS by 2 table
*/

  table = gtk_table_new (PANEL_ROWS, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table), 4);
  gtk_container_border_width (GTK_CONTAINER (table), 0);
  gtk_container_add (GTK_CONTAINER (panel), table); 

/*
**  Start by constructing all of the label widgets
*/

  for (i = 0; i < PANEL_ROWS; i++)
    {
      label = gtk_label_new(gettext(snmp_labels[i]));
      gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, i, i + 1,
		       GTK_FILL, 0, 0, 0);
    }

/*
**  The 1st control is "Read Community String"
*/

  panel->read_community = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT(panel->read_community), "changed",
			(GtkSignalFunc) changed_cb, panel);

  gtk_table_attach (GTK_TABLE (table), panel->read_community, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);

/*
**  The 2nd control is "Write Community String"
*/

  panel->write_community = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT(panel->write_community), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->write_community, 1, 2, 1, 2,
		    GTK_FILL, 0, 0, 0);

/*
**  The 3rd control is the "Version" combo box
*/

  panel->version = gtk_combo_new ();
  gtk_signal_connect (GTK_OBJECT(GTK_COMBO(panel->version)->entry), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(panel->version)->entry), FALSE);
  gtk_table_attach(GTK_TABLE(table), panel->version, 1, 2, 2, 3,
		    GTK_FILL, 0, 0, 0);


/*
**  The 4th control is the "SNMP Port" spin button
*/

  adjustment = gtk_adjustment_new (161, 0, 1024, 1, 10, 10);
  panel->port = gtk_spin_button_new ( GTK_ADJUSTMENT (adjustment), 1, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (panel->port), TRUE);
  gtk_signal_connect (GTK_OBJECT(panel->port), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->port, 1, 2, 3, 4,
		    GTK_FILL, 0, 0, 0);

/*
**  The 5th control is the "SNMP Timeout" spin button
*/

  adjustment = gtk_adjustment_new(30, 1, 360, 1, 1, 0);
  panel->timeout = gtk_spin_button_new( GTK_ADJUSTMENT (adjustment), 1, 0);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(panel->timeout), TRUE);
  gtk_signal_connect (GTK_OBJECT(panel->timeout), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->timeout, 1, 2, 4, 5,
		    GTK_FILL, 0, 0, 0);

/*
**  The 6th control is the "SNMP Retries" spin button
*/

  adjustment = gtk_adjustment_new(3, 1, 100, 1, 1, 0);
  panel->retries = gtk_spin_button_new (GTK_ADJUSTMENT (adjustment), 1, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON(panel->retries), TRUE);
  gtk_signal_connect (GTK_OBJECT(panel->retries), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->retries, 1, 2, 5, 6, 
		    GTK_FILL, 0, 0, 0);

  gtk_widget_show_all (table);
  D_FUNC_END;
}

/****************************************************************************
 *
 *  Callback function for when any of the child control widgets is changed.
 *  Emit a "changed" signal.
 *
 ***************************************************************************/

static void
changed_cb(GtkWidget *widget, gpointer data)
{
  GXsnmp_snmp_panel *panel;		/* Pointer to SNMP panel widget */
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  g_return_if_fail (GXSNMP_IS_SNMP_PANEL (data));
  panel = GXSNMP_SNMP_PANEL(data);	/* Argument points to panel widget */

  gtk_signal_emit (GTK_OBJECT(panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}

/****************************************************************************
 *
 *  Public function to create a new widget
 *
 ***************************************************************************/

GtkWidget *
gxsnmp_snmp_panel_new (GList * version_names,
		       GList * version_values)
{
  GXsnmp_snmp_panel *panel;
  D_FUNC_START;
  g_return_val_if_fail (version_names  != NULL, NULL);
  g_return_val_if_fail (version_values != NULL, NULL);

  panel = gtk_type_new (gxsnmp_snmp_panel_get_type ());

  panel->version_names  = version_names;
  panel->version_values = version_values;
  gtk_combo_set_popdown_strings(GTK_COMBO(panel->version), version_names);

  D_FUNC_END;
  return GTK_WIDGET(panel);

}

/*******************************************************************************
**
**  Public function to load settings into the panel widgets
**
*******************************************************************************/

void
gxsnmp_snmp_panel_put_data (GXsnmp_snmp_panel * panel,
			    DB_snmp           * dbs)
{
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_SNMP_PANEL(panel));
  g_return_if_fail (dbs != NULL);

  gtk_entry_set_text(GTK_ENTRY(panel->read_community), dbs->read_c);
  gtk_entry_set_text(GTK_ENTRY(panel->write_community), dbs->write_c);

  if ((panel->version_names) && (panel->version_values))
    gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(panel->version)->entry),
      g_list_nth_data(panel->version_names, 
        g_list_index(panel->version_values, GINT_TO_POINTER(dbs->version)))); 
  
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(panel->port),    dbs->port);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(panel->timeout), dbs->timeout);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(panel->retries), dbs->retries);
  D_FUNC_END;
}
  
/*******************************************************************************
**
**  Public function to unload settings from the panel widgets
**
*******************************************************************************/

gboolean
gxsnmp_snmp_panel_get_data (GXsnmp_snmp_panel * panel,
			    DB_snmp           * dbs)
{
  gint  changed = FALSE;
  gint  i;
  D_FUNC_START;
  g_return_val_if_fail (panel != NULL, FALSE);
  g_return_val_if_fail (GXSNMP_IS_SNMP_PANEL(panel), FALSE);
  g_return_val_if_fail (dbs != NULL, FALSE);
  changed |= update_string_field (&dbs->read_c,
		gtk_entry_get_text (GTK_ENTRY (panel->read_community)));

  changed |= update_string_field (&dbs->write_c,
                gtk_entry_get_text (GTK_ENTRY (panel->write_community)));

  if ((panel->version_names) && (panel->version_values))
    {
      i = GPOINTER_TO_INT(
            g_list_nth_data(panel->version_values,
              g_list_position(panel->version_names,
                g_list_find_custom(panel->version_names,
                  gtk_entry_get_text 
			(GTK_ENTRY (GTK_COMBO (panel->version)->entry)),
                  (GCompareFunc)strcmp))));
      changed |= (i != dbs->version);
      dbs->version = i;
    }
  else dbs->version = 0;
  
  i = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(panel->port));	
  changed |= (i != dbs->port);
  dbs->port = i;

  i = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(panel->timeout));
  changed |= (i != dbs->timeout);
  dbs->timeout = i;

  i = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(panel->retries));
  changed |= (i != dbs->retries);
  dbs->retries = i;
  D_FUNC_END;
  return (changed != FALSE);
}

/* EOF */

