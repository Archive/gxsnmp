/*
**  gnome_dialog_create_button belongs in gnome-dialog, but the code
**  base is frozen until release 1.0, so it's here for now, along with
**  a copy of gnome_dialog_init_action_area, required because it's
**  a static function.  Blah!  - JohnS
*/

#include <gnome.h>
#include <gnome-dialog-create-button.h>

static void
gnome_dialog_button_clicked (GtkWidget   *button,
                             GtkWidget   *dialog)
{
  GList *list;
  int which = 0;
  gboolean click_closes;
 
  g_return_if_fail(dialog != NULL);
  g_return_if_fail(GNOME_IS_DIALOG(dialog));

  click_closes = GNOME_DIALOG(dialog)->click_closes;
  list = GNOME_DIALOG (dialog)->buttons;

  while (list){
    if (list->data == button) {
      gtk_signal_emit_by_name (GTK_OBJECT (dialog), "clicked", which);
      break;
    }
    list = list->next;
    ++which;
  }
 
  /* The dialog may have been destroyed by the clicked
     signal, which is why we had to save self_destruct.
     Users should be careful not to set self_destruct
     and then destroy the dialog themselves too. */

  if (click_closes) {
    gnome_dialog_close(GNOME_DIALOG(dialog));
  }
}




static void
gnome_dialog_init_action_area (GnomeDialog * dialog)
{
  GtkWidget * separator;

  if (dialog->action_area)
    return;

  dialog->action_area = gtk_hbutton_box_new ();
  gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog->action_area),
                             gnome_preferences_get_button_layout());

  gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog->action_area),
                              GNOME_PAD);

  gtk_box_pack_end (GTK_BOX (dialog->vbox), dialog->action_area,
                    FALSE, TRUE, 0);
  gtk_widget_show (dialog->action_area);

  separator = gtk_hseparator_new ();
  gtk_box_pack_end (GTK_BOX (dialog->vbox), separator,
                      FALSE, TRUE,
                      GNOME_PAD_SMALL);
  gtk_widget_show (separator);
}


/**
 * gnome_dialog_create_button: Create a pixmap button, but instead
 * of placing the button in the buttonbar, return the widget handle to the
 * caller, so the caller can place the button in the client area.
 *
 * @dialog: &GnomeDialog to add the button to.
 * @button_name: Name of the button, or stock button #define.
 * @pixmap_name: Stock pixmap name.
 *
 **/
GtkWidget *
gnome_dialog_create_button (GnomeDialog * dialog,
                            const gchar * button_name,
                            const gchar * pixmap_name)
{
  GtkWidget * button;

  g_return_val_if_fail(dialog != NULL, NULL);
  g_return_val_if_fail(GNOME_IS_DIALOG(dialog), NULL);
  g_return_val_if_fail(button_name != NULL, NULL);

  if (pixmap_name != NULL) {
    GtkWidget *pixmap;

    pixmap = gnome_stock_new_with_icon (pixmap_name);
    button = gnome_pixmap_button (pixmap, button_name);
  } else {
    button = gnome_stock_or_ordinary_button (button_name);
  }

  gnome_dialog_init_action_area (dialog);

  GTK_WIDGET_SET_FLAGS (GTK_WIDGET (button), GTK_CAN_DEFAULT);

  gtk_widget_grab_default (button);
  gtk_widget_show (button);

  gtk_signal_connect_after (GTK_OBJECT (button), "clicked",
                            (GtkSignalFunc) gnome_dialog_button_clicked,
                            dialog);

  dialog->buttons = g_list_append (dialog->buttons, button);
  return button;
}

