/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright 1998, 1999 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The map_panel widget is used to enter in and edit map data.
 */
#ifndef __GXSNMP_MAP_PANEL_H__
#define __GXSNMP_MAP_PANEL_H__
#include <gnome.h>

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

BEGIN_GNOME_DECLS
/****************************************************************************
 * Widget macros
 ***************************************************************************/
#define GXSNMP_TYPE_MAP_PANEL (gxsnmp_map_panel_get_type ())
#define GXSNMP_MAP_PANEL(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MAP_PANEL, \
                        GXsnmp_map_panel)
#define GXSNMP_MAP_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_MAP_PANEL, \
                              GXsnmp_map_panelClass)
#define GXSNMP_IS_MAP_PANEL(obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MAP_PANEL)
#define GXSNMP_IS_MAP_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MAP_PANEL)
/****************************************************************************
 * Control Blocks
 ***************************************************************************/
typedef struct _GXsnmp_map_panel        GXsnmp_map_panel;
typedef struct _GXsnmp_map_panelClass   GXsnmp_map_panelClass;
struct _GXsnmp_map_panel 
{
  GtkVBox    vbox;                      /* base class */

  GtkWidget  *name;                     /* Name of the map */
  GtkWidget  *description;              /* Description */
  GtkWidget  *tab;                      /* What to put on the tab */
  GtkWidget  *created;                  /* When it was created */
  GtkWidget  *modified;                 /* Last modification timestamp */
  GtkWidget  *display;                  /* Checkbox to display this on the 
					   window or now */
  gint       rowid;
};
struct _GXsnmp_map_panelClass
{
  GtkVBoxClass    parent_class;
  
  void (* changed) (GXsnmp_map_panel *panel);
};
/****************************************************************************
 * Public API
 ***************************************************************************/
GtkType      gxsnmp_map_panel_get_type         (void);
GtkWidget    *gxsnmp_map_panel_new             (void);
void         gxsnmp_map_panel_put_data         (GXsnmp_map_panel      *panel,
						DB_map                *dbm);
gboolean     gxsnmp_map_panel_get_data         (GXsnmp_map_panel      *panel,
						DB_map                *dbm);
END_GNOME_DECLS
#endif
/* EOF */
