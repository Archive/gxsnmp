/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  This file defines the SNMP configuration panel widget
*/

#ifndef __GXSNMP_SNMP_PANEL_H__
#define __GXSNMP_SNMP_PANEL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_SNMP_PANEL (gxsnmp_snmp_panel_get_type())

#define GXSNMP_SNMP_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_SNMP_PANEL, GXsnmp_snmp_panel)

#define GXSNMP_SNMP_PANEL_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_SNMP_PANEL, \
			      GXsnmp_snmp_panelClass)

#define GXSNMP_IS_SNMP_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_SNMP_PANEL)

#define GXSNMP_IS_SNMP_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_SNMP_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_snmp_panel	GXsnmp_snmp_panel;
typedef struct _GXsnmp_snmp_panelClass  GXsnmp_snmp_panelClass;

struct _GXsnmp_snmp_panel
{
  GtkVBox 	   vbox; 		/* The VBox base structure */
  GtkWidget       *read_community;	/* "Read Community" entry field */
  GtkWidget  	  *write_community;	/* "Write Community" entry field */
  GtkWidget       *version;		/* "SNMP Version" combo box */
  GtkWidget    	  *port;		/* "SNMP Port" spinbutton */
  GtkWidget    	  *timeout;		/* "SNMP Timeout" spinbutton */
  GtkWidget       *retries;		/* "SNMP Retries" spinbutton */

  GtkWidget  	  *enable_label;	/* Text to display in "enabled" */
  GList		  *version_names;	/* List of possible version names */
  GList		  *version_values;	/* List of possible version values */
};

struct _GXsnmp_snmp_panelClass
{
  GtkVBoxClass	   parent_class;	/* The VBox class base structure */

  void (* changed) (GXsnmp_snmp_panel *panel);  /* The "changed" signal */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkType	    gxsnmp_snmp_panel_get_type	(void);

GtkWidget * gxsnmp_snmp_panel_new	(GList *version_names,
					 GList *version_values);

void	    gxsnmp_snmp_panel_put_data (GXsnmp_snmp_panel * panel,
					DB_snmp           * dbs);

gboolean    gxsnmp_snmp_panel_get_data (GXsnmp_snmp_panel * panel,
					DB_snmp		  * dbs);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_SNMP_PANEL_H__ */


