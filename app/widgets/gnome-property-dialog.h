/*  
**  gnome-propertydialog.h - Property dialog
** 
**  Copyright (C) 1998 Tom Tromey
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Library General Public License
**  as published by the Free Software Foundation; either version 2, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
**  Library General Public License for more details.
**
**  You should have received a copy of the GNU Library General Public
**  License along with this program; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
**  02111-1307, USA.  
*/

#ifndef __GNOME_PROPERTY_DIALOG_H__
#define __GNOME_PROPERTY_DIALOG_H__

BEGIN_GNOME_DECLS

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GNOME_TYPE_PROPERTY_DIALOG (gnome_property_dialog_get_type())

#define GNOME_PROPERTY_DIALOG(obj) \
	GTK_CHECK_CAST ((obj), GNOME_TYPE_PROPERTY_DIALOG, GnomePropertyDialog)
	
#define GNOME_PROPERTY_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PROPERTY_DIALOG, \
			      GnomePropertyDialogClass)

#define GNOME_IS_PROPERTY_DIALOG(obj) \
	GTK_CHECK_TYPE (obj, GNOME_TYPE_PROPERTY_DIALOG)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GnomePropertyDialog      GnomePropertyDialog;
typedef struct _GnomePropertyDialogClass GnomePropertyDialogClass;

struct _GnomePropertyDialog
{
	GnomeDialog dialog;

	GtkWidget * ok_button;	     /* OK button.  */
	GtkWidget * apply_button;    /* Apply button.  */
	GtkWidget * cancel_button;   /* Cancel/Close button.  */
	GtkWidget * help_button;     /* Help button.  */
};

struct _GnomePropertyDialogClass
{
	GnomeDialogClass parent_class;

	void (* apply)  (GnomePropertyDialog * propertydialog);
	void (* help)   (GnomePropertyDialog * propertydialog);
        void (* button) (GnomePropertyDialog * propertydialog);
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

guint      gnome_property_dialog_get_type (void);

GtkWidget *gnome_property_dialog_new      (void);

/* Call this when the user changes something in the property dialog */

void  gnome_property_dialog_changed   (GnomePropertyDialog *property_dialog);

/* Call this to explicitly set the "changed" state of the property dialog */

void  gnome_property_dialog_set_state (GnomePropertyDialog *property_dialog,
				       gboolean 	    state);

END_GNOME_DECLS

#endif /* __GNOME_PROPERTY_DIALOG_H__ */
