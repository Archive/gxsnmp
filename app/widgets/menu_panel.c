/*
**  $Id$
**
**  GXSNMP -- An snmp management application.
**  Copyright (C) 1998 Gregory Mclean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  menu_panel.c -- Configuration panel for host and network custom commands.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "menu_panel.h"
#include "debug.h"
#include "menus.h"

/*
**  Table of descriptive labels to display as the first column of the table
*/

static char *menu_label[] = {
  N_("Host"),
  N_("Network")
};

static char *clist_label[] = {
  N_("Menu Entry"),
  N_("Command")
};

/*
**  Signal information
*/

enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

/*******************************************************************************
**
**  Local forward references
**
*******************************************************************************/

static void    menu_panel_class_init	(GXsnmp_menu_panelClass  *klass);

static void    menu_panel_init		(GXsnmp_menu_panel       *panel);

static void    changed_cb     		(GtkWidget		*widget,
					 gpointer		 data);

static void    add_host_button_cb 	(GtkWidget		*widget,
					 gpointer		 data);

static void    edit_host_button_cb	(GtkWidget		*widget,
					 gpointer		 data);

static void    delete_host_button_cb	(GtkWidget		*widget,
					 gpointer		 data);

static void    add_net_button_cb	(GtkWidget		*widget,
					 gpointer		 data);

static void    edit_net_button_cb	(GtkWidget		*widget,
					 gpointer		 data);

static void    delete_net_button_cb	(GtkWidget		*widget,
					 gpointer		 data);

/*******************************************************************************
**
**  gxsnmp_menu_panel_get_type ()
**
*******************************************************************************/

GtkType
gxsnmp_menu_panel_get_type()
{
  static GtkType menu_type = 0;

  if (!menu_type)
    {
      GtkTypeInfo menu_info =
      {
	"GXsnmp_menu_panel",
	sizeof (GXsnmp_menu_panel),
	sizeof (GXsnmp_menu_panelClass),
	(GtkClassInitFunc) menu_panel_class_init,
	(GtkObjectInitFunc) menu_panel_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      menu_type = gtk_type_unique (gtk_vbox_get_type (), &menu_info);
    }
  return menu_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
menu_panel_class_init (GXsnmp_menu_panelClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  signals[CHANGED_SIGNAL] =
    gtk_signal_new("changed",
                   GTK_RUN_FIRST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (GXsnmp_menu_panelClass, changed),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  class->changed = NULL;
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
menu_panel_init (GXsnmp_menu_panel *panel)
{
  GtkWidget *table;
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *scrolled;
  GtkWidget *button;
  GtkWidget *bar;

  char *labels[2];
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
/*
**  The controls are organized in a 2 by 2 table
*/


  table = gtk_table_new (3, 3, FALSE);
  
  gtk_container_add (GTK_CONTAINER(panel), table);

/*
**  Start by constructing the top label widgets
*/

  label = gtk_label_new (gettext(menu_label[0]));
  gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);

  label = gtk_label_new (gettext(menu_label[1]));
  gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE(table), label, 2, 3, 0, 1, GTK_FILL, 0, 0, 0);

/*
** Now 2 of these clist objects
*/

  vbox = gtk_vbox_new (FALSE, 8);
  gtk_table_attach (GTK_TABLE(table), vbox, 0, 1, 1, 2, GTK_FILL, 0, 0, 0);
  gtk_widget_set_usize(vbox, 200, 100);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				  GTK_POLICY_ALWAYS,
				  GTK_POLICY_AUTOMATIC);

  gtk_box_pack_start (GTK_BOX (vbox), scrolled, TRUE, TRUE, 0);
 
  labels[0] = gettext(clist_label[0]);
  labels[1] = gettext(clist_label[1]);

  panel->hlist = gtk_clist_new_with_titles (2, labels);
  gtk_clist_set_selection_mode (GTK_CLIST (panel->hlist), GTK_SELECTION_BROWSE);
  gtk_clist_column_titles_passive (GTK_CLIST (panel->hlist));

  gtk_container_add (GTK_CONTAINER(scrolled), panel->hlist);
  /* 
   * A seperator bar
   */
  DPRT ("Adding seperator.");
  bar = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE(table), bar, 1, 2, 1, 2, 
		    0, GTK_FILL | GTK_EXPAND, 5, 5);

  vbox = gtk_vbox_new (FALSE, 8);
  gtk_table_attach (GTK_TABLE(table), vbox, 2, 3, 1, 2, GTK_FILL, 0, 0, 0);
  gtk_widget_set_usize(vbox, 200, 100);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				  GTK_POLICY_ALWAYS,
				  GTK_POLICY_AUTOMATIC);

  gtk_box_pack_start (GTK_BOX (vbox), scrolled, TRUE, TRUE, 0);
 
  panel->nlist = gtk_clist_new_with_titles (2, labels);
  gtk_clist_set_selection_mode (GTK_CLIST (panel->nlist), GTK_SELECTION_BROWSE);
  gtk_clist_column_titles_passive (GTK_CLIST (panel->nlist));

  gtk_container_add (GTK_CONTAINER(scrolled), panel->nlist);

/*
** Now the buttons at the bottom
*/

  hbox = gtk_hbox_new (FALSE, 8);
  gtk_table_attach (GTK_TABLE(table), hbox, 0, 1, 2, 3, GTK_FILL, 0, 0, 0);

  button = gtk_button_new_with_label("Add");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (add_host_button_cb), panel);
  button = gtk_button_new_with_label("Edit");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (edit_host_button_cb), panel);
  button = gtk_button_new_with_label("Delete");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (delete_host_button_cb), panel);

  hbox = gtk_hbox_new (FALSE, 8);
  gtk_table_attach (GTK_TABLE(table), hbox, 2, 3, 2, 3, GTK_FILL, 0, 0, 0);
  
  button = gtk_button_new_with_label("Add");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (add_net_button_cb), panel);
  button = gtk_button_new_with_label("Edit");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (edit_net_button_cb), panel);
  button = gtk_button_new_with_label("Delete");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (delete_net_button_cb), panel);
  gtk_widget_show_all (table); 
  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function for when the "add to host menu" button is pressed.
**
****************************************************************************/

static void
add_host_button_cb(GtkWidget *widget, gpointer data)
{
  D_FUNC_START;

  /* Create new structure */
  /* Call up edit dialog pointing to structure */
  /* Depending on return code, add to menu or delete again */

  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function for when the "edit host menu" button is pressed.
**
****************************************************************************/

static void
edit_host_button_cb(GtkWidget *widget, gpointer data)
{
  D_FUNC_START;

  /* Extract structure and copy to temp */
  /* Call up edit dialog pointing to structure */
  /* Depending on return code, replace menu entry or delete again */

  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function for when the "delete from host menu" button is pressed.
**
****************************************************************************/

static void
delete_host_button_cb(GtkWidget *widget, gpointer data)
{
  D_FUNC_START;

  /* Call up confirmation dialog */

  gtk_widget_show (gnome_message_box_new ("Are Your Sure", "question",
	 	   GNOME_STOCK_BUTTON_YES, GNOME_STOCK_BUTTON_NO, NULL));

  /* Depending on return code, delete menu entry or cancel out */

  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function for when the "add to network menu" button is pressed.
**
****************************************************************************/

static void
add_net_button_cb(GtkWidget *widget, gpointer data)
{
  D_FUNC_START;

  /* Create new structure */
  /* Call up edit dialog pointing to structure */
  /* Depending on return code, add to menu or delete again */

  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function for when the "edit network menu" button is pressed.
**
****************************************************************************/

static void
edit_net_button_cb(GtkWidget *widget, gpointer data)
{
  D_FUNC_START;

  /* Extract structure and copy to temp */
  /* Call up edit dialog pointing to structure */
  /* Depending on return code, replace menu entry or delete again */

  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function for when the "delete from network menu" button is pressed.
**
****************************************************************************/

static void
delete_net_button_cb(GtkWidget *widget, gpointer data)
{
  D_FUNC_START;

  /* Call up confirmation dialog */
  /* Depending on return code, delete menu entry or cancel out */

  D_FUNC_END;
}

/****************************************************************************
**
**  Callback function for when any of the child control widgets is changed.
**  Emit a "changed" signal.
**
****************************************************************************/

static void
changed_cb(GtkWidget *widget, gpointer data)
{
  GXsnmp_menu_panel *panel;              /* Pointer to MENU panel widget */
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  panel = GXSNMP_MENU_PANEL(data);       /* Argument points to panel widget */

  gtk_signal_emit (GTK_OBJECT(panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}

/****************************************************************************
**
**  Public function to create a new widget
**
****************************************************************************/

GtkWidget *
gxsnmp_menu_panel_new ()
{
  return gtk_type_new (gxsnmp_menu_panel_get_type());
}

/****************************************************************************
**
**  Public function to load settings into the panel widgets
**
****************************************************************************/

void
gxsnmp_menu_panel_put_data (GXsnmp_menu_panel         *panel,
			    GXsnmp_menu_configuration *c)
{
  GSList    *gsl;
  user_menu *new_entry;
  char      *entry[2];
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_MENU_PANEL(panel));

  gsl = c->hostsmd;
  while (gsl)
    {
      new_entry = (user_menu *)gsl->data;
      entry[0] = new_entry->item;
      entry[1] = new_entry->command;

      gtk_clist_append(GTK_CLIST (panel->hlist), entry);
      gsl = gsl->next;
    }
  gsl = c->netmenu;
  while (gsl)
    {
      new_entry = (user_menu *)gsl->data;
      entry[0] = new_entry->item;
      entry[1] = new_entry->command;

      gtk_clist_append(GTK_CLIST (panel->nlist), entry);
      gsl = gsl->next;
    }
  D_FUNC_END;
}


/****************************************************************************
**
**  Public function to copy configuration data from the panel widgets into an
**  MENU configuration control block
**
****************************************************************************/

void
gxsnmp_menu_panel_get_data (GXsnmp_menu_panel         *panel,
                            GXsnmp_menu_configuration *c)
{
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_MENU_PANEL (panel));

  D_FUNC_END;
}

/* EOF */

