/*
** $Id$
**
**  GXSNMP -- An snmp mangament application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Summary panel code
*/

#ifndef __GXSNMP_SUMMARY_DIALOG_H__
#define __GXSNMP_SUMMARY_DIALOG_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_SUMMARY_DIALOG (gxsnmp_summary_dialog_get_type())

#define GXSNMP_SUMMARY_DIALOG(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_SUMMARY_DIALOG, \
        GXsnmp_summary_dialog)

#define GXSNMP_SUMMARY_DIALOG_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_SUMMARY_DIALOG, \
                              GXsnmp_summary_dialogClass)

#define GXSNMP_IS_SUMMARY_DIALOG(obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_SUMMARY_DIALOG)

#define GXSNMP_IS_SUMMARY_DIALOG_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_SUMMARY_DIALOG)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_summary_dialog     	GXsnmp_summary_dialog;
typedef struct _GXsnmp_summary_dialogClass	GXsnmp_summary_dialogClass;

struct _GXsnmp_summary_dialog
{
  GnomeDialog    parent_object;         /* The GnomeDialog base structure */
  GtkWidget     *hosts;           	/* The "Known Hosts" field */
  GtkWidget     *networks;        	/* The "Known Networks" field */
  GtkWidget     *alarms;                /* The "Outstanding Alarms" field */
  GtkWidget	*events;       		/* The "Pending Events" field */
  GtkWidget     *tasks;                 /* The "Pending Tasks" field */
};

struct _GXsnmp_summary_dialogClass
{
  GnomeDialogClass parent_class;        /* The Gnome Dialog parent class */
};

/*******************************************************************************
**
**  Data structure used to load the panel
**
*******************************************************************************/

typedef struct _GXsnmp_summary_data GXsnmp_summary_data;

struct _GXsnmp_summary_data
{
  guint		host_count;		/* Known hosts */
  guint		network_count;		/* Known networks */
  guint		alarm_count;		/* Outstanding alarms */
  guint		event_count;		/* Pending events */
  guint		task_count;		/* Pending tasks */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

guint	    gxsnmp_summary_dialog_get_type	(void);

GtkWidget * gxsnmp_summary_dialog_new		(void);

void	    gxsnmp_summary_dialog_put_data	(GXsnmp_summary_dialog *dialog,
						 GXsnmp_summary_data   *data);
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_SUMMARY_DIALOG_H__ */

/* EOF */
