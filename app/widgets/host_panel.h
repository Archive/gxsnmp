/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (c) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  host_panel.h -- implements a widget that is used to enter and
**  		    edit information about a host.
*/

#ifndef __GXSNMP_HOST_PANEL_H__
#define __GXSNMP_HOST_PANEL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_HOST_PANEL (gxsnmp_host_panel_get_type())

#define GXSNMP_HOST_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_HOST_PANEL, \
			GXsnmp_host_panel)

#define GXSNMP_HOST_PANEL_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_HOST_PANEL, \
			      GXsnmp_host_panelClass)

#define GXSNMP_IS_HOST_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_HOST_PANEL)

#define GXSNMP_IS_HOST_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_HOST_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_host_panel	GXsnmp_host_panel;
typedef struct _GXsnmp_host_panelClass  GXsnmp_host_panelClass;

struct _GXsnmp_host_panel
{
  GtkVBox         vbox;			/* The Vertical Box base structure */
  GtkWidget	* name;			/* Display name for this host */
  GtkWidget	* dns_name;		/* Preferred DNS name for host */
  GtkWidget 	* description;		/* Description of host */
  GtkWidget	* contact;		/* Contact information for host */
  GtkWidget	* created;		/* Host creation date */
  GtkWidget     * modified;             /* Last modification date */
  gint		  rowid;		/* Rowid of item being edited */
};

struct _GXsnmp_host_panelClass
{
  GtkVBoxClass parent_class;		/* The VBox class base structure */

  void (* changed) (GXsnmp_host_panel *panel);  /* "changed" signal */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

guint	    gxsnmp_host_panel_get_type (void);

GtkWidget * gxsnmp_host_panel_new (void);

void        gxsnmp_host_panel_put_data (GXsnmp_host_panel * panel,
                            		DB_host           * dbh);

gboolean    gxsnmp_host_panel_get_data (GXsnmp_host_panel * panel,
                                        DB_host           * dbh);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_HOST_PANEL_H__ */


