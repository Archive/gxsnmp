/*  gnome-propertydialog.c - Property dialog 
 *
 *  Copyright 1998 John Schulien
 *  Derived from gnome-propertybox.c  Copyright (C) 1998 Tom Tromey
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 *  02111-1307, USA.  
 */
#include <config.h>
#include <gnome.h>
#include "gnome-property-dialog.h"

/****************************************************************************
 * 
 *  Static forward references
 *
 ****************************************************************************/

static void gnome_property_dialog_class_init 
				 	 (GnomePropertyDialogClass *klass);

static void gnome_property_dialog_init   (GnomePropertyDialog *property_dialog);

static void clicked_cb 			(GnomePropertyDialog *dialog, 
					 gint		      button,
					 gpointer 	      data);

/****************************************************************************
 *
 *  Signal definitions
 *
 ****************************************************************************/

enum
{
  APPLY_SIGNAL,
  HELP_SIGNAL,
  BUTTON_SIGNAL,
  LAST_SIGNAL
};

static gint signals [LAST_SIGNAL] = { 0 };

/*****************************************************************************
 *
 *  gnome_property_dialog_get_type:
 *
 *  Internal routine that returns the GtkType of the GnomePropertyDialog 
 *  widget, registering it if necessary.
 *
 ****************************************************************************/

guint
gnome_property_dialog_get_type (void)
{
  static guint property_dialog_type = 0;

  if (!property_dialog_type)
    {
      GtkTypeInfo property_dialog_info = {
		"GnomePropertyDialog",
		sizeof (GnomePropertyDialog),
		sizeof (GnomePropertyDialogClass),
		(GtkClassInitFunc) gnome_property_dialog_class_init,
		(GtkObjectInitFunc) gnome_property_dialog_init,
		(GtkArgSetFunc) NULL,
		(GtkArgGetFunc) NULL
      };
    property_dialog_type = gtk_type_unique (gnome_dialog_get_type (),
					    &property_dialog_info);
  }
  return property_dialog_type;
}

/*****************************************************************************
 *
 *  gnome_property_dialog_new: [constructor]
 *
 *  Creates a new GnomePropertyDialog widget.  The PropertyDialog widget
 *  is useful for making consistent configuration dialogs.
 *
 *  When a setting has been made to something in the PropertyDialog, 
 *  your program needs to invoke gnome_property_dialog_changed() to 
 *  signal a change (this will enable the Ok/Apply buttons).
 *
 *  Returns a newly created GnomePropertyDialog widget.
 *
 ****************************************************************************/

GtkWidget *
gnome_property_dialog_new (void)
{
        return gtk_type_new (gnome_property_dialog_get_type ());
}

/*****************************************************************************
 *
 *  gnome_property_dialog_class_init
 *
 *  Performs class initialization for the GnomePropertyDialog class.
 *
 ****************************************************************************/

static void
gnome_property_dialog_class_init (GnomePropertyDialogClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) klass;

  signals[APPLY_SIGNAL] = gtk_signal_new ("apply", 
			    GTK_RUN_LAST,
			    object_class->type,
			    GTK_SIGNAL_OFFSET (GnomePropertyDialogClass, apply),
			    gtk_marshal_NONE__NONE,
			    GTK_TYPE_NONE, 0);

  signals[HELP_SIGNAL] = gtk_signal_new ("help",
			    GTK_RUN_LAST,
			    object_class->type,
			    GTK_SIGNAL_OFFSET (GnomePropertyDialogClass, help),
			    gtk_marshal_NONE__INT,
			    GTK_TYPE_NONE, 1, GTK_TYPE_INT);

  signals[BUTTON_SIGNAL] = 
	gtk_signal_new ("button",
		        GTK_RUN_LAST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GnomePropertyDialogClass, button),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1, GTK_TYPE_INT);

  gtk_object_class_add_signals (object_class, signals, 
				LAST_SIGNAL);

  klass->apply = NULL;
  klass->help = NULL;
}

/*****************************************************************************
 *
 *  Performs instance initialization for the GnomePropertyDialog class.
 *
 *****************************************************************************/

static void
gnome_property_dialog_init (GnomePropertyDialog *property_dialog)
{
  GList * button_list;

  if (gnome_preferences_get_property_box_apply ())
    {
      gnome_dialog_append_buttons (GNOME_DIALOG (property_dialog),
				   GNOME_STOCK_BUTTON_OK,
				   GNOME_STOCK_BUTTON_APPLY,
				   GNOME_STOCK_BUTTON_CLOSE,
				   GNOME_STOCK_BUTTON_HELP,
				   NULL);
    } 
  else 
    {
      gnome_dialog_append_buttons (GNOME_DIALOG (property_dialog),
				   GNOME_STOCK_BUTTON_OK,
				   GNOME_STOCK_BUTTON_CANCEL,
				   GNOME_STOCK_BUTTON_HELP,
				   NULL);
    }

/*
**  Run through the button list to extract the button widgets.  This is
**  still sort of unattractive.
*/

  button_list = GNOME_DIALOG (property_dialog)->buttons;

  property_dialog->ok_button = GTK_WIDGET (button_list->data);
  gtk_widget_set_sensitive (property_dialog->ok_button, FALSE);
  button_list = button_list->next;
	
  if (gnome_preferences_get_property_box_apply ())
    {
      property_dialog->apply_button = GTK_WIDGET(button_list->data);
      gtk_widget_set_sensitive (property_dialog->apply_button, FALSE);
      button_list = button_list->next;
    } 
  else
    property_dialog->apply_button = NULL;
	
  property_dialog->cancel_button = GTK_WIDGET(button_list->data);
  button_list = button_list->next;

  property_dialog->help_button = GTK_WIDGET(button_list->data);

/*
**  Finish up by connecting our callback function
*/
	
  gtk_signal_connect (GTK_OBJECT(property_dialog), "clicked",
		      GTK_SIGNAL_FUNC(clicked_cb), NULL);
}

/*****************************************************************************
 *
 *  Callback function for when one of the dialog buttons is pressed
 *
 ****************************************************************************/

static void
clicked_cb (GnomePropertyDialog *dialog, gint button, gpointer data)
{

  gint			ok_button     = 0;
  gint 			apply_button  = 1;
  gint 			close_button  = 2;
  gint 			help_button   = 3; 

  g_return_if_fail (dialog != NULL);
  g_return_if_fail (GNOME_IS_PROPERTY_DIALOG (dialog));

/*
**  Adjust the button variables according to whether or not we have an
**  apply button.  This sort of illustrates a problem with the idea of
**  assigning integer values to buttons, doesn't it?
*/
 

  if (!dialog->apply_button) 
    {
      apply_button = (-1);		/* We have no apply button */
      close_button = 1;			/* Move up the close and ... */
      help_button  = 2;			/* ... help button indices.  HACK! */ 
    }

/*
**  If the button was "Help", then emit a help signal, passing the value
**  in "data".
*/

  if (button == help_button)
    {
      gtk_signal_emit (GTK_OBJECT (dialog), signals[HELP_SIGNAL], data);
    }

/*
**  If the button was "Ok" or "Apply", then emit an apply signal, passing
**  the value in "data".  Also reset the apply state.
*/

  if ((button == ok_button) || (button == apply_button))
    {
      gtk_signal_emit (GTK_OBJECT (dialog), signals[APPLY_SIGNAL], data);
      gnome_property_dialog_set_state (dialog, FALSE);
   
    }

/*
**  If the button was "Ok" or "Close", then close the underlying dialog.
*/

  if ((button == ok_button) || (button == close_button))  
    {
      gnome_dialog_close (GNOME_DIALOG (dialog));
    }

  if ((button == ok_button) || (button == apply_button) ||
      (button == close_button) || (button == help_button))
    return;
/*
**  Otherwise, emit the "button" signal.
*/

  gtk_signal_emit (GTK_OBJECT (dialog), signals[BUTTON_SIGNAL], button, data);

}

/****************************************************************************
 *
 *  gnome_property_dialog_set_state:
 *
 * @property_dialog: The GnomePropertyDialog that contains the changed data
 *
 * @state:           The state.  TRUE means modified, FALSE means unmodified.
 *
 * This sets the state of the GnomePropertyDialog to the value in @state.
 *
 ****************************************************************************/

void
gnome_property_dialog_set_state (GnomePropertyDialog *property_dialog,
                                 gboolean             state)
{
  g_return_if_fail (property_dialog != NULL);
  g_return_if_fail (GNOME_IS_PROPERTY_DIALOG (property_dialog));

  gtk_widget_set_sensitive (property_dialog->ok_button,  state);

  if (property_dialog->apply_button)
    gtk_widget_set_sensitive (property_dialog->apply_button, state);
}

/****************************************************************************
 *
 *  gnome_property_dialog_changed:
 *
 *  @property_dialog: The GnomePropertyDialog that contains the changed data
 *
 *  When a setting has changed, the code needs to invoke this routine
 *  to make the Ok/Apply buttons sensitive.
 *
 ****************************************************************************/

void
gnome_property_dialog_changed (GnomePropertyDialog *property_dialog)
{
  gnome_property_dialog_set_state (property_dialog, TRUE);
}
/* EOF */

