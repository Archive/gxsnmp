/*
**  $Id$
**
**  GXSNMP -- An snmp management application.
**  Copyright (C) 1998 Gregory Mclean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  mib_panel.c -- Configuration panel for management of MIB modules
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "mib_panel.h"
#include "debug.h"

#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

/*
**  Table of descriptive labels to display the headers of the clists.
*/

static char *mib_label[] = {
  N_("Installed MIBs"),
  N_("Selected MIBs")
};

/*
**  Signal information
*/

enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

const gchar *gxsnmp_slist_string_key = "gxsnmp-slist-string-value";

/*******************************************************************************
**
**  Local forward references
**
*******************************************************************************/

static void    mib_panel_class_init	(GXsnmp_mib_panelClass  *klass);

static void    mib_panel_init		(GXsnmp_mib_panel       *panel);

static void    add_smib_button_cb       (GtkWidget              *widget,
                                         gpointer                data);

static void    del_smib_button_cb       (GtkWidget              *widget,
                                         gpointer                data);

static void
gxsnmp_slist_item_destroy (GtkObject * object)
{
  gchar *key;

  key = gtk_object_get_data (object, gxsnmp_slist_string_key);
  if (key)
    g_free (key);
}

/*******************************************************************************
**
**  gxsnmp_mib_panel_get_type ()
**
*******************************************************************************/

GtkType
gxsnmp_mib_panel_get_type()
{
  static GtkType mib_type = 0;

  if (!mib_type)
    {
      GtkTypeInfo mib_info =
      {
	"GXsnmp_mib_panel",
	sizeof (GXsnmp_mib_panel),
	sizeof (GXsnmp_mib_panelClass),
	(GtkClassInitFunc) mib_panel_class_init,
	(GtkObjectInitFunc) mib_panel_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      mib_type = gtk_type_unique (gtk_vbox_get_type (), &mib_info);
    }
  return mib_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
mib_panel_class_init (GXsnmp_mib_panelClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  signals[CHANGED_SIGNAL] =
    gtk_signal_new("changed",
                   GTK_RUN_FIRST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (GXsnmp_mib_panelClass, changed),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  class->changed = NULL;
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
mib_panel_init (GXsnmp_mib_panel *panel)
{
  GtkWidget *table;
  GtkWidget *label;
  GtkWidget *button;
  GtkWidget *vbox;
  GtkWidget *item;
  GtkWidget *scrolled;
  GList     *list;
  DIR       *dir;
  struct dirent *dirp;
  gchar     *val;
  char      *path;
  char      *tpath;

  D_FUNC_START;
  g_return_if_fail (panel != NULL);
/*
**  The controls are organized in a 2 by 2 table
*/


  table = gtk_table_new (3, 2, FALSE);
  
  gtk_container_add (GTK_CONTAINER(panel), table);

/*
**  Start by constructing the top label widgets
*/

  label = gtk_label_new (gettext(mib_label[0]));
  gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);

  label = gtk_label_new (gettext(mib_label[1]));
  gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE(table), label, 2, 3, 0, 1, GTK_FILL, 0, 0, 0);

/*
** Now 2 of these list objects and 2 buttons in the middle
*/

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_ALWAYS);

  gtk_widget_set_usize(scrolled, 200, 100);
  gtk_table_attach (GTK_TABLE(table), scrolled, 0, 1, 1, 2, GTK_FILL, 0, 4, 4);

  panel->alist = (GtkList *) gtk_list_new ();
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled), 
                                         GTK_WIDGET(panel->alist));
  gtk_list_set_selection_mode(panel->alist, GTK_SELECTION_MULTIPLE);

  vbox = gtk_vbox_new (FALSE, 8);
  gtk_table_attach (GTK_TABLE(table), vbox, 1, 2, 1, 2, GTK_FILL, 0, 0, 0);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_ALWAYS);
  gtk_table_attach (GTK_TABLE(table), scrolled, 2, 3, 1, 2, GTK_FILL, 0, 4, 4);
  gtk_widget_set_usize(scrolled, 200, 100);

  panel->slist = (GtkList *) gtk_list_new ();
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled), 
                                         GTK_WIDGET(panel->slist));
  gtk_list_set_selection_mode(panel->slist, GTK_SELECTION_MULTIPLE);

  button = gtk_button_new_with_label(">>");
  gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (add_smib_button_cb), panel);

  button = gtk_button_new_with_label("<<");
  gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (del_smib_button_cb), panel);

/*
** Fill alist with available MIB files.
*/

  list = NULL;
  path = smiGetPath();
  tpath = strtok(path,":");
  while (tpath)
    {
      d_print (DEBUG_DUMP, "About to read %s\n", tpath);
      dir = opendir(tpath);
      if (!dir)
	{
	  /* FIXME: What to do if the mib dir isn't there? */
	  d_print (DEBUG_DUMP, "Failed!\n");
	  gtk_widget_show_all (table);
	  D_FUNC_END;
	  return;
	}
      while ((dirp = readdir(dir)))
	{
	  if (dirp->d_name[0] != '.')
	    {
	      item = gtk_list_item_new_with_label(dirp->d_name);
	      val = g_strdup(dirp->d_name);
	      gtk_object_set_data(GTK_OBJECT(item), gxsnmp_slist_string_key, 
			  val);
	      gtk_signal_connect (GTK_OBJECT (item), "destroy",
			  (GtkSignalFunc) gxsnmp_slist_item_destroy, val);
	      list = g_list_append(list, item);
	    }
	}
      closedir(dir);
      tpath = strtok(NULL,":");
    }
  gtk_list_append_items(panel->alist, list);

  gtk_widget_show_all (table); 
  D_FUNC_END;
}

static void
add_smib_button_cb(GtkWidget *widget, gpointer data)
{
  GXsnmp_mib_panel *panel;               /* Pointer to MIB panel widget */
  GList            *selected;
  GList            *list;
  GtkWidget        *item;
  gchar            *name;
  gchar            *val;

  D_FUNC_START;
  g_return_if_fail (data != NULL);
  panel = GXSNMP_MIB_PANEL(data);        /* Argument points to panel widget */

  g_return_if_fail (panel->alist->selection != NULL);

  selected = panel->alist->selection;
  list = NULL;
  while (selected)
    {
      name = gtk_object_get_data(GTK_OBJECT(selected->data), 
                                 gxsnmp_slist_string_key);
      val = g_strdup(name);
      item = gtk_list_item_new_with_label(val);
      gtk_object_set_data(GTK_OBJECT(item), gxsnmp_slist_string_key, val);
      gtk_signal_connect (GTK_OBJECT (item), "destroy",
			 (GtkSignalFunc) gxsnmp_slist_item_destroy, val);
      list = g_list_append(list, item);
      selected = selected->next;
    }
  gtk_list_append_items(panel->slist, list);
  gtk_widget_show_all (panel->slist); 

  gtk_signal_emit (GTK_OBJECT(panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}

static void
del_smib_button_cb(GtkWidget *widget, gpointer data)
{
  GXsnmp_mib_panel *panel;               /* Pointer to MIB panel widget */
  GList            *selected;
  GList            *list;

  D_FUNC_START;
  g_return_if_fail (data != NULL);
  panel = GXSNMP_MIB_PANEL(data);        /* Argument points to panel widget */

  g_return_if_fail (panel->slist->selection != NULL);

  selected = panel->slist->selection;
  list = NULL;
  while (selected)
    {
      list = g_list_append(list, selected->data);
      selected = selected->next;
    }
  gtk_list_remove_items(panel->slist, list);
  g_list_free(list);

  gtk_signal_emit (GTK_OBJECT(panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}

/****************************************************************************
**
**  Public function to create a new widget
**
****************************************************************************/

GtkWidget *
gxsnmp_mib_panel_new ()
{
  return gtk_type_new (gxsnmp_mib_panel_get_type());
}

/****************************************************************************
**
**  Public function to load settings into the panel widgets
**
****************************************************************************/

void
gxsnmp_mib_panel_put_data (GXsnmp_mib_panel         *panel,
                           GXsnmp_mib_configuration *c)
{
  gchar     *val;
  GList     *list;
  GSList    *mibs;
  GtkWidget *item;

  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_MIB_PANEL(panel));
  g_return_if_fail (c != NULL);

  mibs = c->mibs;

  list = NULL;  
  while (mibs)
    {
      val = g_strdup((char *)(mibs->data));
      item = gtk_list_item_new_with_label(val);
      gtk_object_set_data(GTK_OBJECT(item), gxsnmp_slist_string_key, val);
      gtk_signal_connect (GTK_OBJECT (item), "destroy",
		        (GtkSignalFunc) gxsnmp_slist_item_destroy, val);
      list = g_list_append(list, item);
      mibs = mibs->next;
    }
  gtk_list_append_items(panel->slist, list);
  D_FUNC_END;
}


/****************************************************************************
**
**  Public function to copy configuration data from the panel widgets into an
**  MIB configuration control block
**
****************************************************************************/

void
gxsnmp_mib_panel_get_data (GXsnmp_mib_panel         *panel,
                           GXsnmp_mib_configuration *c)
{
  GList            *selected;
  GSList           *list;
  gchar            *name;
  gchar            *val;
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_MIB_PANEL (panel));

/*
  list = c->mibs;
  while (list)
    {
      g_free(list->data);
      list = list->next;
    }
  g_slist_free(c->mibs);
*/
  c->mibs = NULL;
      
  selected = panel->slist->children;
  list = NULL;
  while (selected)
    {
      name = gtk_object_get_data(GTK_OBJECT(selected->data), 
                                 gxsnmp_slist_string_key);
      val = g_strdup(name);
      list = g_slist_append(list, val);
      selected = selected->next;
    }
  c->mibs = list;
  D_FUNC_END;
}

/* EOF */

