/*
**  $Id$
**
**  GXSNMP -- An snmp management application.
**  Copyright (C) 1998 Gregory Mclean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  sql_panel.c -- SQL database configuration panel code
**
**  sql_panel implements a widget that is used to enter SQL
**  database configuration information.
* This is really specific to MySQL and should be moved there.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "sql_panel.h"
#include "debug.h"

/*
**  Table of descriptive labels to display as the first column of the table
*/

#define PANEL_ROWS 5

static char *sql_label[] = {
  N_("Server"),
  N_("Port"),
  N_("User"),
  N_("Password"),
  N_("Database")
};

static char *sql_tooltips[] = {
  N_("The hostname or ip address of the database server."),
  N_("The port that should be used when connecting to a remote server, "
     "this is not used for the localhost server."),
  N_("The user name to use when accessing the database."),
  N_("The password to use when accessing the database, if left blank "
     "you will be prompted for it."),
  N_("The database that holds all the tables.")
};

/*
**  Signal information
*/

enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

/*******************************************************************************
**
**  Local forward references
**
*******************************************************************************/

static void    sql_panel_class_init	(GXsnmp_sql_panelClass  *klass);

static void    sql_panel_init		(GXsnmp_sql_panel       *panel);

static void    changed_cb     		(GtkWidget		*widget,
					 gpointer		 data);

/*******************************************************************************
**
**  gxsnmp_sql_panel_get_type ()
**
*******************************************************************************/

GtkType
gxsnmp_sql_panel_get_type()
{
  static GtkType sql_type = 0;

  if (!sql_type)
    {
      GtkTypeInfo sql_info =
      {
	"GXsnmp_sql_panel",
	sizeof (GXsnmp_sql_panel),
	sizeof (GXsnmp_sql_panelClass),
	(GtkClassInitFunc) sql_panel_class_init,
	(GtkObjectInitFunc) sql_panel_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      sql_type = gtk_type_unique (gtk_vbox_get_type (), &sql_info);
    }
  return sql_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
sql_panel_class_init (GXsnmp_sql_panelClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  signals[CHANGED_SIGNAL] =
    gtk_signal_new("changed",
                   GTK_RUN_FIRST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (GXsnmp_sql_panelClass, changed),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  class->changed = NULL;
}

/*******************************************************************************
**
**  The widget initialization subroutine
**
*******************************************************************************/

static void
sql_panel_init (GXsnmp_sql_panel *panel)
{
  GtkWidget *table;
  GtkWidget *label;
  GtkObject *adjustment;
  int	     i;
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
/*
**  The controls are organized in a PANEL_ROWS by 2 table
*/

  table = gtk_table_new (PANEL_ROWS, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE(table), 4);
  gtk_table_set_col_spacings (GTK_TABLE(table), 4);
  gtk_container_border_width (GTK_CONTAINER(table), 5);
  gtk_container_add (GTK_CONTAINER(panel), table);

/*
**  Start by constructing all of the label widgets
*/

  for (i = 0; i < PANEL_ROWS; i++)
    {
      label = gtk_label_new (gettext(sql_label[i]));
      gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
      gtk_table_attach (GTK_TABLE(table), label, 0, 1, i, i + 1,
		        GTK_FILL, 0, 0, 0);
    }

/*
**  The 1st control is "Server".
*/

  panel->host = gtk_entry_new();
  gtk_signal_connect (GTK_OBJECT(panel->host), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE(table), panel->host, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);   

/*
**  The 2nd control is "Port".
*/

  adjustment = gtk_adjustment_new (1, 0, 65535, 1, 1, 1);
  panel->port = gtk_spin_button_new (GTK_ADJUSTMENT(adjustment), 1, 0);
  gtk_signal_connect (GTK_OBJECT(panel->port), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON(panel->port), TRUE);
  gtk_table_attach (GTK_TABLE(table), panel->port, 1, 2, 1, 2,
                    GTK_FILL, 0, 0, 0);

/*
**  The 3rd control is "User".
*/

  panel->user = gtk_entry_new();
  gtk_signal_connect (GTK_OBJECT(panel->user), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE(table), panel->user, 1, 2, 2, 3, 
		    GTK_FILL, 0, 0, 0);

/*
**  The 4th control is "Password".
*/

  panel->password = gtk_entry_new();
  gtk_entry_set_visibility(GTK_ENTRY(panel->password), FALSE);
  gtk_signal_connect (GTK_OBJECT(panel->password), "changed",
                      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE(table), panel->password, 1, 2, 3, 4, 
		    GTK_FILL, 0, 0, 0);

  /*
   * The 5th control is "Database.
   */
  panel->db_table = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->db_table), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->db_table, 1, 2, 4, 5,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show_all (table);
  D_FUNC_END;
}

/*******************************************************************************
**
**  Callback function for when any of the child control widgets is changed.
**  Emit a "changed" signal.
**
*******************************************************************************/

static void
changed_cb(GtkWidget *widget, gpointer data)
{
  GXsnmp_sql_panel *panel;              /* Pointer to SQL panel widget */
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  g_return_if_fail (GXSNMP_IS_SQL_PANEL (data));
  panel = GXSNMP_SQL_PANEL(data);      /* Argument points to panel widget */

  gtk_signal_emit (GTK_OBJECT(panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}

/****************************************************************************
 *
 *  Public function to create a new widget
 *
 ***************************************************************************/

GtkWidget *
gxsnmp_sql_panel_new (GList * sql_engines)
{
  return gtk_type_new (gxsnmp_sql_panel_get_type());
}

/****************************************************************************
 *
 *  Public function to load settings into the panel widgets
 *
 ****************************************************************************/
void
gxsnmp_sql_panel_put_data (GXsnmp_sql_panel         *panel,
                           GXsnmp_sql_configuration *c,
			   GList		    *engines)
{
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_SQL_PANEL(panel));
  g_return_if_fail (c != NULL);

  gtk_entry_set_text(GTK_ENTRY(panel->host),              c->G_sql.host ?
		     c->G_sql.host : "localhost");
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(panel->port), c->G_sql.port);
  gtk_entry_set_text(GTK_ENTRY(panel->user), 		  c->G_sql.user);
  gtk_entry_set_text(GTK_ENTRY(panel->password), 	  c->G_sql.password ?
		     c->G_sql.password : "");
  gtk_entry_set_text(GTK_ENTRY(panel->db_table),          c->database ?
		     c->database : "gxsnmp");
  D_FUNC_END;
}


/*******************************************************************************
**
**  Public function to copy configuration data from the panel widgets into an
**  SQL configuration control block
**
*******************************************************************************/

void
gxsnmp_sql_panel_get_data (GXsnmp_sql_panel         *panel,
                           GXsnmp_sql_configuration *c)
{
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_SQL_PANEL (panel));
  g_return_if_fail (c != NULL);

  /*  if (c->G_sql.host)
      g_free (c->G_sql.host); */

  c->G_sql.host     = strdup (gtk_entry_get_text (GTK_ENTRY (panel->host)));
  DPRT ("gxsnmp_sql_panel_get_data, got host.");
  c->G_sql.port     = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (panel->port));
  DPRT ("gxsnmp_sql_panel_get_data, got port.");
  /*  if (c->G_sql.user)
      g_free (c->G_sql.user);   -- wtf? This causes a crash?*/
  c->G_sql.user     = strdup (gtk_entry_get_text (GTK_ENTRY (panel->user)));
  DPRT ("gxsnmp_sql_panel_get_data, got user.");
  /*if (c->G_sql.password)
    g_free (c->G_sql.password); */
  c->G_sql.password = strdup (gtk_entry_get_text (GTK_ENTRY (panel->password)));
  DPRT ("gxsnmp_sql_panel_get_data, got password.");
  /*if (c->database)
    g_free (c->database); */
  c->database       = g_strdup (gtk_entry_get_text (GTK_ENTRY (panel->db_table)));
  DPRT ("gxsnmp_sql_panel_get_data, got database.");
  D_FUNC_END;
}

/* EOF */
