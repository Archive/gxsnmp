/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The network_panel widget is used to enter in and edit network data.
 */
#include <config.h>
#include <gnome.h>
#include "network_panel.h"
#include "panel_utility.h"
#include "debug.h"

#define PANEL_ROWS 8

static char *network_label[] = {
  N_("Name"),
  N_("Address"),
  N_("Mask"),
  N_("Speed"),
  N_("Description"),
  N_("Contact"),
  N_("Created"),
  N_("Modified")
};

static char *network_tooltip[] = {
  N_("The name of the network to display under the map icon."),
  N_("The network address of this network."),
  N_("The network mask, if applicable, for this network."),
  N_("The speed, in KBytes per second, of this network."),
  N_("Your discription of this network."),
  N_("Your contact information for this network."),
  N_("Date and time this network was added to the database."),
  N_("Date and time this network was modified in the database.")
};

enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };
/****************************************************************************
 * Forward declarations
 ***************************************************************************/
static void     network_panel_class_init    (GXsnmp_network_panelClass *klass);
static void     network_panel_init          (GXsnmp_network_panel      *panel);
static void     changed_cb                  (GtkWidget                 *widget,
					     gpointer                  data);
/****************************************************************************
 * Widget Methods 
 ***************************************************************************/
GtkType
gxsnmp_network_panel_get_type ()
{
  static GtkType np_type = 0;
  if (!np_type)
    {
      GtkTypeInfo np_info = 
      {
	"GXsnmp_network_panel",
	sizeof (GXsnmp_network_panel),
	sizeof (GXsnmp_network_panelClass),
	(GtkClassInitFunc) network_panel_class_init,
	(GtkObjectInitFunc) network_panel_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      np_type = gtk_type_unique (gtk_vbox_get_type (), &np_info);
    }
  return np_type;
}
/****************************************************************************
 * Class init function
 ***************************************************************************/
static void
network_panel_class_init (GXsnmp_network_panelClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass *) klass;
  signals[CHANGED_SIGNAL] = 
    gtk_signal_new("changed",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET (GXsnmp_network_panelClass, changed),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  klass->changed = NULL;
}
/****************************************************************************
 * Object init
 ***************************************************************************/
static void
network_panel_init (GXsnmp_network_panel *panel)
{
  GtkWidget 	*table;
  GtkTooltips	*tooltips;
  GtkWidget	*label;
  GtkWidget 	*event_box;
  gint	     	 i;
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
/*
**  The controls are organized in a PANEL_ROWS by 2 table
*/

  table = gtk_table_new (PANEL_ROWS, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table), 4);
  gtk_container_border_width (GTK_CONTAINER (table), 0);
  gtk_container_add (GTK_CONTAINER (panel), table);

/*
**  Start by constructing all of the label widgets and adding the tooltips.
*/

  tooltips = gtk_tooltips_new ();
  for (i = 0; i < PANEL_ROWS; i++)
    {
      label = gtk_label_new (gettext(network_label[i]));
      gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
      event_box = gtk_event_box_new ();
      gtk_container_add (GTK_CONTAINER (event_box), label);
      gtk_tooltips_set_tip (tooltips, event_box, 
			    gettext (network_tooltip[i]), NULL);
      gtk_table_attach (GTK_TABLE(table), event_box, 0, 1, i, i + 1,
                        GTK_FILL, 0, 0, 0);
    }
  /* The entry widgets */
  panel->name = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->name), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->name, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);
  
  panel->address = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->address), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->address, 1, 2, 1, 2, 
		    GTK_FILL, 0, 0, 0);
  
  panel->mask = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->mask), "changed", 
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->mask, 1, 2, 2, 3,
		    GTK_FILL, 0, 0, 0);
  
  panel->speed = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->speed), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->speed, 1, 2, 3, 4,
		    GTK_FILL, 0, 0, 0);

  panel->description = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->description), "changed", 
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->description, 1, 2, 4, 5,
		    GTK_FILL, 0, 0, 0);
  
  panel->contact = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->contact), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->contact, 1, 2, 5, 6,
		    GTK_FILL, 0, 0, 0);
  
  panel->created = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->created), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->created, 1, 2, 6, 7,
		    GTK_FILL, 0, 0, 0);
  
  panel->modified = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->modified), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->modified, 1, 2, 7, 8,
		    GTK_FILL, 0, 0, 0);

  gtk_widget_show_all (table);
  D_FUNC_END;
}

static void
changed_cb (GtkWidget *widget, gpointer data)
{
  GXsnmp_network_panel *panel;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  panel = GXSNMP_NETWORK_PANEL (data);
  gtk_signal_emit (GTK_OBJECT (panel), signals[CHANGED_SIGNAL]);
  D_FUNC_END;
}

/****************************************************************************
 * Public functions
 ***************************************************************************/
GtkWidget *
gxsnmp_network_panel_new ()
{
  return GTK_WIDGET (gtk_type_new (gxsnmp_network_panel_get_type()));
}

void
gxsnmp_network_panel_put_data (GXsnmp_network_panel *panel, DB_network *dbn)
{
  D_FUNC_START;
  g_return_if_fail (panel != NULL);
  g_return_if_fail (GXSNMP_IS_NETWORK_PANEL (panel));
  g_return_if_fail (dbn != NULL);

  panel->rowid = dbn->rowid;
  DPRTI ("gxsnmp_network_panel_put_data: rowid is", dbn->rowid);
  gtk_entry_set_text (GTK_ENTRY (panel->name),
		      dbn->name                ? dbn->name : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->address),
		      dbn->address             ? dbn->address : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->mask),
		      dbn->netmask             ? dbn->netmask : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->description),
		      dbn->description         ? dbn->description : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->contact), 
		      dbn->contact             ? dbn->contact : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->created),
		      dbn->created             ? dbn->created : "" );
  gtk_entry_set_text (GTK_ENTRY (panel->modified), 
		      dbn->modified            ? dbn->modified : "");
  D_FUNC_END;
}

gboolean
gxsnmp_network_panel_get_data (GXsnmp_network_panel *panel, DB_network *dbn)
{
  gint   changed = FALSE;
  D_FUNC_START;
  g_return_val_if_fail (panel != NULL, FALSE);
  g_return_val_if_fail (GXSNMP_IS_NETWORK_PANEL (panel), FALSE);
  g_return_val_if_fail (dbn != NULL, FALSE);
  if (dbn->rowid != panel->rowid)
    {
      DPRT ("gxsnmp_network_panel_get_data: dbn->rowid != panel->rowid");
      g_warning ("dbn->rowid != panel->rowid ??? Something is broke.");
      D_FUNC_END;
      return FALSE;
    }
  changed |= update_string_field (&dbn->name,
	       gtk_entry_get_text (GTK_ENTRY (panel->name)));
  changed |= update_string_field (&dbn->address,
	       gtk_entry_get_text (GTK_ENTRY (panel->address)));
  changed |= update_string_field (&dbn->netmask,
	       gtk_entry_get_text (GTK_ENTRY (panel->mask)));
  changed |= update_string_field (&dbn->contact,
	       gtk_entry_get_text (GTK_ENTRY (panel->contact)));
  changed |= update_string_field (&dbn->description,
               gtk_entry_get_text (GTK_ENTRY (panel->description)));
  D_FUNC_END;
  return (changed != FALSE);
}

/* EOF */


