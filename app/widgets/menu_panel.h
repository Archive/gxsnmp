/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  This file defines the MENU configuration panel widget
*/

#ifndef __GXSNMP_MENU_PANEL_H__
#define __GXSNMP_MENU_PANEL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_MENU_PANEL (gxsnmp_menu_panel_get_type())

#define GXSNMP_MENU_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MENU_PANEL, GXsnmp_menu_panel)

#define GXSNMP_MENU_PANEL_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_MENU_PANEL, \
			      GXsnmp_menu_panelClass)

#define GXSNMP_IS_MENU_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MENU_PANEL)

#define GXSNMP_IS_MENU_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MENU_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_menu_panel	GXsnmp_menu_panel;
typedef struct _GXsnmp_menu_panelClass  GXsnmp_menu_panelClass;

struct _GXsnmp_menu_panel
{
  GtkVBox	  vbox;			/* The VBox base structure */
  GtkWidget	* host;			/* The "MENU Host" entry field */
  GtkWidget	* network;		/* The "MENU Network" entry field */
  GtkWidget     * hlist;		/* The Host CLIST */
  GtkWidget     * nlist;		/* The Network CLIST */
};

struct _GXsnmp_menu_panelClass
{
  GtkFrameClass parent_class;		/* The frame class base structure */

  void (* changed) (GXsnmp_menu_panel *panel);  /* The "changed" signal */
};

/*******************************************************************************
**
**  Data structure used to load and unload the panel
**
*******************************************************************************/

typedef struct _GXsnmp_menu_configuration GXsnmp_menu_configuration;

struct _GXsnmp_menu_configuration
{
	GSList * hostsmd;
	GSList * netmenu;
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkType	    gxsnmp_menu_panel_get_type	(void);

GtkWidget * gxsnmp_menu_panel_new	();

void        gxsnmp_menu_panel_get_data   (GXsnmp_menu_panel 	  *panel,
					 GXsnmp_menu_configuration *c);

void	    gxsnmp_menu_panel_put_data	(GXsnmp_menu_panel 	  * panel,
					 GXsnmp_menu_configuration * c);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_MENU_PANEL_H__ */
