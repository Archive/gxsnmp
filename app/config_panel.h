/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Configuration panel for gxsnmp
*/

#ifndef __CONFIG_PANEL_H__
#define __CONFIG_PANEL_H__

#include "snmp_panel.h"
#include "sql_panel.h"
#include "menu_panel.h"
#include "mib_panel.h"

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_CONFIG_PANEL (gxsnmp_config_panel_get_type())

#define GXSNMP_CONFIG_PANEL(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_CONFIG_PANEL, \
			GXsnmp_config_panel)

#define GXSNMP_CONFIG_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_CONFIG_PANEL, \
			      GXsnmp_config_panelClass)

#define GXSNMP_IS_CONFIG_PANEL(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_CONFIG_PANEL)

#define GXSNMP_IS_CONFIG_PANEL_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_CONFIG_PANEL)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_config_panel   		GXsnmp_config_panel;
typedef struct _GXsnmp_config_panelClass	GXsnmp_config_panelClass;

struct _GXsnmp_config_panel
{
  GnomePropertyBox propertybox;		/* Property box base structure */

  GtkWidget	*general;		/* The "General Config" panel */
  GtkWidget	*snmp;			/* The "SNMP Config" panel */
  GtkWidget	*sql;			/* The "SQL Config" panel */
  GtkWidget	*menu;			/* The "MENU Config" panel */
  GtkWidget	*collector;		/* The "Collector config" panel */
  GtkWidget	*mib;			/* The "MIB config" panel */

  GtkWidget     *sql_engine;            /* sql engine combo box */

};

struct _GXsnmp_config_panelClass
{
  GnomePropertyBoxClass parent_class;	/* Property box base class structure */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkWidget * gxsnmp_config_panel_new	();

/*******************************************************************************
**
**  End of widget definition.  Widget data follows.
**  
*******************************************************************************/

#define CONFIG_VERSION   1.0
#define CONFIG_HEADER  "/* Configuration file automatically generated. DO NOT EDIT */"

/*******************************************************************************
**
**  The following is the GXsnmp configuration structure.  In order not to
**  break the rest of the code, I've added a set of #defines with the old
**  names.
**
*******************************************************************************/

typedef struct _gxsnmp_config GXsnmp_configuration;

typedef struct _gxsnmp_config 
{
/*  GXsnmp_snmp_configuration	 snmp;	Removed -- JMS           */
  GXsnmp_sql_configuration 	 sql;		/* See sql_panel.h	  */
  GXsnmp_menu_configuration 	 menu;		/* See menu_panel.h	  */
  GXsnmp_mib_configuration 	 mib;		/* See mib_panel.h	  */
} gxsnmp_config;

/*#define def_snmp_enabled	snmp.enabled
#define def_snmp_domain		snmp.domain
#define def_snmp_com		snmp.read_community
#define def_snmp_wcom		snmp.write_community
#define def_snmp_vers		snmp.version
#define def_snmp_port		snmp.port
#define def_snmp_time		snmp.timeout
#define def_snmp_retr		snmp.retries

#define def_sql_engine		sql.engine
#define def_sql_host		sql.host
#define def_sql_user		sql.user
#define def_sql_pass		sql.password
#define def_sql_db		sql.database
#define def_sql_port		sql.port
*/
/*
**  Global standard panel functions
*/

void                 open_config_panel             (void);
void                 hide_config_panel             (void);
void                 destroy_config_panel          (void);
void                 reset_config_panel            (void);

#endif
/* EOF */
