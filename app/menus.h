/*
**  $Id$
**  GXSNMP -- An snmp mangament application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Main menu code
*/

#ifndef __MAIN_MENU_H__
#define __MAIN_MENU_H__

#include <gnome.h>

/*
 * User defined menus
 */
typedef struct _user_menu
{
  gchar     *item;            /* The menu item */
  gchar     *command;         /* The command to run */
} user_menu;
/*
 * Plugin menus
 */

typedef struct _menu_entry 
{
  gchar    * path;
  gchar    * location;
  gboolean   enabled;
  gpointer   callback;
  gpointer   callback_data;
} 
menu_entry;

GtkWidget * host_popup_menu;
GtkWidget * map_nb_popup_menu;

/*
 * Functions
 */
gboolean            add_menu_entry                     (menu_entry  *entry,
							gboolean    enabled);
void                set_menu_entry_state               (menu_entry  *entry,
							gboolean    state);
void                remove_menu_entry                  (menu_entry  *entry);

/******************************************************************************
**
**  Public menu functions
**
******************************************************************************/

void 	 menu_create	      	  (GtkWidget      * main_window);

gint 	 menu_raise_map_popup     (GdkEventButton * event, gpointer data);
gint 	 menu_raise_host_popup    (GdkEventButton * event, gpointer data);
gint 	 menu_raise_network_popup (GdkEventButton * event, gpointer data);
void	 menu_set_snap_state      (gboolean         state);

gboolean gxsnmp_dialog_check 	  (gchar          * dialog_type,
                     		   gpointer         dialog_data);

void 	 gxsnmp_dialog_add 	  (GtkWidget     * dialog,
                   	  	   gchar         * dialog_type,
                   		   gpointer        dialog_data,
                  		   gchar         * dialog_title);
/****************************************************************************
 * Kludgy function to do dynamic popup menus... 
 * This needs to be re-thunk...
 ***************************************************************************/
void     gxsnmp_add_to_host_popup (GnomeUIInfo         *menu);
void     gxsnmp_add_to_net_popup  (GnomeUIInfo         *menu);

#endif

/* EOF */
