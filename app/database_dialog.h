/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Configuration panel for gxsnmp
*/

#ifndef __DATABASE_DIALOG_H__
#define __DATABASE_DIALOG_H__

#include "sql_panel.h"

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_DATABASE_DIALOG (gxsnmp_database_dialog_get_type())

#define GXSNMP_DATABASE_DIALOG(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_DATABASE_DIALOG, \
			GXsnmp_database_dialog)

#define GXSNMP_DATABASE_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_DATABASE_DIALOG, \
			      GXsnmp_database_dialogClass)

#define GXSNMP_IS_DATABASE_DIALOG(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_DATABASE_DIALOG)

#define GXSNMP_IS_DATABASE_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_DATABASE_DIALOG)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_database_dialog		GXsnmp_database_dialog;
typedef struct _GXsnmp_database_dialogClass	GXsnmp_database_dialogClass;

struct _GXsnmp_database_dialog
{
  GnomeDialog     dialog;		/* GnomeDialog base structure */
  GtkWidget	* main_table;		/* The top level box */
  GtkWidget	* config_list;		/* List of defined configurations */
  GtkWidget     * engine_vbox;		/* Vertical box for engine info */
  GtkWidget     * engine_combo;		/* Database engine combo */
  GtkWidget	* engine_specific;	/* Database engine-specific widget */
  GtkWidget     * database_list;	/* List of databases on connection */
};

struct _GXsnmp_database_dialogClass
{
  GnomeDialogClass parent_class;	/* GnomeDialog base class structure */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkWidget * gxsnmp_database_dialog_new	();

void open_database_dialog ();

#endif

/* EOF */
