/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

#ifndef __FILE_IO_H__
#define __FILE_IO_H__

#include <gtk/gtk.h>

#define DEF_CONFIG_PATH ".gxsnmp"
#define DEF_CONFIG_NAME "gxsnmp.config"

int       file_open                        (char *filename);
int       file_close                       (char *filename);
gboolean  save_config                      (void);
gboolean  load_config                      (void);
void      open_file_dialog                 (void);
void      close_file_dialog                (void);
char      *get_filename                    (char *path, char *filename);
#endif

/* EOF */






