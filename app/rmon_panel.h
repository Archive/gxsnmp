/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Jochen Friedrich & Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Network list panel definitions
 */
#ifndef __RMON_PANEL_H__
#define __RMON_PANEL_H__
#include <gnome.h>
#include "host_panel.h"
#include "tables.h"

#include "snmp_host.h"
BEGIN_GNOME_DECLS

/****************************************************************************
 * Standard widget macros 
 ***************************************************************************/
#define GXSNMP_TYPE_RMON_PANEL (gxsnmp_rmon_panel_get_type ())
#define GXSNMP_RMON_PANEL(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_RMON_PANEL, GXsnmp_rmon_panel)
#define GXSNMP_RMON_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_RMON_PANEL, \
			      GXsnmp_rmon_panelClass)
#define GXSNMP_IS_RMON_PANEL (obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_RMON_PANEL)
#define GXSNMP_IS_RMON_PANEL_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_RMON_PANEL)
/****************************************************************************
 * Control blocks for the widget class and for widget instances 
 ***************************************************************************/
typedef struct _GXsnmp_rmon_panel           GXsnmp_rmon_panel;
typedef struct _GXsnmp_rmon_panelClass      GXsnmp_rmon_panelClass;

struct _GXsnmp_rmon_panel
{
  GnomeDialog dialog;

  GtkObject *clist;
};

struct _GXsnmp_rmon_panelClass
{
  GnomeDialogClass    parent_class;
};
/****************************************************************************
 * Public Widget functions
 ***************************************************************************/
GtkType        gxsnmp_rmon_panel_get_type               (void);
GtkWidget      *gxsnmp_rmon_panel_new                   (gpointer);

#endif 

/* EOF */
