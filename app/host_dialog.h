/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (c) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  The host_dialog widget is used to enter in and edit host data.
*/

#ifndef __GXSNMP_HOST_DIALOG_H__
#define __GXSNMP_HOST_DIALOG_H__

#include <gnome.h>
#include "gnome-property-dialog.h"
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "host_panel.h"
#include "interface_panel.h"
#include "snmp_panel.h"
#include "g_snmp.h"

BEGIN_GNOME_DECLS

/*****************************************************************************
**
**  Standard widget macros
**
*****************************************************************************/

#define GXSNMP_TYPE_HOST_DIALOG (gxsnmp_host_dialog_get_type())

#define GXSNMP_HOST_DIALOG(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_HOST_DIALOG, GXsnmp_host_dialog)

#define GXSNMP_HOST_DIALOG_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_HOST_DIALOG, \
			      GXsnmp_host_dialogClass)

#define GXSNMP_IS_HOST_DIALOG(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_HOST_DIALOG)

#define GXSNMP_IS_HOST_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_HOST_DIALOG)

/*****************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*****************************************************************************/

typedef struct _GXsnmp_host_dialog	GXsnmp_host_dialog;
typedef struct _GXsnmp_host_dialogClass GXsnmp_host_dialogClass;

struct _GXsnmp_host_dialog
{
  GnomePropertyDialog  dialog;		/* Base structure */

  GtkWidget     * table;		/* The top level box */
  GtkWidget	* host_panel;		/* The host editing panel */
  GtkWidget	* host_scroll;		/* Scroll window for host list */
  GtkWidget	* host_discover_button;	/* The "Add Host" button */
  GtkWidget	* host_discover_list;	/* The "Add Host" SNMP combo list */
  GtkWidget	* host_discover_combo;	/* The "Add Host" transport combo */
  GtkWidget	* interface_vbox;	/* Contains panel & snmp combo */
  GtkWidget	* interface_panel;	/* The interface editing panel */
  GtkWidget	* no_interface_panel;	/* Displayed when no interfaces */
  GtkWidget	* interface_scroll;	/* Scroll window for interface list */
  GtkWidget	* snmp_combo_list;	/* The SNMP selection combo list */
  GtkWidget	* interface_clist;    	/* List of available interfaces */
  GtkWidget	* interface_add_button;	   /* "Add Interface" button */
  GtkWidget	* interface_delete_button; /* "Delete Interface" button */
  GtkWidget	* graph_panel;		   /* The graph editing panel */
  GtkWidget	* no_graph_panel;	/* Displayed when host not on map */
  GtkWidget	* graph_scroll;		/* Scroll window for graph list */
  GtkWidget	* graph_clist;		/* List of available maps */
  GtkWidget	* graph_show_button;	/* Show on selected map */
  GtkWidget	* graph_hide_button;	/* Hide on selected map */

  GList         * transport_names;      /* List of possible transport names */
  GList         * transport_values;     /* List of possible transport values */

  DB_host	* selected_host;	/* Current host */
  DB_interface	* selected_interface;   /* Current interface */
  DB_graph	* selected_graph;       /* Current graph entry */

  guint		  interfaces;		/* Number of interfaces owned by host */
  guint		  interface_row;	/* Current row in interface clist */

  guint		* graph_datalets;	/* Array of bits of row_data */
  guint		  graph_row;		/* Current row in graph clist */

  guint		  snmp_rowid;		/* Last selected SNMP rowid */
  guint		  discover_rowid;	/* Last selected SNMP rowid */

  gboolean	  host_dirty;		/* DB_host data has been changed */
  gboolean	  interface_dirty;	/* DB_interface data has been changed */
  gboolean	  graph_dirty;		/* DB_graph data has been changed */

  gdouble	  x;			/* Mouse location on map at */
  gdouble	  y;			/* time dialog was created */

  gpointer        request;              /* SNMP request handle */
  gpointer        stable;               /* SNMP request handle */
  GHashTable     *if_desc;              /* ifDesc cache */
  host_snmp      *host;
};

struct _GXsnmp_host_dialogClass
{
  GnomePropertyDialogClass  parent_class; 	/* Class base structure */
};

/*****************************************************************************
**
**  Public widget manipulation functions
**
*****************************************************************************/

GtkType     gxsnmp_host_dialog_get_type		(void);
GtkWidget * gxsnmp_host_dialog_new		(DB_host   * host,
						 gdouble     x,
						 gdouble     y);
END_GNOME_DECLS

#endif /* __GXSNMP_HOST_DIALOG_H__ */

/* EOF */
