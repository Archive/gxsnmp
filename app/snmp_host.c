/*
 * $Id$
 * GXSNMP -- An snmp management application.
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Host panel code.  
 */

#ifdef HAVE_CONFIG_H 
#include <config.h>
#endif

#include <gnome.h>

#include "main.h"
/* 
 * Global Variables refrenced in this module
 */

extern gxsnmp *app_info;
extern GdkPixmap *book_open;
extern GdkPixmap *book_closed;
extern GdkBitmap *book_open_mask;
extern GdkBitmap *book_closed_mask;

/*
 * SNMP Set panel (single mib. without digging around)
 */
/*
 * Local functions to the set panel
 */
static void        create_set_panel                (void);
static gint        set_panel_delete_cb             (GtkWidget   *widget,
						    GdkEvent    *e,
						    gpointer    data);

/*
 * Local variables
 */
static set_panel   *sset_panel = NULL;

/*
 * Local functions
 */

static void
create_set_panel ()
{
  GtkWidget    *w_box;
  GtkWidget    *w_frame;
  GtkWidget    *b_box;
  GtkWidget    *h_bar;

  sset_panel = (set_panel *)g_malloc (sizeof (set_panel));

  sset_panel->window = gnome_app_new ("GXSNMP", _("Set A Mib"));
  gtk_window_set_wmclass (GTK_WINDOW (sset_panel->window), "mib_set_panel",
				      "GXSNMP");
			  
  gtk_signal_connect (GTK_OBJECT (sset_panel->window), "delete_event",
		      GTK_SIGNAL_FUNC (set_panel_delete_cb),
		      sset_panel->window);
  w_box = gtk_vbox_new (FALSE, 2);
  w_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (w_box), w_frame, FALSE, TRUE, 2);
  
  /*
   * do the panel stuff here
   */

  h_bar = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (w_box), h_bar, FALSE, FALSE, 0);
  w_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (w_box), w_frame, FALSE, FALSE, 0);
  b_box = gtk_hbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (b_box), 4);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (b_box), GTK_BUTTONBOX_EDGE);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (b_box), 2);
  gtk_button_box_set_child_size (GTK_BUTTON_BOX (b_box), 85, 20);
  gtk_container_add (GTK_CONTAINER (w_frame), b_box);
  sset_panel->buttons[SET_QUERY_BUTTON] = gtk_button_new_with_label 
    (_("Query"));
  gtk_container_add (GTK_CONTAINER (b_box), 
		     sset_panel->buttons[SET_QUERY_BUTTON]);
  sset_panel->buttons[SET_CLOSE_BUTTON] = gnome_stock_button 
    (GNOME_STOCK_BUTTON_CLOSE);
  gtk_container_add (GTK_CONTAINER (b_box),
		     sset_panel->buttons[SET_CLOSE_BUTTON]);
  gnome_app_set_contents (GNOME_APP (sset_panel->window), w_box);
  gtk_widget_show_all (sset_panel->window);
}

static gint
set_panel_delete_cb (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  if (sset_panel)
    {
      gtk_widget_destroy (sset_panel->window);
      g_free (sset_panel);
      sset_panel = NULL;
    }
  return TRUE;
}

/*
 * Global panel functions (set mib panel)
 */
void
open_set_panel ()
{
  if (sset_panel == NULL)
    create_set_panel ();
  else
    if (!GTK_WIDGET_VISIBLE (sset_panel->window))
      gtk_widget_show (sset_panel->window);
  reset_set_panel ();
}

void
destroy_set_panel ()
{
  if (sset_panel != NULL)
    {
       set_panel_delete_cb (NULL, NULL, sset_panel);
    }
}

void
hide_set_panel ()
{
  if (sset_panel->window)
    {
      if (GTK_WIDGET_VISIBLE (sset_panel->window))
	gtk_widget_hide (sset_panel->window);
    }
}

void
reset_set_panel ()
{
}

/*
 * Get mib panel
 */

/*
 * Local functions (Forward declarations)
 */
static void       create_get_panel            (void);
static gint       delete_get_panel            (GtkWidget   *widget,
					       GdkEvent    *e,
					       gpointer    data);
static void       close_get_panel_cb          (GtkWidget   *widget,
					       gpointer    data);

/*
 * Local variables
 */
static set_panel *get_panel = NULL;

/* Global variable for this scope */

/*
 * Actual local functions
 */

static void
create_get_panel ()
{
  GtkWidget *frame1;
  GtkWidget *table1;
  GtkWidget *hseparator1;
  GtkWidget *frame2;
  GtkWidget *hbuttonbox1;
  GtkWidget *label;

  get_panel = (set_panel *)g_malloc (sizeof (set_panel));

  get_panel->window = gnome_app_new ("GXSNMP", _("Get A Mib"));
  gtk_window_set_wmclass (GTK_WINDOW (get_panel->window), "mib_get_panel",
			  "GXSNMP");

  gtk_signal_connect (GTK_OBJECT (get_panel->window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_get_panel),
		      get_panel->window);
  frame1 = gtk_frame_new(NULL);

  gtk_container_border_width(GTK_CONTAINER(frame1), 4);

  table1 = gtk_table_new(5, 4, FALSE);
  gtk_container_add(GTK_CONTAINER(frame1), table1);

  label = gtk_label_new (_("Mib "));
  gtk_table_attach (GTK_TABLE(table1), label, 0, 1, 0, 1,
                   GTK_FILL, GTK_FILL, 0, 0);
  label = gtk_label_new (_("Host"));
  gtk_table_attach (GTK_TABLE(table1), label, 0, 1, 1, 2,
                   GTK_FILL, GTK_FILL, 0, 0);
  label = gtk_label_new (_("Value"));
  gtk_table_attach (GTK_TABLE(table1), label, 0, 1, 2, 3,
                   GTK_FILL, GTK_FILL, 0, 0);
  get_panel->entries[SET_MIB_ENTRY] = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table1), get_panel->entries[SET_MIB_ENTRY],
		    1, 4, 0, 1,
		    GTK_FILL, GTK_FILL, 2, 2);
  get_panel->entries[SET_MIB_HOST] = gtk_combo_new ();
  gtk_table_attach (GTK_TABLE (table1), get_panel->entries[SET_MIB_HOST],
		    1, 4, 1, 2,
		    GTK_FILL, GTK_FILL, 2, 2);
  get_panel->entries[SET_MIB_RESULT] = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table1), get_panel->entries[SET_MIB_RESULT],
		    1, 4, 2, 3,
		    GTK_FILL, GTK_FILL, 2, 2);
  hseparator1 = gtk_hseparator_new();
  gtk_table_attach(GTK_TABLE(table1), hseparator1, 0, 4, 3, 4,
                   GTK_FILL, GTK_FILL, 1, 1);
  frame2 = gtk_frame_new(NULL);
  gtk_table_attach(GTK_TABLE(table1), frame2, 0, 4, 4, 5,
                   GTK_FILL, GTK_FILL, 0, 0);
  gtk_container_border_width (GTK_CONTAINER(frame2), 4);
  hbuttonbox1 = gtk_hbutton_box_new();
  gtk_container_add (GTK_CONTAINER (frame2), hbuttonbox1);
  gtk_container_border_width (GTK_CONTAINER (hbuttonbox1), 4);

  get_panel->buttons[SET_QUERY_BUTTON] = gtk_button_new_with_label 
    (_("Query"));
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), 
		     get_panel->buttons[SET_QUERY_BUTTON]);
  get_panel->buttons[SET_CLEAR_BUTTON] = gtk_button_new_with_label
    (_("Clear"));
  gtk_container_add (GTK_CONTAINER (hbuttonbox1),
		     get_panel->buttons[SET_CLEAR_BUTTON]);
  get_panel->buttons[SET_CLOSE_BUTTON] = gnome_stock_button
    (GNOME_STOCK_BUTTON_CLOSE);
  gtk_signal_connect (GTK_OBJECT (get_panel->buttons[SET_CLOSE_BUTTON]),
		      "clicked",
		      (GtkSignalFunc) close_get_panel_cb,
		      get_panel->window);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), 
		     get_panel->buttons[SET_CLOSE_BUTTON]);
  gnome_app_set_contents (GNOME_APP (get_panel->window), frame1);
  gtk_widget_show_all (get_panel->window);
}

/*
 * Callback functions (get mib panel)
 */
static gint
delete_get_panel (GtkWidget *widget, GdkEvent *e, gpointer data)
{
  destroy_get_panel ();
  return 0;
}

static void
close_get_panel_cb (GtkWidget *widget, gpointer data)
{
  hide_get_panel ();
}


/*
 * Global panel functions ( get mib panel )
 */
void
open_get_panel ()
{
  if (!get_panel)
    create_get_panel ();
  else
    if (!GTK_WIDGET_VISIBLE (get_panel->window))
      gtk_widget_show (get_panel->window);
  reset_get_panel ();
}

void 
destroy_get_panel ()
{
  if (get_panel)
    {
      gtk_widget_destroy (get_panel->window);
      g_free (get_panel);
      get_panel = NULL;
    }
}

void 
hide_get_panel ()
{
  if (get_panel->window)
    {
      if (GTK_WIDGET_VISIBLE (get_panel->window))
	gtk_widget_hide (get_panel->window);
    }
}

void
reset_get_panel ()
{
}

/*
 * Monitor mib panel
 */

/*
 * Global panel functions (monitor mib)
 */
void
open_monitor_panel ()
{
}

void 
destroy_monitor_panel ()
{
}

void
hide_monitor_panel ()
{
}

void
reset_monitor_panel ()
{
}

/* EOF */
