/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (C) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  Network map item widget
 *
 *  A map item is derived from a GnomeCanvasGroup.  
 */

#ifndef __GXSNMP_MAP_ITEM_H__
#define __GXSNMP_MAP_ITEM_H__

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "gxsnmp_map.h"

BEGIN_GNOME_DECLS

/**
 *
 *  Standard widget macros
 *
 **/
#define GXSNMP_TYPE_MAP_ITEM (gxsnmp_map_item_get_type())
#define GXSNMP_MAP_ITEM(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MAP_ITEM, GXsnmp_map_item)
#define GXSNMP_MAP_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_MAP_ITEM, GXsnmp_map_itemClass))
#define GXSNMP_IS_MAP_ITEM(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MAP_ITEM)
#define GXSNMP_IS_MAP_ITEM_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MAP_ITEM)
/**
 *
 *  Control blocks for the widget class and for widget instances
 *
 **/
typedef struct _GXsnmp_map_item		GXsnmp_map_item;
typedef struct _GXsnmp_map_itemClass	GXsnmp_map_itemClass;

struct _GXsnmp_map_item
{
  GnomeCanvasGroup	  group;	/* Base object */


  guint is_selected       : 1;          /* We are selected. */
  guint moving            : 1;          /* We are being moved. */
  guint toplevel          : 1;          /* We are a toplevel item, no parent */
  guint sensitive         : 1;          /* We respond to clicks. */

  GList			  *selected;	/* List of maps item is selected on */
  GnomeCanvasItem         *select_box;	/* signal handling method */
  GList		          *wires;	/* GList of attached wires */
  DB_graph		  *DB_graph;     /* DB_graph entry for object */
}; 

typedef struct _GXsnmp_wire __GXsnmp_wire;	/* Hack hack hack hack ... */

struct _GXsnmp_map_itemClass
{
  GnomeCanvasGroupClass canvas_group_class;	/* Base class */
  
  gboolean (* select)   (GXsnmp_map_item * item);
  gboolean (* deselect) (GXsnmp_map_item * item);
  void (* move)		(GXsnmp_map_item * item, 
			 gdouble	    x,
			 gdouble	    y);
  void (* add_wire)	(GXsnmp_map_item * item,
			 __GXsnmp_wire	 * wire);
  void (* move_wire)    (GXsnmp_map_item * item,
			 __GXsnmp_wire	 * wire);
  void (* remove_wire)  (GXsnmp_map_item * item,
			 __GXsnmp_wire	 * wire);
  void (* edit_item)    (GXsnmp_map_item  *item);
  gint (* popup_menu)   (GdkEventButton  * event,
			 gpointer          data);
  void (* changed)	(GXsnmp_map_item * item,
			 gpointer	   data);
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

GtkType	    gxsnmp_map_item_get_type	 (void);
GtkWidget * gxsnmp_map_item_new		 (void);

void        gxsnmp_map_item_set_location (GXsnmp_map_item * item, 
					  gdouble 	    x, 
					  gdouble 	    y);

gboolean    gxsnmp_map_item_get_location (GXsnmp_map_item * item, 
					  gdouble 	  * x, 
					  gdouble 	  * y);

gboolean    gxsnmp_map_item_select       (GXsnmp_map_item * item);

gboolean    gxsnmp_map_item_deselect     (GXsnmp_map_item * item);

END_GNOME_DECLS
#endif /* __GXSNMP_MAP_ITEM_H__ */
/* EOF */
