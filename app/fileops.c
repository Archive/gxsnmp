/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * File operations module.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "main.h"
#include "menus.h"
#include "menu_panel.h"
#include "smi.h"

#include "debug.h"

extern gxsnmp                      *app_info;
extern int                         errno;

/*
 * Local function declarations 
 */
static void     create_file_dialog       (void);
static void     cancel_button_cb         (GtkWidget *widget,
					  gpointer  data);
/* 
 * Local module variables
 */
static GtkWidget *file_dialog = NULL;

/* 
 * Local functions
 */
static void
create_file_dialog ()
{
  struct passwd  *entry;
  char           buf[255];
  D_FUNC_START;
  entry = getpwuid (getuid ());
  file_dialog = gtk_file_selection_new ("Get Filename");
  gtk_window_position (GTK_WINDOW (file_dialog), GTK_WIN_POS_MOUSE);
  snprintf (buf, sizeof(buf), "%s/%s", entry->pw_dir, DEF_CONFIG_PATH);
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (file_dialog),
				   buf);
  DPRT (buf);
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION 
				  (file_dialog)->cancel_button),
				  "clicked", 
				  GTK_SIGNAL_FUNC (cancel_button_cb),
				  NULL);
  gtk_widget_show (file_dialog);
  D_FUNC_END;
}

static void
cancel_button_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_hide (file_dialog);
}

/*
 * Global functions
 */

int
file_open (char *filename)
{
  /* needs to be written */
  return 1;
}

int 
file_close (char *filename)
{
  /* needs to be written */
  return 1;
}

void
close_file_dialog ()
{
  if (file_dialog)
    gtk_widget_destroy (file_dialog);
}

void 
open_file_dialog ()
{
  if (!file_dialog)
    {
      create_file_dialog ();
      return ;
    }
  if (!GTK_WIDGET_VISIBLE (file_dialog))
    {
      gtk_widget_show (file_dialog);
    }
}
/****************************************************************************
 * load_host_menu_config ()
 ***************************************************************************/
void
load_host_menu_config ()
{
  gint       i;
  gchar      menu_key[40], cmd_key[40], *menu_value, *cmd_value;
  user_menu  *new_entry;
  D_FUNC_START;
  app_info->current_config.menu.hostsmd = NULL;
  if (gnome_config_has_section ("/gxsnmp/HostMenu"))
    {
      d_print (DEBUG_DUMP, "Loading user configured host menu addtions.\n");
      gnome_config_push_prefix ("/gxsnmp/HostMenu/");
      for (i = 0; ; i++)
	{
	  g_snprintf (menu_key, sizeof (menu_key), "MenuItem_%d", i);
	  g_snprintf (cmd_key, sizeof (cmd_key), "CommandItem_%d", i);
	  d_print (DEBUG_DUMP, "Fetch key '%s'\n", menu_key);
	  menu_value = gnome_config_get_string (menu_key);
	  if (!menu_value)
	    {
	      d_print (DEBUG_TRACE, "No host menu items found.\n");
	      break;
	    }
	  d_print (DEBUG_DUMP, "load_host_menu_config: Fetch key %s", cmd_key);
	  cmd_value = gnome_config_get_string (cmd_key);
	  new_entry = g_malloc0 (sizeof (user_menu));
	  new_entry->item    = g_strdup (menu_value);
	  new_entry->command = g_strdup (cmd_value);
	  d_print (DEBUG_DUMP, "Appending (%s) to menu.", new_entry->item);
	  app_info->current_config.menu.hostsmd =
            g_slist_append (app_info->current_config.menu.hostsmd, new_entry);
	}
      if (app_info->current_config.menu.hostsmd)
	d_print(DEBUG_TRACE, "User menus loaded %d\n", 
	       g_slist_length (app_info->current_config.menu.hostsmd));
    }
  D_FUNC_END;
}
/****************************************************************************
 * load_network_menu_config ()
 ***************************************************************************/
void
load_network_menu_config ()
{
  gint       i;
  gchar      menu_key[40], cmd_key[40], *menu_value, *cmd_value;
  user_menu  *new_entry;
  app_info->current_config.menu.netmenu = NULL;
  D_FUNC_START;
  if (gnome_config_has_section ("/gxsnmp/NetworkMenu"))
    {
      d_print (DEBUG_TRACE, "Loading user configured network menu addtions.\n");
      gnome_config_push_prefix ("/gxsnmp/NetworkMenu/");
      for (i = 0; ; i++)
	{
	  g_snprintf (menu_key, sizeof (menu_key), "MenuItem_%d", i);
	  g_snprintf (cmd_key, sizeof (cmd_key), "CommandItem_%d", i);
	  d_print (DEBUG_DUMP, "Fetch key '%s'\n", menu_key);
	  menu_value = gnome_config_get_string (menu_key);
	  if (!menu_value)
	    {
	      d_print (DEBUG_TRACE, "No items found.\n");
	      break;
	    }
	  cmd_value = gnome_config_get_string (cmd_key);
	  new_entry = g_malloc0 (sizeof (user_menu));
	  new_entry->item    = g_strdup (menu_value);
	  new_entry->command = g_strdup (cmd_value);
	  d_print (DEBUG_TRACE, "Appending (%s) to menu.\n", new_entry->item);
	  app_info->current_config.menu.netmenu = 
            g_slist_append (app_info->current_config.menu.netmenu, new_entry);
	}
      if (app_info->current_config.menu.netmenu)
	d_print (DEBUG_TRACE, "User menus loaded %d\n", 
	       g_slist_length (app_info->current_config.menu.netmenu));
    }
  D_FUNC_END;
}
/****************************************************************************
 * load_mib_config ()
 ***************************************************************************/
void
load_mib_config ()
{
  gint       i;
  gchar      mib_key[40], *mib_value;
  D_FUNC_START;
  app_info->current_config.mib.mibs = NULL;
  if (gnome_config_has_section ("/gxsnmp/MIB"))
    {
      d_print (DEBUG_TRACE, "Loading MIB configuration.\n");
      gnome_config_push_prefix ("/gxsnmp/MIB/");
      for (i = 0; ; i++)
	{
	  g_snprintf (mib_key, sizeof (mib_key), "Load_MIB_%d", i);
	  d_print (DEBUG_TRACE, "Fetch key '%s'\n", mib_key);
	  mib_value = gnome_config_get_string (mib_key);
	  if (!mib_value)
	    {
	      d_print (DEBUG_TRACE, "No items found.\n");
	      break;
	    }
	  d_print (DEBUG_DUMP, "Appending (%s) to the MIB list.\n", mib_value);
	  smiLoadModule(mib_value);
          app_info->current_config.mib.mibs = 
            g_slist_append ( app_info->current_config.mib.mibs, 
            g_strdup (mib_value));
	}
      if (app_info->current_config.mib.mibs)
	d_print (DEBUG_TRACE, "MIB configuration loaded (%d)\n", 
	       g_slist_length (app_info->current_config.mib.mibs));
    }
  D_FUNC_END;
}
/****************************************************************************
 * Function to read the configuration file using  gnome_config* functions
 ***************************************************************************/
gboolean
load_config ()
{

  D_FUNC_START;

  load_host_menu_config ();
  load_network_menu_config ();
  load_mib_config ();
  if (gnome_config_has_section ("/gxsnmp/SQL"))
    {
      d_print (DEBUG_TRACE, "New config file found, it has sql settings "
	       "loading from there.\n");
      gnome_config_push_prefix ("/gxsnmp/SQL/");
      app_info->general_db.engine = gnome_config_get_string ("Engine-Name");
      app_info->general_db.host = gnome_config_get_string ("DB-Host-Name");
      app_info->general_db.port = gnome_config_get_int ("DB-Port");
      app_info->general_db.user = gnome_config_get_string ("DB-User");
      app_info->general_db.password = gnome_config_get_string ("DB-Pass");
      app_info->general_database = gnome_config_get_string ("General_Database");
      gnome_config_pop_prefix ();
      return TRUE;
    }
  return TRUE;
}
/****************************************************************************
 * save_host_menu_config ()
 ***************************************************************************/
void
save_host_menu_config ()
{
  gint       i;
  gchar      menu_key[40], cmd_key[40];
  user_menu  *new_entry;
  GSList     *gsl;
  D_FUNC_START;
  gnome_config_push_prefix ("/gxsnmp/HostMenu/");
  i = 0;
  gsl = app_info->current_config.menu.hostsmd;
  while (gsl)
    {
      new_entry = (user_menu *)gsl->data;
      g_snprintf (menu_key, sizeof (menu_key), "MenuItem_%d", i);
      g_snprintf (cmd_key, sizeof (cmd_key), "CommandItem_%d", i++);
      gnome_config_set_string (menu_key, new_entry->item);
      gnome_config_set_string (cmd_key, new_entry->command);
      d_print (DEBUG_DUMP, "Storing menu item '%s' with a value of '%s'\n",
	       new_entry->item, new_entry->command);
      gsl = gsl->next;
    }
  D_FUNC_END;
}
/****************************************************************************
 * save_network_menu_config ()
 ***************************************************************************/
void
save_network_menu_config ()
{
  gint       i;
  gchar      menu_key[40], cmd_key[40];
  user_menu  *new_entry;
  GSList     *gsl;
  D_FUNC_START;
  gnome_config_push_prefix ("/gxsnmp/NetworkMenu/");
  i = 0;
  gsl = app_info->current_config.menu.netmenu;
  while (gsl)
    {
      new_entry = (user_menu *)gsl->data;
      g_snprintf (menu_key, sizeof (menu_key), "MenuItem_%d", i);
      g_snprintf (cmd_key, sizeof (cmd_key), "CommandItem_%d", i++);
      gnome_config_set_string (menu_key, new_entry->item);
      gnome_config_set_string (cmd_key, new_entry->command);
      d_print (DEBUG_DUMP, "Storing menu item '%s' with a value of '%s'\n",
	       new_entry->item, new_entry->command);
      gsl = gsl->next;
    }
  D_FUNC_END;
}
/****************************************************************************
 * save_mib_config ()
 ***************************************************************************/
void
save_mib_config ()
{
  gint       i;
  gchar      mib_key[40];
  GSList     *gsl;
  D_FUNC_START;
  gnome_config_push_prefix ("/gxsnmp/MIB/");
  i = 0;
  gsl = app_info->current_config.mib.mibs;
  while (gsl)
    {
      g_snprintf (mib_key, sizeof (mib_key), "Load_MIB_%d", i++);
      gnome_config_set_string (mib_key, (gchar *)gsl->data);
      d_print (DEBUG_DUMP, "Storing key '%s' with a value of '%s'\n",
	       mib_key, (gchar *)gsl->data);
      gsl = gsl->next;
    }
  D_FUNC_END;
}
/****************************************************************************
 * save_config ()
 ***************************************************************************/
gboolean
save_config ()
{
  D_FUNC_START;

  gnome_config_clean_file ("/gxsnmp");
  d_print (DEBUG_DUMP, "Saving SQL section.\n");
  gnome_config_push_prefix ("/gxsnmp/SQL/");
  gnome_config_set_string ("Engine-Name", app_info->general_db.engine);
  gnome_config_set_string ("DB-Host-Name", app_info->general_db.host);
  gnome_config_set_int ("DB-Port", app_info->general_db.port);
  gnome_config_set_string ("DB-User", app_info->general_db.user);
  gnome_config_set_string ("DB-Pass", app_info->general_db.password);
  gnome_config_set_string ("General_Database", app_info->general_database);
  gnome_config_pop_prefix ();
  d_print (DEBUG_DUMP, "Saving host menu configuration.");
  save_host_menu_config ();
  d_print (DEBUG_DUMP, "Saving network menu configuration.");
  save_network_menu_config ();
  d_print (DEBUG_DUMP, "Saving MIB configuration.");
  save_mib_config ();
  //  gnome_config_pop_prefix ();
  gnome_config_pop_prefix ();
  gnome_config_sync();
  D_FUNC_END;
  return TRUE;
}
/* EOF */


