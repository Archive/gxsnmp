/*
 * Function prototypes
 */
#ifndef __FUNC_PROTOS_H
#define __FUNC_PROTOS_H

/* net_map.c */
GtkWidget                 *create_map_panel         (GtkWidget      *parent,
						     gchar          *title);
gboolean                  map_add_node              (NodeType       type,
						     gpointer       data);
gboolean                  map_add_host              (hosts          *host,
						     gdouble        x,
						     gdouble        y);
gboolean                  map_del_host              (hosts          *host);
gboolean                  map_add_network           (net_entry      *network,
						     gdouble        x,
						     gdouble        y);
gboolean                  map_del_network           (net_entry      *network);
void                      map_zoom_in               (GnomeCanvas    *canvas);
void                      map_zoom_out              (GnomeCanvas    *canvas);
void                      map_highlight_host        (hosts          *host);
void                      map_unhighlight_host      (hosts          *host);
GtkWidget                 *map_get_canvas_widget    (GtkWidget      *panel);
hosts                     *map_get_selected_host    (GtkWidget      *panel);
map_node                  *map_get_selected_node    (GtkWidget      *panel);
GtkWidget                 *map_get_active_map       (void);
void                      map_update_stat           (gchar          *msg, ...)
                                                    __attribute__ 
                                                    ((format (printf, 1, 2)));
void                      map_set_node_label        (map_node       *node,
						     gchar          *text,
						     gchar          *color);
void                      map_set_node_label_color  (map_node       *node,
						     GdkColor       *color,
						     gchar          *cl_name);
void                      map_link_nodes            (map_node       *node1,
						     map_node       *node2);
void                      map_move_node             (map_node       *node,
						     double         x,
						     double         y);
void                      map_select_node           (GtkWidget      *panel,
						     map_node       *node);
void                      map_unselect_node         (GtkWidget      *panel,
					             map_node       *node);
void           host_pixmap                   (GnomeCanvasGroup *group,
					      gchar            *pixmap,
					      double           x,
					      double           y);

/* net.c */
GHashTable     *nl_get_network_hash_table    (void);
void           nl_free_network_hash_table    (void);
net_entry      *nl_add_network               (net_entry          *network);
gboolean       nl_del_network                (net_entry          *network);
map_node       *nl_get_host_net              (hosts              *host);
net_entry      *nl_find_network_by_name      (gchar              *name);

/* network_panel.c */
void           open_add_network_panel        (void);
void           open_edit_network_panel       (void);
void           network_panel_do_edit         (net_entry          *network);

/* host_list.c */
void           create_hlist_panel            (void);
void           hlist_panel_open              (void);
void           populate_list                 (void);
void           list_add                      (void               *key, 
					      void               *val, 
					      void               *data);
void           create_sysinfo_panel          (void);
void           open_sysinfo_panel            (void);
void           open_interface_panel          (void);
void           refresh_info                  (hosts              *host);
void           refresh_interfaces            (hosts *host);
gboolean  update_widget                  (host_snmp *host, 
					  void *magic, 
					  SNMP_PDU *spdu, 
					  GSList *objs);
gboolean  update_ctable                  (host_snmp *host, 
					  void *magic, 
					  SNMP_PDU *spdu, 
					  GSList *objs);
gboolean  update_interface_table         (host_snmp   *host,
					  void        *magic,
					  SNMP_PDU    *spdu,
					  GSList      *objs);

gboolean  update_int_detail              (host_snmp   *host,
					  void        *magic,
					  SNMP_PDU    *spdu,
					  GSList      *objs);

/*
 * Callbacks for snmp queries (if the query timed out)
 */
void       destroy_widget                 (host_snmp *host,
                                           void *magic);
void       destroy_ctable                 (host_snmp *host,
					   void *magic);

void       destroy_interface_table        (host_snmp  *host, 
					   void       *magic);
/*
 * Standard panel functions
 *
 * route panel
 */
void       open_route_panel               (void);
void       destroy_route_panel            (void);
void       reset_route_panel              (void);
void       hide_route_panel               (void);

/*
 * Interfaces panel
 */
void       open_interface_panel           (void);
void       destroy_interface_panel        (void);
void       reset_interface_panel          (void);
void       hide_interface_panel           (void);
void       interface_set_selected_host    (hosts     *host);
hosts      *interface_get_selected_host   (void);
/*
 * Interface detail panel 
 */
void       open_int_detail_panel         (void);
void       destroy_int_detail_panel      (void);
void       reset_int_detail_panel        (void);
void       hide_int_detail_panel         (void);
void       int_detail_set_selected_host  (hosts      *host);
hosts      *int_detail_get_selected_host (void);
/*
 * System information panel.
 */
void                      update_host_load         (void);
void                      refresh_info             (hosts      *host);
void                      sysinfo_set_selected_host    (hosts      *entry);
hosts                     *sysinfo_get_selected_host   (void);

/*
 * FIXME: This doesn't really belong here
 */
void                      add_null_var             (GSList         **objs, 
						    GSList         **entry, 
						    char           *what, 
						    GtkWidget      *name);

/* host.c */
struct hostent            *lookuphost              (char           *hostname);
hosts                     *find_host_by_name       (const gchar    *hostname);
hosts                     *find_host_by_addr       (struct in_addr *addr);
hosts                     *hl_add_host             (hosts          *entry);
void                      hl_del_host              (hosts          *entry);
hosts                     *get_current_host        (GtkWidget      *widget);
void                      add_interface            (hosts          *entry, 
						    inf_list       *inf);
void                      remove_interface         (hosts          *entry, 
						    inf_list       *interface);
void                      show_interface_list      (hosts          *entry);
inf_list                  *find_interface_by_addr  (struct in_addr *if_addr);
GHashTable                *get_host_hash           (void);

/* menus.c */
void                      do_network_popup_menu    (GdkEventButton *event, 
						    gpointer       data);
void                      do_net_map_popup_menu    (GdkEventButton *event,
						    gpointer       data);
/* host_panel.c */
GtkWidget                 *create_snmp_frame       (GtkWidget      *parent);
void                      clear_snmp_frame         (GtkWidget      *parent,
						    gboolean       defaults);
void                      set_snmp_frame           (GtkWidget      *parent,
						    host_snmp      *snmp);
void                      get_snmp_frame           (GtkWidget      *parent,
						    host_snmp      *snmp);


#endif



