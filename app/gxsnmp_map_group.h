/*
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The map_group widget is for grouping other map items together.
 * It can be expanded/collasped.
 */
#ifndef __GXSNMP_MAP_GROUP_H__
#define __GXSNMP_MAP_GROUP_H__

#include <libgnome/gnome-defs.h>

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "gxsnmp_map.h"
#include "gxsnmp_map_item.h"
BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros
 ***************************************************************************/
#define GXSNMP_TYPE_MAP_GROUP (gxsnmp_map_group_get_type ())
#define GXSNMP_MAP_GROUP(obj) \
        GTK_CHECK_CAST ((obj), GXSNMP_TYPE_MAP_GROUP, GXsnmp_map_group)
#define GXSNMP_MAP_GROUP_CLASS(klass) \
        GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_MAP_GROUP, \
                              GXsnmp_map_groupClass)
#define GXSNMP_IS_MAP_GROUP(obj) \
        GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MAP_GROUP)
#define GXSNMP_IS_MAP_GROUP_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MAP_GROUP)
/****************************************************************************
 * Control blocks for the widget class and widget instances
 ***************************************************************************/
typedef struct _GXsnmp_map_group          GXsnmp_map_group;
typedef struct _GXsnmp_map_groupClass     GXsnmp_map_groupClass;
struct _GXsnmp_map_group
{
  GXsnmp_map_item        map_item;        /* Base object */

  /* Collapsed */
  GnomeCanvasItem        *image;          /* Visual representation */
  GnomeCanvasItem        *text;           /* Label */
  gchar                  *text_color;     /* The color of the label */
  
  GList                  *members;        /* List of items in this group */

  gboolean               collapsed;       /* Is the group collapsed or not */
  GXsnmp_map             *expand_map;     /* The map we expand onto, NULL if
					     we expand in place on the 
					     map we are on. */

};

struct _GXsnmp_map_groupClass 
{
  GXsnmp_map_itemClass   map_item_class; /* Base class */
};
/****************************************************************************
 * Public widget manipulation functions.
 ***************************************************************************/
GtkType           gxsnmp_map_group_get_type            (void);
GXsnmp_map_item   *gxsnmp_map_group_new                (DB_graph   *graph);


END_GNOME_DECLS
#endif /* __GXSNMP_MAP_GROUP_H__ */
/* EOF */
