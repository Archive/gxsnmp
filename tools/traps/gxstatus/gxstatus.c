/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* gxstatus.c -- CMD program to manipulate daemon status
*
*/

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <glib.h>


#include "dae.h"

char version[] = "$Id$"; /*usefull to 'strings' the binary*/

aclist *trapfilter;
int gxsocket;
char gxsocketfile[] = "/gxsfile";
glb_enviroment enviroment;
char pipestring[4000];

char usage[] = "usage: gxstatus [status|start|stop|reload| [daemon]] | [cmd <cmdname>]\n";

main(int argc, char **argv)
{   
int nbytes;
char line[200];

trappdu tpdu;

	if(load_enviroment()){
		fprintf(stderr,"gxstatus: Error, couldn't load enviromental files.\n Please check that you are standing in the directory where gxstatus lives.\n");
		goto quit;
	}
        sprintf(line,"%s%s", enviroment.base, gxsocketfile);
	unlink(line);
	gxsocket = createsocket(line,0);
        fprintf(stderr,"Socket used(%s)\n", line);

	if(argc == 1 || !strcmp(argv[1],"status")){		/* gxstatus */
		tpdu.type = PDU_PROGRAM_STATUS;
		strcpy(tpdu.argbuf, "/gxsfile");
		tpdu_write(gxsocket, enviroment.gxdhd, tpdu);

		printf("Wait..\n");
                tpdu_read(gxsocket, &tpdu);
		printf("Status:\n%s\n", tpdu.argbuf);
		goto quit;
	}


	else if(!strcmp(argv[1],"stop") && argc == 2){		/* gxstatus stop */
		tpdu.type = PDU_PROGRAM_STOP;
		strcpy(tpdu.argbuf, "all");
		tpdu_write(gxsocket, enviroment.gxdhd, tpdu);
		goto quit;
	}
	else if(!strcmp(argv[1],"start") && argc == 2){		/* gxstatus start */
	fprintf(stderr,"Starting all daemons\n");
		strcpy(line,enviroment.base);
		strcat(line,"/bin/gxdhd");
		fprintf(stderr,"Running(%s)\n", line);
		execl(line,NULL);
		goto quit;
        }
	else if(!strcmp(argv[1],"start") && argc == 3){		/* gxstatus start daemon */
		tpdu.type = PDU_PROGRAM_START;
		strcpy(tpdu.argbuf, argv[2]);
		tpdu_write(gxsocket, enviroment.gxdhd, tpdu);
		goto quit;
        }
	else if(!strcmp(argv[1],"stop") && argc == 3){		/* gxstatus stop daemon */
		tpdu.type = PDU_PROGRAM_STOP;
		strcpy(tpdu.argbuf, argv[2]);
		tpdu_write(gxsocket, enviroment.gxdhd, tpdu);
		goto quit;
        }
	else if(!strcmp(argv[1],"reload") && argc == 3){	/* gxstatus reload daemon */
		tpdu.type = PDU_PROGRAM_RELOAD;
		strcpy(tpdu.argbuf,argv[2]);
		
		tpdu_write(gxsocket, enviroment.gxdhd, tpdu);
		goto quit;
        }
	else if(!strcmp(argv[1],"cmd") && argc == 3){		/* gxstatus cmd */
		if(!strcmp(argv[2], "EVDPYSRVD_PRINT_CONNECTED")){
			tpdu.type = PDU_CMD_EVDPYSRVD_PRINT_CONNECTED;
			sprintf(tpdu.argbuf,"/gxsfile");
			tpdu_write(gxsocket, enviroment.evdpysrvd, tpdu);

			printf("Wait..\n");
			tpdu_read(gxsocket, &tpdu);
			printf("Status:\n%s\n",tpdu.argbuf);
			goto quit;
		}
        }

quit:
	unlink(gxsocketfile);
	exit(0);
}
