/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* log.c -- logging facility
*
*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <netinet/in.h>

#include "dae.h"

/* log functions, to have a logging facility*/

void logstr(FILE *fp, gchar *text){
time_t i = time(0);
gchar *date = ctime(&i);
  date[strlen(date)-1] = 0;       /*remove linefeed*/
  fprintf(fp,"%d, %s, %s", time, date, text);
  fflush(fp);
}

FILE *logopen(gchar *filename){
  return fopen(filename, "a");
}
