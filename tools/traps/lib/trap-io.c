/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* trap-io.c -- format incoming traps
*
*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/un.h>
#include <netinet/in.h>   
#include <arpa/inet.h>
#include <unistd.h>

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "ddserver.h"

#include "g_snmp.h"

#include "dae.h"

/****************************************************************************
 * Inter-daemon data transfer functions
 ***************************************************************************/

/*obsolete*/
int createsocket(const char *filename, int flag){
struct sockaddr_un name;
int sock;
size_t size;

  fprintf(stderr,"createsocket():WARNING this function is obsoleted.\n");
  sock = socket(PF_UNIX, SOCK_DGRAM, 0);
  if(sock < 0){ 
    perror("socket()");
    return 0;
  }
  name.sun_family = AF_UNIX;
  strcpy(name.sun_path, filename);
  size = (offsetof (struct sockaddr_un, sun_path) + strlen(name.sun_path) + 1);
  if(bind(sock,(struct sockaddr *) &name, size) < 0){ 
    perror("bind()");
    return 0;
  }
  if(flag) fcntl(sock, F_SETFL, O_NONBLOCK);
  return sock;	
}

/*obsolete*/
int sendsocket(int sock, char *dst, char *text){
struct sockaddr_un name;
size_t size;
int nbytes;

  fprintf(stderr,"sendsocket():WARNING this function is obsoleted.\n");
  name.sun_family = AF_UNIX;
  strcpy(name.sun_path, dst);
  size = strlen(name.sun_path) + sizeof( name.sun_family);
  nbytes = sendto(sock, text, strlen(text) + 1, 0, (struct sockaddr *) &name, size);
  return 0;
}
int senddsocket(int sock, char *dst, char *data, size_t len){
struct sockaddr_un name;
size_t size;
int nbytes;

  name.sun_family = AF_UNIX;
  strcpy(name.sun_path, dst);
  size = strlen(name.sun_path) + sizeof( name.sun_family);
  nbytes = sendto(sock, data, len + 1, 0, (struct sockaddr *) &name, size);
  return 0;
}

/* g_trap_decode()
*
* decode trap to a g_ style struct
*/
gint g_trap_decode(trappdu *tpdu, g_trap *trap){
gint k, traplen;
struct in_addr ip;
g_oid *goid;

static gchar mds[2049];
       guint n, a;

  union _member {
    gchar string[2049];
    guint integer;
    guint dbl;
  }member;

  if(trap->args =! NULL)
    fprintf(stderr,"Warning: arguments is not null ( mem-lost )\n");

/* FIXME: free args or return -1 !
  if(trap->args =! NULL) return 0;
  else trap->args = NULL;
*/
  trap->args = NULL;

  trap->severity = tpdu->severity;
  trap->timestamp = tpdu->time;

  memcpy(&ip, &(tpdu->address), sizeof(struct in_addr));		/* get address */
  trap->address = g_strdup(inet_ntoa(ip));

  trap->g = tpdu->generic;
  trap->s = tpdu->specific;

  if(tpdu->oidlen > 130) return 0; 				/*buffer overflow*/

  trap->oid = g_strndup(tpdu->oid, tpdu->oidlen);

  n = 0;
  for(k=0;10>k;k++){		/*process trap arguments, maximum of 10 arguments */


    memcpy(&a, tpdu->argbuf+n, sizeof(gint));
    //if( *((guint *) (tpdu->argbuf+n)) == 255 ){ 		/*end of block*/
    if(a == 255){
      break;
    }

    goid = g_malloc0(sizeof(g_oid));

    get_member(&n, tpdu->argbuf, &member);    /* get Argument OID */
    goid->name = g_strdup(member.string);

    switch(get_member(&n, tpdu->argbuf, &member)){
      case PDUMEMTYPE_INT:
        goid->type = 1;
        sprintf(mds, "%u", member.integer);
      break;
      case PDUMEMTYPE_STR:
        goid->type = 2;
        strcpy(mds, member.string);
      break;
      default:
        fprintf(stderr,"APPERR: g_trap_decode(): get_member() unknown type\n");
      return 0;
    }
    goid->data = g_strdup(mds);

    trap->args = g_list_append(trap->args, goid);
  }

  traplen = sizeof(trappdu);

  return traplen;
}


void
g_trap_free(g_trap *trap)
{
  GList *gl;
  g_oid *goid;

  if(trap->oid)
    g_free(trap->oid);
  if(trap->address)
    g_free(trap->address);

  gl = trap->args;
  while(gl){
    goid = gl->data;
    if(goid->name) g_free(goid->name);
    if(goid->data) g_free(goid->data);
    g_free(goid);
    gl = gl->next;
  }
  g_list_free(trap->args);
  trap->args = NULL;
}

