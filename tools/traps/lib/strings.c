/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* strings.c -- string processing
*
*/

/* strings library
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <netinet/in.h>
#include "dae.h"

char *wordcpy(char *dst, char *src, int w){ /*copy space separated word 'w' to 'dst' from 'src'*/
int i=0,j=0;

  w--;
  *dst = '\0';
  while(1){
    if(!w) break;
      if((*(src+i) == '\0' || *(src+i) == '\n')) return 0; /*leave dst empty*/
      if(*(src+i) == '\"'){
        i++;
        while(*(src+i) != '\"' && !(*(src+i) == '\0' || *(src+i) == '\n')) i++;
        if(!(*(src+i) == '\0' || *(src+i) == '\n')) i++;
        continue;
      }
      if(*(src+i) == ' '){w--;i++; continue;}
      i++;
    }
    if(*(src+i) == '\"'){
    i++;
    while( !(*(src+i) == '\0' || *(src+i) == '\n') && !(*(src+i) == '\"') ){ *(dst+j) = *(src+i);i++;j++;}   /*move each character*/
    *(dst+j) = '\0';
    return dst;
  }
  while( !(*(src+i) == '\0' || *(src+i) == '\n') && !(*(src+i) == ' ') ){ *(dst+j) = *(src+i);i++;j++;}   /*move each character*/
  *(dst+j) = '\0';
  return dst;
}

void restrap(char *line) 
{
  if(!(strncmp(line,"\"",1)))
  {
    if (!(strncmp(line+(strlen(line)-1),"\"",1)))
    {
      strncpy(line,line+1,strlen(line)-2);
      strncpy(line+strlen(line)-2,"\0",1);
    }	
  }
}

gchar *
trap_get_arg(g_trap *trap, gint num)
{
  GList *gl;
  g_oid *goid;
  gint i;

  i = 0;
  gl = trap->args;
  while(gl){
    i++;
    if(i != num){
      gl = gl->next;
      continue;
    }
    goid = gl->data;
    return goid->data;
  }
}

gchar *
var_interpret2(gchar *src, g_trap *trap)
{
gchar w1[200];
gchar line1[2000];
gchar line2[2000];
gint i;
gchar *arg;

  fprintf(stderr,"INPUT(%s)\n", src);
  i = 1;
  line1[0] = 0;
  while(wordcpy(w1, src, i)){
    i++;
    if(!strcmp(w1, "$IP")){
      sprintf(line2, "%s ", trap->address);
      strcat(line1, line2);
      continue;
    }
    if(!strcmp(w1, "$NAME")){
      sprintf(line2, "%s ", trap->name);
      strcat(line1, line2);
      continue;
    }
    if(!strcmp(w1, "$ARG1")){
      arg = trap_get_arg(trap, 1);
      sprintf(line2, "%s ", arg);
      strcat(line1, line2);
      continue;
    }
    if(!strcmp(w1, "$ARG3")){
      arg = trap_get_arg(trap, 3);
      sprintf(line2, "%s ", arg);
      strcat(line1, line2);
      continue;
    }
    if(!strcmp(w1, "$ARG4")){
      arg = trap_get_arg(trap, 4);
      sprintf(line2, "%s ", arg);
      strcat(line1, line2);
      continue;
    }
    if(!strcmp(w1, "$ARG2")){
      arg = trap_get_arg(trap, 2);
      sprintf(line2, "%s ", arg);
      strcat(line1, line2);
      continue;
    }
    sprintf(line2, "%s ", w1);
    strcat(line1, line2);
  }
  fprintf(stderr,"OUTPUT(%s)\n", line1);
  return g_strdup(line1);
}

gint
var_interpret(gchar *dst, gchar *src, int max, g_trap *trap)
{ /*variable substitution*/
gint i,j=0;

  dst[0] = 0;
  for(i=0;;i++, j++){
    if(*(src+i) == '\0') break;
    if(!strncmp(src+i,"$IP",3)){	/*variable $IP is detected*/
      if(!strlen(trap->address)){
        *(dst+j) = *(src+i);		/*set current dst+j and continue, else for's j++ would leave this dst+j unset*/
        continue;
      }
      if((strlen(trap->address)+j) > max){		/*destination buffer overflowed*/
        dst[0] = 0;
        return 0;
      }
      strcpy(dst+j, trap->address);
      j += strlen(trap->address);
      i += 3;
      *(dst+j) = *(src+i);
      continue;
    }
    *(dst+j) = *(src+i);
  }
  *(dst+j) = 0;
  return strlen(dst);
}

/**
* String Substituion library
*/

/* strsub_add ()
*
* add a identifier
* to the strsub list
*/

GList *
g_strsub_list_add (GList *gl, gchar *name, gpointer *text)
{
  g_strsub *strsub;
  strsub = g_malloc0(sizeof(g_strsub));
  if(!name || !text) return strsub;
  strsub->name = g_strdup(name);
  strsub->text = text;
  return g_list_append(gl, strsub);
}

gint
g_strsub_list_remove (GList *gl, gchar *name)
{
}

gpointer
g_strsub_lookup (GList *gl, gchar *name)
{
  g_strsub *strsub;
  while(gl){
    strsub = gl->data;
    if(!g_strcasecmp(name, strsub->name))
      return strsub->text;
    gl = gl->next;
  }
  return 0;

}

gchar *
g_strsub_substitute (GList *gl, gchar *intext)
{
gchar w1[200], line1[2000], line2[2000]; /*ugly..*/
gpointer *text;
gint i;

  line1[0] = 0;
  i = 1;
  while(wordcpy(w1, intext, i)){
    i++;
    text = g_strsub_lookup(gl, w1);
    if(text){ /* word is recognized */
      if(*text){ /* text contains a non-null-pointer */
        sprintf(line2, "%s ", *text); 
        strcat(line1, line2);
        continue;
      }
    }
    sprintf(line2, "%s ", w1);
    strcat(line1, line2);
  }
  return g_strdup(line1);
}
