/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* lex.c -- lexical analyzer wrapper
*
*/

/* gscanner test
 *
 * template:
 * everything in parantheses is optional
 *
 * Identification (IdentificationValue) {
 *
 * "Entity" (=) "Value"  <-this is a Configentry
 *
 * }
 *
 */
#include <stdio.h>
#include <glib.h>

#include "lex.h"

int lex_init(lexrunst *lex, gchar *filename)
{
gint fd;

	lex->fp = fopen(filename, "r");
	fd = fileno(lex->fp);
	
	lex->scanner = g_scanner_new((GScannerConfig *) &gscanner_config);
	g_scanner_input_file(lex->scanner, fd);

	return 0;
}

/* lex_block_start()
 *
 * lookup and set filepointer
 * to beginning of block: { ... ... ... }
 *                          ^
 * UNTESTED
 */

int lex_block_start(lexrunst *lex, gchar *id)
{
	while(g_scanner_get_next_token(lex->scanner)){		/* lookup ID in identification part in blocks */
		if(lex->scanner->token == G_TOKEN_IDENTIFIER){
			if(strcmp(id, lex->scanner->value.v_identifier))
				return 0;
		}
	}
}

void lex_close(lexrunst *lex)
{
	fclose(lex->fp);
}

void lex_load_symbol_table(lexrunst *lexs, lex_symtab *symtab)
{
gint i;

	g_scanner_set_scope(lexs->scanner, 1);
	g_scanner_freeze_symbol_table (lexs->scanner);
	for (i = 0; symtab[i].name[0] != 0; i++)
		g_scanner_scope_add_symbol (lexs->scanner, 1, symtab[i].name, GINT_TO_POINTER (symtab[i].token));
	g_scanner_thaw_symbol_table (lexs->scanner);
}


/* lex_read_token()
 *
 * get one or more tokens.
 * it will identify tokens using the symbol table
 * with extended information.
 * If it identifies a token using the symbol table
 * lex.known will be TRUE.
 *
 */
int lex_read_token(lexrunst *lex, lex_symtab *symtab)
{
int n=0;
int i,h;

  lex->numread = 0;
  lex->known = FALSE;					/* reset known state */
  while(n = g_scanner_get_next_token(lex->scanner)){	/* read in a token */
    switch(n){
      case G_TOKEN_LEFT_CURLY:	/* start of block */
      case G_TOKEN_RIGHT_CURLY:	/* end of block */
        return n;		/* let user handle blocks */
      case G_TOKEN_IDENTIFIER:	/* Identifier not in symtab */
        strcpy(lex->u_entity, lex->scanner->value.v_identifier);
        return G_TOKEN_IDENTIFIER;
      case G_TOKEN_STRING:		/* unknown user data */
        strcpy(lex->value_str, lex->scanner->value.v_string);
        return n;
      break;
      case G_TOKEN_INT:		/* unknown user data */
        lex->value_int = lex->scanner->value.v_int;
        return n;
      break;
      default:			/* lookup token in symbol table */
        for(i=0; symtab[i].name[0] != 0; i++){
          if(n == symtab[i].token && symtab[i].num == 1){			/* we know ONE value after */
            lex->entity = n;	/* save entity */
            lex->numread = 2;   /* success to read data */
            h = g_scanner_get_next_token(lex->scanner);		/* read data token */
            switch(h){
              case G_TOKEN_STRING:
                strcpy(lex->value_str, lex->scanner->value.v_string);
                lex->datatype = G_TOKEN_STRING;
              break;
              case G_TOKEN_INT:
                lex->value_int = lex->scanner->value.v_int;
                lex->datatype = G_TOKEN_INT;
              break;
              default:
                return LEX_PARSE_ERROR;	 /* we can't continue to parse file */
            }
            lex->known = TRUE;
            return n;
          } else if(n == symtab[i].token ){		/* we only know token, but
							   we will try to read a data */
            lex->known = TRUE;
            lex->entity = n;					/* save entity */
            lex->numread = 2;			/* if we success read */
            switch(g_scanner_peek_next_token(lex->scanner)){	/* check if next is data */
              case G_TOKEN_STRING:
                g_scanner_get_next_token(lex->scanner);
                strcpy(lex->value_str, lex->scanner->value.v_string);
                lex->datatype = G_TOKEN_STRING;
                return n;
              case G_TOKEN_INT:
                g_scanner_get_next_token(lex->scanner);
                lex->value_int = lex->scanner->value.v_int;
                lex->datatype = G_TOKEN_INT;
                return n;
              default:				/* it seems to be something else */
                lex->numread = 1;		/* only entity success to read */
                return n;
            }
          } /* end of symtab IF recognize */
        } /* end of symtab FOR lookup */
        return n;	/* UNKNOWN TOKEN (user: lex->known is FALSE) */
    } /* end of main switch */
  } /* end of read token-while loop */
  return n; /* End of File */
}

/* lex_read_block()
 *
 * read a whole block
 * returns 0 if end of block
 */

guint lex_read_block(lexrunst *lex, lex_symtab *symtab)
{
gint n;

  n = g_scanner_peek_next_token(lex->scanner);
  switch(n){
    case G_TOKEN_LEFT_CURLY:		/* start of block */
      g_scanner_get_next_token(lex->scanner);
      lex->r++;				/*a sub block starting*/
    break;
    case G_TOKEN_RIGHT_CURLY:		/* end of block */
      g_scanner_get_next_token(lex->scanner);
      lex->r--;				/* end of sub block */
      if(lex->r == 0) return 0;		/* whole block readed */
    break;
    default:
      n = lex_read_token(lex, symtab);
    break;
  }
  return n;	/* be transparent */
}
