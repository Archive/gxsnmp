/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* acl.c -- Access Control List functions
*
*/

/* event handling system, general library
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include <errno.h>
//#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/un.h>
#include <netinet/in.h>   
#include <arpa/inet.h>
#include <unistd.h>

#include "g_snmp.h"


#include "dae.h"

/*Access control functions
 *  test_acl	This will take oid/g/s and check to the loaded access list wheter to drop or display trap
 *  load_acl	This loads an access list from (char *filename)
*/

int test_acl(aclist acl, GList *gl){	/* test if *acl is permitted by the access list loaded by load_acl()*/
int l = 0;
int lm=0;				/*longest match*/
int tlm=0;				/*tested oid longest match*/

int lm10=0;				/*longest match command 10 ( deny oid )*/
int lm13=0;				/*longest match command 13 ( deny oid+g+s )*/
int lm20=0;				/*longest match command 20 ( permit oid )*/
int lm23=0;				/*longest match command 23 ( deny oid+g+s )*/

  aclist *fil;

/* logic needs to be rewritten */
	while(gl){
                fil = gl->data;
		if(!strcmp(fil->oid,"NULL")) continue;
                   /*empty slot*/
		lm = strlen(fil->oid);
                   /*get longest match length of current oid in acl file*/
		tlm = strlen(acl.oid);
                   /*get longest match length of oid beeing tested*/

		if(fil->action == 10){
                   /*denial oid  command*/
			if(!strcmp(fil->oid,"*")) lm10 = 1;
                   /*set longest match to smallest possible, because everything else that matches should have a higher match value*/
			if(!strncmp(fil->oid, acl.oid, lm))  lm10 = lm;
                    /*denial longest match found*/
		}	
		else if(fil->action == 13){
                    /*deny oid+g+s command*/
			if(!strcmp(fil->oid,"*")) lm13 = 1;
                    /*set longest match to smallest possible, because everything else that matches should have a higher match value*/
			if(!strncmp(fil->oid, acl.oid, lm) && fil->g == acl.g && fil->s == acl.s) lm13 = lm;
                     /*permitted longest match found*/
		}
		else if(fil->action == 20){
                     /*permit oid command*/
			if(!strncmp(fil->oid, acl.oid, lm))  lm20 = lm;
                     /*permitted longest match found*/
		}
		else if(fil->action == 23){
                      /*permit oid+g+s command*/
			if(!strcmp(fil->oid,"*")) lm23 = 1;
                      /*set longest match to smallest possible, because everything else that matches should have a higher match value*/
			if(!strncmp(fil->oid, acl.oid, lm) && fil->g == acl.g && fil->s == acl.s)  lm23 = lm;	/*permitted longest match found*/
		}
		l++;
	}

  if(lm23 == 0 && lm13 == 0) return 1;				/*no acl at all was found*/
  if(lm13 >= lm23) return 0;					/*deny oid+g+s was found, actual this is boolean, if they are equal aclist is wrong*/
  if(lm23) return 1;						/*permit oid+g+s was found, so deny oid+g+s (lm23) must be zero*/
  if(lm13 >= lm23) return 0;					/*deny oid is greater or equal than permit oid */
  return 1;							/*else trap is permited*/
}

GList *
load_acl(gchar *filename)
{				/*load event display access list*/
  char line[200],foo[200];
  FILE *fp;
  int i=0;
  aclist *acl;
  GList * gl;

  gl = NULL;
  
  if(!(fp = fopen(filename, "r"))) 
    return 0;

  while(fgets(line,200,fp))
    {	/*parse corr.conf*/
      if(line[0] == '#') continue;

      if (!(acl = (aclist *) g_malloc(sizeof(aclist))))
        {
          fprintf (stderr, "Can't alloc enough space for %s", filename);
          fclose(fp);
          return 0; /* memory lost */
        }

      wordcpy(foo,line,1);
      if(!strcmp(foo,"deny"))
	{
	  wordcpy(foo,line,2);
	  if(!strcmp(foo,"oid"))
	    {
	      acl->action = 10;
	      wordcpy(foo,line,3);
	      acl->oid = g_strdup(foo);
	    }
	  else if(!strcmp(foo,"oid+g+s"))
	    {
	      acl->action = 13;
	      wordcpy(foo,line,3);
	      acl->oid = g_strdup(foo);
	      wordcpy(foo,line,4);
	      acl->g = atoi(foo);
	      wordcpy(foo,line,5);
	      acl->s = atoi(foo);
	    }
	}
      else if(!strcmp(foo,"permit"))
	{
	  wordcpy(foo,line,2);
	  if(!strcmp(foo,"oid"))
	    {
	      acl->action = 20;
	      wordcpy(foo,line,3);
	      acl->oid = g_strdup(foo);
	    }
	  else if(!strcmp(foo,"oid+g+s"))
	    {
	      acl[i].action = 23;
	      wordcpy(foo,line,3);
	      acl->oid = g_strdup(foo);
	      wordcpy(foo,line,4);
	      acl->g = atoi(foo);
	      wordcpy(foo,line,5);
	      acl->s = atoi(foo);
	    }
	}
      i++;
      gl = g_list_append(gl, acl);
    }

  fclose(fp);
  return gl;
}
/**
 * save_acl: 
 * @filename: The filename that will hold the trapfilter acl.
 * Save the current trapfilter acl. Note: This will overwrite the @filename.
 *
 **/
int 
save_acl(GList *gl, gchar *filename)
{			
  gint     l=0;
  FILE    *fp;
  gchar    line[200];
  gint     line_size;
  aclist  *acl;

  fp = fopen(filename,"w");
  line_size = sizeof (line);
  while(gl)
    {
      acl = gl->data;
      if(!strcmp(acl->oid,"NULL")) 
	continue;
      switch(acl->action)
	{
	case 10:
	  g_snprintf(line, line_size, "deny oid %s\n", acl->oid);
	  break;
	case 13:
	  g_snprintf(line, line_size, "deny oid+g+s %s %d %d\n", 
		  acl->oid, acl->g, acl->s);
	  break;
	case 20:
	  g_snprintf(line, line_size, "permit oid %s\n", acl->oid);
	  break;
	case 23:
	  g_snprintf(line, line_size, "permit oid+g+s %s %d %d\n",
		   acl->oid, acl->g, acl->s);
	  break;
	default:			/*unsupported action*/
	  fprintf(stderr,"APP_ERR: unsupported action in save_acl()\n");
	  continue;
	}
      fputs(line,fp);
      gl = gl->next;
    }
  fclose(fp);
  return 0;
}
int 
acl_add_filter(aclist acl, char *filename)
{
  char   line[200];
  FILE   *fp;
  gint   line_size;
  
  if(!filename) 
    return 1;
  fp = fopen(filename,"a");
  line_size = sizeof (line);
  switch(acl.action)
    {
    case 10:
      g_snprintf(line, line_size,"deny oid %s\n",acl.oid);
      break;
    case 13:
      g_snprintf(line, line_size,"deny oid+g+s %s %d %d\n",acl.oid,acl.g,acl.s);
      break;
    case 20:
      g_snprintf(line, line_size, "permit oid %s\n",acl.oid);
      break;
    case 23:
      g_snprintf(line, line_size,"permit oid+g+s %s %d %d\n", acl.oid, 
	       acl.g,acl.s);
      break;
    default:                        /*unsupported action*/
      fprintf(stderr, 
	      "APP_ERR: unsupported action in acl_add_filter()\n");
      return 1;
    }
  fputs(line,fp);
  fclose(fp);
  return 0;
}
