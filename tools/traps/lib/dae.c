/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* dae.c -- various Event-handling-system functions
*
*/

/* event handling system, general library
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/un.h>
#include <netinet/in.h>   
#include <arpa/inet.h>
#include <unistd.h>

#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>


#include "g_snmp.h"

#include "dae.h"

char libdae_version[] = "$Id$"; /*usefull to 'strings' the binary*/

char * UNUSED_load_file(char *filename)
{
char *text;
FILE *fp;
struct stat statb;

	if(!filename || !*filename){
		fprintf(stderr,"Empty filename");
		return 0;
	}

	if (stat(filename, &statb) == -1 || 
	    (statb.st_mode & S_IFMT) != S_IFREG || 
	    !(fp = fopen(filename, "r")))
	  {
	    if((statb.st_mode & S_IFMT) == S_IFREG) 
	      perror(filename);
	    else 
	      fprintf(stderr,"%s: not a regular file\n",filename);
	    return 0;
	}
	if (!( statb.st_size )) 
	  { 
	    text = (char *) NULL;
	    fclose(fp); 
	    return NULL;
	  }
	if (!(text = (char *) g_malloc((unsigned)(statb.st_size+1))))
	  {
	    fprintf(stderr, "Can't allocate enough memory\n");
	    fclose(fp); 
	    return 0;
	  }
	if (!fread(text, sizeof(char), statb.st_size+1, fp))
	  {
	    fprintf(stderr, "Couldn't read file\n");
	    g_free(text);
	    fclose(fp);
	    return 0;
	  }
	text[statb.st_size] = 0;
	fclose(fp);
	return text;
}

/****************************************************************************
 * SNMP Functions
 ***************************************************************************/
/*
 * converts oid to a text:
 * .1.3.6.1   -->   .internet.dod.org... 
 */
gint 
oid2text(char *text, char *oidtext)
{
  gulong  name[SNMP_SIZE_OBJECTID];
  guint   name_len;
  
  name_len = SNMP_SIZE_OBJECTID;
//  text[0] = 0;
//  oidtext[0] = 0;
/*
  if (!read_objid(oidtext, name, &name_len))
    {
      return 1; 
    }
  sprint_objid(text, 200, name, name_len);
*/
  return 0;
}

/*
 * global functions
 */

gint load_environment(glb_enviroment * environment){
gchar line[200],entity[200],value[200];
gchar *env;
FILE *fp;


	env = g_getenv("DAE_BASE");
	if(!env) {
		fprintf(stderr,"Error: enviroment variable: DAE_BASE is not set.\n");
		exit(1); /*critical*/
	}

	environment->trapd_unknown_traps = 1;
	environment->trapd_log_unknown_traps = 1;
	environment->corrd_log = 1;
	environment->evdpysrvd_log = 1;
	environment->evdpysrvd_poll = 300;		/*default timeout/poll of event displays (in seconds) */
	environment->gxevents_max_traps = 100;		/*default number of traps gxevents will display before removing*/
	environment->gxevents_lookup = 1;			/*default don't lookup hostname from IP*/
        environment->popup_date = 0;			/*default to negative*/

        sprintf(line, "%s/conf/nb.conf", env);

	if(!(fp = fopen(line, "r"))) return 1;
while(fgets(line,200,fp)){
  if(line[0] == '#') continue;
  wordcpy(entity,line,1);		/* first check if it is a define statement */
  if(!strcmp(entity,"CORRD_LOG")) environment->corrd_log = 1;
  else if(!strcmp(entity,"TRAP_UNKNOWN_TRAPS")) environment->trapd_unknown_traps = 1;
  else if(!strcmp(entity,"TRAP_LOG_UNKNOWN_TRAPS")) environment->trapd_log_unknown_traps = 1;
  else if(!strcmp(entity,"EVDPYSRVD_LOG")) environment->evdpysrvd_log = 1;
  else if(!strcmp(entity,"POPUP_DATE"))  environment->popup_date = 1;
  else if(!strcmp(entity,"GXEVENTS_LOOKUP"))  environment->gxevents_lookup = 1;
  else {
    wordcpyt(entity,line,"=",1);
    wordcpyt(value,line,"=",2);
    restrap(value);
    if(!strcmp("DATABASE_HOST",entity)) environment->host = g_strdup(value);
    else if(!strcmp("DATABASE_NAME",entity)) environment->name = g_strdup(value);
    else if(!strcmp("DATABASE_USER",entity)) environment->user = g_strdup(value);
    else if(!strcmp("DATABASE_PASS",entity)) environment->passwd  = g_strdup(value);
    else if(!strcmp("BASE",entity))          environment->base  = g_strdup(value);
    else if(!strcmp("EVDPYSRVD_POLL",entity)) environment->evdpysrvd_poll = atoi(value);
    else if(!strcmp("TRAP_DEFAULT_SEVERITY",entity)) environment->severity = atoi(value);
    else if(!strcmp("POPUP_EXEC",entity))  environment->popup_exec = g_strdup(value);
    else if(!strcmp("GXEVENTS_MAX_TRAPS",entity))  environment->gxevents_max_traps = atoi(value);
  }
}

  fclose(fp);
  return 0;
}

GList *
load_trapconf(gchar *filename){
gint f;
GList * gl = NULL;
gchar *c;
g_trap *gtrap;
g_trap_context *ctx;
xmlDocPtr doc;
xmlNodePtr node;

  doc = xmlParseFile(filename);
  if(!doc)
    return NULL;

  if(!doc->root){
    xmlFreeDoc(doc);
    return NULL;
  }

  node = doc->root->childs;

  for(; node != NULL; node = node->next){
    if(!g_strncasecmp(node->name, "TRAPD", 4)) break;
  }
  if(!node) return NULL;

  for(node = node->childs; node != NULL; node = node->next){
    if(!(gtrap = g_malloc0(sizeof(g_trap)))) return NULL;
    if(!(ctx = g_malloc0(sizeof(g_trap_context)))) return NULL;
    gtrap->ctx = ctx;
    gtrap->oid = xmlGetProp(node, "oid");
    c          = xmlGetProp(node, "generic");
    f = atoi(c);
    gtrap->g = f;
    g_free(c);
    c          = xmlGetProp(node, "specific");
    f = atoi(c);
    gtrap->s = f;
    g_free(c);
    ctx->name   = xmlGetProp(node, "name");
    ctx->script = xmlGetProp(node, "script");
    ctx->popup  = xmlGetProp(node, "msg");
    c          = xmlGetProp(node, "flags");
    f = atoi(c);
    gtrap->flag = f;
    g_free(c);
    c          = xmlGetProp(node, "severity");
    f = atoi(c);
    gtrap->severity = f;
    g_free(c);

    gl = g_list_append(gl, gtrap);
  }
  xmlFreeDoc(doc);
  return gl;
}
