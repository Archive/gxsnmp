/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* g_trap_func.c -- SNMP trap functions
*
*/

/*various functions to 
  process a g_trap struct

*/

#include <glib.h>
#include <netinet/in.h>
#include <stdio.h>
#include "dae.h"

gint
g_trap_lookup(GList *gl, g_trap *trap)
{
gint i;
g_trap * traplist;

  while(gl){
    traplist = gl->data;
    if( !strcmp(traplist->oid,trap->oid) && 
        traplist->g == trap->g && 
        traplist->s == trap->s){
      trap->name = traplist->name;
      trap->ctx = traplist->ctx;
      trap->flag = traplist->flag;
      trap->severity = traplist->severity;
#if 0    //DEBUG CODE
fprintf(stderr,"RES(");
if(trap->name)
  fprintf(stderr,"-name->%s ",trap->name);
if(trap->ctx->popup)
  fprintf(stderr,"-popup->%s ",trap->ctx->popup);
if(trap->ctx->script)
  fprintf(stderr,"-script->%s ",trap->ctx->script);
fprintf(stderr,")\n");
#endif
      return 1;
    }
    gl = gl->next;
  }
  return 0;
}

gchar *
g_trap_arg_to_string(g_trap *trap)
{
  gchar buffer[2000], buf[1000];
  GList *gl;
  g_oid *goid;

  buffer[0] = 0;
  strcat(buffer, "ARG: ");
  gl = trap->args;
  while(gl){
      goid = gl->data;
      snprintf(buf, 1000, "Name: %s Data: %s ", goid->name, goid->data);
      strcat(buffer, buf);
      gl = gl->next;
  }
  return g_strdup(buffer);
}

