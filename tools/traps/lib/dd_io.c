/*  $Id$
 *
 * Copyright 2000
 * tcp_service.c -- create requests from socket
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  dd_io.c -- daemon to daemon I/O
 *
 * This file is based on database/server/tcp_service.c
 * Try replace tcp_service with this and dd_io2.c file
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>


/****************************************************************************
 * Forward references
 ***************************************************************************/
gboolean             dd_new_connection            (GIOChannel     *source,
						   GIOCondition   condition,
						   gpointer       data);
gboolean             dd_peer_activity             (GIOChannel     *source,
						   GIOCondition   condition,
                                                   gpointer       data);
/****************************************************************************
 * Implementation.
 ***************************************************************************/
void
dd_enable_reuseaddr (gint sock)
{
  gint tmp = 1;
  if (sock < 0)
    return;
  if (setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, (gchar *)&tmp, 
		  sizeof (tmp)) == -1)
    perror ("Bah! Bad setsockopt ()\n");
}

void 
dd_enable_nbio (gint fd)
{
  if (fcntl (fd, F_SETOWN, getpid()) == -1)
    perror ("fcntl (F_SETOWN) error\n");
/*  //FNDELAY NOT SUPPORTED BY SOLARIS
  if (fcntl (fd, F_SETFL, FNDELAY) == -1)
    perror ("fcntl (F_SETFL, FNDELAY\n");
*/
}

/*
*
*
*/

void
dd_connect_as_server (gint port, gpointer func,
                                 gpointer data)
{
static  gint                s;
static  struct sockaddr_in  addr;
static  GIOChannel          *channel;

  if (port <= 0)
    return ;

  s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (s == -1)
    return;
  dd_enable_reuseaddr (s);
  memset (&addr, 0, sizeof (addr));
  addr.sin_family       = AF_INET;
  addr.sin_port         = htons ((u_short)port);
  addr.sin_addr.s_addr  = INADDR_ANY;
  if (bind (s, (struct sockaddr *)&addr, sizeof (addr)) == -1)
    {
      close (s);
      return ;
    }
  if (listen (s, 5) == -1)
    {
      close (s);
      return ;
    }
  channel = g_io_channel_unix_new (s);
  g_io_add_watch (channel, G_IO_IN, dd_new_connection, func);
}

GIOChannel *
dd_connect_as_client (gchar *address, gint port, 
		      void (* activity_func)(), gpointer data)
{
static  gint     sock;
static  struct sockaddr_in ddserver;   /* ick */
static  GIOChannel          *channel;
  
  sock = socket (PF_INET, SOCK_STREAM, 0);
  if (sock < 0)
    return -1;

  ddserver.sin_family = AF_INET;
  ddserver.sin_port   = htons(port);
  ddserver.sin_addr.s_addr = inet_addr (address);
  if (0 > connect (sock, (struct sockaddr *) &ddserver, sizeof (ddserver)))
    return -1;
  fcntl(sock, F_SETFL, O_NONBLOCK);

  channel = g_io_channel_unix_new (sock);
  g_io_add_watch (channel, G_IO_IN, dd_peer_activity, activity_func);

  return channel;
}

/****************************************************************************
 * Callback functions
 ***************************************************************************/
/**
 * dd_new_connection 
 * Will accept a new connection and add the client to the list of clients.
 * actual client communication is handled elsewhere.
 **/
gboolean 
dd_new_connection (GIOChannel *source, GIOCondition cond, gpointer data)
{
  gint new;              /* new socket descriptor */
  gint               client;
  GIOChannel *new_channel;
static  struct sockaddr_in client_addr;
static  void (* user_func)();
static  gint fd;
  
  user_func = data;


  if (cond == G_IO_IN)
    {
      fd = g_io_channel_unix_get_fd(source);
      if ( (new = accept (fd,
			  (struct sockaddr *)&client_addr, &client)) < 0)
        {
          fprintf(stderr,"errno(%u)\n", errno);
	  g_warning ("Unable to accept new connection.");
	  return FALSE;
        }
      new_channel = g_io_channel_unix_new (new);

      /**** Connection Register Callback ****/
      user_func (0, new_channel, 0);
      /******************************/
      g_io_add_watch (new_channel, G_IO_IN, dd_peer_activity, data);
    }
  return TRUE;
}

/**
 * dd_peer_activity
 * Handles input from the clients and passes it off to the parser/dispatcher.
 * This will get the raw data from the socket, make sure there is a NULL 
 * terminator, and call the command dispatcher.
 **/
gboolean 
dd_peer_activity (GIOChannel *source, GIOCondition cond, 
                     gpointer data)
{
  guint num_read = 0;
  gchar bufme[40000];
  void (* user_func)();

  user_func = data;

  if (cond == G_IO_IN)
    {
      switch(g_io_channel_read (source, bufme, sizeof (bufme), &num_read))
        {
          case G_IO_ERROR_NONE:
            if(!num_read){
              user_func (2, source, 0);
              return FALSE;
            }
            bufme[num_read] = '\0';           /* Make sure its null terminated */
            user_func (1, source, bufme);
            return TRUE;
          default:
            fprintf(stderr,"dd_io.c: G_IO_ERROR\n");
        }
    }
  else
    {
      fprintf(stderr, "dd_io.c: UNKNOWN CONDITION\n");
    }
  return TRUE;
}

/**
 * dd_server_peer_disconnect()
 * Called when A client disappears, should remove it from the list and
 * go on with life.
 *
 * go through transaction request list, and remove read-only actions,
 * from this source
 *
 * remove this user from profile list
 *
 **/
gboolean
dd_server_peer_disconnect (GIOChannel *source, GIOCondition cond, gpointer data)
{
  GList *gl;
#if 0
  gl = tranrec;
  while(gl)
    {
      if( ((tranrec *)gl->data)->source == source){

        /*ok check if request is stale.*/

        if( ((tranrec *)gl->data)->action == ROW_READ   ||
            ((tranrec *)gl->data)->action == TABLE_LOAD    )
          {
            tranrec = g_list_remove(gl);
          }
      }
      gl->next;
    }
#endif
  return TRUE;
}
