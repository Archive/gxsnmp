/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* dae.h -- process incoming SNMP TRAP-PDUs
*
*/

#include <netinet/in.h>

#include "trap-io.h"
#include "g_strsub.h"

#if 0

typedef struct root_oids_ { /*is this obsolete?*/
	char name[40];
	char prefix[60];
	int dii;
} oid_slot;

typedef struct _trap_slot{	/*this struct will in future only contain trap.conf fields*/
	gchar *oid;		/*OID*/
	int g;			/*generic*/
	int s;			/*specific*/
	gchar *name;		/*trap name */
	gchar *script;		/*script to run*/
	gchar *popup;		/*text to pop up*/
	int eid;		/*event ID to run*/
	int flag;		/*this is used for setting/retrieving single bit fields*/
	int severity;		/*set severity according to oid+g+s in trap.conf*/
} trap_slot;

#endif

typedef struct _trap_{	/*this field should be used to hold traps */
        int timestamp;          /*date trap arrive*/
        char date[30];          /*date trap arrive*/
        char IP[60];            /*system name*/
        char oid[130];          /*OID*/
        char noid[200];         /*text OID*/
        int g;                  /*generic*/
        int s;                  /*specific*/
        char name[80];          /*trap name */
        char script[80];        /*script to run*/
        char popup[80];         /*text to pop up*/
        int eid;                /*event ID to run*/
        int flag;               /*this is used for setting/retrieving single bit fields*/
        int severity;           /*set severity according to oid+g+s in trap.conf*/
        char argoid[3][130];	/*due to this big array, don't alloc to much of this struct type :) */
        char argdat[3][200];	/*due to this big array, don't alloc to much of this struct type :) */
} trapslot;

typedef struct _g_trap_context {
  gchar *name;          /* FUTURE: trap group name */
  gchar *script;        /* script to run */
  gchar *popup;         /* text to pop up */
} g_trap_context;

typedef struct _g_oid {
  gint type;
  gpointer name;
  gpointer data;
} g_oid;

typedef struct _g_trap{
  gint timestamp;         /*date trap arrive*/
  gint severity;           /*set severity according to oid+g+s in trap.conf*/
  gchar *date;            /*date trap arrive*/
  gchar *address;         /*device network address */
  gchar *name;            /* name of this trap (from trap.conf) */
  gchar *oid;             /*OID*/
  gchar *noid;            /*text OID*/
  gint g;                 /*generic*/
  gint s;                 /*specific*/
  gint flag;              /* trap process state information */
  GList *args;
  g_trap_context *ctx;
} g_trap;

#if 0
typedef struct _corr_slot{
	gchar *mask;			/*IP-mask*/
	gchar *file;			/*correlation file to run*/
}corr_slot;
#endif

typedef struct {
	gchar *host;			/* database host*/
	gchar *name;			/* database name*/
	gchar *user;			/* database user*/
	gchar *passwd;			/* database pass*/
	gchar *base;			/* dirpath*/
	GList *traptab;			/* OLD loaded trap.conf pointer*/
        GList *trapconf;                /* NEW loaded trap.conf list */
	gchar *popup_exec;		/* executable to use when popping up a message*/
	int corrd_log;
	int evdpysrvd_log;
	int trapd_unknown_traps;
	int trapd_log_unknown_traps;
	int evdpysrvd_poll;
	int severity;
	int gxevents_max_traps;
	int gxevents_lookup;        /*lookup hostname from IP*/
	int popup_date;			/*boolean, should Popups contain a date*/
}glb_enviroment;


/*
access list action codes:
10	deny oid
13	deny oid+g+s
20	permit oid
23	permit oid+g+s
*/

typedef struct _aclist{
	int action;
	gchar * oid;
	gchar * ip;
	gchar * ipm;
	int severity;
	int g;
	int s;
}aclist;


/******************
 *** Prototypes ***
 ******************/

gchar      * g_trap_arg_to_string (g_trap *trap);
gint         g_trap_lookup(GList *gl, g_trap *trap);
int          acl_add_filter       (aclist acl, char *filename);
char       * wordcpy              (char *dst, char *src, int w);
gchar      * var_interpret2       (gchar *src, g_trap *trap);
gint         var_interpret        (gchar *dst, gchar *src, int max, 
                                   g_trap *trap);
GList      * load_acl             (gchar *filename);
int          test_acl             (aclist acl, GList *gl);
gint         load_environment     (glb_enviroment * environment);
GList      * load_trapconf        (gchar *filename);

GIOChannel * dd_connect_as_client (gchar *address, gint port,
                                   void (* activity_func)(), gpointer data);
void         dd_connect_as_server (gint port, gpointer func,
                                   gpointer data);

void         g_trap_free          (g_trap *trap);
gint          g_trap_decode        (trappdu *tpdu, g_trap *trap);
gint          oid2text             (char *text, char *oidtext);

gint         tpdu_read            (gint sock, trappdu *tpdu);
gint         tpdu_write           (gint sock, gchar *dest, trappdu *tpdu);

FILE       * logopen              (gchar *filename);
void         logstr               (FILE *fp, gchar *text);

