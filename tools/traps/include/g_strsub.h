#include <glib.h>

typedef struct _g_strsub {
  gchar *name;
  gpointer *text;
} g_strsub;

   GList    * g_strsub_list_add    (GList *gl, gchar *name, gpointer *text);
   gint       g_strsub_list_remove (GList *gl, gchar *name);
   gpointer   g_strsub_lookup      (GList *gl, gchar *name);
   gchar    * g_strsub_substitute  (GList *gl, gchar *intext);

