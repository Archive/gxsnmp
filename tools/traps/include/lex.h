/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* lex.h 
*
*/

#define LEX_PARSE_ERROR -1



typedef struct _lex_symtab {
        gchar name[40];         /* name of token */
        guint token;            /* Token Identifier */
        guint num;              /* number of additional data to read with this symbol */
        guint type;             /* if nodata == 1, this is the type of data */
}lex_symtab;

static const GScannerConfig     gscanner_config =
{
(
    " \t\n"
    )                    /* cset_skip_characters */,
   (
    G_CSET_a_2_z
    "_"
    G_CSET_A_2_Z
    )                    /* cset_identifier_first */,
   (
    G_CSET_a_2_z
    "_-0123456789"
    G_CSET_A_2_Z
    )                    /* cset_identifier_nth */,
   ( "#\n" )             /* cpair_comment_single */,

   TRUE                  /* case_sensitive */,

   TRUE                  /* skip_comment_multi */,
   TRUE                  /* skip_comment_single */,
   TRUE                  /* scan_comment_multi */,
   TRUE                  /* scan_identifier */,
   FALSE                 /* scan_identifier_1char */,
   FALSE                 /* scan_identifier_NULL */,
   TRUE                  /* scan_symbols */,
   TRUE                  /* scan_binary */,
   TRUE                  /* scan_octal */,
   TRUE                  /* scan_float */,
   TRUE                  /* scan_hex */,
   TRUE                  /* scan_hex_dollar */,
   TRUE                  /* scan_string_sq */,
   TRUE                  /* scan_string_dq */,
   TRUE                  /* numbers_2_int */,
   FALSE                 /* int_2_float */,
   FALSE                 /* identifier_2_string */,
   TRUE                  /* char_2_token */,
   TRUE                  /* symbol_2_token */,
   FALSE                 /* scope_0_fallback */,
 };


typedef struct _lexrunst{       /* Lex running state information */
  GScanner *scanner;
  FILE *fp;
  gint known;                             /* if token is Identified using symtab, TRUE*/
  gchar u_entity[200];                    /* uknown entitys saved here */
  guint entity;
  gchar value_str[200];
  gint value_int;
  guint numread;                          /* number of data returned in this struct */
  guint datatype;                         /* type of data */
  gint r;				/*recursive block counter */
} lexrunst;

