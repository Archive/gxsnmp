/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* trap-io.h -- process incoming SNMP TRAP-PDUs
*
*/


/*****
** PROGRAM IDENTIFIER
******/

#define GXSNMP_PROGRAM_CORRD      1
#define GXSNMP_PROGRAM_TRAPD      2
#define GXSNMP_PROGRAM_EVDPYSRVD  3
#define GXSNMP_PROGRAM_GXEVENTS   4

/* interprocess communication definitions
 * 
 *
 */

#define PDU_DPY_OPEN              1
#define PDU_DPY_CLOSE             2
#define PDU_PROGRAM_STATUS        3
#define PDU_PROGRAM_STATUS_REPLY  4
#define PDU_PROGRAM_START         5
#define PDU_PROGRAM_STOP          6
#define PDU_PROGRAM_RELOAD        7
#define PDU_PROGRAM_REGISTER      8
#define PDU_TRAP                  9
#define PDU_NETMON                10
#define PDU_CMD_EVDPYSRVD_PRINT_CONNECTED 11
#define PDU_POLL                 12
#define PDU_POLL_REPLY           13
#define PDU_EVENTDPY_MESSAGE_SHORT 14

#define NETMON_ALIVE      1
#define NETMON_DEAD       2

typedef struct _trappdu {
  gint type;     /* see above */
  gint severity;
  guint time;
  struct sockaddr_in address;
  guint generic;
  guint specific;
  gint  oidlen;
  gchar oid[130];
  gchar argbuf[4000];
} trappdu;
