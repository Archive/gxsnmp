/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* pdud.c -- SNMP PDUD forwarder
*
*/

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <syslog.h>
#include <errno.h>
#include <unistd.h>
#include <sys/un.h>

#include <glib.h>
#include <ucd-snmp-includes.h>

#include "dae.h"

#ifndef BSD4_3

#ifndef NFDBITS
typedef long	fd_mask;
#define NFDBITS	(sizeof(fd_mask) * NBBY)	/* bits per mask */

#define	FD_SET(n, p)	((p)->fds_bits[(n)/NFDBITS] |= (1 << ((n) % NFDBITS)))
#define	FD_CLR(n, p)	((p)->fds_bits[(n)/NFDBITS] &= ~(1 << ((n) % NFDBITS)))
#define	FD_ISSET(n, p)	((p)->fds_bits[(n)/NFDBITS] & (1 << ((n) % NFDBITS)))
#define FD_ZERO(p)	bzero((char *)(p), sizeof(*(p)))
#endif

#endif

char pdud_version[] = "$Id$"; /*usefull to 'strings' the binary*/

aclist trapfilter;
glb_enviroment enviroment;
int pdudsock;

char buffer[2000];
int pz;			//buffer pointer;
struct {
  unsigned char type;
  unsigned int len;
}block;

struct _trapinfo{
  char IP[20];
  char ts[80];
  char enterprise[200];
  int g;
  int s;
  ulong uptime;
  int argc;
  int argv[3][200];
} trap;

extern int  errno;
int snmp_input(int op, struct snmp_session *session, int reqid, struct snmp_pdu *pdu, void *magic){
char *ptr;
guint i, a, u, len, v;
struct variable_list *vars;
time_t ts; 	/*timestamp*/
struct sockaddr_in *aip;

gchar strbuf[2000];

typedef struct _trapbuf {
  gint type;
  gint severity;
  guint time;
  struct sockaddr_in address;
  guint generic;
  guint specific;
  gint  oidlen;
  gchar oid[130];
  gchar argbuf[10000];
} trapbuf;

trapbuf gtrap;


  
  



  if (op == RECEIVED_MESSAGE && pdu->command == SNMP_MSG_TRAP){
/* parse buffer */
    trap.enterprise[0] = 0;
    for (i = 0; i < pdu->enterprise_length; i++){
      sprintf(&trap.enterprise[strlen(trap.enterprise)], ".%d", pdu->enterprise[i]);
    }

        u = PDU_TRAP;
        gtrap.type = PDU_TRAP;

        /*gtrap.type = severity; */       /* severity */

	ts = time(NULL);
        gtrap.time = ts;
        aip = (struct sockaddr_in *)&pdu->agent_addr;
	memcpy(&(gtrap.address), (char *)&aip->sin_addr, 4);	/* IP */
        gtrap.generic = pdu->trap_type;
        gtrap.specific= pdu->specific_type;
        gtrap.oidlen = strlen(trap.enterprise);
	memcpy(&(gtrap.oid), &(trap.enterprise),130);	/* OID */

/* all variables in trap */

        a = 0;
	for (trap.argc = 0, vars = pdu->variables;  vars;  trap.argc++, vars = vars->next_variable){
		trap.enterprise[0] = 0;
		for (i = 0; i < vars->name_length; i++){	/*variable OID*/
			sprintf(&trap.enterprise[strlen(trap.enterprise)], ".%d", vars->name[i]);
		}
		
		switch(vars->type){			/*variable value*/
		case ASN_INTEGER:

			add_asciiz(&a, gtrap.argbuf, trap.enterprise); 
			add_guint(&a, gtrap.argbuf, *(vars->val.integer));

		break;
		case ASN_OCTET_STR:

			add_asciiz(&a, gtrap.argbuf, trap.enterprise);
			g_snprintf(strbuf, vars->val_len, "%s", vars->val.string);
			add_asciiz(&a, gtrap.argbuf, strbuf);

		break;

/*case ASN_OBJECT_ID: .. vars->val.objid .. vars->val_len*/
/*case IPADDRESS: .. bcopy(vars->val.string, &sin_addr.s_addr, sizeof(sin_addr.s_addr));*/
		}

	}

/*forward trap to trapd*/

/*set end of block marker must not colide with add_asciiz/guint PDU defines */
        len = a;
        a = 255;
        memcpy(gtrap.argbuf+len, &a, sizeof(gint));
	len = sizeof(trapbuf);
fprintf(stderr,"PDUD sending %u bytes.\n", len);
	senddsocket(pdudsock, enviroment.trapd, gtrap, len);
    }
   if (op == TIMED_OUT)
    {
      fprintf(stderr,"Timeout: This shouldn't happen!\n");
    }
  return 0;
}

int main()
{
struct snmp_session session, *ss;
int count, numfds, block;
fd_set fdset;
struct timeval timeout, *tvp;
int i,j;


  load_enviroment();
  unlink(enviroment.pdud);
  if(!(pdudsock = createsocket(enviroment.pdud,1))) exit(0);

/*daemonize*/
#if 0 
  if(fork() != 0) _exit(0);
  if(setsid() == -1) _exit(0);
  if(fork() != 0) _exit(0);
  umask(777);
  if(chdir("/")<0) _exit(0);
  for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
  open("/dev/null",O_RDWR); dup(0); dup(0);
#endif

  memset((char *)&session, '\0', sizeof(struct snmp_session));
  /*bzero((char *)&session,sizeof(struct snmp_session));*/
  session.authenticator = NULL;
  session.callback = snmp_input;
  session.callback_magic = NULL;
  session.community = NULL;
  session.community_len = 0;
  session.local_port = SNMP_TRAP_PORT;
  session.peername = NULL;
  session.retries = SNMP_DEFAULT_RETRIES;
  session.timeout = SNMP_DEFAULT_TIMEOUT;
  ss = snmp_open(&session);
  if(ss == NULL){  
    fprintf(stderr,"Couldn't initialize SNMP session, port buzy?\n");
    _exit(-1);
  }
  while(1){
fprintf(stderr,".");
    numfds = 0;
    FD_ZERO(&fdset);
    block = 1;
    tvp = &timeout;
    timerclear(tvp);
    snmp_select_info(&numfds, &fdset, tvp, &block);
    //if (block == 1) tvp = NULL;	/* block without timeout */
    count = select(numfds, &fdset, 0, 0, tvp);
    if (count > 0)
      snmp_read(&fdset);
      /*snmp_sess_read(session, &fdset);  thought single session API supported this*/
    else switch(count){
/*FIX: log, and try to restart daemon*/
      case 0:
fprintf(stderr,",");
        snmp_timeout();
        break;
      case -1:
        if (errno == EINTR) continue;
        else perror("select");
        return -1;
      default:
        printf("select returned %d\n", count);
        return -1;
    }
  }
    /* not reached */
}
