/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* util.c -- utility functions
*
*/

#include <gnome.h>
#include <stdio.h>
#include "dae.h"

extern glb_enviroment enviroment;
extern GtkCList *oidlist;
extern GtkCList *traplist;
extern char *oidtext[4];
extern int trapselfill;

void
list_reload(GList *gl){
g_oid *toid;

  gtk_clist_freeze(oidlist);                                    /*reload oidlist*/
  gtk_clist_clear(oidlist);

  while(gl){
    toid = gl->data;
    oidtext[0] = toid->oid;
    gtk_clist_append(oidlist, oidtext);
    gl = gl->next;
  }
  gtk_clist_thaw(oidlist);
  gtk_clist_clear(traplist);    /*this version clears trap list whenever deleting*/
  trapselfill = 0;
}

GList *
trapconf_load()
{
char line[200],foo[200];
FILE *fp;
int i=0,f;
gchar *trapconf_file;
gchar *oid;
GList *tgl;
trap_slot *trap;
trap_oid *toid;
trap_entry *entry;

  trapconf_file = g_strdup_printf("%s/conf/trap.conf", enviroment.base);
  tgl = NULL;

  if(!(fp = fopen(trapconf_file, "r"))) return 1;

  while(fgets(line,200,fp)){
    if(line[0] == '#') continue;            /*skip remarks in trap.conf*/
    entry = g_malloc(sizeof(trap_entry));

    oid = g_strdup(wordcpy(foo, line, 1)); /* OID */

    wordcpy(foo,line,2);                    /*generic*/
    f = atoi(foo);
    entry->g = f;

    wordcpy(foo,line,3);                    /*specific*/
    f = atoi(foo);
    entry->s = f;

    entry->name = g_strdup(wordcpy(foo, line, 4)); /* Name */
    entry->script = g_strdup(wordcpy(foo, line, 5)); /* Script */
    entry->popup = g_strdup(wordcpy(foo, line, 6)); /* Popup */

    wordcpy(foo,line,7);                    /*EID*/
    f = atoi(foo);
    entry->eid = f;

    wordcpy(foo,line,8);                    /*Flags*/
    f = atoi(foo);
    entry->flag = f;

    wordcpy(foo,line,9);                    /*Severity*/
    f = atoi(foo);
    entry->severity = f;

    if(!(toid = get_trap_slot(tgl, oid))){ /*entry doesn't exists */
      toid = g_malloc(sizeof(trap_oid));
      toid->oid = oid;
      tgl = g_list_append(tgl, toid);
    }

    toid->entrys = g_list_append(toid->entrys, entry);
  }

  fclose(fp);
  g_free(trapconf_file);
  return tgl;
}

GList *get_trap_slot(GList *trapoid, gchar *oid)
{
  trap_oid   *toid;

  while(trapoid){
    toid = trapoid->data;
    if(!strcmp(toid->oid, oid)) return toid;
    trapoid = trapoid->next;
  }
  return 0;

}
