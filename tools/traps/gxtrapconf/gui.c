/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* gui.c -- GUI wrapper
*
*/

#include <gnome.h>

GtkWidget *gtkcreate_query(GtkWidget *table, char *text,int type, int x, int y, void *callback, void *data, int data2 ){ /*this function creates a checkbutton or a textinput .. with label and callbacks */
GtkWidget *lab;
GtkWidget *wid;

  lab = gtk_label_new(text);
  gtk_table_attach (GTK_TABLE (table), lab, 1, 2, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
  gtk_widget_show(lab);
  if(type == 1){
    wid = gtk_entry_new_with_max_length(data2);
    gtk_signal_connect(GTK_OBJECT(wid), "activate", GTK_SIGNAL_FUNC(callback), wid);
    gtk_table_attach(GTK_TABLE (table), wid, 2, 3, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
    gtk_entry_set_text(wid,data);
    gtk_widget_show(wid);
    return wid;
  }else if (type == 2){
    wid = gtk_toggle_button_new();
    gtk_table_attach (GTK_TABLE (table), wid, 2, 3, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
    gtk_signal_connect_object(GTK_OBJECT(wid), "clicked", GTK_SIGNAL_FUNC(callback),GTK_OBJECT(wid));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wid), data2);
    gtk_widget_show(wid);
  }else if (type == 3){
    wid = gtk_button_new_with_label(text);
    //gtk_object_set_data (GTK_OBJECT(table), "foo", wid);
    gtk_table_attach (GTK_TABLE (table), wid, x, y, x+1, y+1, GTK_SHRINK , GTK_SHRINK, 2, 1);
    gtk_signal_connect(GTK_OBJECT(wid), "clicked",GTK_SIGNAL_FUNC(callback),(gpointer) "savecfg");
  }
}

