/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * gxtrapconf.c -- file front end
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stddef.h>	//six row downs is needed by socket_functions
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>

#include <gnome.h>
#include "config.h"
#include <sys/types.h>
#include "events.h"

#include "sys/stat.h"
#include "dae.h"

char version[] = "$Id$"; /*usefull to 'strings' the binary*/

aclist *trapfilter; /*unused, but needed by libdae*/
void quit_cb();

int get_line_cnt, get_line_base;
glb_enviroment enviroment;

int trapselfill = 0;		/*trapsel structure is filled*/
trap_slot trapsel;		/*this is the current selected trap*/
GtkCList *oidlist;
GtkCList *traplist;

GtkWidget *mod_win;		/*modify window*/
GtkWidget *mod_vbox;
GtkWidget *mod_table;
GtkWidget *mod_name;
GtkWidget *mod_popup;		/*modify popup text*/
GtkWidget *mod_script;		/*modify script text*/
GtkWidget *mod_flag1;		/*flag bit 1 - forward to corrd*/
GtkWidget *mod_flag2;		/*flag bit 2 - forward to corrd*/
GtkWidget *mod_flag3;		/*flag bit 3 - forward to corrd*/

char *oidtext[4];		/*number is number of columns in oidlist*/
char *traptext[4];		/*number is number of columns in traplist*/
char text1[80];			/*holds some text*/
char text2[80];
char text3[80];
char text4[80];
char text5[80];

/* Prototypes */
void             quit_cb         ();
void             buttoncb        (GtkWidget *);
void             oidselect_cb    (GtkWidget *, gint, gint, GdkEventButton, gpointer);
void             trapselect_cb   (GtkWidget *, gint, gint, GdkEventButton, gpointer);
void             add_trap_cb     ();
void             del_trap_cb     ();
void             modify_trap_cb  ();
void             mod_bit1_cb     ();
void             mod_bit2_cb     ();
void             mod_bit3_cb     ();
void             mod_cancel_cb   ();
void             mod_ok_cb       ();
/**************/

int main(int argc, char **argv)
{
  GtkWidget   *config;
  GtkWidget   *button;
  GtkWidget   *vbox;
  GtkWidget   *bar;
  GtkWidget   *table;
  GtkWidget   *oidwindow;
  GtkWidget   *trapwindow;
  int          n,modn=0,i, tmp, w_size;
  gint        char_size;

  if(load_enviroment()){
    fprintf(stderr,"gxdaeconf: Error, couldn't load enviromental file.\n");
    exit(0);
  }
//  load_trapconf();	/*load trap definition file*/
  enviroment.trapconf = trapconf_load();

/* check arguments if exist set as selected item and go into modify mode */
  if(argc == 4){
    strcpy(trapsel.oid, argv[1]);
    trapsel.g = atoi(argv[2]);
    trapsel.s = atoi(argv[3]);
    trapsel.name[0] = 0;						/*set empty to check if trapsel.oid/g/s exists in trap.conf*/
fprintf(stderr,"!");
    for(n=0;strlen(enviroment.traptab[n].oid);n++){
      if(strcmp(trapsel.oid,enviroment.traptab[n].oid)) continue;
      if(trapsel.g != enviroment.traptab[n].g) continue;
      if(trapsel.s != enviroment.traptab[n].s) continue;
      strcpy(trapsel.name,enviroment.traptab[n].name);                   /*fill in rest of selected trap data*/
      strcpy(trapsel.popup,enviroment.traptab[n].popup);
      strcpy(trapsel.script,enviroment.traptab[n].script);
      trapsel.flag = enviroment.traptab[n].flag;
    }
    if(strlen(trapsel.name)) modn++;	/*if trap specified by arguments was found in trap.conf lets go into modify mode*/
  }

  gtk_init(&argc, &argv);
  config = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(config), vbox);
  gtk_widget_show(vbox);

  table = gtk_table_new (3, 3, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 3);
  tmp = gdk_text_width (vbox->style->font, "1", 1);
  tmp = tmp * 18;
  w_size = 0;
  char_size = gdk_string_width (table->style->font, "xW") / 2;

  button = gtk_button_new_with_label (_("Save, reload & quit"));
  gtk_object_set_data (GTK_OBJECT (config), "ack_button", button);
  gtk_table_attach (GTK_TABLE (table), button, 0, 1, 0, 1, GTK_FILL | GTK_SHRINK, GTK_SHRINK, 1, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(quit_cb),(gpointer) "savecfg");
  button = gtk_button_new_with_label (_("Dismiss"));
  gtk_object_set_data (GTK_OBJECT (config), "ack_button", button);
  gtk_table_attach (GTK_TABLE (table), button, 1, 2, 0, 1, GTK_FILL | GTK_SHRINK, GTK_SHRINK, 1, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(quit_cb),(gpointer) "savecfg");

  bar = gtk_hseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 0, 3, 1, 2, GTK_FILL, GTK_FILL | GTK_EXPAND, 2, 1);
  bar = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 0, 1, 0, 7, GTK_FILL, GTK_FILL | GTK_EXPAND, 2, 1);

  gtkcreate_query(table, "Add", 3, 1, 2, add_trap_cb, 0, 0);
  gtkcreate_query(table, "Modify", 3, 1, 3, modify_trap_cb, 0, 0);
  gtkcreate_query(table, "Delete", 3, 1, 4, del_trap_cb, 0, 0);

  oidlist = gtk_clist_new(1);
  gtk_signal_connect(GTK_OBJECT(oidlist), "select_row",GTK_SIGNAL_FUNC(oidselect_cb),NULL);
  gtk_clist_set_selection_mode(oidlist,GTK_SELECTION_SINGLE);
  gtk_clist_column_titles_hide(oidlist);
  gtk_clist_set_column_width(oidlist,0,200);

  oidwindow = gtk_scrolled_window_new(0, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (oidwindow),  GTK_POLICY_ALWAYS,  GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (oidwindow), oidlist);
  gtk_table_attach (GTK_TABLE (table), oidwindow, 0, 1, 3, 5, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 2, 1);

  traplist = gtk_clist_new(2);
  gtk_signal_connect(GTK_OBJECT(traplist), "select_row",GTK_SIGNAL_FUNC(trapselect_cb),NULL);
  gtk_clist_set_selection_mode(traplist,GTK_SELECTION_SINGLE);
  gtk_clist_column_titles_hide(traplist);
  gtk_clist_set_column_width(traplist,0,100);
  gtk_clist_set_column_width(traplist,1,100);

  trapwindow = gtk_scrolled_window_new(0, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(trapwindow),  GTK_POLICY_ALWAYS,  GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (trapwindow), traplist);
  gtk_table_attach (GTK_TABLE (table), trapwindow, 0, 1, 5, 7, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 2, 1);

  list_reload(enviroment.trapconf);
/***********create modify window************/
  mod_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  mod_vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(mod_win), mod_vbox);
  gtk_widget_show(mod_vbox);

  mod_table = gtk_table_new (3, 6, FALSE);
  gtk_box_pack_start (GTK_BOX (mod_vbox), mod_table, TRUE, TRUE, 3);

  mod_name = gtkcreate_query(mod_table, "Name", 1, 0, 0, quit_cb, 0, 0);
  mod_popup = gtkcreate_query(mod_table, "Popup", 1, 0, 1, quit_cb, 0, 0);
  mod_script = gtkcreate_query(mod_table, "Command", 1, 0, 2, quit_cb, 0, 0);

  mod_flag1 = gtkcreate_query(mod_table, "Correlate", 2, 0, 3, mod_bit1_cb, 0, 0);
  mod_flag2 = gtkcreate_query(mod_table, "Display", 2, 0, 4, mod_bit2_cb, 0, 0);
  mod_flag3 = gtkcreate_query(mod_table, "Log trap", 2, 0, 5, mod_bit3_cb, 0, 0);

  gtkcreate_query(mod_table, "Ok", 3, 0, 6, mod_ok_cb, 0, 0);
  gtkcreate_query(mod_table, "Cancel", 3, 5, 10, mod_cancel_cb, 0, 0);

/*******************************************/
  gtk_widget_show_all (config);
  if(modn){  /*command line mode, modify trap window*/
    trapselfill = 1;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag1), trapsel.flag & 1);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag2), trapsel.flag & 2);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag3), trapsel.flag & 4);
    modify_trap_cb();
  }
  gtk_main();
}
