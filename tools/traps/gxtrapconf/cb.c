/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* cb.c -- GUI callback code
*
*/

/* interim place for
GUI callback code */

#include <gnome.h>
#include "dae.h"
#include "trap.h"

extern glb_enviroment enviroment;
extern GtkCList *oidlist;
extern GtkCList *traplist;
extern char *oidtext[4];
extern int trapselfill;
extern trap_slot trapsel;
extern char *traptext[4];              /*number is number of columns in traplist*/

extern char text1[80];                 /*holds some text*/
extern char text2[80];
extern char text3[80];
extern char text4[80];
extern char text5[80];

extern GtkWidget *mod_win;             /*modify window*/
extern GtkWidget *mod_vbox;
extern GtkWidget *mod_table;
extern GtkWidget *mod_name;
extern GtkWidget *mod_popup;           /*modify popup text*/
extern GtkWidget *mod_script;          /*modify script text*/
extern GtkWidget *mod_flag1;           /*flag bit 1 - forward to corrd*/
extern GtkWidget *mod_flag2;           /*flag bit 2 - forward to corrd*/
extern GtkWidget *mod_flag3;           /*flag bit 3 - forward to corrd*/




void quit_cb(){
  exit(0);
}

void buttoncb(GtkWidget *wid){
/*
  if(GTK_TOGGLE_BUTTON(wid)->active){
    cfg.utrap = 1;
  } else {
    cfg.utrap = 0;
  }
fprintf(stderr,"LUT(%d)\n",cfg.utrap);
*/
}

void oidselect_cb(GtkWidget *oidlist, gint row, gint column, GdkEventButton *event, gpointer data){
gchar *oid;
int a;
GList *tgl, *egl;
trap_oid *toid;
trap_entry *entry;

  gtk_clist_get_text(GTK_CLIST(oidlist), row, column, &oid);
  gtk_clist_freeze(traplist);
  gtk_clist_clear(traplist);
  trapsel.oid = oid;

  if(toid = get_trap_slot(enviroment.trapconf, trapsel.oid)){
    egl = toid->entrys;
    while(egl){
      entry = egl->data;
      sprintf(text1,"%d",entry->g);
      sprintf(text2,"%d",entry->s);
      traptext[0] = text1;
      traptext[1] = text2;
      gtk_clist_append(traplist,traptext);
      egl = egl->next;
    }
  }

  gtk_clist_thaw(traplist);
  trapsel.g = 0;
  trapsel.s = 0;
  trapselfill = 0;
}

void trapselect_cb(GtkWidget *oidlist, gint row, gint column, GdkEventButton *event, gpointer data){
gchar *txt;
int n;
GList *gl;
trap_slot *trap;
GList *tgl, *egl;
trap_oid   *toid;
trap_entry *entry;

  gtk_clist_get_text(GTK_CLIST(oidlist), row, 0, &txt);
  trapsel.g = atoi(txt);
  gtk_clist_get_text(GTK_CLIST(oidlist), row, 1, &txt);
  trapsel.s = atoi(txt);

  if(toid = get_trap_slot(enviroment.trapconf, trapsel.oid)){
    egl = toid->entrys;
    while(egl){
      entry = egl->data;
      if(trapsel.g == entry->g && trapsel.s == entry->s){
        trapsel.name = entry->name;                   /*fill in rest of selected trap data*/
        trapsel.popup = entry->popup;
        trapsel.script = entry->script;
        trapsel.flag = entry->flag;
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag1), trapsel.flag & 1);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag2), trapsel.flag & 2);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag3), trapsel.flag & 4);
        break;
      }
      egl = egl->next;
    }
  }

  trapselfill = 1;
}

void add_trap_cb(){     /*use modifies window to add. clear all old data before showing*/
  gtk_entry_set_text(mod_popup,0);
  gtk_entry_set_text(mod_script,0);
  gtk_widget_show_all(mod_win);
}

void del_trap_cb(){
int n;
trap_slot *trap;
GList *gl;
GList *tgl, *egl;
trap_oid *toid;
trap_entry *entry;

  if(!trapselfill) return;

  if(toid = get_trap_slot(enviroment.trapconf, trapsel.oid)){
    egl = toid->entrys;
    while(egl){
      entry = egl->data;
      if(trapsel.g == entry->g && trapsel.s == entry->s){
        toid->entrys = g_list_remove(toid->entrys, entry);
        break;
      }
      egl = egl->next;
    }
  }

  list_reload(enviroment.trapconf);
}

void modify_trap_cb(){
int n;

  if(!trapselfill) return;
  gtk_entry_set_text(mod_name,trapsel.name);
  gtk_entry_set_text(mod_popup,trapsel.popup);
  gtk_entry_set_text(mod_script,trapsel.script);
  trapselfill = 0;
  gtk_widget_show_all(mod_win);
}

void mod_bit1_cb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    trapsel.flag |= 1;                          /*set bit 1 - forward to corrd*/
  } else {
    trapsel.flag &= 254;                        /*clear bit 1*/
  }
}

void mod_bit2_cb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    trapsel.flag |= 2;                          /*set bit 1 - forward to corrd*/
  } else {
    trapsel.flag &= 253;                        /*clear bit 1*/
  }
}

void mod_bit3_cb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    trapsel.flag |= 4;                          /*set bit 1 - forward to corrd*/
  } else {
    trapsel.flag &= 251;                        /*clear bit 1*/
  }
}

void mod_cancel_cb(){
  gtk_widget_hide_all(mod_win);
}

void mod_ok_cb(){
gchar *entry;
  mod_cancel_cb();

  entry = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(mod_name)->entry));
  fprintf(stderr,"Name(%s)\n",entry);

  entry = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(mod_popup)->entry));
  fprintf(stderr,"Popup(%s)\n",entry);

  entry = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(mod_script)->entry));
  fprintf(stderr,"Script(%s)\n",entry);
}

