/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * gxdaeconf.c -- Configuration file front end
 * 
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stddef.h>	/*think we have some unnessecery includes here*/
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <netinet/in.h>

#include <gnome.h>
#include "config.h"
#include <sys/types.h>
#include "events.h"

#include "sys/stat.h"
#include "dae.h"

char gxdaeconf_version[] = "$Id$"; /*usefull to 'strings' the binary*/

aclist *trapfilter; /*unused, needed by libdae*/
void quit_cb();
glb_enviroment config;
struct _cfg{
	int utrap;  /*pass unknown traps*/
	int lutrap; /*log unknown traps*/
        int corrdl; /*corrd logging */
        int evdpyl; /*evdpysrvd loggin*/
	int severity; /*default severity of unknown traps */
	int evpoll; /*how often to poll event displays in seconds*/
        char popup[100]; /*command line popup*/
        
} cfg;
GList *severitys = 0;
GtkWidget *severitywid;
GtkWidget *evpollwid;
GtkWidget *popupwid;
GtkWidget *gxeventswid;
int get_line_cnt, get_line_base;
glb_enviroment enviroment;

/*widgets callback*/
void save_config(){
FILE *fp;
char cfile[200];
gchar *entry;

  strcpy(cfile,enviroment.base);
  strcat(cfile,"/conf/nb.conf");
  fp = fopen(cfile,"w");


  fprintf(fp,"BASE=\"%s\"\n",enviroment.base);			/*Write BASE that is given during installation */
  fprintf(fp,"DATABASE_HOST=\"%s\"\n",enviroment.host);
  fprintf(fp,"DATABASE_USER=\"%s\"\n",enviroment.user);
  fprintf(fp,"DATABASE_PASS=\"%s\"\n",enviroment.passwd);

  if(config.trapd_unknown_traps) fprintf(fp,"TRAP_UNKNOWN_TRAPS\n");		/*Write all definitions*/
  if(config.trapd_log_unknown_traps) fprintf(fp,"TRAP_LOG_UNKNOWN_TRAPS\n");
  if(config.corrd_log) fprintf(fp,"CORRD_LOG\n");
  if(config.evdpysrvd_log) fprintf(fp,"EVDPYSRVD_LOG\n");

  entry = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(severitywid)->entry));
  if(!strcmp(entry,"Critical")) fprintf(fp,"TRAP_DEFAULT_SEVERITY=\"64\"\n");
  else if(!strcmp(entry,"Alarm")) fprintf(fp,"TRAP_DEFAULT_SEVERITY=\"48\"\n");
  else if(!strcmp(entry,"Warning")) fprintf(fp,"TRAP_DEFAULT_SEVERITY=\"32\"\n");
  else if(!strcmp(entry,"Informative")) fprintf(fp,"TRAP_DEFAULT_SEVERITY=\"16\"\n");
  else if(!strcmp(entry,"Unknown")) fprintf(fp,"TRAP_DEFAULT_SEVERITY=\"0\"\n");

  fprintf(fp,"TRAP_DEFAULT_SEVERITY=\"%s\"\n", entry);
  entry = gtk_entry_get_text(GTK_ENTRY(popupwid));
  fprintf(fp,"POPUP_EXEC=\"%s\"\n", entry);
  entry = gtk_entry_get_text(GTK_ENTRY(evpollwid));
  fprintf(fp,"EVDPYSRVD_POLL=\"%s\"\n", entry);
  entry = gtk_entry_get_text(GTK_ENTRY(gxeventswid));
  fprintf(fp,"GXEVENTS_MAX_TRAPS=\"%s\"\n", entry);

  fclose(fp);
/*add: reload daemons (send a command to gxdhd )*/
  exit(0);
}
void quit_cb(){
  exit(0);
}

void utrapcb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    config.trapd_unknown_traps = 1;
  } else {
    config.trapd_unknown_traps = 0;
  }
}
void lutrapcb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    config.trapd_log_unknown_traps = 1;
  } else {
    config.trapd_log_unknown_traps = 0;
  }
}
void corrdlcb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    config.corrd_log = 1;
  } else {
    config.corrd_log = 0;
  }
}
void evdpylcb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    config.evdpysrvd_log = 1;
  } else {
    config.evdpysrvd_log = 0;
  }
}
void severitycb(GtkWidget *wid, GtkWidget *entry){
gchar *text;

  text = gtk_entry_get_text(GTK_ENTRY(entry));
  cfg.severity = atoi(text);
}
void evpollcb(GtkWidget *wid, GtkWidget *entry){
gchar *text;

  text = gtk_entry_get_text(GTK_ENTRY(entry));
  cfg.evpoll = atoi(text);
}
void popupcb(GtkWidget *wid, GtkWidget *entry){
gchar *text;

}

/* gtkcreate_query()
 * this function creates a checkbutton 
 * or a textinput .. with label and callbacks 
 */

GtkWidget *gtkcreate_query(GtkWidget *table, char *text,int y,int type, void *callback, void *data, int data2 )
{ 
GtkWidget *lab;
GtkWidget *wid;

  lab = gtk_label_new(text);
  gtk_table_attach (GTK_TABLE (table), lab, 1, 2, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
  gtk_widget_show(lab);
  if(type){
    wid = gtk_entry_new_with_max_length(data2);
    gtk_signal_connect(GTK_OBJECT(wid), "activate", GTK_SIGNAL_FUNC(callback), wid);
    gtk_table_attach(GTK_TABLE (table), wid, 2, 3, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
    gtk_entry_set_text(wid,data);
    gtk_widget_show(wid);
    return wid;
  }else{
    wid = gtk_toggle_button_new();
    gtk_table_attach (GTK_TABLE (table), wid, 2, 3, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
    gtk_signal_connect_object(GTK_OBJECT(wid), "clicked", GTK_SIGNAL_FUNC(callback),GTK_OBJECT(wid));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wid), data2);
    gtk_widget_show(wid);
  }
  return 0;
}

int main(int argc, char **argv)
{
  char foo[80];
  GtkWidget *wid;
  GtkWidget *configwid;
  GtkWidget *button;
  GtkWidget *vbox;
  GtkWidget *bar;
  GtkWidget *table;
  int tmp, w_size;
  gint        char_size;

  if(load_enviroment()){
    fprintf(stderr,"gxdaeconf: Error, couldn't load enviromental file.\n");
  }else{ /*else lets parse config*/
    cfg.utrap = enviroment.trapd_unknown_traps;
    cfg.lutrap = enviroment.trapd_log_unknown_traps;
    cfg.corrdl = enviroment.corrd_log; 
    cfg.evdpyl = enviroment.evdpysrvd_log;
    cfg.evpoll = enviroment.evdpysrvd_poll;
    cfg.severity = enviroment.severity;
    strcpy(cfg.popup,enviroment.popup_exec);
  }
  severitys = g_list_append(severitys, "Critical");
  severitys = g_list_append(severitys, "Alarm");
  severitys = g_list_append(severitys, "Warning");
  severitys = g_list_append(severitys, "Informative");
  severitys = g_list_append(severitys, "Unknown");

  gtk_init(&argc, &argv);
  configwid = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(configwid), vbox);
  gtk_widget_show(vbox);

  table = gtk_table_new (3, 3, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 3);
  tmp = gdk_text_width (vbox->style->font, "1", 1);
  tmp = tmp * 18;
  w_size = 0;
  char_size = gdk_string_width (table->style->font, "xW") / 2;

  button = gtk_button_new_with_label (_("Save & reload"));
  gtk_object_set_data (GTK_OBJECT (configwid), "ack_button", button);
  gtk_table_attach (GTK_TABLE (table), button, 0, 1, 0, 1, GTK_FILL | GTK_SHRINK, GTK_SHRINK, 1, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(save_config),(gpointer) "savecfg");
  button = gtk_button_new_with_label (_("Dismiss"));
  gtk_object_set_data (GTK_OBJECT (configwid), "ack_button", button);
  gtk_table_attach (GTK_TABLE (table), button, 1, 2, 0, 1, GTK_FILL | GTK_SHRINK, GTK_SHRINK, 1, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(quit_cb),(gpointer) "savecfg");

  bar = gtk_hseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 0, 3, 0, 2, GTK_FILL, GTK_FILL | GTK_EXPAND, 2, 1);

  gtkcreate_query(table, "Process unknown traps", 1,0,utrapcb,0,cfg.utrap);
  gtkcreate_query(table, "Log unknown traps", 2,0,lutrapcb,0,cfg.lutrap);
  gtkcreate_query(table, "Log correlation daemon", 3,0,corrdlcb,0,cfg.corrdl);
  gtkcreate_query(table, "Log event display traps", 4,0,evdpylcb,0,cfg.evdpyl);

  sprintf(foo,"%d",cfg.evpoll);
  evpollwid = gtkcreate_query(table, "Event Displays timeout", 5,1,evpollcb,foo,4);

  sprintf(foo,"%s",cfg.popup);
  popupwid = gtkcreate_query(table, "Popup command line", 7,1,popupcb,foo,80);

  sprintf(foo,"%d",config.gxevents_max_traps);
  gxeventswid = gtkcreate_query(table, "Maximum of events in gxevents", 8,1,popupcb,foo,80);

  wid = gtk_label_new("Severity");
  gtk_table_attach (GTK_TABLE (table), wid, 0, 2, 9, 10, GTK_SHRINK, GTK_SHRINK, 2, 1);
  gtk_widget_show(wid);
  sprintf(foo,"%d",cfg.severity);
  severitywid = gtk_combo_new();
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(severitywid)->entry), foo);
  gtk_combo_set_popdown_strings(severitywid, severitys);
  gtk_table_attach(GTK_TABLE(table), severitywid, 2, 3, 10, 11, GTK_FILL, GTK_FILL | GTK_EXPAND, 2, 1);
  gtk_widget_show(severitywid);

  gtk_widget_show_all (configwid);
  gtk_main();
  return 0;
}
