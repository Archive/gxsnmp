/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* gxdhd.c -- Daemon handling daemon
*
*/

/*daemon handler */
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <glib.h>

#include "dae.h"

#define default_timeout 6

char gxdhd_version[] = "$Id$"; /*usefull to 'strings' the binary*/

typedef struct _dae_stat{
	char name[20];		/*name of daemon*/
	char *addr;		/*address of daemon, IP addr, or a pathname+filename*/
	int state;		/*bit 0 (1=admin-Started,0=Stoped) bit 1 (1=send,0=wait_for_hello)*/
	int timeout;		/*timeout in seconds waiting for a daemon status answer*/
	int info_died;		/*hold how many time this daemon has lost contact/died*/
}daestat;

daestat dstat[] = {
"trapd",0, 3, default_timeout, 0,
"corrd",0, 3, default_timeout,0,
"evdpysrvd",0, 3, default_timeout,0,
"",0,0,0,0
}; 

aclist *trapfilter;
int masocket;
pid_t daemonp;
glb_enviroment enviroment;
char foo[40];
FILE *fpgxdhd;
char buffer[400];

void quit_call()
{
	logstr(fpgxdhd,"Killing all registered daemons.\n");
	sendsocket(masocket, enviroment.corrd, "kill ");
	sendsocket(masocket, enviroment.trapd, "kill ");
	sendsocket(masocket, enviroment.evdpysrvd, "kill ");

	close(masocket);
	unlink(enviroment.gxdhd);
	logstr(fpgxdhd,"gxdhd stoped.\n");
	fclose(fpgxdhd);
	_exit(0);
}
int launch_child(char *daemon){
pid_t pid;

        fprintf(stderr,"Creating fullpath\n");
	sprintf(buffer,"%s/bin/%s",enviroment.base,daemon);
        fprintf(stderr,"Launch(%s)\n", buffer);
	if(!(pid = fork())){
		if(!(fork())){
		fprintf(stderr,"START(%s)\n", buffer);
		 execv(buffer,0);
		}
		_exit(0);
	}
	if(pid < 0 || waitpid(pid,NULL,0) < 0) return 1;
	return 0;
}

daestat *d_lookup(char *name){ /* search in daemon struct, using daemon name as input */
int l=0;

	while(1){
		if(!strlen(dstat[l].name)) return 0;
		if(!strcmp(dstat[l].name, name)) return &dstat[l];
		l++;
	}
}

int main(int argc, char **argv)
{   
gint l=0,i,j;
char foo[4000];
char foo2[4000];
daestat *ds;

trappdu tpdu;

/*daemonize*/
#if 0
  if(fork() != 0) _exit(0);
  if(setsid() == -1) _exit(0);
  if(fork() != 0) _exit(0);
  umask(777);
  if(chdir("/")<0) _exit(0);
  for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
  open("/dev/null",O_RDWR); dup(0); dup(0);
#endif
/***********/
  load_enviroment();

  fpgxdhd = logopen("gxdhd.log");
  dstat[0].addr = enviroment.trapd;
  dstat[1].addr = enviroment.corrd;
  dstat[2].addr = enviroment.evdpysrvd;

  unlink(enviroment.gxdhd);
  unlink(enviroment.corrd);
  unlink(enviroment.trapd);
  unlink(enviroment.evdpysrvd);
  masocket = createsocket(enviroment.gxdhd,1);
  logstr(fpgxdhd,"Starting all daemons\n");
  launch_child("trapd");
  launch_child("corrd");
  launch_child("evdpysrvd");

start:
  sleep(2);
fprintf(stderr,".");

  switch(tpdu_read(masocket, &tpdu)){

  case PDU_PROGRAM_STATUS:    /* gxstatus asking status about daemons */
    strcpy(foo2, tpdu.argbuf);			/*get reply address*/
    for(l=0;strlen(dstat[l].name);l++){
      sprintf(foo,"\n%s: ", dstat[l].name);
      strcat(tpdu.argbuf,foo);
      if(dstat[l].state & 1)
        strcat(tpdu.argbuf, "Started, ");
      else
        strcat(tpdu.argbuf, "Stoped, ");
      sprintf(foo,"died %d times.", dstat[l].info_died);
      strcat(tpdu.argbuf, foo);
    }

    tpdu_write(masocket, foo2, tpdu); /* send daemon status */

  break;

  case PDU_PROGRAM_STOP: 	/*from gxstatus asking to stop a daemon*/
    if(!strcmp(tpdu.argbuf, "all"))
      quit_call();
    ds = d_lookup(tpdu.argbuf);				/*fetch info from daemon name*/
    sprintf(foo2,"Stoping daemon: %s\n", ds->name);
    logstr(fpgxdhd,foo2);
    tpdu_write(masocket, ds->addr, tpdu);
    ds->state = 0;			/*set daemon as state stoped (clear bit 0)*/
  break;

  case PDU_PROGRAM_START: 			/*from gxstatus asking to start a daemon*/
    ds = d_lookup(tpdu.argbuf);				/*fetch info from daemon name */
    unlink(ds->addr);				/*remove old socket*/
    sprintf(foo2,"Starting daemon: %s\n", ds->name);
    logstr(fpgxdhd,foo2);
    launch_child(ds->name);
    ds->timeout = default_timeout;
    ds->state = 3; /*set daemon in state send_hello and started*/
  break;

  case PDU_PROGRAM_RELOAD:  /*from gxstatus asking a daemon to reload configuration */
    sprintf(foo2,"Reloading daemon: %s\n",tpdu.argbuf);
    logstr(fpgxdhd,foo2);
    ds = d_lookup(tpdu.argbuf);					/*fetch info about daemon*/
    tpdu_write(masocket, ds->addr, tpdu);
  break;

  case PDU_PROGRAM_STATUS_REPLY:
    ds = d_lookup(tpdu.argbuf);			/*fetch info from daemon name*/
    ds->timeout = default_timeout;		/*reset timeout*/
    ds->state |= 2;				/*set daemon in send_hello state*/
  break;
  } /* switch */

  for(l=0;strlen(dstat[l].name);l++){				/*loop through and send hello queries*/
    if(!strlen(dstat[l].name)) break;
    if((dstat[l].state & 1) && (dstat[l].state & 2)){

      tpdu.type = PDU_PROGRAM_STATUS;
      tpdu_write(masocket, dstat[l].addr, tpdu);
      dstat[l].state &= 1;					/*set in dont send hello state (clear bit 1, leave bit 0 alone)*/
    }
    else if(dstat[l].state & 1){				/*if daemon is in started state, and wait_for_hello*/
      dstat[l].timeout--;
      if(!dstat[l].timeout){					/*daemon has probably died*/
        unlink(dstat[l].addr);					/*remove old socket*/
        sprintf(foo2,"Deamon has died, launching: %s\n",dstat[l].name);
        logstr(fpgxdhd,foo2);
        dstat[l].info_died++;
        launch_child(dstat[l].name);				/*reinit daemon*/
        dstat[l].timeout = default_timeout;
        dstat[l].state = 3;					/*set daemon in started+send_hello state*/
      }
    }
  }
  goto start;
}
