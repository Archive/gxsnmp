/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* main.c -- check incoming SNMP-TRAPs and messages
*
*/

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <syslog.h>
#include <errno.h>
#include <unistd.h>
#include <sys/un.h>

#include <glib.h>
#include <ucd-snmp-includes.h>

#include "dae.h"
#include "trapd.h"

#ifndef BSD4_3

#ifndef NFDBITS
typedef long	fd_mask;
#define NFDBITS	(sizeof(fd_mask) * NBBY)	/* bits per mask */

#define	FD_SET(n, p)	((p)->fds_bits[(n)/NFDBITS] |= (1 << ((n) % NFDBITS)))
#define	FD_CLR(n, p)	((p)->fds_bits[(n)/NFDBITS] &= ~(1 << ((n) % NFDBITS)))
#define	FD_ISSET(n, p)	((p)->fds_bits[(n)/NFDBITS] & (1 << ((n) % NFDBITS)))
#define FD_ZERO(p)	bzero((char *)(p), sizeof(*(p)))
#endif

#endif

int    snmp_input        (int op, struct snmp_session *session,
                          int reqid, struct snmp_pdu *pdu,
                          void *magic);



const char main_version[] = "$Id$"; /*usefull to 'strings' t
he binary*/

int debug_level;

GList *snmp_trap_receivers;
glb_enviroment enviroment;
aclist *trapfilter;
FILE *fptrapd;
GList * trapdcgl;

int  errno;

int main()
{
struct snmp_session session, *ss;
int count, numfds, block;
fd_set fdset;
struct timeval timeout, *tvp;
gchar line[1000];
GMainLoop *loop;


/*daemonize*/
#if 0 
  if(fork() != 0) _exit(0);
  if(setsid() == -1) _exit(0);
  if(fork() != 0) _exit(0);
  umask(777);
  if(chdir("/")<0) _exit(0);
  for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
  open("/dev/null",O_RDWR); dup(0); dup(0);
#endif


  load_environment(&enviroment);
  fptrapd = logopen("trapd.log");

  g_snprintf(line,400,"%s/conf/trap.conf", enviroment.base);
  fprintf(stderr,"Loading trap.conf (%s)\n", line);
  enviroment.traptab = load_trapconf(line);

  g_snprintf(line,400,"%s/conf/trap_rec.conf", enviroment.base);
  fprintf(stderr,"Loading trap_rec.conf (%s)\n", line);
  snmp_trap_receivers = trapd_load_trap_receivers(line);

  trapdcgl = NULL;
  dd_connect_as_server(4002, trapd_client_activity, NULL);

//  signal(SIGHUP, reload_config);
//  signal(SIGINT, coredump);
  logstr(fptrapd,"Trapd started.\n");

  memset((char *)&session, '\0', sizeof(struct snmp_session));
  /*bzero((char *)&session,sizeof(struct snmp_session));*/
  session.authenticator = NULL;
  session.callback = snmp_input;
  session.callback_magic = NULL;
  session.community = NULL;
  session.community_len = 0;
  session.local_port = SNMP_TRAP_PORT;
  session.peername = NULL;
  session.retries = SNMP_DEFAULT_RETRIES;
  session.timeout = SNMP_DEFAULT_TIMEOUT;
  ss = snmp_open(&session);
  if(ss == NULL){  
    fprintf(stderr,"Couldn't initialize SNMP session, port buzy?\n");
    _exit(-1);
  }

  loop = g_main_new (TRUE);
  while (g_main_is_running (loop))
    {
    g_main_iteration(0);
    numfds = 0;
    FD_ZERO(&fdset);
    block = 1;
    tvp = &timeout;
    timerclear(tvp);
    snmp_select_info(&numfds, &fdset, tvp, &block);
    //if (block == 1) tvp = NULL;	/* block without timeout */
    count = select(numfds, &fdset, 0, 0, tvp);
    if (count > 0)
      snmp_read(&fdset);
      /*snmp_sess_read(session, &fdset);  thought single session API supported this*/
    else switch(count){
/*FIX: log, and try to restart daemon*/
      case 0:
        snmp_timeout();
        break;
      case -1:
        if (errno == EINTR) continue;
        else perror("select");
        return -1;
      default:
        printf("select returned %d\n", count);
        return -1;
    }
  }

  g_main_destroy (loop);
  _exit(0);
}

