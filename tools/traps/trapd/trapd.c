/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* trapd.c -- process incoming service messages
*
*/

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>
#include <netinet/in.h>

#include "dae.h"

extern glb_enviroment enviroment;
extern aclist *trapfilter;
extern FILE *fptrapd;

int trapd_mainloop(){
gint trapdsock, nbytes;
gint netmon_host, netmon_msg;
struct in_addr netmon_addr;
gchar *netmon_ip;
gchar pipestring[4000];

trappdu tpdu;

  switch(1){
  case PDU_PROGRAM_STOP:
    close(trapdsock);
    logstr(fptrapd,"Trapd stoped.\n");
    fclose(fptrapd);
    _exit(0);
  break;
  case PDU_PROGRAM_STATUS:
    tpdu.type = PDU_PROGRAM_STATUS_REPLY;
    strcpy(tpdu.argbuf, "trapd");
  break;
  case PDU_PROGRAM_RELOAD:
    reload_config();
  break;
  case PDU_NETMON:
    memcpy(&netmon_msg, pipestring+sizeof(gint), sizeof(gint));
    memcpy(&netmon_host, pipestring+sizeof(gint)*2, sizeof(gint));
    memcpy(&netmon_addr, pipestring+sizeof(gint)*3, sizeof(gint));
    netmon_ip = inet_ntoa(netmon_addr);
    fprintf(stderr, "Host %d, IP %s, msg %u\n", netmon_host, netmon_ip, netmon_msg);
  break;
  default:
  /* unknown PDU_TYPE */
  break;
  }/* end of while pdutype */
}
