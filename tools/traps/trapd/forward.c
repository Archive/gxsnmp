/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* forward.c -- forward traps to other SNMP monitors
*
*/



#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include "dae.h"
#include "trapd.h"

gint
trapd_forward_trap (gchar *address, gint port, 
                      gpointer datagram, gint datagram_len)
{
gint sock;
struct sockaddr_in server;
  
  server.sin_family = AF_INET;
  server.sin_port   = htons(port);
  server.sin_addr.s_addr = inet_addr (address);

  sock = socket(AF_INET, SOCK_DGRAM, 0);
  sendto(sock, datagram, datagram_len, 0, &server, sizeof(server));

}

GList *
trapd_load_trap_receivers(gchar *filename)
{
FILE *fp;
gchar w1[200], line[2000];
gint port;
trapd_client *trapdc;
GList *gl;

  gl = NULL;
  if(!(fp = fopen(filename, "r"))){
    fprintf(stderr,"Error opening file(%d)\n", errno);
    return NULL;
  }
  while(fgets(line,200,fp)){
    if(line[0] == '#') continue;
    wordcpy(w1,line,1);
    port = atoi(wordcpy(w1,line,2));

    trapdc = g_malloc0(sizeof(trapd_client));
    trapdc->address = g_strdup(w1);
    trapdc->port = port;
    gl = g_list_append(gl, trapdc);
  }
  fclose(fp);
  return gl;
}
