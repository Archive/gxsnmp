/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* trap.c -- process SNMP TRAP data
*
*/

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>
#include <netinet/in.h>

#include "dae.h"
#include "trapd.h"

extern glb_enviroment enviroment;
extern FILE *fptrapd;
extern GList *snmp_trap_receivers;

gint 
trap_command(char *cmd, char **args)
{	/*runs command using fork and system calls*/
pid_t pid;	

  if(!(pid = fork())){
    if(!(fork())) execv(cmd,args);
    _exit(0);
  }
  waitpid(pid,NULL,0);
  return 0;
}

void
trapd_trapdc_node_free(trapd_client *trapdc)
{
  if(trapdc->address)
    g_free(trapdc->address);
  if(trapdc->name)
    g_free(trapdc->name);
}

void 
reload_config()
{
gchar line[2000];

  g_list_free(enviroment.traptab);
  g_snprintf(line,400,"%s/conf/trap.conf", enviroment.base);
  enviroment.traptab = load_trapconf(line);
  logstr(fptrapd,"Reloaded trap.conf.\n");

  g_list_foreach(snmp_trap_receivers, trapd_trapdc_node_free, NULL);
  g_list_free(snmp_trap_receivers);
  g_snprintf(line,400,"%s/conf/trap_rec.conf", enviroment.base);
  snmp_trap_receivers = trapd_load_trap_receivers(line);
  logstr(fptrapd,"Reloaded trap_rec.conf.\n");
}

void 
coredump()
{
pid_t pid;
  logstr(fptrapd,"Administratively coredumped.\n");
  if(!(pid = fork())){
    raise(SIGQUIT);
    _exit(0); /*precausious if SIGQUIT has other purpouses on other machines*/
  }
  waitpid(pid,NULL,0);
}

trapd_client *
trapdc_lookup(const GIOChannel *channel)
{
  GList *gl;
  trapd_client *trapdc;
  gl = trapdcgl;
  while(gl){
    trapdc = gl->data;
    if(channel == trapdc->channel)
      return trapdc;
    gl = gl->next;
  }
  return 0;
}

void
trapdc_client_new(const GIOChannel *channel)
{
  trapd_client *trapdc;

  trapdc = g_malloc0(sizeof(trapd_client));
  if(!trapdc) return;
  trapdc->channel = channel;
  trapdcgl = g_list_append(trapdcgl, trapdc);
  fprintf(stderr,"CLIENT_NEW(%u)\n", channel);
}

void
trapdc_client_remove(const GIOChannel *channel)
{
  trapd_client *trapdc;

  trapdc = trapdc_lookup(channel);
  if(!trapdc) return;
  trapdcgl = g_list_remove(trapdcgl, trapdc);
  fprintf(stderr,"CLIENT_REMOVE(%u)\n", channel);
}

void
trapd_send_to_clients(trappdu *tpdu, const gint app_type)
{
  GList *gl;
  trapd_client *trapdc;
  gint num_write;

  gl = trapdcgl;
  while(gl){
    trapdc = gl->data;
    if(app_type){
      if(app_type == trapdc->app_type){
        fprintf(stderr,"CLIENT SEND_TO\n");
        g_io_channel_write(trapdc->channel, tpdu, sizeof(trappdu), &num_write);
      }
    }else{
      fprintf(stderr,"CLIENT SEND_ALL\n");
      g_io_channel_write(trapdc->channel, tpdu, sizeof(trappdu), &num_write);
    }
    gl = gl->next;
  }
}


gint trap_process(trappdu * tpdu, int len)
{
gint i, j, *args[10];
gchar eid[512], row[200], cmd[200];

gchar trapd[] = "trapd";
g_trap trap;
g_trap_context trap_ctx;
gchar *txt;

  memset(&trap, 0, sizeof(g_trap));
  memset(&trap_ctx, 0, sizeof(g_trap_context));

  g_trap_decode(tpdu, &trap);

  trap.ctx = &trap_ctx;
  if(g_trap_lookup(enviroment.traptab, &trap)){/*Do some action if trap is identified in trap.conf*/
    tpdu->severity = trap.severity;	/*set severity in trap block to forward*/
    if(trap_ctx.popup){		/*POPUP a message to DISPLAY*/
      txt = var_interpret2(trap.ctx->popup, &trap);
      if(enviroment.popup_date) args[0] = ctime((time_t *)&trap.timestamp);
      else args[0] = trapd;
      args[1] = txt;
      args[2] = 0;
      trap_command(enviroment.popup_exec,args);
      g_free(txt);
    }
    if(trap.ctx->script){
      wordcpy(cmd,trap.ctx->script,1);              /*get command*/
      wordcpy(eid,trap.ctx->script,2);              /*get command arguments*/

      txt = var_interpret2(trap.ctx->script, &trap);

      j = 1;
      wordcpy(cmd,txt,j);              /*get command*/
      args[0] = trapd;

      i = 1;j++;
      while(wordcpy(row,txt,j)){              /*get command arguments*/
        j++;
        args[i] = g_strdup(row);
        i++;
      }
      args[j] = 0;

      trap_command(cmd,args);

      j = 1;
      while(args[j]){
        g_free(args[j]);
        j++;
      }
      g_free(txt);
    }
  
    if(trap.flag & 1){                          /*bit 0 is forward to corrd*/
      fprintf(stderr,"CORRD\n");
      trapd_send_to_clients(tpdu, GXSNMP_PROGRAM_CORRD);
    }
    if(trap.flag & 2){                          /*bit 1 is forward to evdpysrvd*/
      trapd_send_to_clients(tpdu, GXSNMP_PROGRAM_EVDPYSRVD);
    }                                                                                           
    if(trap.flag & 4){                          /*bit 2 log trap*/
      g_snprintf(eid,512,"Trap: %s#%s#%d#%d\n",trap.address,trap.oid,trap.g,trap.s);
      logstr(fptrapd,eid);
    }
  }else{
  trap.severity = enviroment.severity; /* trap is unknown, override severity */
  if(enviroment.trapd_unknown_traps){         /*may I forward unknown traps to evdpysrvd*/
      trapd_send_to_clients(tpdu, GXSNMP_PROGRAM_EVDPYSRVD);
    }                                                                                           
    if(enviroment.trapd_log_unknown_traps){
      g_snprintf(eid,512,"Trap: %s#%s#%d#%d\n",trap.address,trap.oid,trap.g,trap.s);
      logstr(fptrapd,eid);
    }
  }
/*lets do some actions for both unknown and identified traps*/
  g_trap_free(&trap);
  
  return 0;
} 


void
trapd_client_activity(gint type, GIOChannel *channel, trappdu * tpdu)
{
  trapd_client *trapdc;

  switch(type){ /* of activity */
  case 0:
    trapdc_client_new(channel);
  break;
  case 1:
    trapdc = trapdc_lookup(channel);
    if(!trapdc) return;
    switch(tpdu->type){
      case PDU_PROGRAM_REGISTER:
        fprintf(stderr,"PROGRAM_REGISTER\n");
        trapdc->app_type = tpdu->severity;
        trapdc->name = g_strdup(tpdu->argbuf);
      break;
    }
  break;
  case 2:
    trapdc_client_remove(channel);
  break;
  }

}
