/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* snmp.c -- process incoming SNMP TRAP-PDUs
*
*/

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/un.h>

#include <glib.h>
#include <ucd-snmp-includes.h>

#include "dae.h"
#include "trapd.h"

#ifndef BSD4_3

#ifndef NFDBITS
typedef long	fd_mask;
#define NFDBITS	(sizeof(fd_mask) * NBBY)	/* bits per mask */

#define	FD_SET(n, p)	((p)->fds_bits[(n)/NFDBITS] |= (1 << ((n) % NFDBITS)))
#define	FD_CLR(n, p)	((p)->fds_bits[(n)/NFDBITS] &= ~(1 << ((n) % NFDBITS)))
#define	FD_ISSET(n, p)	((p)->fds_bits[(n)/NFDBITS] & (1 << ((n) % NFDBITS)))
#define FD_ZERO(p)	bzero((char *)(p), sizeof(*(p)))
#endif

#endif

extern glb_enviroment enviroment;
extern int pdudsock;
extern GList *snmp_trap_receivers;

int 
snmp_input(int op, struct snmp_session *session, 
           int reqid, struct snmp_pdu *pdu, 
           void *magic)
{

char buffer[2000];
int pz;			/*buffer pointer */
//struct {
//  unsigned char type;
//  unsigned int len;
//}block;

struct _trapinfo{
  char IP[20];
  char ts[80];
  char enterprise[200];
  int g;
  int s;
  ulong uptime;
  int argc;
//  int argv[3][200];
} trap;

char *ptr;
guint i, a, u, len, v;
struct variable_list *vars;
time_t ts; 	/*timestamp*/
struct sockaddr_in *aip;
GList *gl;
gint result;

gchar strbuf[2000];
trappdu tpdu;
trapd_client *trapdc;

  if (op == RECEIVED_MESSAGE && pdu->command == SNMP_MSG_TRAP){
/* parse buffer */
    trap.enterprise[0] = 0;
    for (i = 0; i < pdu->enterprise_length; i++){
      sprintf(&trap.enterprise[strlen(trap.enterprise)], ".%d", pdu->enterprise[i]);
    }

        u = PDU_TRAP;
        tpdu.type = PDU_TRAP;

        /*tpdu.type = severity; */       /* severity */

	ts = time(NULL);
        tpdu.time = ts;
        aip = (struct sockaddr_in *)&pdu->agent_addr;
	memcpy(&(tpdu.address), (char *)&aip->sin_addr, 4);	/* IP */
        tpdu.generic = pdu->trap_type;
        tpdu.specific= pdu->specific_type;
        tpdu.oidlen = strlen(trap.enterprise);
	memcpy(&tpdu.oid, &trap.enterprise,130);	/* OID */

/* all variables in trap */

        a = 0;
	for (trap.argc = 0, vars = pdu->variables;  vars;  trap.argc++, vars = vars->next_variable){
		trap.enterprise[0] = 0;
		for (i = 0; i < vars->name_length; i++){	/*variable OID*/
			sprintf(&trap.enterprise[strlen(trap.enterprise)], ".%d", vars->name[i]);
		}
		
		switch(vars->type){			/*variable value*/
		case ASN_INTEGER:

			add_asciiz(&a, tpdu.argbuf, trap.enterprise); 
			add_guint(&a, tpdu.argbuf, *(vars->val.integer));

		break;
		case ASN_OCTET_STR:

			add_asciiz(&a, tpdu.argbuf, trap.enterprise);
			g_snprintf(strbuf, (vars->val_len)+1, "%s", vars->val.string);
			add_asciiz(&a, tpdu.argbuf, strbuf);

		break;

/*case ASN_OBJECT_ID: .. vars->val.objid .. vars->val_len*/
/*case IPADDRESS: .. bcopy(vars->val.string, &sin_addr.s_addr, sizeof(sin_addr.s_addr));*/
		}

	}

/*forward trap to trapd*/

/*set end of block marker must not colide with add_asciiz/guint PDU defines */
        len = a;
        a = 255;
        memcpy(tpdu.argbuf+len, &a, sizeof(gint));
	len = sizeof(trappdu);
	trap_process(&tpdu, len);

        /***** to make things easy.
               'pdu' is a pointer to a VALID SNMP TRAP
               if we simply exit here we have no control
               of the trap, and would have to generate
               it from our trappdu data format.
               So lets travers a GList here and send 
               this TRAP to whoever wants it.
        ******/

        /**** this is so stupid, I which the
              routine calling this callback
              could pass a raw data pointer
              instead of all this unnecessery
              rebuilding of a SNMP TRAP PDU.
        *****/

        pz = 1900;
        result = snmp_build(session, pdu, &buffer, &pz);
        if (result < 0){
          fprintf(stderr,"trapd: error forwarding trap(%d, %d)\n", result,
              session->s_snmp_errno);
          return 0;
        }

        gl = snmp_trap_receivers;
        while(gl){
          trapdc = gl->data;
          trapd_forward_trap (trapdc->address, trapdc->port,
                            buffer, pz);
          gl = gl->next;
        }

    }
   if (op == TIMED_OUT)
    {
      fprintf(stderr,"Timeout: This shouldn't happen!\n");
    }
  return 0;
}
