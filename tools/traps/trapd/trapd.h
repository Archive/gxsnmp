#include <glib.h>


typedef struct _trapd_client {
  GIOChannel *channel;  /* address */
  gchar *address;  /* address for trap forward */
  gint port;       /* UDP port for trap forward */
  gint app_type;   /* Application Type */
  gchar *name;     /* Name of application */
} trapd_client;

extern GList * trapdcgl;

void    trapd_client_activity     (gint type, GIOChannel *channel,
                                   trappdu *tpdu );
GList * trapd_load_trap_receivers (gchar *filename);
gint    trapd_forward_trap        (gchar *address, gint port, 
                                   gpointer datagram, gint datagram_len);
void    trapdc_client_remove      (const GIOChannel *channel);
void    trapdc_client_new         (const GIOChannel *channel);

void    reload_config             ();

