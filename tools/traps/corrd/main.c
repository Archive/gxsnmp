/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* corrd.c -- SNMP TRAP Correlation daemon
*
*/

#define G_THREADS_ENABLED
#define G_THREADS_IMPL_POSIX


#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <glib.h>
#include <EXTERN.h>
#include <perl.h>
#include "dae.h"
#include "corrd.h"

int debug_level;

char main_version[] = "$Id$"; /*usefull to 'strings' the binary*/

FILE *forkfp;
FILE *fpcorrd;
glb_enviroment environment;

GList *background, *bgs, *foreground, *fgs;

/*needs argument due to PERL , but we pass NULLs anyway*/
int main(int argc, char **argv, char **env)
{

trappdu tpdu;

GList *tmp;
fg_script *fgs;
GIOChannel *trapd_channel;
GMainLoop *loop;
gint num_write;
gchar line[2000];

  if(!g_thread_supported())
    g_thread_init(NULL);
  else {
    fprintf(stderr,"corrd exiting: Executing environment does not support\
		threads\n");
    exit(-1);
  }
 
/*daemonize*/
#if 0
  if(fork() != 0) _exit(0);
  if(setsid() == -1) _exit(0);
  if(fork() != 0) _exit(0);
  umask(777);
  if(chdir("/")<0) _exit(0);
  for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
  open("/dev/null",O_RDWR); dup(0); dup(0);
#endif
/***********/

fprintf(stderr,"loading env.\n");
  load_environment(&environment);

fprintf(stderr, "opening logfile\n");

  g_snprintf(line, 1000, "%s/log/corrd.log", environment.base);
  fpcorrd = logopen(line);

fprintf(stderr, "loading correlation configuration\n");
  /* loading file with DAE_BASE offset */
  snprintf(line, 400, "%s/%s", environment.base, "conf/corrd.conf");
  if(load_corrd_config(line, &background, &foreground) == -1){
    fprintf(stderr,"Error loading configuration\n");
    exit(0);
  }

  fprintf(stderr,"Foreground config\n");
  tmp = foreground;
  while(tmp){
    fgs = tmp->data;
    fprintf(stderr,"File: %s\n", fgs->file);
//    fprintf(stderr,"  ip: %s\n", fgs->ip);
    tmp = tmp->next;
  }
  fprintf(stderr,"background config\n");
  tmp = background;
  while(tmp){
    fgs = tmp->data;
    fprintf(stderr,"File: %s\n", fgs->file);
    tmp = tmp->next;
  }

  trapd_channel = dd_connect_as_client ("127.0.0.1", 4002,
                             trapd_activity, NULL);

  sleep(1); /* Wait for trapd to accept connection */

  /** Register evdpysrvd in trapd daemon **/
  tpdu.type = PDU_PROGRAM_REGISTER;
  tpdu.severity = GXSNMP_PROGRAM_CORRD;
  strcpy(tpdu.argbuf, "corrd");
  g_io_channel_write(trapd_channel, &tpdu, sizeof(trappdu), &num_write);

  loop = g_main_new (TRUE);
  while (g_main_is_running (loop))
    {
      g_main_iteration(1);
    }
  g_main_destroy (loop);
  return 0;
}
