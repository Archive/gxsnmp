/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* perl.c -- PERL engine for SNMP TRAP correlation
*
*/

#include <glib.h>

#if defined(__cplusplus) && !defined(PERL_OBJECT)
#define is_cplusplus
#endif

#ifdef is_cplusplus
extern "C" {
#endif

#include <EXTERN.h>
#include <perl.h>
#ifdef PERL_OBJECT
#define NO_XSLOCKS
#include <XSUB.h>
#include "win32iop.h"
#include <fcntl.h>
#include <perlhost.h>
#endif
#ifdef is_cplusplus
}
#  ifndef EXTERN_C
#    define EXTERN_C extern "C"
#  endif
#else
#  ifndef EXTERN_C
#    define EXTERN_C extern
#  endif
#endif

#include "dae.h"
#include "corrd.h"

EXTERN_C void xs_init _((void));

EXTERN_C void boot_DynaLoader _((CV* cv));

EXTERN_C void
xs_init(void)
{
        char *file = __FILE__;
        dXSUB_SYS;

        /* DynaLoader is a special case */
        newXS("DynaLoader::boot_DynaLoader", boot_DynaLoader, file);
}

void
perl_main(corrthreadslot *cts)
{
  gint argc = 0;
  gchar *argv[2];
  PerlInterpreter *perl;

  if((perl = perl_alloc()) == 0){
    perror("alloc");
    fprintf(stderr,"Can't correlate using perl_script\n");
  }
  argv[0] = "corrd";
  argv[1] = cts->script;
  argv[2] = 0;
  argc = 2;
fprintf(stderr,"FILEX(%s)\n", argv[0]);
  perl_construct(perl);
  perl_parse(perl, xs_init, argc, argv, NULL);
  perl_run(perl);
  perl_destruct(perl);
  perl_free(perl);


}
