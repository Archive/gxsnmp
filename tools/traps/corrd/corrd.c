/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* corrd.c -- SNMP TRAP Correlation daemon
*
*/

#define G_THREADS_ENABLED
#define G_THREADS_IMPL_POSIX

#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <glib.h>

#include "dae.h"
#include "corrd.h"

gchar corrd_version[] = "$Id$"; /*usefull to 'strings' the binary*/

gint corrdsock;
extern FILE *forkfp;
extern FILE *fpcorrd;
extern glb_enviroment environment;
gint pstatus, traplen;

extern GList *background, *bgs, *foreground, *fgs;
GList *corrthreads = 0; 	/* all corrthreadslots is saved in this list, for use by corrd */
int scheme_ct = 0; 		/* this is set, whenever there is a new trap to be correlated*/
struct timespec delay;

/* corr_lookup()
 *
 * return of this function is 
 * private for the function
 */

gint
corr_match(fg_script *fg, g_trap *trap){

    if(fg->ip && trap->address){
      if(!strcmp(fg->ip, trap->address)) return 1;
    }

    if(fg->oid && trap->oid){
      if(!strcmp(fg->oid, trap->oid)) return 1;
    }

    if(fg->generic){
      if(fg->generic == trap->g) return 1;
    }

    if(fg->specific){
      if(fg->specific == trap->s) return 1;
    }

  return 0;	/* end of foreground configuration file */
}

void
correlate(fg_script *corrdst, g_trap *trap)
{
  gint nbytes, l;
  pthread_t sid;
  corrthreadslot *newthread; 	/*this is a pointer to the trap to be correlated*/

  fprintf(stderr,"Trap identified.\n");
  newthread = g_new0(corrthreadslot, 1);   /*create a new corrthreadslot */
  newthread->stype = 0;                    /*default to shell script */
  newthread->traplen = nbytes;
  if(strncmp(corrdst->file,"/",1)){	
    sprintf(newthread->script, "%s/script/%s",environment.base, corrdst->file);
  }else{              /*scriptfile have full pathname, its must be a shellscript */
    sprintf(newthread->script, "%s", corrdst->file);
  }

  if(environment.corrd_log) 
    logstr(fpcorrd,"Launching correlation script.\n");
  /*corrthreads = g_list_append(corrthreads, newthread);*/	
  /*save it in corrd's thread lookup table, for all traps*/
  fprintf(stderr,"Running script in thread.\n");
  pthread_create(&sid, 0, &do_script, newthread);

/*something checking sid here*/

//  g_free(newthread); /*REMOVE when we have threads, and a trap GList revision function, that throws away old traps*/

}

void
trapd_activity (gint type, GIOChannel *channel, trappdu *tpdu)
{
  g_trap trap;
  fg_script *fg;
  GList *gl;

  switch(type){
    case 2:         /* Peer has disconnected */
    return;
  }

  switch(tpdu->type){
  case PDU_TRAP:
    fprintf(stderr,"Trap received.\n");
    g_trap_decode(tpdu, &trap);

    gl = foreground;
    while(gl){
      fg = gl->data;
      if(corr_match(fg, &trap))
        correlate(fg, &trap);
      gl = gl->next;
    }
  }
}

void
corrd_client_activity (gint type, GIOChannel *channel, trappdu *tpdu)
{
  gint num_write;

  switch(type){
  case 2:
    return;
  }

  switch(tpdu->type){
  case PDU_PROGRAM_STOP:
    logstr(fpcorrd,"Corrd stopped.\n");
    fclose(fpcorrd);
    _exit(0);
  case PDU_PROGRAM_STATUS: /*answer a hello query from gxstatus */
    tpdu->type = PDU_PROGRAM_STATUS_REPLY;
    strcpy(tpdu->argbuf, "corrd");
    g_io_channel_write(channel, tpdu, sizeof(trappdu), &num_write);
  break;
  case PDU_PROGRAM_RELOAD:
    load_corrd_config("/conf/corrd.conf", &background, &foreground);
    /*FIX free old config, if new config was successfully loaded */
    logstr(fpcorrd,"Reloaded conf/corrd.conf.\n");
  break;

  default:
  break;
  }/*switch pdutype */
}

void 
corrd_client_new      (GIOChannel *channel)
{
#if 0
  trapd_client *trapdc;

  trapdc = g_malloc0(sizeof(trapd_client));
  trapdc->channel = channel;

  trapdcgl = g_list_append(trapdcgl, trapdc);

  fprintf(stderr,"CLIENT_NEW\n");
#endif
}

