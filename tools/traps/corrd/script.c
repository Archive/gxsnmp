/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* script.c -- Script launcher
*
*/

#define G_THREADS_ENABLED
#define G_THREADS_IMPL_POSIX

#if defined(__cplusplus) && !defined(PERL_OBJECT)
#define is_cplusplus
#endif

#ifdef is_cplusplus
extern "C" {
#endif

#include <EXTERN.h>
#include <perl.h>

#include <glib.h>
#include <pthread.h>
#include "dae.h"
#include "corrd.h"

extern char pipestring[400];
extern int traplen;
extern int corrdsock;
extern glb_enviroment enviroment;

static PerlInterpreter *perl;

corrthreadslot *newthread;      /*this is a pointer to the trap to be correlated*/

void do_script(corrthreadslot *newthread){
gint argc = 0;
gchar *argv[2];

  if((perl = perl_alloc()) == 0){
    perror("alloc");
    fprintf(stderr,"Can't correlate using perl_script\n");
  }

  argc = 0;
  argv[0] = 0;

  perl_construct(perl);
  perl_parse(perl, NULL, argc, argv, (char **)NULL);
  perl_run(perl);
  perl_destruct(perl);
  perl_free(perl);

  pthread_exit(0);
}
