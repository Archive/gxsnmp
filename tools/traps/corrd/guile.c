/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* guile.c -- SCHEME engine for SNMP TRAP correlation
*
*/

#define G_THREADS_ENABLED
#define G_THREADS_IMPL_POSIX

#include <glib.h>
#include <guile/gh.h>
#include <libguile.h>
//#include <guile/gscm.h> /* use instead of two above */
#include <pthread.h>
#include "dae.h"
#include "corrd.h"

extern char pipestring[400];
extern int traplen;
extern int corrdsock;
extern glb_enviroment enviroment;

corrthreadslot *newthread;      /*this is a pointer to the trap to be correlated*/

/********************************
* SCHEME PROCEDURES             *
********************************/
SCM gx_compare_trapvar(SCM ivar, SCM iip){      /* compare vars against trap-fields*/
char *var;
char *value;
        
  var = gh_scm2newstr(ivar, NULL);
  value = gh_scm2newstr(iip, NULL);
         
  if(!strcmp(var,"IP")){
    if(!strcmp(value,newthread->trap.IP)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else if(!strcmp(var,"OID")){
    if(!strcmp(value,newthread->trap.oid)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else if(!strcmp(var,"GENERIC")){
    if(!strcmp(value,newthread->trap.g)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else if(!strcmp(var,"SPECIFIC")){
    if(!strcmp(value,newthread->trap.s)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else {  /* syntax error */
  }
  return 0;
}
             
SCM gx_forward(){  /*this will forward trap to evdpysrvd*/
  newthread->flag |= 16;
  return 0;
}
             
SCM gx_block(){   /*this will block trap to beeing evdpysrvd*/
  newthread->flag &= CTSFMASK - 16;
  return 0;
}

/* gx_wait_for_trap()
* wait for a specific trap to come in.
*/
SCM gx_wait_for_trap(SCM ip, SCM oid, SCM g, SCM s) {
gint t;

  t = 30;
  while(t){
    sleep(2);
    t--;
  }
  return 0;
}
           
SCM gx_get_trapvar(SCM var){ /*returns 'var' from trap*/
char *text;
             
  text = gh_scm2newstr(var, NULL); 
  if(!strcmp(text,"IP")){
    text = newthread->trap.IP;
    return gh_str2scm(text,strlen(text));
  }
  else if(!strcmp(text,"OID")){
    fprintf(stderr,"GetOID\n");
    text = newthread->trap.oid;
    return gh_str2scm(text,strlen(text));
  }
  else if(!strcmp(text,"GENERIC")){
    return gh_long2scm(newthread->trap.g);
  }
  else if(!strcmp(text,"SPECIFIC")){
    return gh_long2scm(newthread->trap.s);
  }
  else{   /*syntax error*/
  }
  return 0;
}
             
SCM gx_change_severity(SCM var){ /*change severity of trap*/
char *text;
  text = gh_scm2newstr(var, NULL);
  newthread->trap.severity = atoi(text);
  return 0;
}

SCM gx_print(SCM var){
char *text;
  text = gh_scm2newstr(var, NULL);
  fprintf(stderr,"%s\n",text);
  return 0;
}
             
/********************************
* CORRELATION FUNCTIONS         *
********************************/
          
static void 
scheme_main(int argc, char **argv){
FILE *fp;
corrthreadslot *cts;
gchar line[200];

  fprintf(stderr,"scm1");

  /*                func-name            function            noargs */
  gh_new_procedure("gx_compare_trapvar", gx_compare_trapvar, 2, 0, 0);
  gh_new_procedure("gx_forward",         gx_forward,         0, 0, 0);
  gh_new_procedure("gx_block",           gx_block,           0, 0, 0);
  gh_new_procedure("gx_get_trapvar",     gx_get_trapvar,     1, 0, 0);
  gh_new_procedure("gx_change_severity", gx_change_severity, 1, 0, 0);
  gh_new_procedure("gx_print",           gx_print,           1, 0, 0);

  cts = (corrthreadslot *) argv[0];

  fprintf(stderr,"scm2:%s\n", cts->script);

  if(!(fp = fopen(cts->script,"r"))){
    pthread_exit(-1);
  }
 
  fprintf(stderr,"scm3");
  while(fgets(line, 200, fp)){
    gh_eval_str(line);
  }
  fclose(fp);

/**** RULESET POST PROCESSING *****/
  if( cts->flag & 16){  /* script said to forward this trap */
    //senddsocket(corrdsock, enviroment.evdpysrvd, pipestring, cts->traplen);
    //g_io_channel_write(channel, 
  }
/**********************************/

//  gh_load( cts->script);  /*load and execute a Scheme file*/
  g_free(argv[0]);	/* free newthread */
  pthread_exit(0);
//  _exit(0);
}

void do_script(corrthreadslot *newthread){

    switch(newthread->stype){
      case 1:
        perl_main(newthread);
        break;
      case 2:
        gh_enter(1, &newthread, scheme_main);
        fprintf(stderr,"WARNING: guile.c:do_script()@gh_enter() returned.\n");
        break;
      case 3:
        break;
      default:
       /* unsupported script type, APP_ERR */
    }
  pthread_exit(0);
}
