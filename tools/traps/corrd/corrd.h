/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* corrd.h
*
*/

#define CTSFMASK 8191          /* corrthreadslot-flag-mask */
#define MAXTRAPHOLD 100        /*number of traps to keep in buffer */

typedef struct _bg_script{
  gchar *file;
}bg_script;

typedef struct _fg_script{
  gchar *file;
  gchar *ip;
  gchar *oid;
  guint generic;
  guint specific;
}fg_script;

typedef struct _corrthreadslot{ /* each correlating thread will receive its own corrthreadslot
 */
  gint traplen;
  gint pstatus;          /* status of thread when it terminates */
  gint seconds;          /* how long, if wanted, to keep this trap in memory*/
  gint flag;             /* flags about trap and so, mask is CTSFMASK */
    /* bit 0 - 0=running 1=finished , correlation done.
     * bit 1 - 0=no 1=yes, corrd can remove trap.
     * bit 2 - 
     * bit 3 - 
     * bit 4 - 0=block / 1=forward trap after finished.
    */
  trapslot trap;        /* trap to be correlated */
  gint stype;            /* script type */
  gchar script[200];     /* scriptfile */
  gchar *scriptargs[2];   /* script arguments, stupid but simple */
  gchar args[2][200];
  GIOChannel *channel;    /* reply address */
}corrthreadslot;


/******** PROTOTYPES *******/

void corrd_client_new      (GIOChannel *channel);
void corrd_client_activity (gint type, GIOChannel *channel, trappdu *tpdu);
void trapd_activity        (gint type, GIOChannel *channel, trappdu *tpdu);
void do_script             (corrthreadslot *);
void perl_main             (corrthreadslot *cts);

