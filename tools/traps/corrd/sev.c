/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* sev.c --
*
*/

/********************************
* SCHEME PROCEDURES             *
********************************/
SCM gx_compare_trapvar(SCM ivar, SCM iip){      /* compare vars against trap-fields*/
char *var;
char *value;
        
  var = gh_scm2newstr(ivar, NULL);
  value = gh_scm2newstr(iip, NULL);
         
  if(!strcmp(var,"IP")){
    if(!strcmp(value,newthread->trap.IP)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else if(!strcmp(var,"OID")){
    if(!strcmp(value,newthread->trap.oid)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else if(!strcmp(var,"GENERIC")){
    if(!strcmp(value,newthread->trap.g)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else if(!strcmp(var,"SPECIFIC")){
    if(!strcmp(value,newthread->trap.s)) return SCM_BOOL_T;
    else return SCM_BOOL_F;
  }
  else {  /* syntax error */
  }
}
             
SCM gx_forward(){  /*this will forward trap to evdpysrvd*/
  newthread->flag |= 16;
}
             
SCM gx_block(){   /*this will block trap to beeing evdpysrvd*/
  newthread->flag &= CTSFMASK - 16;
}
           
SCM gx_get_trapvar(SCM var){ /*returns 'var' from trap*/
char *text;
             
  text = gh_scm2newstr(var, NULL); 
  if(!strcmp(text,"IP")){
    text = newthread->trap.IP;
    return gh_str2scm(text,strlen(text));
  }
  else if(!strcmp(text,"OID")){
    fprintf(stderr,"OIDlokkup\n");
    text = newthread->trap.oid;
    return gh_str2scm(text,strlen(text));
  }
  else if(!strcmp(text,"GENERIC")){
    return gh_long2scm(newthread->trap.g);
  }
  else if(!strcmp(text,"SPECIFIC")){
    return gh_long2scm(newthread->trap.s);
  }
  else{   /*syntax error*/
  }
}
             
SCM gx_change_severity(SCM var){ /*change severity of trap*/
char *text;
  text = gh_scm2newstr(var, NULL);
  newthread->trap.severity = atoi(text);
}

SCM gx_print(SCM var){
char *text;
  text = gh_scm2newstr(var, NULL);
  fprintf(stderr,"%s\n",text);
}
             
/********************************
* CORRELATION FUNCTIONS         *
********************************/
          
void scheme_main(int argc, char **argv){
  fprintf(stderr,"scm1");
  gh_new_procedure("gx_compare_trapvar", gx_compare_trapvar, 2, 0, 0);
  gh_new_procedure("gx_forward", gx_forward, 0, 0, 0);
  gh_new_procedure("gx_block", gx_block, 0, 0, 0);
  gh_new_procedure("gx_get_trapvar", gx_get_trapvar, 1, 0, 0);
  gh_new_procedure("gx_change_severity", gx_change_severity, 1, 0, 0);
  gh_new_procedure("gx_print", gx_print, 1, 0, 0);
  fprintf(stderr,"scm2:%s\n",((corrthreadslot *)argv[0])->script);
  gh_load( ((corrthreadslot *)argv[0])->script);                  /*load and execute a Scheme file*/
  fprintf(stderr,"scm3");
  if(newthread->flag & 16){ 					  /* script said to forward this trap */
    /*IF CHANGING TO THREADS PIPESTRING & TRAPLEN MUST BE SAVED IN NEWTHREAD INSTEAD */
    senddsocket(corrdsock, enviroment.evdpysrvd, pipestring, traplen);
  }
  pthread_exit(0);
//  _exit(0);
}
            
void script_service(corrthreadslot *tdata){
/*purpose of this function is to provide information to the other thread that is executing a script*/
start: goto start;

}

void do_script(corrthreadslot *newthread){

    gh_enter(1, &newthread, scheme_main);
}
            
void exec_script(corrthreadslot *tdata){
pid_t pid;
pthread_t sid;  

  switch(pid = fork()){
    case 0:
      if(fork() == 0) break;
    case -1:
      return;
    default:
      waitpid(pid,NULL,0);
      return;
  }
/*  pthread_create(&sid, 0, &script_service, tdata);*/ /*start service thread*/
           
  switch(tdata->stype){ 
  case 0:                                                             /* shell script */
    fprintf(stderr,"EXECV(%s)\n",tdata->script);
    execv(tdata->script, tdata->scriptargs);
    _exit(0);
  case 1:                                                             /* perl script */
     fprintf(stderr,"PERL(%s)\n",tdata->scriptargs[0]);
     tdata->perl = perl_alloc();
     perl_construct(tdata->perl);
     perl_parse(tdata->perl, NULL, 1, tdata->scriptargs, (char **) NULL);
     perl_run(tdata->perl);
     perl_destruct(tdata->perl);
     perl_free(tdata->perl);
     _exit(0);
  case 2:
    gh_enter(1, &tdata, scheme_main);
    /*if returns control something is wrong*/
    _exit(0);
  }
}
