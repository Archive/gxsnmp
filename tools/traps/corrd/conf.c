/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* conf.c -- config file processing
*
*/

/* load corrd configuration file using
 * gscanner wrapper ( lib/lex.c )
 *
 */
#include <stdio.h>
#include <glib.h>

#include "dae.h" 
#include "corrd.h" 
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

fg_script *new_fg()
{
fg_script *fg;
	if(!(fg = malloc(sizeof(fg_script)))){
		return 0;
        }
	return fg;
}

int load_corrd_config(gchar *file, GList **back, GList **fore)
{
xmlDocPtr doc;
xmlNodePtr node;
gchar *script, *ip, *generic, *specific, *oid;
fg_script *fgs;
GList *foreground = NULL;
GList *background = NULL;

  doc = xmlParseFile(file);
  if(!doc)
    return -1;

  if(!doc->root){
    xmlFreeDoc(doc);
    return -1;    
  }

  node = doc->root->childs;

  for(; node != NULL; node = node->next){
    if(!g_strncasecmp(node->name, "TRIG", 4)) break;
  }
  if(!node) return -1;

  for(node = node->childs; node != NULL; node = node->next){
    if(!(fgs = new_fg())) return -1;
    fgs->file     = xmlGetProp(node, "script");
    fgs->ip       = xmlGetProp(node, "ip");
    fgs->oid      = xmlGetProp(node, "oid");
    fgs->generic  = xmlGetProp(node, "generic");
    fgs->specific = xmlGetProp(node, "specific");
    foreground = g_list_append(foreground, fgs);
  }

  node = doc->root->childs;

  for(; node != NULL; node = node->next){
    if(!g_strncasecmp(node->name, "BACK", 4)) break;
  }
  if(!node) return -1;

  for(node = node->childs; node != NULL; node = node->next){
    if(!(fgs = new_fg())) return -1;
    fgs->file = xmlGetProp(node, "script");
    background = g_list_append(background, fgs);
  }

  *back = background;
  *fore = foreground;
  return 0;
}
