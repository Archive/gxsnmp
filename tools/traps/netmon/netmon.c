/* -*- Mode: C -*-
 * $Id$
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * netmon.c -- network monitor daemon
 *
 */ 


#include <stdio.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>

#include <glib.h>
#include "debug.h"

#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "ddserver.h"

#include "dae.h"

aclist *trapfilter;
glb_enviroment enviroment;

int debug_level;

int dcsock;

int
main (int argc, char **argv)
{
  DB_host host, *dbhp;
  DB_graph dbg;
  DB_interface dbi, *dbip;
  GIOChannel *dbserver;
  GList *gl, *targets;
  gchar *hostname;
  gchar buffer[2000];

  struct sockaddr_in iaddr;

  guint netmonsock;

  unsigned long int i;
  gint n,h;


  initpingsocket();

#if 0
fprintf(stderr,"loading env\n");
  load_enviroment();
fprintf(stderr,"removing socket file\n");
  unlink(enviroment.netmon);
fprintf(stderr,"activating socket file\n");
  if(!(netmonsock = createsocket(enviroment.netmon,0))) _exit(0);
#endif


/* Load Interface table as input */

  dbserver= db_server_connect_config ("127.0.0.1", 4000);

  interface_sqldb->channel = dbserver;
  g_sqldb_table_load(interface_sqldb);
  targets = g_sqldb_table_list(interface_sqldb);

  gl = targets;

  if(!gl){
    fprintf(stderr,"No interfaces available from database.\n");
    exit(0);
  }
  else while(gl){
    dbip = gl->data;
    printf("created: %s\n", dbip->created);
    printf("modified: %s\n", dbip->modified);
    printf("host: %d\n", dbip->host);
    printf("address: %s\n", dbip->address);
    gl = gl->next;
  }
  fprintf(stderr,"\n\n");

mainloop:
  sleep(2);

  gl = targets;
  while(gl){
    dbip = gl->data;
    fprintf(stderr,"\nPing(%s): ", dbip->address);
    sendping(inet_addr(dbip->address));
    sleep(1);
    if(readping(&iaddr)){
      fprintf(stderr,"Alive");
      
      if(!strcmp(dbip->tags,"Alive")) /* interface is already down */
        goto noupdate;

      /* Update its 'alive' flag in interface.tags db table */

      dbip->tags = "Alive"; /* primitive but a starter */
      g_sqldb_row_update(interface_sqldb, dbip);

      /* Generate a alarm and send it to trapd */
#if 0
      i = PDU_NETMON;
      memcpy(buffer, &i, sizeof(gint));
      i = NETMON_DEAD;
      memcpy(buffer+sizeof(gint), &i, sizeof(gint));
      i = dbip->host;
      memcpy(buffer+sizeof(gint)*2, &i, sizeof(gint));
      i = inet_addr(dbip->address);
      memcpy(buffer+sizeof(gint)*3, &i, sizeof(gint));

      senddsocket(netmonsock, enviroment.trapd, buffer, sizeof(gint)*4);
#endif

    }else{
      fprintf(stderr,"Dead");

      if(!strcmp(dbip->tags,"Dead")) /* interface is already down */
        goto noupdate;

      /* Update its 'alive' flag in interface.tags db table */

      dbip->tags = "Dead"; /* primitive but a starter */
      g_sqldb_row_update(interface_sqldb, dbip);

      /* Generate a alarm and send it to trapd */
#if 0
      i = PDU_NETMON;
      memcpy(buffer, &i, sizeof(gint));
      i = NETMON_DEAD;
      memcpy(buffer+sizeof(gint), &i, sizeof(gint));
      i = dbip->host;
      memcpy(buffer+sizeof(gint)*2, &i, sizeof(gint));
      i = inet_addr(dbip->address);
      memcpy(buffer+sizeof(gint)*3, &i, sizeof(gint));

      senddsocket(netmonsock, enviroment.trapd, buffer, sizeof(gint)*4);
#endif

    }
noupdate:
 
    gl = gl->next;
  }

goto mainloop;

  db_server_disconnect(dcsock);
  exit(0);
}
/*eof*/
