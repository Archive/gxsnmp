/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Header file for the ping module.
 */
#ifndef __PING_IO_H_
#define __PING_IO_H_

#include <gtk/gtk.h>
#include <netinet/in.h>
/*
 * Prototypes
 */
void ping_input_cb             (gpointer          data,
				gint              source,
				GdkInputCondition condition);
void initpingsocket            (void);
void initping                  (void);
int sendping                   (u_long            address);
struct sockaddr_in *readping   (void);
u_short in_cksum               (u_short           *addr,
				int               len);
#endif

