/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* event.c -- format events
*
*/

#include <stdio.h>
#include <netinet/in.h>
#include <orb/orbit.h>
#include <gnome.h>
#include <liboaf/liboaf.h>
#include "orbit-gxsnmp-db.h"
#include "dae.h"
#include "gxevents_util.h"

/* trap_to_event()
*
* Fill in trap->name
* Fill in event struct using trap data.
* Make summary and description of event
*/
gint trap_to_event(GList *gl, g_trap *trap, gxsnmp_event *event)
{

  event->source = trap->address;

  if(g_trap_lookup(gl, trap )) /*check if trap is known by trap.conf */
    event->summary = g_strdup(trap->name);
  else if(trap->noid)
    event->summary = g_strdup(trap->noid);
  else if(trap->oid)
    event->summary = g_strdup(trap->oid);
  else
    event->summary = g_strdup("UNDEFINED");

  event->description = g_trap_arg_to_string(trap);
  event->category = GXSNMP_EVENT_SNMP_TRAP;
  event->type = GXSNMP_EVENT_SNMP_TRAP;
  event->severity = trap->severity;
  event->stat = 0; 
  event->timestamp = trap->timestamp;
  event->data = trap;
}
