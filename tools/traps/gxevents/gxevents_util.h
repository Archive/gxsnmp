#include "dbapi.h"

/********************
 ** Definitions *****
 ********************/

#define SUBWIN_EMPTY 1
#define SUBWIN_ACTIVE 2

#define GXSNMP_EVENT_MESSAGE_SHORT 1
#define GXSNMP_EVENT_SNMP_TRAP     2

/********************
 ** Data types ******
 ********************/

typedef struct _sub_window {
  int stat;                     /*0=endoflist, 1=empty, 2=used*/
  GtkWidget *sublist;           /*List to append trap to*/
  GtkWidget *win;               /*main window, used when closing sub window*/
  aclist *acl;                  /*loaded trapfilter*/
} sub_window;


typedef struct _menutoollist {
  char name[80];                /*name that appear in Menu*/
  char cmd[200];                /*command to interpret*/
} menutoollist;

typedef struct _evdpys{
  gchar *name;                /* name that appear in notebook page */
  char cmd[200];                /*command to interpret*/
} evdpy;


/* function definitions for gxevents.c */

void menutools_interpret    (GtkWidget *mti, gchar *text);
void tvw_open               ();
void sub_close              (GtkWidget *win);
void sub_open               (struct _sub_window *subw);
void prune_trap             ();

typedef struct _gxsnmp_evdpy {
  GIOChannel * channel;
  GxSNMP_GSQLDB db_client;
  gchar *name;
  gchar *address;
  guint port;
  GtkWidget *clist;
  gboolean use_corba;	/* dumb, to detect which API we use */
} gxsnmp_evdpy;

typedef struct __gxsnmp_event {
  gint     eventid;
  gint     category;
  gint     severity;
  gint     stat;                  /* New/Old/Acknowledged etc */
  guint    timestamp;
  gchar    *source;               /* source of the event */
  gchar    *summary;              /* a 20 to 30 char summary for display */
  gchar    *description;          /* A longer description for the detail */
  gint    type;			  /* Type of event snmp_trap etc.. */
  gpointer data;                 /* original event data */
} gxsnmp_event;

typedef struct _GxSNMPEventDisplay             GxSNMPEventDisplay;
typedef struct _GxSNMPEventDisplayClass        GxSNMPEventDisplayClass;

struct _GxSNMPEventDisplay
{

  GdkVisual *visual;
  GdkColormap *colormap;
  GdkColor black;
  GdkColor red;
  GdkColor yellow;
  GdkColor amber;
  GdkColor green;
  GdkColor white;
  GdkColor grey;

  GtkWidget *notebook;
  gint notebook_cnt;

  GtkWidget   *window;
  GtkWidget   *clist;

  GtkWidget *tvw_win;
  GtkWidget *tvw_vbox;
  GtkWidget *fileacl;
  GtkWidget *subfileacl;

  GList *evdpys;
  GList *strsub;

  GList *sub_windows;
  FILE *tmpfp;
  glb_enviroment env;
  gchar mydpyfile[40];
  gint mydpysock;
  gint trapid;          /*uniq ID of every Event */
  gint col_sizes[6];
  GList *event_list;
  gxsnmp_event selected_event;	        /*current selected event in eventlist*/
  aclist *trapfilter;			/* this is main displays trapfilter */
  gchar aclfile[200];			/* main event_display loaded trapfilter filename */

/*** database access ***/
  /** CORBA API **/
  CORBA_Environment ev;
  CORBA_ORB orb;
  GxSNMP_GSQLDB db_client;
  GxSNMP_GSQLDB_GSQLDBTable *db_table;
  GxSNMP_GSQLDB_RowInterface *db_table_host;
  /** Native API **/
  GIOChannel *ddchannel;	/*if zero we can't use database */
  G_sqldb_table host_sqldb;
/***********************/

};



struct _GxSNMPEventDisplayClass
{
  GtkVBoxClass    parent_class;
};

/********************
 ** Prototypes ******
 ********************/

//void     open_event_panel       (void);
//void     destroy_event_panel    (void);
//void     hide_event_panel       (void);
//void     reset_event_panel      (void);
int      add_event              (GIOChannel * channel, gxsnmp_event *event);
int      remove_event           (int event_id);
void     evdpysrvd_activity     (gint type, GIOChannel *channel, trappdu *tpdu);

void event_display_init       (GxSNMPEventDisplay *control);
void gxsnmp_gxsnmp_event_copy(gxsnmp_event *dst, gxsnmp_event *src);

/* EOF */
