/**

This file takes care of inserting
a page in the notebook, and connecting
a evdpysrvd to the page.

api outside this file:
PAGE_NEW
  make gui
  ask user of evdpysrvd info
PAGE_DESTROY
  release alot of memory

*/

#define USE_DATABASE 0

#include <orb/orbit.h>
#include <gnome.h>
#include <liboaf/liboaf.h>
#include <netinet/in.h> /*needed by dae.h*/
#include "dae.h"
#include "orbit-gxsnmp-db.h"
#include "gxevents_util.h"
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"

extern GxSNMPEventDisplay * control;


      void eventlist_cb (GtkWidget *eventlist, gint row, gint column, 
                         GdkEventButton *event);

/* gxsnmp_evdpy_new()
*
* Create a new event display as a notebook tab
*
*/

void
gxsnmp_evdpy_new(GxSNMPEventDisplay *control, 
                 gchar *name, gchar *ipaddress, gint udpport, gboolean use_corba)
{
  GtkWidget * label;
  GtkWidget * page;
  GtkWidget * scrolledwindow;
  GtkWidget * index;
  GtkWidget * stamp;
  GtkWidget * severity;
  GtkWidget * source;
  GtkWidget * summary;
  GtkWidget * arguments;
  GtkWidget * clist;
  gxsnmp_evdpy * evdpy;
  gchar *query;

/****** CREATE CONNECTION BLOCK AND CONNECT ******/

  evdpy = g_malloc0(sizeof(gxsnmp_evdpy));

  evdpy->use_corba = use_corba;
  if(use_corba){
    query = g_strdup("repo_ids.has ('IDL:GxSNMP/Eventdisplayserver:1.0')");
    evdpy->db_client = oaf_activate (query, NULL, 0, NULL, &control->ev);
    g_free(query);
    if(evdpy->db_client == CORBA_OBJECT_NIL) {
      g_print("CONNECTION TO EVDPYSRVD FAILED\n");
      g_free(evdpy);
      return;
    }
  }else{
    evdpy->channel = dd_connect_as_client(ipaddress , udpport, 
                                          evdpysrvd_activity, NULL);
  }
  evdpy->name = g_strdup("evdpysrvd");

/*** Scrolled WINDOW *************/

  page = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (page);
  gtk_container_add (GTK_CONTAINER (control->notebook), page);
 
  scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_ref (scrolledwindow);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "scrolledwindow", scrolledwindow,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (scrolledwindow);
//  gtk_container_add (GTK_CONTAINER (control->notebook), scrolledwindow);
  gtk_container_add (GTK_CONTAINER (page), scrolledwindow);

/*** Event LIST ****************/
 
  evdpy->clist = clist = gtk_clist_new (6);
  gtk_widget_ref (clist);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "clist", clist,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_signal_connect(GTK_OBJECT(clist), "select_row",
                     GTK_SIGNAL_FUNC(eventlist_cb),NULL);
 
  gtk_widget_show (clist);
  gtk_container_add (GTK_CONTAINER (scrolledwindow), clist);
  gtk_clist_set_column_width (GTK_CLIST (clist), 0, 40);
  gtk_clist_set_column_width (GTK_CLIST (clist), 1, 60);
  gtk_clist_set_column_width (GTK_CLIST (clist), 2, 60);
  gtk_clist_set_column_width (GTK_CLIST (clist), 3, 60);
  gtk_clist_set_column_width (GTK_CLIST (clist), 4, 60);
  gtk_clist_set_column_width (GTK_CLIST (clist), 5, 2000);
  gtk_clist_column_titles_show (GTK_CLIST (clist));
 
  index = gtk_label_new (_("index"));
  gtk_widget_ref (index);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "index", index,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (index);
  gtk_clist_set_column_widget (GTK_CLIST (clist), 0, index);
 
  stamp = gtk_label_new (_("Timestamp"));
  gtk_widget_ref (stamp);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "stamp", stamp,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (stamp);
  gtk_clist_set_column_widget (GTK_CLIST (clist), 1, stamp);
 
  severity = gtk_label_new (_("Severity"));
  gtk_widget_ref (severity);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "severity", severity,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (severity);
  gtk_clist_set_column_widget (GTK_CLIST (clist), 2, severity);

  source = gtk_label_new (_("Source"));
  gtk_widget_ref (source);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "source", source,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (source);
  gtk_clist_set_column_widget (GTK_CLIST (clist), 3, source);

  summary = gtk_label_new (_("summary"));
  gtk_widget_ref (summary);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "summary", summary,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (summary);
  gtk_clist_set_column_widget (GTK_CLIST (clist), 4, summary);

  arguments = gtk_label_new (_("arguments"));
  gtk_widget_ref (arguments);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "arguments", arguments,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (arguments);
  gtk_clist_set_column_widget (GTK_CLIST (clist), 5, arguments);

/****** CREATE NOOTEBOOK PAGE ********/

  label = gtk_label_new (name);
  gtk_widget_ref (label);
  gtk_object_set_data_full (GTK_OBJECT (control->window), name, label,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (control->notebook), 
           gtk_notebook_get_nth_page (GTK_NOTEBOOK (control->notebook), control->notebook_cnt), label);

  control->notebook_cnt++;

  control->evdpys = g_list_append(control->evdpys, evdpy);
}

void
evdpysrvd_activity(gint type, GIOChannel *channel, trappdu *tpdu)
{
  aclist acl;
  gxsnmp_event event;
  gchar buf2[1000];
  g_trap trap;
  gchar *listitems[2];  /* this is for new trap windows */
  GList *swgl;
  sub_window *subwin;
  DB_host *dbh;

  switch(type){
    case 2:      /* Peer has disconnected */
      /* append and EOF event to the event list */
    return;
    break;
  }

  switch(tpdu->type){
  case PDU_POLL:
    tpdu->type = PDU_POLL_REPLY;
  return;
  case PDU_TRAP:
    g_trap_decode(tpdu, &trap);
    acl.ip = trap.address;
    acl.oid = trap.oid;
    acl.g = trap.g;
    acl.s = trap.s;
    trap.noid = 0;
    if(oid2text(&trap.noid, trap.oid)) trap.noid = trap.oid;
                /*translate OID, if fail, give OID as is */

    /* IP TO NAME RESOLUTION */
#if 0
/* WE must have a evdpy lookup before doing name resolution */
    if(evdpy->use_corba){
    }else{
      if(control->host_sqldb.channel){ /* Have main-app loaded a list of hosts */
        dbh = g_sqldb_row_find (&(control->host_sqldb), "address", trap.address);
        if(dbh){
          g_free(trap.address);
          trap.address = dbh->address;
        }
      }
    }
#endif
    /*************************/


/*Distribute event to all sub windows */
    swgl = control->sub_windows;
    while(swgl){
      subwin = swgl->data;
      if(subwin->acl){     /* Shouldn't happen, empty access list */
        printf("APP_ERR: no access control list loaded to a sub window trap display.\n");
        break;
      } 
      if(!test_acl(acl, subwin->acl)) continue; /* check if event is allowed by filter */
      /* Create Event entry, and append it to sub window */
      g_snprintf(buf2,1000,"%s %s %d %d", trap.address, trap.noid, trap.g, trap.s);
      listitems[0] = buf2;
      listitems[1] = 0;
      gtk_clist_append(GTK_CLIST(subwin->sublist), listitems);
      swgl = swgl->next;
    }

    if(!test_acl(acl, control->trapfilter)) return; /*check if event is allowed by filter*/

    trap_to_event(control->env.traptab, &trap, &event);
    add_event(channel, &event);
  break;
  }

}

void
evdpysrvd_connect_connect_cb(GtkWidget *widget, GtkWidget *window)
{

  GtkWidget *text1 = NULL;
  GtkWidget *text2 = NULL;
  GtkWidget *text3 = NULL;

  gchar *pagename = NULL;
  gchar *ipaddress = NULL;
  gchar *udp = NULL;
  gint port = NULL;

  text1 = gtk_object_get_data(window, "text1");
  if(!text1) goto fail;
  text2 = gtk_object_get_data(window, "text2");
  if(!text1) goto fail;
  text3 = gtk_object_get_data(window, "text3");
  if(!text1) goto fail;
 
  
  pagename  = g_strdup(gtk_entry_get_text(text1)); 
  if(!pagename) goto fail;
  ipaddress = g_strdup(gtk_entry_get_text(text2)); 
  if(!ipaddress) goto fail;
  udp       = g_strdup(gtk_entry_get_text(text3)); 
  if(!udp) goto fail;
  port = atoi(udp);
  if(!port) goto fail;

  gxsnmp_evdpy_new(control, pagename, ipaddress, port, 0);

fail:

  if(pagename)  g_free(pagename);
  if(ipaddress) g_free(ipaddress);
  if(udp)       g_free(udp);

  gtk_widget_destroy(window);
}

void
evdpysrvd_connect_cancel_cb(GtkWidget *widget, GtkWidget *window)
{
  gtk_widget_destroy(window);
}

void
evdpysrvd_connect(GtkWidget *widget)
{

  GtkWidget *window1;
  GtkWidget *table1;
  GtkWidget *pagename;
  GtkWidget *text1;
  GtkWidget *connect;
  GtkWidget *cancelbut;
  GtkWidget *ipaddress;
  GtkWidget *text2;
  GtkWidget *label5;
  GtkWidget *udpport;
  GtkWidget *text3;

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window1), "window1", window1);
  gtk_window_set_title (GTK_WINDOW (window1), _("window1"));

  table1 = gtk_table_new (5, 2, FALSE);
  gtk_widget_ref (table1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "table1", table1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table1);
  gtk_container_add (GTK_CONTAINER (window1), table1);

  pagename = gtk_label_new (_("Page name"));
  gtk_widget_ref (pagename);
  gtk_object_set_data_full (GTK_OBJECT (window1), "pagename", pagename,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (pagename);
  gtk_table_attach (GTK_TABLE (table1), pagename, 0, 1, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  text1 = gtk_entry_new ();
  gtk_widget_ref (text1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "text1", text1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (text1);
  gtk_table_attach (GTK_TABLE (table1), text1, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  connect = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
  gtk_widget_ref (connect);
  gtk_object_set_data_full (GTK_OBJECT (window1), "connect", connect,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (connect);
  gtk_table_attach (GTK_TABLE (table1), connect, 0, 1, 4, 5,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  gtk_signal_connect(GTK_OBJECT(connect), "clicked",
                            GTK_SIGNAL_FUNC(evdpysrvd_connect_connect_cb), window1);

  cancelbut = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
  gtk_widget_ref (cancelbut);
  gtk_object_set_data_full (GTK_OBJECT (window1), "cancelbut", cancelbut,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cancelbut);
  gtk_table_attach (GTK_TABLE (table1), cancelbut, 1, 2, 4, 5,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  gtk_signal_connect(GTK_OBJECT(cancelbut), "clicked",
                            GTK_SIGNAL_FUNC(evdpysrvd_connect_cancel_cb), window1);

  ipaddress = gtk_label_new (_("IP address"));
  gtk_widget_ref (ipaddress);
  gtk_object_set_data_full (GTK_OBJECT (window1), "ipaddress", ipaddress,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (ipaddress);
  gtk_table_attach (GTK_TABLE (table1), ipaddress, 0, 1, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  text2 = gtk_entry_new ();
  gtk_widget_ref (text2);
  gtk_object_set_data_full (GTK_OBJECT (window1), "text2", text2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (text2);
  gtk_table_attach (GTK_TABLE (table1), text2, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  label5 = gtk_label_new (_("Connect to a remote GxSNMP Event Display\nServer Manager ( evdpysrvd ). Please specify IP address \nand UDP port the evdpysrvd serves. UDP Port of \nevdpysrvd is usually 4003. Enter a short describing name of the\n source you are connecting to as Page name."));
  gtk_widget_ref (label5);
  gtk_object_set_data_full (GTK_OBJECT (window1), "label5", label5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table1), label5, 1, 2, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  udpport = gtk_label_new (_("UDP port"));
  gtk_widget_ref (udpport);
  gtk_object_set_data_full (GTK_OBJECT (window1), "udpport", udpport,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (udpport);
  gtk_table_attach (GTK_TABLE (table1), udpport, 0, 1, 3, 4,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  text3 = gtk_entry_new ();
  gtk_widget_ref (text3);
  gtk_object_set_data_full (GTK_OBJECT (window1), "text3", text3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (text3);
  gtk_table_attach (GTK_TABLE (table1), text3, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  gtk_widget_show(window1);

}

void
evdpysrvd_disconnect(GtkWidget *widget)
{
}

