#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>

#include "dae.h"

glb_enviroment enviroment;
aclist *trapfilter = 0;

main(int argc, char **argv)
{   
aclist acl;
char line[200];
 
  load_enviroment();
  trapfilter = load_acl("acl/fel.acl");
  strcpy(acl.oid,".1.3.6.1.4.1.9.5.17");
  acl.g = 2;
  acl.s = 0;

  if(!test_acl(acl, trapfilter)) printf("\nDenied\n");
  else printf("\nPermited\n");

}
