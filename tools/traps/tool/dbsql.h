/*
dbsql depends on these libraries:
	<stdio.h>
	<string.h>
	<sys/stat.h>
	<mysql.h>
*/

// General DB structs:
typedef struct {		// db interface
	MYSQL mysql;		// mysql structure
	char host[40];		// current database host
	char user[20];		// username
	char passwd[49];	// password
	char db[20];		// current active database name
	int sock;		// current active socket
	char err[40];
	} db_init;

typedef struct {
	char name[20];
	char parent[20];
	} db_map;

typedef struct {
	int camid;
	char name[20];
	char descr[40];
	} db_site;

typedef struct {		// quick selection
	int dnid;
	int camid;
	char map[20];
	} db_qs;
	

typedef struct {
	int _rowid;
	char created[20];
	char modified[20];
	char dns_name[32];
	char name[16];
	char description[127];	
	char contact[64];
	char tags[255];
	} db_host;

typedef struct {		// general node
	int dnid;		// GS identifier
	int idx;		// snmp index name of node
	int speed;		// speed of port
	char pname[20];		// portname
	char descr[40];
	int camid;		// Campus ID local 
	} db_gn;

typedef struct {
        int state;      // running?
        char min[2];    // minute started local field
        int eid;
        char edate[20];
        char name[20];
        char map[20];	//map to poll or script to execute
        char descr[40];
        char dfile[80]; //script file
	int dii;
	int sid;
        } db_event;

typedef struct {
	int eid;		// event
	int camid;		// object site
	int dnid;		// object
	int idx;		// object node index
	} db_poll;

typedef struct {		// mibs
	int sid;		// mibs ID
	int dii;		// attribute
	char name[40];		// name of OID
	char string[60];	// OID string
	char descr[80];		// descr of OID
	} db_mibs;

typedef struct {	// global mib reference.. system types
	int stid;		// system type
	char name[40];		// name of system
	char descr[80];		// descr of system
	} db_mibr;

typedef struct {	// system values
	int sid;		// string ID
	int dii;		// snmp string attributes
	char string[60];	// actual string value 
	char name[40];		// name of string
	char descr[80];		// descr of string
	int stid;		// local
	} db_mibstr;

typedef struct {	// add record struct
	int eid;
	int dnid;
	int idx;
	char rdate[16];
	char raw[260];
	int dii;	//local use
	} db_rec;
