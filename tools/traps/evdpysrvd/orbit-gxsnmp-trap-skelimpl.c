#include <stdio.h>
#include "gxsnmp.h"

/*******************
**** PROTOTYPES ****
********************/

/***** EPV STRUCTURES -- TRAP *****/

/*** epv structures ***/
static PortableServer_ServantBase__epv TRAPbase_epv = {
        NULL,                   /* _private data */
        NULL,                   /* finalize routine */
        NULL                    /* default_POA routine */
};

static POA_GxSNMP_TRAP__epv TRAPepv = {
        NULL                   /* _private */
};

/*** vepv structures ***/
static POA_GxSNMP_TRAP__vepv TRAPvepv = {
        &TRAPbase_epv,
        &TRAPepv
};


/******** FUNCTIONS *******/



impl_POA_GxSNMP_TRAP *
impl__TRAPCreate (PortableServer_POA poa, CORBA_Environment *ev)
{
        impl_POA_GxSNMP_TRAP *newservant;
        PortableServer_ObjectId *objid;

        newservant = g_new0(impl_POA_GxSNMP_TRAP, 1);
        newservant->servant.vepv = &TRAPvepv;
        newservant->poa = poa;

        POA_GxSNMP_TRAP__init ((PortableServer_Servant) newservant, ev);
        objid = PortableServer_POA_activate_object (poa, newservant, ev);
        CORBA_free(objid);

        return newservant;
}


void
impl__destroy (impl_POA_GxSNMP_TRAP *servant, CORBA_Environment *ev)
{
        PortableServer_ObjectId *objid;
        
        objid = PortableServer_POA_servant_to_id (servant->poa, servant, ev);
        PortableServer_POA_deactivate_object (servant->poa, objid, ev);
        CORBA_free (objid);
        POA_GxSNMP_TRAP__fini ((PortableServer_Servant) servant, ev);
        g_free(servant);
}

