/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* evdpysrvd.c -- Event Display Server Daemon
*
*/

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>
#include "dae.h"
#include "evdpysrvd.h"

char func_version[] = "$Id$"; /*usefull to 'strings' the binary*/

extern FILE * fpevdpysrvd;

/* reload ()
* reload config
*/
void
reload()
{
}

/* restart ()
*
* Close all I/O
* and restart daemon.
*
*/
void
restart()
{
  /* g_io_channel_close(channel); */
  reload();
}

void
evdpysrvd_services (GIOChannel *channel, trappdu *tpdu)
{

gchar foo[1000],foo2[80];
gchar text[1000];
gint i, traplen, pdutype, num_write;
GList *gl;
g_trap trap;
xdpy * dpy;

  switch(tpdu->type){
  case PDU_PROGRAM_STOP:
    quit();
  break;
  case PDU_DPY_OPEN:
    xdpy_add(tpdu->argbuf);		/* Add a display */
    g_snprintf(foo2,80,"Registered event display: %s\n",tpdu->argbuf);
    logstr(fpevdpysrvd,foo2);
    fprintf(stderr,foo2);
  break;
  case PDU_DPY_CLOSE:
    xdpy_del(tpdu->argbuf);  /*del old event display from receiveing traps*/
    g_snprintf(foo2,80,"Unregistered event display: %s\n", tpdu->argbuf);
    logstr(fpevdpysrvd,foo2);
  break;
  case PDU_POLL_REPLY:
    xdpy_reset(tpdu->argbuf);
  break;
  case PDU_PROGRAM_STATUS:
    return;
    strcpy(tpdu->argbuf, "evdpysrvd");
    tpdu->type = PDU_PROGRAM_STATUS_REPLY;
    g_io_channel_write(channel, tpdu, sizeof(trappdu), &num_write);
    return;
  break;
  case PDU_CMD_EVDPYSRVD_PRINT_CONNECTED:
    fprintf(stderr,"Print connected dpy status\n");
    sprintf(tpdu->argbuf,"EVDPYSRVD_PRINT_CONNECTED result.\n");
    gl = xdpygl;
    while(gl){	/* go through all registered displays and check if they are alive*/
      dpy = gl->data;
      sprintf(foo,"Address: %s\n", dpy->address);
      strcat(tpdu->argbuf,foo);
      sprintf(foo,"Last polled: %u\n", dpy->lastpoll);
      strcat(tpdu->argbuf,foo);
      sprintf(foo,"State: %u\n\n", dpy->state);
      strcat(tpdu->argbuf,foo);
      gl = gl->next;
    }
    fprintf(stderr,"Sending result to socket(%s)\n", text);
    g_io_channel_write(channel, tpdu, sizeof(trappdu), &num_write);
  break;
#if 0
  case PDU_NETMON:
    memcpy(&netmon_msg, pipestring+sizeof(gint), sizeof(gint));
    memcpy(&netmon_host, pipestring+sizeof(gint)*2, sizeof(gint));
    memcpy(&netmon_addr, pipestring+sizeof(gint)*3, sizeof(gint));
    netmon_ip = inet_ntoa(netmon_addr);
    fprintf(stderr, "Host %d, IP %s, msg %u\n", netmon_host, netmon_ip, netmon_msg);
    senddsocket(trapdsock, enviroment.evdpysrvd, pipestring, nbytes);       

  break;
#endif
  case PDU_TRAP:
fprintf(stderr,"EVTRAP\n");
    traplen = g_trap_decode(tpdu, &trap);

    gl = xdpygl;
    while(gl){
      dpy = gl->data;
      fprintf(stderr,"EVTRAPSEND\n");
      g_io_channel_write(dpy->channel, tpdu, sizeof(trappdu), &num_write);
      gl = gl->next;
    }
    g_trap_free(&trap);
  break;
  default:
    /* pdutype doesn't belong to evdpyrsvd. */
  break;
  } /* end of while pdutype */
  
}

void
xdpy_activity(gint type, GIOChannel *channel, trappdu *tpdu)
{
  xdpy *evdpy;

  fprintf(stderr,"XDPY(%u)", type);

  switch(type){
    case 0:
      evdpy = g_malloc0(sizeof(xdpy));
      evdpy->channel = channel;
      xdpygl = g_list_append(xdpygl, evdpy);
    break;
    case 1:
      xdpy_connection_mgmt (); /* Lookup through ALL CSBs */
      evdpysrvd_services (channel, tpdu);  /* process user data */
    break;
  }
}
 
void
trapd_activity (gint type, GIOChannel *channel, trappdu *tpdu)
{

  switch(type){
    case 2:       /* Peer has disconnected */
    quit();
  }

  xdpy_connection_mgmt ();
  evdpysrvd_services (channel, tpdu);

}

