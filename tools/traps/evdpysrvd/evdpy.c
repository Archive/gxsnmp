/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* evdpysrvd.c -- Event Display Server Daemon
*
*/

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>
#include "dae.h"
#include "evdpysrvd.h"

char evdpy_version[] = "$Id$"; /*usefull to 'strings' the binary*/

void xdpy_free(xdpy *dpy)
{
  g_free(dpy->address);
  g_free(dpy);
}

gint xdpy_add(gchar *address){	/*adds an remote display to the xdpy-struct*/
xdpy *dpy;

  if(!(dpy = g_malloc(sizeof(xdpy)))){
    return -1;
  }
  dpy->address = g_strdup(address);
  dpy->state = 1;		/* State is alive */
  dpy->lastpoll = currt;		/* last polled is current time */

  xdpygl = g_list_append(xdpygl, dpy);
  return 0;
}

gint xdpy_del(gchar *address){	/*removes an remote display to the xdpy-struct*/
GList *gl;
xdpy *dpy;

  gl = xdpygl;

  while(gl){
    dpy = gl->data;
    if(!strcmp(dpy->address,address)){
      xdpygl = g_list_remove_link(xdpygl, gl);
      xdpy_free(dpy);
      return 1;
    }	
    gl = gl->next;
  }
  return 0;
}
void xdpy_reset(gchar *address)
{
GList *gl;
xdpy *dpy;

  gl = xdpygl;

  while(gl){
    dpy = gl->data;
    if(!strcmp(dpy->address,address)){
      dpy->state = 1;				/*far end evdpy is set to alive*/
      dpy->lastpoll = currt;			/*set date when we last polled */
      return;
    }	
    gl = gl->next;
  }
}

void xdpy_connection_mgmt ()
{
  trappdu tpdu;
  xdpy *dpy;
  GList * gl;
  gint num_write;

  currt = time(0);

  gl = xdpygl;
  while(gl){    /* go through all registered displays and check if they are alive*/
    dpy = gl->data;
    if(((currt - dpy->lastpoll)> EVPOLL) && dpy->state == 1){
      /*this evdpy hasn't been polled for a while*/
      tpdu.type = PDU_POLL;
      g_snprintf(tpdu.argbuf,1000,"%s", dpy->address);                          /*GetStatus*/
      g_io_channel_write(dpy->channel, &tpdu, sizeof(trappdu), &num_write);
      dpy->state = 2;           /*pending state (evdpysrvd waiting for reply)*/
    }
    else if(((currt - dpy->lastpoll) > EVPOLL ) && dpy->state == 2){
      /*this evdpy hasn't answered a poll, within time
        and it will be removed. */
      xdpy_del(dpy->address);
    }
    gl = gl->next;
  }
}
