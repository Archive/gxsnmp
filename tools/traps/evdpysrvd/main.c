/*  $Id$
*
* Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
* evdpysrvd.c -- Event Display Server Daemon
*
*/

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>


#include <orb/orbit.h>
#include <liboaf/liboaf.h>
#include "gxsnmp.h"

#include "dae.h"
#include "evdpysrvd.h"

char main_version[] = "$Id$"; /*usefull to 'strings' the binary*/


glb_enviroment enviroment;
int currt;                                               /*holds current time() */
FILE *fpevdpysrvd;
aclist *trapfilter;
GList *xdpygl = NULL;            /* list of connected event displays */
gint evdpysrvdsock;
gint debug_level;
GIOChannel *trapd_channel;

void
quit()
{
  logstr(fpevdpysrvd,"Evdpysrvd stoped.\n");
  fclose(fpevdpysrvd);
  _exit(0);

}

int main(int argc, char **argv){
  trappdu tpdu;
  GMainLoop    *loop;
  gint num_write;


  CORBA_Environment ev;
  CORBA_ORB orb;
  GxSNMP_TRAP gsqldbclient;
  PortableServer_POA poa;
  impl_POA_GxSNMP_TRAP *servant;
  GxSNMP_TRAP object;


/**** START CORBA INTERFACE ****/
  oaf_init(argc,argv); CORBA_exception_init (&ev);
  orb = oaf_orb_get(); poa = (PortableServer_POA)
  CORBA_ORB_resolve_initial_references (orb, "RootPOA", &ev);
  if (ev._major != CORBA_NO_EXCEPTION) {
    fprintf (stderr, "Error: %s\n", CORBA_exception_id(&ev));
    _exit (1);
  }
  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);
  if (ev._major != CORBA_NO_EXCEPTION) {
    fprintf (stderr, "Error: %s\n", CORBA_exception_id(&ev));
    _exit (-1);
  }
  servant = impl__TRAPCreate (poa, &ev);
  object = PortableServer_POA_servant_to_reference (poa, servant, &ev);
  switch(
    oaf_active_server_register(
    "OAFIID:gxsnmp-event_display_server:ee8e6ae0-c604-11d4-9f31-0800208225d2"
    , object)){
      case OAF_REG_SUCCESS:
      break;
      case OAF_REG_NOT_LISTED:
        fprintf(stderr,"OAF_REG_NOT_LISTED\n");
      break;
      case OAF_REG_ALREADY_ACTIVE:
        fprintf(stderr,"OAF_REG_ALREADY_ACTIVE\n");
      break;
      case OAF_REG_ERROR:
        fprintf(stderr,"OAF_REG_ERROR\n");
      break;
  }
  CORBA_Object_release (object, &ev);

/****************************************/


#if 0
  if(fork() != 0) _exit(0);
  if(setsid() == -1) _exit(0);
  if(fork() != 0) _exit(0);
  umask(777);
  if(chdir("/")<0) _exit(0);
  for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
  open("/dev/null",O_RDWR); dup(0); dup(0);
#endif

  load_environment(&enviroment);
  xdpygl = NULL;
  fpevdpysrvd = logopen("evdpysrvd.log");  /*start logging*/

  dd_connect_as_server (4003, xdpy_activity, NULL);
  trapd_channel = dd_connect_as_client ("127.0.0.1", 4002, trapd_activity, NULL);
  sleep(1); /* Wait for trapd to accept connection */
  /** Register evdpysrvd in trapd daemon **/
  tpdu.type = PDU_PROGRAM_REGISTER;
  tpdu.severity = GXSNMP_PROGRAM_EVDPYSRVD;
  strcpy(tpdu.argbuf, "evdpysrvd");
  g_io_channel_write(trapd_channel, &tpdu, sizeof(trappdu), &num_write);


  logstr(fpevdpysrvd,"Evdpysrvd started.\n");

  loop = g_main_new (TRUE);
  while (g_main_is_running (loop))
    {
      g_main_iteration(1);
    }
  g_main_destroy (loop);
  return 0;
}
