/*  $Id$
*
*  Copyright 2000 Larry Liimatainen
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*
*
*/

#include <glib.h>

#define EVPOLL 300 /*poll time gap when polling event displays*/

typedef struct _xdpy{			/* Hold remote event display info*/
        GIOChannel * channel;
	gchar *address;		/* this is a file name when using AF_UNIX*/
	time_t lastpoll;	/* when did we last poll this evdpy*/
	gint state;		/* state of evdpy */
} xdpy;				

extern glb_enviroment enviroment;
extern int currt;						/*holds current time() */
extern FILE *fpevdpysrvd;
extern aclist *trapfilter;
extern GList *xdpygl;		/* list of connected event displays */


/****** PROTOTYPE *******/
      void  restart              ();
      void  xdpy_activity        ( gint type, GIOChannel * channel, trappdu * tpdu );
      void  trapd_activity       ( gint type, GIOChannel * channel, trappdu * tpdu );
      gint  xdpy_add             (gchar *address);
      void  xdpy_free            (xdpy *dpy);
      gint  xdpy_del             (gchar *address);
      void  xdpy_reset           (gchar *address);
      void  xdpy_connection_mgmt ();

/************************/

