trap.conf(1)		daemons                  trap.conf(1)

NAME
	trap.conf

DESCRIPTION
	When trapd(1) and gxevents(1) starts they
	will read in trap.conf that specifies what
	to do with a incoming trap.

	trap.conf resides under ./conf sub-directory
	of installation directory.

	The format of trap.conf is as
	follows:

	<OID> <G> <S> <Name> <Script> <Popup> <EID> <FLAG> <Severity>

	<OID> - This is the traps Object ID, example
	.1.3.6.1.2.1.1.1 specifies an mib-ii interface
	context.

	<G> - This is the Generic trap number
	<S> - This is the Specific trap number
	<Name> - This is the name gxevents will use when
	displaying the trap.
	<Script> - filename of a script to execute when
        trapd is receiving a trap. You can use following
        variables in the string: $IP, $ARG1-4.

	<Popup> - This text string will be showed to the user.
	Following variables can be used in the string:
	$IP - IP address of trap
	$ARG1-4 - Four arguments is supported.

	<EID> - unused

	<FLAG> - This is an arithmetic number, where:
	1 bit is forward to corrd
	2 bit is forward to evdpysrvd
	3 bit is log trap to log/trapd.log

	<Severity> - This determines what severity level 
	the trap should have.  See nb.conf(section) how 
	to set default trap severity level.
	These severity levels can be assigned a trap.
	64 - Critical
	48 - Alarm
	32 - Warning
	16 - Informative
	 0 - Unknown

EXAMPLES
	This identifies Cisco up and down traps.
	.1.3.6.1.4.1.9.5.17 2 0 "/home/foo/alarm.sh $IP $ARG4" "$ARG4 at $IP is down!" 0 3 4 
	.1.3.6.1.4.1.9.5.17 2 0 "" "System  is down!" 0 3 4

SEE ALSO
	trapd(1)
