/* this is a debug tool, for sending data to daemon sockets */
#include <stddef.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#include "dae.h"

//globals
aclist *trapfilter;
int gxsocket;
char gxsocketfile[] = "/var/mydpy";
glb_enviroment enviroment;
//_________________________________________
#include "glb_conf.c"
#include "soc.c"

main(int argc, char **argv)
{   
char line[2000];
char objna[] = ".1.3.6.1.2.1.1.1";

  init_mib();
  line[0] = 0;
  oid2text(line, objna);
  fprintf(stderr,"OBJ%s\n",line);
  exit(0);

	unlink(gxsocketfile);
	gxsocket = createsocket(gxsocketfile,0);
	load_enviroment();

		strcpy(line,"open /tmp/mydpy");
		sendsocket(gxsocket,enviroment.evdpysrvd,line);

quit:
	unlink(gxsocketfile);
	exit(0);
}
