/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * trap.conf file front end
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stddef.h>	//six row downs is needed by socket_functions
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>

#include <gnome.h>
#include "config.h"
#include <sys/types.h>
#include "events.h"

#include "sys/stat.h"
#include "dae.h"

char version[] = "$Id$"; /*usefull to 'strings' the binary*/

aclist *trapfilter; /*unused, but needed by libdae*/
void quit_cb();

int get_line_cnt, get_line_base;
glb_enviroment enviroment;

int trapselfill = 0;		/*trapsel structure is filled*/
trap_slot trapsel;		/*this is the current selected trap*/
GtkCList *oidlist;
GtkCList *traplist;

GtkWidget *mod_win;		/*modify window*/
GtkWidget *mod_vbox;
GtkWidget *mod_table;
GtkWidget *mod_name;
GtkWidget *mod_popup;		/*modify popup text*/
GtkWidget *mod_script;		/*modify script text*/
GtkWidget *mod_flag1;		/*flag bit 1 - forward to corrd*/
GtkWidget *mod_flag2;		/*flag bit 2 - forward to corrd*/
GtkWidget *mod_flag3;		/*flag bit 3 - forward to corrd*/

char *oidtext[4];		/*number is number of columns in oidlist*/
char *traptext[4];		/*number is number of columns in traplist*/
char text1[80];			/*holds some text*/
char text2[80];
char text3[80];
char text4[80];
char text5[80];

/*widgets callback*/
void quit_cb(){
  exit(0);
}

void buttoncb(GtkWidget *wid){
/*
  if(GTK_TOGGLE_BUTTON(wid)->active){
    cfg.utrap = 1;
  } else {
    cfg.utrap = 0;
  }
fprintf(stderr,"LUT(%d)\n",cfg.utrap);
*/
}

void list_reload(){
int i;  
char oldoid[130];

  gtk_clist_freeze(oidlist);					/*reload oidlist*/
  gtk_clist_clear(oidlist);
  for(i=1;strlen(enviroment.traptab[i].oid);i++){
    if(!strcmp(enviroment.traptab[i].oid,"NULL")) continue;	/*skip empty slots*/
    if(!strcmp(enviroment.traptab[i].oid,oldoid)) continue;	/*skip old slots*/
    oidtext[0] = enviroment.traptab[i].oid;
    strcpy(oldoid,oidtext[0]);					/*save as old*/
    gtk_clist_append(oidlist, oidtext);
  }
  gtk_clist_thaw(oidlist);

  gtk_clist_clear(traplist);					/*this version clears trap list whenever deleting*/
  trapselfill = 0;
}

void oidselect_cb(GtkWidget *oidlist, gint row, gint column, GdkEventButton *event, gpointer data){
gchar *oid;
int a;

  gtk_clist_get_text(GTK_CLIST(oidlist), row, column, &oid);
  gtk_clist_freeze(traplist);
  gtk_clist_clear(traplist);
  strcpy(trapsel.oid,oid);
  for(a=0;strlen(enviroment.traptab[a].oid);a++){
    if(strcmp(oid,enviroment.traptab[a].oid)) continue;
    sprintf(text1,"%d",enviroment.traptab[a].g);
    sprintf(text2,"%d",enviroment.traptab[a].s);
    traptext[0] = text1;
    traptext[1] = text2;
    gtk_clist_append(traplist,traptext);
  }
  gtk_clist_thaw(traplist);
  trapsel.g = 0;
  trapsel.s = 0;
  trapselfill = 0;
}

void trapselect_cb(GtkWidget *oidlist, gint row, gint column, GdkEventButton *event, gpointer data){
gchar *trap;
int n;

  gtk_clist_get_text(GTK_CLIST(oidlist), row, 0, &trap);
  trapsel.g = atoi(trap);
  gtk_clist_get_text(GTK_CLIST(oidlist), row, 1, &trap);
  trapsel.s = atoi(trap);
  for(n=0;strlen(enviroment.traptab[n].oid);n++){
    if(strcmp(trapsel.oid,enviroment.traptab[n].oid)) continue;
    if(trapsel.g != enviroment.traptab[n].g) continue;
    if(trapsel.s != enviroment.traptab[n].s) continue;
    strcpy(trapsel.name,enviroment.traptab[n].name);                   /*fill in rest of selected trap data*/
    strcpy(trapsel.popup,enviroment.traptab[n].popup);
    strcpy(trapsel.script,enviroment.traptab[n].script);
    trapsel.flag = enviroment.traptab[n].flag;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag1), trapsel.flag & 1);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag2), trapsel.flag & 2);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag3), trapsel.flag & 4);
  }

  trapselfill = 1;
}

void add_trap_cb(){	/*use modifies window to add. clear all old data before showing*/
  gtk_entry_set_text(mod_popup,0);
  gtk_entry_set_text(mod_script,0);
  gtk_widget_show_all(mod_win);
}

void del_trap_cb(){
int n;

  if(!trapselfill) return;
  for(n=0;strlen(enviroment.traptab[n].oid);n++){
    if(strcmp(trapsel.oid,enviroment.traptab[n].oid)) continue;
    if(trapsel.g != enviroment.traptab[n].g) continue;
    if(trapsel.s != enviroment.traptab[n].s) continue;
    strcpy(enviroment.traptab[n].oid,"NULL");			/*Set this slot as empty*/
  }
  list_reload();
}

void modify_trap_cb(){
int n;

  if(!trapselfill) return;
  gtk_entry_set_text(mod_name,trapsel.name);
  gtk_entry_set_text(mod_popup,trapsel.popup);
  gtk_entry_set_text(mod_script,trapsel.script);
  trapselfill = 0;
  gtk_widget_show_all(mod_win);
}

void mod_bit1_cb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    trapsel.flag |= 1;				/*set bit 1 - forward to corrd*/
  } else {
    trapsel.flag &= 254;			/*clear bit 1*/
  }
}

void mod_bit2_cb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    trapsel.flag |= 2;				/*set bit 1 - forward to corrd*/
  } else {
    trapsel.flag &= 253;			/*clear bit 1*/
  }
}

void mod_bit3_cb(GtkWidget *wid){
  if(GTK_TOGGLE_BUTTON(wid)->active){
    trapsel.flag |= 4;				/*set bit 1 - forward to corrd*/
  } else {
    trapsel.flag &= 251;			/*clear bit 1*/
  }
}

void mod_cancel_cb(){
  gtk_widget_hide_all(mod_win);
}
void mod_ok_cb(){
gchar *entry;
  mod_cancel_cb();

  entry = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(mod_name)->entry));
  fprintf(stderr,"Name(%s)\n",entry);

  entry = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(mod_popup)->entry));
  fprintf(stderr,"Popup(%s)\n",entry);

  entry = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(mod_script)->entry));
  fprintf(stderr,"Script(%s)\n",entry);
}



GtkWidget *gtkcreate_query(GtkWidget *table, char *text,int type, int x, int y, void *callback, void *data, int data2 ){ /*this function creates a checkbutton or a textinput .. with label and callbacks */
GtkWidget *lab;
GtkWidget *wid;

  lab = gtk_label_new(text);
  gtk_table_attach (GTK_TABLE (table), lab, 1, 2, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
  gtk_widget_show(lab);
  if(type == 1){
    wid = gtk_entry_new_with_max_length(data2);
    gtk_signal_connect(GTK_OBJECT(wid), "activate", GTK_SIGNAL_FUNC(callback), wid);
    gtk_table_attach(GTK_TABLE (table), wid, 2, 3, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
    gtk_entry_set_text(wid,data);
    gtk_widget_show(wid);
    return wid;
  }else if (type == 2){
    wid = gtk_toggle_button_new();
    gtk_table_attach (GTK_TABLE (table), wid, 2, 3, y, y+1, GTK_SHRINK, GTK_SHRINK, 2, 1);
    gtk_signal_connect_object(GTK_OBJECT(wid), "clicked", GTK_SIGNAL_FUNC(callback),GTK_OBJECT(wid));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wid), data2);
    gtk_widget_show(wid);
  }else if (type == 3){
    wid = gtk_button_new_with_label(text);
    //gtk_object_set_data (GTK_OBJECT(table), "foo", wid);
    gtk_table_attach (GTK_TABLE (table), wid, x, y, x+1, y+1, GTK_SHRINK , GTK_SHRINK, 2, 1);
    gtk_signal_connect(GTK_OBJECT(wid), "clicked",GTK_SIGNAL_FUNC(callback),(gpointer) "savecfg");
  }
}
int main(int argc, char **argv)
{
  GtkWidget   *config;
  GtkWidget   *button;
  GtkWidget   *vbox;
  GtkWidget   *bar;
  GtkWidget   *table;
  GtkWidget   *oidwindow;
  GtkWidget   *trapwindow;
  int          n,modn=0,i, tmp, w_size;
  gint        char_size;

  if(load_enviroment()){
    fprintf(stderr,"gxdaeconf: Error, couldn't load enviromental file.\n");
    exit(0);
  }
  load_trapconf();	/*load trap definition file*/

/* check arguments if exist set as selected item and go into modify mode */
  if(argc == 4){
    strcpy(trapsel.oid, argv[1]);
    trapsel.g = atoi(argv[2]);
    trapsel.s = atoi(argv[3]);
    trapsel.name[0] = 0;						/*set empty to check if trapsel.oid/g/s exists in trap.conf*/
fprintf(stderr,"!");
    for(n=0;strlen(enviroment.traptab[n].oid);n++){
      if(strcmp(trapsel.oid,enviroment.traptab[n].oid)) continue;
      if(trapsel.g != enviroment.traptab[n].g) continue;
      if(trapsel.s != enviroment.traptab[n].s) continue;
      strcpy(trapsel.name,enviroment.traptab[n].name);                   /*fill in rest of selected trap data*/
      strcpy(trapsel.popup,enviroment.traptab[n].popup);
      strcpy(trapsel.script,enviroment.traptab[n].script);
      trapsel.flag = enviroment.traptab[n].flag;
    }
    if(strlen(trapsel.name)) modn++;	/*if trap specified by arguments was found in trap.conf lets go into modify mode*/
  }

  gtk_init(&argc, &argv);
  config = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(config), vbox);
  gtk_widget_show(vbox);

  table = gtk_table_new (3, 3, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 3);
  tmp = gdk_text_width (vbox->style->font, "1", 1);
  tmp = tmp * 18;
  w_size = 0;
  char_size = gdk_string_width (table->style->font, "xW") / 2;

  button = gtk_button_new_with_label (_("Save, reload & quit"));
  gtk_object_set_data (GTK_OBJECT (config), "ack_button", button);
  gtk_table_attach (GTK_TABLE (table), button, 0, 1, 0, 1, GTK_FILL | GTK_SHRINK, GTK_SHRINK, 1, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(quit_cb),(gpointer) "savecfg");
  button = gtk_button_new_with_label (_("Dismiss"));
  gtk_object_set_data (GTK_OBJECT (config), "ack_button", button);
  gtk_table_attach (GTK_TABLE (table), button, 1, 2, 0, 1, GTK_FILL | GTK_SHRINK, GTK_SHRINK, 1, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(quit_cb),(gpointer) "savecfg");

  bar = gtk_hseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 0, 3, 1, 2, GTK_FILL, GTK_FILL | GTK_EXPAND, 2, 1);
  bar = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 0, 1, 0, 7, GTK_FILL, GTK_FILL | GTK_EXPAND, 2, 1);

  gtkcreate_query(table, "Add", 3, 1, 2, add_trap_cb, 0, 0);
  gtkcreate_query(table, "Modify", 3, 1, 3, modify_trap_cb, 0, 0);
  gtkcreate_query(table, "Delete", 3, 1, 4, del_trap_cb, 0, 0);

  oidlist = gtk_clist_new(1);
  gtk_signal_connect(GTK_OBJECT(oidlist), "select_row",GTK_SIGNAL_FUNC(oidselect_cb),NULL);
  gtk_clist_set_selection_mode(oidlist,GTK_SELECTION_SINGLE);
  gtk_clist_column_titles_hide(oidlist);
  gtk_clist_set_column_width(oidlist,0,200);

  oidwindow = gtk_scrolled_window_new(0, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (oidwindow),  GTK_POLICY_ALWAYS,  GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (oidwindow), oidlist);
  gtk_table_attach (GTK_TABLE (table), oidwindow, 0, 1, 3, 5, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 2, 1);

  traplist = gtk_clist_new(2);
  gtk_signal_connect(GTK_OBJECT(traplist), "select_row",GTK_SIGNAL_FUNC(trapselect_cb),NULL);
  gtk_clist_set_selection_mode(traplist,GTK_SELECTION_SINGLE);
  gtk_clist_column_titles_hide(traplist);
  gtk_clist_set_column_width(traplist,0,100);
  gtk_clist_set_column_width(traplist,1,100);

  trapwindow = gtk_scrolled_window_new(0, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(trapwindow),  GTK_POLICY_ALWAYS,  GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (trapwindow), traplist);
  gtk_table_attach (GTK_TABLE (table), trapwindow, 0, 1, 5, 7, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 2, 1);

  list_reload();
/***********create modify window************/
  mod_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  mod_vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(mod_win), mod_vbox);
  gtk_widget_show(mod_vbox);

  mod_table = gtk_table_new (3, 6, FALSE);
  gtk_box_pack_start (GTK_BOX (mod_vbox), mod_table, TRUE, TRUE, 3);

  mod_name = gtkcreate_query(mod_table, "Name", 1, 0, 0, quit_cb, 0, 0);
  mod_popup = gtkcreate_query(mod_table, "Popup", 1, 0, 1, quit_cb, 0, 0);
  mod_script = gtkcreate_query(mod_table, "Command", 1, 0, 2, quit_cb, 0, 0);

  mod_flag1 = gtkcreate_query(mod_table, "Correlate", 2, 0, 3, mod_bit1_cb, 0, 0);
  mod_flag2 = gtkcreate_query(mod_table, "Display", 2, 0, 4, mod_bit2_cb, 0, 0);
  mod_flag3 = gtkcreate_query(mod_table, "Log trap", 2, 0, 5, mod_bit3_cb, 0, 0);

  gtkcreate_query(mod_table, "Ok", 3, 0, 6, mod_ok_cb, 0, 0);
  gtkcreate_query(mod_table, "Cancel", 3, 5, 10, mod_cancel_cb, 0, 0);

/*******************************************/
  gtk_widget_show_all (config);
  if(modn){  /*command line mode, modify trap window*/
    trapselfill = 1;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag1), trapsel.flag & 1);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag2), trapsel.flag & 2);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mod_flag3), trapsel.flag & 4);
    modify_trap_cb();
  }
  gtk_main();
}
