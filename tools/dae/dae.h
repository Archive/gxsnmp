typedef struct root_oids_ { /*is this obsolete?*/
	char name[40];
	char prefix[60];
	int dii;
} oid_slot;

typedef struct _trap_slot{	/*this struct will in future only contain trap.conf fields*/
//	int timestamp;		/*date trap arrive*/
//	char date[30];		/*date trap arrive*/
//	char IP[60];		/*system name*/
	char oid[130];		/*OID*/
	int g;			/*generic*/
	int s;			/*specific*/
	char name[80];		/*trap name */
	char script[80];	/*script to run*/
	char popup[80];		/*text to pop up*/
	int eid;		/*event ID to run*/
	int flag;		/*this is used for setting/retrieving single bit fields*/
	int severity;		/*set severity according to oid+g+s in trap.conf*/
//	char argv[3][130];
} trap_slot;

typedef struct _trap_{	/*this field should be used to hold traps */
        int timestamp;          /*date trap arrive*/
        char date[30];          /*date trap arrive*/
        char IP[60];            /*system name*/
        char oid[130];          /*OID*/
        char noid[200];         /*text OID*/
        int g;                  /*generic*/
        int s;                  /*specific*/
        char name[80];          /*trap name */
        char script[80];        /*script to run*/
        char popup[80];         /*text to pop up*/
        int eid;                /*event ID to run*/
        int flag;               /*this is used for setting/retrieving single bit fields*/
        int severity;           /*set severity according to oid+g+s in trap.conf*/
        char argoid[3][130];	/*due to this big array, don't alloc to much of this struct type :) */
        char argdat[3][200];	/*due to this big array, don't alloc to much of this struct type :) */
} trapslot;

typedef struct _corr_slot{
	char mask[20];			/*IP-mask*/
	char file[20];			/*correlation file to run*/
}corr_slot;

typedef struct {
	char host[30];			/* database host*/
	char name[80];			/* database name*/
	char user[40];			/* database user*/
	char passwd[40];		/* database pass*/
	char base[100];			/* dirpath*/
	oid_slot *mibtab;		/* mib table pointer*/
	trap_slot *traptab;		/* loaded trap.conf pointer*/
	char *popup_exec;		/* executable to use when popping up a message*/
	char *actiond;			/* action daemon socket pathname*/
	char *corrd;			/* daemon socket pathname*/
	char *crond;
	char *evdpysrvd;
	char *gxdhd;
	char *pdud;
	char *trapd;
	char *gxdd;
	int corrd_log;
	int evdpysrvd_log;
	int trapd_unknown_traps;
	int trapd_log_unknown_traps;
	int evdpysrvd_poll;
	int severity;
	int gxevents_max_traps;
	int gxevents_lookup;        /*lookup hostname from IP*/
	int popup_date;			/*boolean, should Popups contain a date*/
	}glb_enviroment;


/*
access list action codes:
10	deny oid
13	deny oid+g+s
20	permit oid
23	permit oid+g+s
*/

typedef struct _aclist{
	int action;
	char oid[128];
	char ip[16];
	char ipm[16];
	int severity;
	int g;
	int s;
}aclist;

/*CODE NOT IN DAE.C*/
extern void sprint_objid(char *, int , gulong *, int);			/*this is a function in lib/mib.c*/
extern void init_mib(void);
/*GENERIC FUNCTIONS ( maybe glib can replace some of them )*/
extern char *load_file(char *);
extern char *wordcpy(char *, char *, int );
extern int load_enviroment();
extern char *wordcpyt(char *, char *, char *, int );
extern void restrap(char *);
extern void klocka(char *tid[40]);
extern int conv_a_i(char *);
extern char *sls(char*);
extern void logstr(FILE *, char *);
extern FILE *logopen(char *);
extern int var_interpret(gchar *, gchar *, int, trapslot *);
/*TRAP.CONF DATABASE FUNCTIONS*/
extern int trap_lookup(trapslot *);
/*TRAP FILTER ( ACCESS CONTROL )*/
extern int test_acl(aclist, aclist *);
extern aclist *load_acl(char *);
extern int save_acl(char *);
extern int acl_add_filter(aclist, char *);
extern int oid2text(char *, char *);
extern int load_trapconf();
extern corr_slot *load_corrconf();
/*TRAP PARSING*/
extern int trap_decode(char *, trapslot *);
/*SOCKET FUNCTIONS*/
extern int createsocket(const char *, int);
extern int sendsocket(int, char *, char *);
extern int senddsocket(int, char *, char *, size_t);
/*database daemon*/
extern int dd_connect(char *);
extern void dd_disconnect(int);
extern char *dd_read(int, int *);
extern int dd_write(int, int, char *);
