#!/usr/bin/perl
#
#this script converts NetView 5.x /usr/OV/conf/C/trapd.conf
#to GxSNMP formatted gxsnmp/tools/dae/conf/trap.conf file.
#to run this script:
#stand in /usr/OV/conf/C
#'./trap2dae.pl'
#
# the script outputs to STDOUT so you redirect it to your new trap.conf file
#
# 1999-10-01 remlali@remsa.ericsson.se

open(DAT, "./trapd.conf");
while($l=<DAT>){
chop $l;
if($l =~ /{.*}/){
@b=split(' ',$l);
$name=@b[0]; $oid=@b[1]; $g=@b[2]; $s=@b[3];
if($g eq ""){next;}; if($s eq ""){next;}
popmore:
$c=<DAT>;
chop $c;
if($c =~ /SDESC/){ goto nextdef; }
if($c =~ /DISPLAY/){
$c =~ s/^DISPLAY (.*)$/$1/;
$c =~ s/\$A/\$IP/;
$c =~ s/\$1/\$ARG1/;
$c =~ s/\$2/\$ARG2/;
$c =~ s/\$3/\$ARG3/;
$c =~ s/\$4/\$ARG4/;
$oid =~ s/[\{\}]//g;
$oid =~ s/^/./g;
print "$oid $g $s \"$name\" \"\" \"$c\" 0 0 0\n";
}
goto popmore;
nextdef:
}
}
close DAT;
