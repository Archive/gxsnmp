/*
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * gxecho utility argument 1 is text string to display, every thing else is undefined
 * This routine is slower than doing xterm -c 'echo "mess";read'
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stddef.h>	//six row downs is needed by socket_functions
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <sys/types.h>
#include "sys/stat.h"
#include <gtk/gtk.h>

char version[] = "$Id$"; /*usefull to 'strings' the binary*/

void quit_cb(){
  exit(0);
}

int main(int argc, char **argv)
{
  GtkWidget   *window;
  GtkWidget   *button;
  GtkWidget   *label;
  GtkWidget   *vbox;
  GtkWidget   *bar;

GdkVisual *visual;
GdkColormap *colormap;
GdkColor black = { 0,0,0,0 };                /*textcolor*/
GdkColor red = { 0,255,0,0 };                  /*critical*/
GdkColor green = { 0,0,255,0 };
GdkColor yellow = { 0,0,255,255 };
GdkColor amber = { 0,0,128,128 };
GdkColor white = { 0,255,255,255 };                /*informational*/

  gtk_init(&argc, &argv);
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  visual = gdk_visual_get_best();
  colormap = gtk_widget_get_colormap(window);
  gdk_colormap_alloc_color(colormap, &black,TRUE,TRUE);
  gdk_colormap_alloc_color(colormap, &red,TRUE,TRUE);  
  gdk_colormap_alloc_color(colormap, &yellow,TRUE,TRUE);
  gdk_colormap_alloc_color(colormap, &amber,TRUE,TRUE); 
  gdk_colormap_alloc_color(colormap, &green,TRUE,TRUE);
  gdk_colormap_alloc_color(colormap, &white,TRUE,TRUE);

  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(window), vbox);
  gtk_widget_show(vbox);

  label = gtk_label_new(argv[1]);
  gtk_container_add(GTK_CONTAINER(vbox), label);
  button = gtk_button_new_with_label ("Dismiss");
  gtk_signal_connect(GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(quit_cb),(gpointer) "quit");
  gtk_container_add(GTK_CONTAINER(vbox), button);
  gtk_widget_show_all (window);
  gtk_main();
}
