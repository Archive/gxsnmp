#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <glib.h>


#include "dae.h"

char version[] = "$Id$"; /*usefull to 'strings' the binary*/

aclist *trapfilter;
int gxsocket;
char gxsocketfile[] = "/gxsfile";
glb_enviroment enviroment;
char pipestring[400];

char usage[] = "usage: gxstatus status|start|stop|reload [daemon]\n";


main(int argc, char **argv)
{   
int nbytes;
char line[200];

	unlink(gxsocketfile);
	gxsocket = createsocket(gxsocketfile,0);
	if(load_enviroment()){
		fprintf(stderr,"gxstatus: Error, couldn't load enviromental files.\n Please check that you are standing in the directory where gxstatus lives.\n");
		goto quit;
	}

	if(argc == 1 || !strcmp(argv[1],"status")){		/* gxstatus */
		sendsocket(gxsocket,enviroment.gxdhd,"status /gxsfile"); /*ask gxdhd of status of all daemons */
		printf("Wait..\n");
		nbytes = recv(gxsocket,pipestring,300,0);	 /*wait for reply */
		printf("Status:\n%s\n",pipestring);
		goto quit;
	}


	else if(!strcmp(argv[1],"stop") && argc == 2){		/* gxstatus stop */
		sendsocket(gxsocket,enviroment.gxdhd,"kill ");
		goto quit;
	}
	else if(!strcmp(argv[1],"start") && argc == 2){		/* gxstatus start */
	fprintf(stderr,"Starting all daemons\n");
		strcpy(line,enviroment.base);
		strcat(line,"/gxdhd");
		execl(line,NULL);
		goto quit;
        }
	else if(!strcmp(argv[1],"start") && argc == 3){		/* gxstatus start daemon */
		sprintf(line,"start %s",argv[2]);
		sendsocket(gxsocket,enviroment.gxdhd,line);
		goto quit;
        }
	else if(!strcmp(argv[1],"stop") && argc == 3){		/* gxstatus stop daemon */
		sprintf(line,"stop %s",argv[2]);
		sendsocket(gxsocket,enviroment.gxdhd,line);
		goto quit;
        }
	else if(!strcmp(argv[1],"reload") && argc == 3){	/* gxstatus reload daemon */
		sprintf(line,"reload %s",argv[2]);
		sendsocket(gxsocket,enviroment.gxdhd,line);
		goto quit;
        }

quit:
	unlink(gxsocketfile);
	exit(0);
}
