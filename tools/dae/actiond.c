#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <snmp/snmp.h>
#include "f.h"
//#include "snmp.h"
#include "scm.h"
#include "soc.c"

//GLOBALS
char pipestring[400];	//need to be a global cause fork-child uses this
glb_enviroment enviroment;
int actiondsock;
typedef struct _edr{	// this struct holds data from action-chain, used by original event
	int dii;
	char preset[200];
}event_edr;
trap_slot trap;		//this holds current incoming trap
int get_line_base, get_line_cnt;
db_init dbs;
scheme_event_status scheme;
FILE *forkfp;
int forkfdes[2];
//_______

//#include "sev.c"
#include "glb_conf.c"

int main(){
int i,el,estatus, w, j, c;
pid_t pid;	
char foo[40]; 
db_event event;
//FILE *eventd_fp_child;
int eid;
int nbytes;
struct sockaddr_un name;

	load_enviroment();

		strcpy(event.dfile,"/home/remlali/dae/scheme/corr.scm");
		strcpy(trap.IP,"130.100.31.200");
		strcpy(trap.oid,".1.3.6.1.2.1.1.1");
		trap.g = 6;
		trap.s = 2;
goto myrun;


	i = 3;
	j = sysconf(_SC_OPEN_MAX);
	while (i < j) close(i++);

	if(!(actiondsock = createsocket(enviroment.actiond,0))) _exit(0);				// create socket

	fprintf(stderr,"Actiond started.\n");
start:									//deamon-loop
	nbytes = recv(actiondsock,pipestring,100,0);

	if(nbytes < 0) goto start;
	if(!strncmp(pipestring,"kill",4)){
			unlink(enviroment.actiond);
			close(actiondsock);
			fprintf(stderr,"Actiond stoped.\n");
			_exit(0);
	}
	if(!strncmp(pipestring,"scheme#",6)){	//event
		strcpy(event.dfile,wordcpyt(foo,pipestring,"#",2));
		strcpy(trap.IP,wordcpyt(foo,pipestring,"#",3));
		strcpy(trap.oid,wordcpyt(foo,pipestring,"#",4));
		trap.g = atoi(wordcpyt(foo,pipestring,"#",5));
		trap.s = atoi(wordcpyt(foo,pipestring,"#",6));

myrun:
		pid = fork();
		switch(pid){
			case 0:			// launch event
				//exec_single_event(&event);
			break;
			case -1:		// fork failure
				//estatus = ptr->eid;
			break;
			default:		// don't wait for child ( preventing zombies )
		exit(0);
				waitpid(-1, NULL, WNOHANG );
		}
	}
	goto start;
}
