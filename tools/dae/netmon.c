/*
Network Monitor.
This daemon exists because: 
Keepalive plugin doesn't utilized ICMP.
Quicker to code from scratch. 
I don't wanna bloat main gxsnmp code.

So maybe it could be merged with keepalive, atleast this code is built looking at keepalive.
*/

#include <stddef.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>

#include "dae.h"
#include "g_sql.h"

#define MASK 8191 /* all-bits-set-mask, so MASK-8 will set all bits but bit 3 */

/* pingobj.state bits assignment:
Bit:	Set/Cleared
0	alive/dead
*/


aclist *trapfilter;
glb_enviroment enviroment;

typedef struct _pingobj{ 	/*define a own structure instead of using DB_interface, becaus this is more memory efficient*/
	int rowid;		/*Object ID*/
	int level;		/*topology level, lower is closer */
	int state;		/*current state of this object*/
	time_t time;		/*t_time this object changed into current state*/
	unsigned long int ip;	/*IP address in machine format*/
}pingobj;

pingobj *pinglist = 0;

int load_pinglist(){
//G_sql_query *dbq;			/*Database handle*/
gchar *query; 				/*QUERY TEXT*/
GSList *gl; 				/*temporary list to hold the interface SQL table*/
int l;

return 0;
/*
  if(!(gl = g_sqldb_table_list(interface_sqldb))) return 1;
  l = g_slist_length(gl);
  pinglist = malloc(sizeof(pingobj)*l);
  while(gl){
    pinglist->rowid = ((DB_interface *) gl->data)->rowid;
    pinglist->state = 0;
    
    gl = gl->next;
  }
  g_slist_free(gl);
  return 0;
*/
}

G_sql_backend dbb;

int main(){
char pipestring[400] = "";
trap_slot trap;
gchar engine[] = "MySQL";
gchar host[] = "130.100.31.248";
gchar user[] = "root";
gchar password[] = "";
gchar database[] = "gxsnmp";

/*
  gxsnmp_sql_db.engine = engine;
  gxsnmp_sql_db.host = host;
  gxsnmp_sql_db.port = 3306,
  gxsnmp_sql_db.user = user;
  gxsnmp_sql_db.password = password;

  gxsnmp_sql_database = database;
*/


  g_sql_register_backend(&dbb);

//  if(g_sqldb_table_load(interface_sqldb)) printf("!");

  load_pinglist();
  exit(0);
}
