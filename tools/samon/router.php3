<?
  header("Content-Type: image/gif");
  $img = imagecreatefromgif("router.gif");
  $red = imagecolorallocate($img, 255, 0, 0);
  $green = imagecolorallocate($img, 0, 128, 0);
  if (file_exists("/usr/httpd/mrtg/$router.status")) {
    imagestring($img, 3, 60, 25, $router, $green);
  } else {
    imagestring($img, 3, 60, 25, $router, $red);
  }
  imagegif($img);
?> 
