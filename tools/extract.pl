#!/usr/bin/perl

$count = 0;
$state = 0;

while (<>)
{
  if ($state == 0)
    {
      if (m/([a-zA-Z_\-0-9]*) *DEFINITIONS *::= *BEGIN/)
        {
          $state = 1;
          push (@line,$_);
          $count = 1;
          open (OUTFILE, ">$1");
        }
    }
  elsif ($state == 1)
    {
      if (m/^ *END/)
        {
          $state = 0;
          while (@line)
            {
              print OUTFILE shift(@line);
            }
          print OUTFILE $_;
        }
      else
        {
          if (($count == 8) &&
              ($line[0] =~ m/^[ \n\r]*$/) &&
              ($line[1] =~ m/^[ \n\r]*$/) &&
              ($line[2] =~ m/^[ \n\r]*$/) &&
              ($line[3] =~ m/Page/)       &&
              ($line[5] =~ m/RFC/)        &&
              ($line[6] =~ m/^[ \n\r]*$/) &&
              ($line[7] =~ m/^[ \n\r]*$/))
            {
              while (@line)
                {
                  shift(@line);
                }
              $count = 0;
            }
          if ($count == 8)
            {
              print OUTFILE shift(@line);
              $count = 7;
            }
          push(@line, $_);
          $count = $count + 1;
        }
    }
}
close(OUTFILE);
