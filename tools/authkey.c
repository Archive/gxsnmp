#include <g_snmp.h>
#include <string.h>
#include <stdio.h>

int
main(int argc, char *argv[])
{
  int i, cnt;
  guchar *ptr;
  guchar key[20];

  guchar engineid[32]; /* Max 32 bytes as per RFC */

  if (argc != 3) 
    {
       printf("Usage: %s password engineid\n", argv[0]);
       return 1;
    }

  cnt = 0;
  ptr = argv[2];
  while(*ptr)
    {
      if (cnt > 64)
        {
          printf("engineid too long\n");
          return 1;
        }
      switch(*ptr)
        {
          case ' ':
            ptr++;
            break;
          case '0'...'9':
            if (cnt % 2)
              {
                engineid[cnt/2] |= (*ptr & 0x0f);
              }
            else
              {
                engineid[cnt/2] = (*ptr & 0x0f) << 8;
              }
            ptr++;
            cnt++;
            break;
          case 'a'...'f':
          case 'A'...'F':
            if (cnt % 2)
              {
                engineid[cnt/2] |= ((*ptr & 0x0f) +9);
              }
            else
              {
                engineid[cnt/2] = ((*ptr & 0x0f) +9) << 8;
              }
            ptr++;
            cnt++;
            break;
          default:
            printf("Parse error in engineid at %c\n", *ptr);
            return 1;
        }
    }
  if (cnt % 2)
    {
      printf("Uneven number of characters in engineid\n");
      return 1;
    }

  printf("EngineID:          ");
  for (i=0;i<cnt/2;i++) printf("%02x ", engineid[i]);
  printf("\n");
 
  g_password_to_key_md5(argv[1], strlen(argv[1]), key);
  printf("MD5 key non-local: ");
  for (i=0;i<16;i++) printf("%02x ", key[i]);
  printf("\n");
  g_localize_key_md5(key, engineid, cnt/2);
  printf("MD5 key locatized: ");
  for (i=0;i<16;i++) printf("%02x ", key[i]);
  printf("\n");

  g_password_to_key_sha(argv[1], strlen(argv[1]), key);
  printf("SHA key non-local: ");
  for (i=0;i<20;i++) printf("%02x ", key[i]);
  printf("\n");
  g_localize_key_sha(key, engineid, cnt/2);
  printf("SHA key localized: ");
  for (i=0;i<20;i++) printf("%02x ", key[i]);
  printf("\n");

  return 0;
}
