/*
 * turtletalk.h : The definition of the communication between turtle master
 * and slave.
 */

struct _turtalkhdr {
    u_long counter;
    int length;
    char type;
    /* message payload */
} turttalkhdr;

/* Message types */
#define TURTALK_OPEN            1
#define TURTALK_MANAGE          2
#define TURTALK_KEEPALIVE       3
#define TURTALK_NOTIFICATION    4
#define TURTALK_HOST            5
#define TURTALK_NETWORK         6
#define TURTALK_POLL            7
#define TURTALK_RESET           8

/* OPEN Message options */
#define TURTALK_OPENOPT_AUTH    1

/* NOTIFICATION codes */
#define TURTALK_NOTF_MSGHDR     1
#define TURTALK_NOTF_OPEN       2

/* NOTIFICATION subcodes  message header*/
#define TURTALK_MSGHDR_SYNC     1
#define TURTALK_MSGHDR_LEN      2
#define TURTALK_MSGHDR_TYPE     3

/* subcodes for OPEN notifications */
#define TURTALK_NOPEN_BADVER     1
#define TURTALK_NOPEN_BADKEEP    2
#define TURTALK_NOPEN_BADOPT     3
#define TURTALK_NOPEN_AUTH       4


