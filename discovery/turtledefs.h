/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (C) 1999 Craig Small
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Variables and functions specific to the host panel
 */
#ifndef __TURTLEDEFS_H__
#define __TURTLEDEFS_H__

#include <netinet/in.h>

typedef struct __TurtleHost {
    struct in_addr address;
} TurtleHost;

struct gxnlkey {
    struct in_addr address;
    struct in_addr netmask;
};
#endif
