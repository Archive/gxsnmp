 /*
 * $Id$
 *                      P I N G . C
 *
 * Using the InterNet Control Message Protocol (ICMP) "ECHO" facility,
 * measure round-trip-delays and packet loss across network paths.
 *
 * Author -
 *      Mike Muuss
 *      U. S. Army Ballistic Research Laboratory
 *      December, 1983
 * Modified at Uc Berkeley
 * Record Route and verbose headers - Phil Dykstra, BRL, March 1988.
 * Multicast options (ttl, if, loop) - Steve Deering, Stanford, August 1988.
 * ttl, duplicate detection - Cliff Frost, UCB, April 1989
 * Pad pattern - Cliff Frost (from Tom Ferrin, UCSF), April 1989
 * Wait for dribbles, option decoding, pkt compare - vjs@sgi.com, May 1989
 *
 * Status -
 *      Public Domain.  Distribution Unlimited.
 *
 * Bugs -
 *      More statistics could always be gathered.
 *      This program has to run SUID to ROOT to access the ICMP socket.
 *
 * More modifications to make this work with my snmp tool.  gregm@randomc.com
 */
#include "config.h"
#include <stdio.h>
#define _USE_BSD
#include <sys/types.h>
#if TIME_WITH_SYS_TIME
#include <sys/time.h>
#include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#ifdef HAVE_SYS_PARAM_H
# include <sys/param.h>
#endif
#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
/* #include <netinet/ip_var.h> */
#ifdef HAVE_SYS_FILIO_H  /* needed for solaris */
#  include <sys/filio.h>
#endif

#ifndef HAVE_STRUCT_ICMP
/* This should only be the case on Linux Libc 5. */
typedef struct icmphdr		struct_icmp;

#define icmp_type type
#define icmp_code code
#define icmp_cksum checksum
#define icmp_id un.echo.id
#define icmp_seq un.echo.sequence
#define icmp_gwaddr un.gateway
#else
typedef struct icmp		struct_icmp;
#endif
#include "ping.h"
#include "snmp_lib.h"


#define MAXPACKET    (65536-60-8)    /* max packet size */
#define MAX_DUP_CHK  8 * 128
int  mx_dup_ck = MAX_DUP_CHK;
char rcvd_tbl [ MAX_DUP_CHK / 8 ];

#define A(bit)          rcvd_tbl[ (bit>>3) ]    /* identify byte in array */
#define B(bit)          ( 1 << (bit & 0x07) )   /* identify bit in byte */
#define SET(bit)        A(bit) |= B(bit)
#define CLR(bit)        A(bit) &= (~B(bit))
#define TST(bit)        (A(bit) & B(bit)) */

u_char  *packet;
int     i;

struct sockaddr whereto;        /* Who to ping */
#define DATALEN 64-8
int datalen = DATALEN;          /* How much data */
int packlen=DATALEN+100;

#ifdef NOTDEF
extern char *sys_errlist[];
#endif

/* static u_char outpack[MAXPACKET];  -- Not used? */

int ntransmitted = 0;           /* sequence # for outbound packets = #sent */
int ident;

struct sockaddr_in *to = (struct sockaddr_in *) &whereto;
/* static u_char *datap = &outpack[8+sizeof(struct timeval)]; -- Not used? */
gint   Pingsocket=0;
GSList *pingq =NULL;

void 
ping_input_cb ()
{
  time_t             nowtime;
  struct sockaddr_in from;
  char packet[packlen];
  GSList *tmpgs;
  GxPing *tmpping;
  int cc,hlen;
  struct ip *ip;
  struct_icmp *icp;

  nowtime = time (NULL);
  i     = sizeof(from);
  while ((cc = recvfrom (Pingsocket, (char *)packet, packlen, 0, (struct sockaddr*)&from, &i)) >= 0)
  {
      /* Lets check the ip header */
      ip   = (struct ip *)packet;
      hlen = ip->ip_hl << 2;
      icp  = (struct_icmp *)(packet + hlen);
      if (icp->icmp_type == ICMP_ECHOREPLY && icp->icmp_id == ident)
      {
          /* It's our packet */
          for(i=0; i < g_slist_length(pingq); i++)
          {
              tmpgs = g_slist_nth(pingq,i);
              tmpping = (GxPing*)tmpgs->data;
              if (tmpping->sequence != ntohs(icp->icmp_seq))
                  continue;
              if (tmpping->address != ip->ip_src.s_addr) 
              {
                  g_warning("Error with pings, sequence number %d.", ntohs(tmpping->sequence));
                  g_warning("We expected %s", inet_ntoa(*(struct in_addr*)&(tmpping->address)));
                  g_warning("We got %s.\n", inet_ntoa(*(struct in_addr*)&(ip->ip_src.s_addr)));
                 continue;
              }
              /* We found it */
              /*g_print("Receiving ping, %d seconds.\n", (int)(nowtime - tmpping->sent_time));*/
                if (tmpping->reply_cb)
                    tmpping->reply_cb(tmpping->address, tmpping->sent_time,
                            tmpping->user_data);
              pingq = g_slist_remove(pingq, tmpping);
              break;
          }
          if (tmpping->sequence != ntohs(icp->icmp_seq))
          {
              g_warning("Found ping with no sequence (%d)\n", ntohs(icp->icmp_seq));
          }
      }
  }
}

void
init_ping() 
{
  struct protoent *proto;
  
  if(! Pingsocket) 
    {
      if((proto = getprotobyname("icmp")) == (struct protoent *)NULL) 
	{
	  fprintf(stderr, "icmp: unknown protocol\n");
	  exit(10);
	}
      
      if((Pingsocket = socket(AF_INET, SOCK_RAW, proto->p_proto)) < 0) 
	{
          if (errno == EPERM)
          {
              g_warning("You must be running as root to use pings.\n");
              return;
          }
	  perror("ping: socket");
	  exit(5);
	}
      /* make it nonblocking */
      /*i_sock = 1;
      ioctl(Pingsocket, FIONBIO, &i);*/
      fcntl(Pingsocket, F_SETFL, O_NONBLOCK);
    }
  turtle_input_read_add(Pingsocket, ping_input_cb);

}

int 
sendping (gulong address, GxPingCB reply_cb, GxPingCB timeout_cb, gpointer user_data)
{
  struct in_addr       if_addr;
  register struct_icmp *icp;
  int                  cc;
  char                 outpack[sizeof(struct_icmp)];
  GxPing *tmpping;

  if (Pingsocket <= 0)
      return 0;

  memset ((char *)&whereto, '\0', sizeof(struct sockaddr) );
  to->sin_family      = AF_INET;
  to->sin_addr.s_addr = address;
  if_addr.s_addr      = address;

  if (!ident)
    ident = getpid () & 0xFFFF;
  
  icp = (struct_icmp *)outpack;

  icp->icmp_type  = ICMP_ECHO;
  icp->icmp_code  = 0;
  icp->icmp_cksum = 0;
  icp->icmp_seq   = htons((unsigned short)(ntransmitted++));
  icp->icmp_id    = ident;
  CLR (ntohs(icp->icmp_seq) % mx_dup_ck);
  cc = datalen + 8;                          /* skips ICMP portion */
  icp->icmp_cksum = in_cksum ((u_short *)icp, cc);
  /* Add ping to queue */
  tmpping = g_malloc(sizeof(GxPing));
  tmpping->address = address;
  tmpping->sequence = ntransmitted - 1;
  tmpping->reply_cb = reply_cb;
  tmpping->timeout_cb = timeout_cb;
  tmpping->sent_time = time(NULL);
  tmpping->user_data = user_data;
  pingq = g_slist_append(pingq, tmpping);
  if (sendto (Pingsocket, (char *)outpack, cc, 0, &whereto, 
	  sizeof (struct sockaddr)) <0) 
  {
#ifdef NOTDEF
      g_warning("Problem with sending ping (%s).\n", sys_errlist[errno]);
#else NOTDEF
      g_warning("Problem with sending ping (%s).\n", strerror(errno));
#endif NOTDEF
      return 0;
  }

  return 1;
}

struct sockaddr_in *
readping ()
{
  struct ip              *ip;
  register struct_icmp   *icp;
  int                    hlen;
  int                    i, cc;
  struct sockaddr_in     from;
  struct sockaddr_in     *ret;

  if (!packet &&(packet = (u_char *)g_malloc(packlen)) == NULL )
    {
      g_error ("ping: g_malloc failed for %d bytes\n", packlen);
      exit (1);
    }
  errno = 0;
  i     = sizeof(from);
  if ((cc = recvfrom (Pingsocket, (char *)packet, packlen, 0, &from, &i)) < 0)
    return NULL;
  /*
    Lets check the ip header
  */
  ip   = (struct ip *)packet;
  hlen = ip->ip_hl << 2;
  icp  = (struct_icmp *)(packet + hlen);
  if (icp->icmp_type == ICMP_ECHOREPLY && icp->icmp_id == ident)
    {
      ret = (struct sockaddr_in *)g_malloc (sizeof (struct sockaddr_in));
      memcpy ((char *)ret, &from, sizeof (struct sockaddr_in));
      return ret;
    }
  else
    return NULL;
}

/*
 * IN_CKSUM
 *
 * Checksum routine for Internet Protocol family headers (C Version)
 *
 */
u_short
in_cksum(u_short *addr, int len)
{
  register int nleft  = len;
  register u_short *w = addr;
  register int sum    = 0;
  u_short answer      = 0;
  
  /*
   *  Our algorithm is simple, using a 32 bit accumulator (sum),
   *  we add sequential 16 bit words to it, and at the end, fold
   *  back all the carry bits from the top 16 bits into the lower
   *  16 bits.
   */
  while( nleft > 1 )  
    {
      sum += *w++;
      nleft -= 2;
    }
  
  /* mop up an odd byte, if necessary */
  if( nleft == 1 ) 
    {
      *(u_char *)(&answer) = *(u_char *)w ;
      sum += answer;
    }
  
  /*
   * add back carry outs from top 16 bits to low 16 bits
   */
  sum    = (sum >> 16) + (sum & 0xffff);     /* add hi 16 to low 16 */
  sum   += (sum >> 16);                      /* add carry */
  answer = ~sum;                             /* truncate to 16 bits */
  return (answer);
}






