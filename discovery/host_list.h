/*
 * $Id$
 * GXSNMP -- An snmp management application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Definitions for the host list panel
 */
#ifndef __HOST_LIST_H__
#define __HOST_LIST_H__

#include "snmp_host.h"

typedef struct __hlist_panel {
  GtkWidget	*window;	/* The window for this panel */
  GtkWidget     *list;          /* the list widget */
} hlist_panel;

typedef struct __oidentry {
  GtkWidget     *name;
  gchar         *oid_name;           /* Which oid in _text_ we are fetching */
  gulong        id [SNMP_SIZE_OBJECTID];
  int           id_len;
} oidentry;

typedef struct __sysinfo_panel {
  GtkWidget     *window;
  GtkWidget     *sys_name;
  GtkWidget     *sys_desc;
  GtkWidget     *sys_contact;
  GtkWidget     *sys_location;
  GtkWidget     *sys_uptime;
  GtkWidget     *sys_services;
  GtkWidget     *sql_widgets[7];
  gpointer      irequest;
} sinfo_panel;

#define ROUTE_CLOS_BUTTON   0
#define ROUTE_REFR_BUTTON   1
#define ROUTE_ABRT_BUTTON   2

#define LOAD_IN_DIAL        0
#define LOAD_OUT_DIAL       1
#define LOAD_AVG_DIAL       2


typedef struct __route_panel {
  GtkWidget     *window;
  GtkWidget     *buttons[3];
  GtkWidget     *rtt_clist;
  GtkWidget     *flt_entry;
  gpointer      rrequest;
} route_panel;

#define INTF_NA_IDX        0
#define INTF_IP_IDX        1
#define INTF_NM_IDX        2
#define INTF_CN_IDX        3

#define SQL_HN_IDX         0
#define SQL_DB_IDX         1
#define SQL_TB_IDX         2
#define SQL_UD_IDX         3
#define SQL_CT_IDX         4
#define SQL_LO_IDX         5

typedef struct __interface_panel {
  GtkWidget     *window;
  GtkWidget     *list;
  GtkWidget     *widget[5];
  GtkWidget     *buttons[3];
  gpointer      request;
} interface_panel;

#define INTER_CLOS_BUT   0
#define INTER_REFR_BUT   1
#define INTER_ABRT_BUT   2

typedef struct __int_detail__ {
  GtkWidget    *window;
  GtkWidget    *buttons[3];
  GtkWidget    *entry[15];
  gpointer     request;
  gint         update_id;
} detail_panel;

#define DETAIL_CLOS_BUT  0
#define DETAIL_REFR_BUT  1
#define DETAIL_ABRT_BUT  2

#define D_NODE_NAME      0
#define D_NODE_ADDR      1
#define D_IFNAME         2
#define D_IFADDR         3
#define D_IFSPEED        4
#define D_IFMTU          5
#define D_IFIDX          6
#define D_IFTYPE         7
#define D_IFADMIN        8
#define D_IFOPER         9
#define D_IFINOCT        10
#define D_IFOUTOCT       11
#define D_IFINERR        12
#define D_IFOUTERR       13

#endif 

/* EOF */
