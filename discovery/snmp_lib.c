#include <netinet/in.h>
#include <arpa/inet.h>
#include <gtk/gtk.h>

gboolean    
turtle_io_invoke(GIOChannel *source, GIOCondition condition, gpointer data)
{
    void (*func)(void);

    func = data;
    func();
    return TRUE;
}

void
turtle_input_read_add(int skt, void(*receiveMessage)())
{
    guint result;
    GIOChannel *channel;
    GIOCondition cond;
        
    cond = (G_IO_IN | G_IO_PRI); /* GDK_INPUT_READ */
    channel = g_io_channel_unix_new(skt);
    result = g_io_add_watch_full(channel, G_PRIORITY_DEFAULT, cond,
            turtle_io_invoke, receiveMessage, NULL);
    g_io_channel_unref(channel);
}
