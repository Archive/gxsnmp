/*
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Header file for the ping module.
 */
#ifndef __PING_IO_H_
#define __PING_IO_H_

#include <netinet/in.h>
#include <time.h>
#include <glib.h>
/*
 * Prototypes
 */
typedef void(*GxPingCB) (gulong address, time_t sent_time, gpointer user_data);

void ping_input_cb            (void);
void init_ping                  (void);
int sendping                   (gulong            address,
                                GxPingCB        reply_cb,
                                GxPingCB        timeout_cb,
                                gpointer        user_data);
struct sockaddr_in *readping   (void);
u_short in_cksum               (u_short           *addr,
				int               len);


typedef struct {
    gulong address; /* IP Address we have pinged */
    int sequence; /* ID for ping */
    time_t sent_time; /* Time we sent the ping */
    GxPingCB reply_cb;
    GxPingCB timeout_cb;
    gpointer user_data;
} GxPing;

#endif

