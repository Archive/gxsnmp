/*
 * GXSNMP -- An snmp mangament application
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>   
#include <getopt.h>
#include <strings.h>
#include <signal.h>
#include <errno.h>
#include <malloc.h>
#include <sys/time.h>
#include <sys/socket.h>
#include "g_snmp.h"
#include "mib.h"

int domain;
int retry, timeout, version;
char *name, *variable, *community;
char result[1024];

static void
snmp_get()
{
  host_snmp *host;
  GSList *var = NULL;
  GSList *objs;

  host = malloc(sizeof(host_snmp));

  if (community)
    {
      host->rcomm      = community;
      host->wcomm      = community;
    }
  else
    {
      host->rcomm      = "public";
      host->wcomm      = "public";
    }
  host->retries    = retry;
  host->timeout    = timeout;
  host->version    = version;
  host->name       = name;
  host->domain     = domain;
  host->port       = 0;
  host->status     = 0;
  host->magic      = NULL;

  g_pdu_add_name(&var, variable, SNMP_NULL, NULL);
  objs = g_sync_get(host, var);
  if (! objs)
    {
      printf("Error while trying to send SNMP packet\n");
      exit(1);
    }
  while (objs) 
    {
      g_snmp_printf(result, sizeof(result), objs->data);
      printf("%s\n",result);
      objs = objs->next;
    }
}

static int
usage()
{
  printf("\n"
         "Usage: test [-t timeout] [-r retry] [-v version] [-c community] "
         "host variable\n"
         "\n"
         "-t timeout:   time to wait for a SNMP response\n"
         "-r retry:     number of retries until a node is considered unreachable.\n"
	 "-d domain:    transport domain to be used.\n"
         "-c community: community name.\n"
         "-h            print this message.\n\n");
  exit(1);
}

int main(int argc, char *argv[])
{
  int c;
  int oi = 0;

  static const struct option long_options[] = 
    {
      {"retry",    required_argument, 0, 'r'},
      {"timeout",  required_argument, 0, 't'},
      {"version",  required_argument, 0, 'v'},
      {"engineid", required_argument, 0, 'e'},
      {"secname",  required_argument, 0, 'c'},
      {"secmodel", required_argument, 0, 's'},
      {"seclevel", required_argument, 0, 'l'},
      {"domain",   required_argument, 0, 'd'},
      {"debug",    no_argument,       0, 'x'},
      {"help",     no_argument,       0, 'h'},
      {0, 0, 0, 0}
    };

  community = NULL;
  version = SNMP_V1;
  domain  = AF_INET;
  while (1) 
    {
      c = getopt_long(argc, argv, "v:c:o:p:m:t:r:d:xh?", long_options, &oi);

      if (c == -1)
        break;

      switch(c) 
        {
          case 'r':
            retry = atoi(optarg);
            break;
          case 't':
            timeout = atoi(optarg);
            break;
          case 'v':
            version = atoi(optarg);
            break;
          case 'c':
            community = optarg;
            break;
          case 'd':
            domain = atoi(optarg);
            break;
          case 'h':
            usage();
            break;
        }
    }
  if (argc - optind < 2) 
    usage();

  name     = argv[optind];
  variable = argv[optind+1];

  init_mib();
  g_snmp_init(FALSE);
  printf("sending SNMPGET to %s for %s\n", name, variable);
  snmp_get();
  return 0;
}
