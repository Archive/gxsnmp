/*
 * Datagram Messages
 */

typedef struct _GIODgramMessage GIODgramMessage;
typedef struct _GIODgramWatch GIODgramWatch;
typedef struct _GIODgramAddress GIODgramAddress;

struct _GIODgramMessage {
  GIOMessage message;
  gint fd;
};

struct _GIODgramWatch {
  GPollFD       pollfd;
  GIOMessage   *message;
  GIOCondition  condition;
  GIOMFunc      callback;
};

struct _GIODgramAddress {
  socklen_t	  len;
  struct sockaddr addr;
};

GIOMessage * g_io_message_dgram_new (gint fd);

