#include <glib.h>
#include "giomessage.h"

void
g_io_message_init (GIOMessage *message)
{
  message->message_flags = 0;
  message->ref_count = 1;
}

void 
g_io_message_ref (GIOMessage *message)
{
  g_return_if_fail (message != NULL);

  message->ref_count++;
}

void 
g_io_message_unref (GIOMessage *message)
{
  g_return_if_fail (message != NULL);

  message->ref_count--;
  if (message->ref_count == 0)
    message->funcs->io_free (message);
}

GIOError 
g_io_message_recv (GIOMessage *message, 
		   gchar      *buf, 
		   guint       count,
		   guint      *bytes_read,
		   GIOAddress  sender)
{
  g_return_val_if_fail (message != NULL, G_IO_ERROR_UNKNOWN);

  return message->funcs->io_recv (message, buf, count, bytes_read, sender);
}

GIOError 
g_io_message_send (GIOMessage *message, 
		   gchar      *buf, 
		   guint       count,
		   guint      *bytes_written,
		   GIOAddress  recipient)
{
  g_return_val_if_fail (message != NULL, G_IO_ERROR_UNKNOWN);

  return message->funcs->io_send (message, buf, count, bytes_written, recipient);
}

void
g_io_message_close (GIOMessage *message)
{
  g_return_if_fail (message != NULL);

  message->funcs->io_close (message);
}

guint 
g_io_madd_watch_full (GIOMessage    *message,
		      gint           priority,
		      GIOCondition   condition,
		      GIOMFunc       func,
		      gpointer       user_data,
		      GDestroyNotify notify)
{
  g_return_val_if_fail (message != NULL, 0);

  return message->funcs->io_add_watch (message, priority, condition,
				       func, user_data, notify);
}

guint 
g_io_madd_watch (GIOMessage    *message,
		 GIOCondition   condition,
		 GIOMFunc       func,
		 gpointer       user_data)
{
  return g_io_madd_watch_full (message, 0, condition, func, user_data, NULL);
}
