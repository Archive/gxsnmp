/*
**  $Id$
**
**  g_sqldb -- An SQL backed, in-storage database
**  Copyright (C) 1999 John Schulien
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place - Suite 330 , Cambridge, MA 02139, USA.
**
**  Subroutines for database access
*/

#ifndef __G_SQLDB_H__
#define __G_SQLDB_H__

#include <glib.h>
#include <g_sql.h>

typedef struct _G_sqldb_column
{
  gchar			         * name;	  /* Name of the column */
  G_sql_column_type	           type;	  /* Type of the column */
  gint			           column_offset; /* Offset of column in row */
  struct _G_sqldb_column_private * private;	  /* For use by library only */
}
G_sqldb_column;

typedef struct _G_sqldb_table
{
  G_sql			        * engine;	/* SQL engine structure */
  gchar			       ** database;	/* Name of the SQL database */
  gchar			        * table;	/* Name of the SQL table */
  gint			          row_length;   /* Size of each row in bytes */
  gint				  row_private;  /* Offset in row to gpointer
						   for use by library only */
  G_sqldb_column                * columns;      /* Pointer to column array */
  struct _G_sqldb_table_private * private;	/* For use by library only */
}
G_sqldb_table;
 
/******************************************************************************
**
**  Subroutines to access databases
**
******************************************************************************/

gboolean      g_sqldb_table_load	(G_sqldb_table * table);

void          g_sqldb_table_close  	(G_sqldb_table * table);

GList       * g_sqldb_table_list        (G_sqldb_table * table);

gboolean      g_sqldb_row_add    	(G_sqldb_table * table, 
				 	 gpointer        row);

gboolean      g_sqldb_row_update	(G_sqldb_table * table,
					 gpointer        row);

gboolean      g_sqldb_row_delete	(G_sqldb_table * table,
					 gpointer        row);

gboolean      g_sqldb_row_unique	(G_sqldb_table * table,
					 gpointer        row);

gboolean      g_sqldb_reload_row        (G_sqldb_table * table,
					 gpointer        row);

gpointer      g_sqldb_row_find		(G_sqldb_table * table,
					 gchar	       * field,
					 gpointer	 data);

gint          g_sqldb_highest_rowid     (G_sqldb_table * table, 
					 gchar	       * field);

/*
**  Subroutines to register callbacks to be invoked when the database is
**  modified.  Any or all of these may be specified at once.
*/

typedef enum 
{
  G_SQLDB_CB_ADD    = 0x01, /* Issue callback when record is added to DB */
  G_SQLDB_CB_UPDATE = 0x02, /* Issue callback when record is updated in DB */
  G_SQLDB_CB_DELETE = 0x04, /* Issue callback when record is deleted from DB */
  G_SQLDB_CB_BEFORE = 0x08, /* Issue callback before operation */
  G_SQLDB_CB_AFTER  = 0x10  /* Issue callback after operation */
} 
G_sqldb_cb_type;  

/*
**  The callback function receives the table, the row, a flag byte
**  that is either {add, update, delete} ored with either {before, after},
**  and a gpointer of user-supplied data.
*/

typedef void  (*G_sqldb_cb)           (G_sqldb_table * table, 
                                       gpointer        row,   
				       G_sqldb_cb_type type,
				       gpointer	       data);

/*
**  Here are the functions to add callbacks to the tables
*/

gint	      g_sqldb_table_cb_add    (G_sqldb_table   * table, 
				       gpointer		 row,
				       G_sqldb_cb_type   type,
				       G_sqldb_cb        func,
				       gpointer		 data);

gint	      g_sqldb_table_cb_remove (G_sqldb_table   * table,
				       gpointer	   row,
				       G_sqldb_cb        func);

#endif /* __G_SQLDB_H__ */

/* EOF */
