/*
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 1998 Gregory McLean & Jochen Friedrich
 * Beholder RMON ethernet network monitor,Copyright (C) 1993 DNPAP group
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <glib.h>

#ifndef _GXSNMP_SMUX_H_
#define _GXSNMP_SMUX_H_

#define SNMP_NULL                0
#define SNMP_INTEGER             1
#define SNMP_OCTETSTR            2
#define SNMP_DISPLAYSTR          2
#define SNMP_OBJECTID            3
#define SNMP_IPADDR              4
#define SNMP_COUNTER             5
#define SNMP_UINTEGER            5
#define SNMP_GAUGE               6
#define SNMP_TIMETICKS           7
#define SNMP_OPAQUE              8
#define SNMP_BITSTR              9
#define SNMP_NSAP               10
#define SNMP_COUNTER64          11
#define SNMP_NOSUCHOBJECT       12
#define SNMP_NOSUCHINSTANCE     13
#define SNMP_ENDOFMIBVIEW       14

#define RONLY 0

/* SMUX Pdu */
typedef enum
{
  GXSNMP_SMUX_OPEN  = 0, /* Open               */
  GXSNMP_SMUX_CLOSE = 1, /* Close              */
  GXSNMP_SMUX_RREQ  = 2, /* Register Request   */
  GXSNMP_SMUX_RRSP  = 3, /* Register Response  */
  GXSNMP_SMUX_SOUT  = 4  /* Commit Or Rollback */
} GxsnmpSmuxPdu;

/* SNMP Pdu */
typedef enum
{
  GXSNMP_SNMP_GET     =  0, /* GET Request     */
  GXSNMP_SNMP_GETNEXT =  1, /* GETNEXT Request */
  GXSNMP_SNMP_GETRSP  =  2, /* Response        */
  GXSNMP_SNMP_SET     =  3, /* SET Request     */
  GXSNMP_SNMP_TRAP    =  4  /* TRAP Request    */
} GxsnmpSnmpPdu;

/* SNMP Error */
typedef enum
{
  GXSNMP_SMUX_NOERROR =  0, /* No Error        */
  GXSNMP_SMUX_TIMEOUT =  1  /* SMUX Timeout    */
} GxsnmpSmuxError;

struct gxsnmp_variable;

typedef struct _GxsnmpSmux GxsnmpSmux;

typedef gint (GxsnmpPutVar)(gint action, guchar *var_val, guchar var_val_type,
  guint var_val_len, guchar *statP, gulong *name, guint length);

typedef guchar *(GxsnmpGetVar)(struct gxsnmp_variable *vp, gulong *name,
  guint *length, gboolean exact, guint *var_len, GxsnmpPutVar **write_method);

struct gxsnmp_variable
{
    guchar        magic;
    gchar         type;
    gushort       acl;
    GxsnmpGetVar *find;
    guchar        namelen;
    gulong        name[255];
};

struct gxsnmp_variable2
{
    guchar        magic;
    gchar         type;
    gushort       acl;
    GxsnmpGetVar *find;
    guchar        namelen;
    gulong        name[2];
};

struct gxsnmp_variable4
{
    guchar        magic;
    gchar         type;
    gushort       acl;
    GxsnmpGetVar *find;
    guchar        namelen;
    gulong        name[4];
};

struct gxsnmp_variable8
{
    guchar        magic;
    gchar         type;
    gushort       acl;
    GxsnmpGetVar *find;
    guchar        namelen;
    gulong        name[8];
};

struct gxsnmp_subtree
{
    gulong                  name[256];
    guchar                  name_len;
    struct gxsnmp_variable *variables;
    guint                   variables_num;
    guint                   variables_width;
    gboolean                registered;
};
    
struct _GxsnmpSmux
{                                   /* SMUX object                         */
    GSList         *registration;   /* OID registrations                   */
    GIOChannel     *channel;        /* Channel for TCP/IP communication    */
    GxsnmpSmuxError error;          /* Error condition                     */
};

GxsnmpSmux* gxsnmp_smux_new (void);
void gxsnmp_smux_destroy (GxsnmpSmux *smux);
gboolean gxsnmp_smux_connect (GxsnmpSmux *smux, guchar *host, gulong *oid,
		guint len, guchar *password);
guint gxsnmp_smux_register (GxsnmpSmux *smux, struct gxsnmp_variable *var,
		guint var_width, guint var_num, gulong *oid, guint len);
gboolean gxsnmp_smux_unregister (GxsnmpSmux *smux, guint handle);
gboolean gxsnmp_smux_trap (GxsnmpSmux *smux, gulong *oid, guint len,
		GSList *variables);
gboolean gxsnmp_header_generic (struct gxsnmp_variable *v, gulong *name,
		gint *length, gboolean exact, guint *var_len,
		GxsnmpPutVar **write_method);

#endif
