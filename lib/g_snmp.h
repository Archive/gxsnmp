/*
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 1998 Gregory McLean & Jochen Friedrich
 * Beholder RMON ethernet network monitor,Copyright (C) 1993 DNPAP group
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#ifndef _G_SNMP_H_
#define _G_SNMP_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <glib.h>

/* Our Enterprise Handle */

#define ENTERPRISE_GNOME 3319

/* SNMP Versions */

#define SNMP_V1    0
#define SNMP_V2C   1
#define SNMP_V2    2
#define SNMP_V3    3

/* Default Sizes */

#define SNMP_SIZE_COMM              256
#define SNMP_SIZE_OBJECTID          128
#define SNMP_SIZE_BUFCHR            256
#define SNMP_SIZE_BUFINT            128

#define SNMP_SIZE_SMALLOBJECTID     16

/* Requests */

#define SNMP_PDU_GET                0
#define SNMP_PDU_NEXT               1
#define SNMP_PDU_RESPONSE           2
#define SNMP_PDU_SET                3
#define SNMP_PDU_TRAP1              4

#define SNMP_PDU_BULK               5
#define SNMP_PDU_INFORM             6
#define SNMP_PDU_TRAP2              7

/* Errors */

#define SNMP_NOERROR                0
#define SNMP_TOOBIG                 1
#define SNMP_NOSUCHNAME             2
#define SNMP_BADVALUE               3
#define SNMP_READONLY               4
#define SNMP_GENERROR               5

#define SNMP_NOACCESS               6
#define SNMP_WRONGTYPE              7
#define SNMP_WRONGLENGTH            8
#define SNMP_WRONGENCODING          9
#define SNMP_WRONGVALUE            10
#define SNMP_NOCREATION            11
#define SNMP_INCONSISTENTVALUE     12
#define SNMP_RESOURCEUNAVAILABLE   13
#define SNMP_COMMITFAILED          14
#define SNMP_UNDOFAILED            15
#define SNMP_AUTHORIZATIONERROR    16
#define SNMP_NOTWRITABLE           17
#define SNMP_INCONSISTENTNAME      18

/* General SNMP V1 Traps */

#define SNMP_TRAP_COLDSTART          0
#define SNMP_TRAP_WARMSTART          1
#define SNMP_TRAP_LINKDOWN           2
#define SNMP_TRAP_LINKUP             3
#define SNMP_TRAP_AUTFAILURE         4
#define SNMP_TRAP_EQPNEIGHBORLOSS    5
#define SNMP_TRAP_ENTSPECIFIC        6


/* SNMPv1 Types */
         
#define SNMP_NULL                0
#define SNMP_INTEGER             1    /* l  */
#define SNMP_OCTETSTR            2    /* c  */
#define SNMP_DISPLAYSTR          2    /* c  */
#define SNMP_OBJECTID            3    /* ul */
#define SNMP_IPADDR              4    /* uc */
#define SNMP_COUNTER             5    /* ul */
#define SNMP_GAUGE               6    /* ul */
#define SNMP_TIMETICKS           7    /* ul */
#define SNMP_OPAQUE              8    /* c  */

/* additional SNMPv2 Types */

#define SNMP_UINTEGER            5    /* ul */
#define SNMP_BITSTR              9    /* uc */
#define SNMP_NSAP               10    /* uc */
#define SNMP_COUNTER64          11    /* ul */
#define SNMP_NOSUCHOBJECT       12
#define SNMP_NOSUCHINSTANCE     13
#define SNMP_ENDOFMIBVIEW       14

/* Typedefs */

typedef struct  _SNMP_STAT       SNMP_STAT;
typedef struct  _SNMP_REQUEST    SNMP_REQUEST;
typedef struct  _SNMP_V1_TRAP    SNMP_V1_TRAP;
typedef union   _SNMP_PDU        SNMP_PDU;
typedef union   _SNMP_SYNTAX     SNMP_SYNTAX;
typedef struct  _SNMP_OBJECT     SNMP_OBJECT;

union _SNMP_SYNTAX
{
    guchar   uc[0]; /*  8 bit unsigned */
    gchar    c[0];  /*  8 bit signed   */
    gulong   ul[0]; /* 32 bit unsigned */
    glong    l[0];  /* 32 bit signed   */
};

struct _SNMP_OBJECT
{
    gulong            *id;
    guint	       id_len;
    gushort            type;
    guint              syntax_len;
    SNMP_SYNTAX        syntax;
};

struct _SNMP_REQUEST
{
    guint        type;
    gulong       id;
    guint        error_status;
    guint        error_index;
    GSList      *variables;
};

struct _SNMP_V1_TRAP
{
    guint    type;
    gulong  *id;
    guint    id_len;
    gulong   ip_address;
    guint    general;
    guint    specific;
    gulong   time;
    GSList  *variables;
};

union _SNMP_PDU
{
    guint           type;
    SNMP_V1_TRAP    trap;
    SNMP_REQUEST    request;
};

/* Include other SNMP headers */

#include "g_asn1.h"
#include "g_access.h"
#include "g_date.h"
#include "g_message.h"
#include "g_security.h"
#include "g_session.h"
#include "g_transport.h"
#include "g_dispatch.h"
#include "g_snmp_table.h"

/* SNMP V1 trap names */

extern const char *SnmpTrap[];

/* SNMP encoding and decoding */
 
gboolean g_snmp_pdu_v1_encode ( ASN1_SCK *asn1, SNMP_PDU *pdu);
gboolean g_snmp_pdu_v1_decode ( ASN1_SCK *asn1, SNMP_PDU *pdu);
gboolean g_snmp_pdu_v2_encode ( ASN1_SCK *asn1, SNMP_PDU *pdu);
gboolean g_snmp_pdu_v2_decode ( ASN1_SCK *asn1, SNMP_PDU *pdu);
gboolean g_snmp_pdu_v3_encode ( ASN1_SCK *asn1, SNMP_PDU *pdu, 
         char *cenid, int cenidlen, char *cname, int cnamelen);
gboolean g_snmp_pdu_v3_decode ( ASN1_SCK *asn1, SNMP_PDU *pdu,
         char **cenid, int *cenidlen, char **cname, int *cnamelen);

#endif
/* EOF */
