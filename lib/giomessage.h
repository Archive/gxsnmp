/* GIOMessage
 */

typedef struct _GIOMFuncs  GIOMFuncs;
typedef struct _GIOMessage GIOMessage;

typedef gpointer GIOAddress;

struct _GIOMessage
{
  guint message_flags;
  guint ref_count;
  GIOMFuncs *funcs;
};

typedef gboolean (*GIOMFunc) (GIOMessage   *source,
                              GIOCondition  condition,
                              gpointer      data);

struct _GIOMFuncs
{
  GIOError (*io_recv)   (GIOMessage 	*message, 
		         gchar      	*buf, 
		         guint      	 count,
			 guint      	*bytes_read,
			 GIOAddress	 sender);
  GIOError (*io_send)   (GIOMessage 	*message, 
		 	 gchar      	*buf, 
			 guint      	 count,
			 guint      	*bytes_written,
			 GIOAddress	 recipient);
  void (*io_close)      (GIOMessage	*message);
  guint (*io_add_watch) (GIOMessage     *message,
			 gint            priority,
			 GIOCondition    condition,
			 GIOMFunc        func,
			 gpointer        user_data,
			 GDestroyNotify  notify);
  void (*io_free)       (GIOMessage	*message);
};

void        g_io_message_init   (GIOMessage    *message);
void        g_io_message_ref    (GIOMessage    *message);
void        g_io_message_unref  (GIOMessage    *message);
GIOError    g_io_message_recv   (GIOMessage    *message, 
			         gchar         *buf, 
			         guint          count,
			         guint         *bytes_read,
				 GIOAddress     sender);
GIOError  g_io_message_send     (GIOMessage    *message, 
			         gchar         *buf, 
			         guint          count,
			         guint         *bytes_written,
				 GIOAddress     recipient);
void      g_io_message_close    (GIOMessage    *message);
guint     g_io_madd_watch_full  (GIOMessage    *message,
			         gint           priority,
			         GIOCondition   condition,
			         GIOMFunc       func,
			         gpointer       user_data,
			         GDestroyNotify notify);
guint    g_io_madd_watch        (GIOMessage    *message,
			         GIOCondition   condition,
			         GIOMFunc       func,
			         gpointer       user_data);


