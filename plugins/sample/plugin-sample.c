/*
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  plugin-sample.c -- A sample plugin, it don't do anything but its useful
**                     for testing.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "plugins.h"
#include "main.h"

extern gxsnmp * app_info;

/******************************************************************************
**
**  Static data
**
******************************************************************************/

static gchar * menu_location;
static void    menu_callback (GtkWidget * widget, gpointer data);

/******************************************************************************
**
**  The plugin load routine
**
**  This function is invoked at the very start of GXSNMP.  It should
**  set one or more of the following flags:
**
**  PLUGIN_DATABASE	-- for plugins that provide a database service
**  PLUGIN_COLLECTOR	-- for plugins that do not require GTK services
**  PLUGIN_APPLICATION	-- for plugins that require GTK services
**
**  The flags determine at what point the start function is called during
**  initialization.  The database plugins need to be started before the
**  database is read in (for obvious reasons), and the collector and 
**  application plugins are not started until the database is completely
**  read in and set up.
**
******************************************************************************/

gint
load_plugin (PluginData * pd)
{
  pd->type = PLUGIN_APPLICATION;
  return 0;
}

/******************************************************************************
**
**  The plugin unload routine
**
**  This function is invoked during the shutdown callback in main.c
**  It should free any storage the plugin might have allocated, and
**  generally clean itself up.
**
******************************************************************************/

void
unload_plugin (PluginData * pd)
{
}

/******************************************************************************
**
**  The plugin start routine
**
**  This function is invoked during initialization.  It should perform      
**  initialization and start the plugin running.
**
**  Included here is sample code to install the plugin in the 'plugins' menu.
**  A collector-only plugin would NOT install a menu entry, and would not
**  have a menu callback routine (below).
**
******************************************************************************/

gint 
start_plugin (PluginData * pd)
{
  GnomeUIInfo   * menu;

  g_print ("Starting Sample plugin.\n");

  menu_location         = g_strdup_printf ("%s/", gettext("_Plugins"));

  menu                  = g_malloc0 (2 * sizeof(GnomeUIInfo));
  menu->type            = GNOME_APP_UI_ITEM;
  menu->label           = g_strdup ("Sample Plugin");
  menu->hint            = NULL;
  menu->moreinfo        = menu_callback;       /* Menu activation cb routine */
  menu->user_data       = "Sample data";       /* User data for the callback */
  menu->unused_data     = NULL;                /* Reserved for future use */
  menu->pixmap_type     = 0;
  menu->pixmap_info     = NULL;
  menu->accelerator_key = 0;
  (menu + 1)->type      = GNOME_APP_UI_ENDOFINFO;

  gnome_app_insert_menus (GNOME_APP (app_info->window), menu_location, menu);

  return 0;
}

/******************************************************************************
**
**  Menu callback routine for the plugin
**
**  This function is invoked when the user selects the plugin from the
**  plugins menu.
**
******************************************************************************/

static void
menu_callback (GtkWidget * widget, gpointer data)
{
  g_print ("Sample plugin menu callback invoked with data='%s'\n", 
	   (gchar *)data);
}

