/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*/

/*
**  traffic_dialog.c is the dialog for the interface traffic function
*/

#include <gnome.h>
#include "main.h"			/* Needed for gxsnmp struct */
#include "traffic_dialog.h"
#include "tables.h"
#include "gnome-property-dialog.h"
#include "gnome-dialog-create-button.h"

extern gxsnmp *app_info;

/******************************************************************************
**
**  Static data 
**
******************************************************************************/

#define CLIST_COLUMNS 5

static char * titles [] =
{
  "Hostname", "Interface", "Status", "Last checked", "Next check"
};

static gint title_widths[5] = { 20, 16, 8, 18, 18 };
static gint total_height = 16;
static gint total_width	= 84;	/* Empirically determined :-| */

/*******************************************************************************
**
**  Forward declarations and callback functions
**
*******************************************************************************/

static void traffic_dialog_class_init (GXsnmp_traffic_dialogClass * klass);

static void traffic_dialog_init		(GXsnmp_traffic_dialog      * dialog);

/*******************************************************************************
**
**  gxsnmp_traffic_dialog_get_type()
**
*******************************************************************************/

guint
gxsnmp_traffic_dialog_get_type ()
{
  static guint widget_type = 0;

  if (!widget_type)
    {
      GtkTypeInfo widget_info =
      {
        "GXsnmp_traffic_dialog",
        sizeof (GXsnmp_traffic_dialog),
        sizeof (GXsnmp_traffic_dialogClass),
        (GtkClassInitFunc) traffic_dialog_class_init,
        (GtkObjectInitFunc) traffic_dialog_init,
        (GtkArgSetFunc) NULL,
        (GtkArgGetFunc) NULL
      };
      widget_type = gtk_type_unique (gnome_property_dialog_get_type (), 
				     &widget_info);
    }
  return widget_type;
}

/*******************************************************************************
**
**  The class initialization subroutine
**
*******************************************************************************/

static void
traffic_dialog_class_init (GXsnmp_traffic_dialogClass *class)
{
}

/*****************************************************************************
**
**  The widget initialization subroutine
**
*****************************************************************************/

static void
traffic_dialog_init (GXsnmp_traffic_dialog *dialog)
{
  GtkWidget * label;
  GtkWidget * clist;
  GtkWidget * scrolled_win;	
  gint        c_width;		/* Average width of a character */
  gint	      c_height;		/* Average height of a character */
  gint	      i;

/*
**  Build the main framework for the whole thing
*/

  dialog->table = gtk_table_new (1, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      dialog->table, TRUE, TRUE, 0);
  gtk_widget_show (dialog->table);
  c_width  = gdk_string_width (dialog->table->style->font, "xW") / 2;
  c_height = dialog->table->style->font->ascent + 
             dialog->table->style->font->descent;

/*
**  The title label
*/

  label = gtk_label_new (_("Interface Keepalive Status Monitor"));
  gtk_table_attach (GTK_TABLE (dialog->table), label,
		    0, 1, 0, 1, 0, 0, 0, 0);
  gtk_widget_show (label);

/*
**  Create a scrolled window, and insert the clist in the window
*/

  scrolled_win = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_set_border_width (GTK_CONTAINER (scrolled_win), 5);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
                                  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_table_attach (GTK_TABLE (dialog->table), scrolled_win, 
		    0, 1, 1, 2, 0, 0, 0, 0);
  gtk_widget_show (scrolled_win);

  clist = gtk_clist_new_with_titles (CLIST_COLUMNS, titles);

  for (i=0; i < CLIST_COLUMNS; i++)
    gtk_clist_set_column_width (GTK_CLIST (clist), i, 
				c_width * title_widths[i]);

  gtk_container_add (GTK_CONTAINER (scrolled_win), clist);
  gtk_widget_set_usize (clist, total_width  * c_width , 
			       total_height * c_height);
  gtk_widget_show (clist);

}

/****************************************************************************
**
**  Public function to create a new traffic dialog widget    
**
****************************************************************************/

GtkWidget *
gxsnmp_traffic_dialog_new (void)
{
  GXsnmp_traffic_dialog * traffic_dialog;

  traffic_dialog = gtk_type_new( gxsnmp_traffic_dialog_get_type());

  gnome_property_dialog_set_state (GNOME_PROPERTY_DIALOG (traffic_dialog), 
				   FALSE);

  return GTK_WIDGET (traffic_dialog);
}

/* EOF */
