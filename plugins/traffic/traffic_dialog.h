/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  The traffic_dialog widget is the control panel for the traffic plugin
*/

#ifndef __GXSNMP_TRAFFIC_DIALOG_H__
#define __GXSNMP_TRAFFIC_DIALOG_H__

#include <gnome.h>
#include "gnome-property-dialog.h"
#include "tables.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_TRAFFIC_DIALOG (gxsnmp_traffic_dialog_get_type())

#define GXSNMP_TRAFFIC_DIALOG(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_TRAFFIC_DIALOG, \
						GXsnmp_traffic_dialog)

#define GXSNMP_TRAFFIC_DIALOG_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_TRAFFIC_DIALOG, \
			      GXsnmp_traffic_dialogClass)

#define GXSNMP_IS_TRAFFIC_DIALOG(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_TRAFFIC_DIALOG)

#define GXSNMP_IS_TRAFFIC_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_TRAFFIC_DIALOG)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_traffic_dialog	     GXsnmp_traffic_dialog;
typedef struct _GXsnmp_traffic_dialogClass   GXsnmp_traffic_dialogClass;

struct _GXsnmp_traffic_dialog
{
  GnomePropertyDialog   dialog;			/* Base structure */
  GtkWidget           * table;			/* The top level box */
};

struct _GXsnmp_traffic_dialogClass
{
  GnomePropertyDialogClass  parent_class; 	/* Class base structure */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

guint	    gxsnmp_traffic_dialog_get_type	(void);
GtkWidget * gxsnmp_traffic_dialog_new		();

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_TRAFFIC_DIALOG_H__ */

/* EOF */













