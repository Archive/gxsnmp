/*
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Jochen Friedrich
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  plugin-mtr.c -- A sample plugin, it just adds the MTR command to the host
**                  menu for testing.
**
**  MTR stands for "Matt's Traceroute"
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include "plugins.h"
#include "main.h"

/* There should *really* be a public method of the individual map_item objects 
** to add more general menu items if a user wants this. Some external programs
** might be of general interest, no matter of the sub type. mtr, eg. can be
** useful no matter if the host object is a database server, a printer or
** some sort of router.
*/

#include "gxsnmp_map.h"
#include "gxsnmp_host.h"

/* FIXME: You must remove the static attribute in menu.c to make this work. Else
** you get an undefined symbol if you load this module
*/

extern GtkWidget * host_popup_menu;

extern gxsnmp *app_info;

/******************************************************************************
**
**  Forward references
**
******************************************************************************/

static void menu_callback (GtkWidget * widget, gpointer data);
static void trace_menu_cb (GtkWidget * widget, gpointer data);

/******************************************************************************
**
**  Static data
**
******************************************************************************/

static gchar * menu_location;

/******************************************************************************
**
**  The plugin load routine
**
******************************************************************************/

gint
load_plugin (PluginData * pd)
{
  pd->type = PLUGIN_APPLICATION;
  return 0;
}

/******************************************************************************
**
**  The plugin unload routine
**
******************************************************************************/

void
unload_plugin (PluginData * pd)
{
  g_print ("MTR plugin unloaded\n");
}

/******************************************************************************
**
**  The plugin start routine
**
******************************************************************************/

gint
start_plugin (PluginData * pd)
{
  GnomeUIInfo   * menu;
  GtkWidget     * menu_item;

  g_print ("Starting MTR plugin.\n");

  menu                  = g_malloc0 (2 * sizeof(GnomeUIInfo));
  menu->type            = GNOME_APP_UI_ITEM;
  menu->label           = g_strdup ("MTR Host");
  menu->hint            = NULL;
  menu->moreinfo        = trace_menu_cb;       /* Menu activation cb routine */
  menu->user_data       = NULL;                /* User data for the callback */
  menu->unused_data     = NULL;                /* Reserved for future use */
  menu->pixmap_type     = GNOME_APP_PIXMAP_STOCK;
  menu->pixmap_info     = GNOME_STOCK_MENU_BLANK;
  menu->accelerator_key = 0;
  (menu + 1)->type      = GNOME_APP_UI_ENDOFINFO;

  gxsnmp_add_to_host_popup(menu);

  return 0;
}

/******************************************************************************
**
**  The menu callback routine
**
******************************************************************************/

static void
menu_callback (GtkWidget * widget, gpointer data)
{
  g_print ("MTR plugin menu callback invoked\n");
}

void
host_panel_trace(char *host)
{
  char buffer[1024];

  snprintf(buffer, sizeof(buffer), "mtr %s &", host);
  system(buffer);
}
 
static void           
trace_menu_cb (GtkWidget *widget, gpointer data)
{
  GXsnmp_map_item * item;
  DB_host         * dbh;
  GtkWidget       * dialog_widget;

  g_return_if_fail (GXSNMP_IS_HOST (data));
  item = GXSNMP_MAP_ITEM (data);
  g_return_if_fail (item->DB_graph != NULL);
  g_return_if_fail (item->DB_graph->type == DB_GRAPH_HOST);
  dbh = item->DB_graph->DB_host;
  g_return_if_fail (dbh != NULL);

  host_panel_trace (dbh->dns_name);
}

/* EOF */
