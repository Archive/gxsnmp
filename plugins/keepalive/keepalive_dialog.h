/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  The keepalive_dialog widget is the control panel for the keepalive
**  plugin.
*/

#ifndef __GXSNMP_KEEPALIVE_DIALOG_H__
#define __GXSNMP_KEEPALIVE_DIALOG_H__

#include <gnome.h>
#include "gnome-property-dialog.h"
#include "tables.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
**
**  Standard widget macros
**
*******************************************************************************/

#define GXSNMP_TYPE_KEEPALIVE_DIALOG (gxsnmp_keepalive_dialog_get_type())

#define GXSNMP_KEEPALIVE_DIALOG(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_KEEPALIVE_DIALOG, \
						GXsnmp_keepalive_dialog)

#define GXSNMP_KEEPALIVE_DIALOG_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_KEEPALIVE_DIALOG, \
			      GXsnmp_keepalive_dialogClass)

#define GXSNMP_IS_KEEPALIVE_DIALOG(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_KEEPALIVE_DIALOG)

#define GXSNMP_IS_KEEPALIVE_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_KEEPALIVE_DIALOG)

/*******************************************************************************
**
**  Control blocks for the widget class and for widget instances
**
*******************************************************************************/

typedef struct _GXsnmp_keepalive_dialog	     GXsnmp_keepalive_dialog;
typedef struct _GXsnmp_keepalive_dialogClass GXsnmp_keepalive_dialogClass;

struct _GXsnmp_keepalive_dialog
{
  GnomePropertyDialog   dialog;			/* Base structure */
  GtkWidget           * table;			/* The top level box */
};

struct _GXsnmp_keepalive_dialogClass
{
  GnomePropertyDialogClass  parent_class; 	/* Class base structure */
};

/*******************************************************************************
**
**  Public widget manipulation functions
**
*******************************************************************************/

guint	    gxsnmp_keepalive_dialog_get_type	(void);
GtkWidget * gxsnmp_keepalive_dialog_new		();

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __GXSNMP_KEEPALIVE_DIALOG_H__ */

/* EOF */













