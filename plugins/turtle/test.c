/*
 * This is just supposed to be testing the add host stuff
 */

#include <gnome.h>
#include "plugins.h"

/* Bah - wait for the API to settle first */
#if 0
gboolean
turtle_add_host(gxsnmp *app_info, gchar *name, gchar *dnsname)
{
	DB_host *myhost;

	/* Check to see if it already exists */
	myhost = host_find_by_name(name);

	if (myhost == NULL) 
	{
		g_print("Couldn't find that host, adding one.\n");

		myhost = host_create();

		myhost->dns_name = g_strdup(dnsname);
		myhost->name = g_strdup(name);
		host_add(myhost);
	} else {
		g_print("Updating host\n");
		myhost->dns_name = g_strdup(dnsname);
		host_update(myhost);
	}

}

void
turtle_callback (GtkWidget *widget, gpointer data)
{
	gxsnmp *app_info = (gxsnmp*)data;

	g_print("Adding a test host\n");
	turtle_add_host(app_info, "testhost", "testhost.foo");

}


void
turtle_test_net (GtkWidget *widget, gpointer data)
{
	DB_network *mynet;
	gxsnmp *app_info = (gxsnmp*)data;

	g_print("Adding a test network\n");
}
#endif
