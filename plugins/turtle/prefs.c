/*
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  prefs.c: turtle plugin configuration
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "plugins.h"

/* Properties Dialog */
typedef struct {
    GtkWidget *dlg;
    GtkWidget *clist;
} PropDlg;

/* Configuration menu */
void plugin_config(PluginData *pd)
{
    GtkWidget *w;
    PropDlg *dlg;
    gchar *s;

    dlg = g_malloc(sizeof(PropDlg));
    dlg->dlg = gnome_property_box_new();
    gtk_window_set_title(GTK_WINDOW(dlg->dlg),_("Turtle Properties"));
    dlg->clist = gtk_clist_new(2);
    w = gtk_label_new(_("Remote Turtles"));
    gnome_property_box_append_page(GNOME_PROPERTY_BOX(dlg->dlg),
            dlg->clist, w);
    gtk_widget_show(w);
    gtk_widget_show(dlg->clist);

    gtk_widget_show(dlg->dlg);
}

/* Save Configuration */
gboolean plugin_save(PluginData *pd)
{
    g_print("Saving turtle configs.\n");
    return TRUE;
}

/* Load Configuration */
gboolean plugin_load(PluginData *pd)
{
    g_print("Loading turtle configs.\n");
    return TRUE;
}

