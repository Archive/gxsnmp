/*
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  plugin-turtle.c -- Someday this will be a real live turtle.
*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "plugins.h"
/*#include "main.h"*/
#include "prefs.h"


/******************************************************************************
**
**  Static data
**
******************************************************************************/

static gchar * menu_location;
static void    menu_callback (GtkWidget * widget, gpointer data);
static void turtle_conf_cb (GtkWidget *widget, gpointer data);

/******************************************************************************
**
**  The plugin load routine
**
**  This function is invoked at the very start of GXSNMP.  It should
**  set one or more of the following flags:
**
**  PLUGIN_DATABASE	-- for plugins that provide a database service
**  PLUGIN_COLLECTOR	-- for plugins that do not require GTK services
**  PLUGIN_APPLICATION	-- for plugins that require GTK services
**
**  The flags determine at what point the start function is called during
**  initialization.  The database plugins need to be started before the
**  database is read in (for obvious reasons), and the collector and 
**  application plugins are not started until the database is completely
**  read in and set up.
**
******************************************************************************/

extern void turtle_callback(GtkWidget *widget, gpointer data);

GnomeUIInfo turtle_menu[] = {
	{ GNOME_APP_UI_ITEM, "Add Test Host", NULL, turtle_callback, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT, 0, 0, NULL },
	{ GNOME_APP_UI_ENDOFINFO }
};

GnomeUIInfo top_turtle_menu[] = {
	{ GNOME_APP_UI_SUBTREE, ("Discovery"), NULL, turtle_menu, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
	{ GNOME_APP_UI_ENDOFINFO }
};

GnomeUIInfo configure_turtle_menu[] = {
        { GNOME_APP_UI_ITEM, "Turtle", NULL, turtle_conf_cb, NULL, NULL,
            GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF, 0, 0, NULL},
        { GNOME_APP_UI_ENDOFINFO }
};

gint
load_plugin (PluginData * pd)
{
  pd->type = PLUGIN_APPLICATION;
  pd->name = g_strdup("Turtle master");
  pd->plugin_config = plugin_config;
  return 0;
}

/******************************************************************************
**
**  The plugin unload routine
**
**  This function is invoked during the shutdown callback in main.c
**  It should free any storage the plugin might have allocated, and
**  generally clean itself up.
**
******************************************************************************/

void
unload_plugin (PluginData * pd)
{
}

/******************************************************************************
**
**  The plugin start routine
**
**  This function is invoked during initialization.  It should perform      
**  initialization and start the plugin running.
**
**  Included here is sample code to install the plugin in the 'plugins' menu.
**  A collector-only plugin would NOT install a menu entry, and would not
**  have a menu callback routine (below).
**
******************************************************************************/

gint 
start_plugin (PluginData * pd)
{
    app_update_init_status ("Starting plugins.", "Turtle master");
  return 0;
}

/******************************************************************************
**
**  Menu callback routine for the plugin
**
**  This function is invoked when the user selects the plugin from the
**  plugins menu.
**
******************************************************************************/
/*
static void
menu_callback (GtkWidget * widget, gpointer data)
{
  g_print ("I am TURTLE.  See me crawl\n");
}
*/

static void
turtle_conf_cb (GtkWidget *widget, gpointer data)
{
    g_print("Configuring turtle\n");
}

