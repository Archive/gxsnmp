/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  The process_dialog widget is the control panel for the process
**  plugin.
*/

#ifndef __GXSNMP_PROCESS_DIALOG_H__
#define __GXSNMP_PROCESS_DIALOG_H__

#include <gnome.h>
#include "gnome-property-dialog.h"
#include "tables.h"

BEGIN_GNOME_DECLS
/**************************************************************************
 *  Standard widget macros
 *************************************************************************/
#define GXSNMP_TYPE_PROCESS_DIALOG (gxsnmp_process_dialog_get_type())
#define GXSNMP_PROCESS_DIALOG(obj) \
	GTK_CHECK_CAST ((obj), GXSNMP_TYPE_PROCESS_DIALOG, \
						GXsnmp_process_dialog)
#define GXSNMP_PROCESS_DIALOG_CLASS(klass) \
 	GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_PROCESS_DIALOG, \
			      GXsnmp_process_dialogClass)
#define GXSNMP_IS_PROCESS_DIALOG(obj) \
	GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_PROCESS_DIALOG)
#define GXSNMP_IS_PROCESS_DIALOG_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_PROCESS_DIALOG)
/**************************************************************************
 *  Control blocks for the widget class and for widget instances
 *************************************************************************/
typedef struct _GXsnmp_process_dialog	     GXsnmp_process_dialog;
typedef struct _GXsnmp_process_dialogClass   GXsnmp_process_dialogClass;
typedef struct _ps_data                      ps_data;
struct _GXsnmp_process_dialog
{
  GnomeDialog           dialog;			/* Base structure */
  GtkWidget             *table;			/* The top level box */
  GtkWidget             *clist;

  ps_data               *ps;

};
struct _GXsnmp_process_dialogClass
{
  GnomeDialogClass      parent_class; 	/* Class base structure */
};
struct _ps_data
{
  gint          rowid;     /* rowid of the DB_interface to use */
  host_snmp     host;      /* Host snmp structure for the process requests */
  GtkWidget     *dialog;
  gpointer      table;     /* The SNMP request */
};
/**************************************************************************
 *  Public widget manipulation functions
 *************************************************************************/
guint	    gxsnmp_process_dialog_get_type	(void);
GtkWidget * gxsnmp_process_dialog_new		(ps_data *ps);
void        process_dialog_set_state            (GXsnmp_process_dialog  *dialog);
END_GNOME_DECLS
#endif /* __GXSNMP_PROCESS_DIALOG_H__ */
/* EOF */



