/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*/

/*
**  sw_dialog.c is the dialog for the software function
*/

#include <gnome.h>
#include "main.h"			/* Needed for gxsnmp struct */
#include "sw_dialog.h"
#include "tables.h"
#include "gnome-property-dialog.h"
#include "gnome-dialog-create-button.h"

extern gxsnmp *app_info;

/*************************************************************************
 *  Static data 
 ************************************************************************/

#define CLIST_COLUMNS 4

static char * titles [] =
{
  "ID", "Name", "Type", "Date"
};

static gint title_widths[4] = { 8, 13, 13, 13 };
static gint total_height = 16;
static gint total_width	= 86;	/* Empirically determined :-| */
/**************************************************************************
 *  Forward declarations and callback functions
 *************************************************************************/
static void sw_dialog_class_init (GXsnmp_sw_dialogClass       *klass);
static void sw_dialog_init	 (GXsnmp_sw_dialog            *dialog);
static void sw_dialog_cb         (GnomeDialog                 *dialog,
				  gint                         button,
				  gpointer                     data);

/**************************************************************************
 *  gxsnmp_sw_dialog_get_type()
 *************************************************************************/
GtkType
gxsnmp_sw_dialog_get_type ()
{
  static guint widget_type = 0;

  if (!widget_type)
    {
      GtkTypeInfo widget_info =
      {
        "GXsnmp_sw_dialog",
        sizeof (GXsnmp_sw_dialog),
        sizeof (GXsnmp_sw_dialogClass),
        (GtkClassInitFunc) sw_dialog_class_init,
        (GtkObjectInitFunc) sw_dialog_init,
        (GtkArgSetFunc) NULL,
        (GtkArgGetFunc) NULL
      };
      widget_type = gtk_type_unique (gnome_dialog_get_type (), 
				     &widget_info);
    }
  return widget_type;
}

/****************************************************************************
 *  The class initialization subroutine
 ***************************************************************************/

static void
sw_dialog_class_init (GXsnmp_sw_dialogClass *class)
{
}

/*****************************************************************************
**
**  The widget initialization subroutine
**
*****************************************************************************/

static void
sw_dialog_init (GXsnmp_sw_dialog *dialog)
{
  GtkWidget * label;
  GtkWidget * scrolled_win;	
  gint        c_width;		/* Average width of a character */
  gint	      c_height;		/* Average height of a character */
  gint	      i;

/*
**  Build the main framework for the whole thing
*/

  dialog->table = gtk_table_new (1, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      dialog->table, TRUE, TRUE, 0);
  gtk_widget_show (dialog->table);
  c_width  = gdk_string_width (dialog->table->style->font, "xW") / 2;
  c_height = dialog->table->style->font->ascent + 
             dialog->table->style->font->descent;

/*
**  The title label
*/

  label = gtk_label_new (_("Node software table"));
  gtk_table_attach (GTK_TABLE (dialog->table), label,
		    0, 1, 0, 1, 0, 0, 0, 0);
  gtk_widget_show (label);

/*
**  Create a scrolled window, and insert the clist in the window
*/

  scrolled_win = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_set_border_width (GTK_CONTAINER (scrolled_win), 5);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
                                  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_table_attach (GTK_TABLE (dialog->table), scrolled_win, 
		    0, 1, 1, 2, 0, 0, 0, 0);
  gtk_widget_show (scrolled_win);

  dialog->clist = gtk_clist_new_with_titles (CLIST_COLUMNS, titles);
 
  gtk_container_add (GTK_CONTAINER (scrolled_win), dialog->clist);
  for (i=0; i < CLIST_COLUMNS; i++)
    {
    gtk_clist_set_column_width (GTK_CLIST (dialog->clist), i, title_widths[i] * c_width);
    }
  gtk_widget_set_usize (dialog->clist, total_width * c_width , 
			       total_height * c_height);
  gtk_widget_show (dialog->clist);
  gnome_dialog_close_hides (GNOME_DIALOG (dialog), FALSE);

  /* Append some buttons */
  /* Button 0 */
  gnome_dialog_append_button_with_pixmap (GNOME_DIALOG (dialog),
					  "Abort",
					  GNOME_STOCK_PIXMAP_STOP);
  /* Button 1 */
  gnome_dialog_append_button_with_pixmap (GNOME_DIALOG (dialog),
					  "Reload",
					  GNOME_STOCK_PIXMAP_REFRESH);
  /* Button 2 & 3 */
  gnome_dialog_append_buttons (GNOME_DIALOG (dialog),
			       GNOME_STOCK_BUTTON_CLOSE, 
			       GNOME_STOCK_BUTTON_HELP,
			       NULL);

  gtk_signal_connect (GTK_OBJECT (dialog), "clicked",
		      (GtkSignalFunc) sw_dialog_cb, dialog);

}
/****************************************************************************
 * Callback for the custom dialog buttons
 ***************************************************************************/
static void
sw_dialog_cb (GnomeDialog *dialog, gint button, gpointer data)
{
  GXsnmp_sw_dialog       *sdialog;
  sw_data                *sw;
  sdialog = GXSNMP_SW_DIALOG (dialog);
  sw = sdialog->sw;
  switch (button)
    {
    case 0:                                       /* Abort button */
      g_print ("Abort!\n");
      if (sw->table)
	g_snmp_table_destroy (sw->table);
      sw->table = NULL;
      sw_dialog_set_state (sdialog);
      break;
    case 1:                                       /* Reload */
      gtk_clist_clear (GTK_CLIST (sdialog->clist));
      sw->table = NULL;
      sw_start_request (sw);
      g_print ("Reload %s!\n", sw->host.name);
      break;
    case 2:                                       /* Close button */
      if (sw->table)
	g_snmp_table_destroy (sw->table);
      sw->table = NULL;
      gnome_dialog_close (GNOME_DIALOG (dialog));
      break;
    case 3:                                       /* Help button */
      g_print ("Help me! Help me!\n");
      break;
    default:
      g_print ("Button %d hit!\n", button);
      break;
    }
}
/****************************************************************************
 * Set the state of the buttons.
 ***************************************************************************/
void
sw_dialog_set_state (GXsnmp_sw_dialog *dialog)
{
  sw_data   *sw;

  sw = dialog->sw;
  if (sw->table)
    {
      g_print ("Setting state to a request running state....\n");
      /* have a request running turn off the reload button */
      gnome_dialog_set_sensitive (GNOME_DIALOG (dialog), 1, FALSE);
      /* Make sure the abort button can be clicked */
      gnome_dialog_set_sensitive (GNOME_DIALOG (dialog), 0, TRUE);
    }
  else
    {
      g_print ("Setting state to idle..\n");
      gnome_dialog_set_sensitive (GNOME_DIALOG (dialog), 0, FALSE);
      gnome_dialog_set_sensitive (GNOME_DIALOG (dialog), 1, TRUE);
    }
}

/****************************************************************************
 *  Public function to create a new sw dialog widget  
 ****************************************************************************/
GtkWidget *
gxsnmp_sw_dialog_new (sw_data *sw)
{
  GXsnmp_sw_dialog * sw_dialog;

  sw_dialog = gtk_type_new( gxsnmp_sw_dialog_get_type());
  sw_dialog->sw = sw;
  sw_dialog_set_state (sw_dialog);
  return GTK_WIDGET (sw_dialog);
}

/* EOF */
