#
#  The NETWORK table is used to store information about networks
#
#  _rowid	is a positive integer, unique within the NETWORK table,
#		that is used to identify a specific row in the table.
#
#  created	is the date and time that the row entry was created
#
#  modified	is the date and time that the row entry was last modified
#  
#  address	is the address of the network.  For IPv4, address specifies
#		the first address of the network or subnet, in dotted decimal
#		format.
#
#  netmask	Specifies the netmask for the network, if specified.
#		For IPv4, the netmask is represented in dotted decimal format.
#
#  speed	is the rated speed of the network, in KILOBYTES per second.
#
#  name		is the user assigned name of the network.  This is the name
#		that is displayed on the maps.
# 
#  description  is the user assigned description of the network, possibly
#		including its physical location or other notes.
#
#  contact      is the user assigned name of the person responsible for the
#		network, and possibly other identifying information, such
#		as a phone number
#
#  tags         is the application specific tags for this object.
#
create table network (
   _rowid	int not null,
   created	char(20),
   modified	char(20),
   address      char(64),
   netmask	char(64),
   speed        int,
   name         char (16),
   description  char (127),
   contact	char (64),
   tags         char (255),
   primary key  (_rowid)
);

