/*
 * $Id$
 * 
 * TCP Service  -- Handles all socket level issues.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "g_sql.h"
#include "g_sql-table.h"
#include "pdu.h"
#include "gxsnmpdb_types.h"
#include "gxsnmpdbcore.h" /*think its obsolete*/

extern GList *tranq;
extern GList *profiles;
extern GMainLoop *loop;

gchar bufme[1024];
G_sql_connection *dbhandler;
G_sql_table *tbh;

/****************************************************************************
 * Forward references
 ***************************************************************************/
gboolean             tcp_new_connection           (GIOChannel     *source,
						   GIOCondition   condition,
						   gpointer       data);
gboolean             tcp_client_activity          (GIOChannel     *source,
						   GIOCondition   condition,
						   profile        *);
/****************************************************************************
 * Implementation.
 ***************************************************************************/
void
tcp_enable_reuseaddr (gint sock)
{
  gint tmp = 1;
  if (sock < 0)
    return;
  if (setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, (gchar *)&tmp, 
		  sizeof (tmp)) == -1)
    perror ("Bah! Bad setsockopt ()\n");
}

void 
tcp_enable_nbio (gint fd)
{
  if (fcntl (fd, F_SETOWN, getpid()) == -1)
    perror ("fcntl (F_SETOWN) error\n");
  if (fcntl (fd, F_SETFL, FNDELAY) == -1)
    perror ("fcntl (F_SETFL, FNDELAY\n");
}

void 
tcp_socket_init (gint port)
{
  gint                s;
  struct sockaddr_in  addr;
  GIOChannel          *channel;

  if (port <= 0)
    return ;

  s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (s == -1)
    return;
  tcp_enable_reuseaddr (s);
  memset (&addr, 0, sizeof (addr));
  addr.sin_family       = AF_INET;
  addr.sin_port         = htons ((u_short)port);
  addr.sin_addr.s_addr  = INADDR_ANY;
  if (bind (s, (struct sockaddr *)&addr, sizeof (addr)) == -1)
    {
      close (s);
      return ;
    }
  if (listen (s, 5) == -1)
    {
      close (s);
      return ;
    }
  channel = g_io_channel_unix_new (s);
  g_io_add_watch (channel, G_IO_IN, tcp_new_connection, NULL);
}

/****************************************************************************
 * Callback functions
 ***************************************************************************/
/**
 * tcp_new_connection 
 * Will accept a new connection and add the client to the list of clients.
 * actual client communication is handled elsewhere.
 **/
gboolean 
tcp_new_connection (GIOChannel *source, GIOCondition cond, gpointer data)
{
  gint               new;              /* new socket descriptor */
  gint               client;
  GIOChannel         *new_channel;
  struct sockaddr_in client_addr;
  profile *prf;

  if (cond == G_IO_IN)
    {
      if ( (new = accept (g_io_channel_unix_get_fd (source),
			  (struct sockaddr *)&client_addr, &client)) < 0)
	{
	  g_warning ("Unable to accept new connection.");
	  return FALSE;
	}
      new_channel = g_io_channel_unix_new (new);
      if(!(prf = g_malloc(sizeof(profile)))){
        fprintf(stderr,"Couldn't connect to client, due to malloc() failure\n");
        return FALSE;
      }
      prf->value = DBCTL_NOTIFY_FULL | DBCTL_AUTH | DBCTL_RW;	
		/* a connection has full notification, read_write and
                   is authenticated by default*/
      prf->source = new_channel;
      profiles = g_list_append(profiles, prf);
      g_io_add_watch (new_channel, G_IO_IN, tcp_client_activity, prf);
    }
  fprintf(stderr,"TRUE\n");
  return TRUE;
}

/**
 * tcp_client_activity
 * Handles input from the clients and passes it off to the parser/dispatcher.
 * This will get the raw data from the socket, make sure there is a NULL 
 * terminator, and call the command dispatcher.
 **/
gboolean 
tcp_client_activity (GIOChannel *source, GIOCondition cond, profile *prf)
{
  guint num_read = 0;
  guint h, tpl, tl;  /* header len h, total_packet_len tpl, table len tl */
  tranrec *rec;
  GList *gl;

  if (cond == G_IO_IN)
    {
      g_print ("Read channel...\n");
      if (g_io_channel_read (source, bufme, sizeof (bufme), &num_read) == G_IO_ERROR_NONE)
        {
          if(!num_read) 
	    return FALSE;
	  bufme[num_read] = '\0';           /* Make sure its null terminated */
          g_print ("Read (%d) from the socket\n", num_read);
              /*Process packet header only
                assing request to transaction list */
          rec = g_malloc0 ((unsigned int)sizeof(tranrec));		/* alloc space to hold a record */
	  
          memcpy(&rec->action, bufme, sizeof(guint));			/* get action */
          fprintf(stderr,"  action: %d\n", rec->action);
          memcpy(&tpl, bufme+sizeof(guint), sizeof(guint));		/* get Total packet length*/
          memcpy(&rec->table, bufme+(sizeof(gint)*2 ), sizeof(gint));	/* get table name */
          fprintf(stderr,"  table_name: %d\n", rec->table);
	  
          h = sizeof(gint) * 3;						/* h is header length */

	  /* pointer into memberrecords */
          rec->mlen = tpl - h;						/* set length of member records */
	  fprintf(stderr,"  MemberRecords len: %d\n", rec->mlen);
          if(rec->mlen)
	    {								/* copy memberRecords if there are any*/
	      rec->members = g_malloc0 ((unsigned int)rec->mlen);	/* alloc space to hold all members */
	      memcpy(rec->members, bufme+h, rec->mlen);			/* copy all members */
	    } 
	  else 
	    rec->members = 0;
          rec->source = source;
          rec->prf = prf;

          /* OK, rec is setup, assign it to queue 'tranq' */
          tranq = g_list_append(tranq, rec);
        }
    }
  return TRUE;
}

/**
 * tcp_client_disconnect()
 * Called when A client disappears, should remove it from the list and
 * go on with life.
 *
 * go through transaction request list, and remove read-only actions,
 * from this source
 *
 * remove this user from profile list
 *
 **/
gboolean
tcp_client_disconnect (GIOChannel *source, GIOCondition cond, gpointer data)
{
  GList *gl;
/*
  gl = tranrec;
  while(gl)
    {
      if( ((tranrec *)gl->data)->source == source){
*/
        /*ok check if request is stale.*/
/*
        if( ((tranrec *)gl->data)->action == ROW_READ   ||
            ((tranrec *)gl->data)->action == TABLE_LOAD    )
          {
            tranrec = g_list_remove(gl);
          }
      }
      gl->next;
    }
*/
  return TRUE;
}

