/*
 * Common utility functions.
 */

#ifndef __UTIL_H__
#define __UTIL_H__

/* copy a space seperated word 'w' to destination 'dst' from 'src' */
gchar              *wordcpyt                      (gchar       *dst,
						   gchar       *src,
						   gint        w);

/* Produce a 'standard' timestamp for the databases */
gchar              *db_timestamp                  ();

/* Establish a connection to the database daemon (client) */
gint               dd_connect                     (gchar       *IP, gint port);

/* Disconnect from the database daemon */
void               dd_disconnect                  (gint        sock);

#endif
