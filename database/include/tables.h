/*
**  $Id$
**
**  GXSNMP - An snmp management application
** 
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
** 
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
** 
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
** 
**  Database column definitions.  See tables.c for more information.
*/
/* OBJECT DEFINITION IS FOUND IN BOTTOM */

#ifndef __TABLES_H__
#define __TABLES_H__

#include "g_sql.h"
#include "g_sqldb.h"

extern G_sqldb_table * snmp_sqldb;
extern G_sqldb_table * host_sqldb;
extern G_sqldb_table * interface_sqldb;
extern G_sqldb_table * network_sqldb;
extern G_sqldb_table * map_sqldb;
extern G_sqldb_table * graph_sqldb;
extern G_sqldb_table * hinventory_sqldb;
extern G_sqldb_table * hitem_sqldb;

/*
**  The SNMP table is used to store information about SNMP configurations.
*/

typedef struct _DB_snmp
{
  guint           rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gchar         * name;                 /* Name of SNMP configuration */
  guint           version;              /* SNMP version to use */
  guint           port;                 /* SNMP port to contact */
  guint           timeout;              /* Seconds before timeout */
  guint           retries;              /* Retries before failure */
  gchar         * read_c;               /* Read community string */
  gchar         * write_c;              /* Write community string */
  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */

  GList         * DB_interfaces;        /* GList of all interfaces that use
					   this SNMP definition */
}
DB_snmp;

/*
**  The INTERFACE table is used to store information about network interfaces.
*/

typedef struct _DB_interface
{
  guint           rowid;		/* Unique row identifier */
  gchar         * created;		/* Row creation date/time */
  gchar         * modified;		/* Last row modification date/time */
  guint		  host;			/* RowID of host owning interface */  
  guint		  snmp;			/* RowID of snmp Def. for interface */
  guint		  transport;		/* AF_xxx Def. of transport method */
  gchar 	* address;		/* Interface hardware address */
  gchar		* netmask;		/* IPv4 netmask used by interface */
  gchar		* name;			/* Name of interface, e.g. "eth0" */
  gpointer	  g_sqldb_private;	/* For use by the g_sqldb library */

  struct _DB_host * DB_host;		/* pointer to parent host structure */
  DB_snmp	* DB_snmp;		/* pointer to snmp definition */

  gpointer	  application;		/* Pointer to application-specific
					   data for the interface.  Currently
					   unused. */
}
DB_interface;

/*
**  The HOST table is used to store information about hosts
*/

typedef struct _DB_host
{
  guint           rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gchar         * dns_name;             /* Preferred DNS name of host */
  gchar         * name;                 /* Name of host to display on maps */
  gchar         * description;          /* User-provided description of host */
  gchar         * contact;              /* User-provided contact information */
  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */

  GList         * DB_interfaces;        /* GList of all interfaces assigned to
                                           this host */
  GList         * DB_graphs;            /* GList of graphical representations
                                           of this host */

  gpointer        application;          /* Pointer to application-specific
                                           data for the host.  Currently
                                           unused. */
}
DB_host;

/*
**  The NETWORK table is used to store information about networks
*/

typedef struct _DB_network
{
  guint           rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gchar         * address;              /* Starting address of network */
  gchar         * netmask;              /* IPv4 netmask of network */
  guint           speed;                /* Speed of network, in KBytes/Sec */
  gchar         * name;                 /* User-assigned name of network */
  gchar         * description;          /* User-provided Desc. of network */
  gchar         * contact;              /* User-provided contact information */
  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */

  GList         * DB_graphs;            /* GList of graphical representations */

  gpointer	  application;		/* Pointer to application-specific
					   data for the network.  Currently
					   unused. */
}
DB_network;

/*
**  The MAP table is used to store information about a map.
*/

typedef struct _DB_map
{
  guint           rowid;        	/* Unique row identifier */
  gchar         * created;      	/* Row creation date/time */
  gchar         * modified;     	/* Last row modification date/time */
  gchar         * name;         	/* Name of this map */
  gchar         * tab;          	/* Name to display in map tab */
  gchar		* description;  	/* User-provided description of map */
  gpointer	  g_sqldb_private;	/* For use by the g_sqldb library */

  GList		* DB_graphs;		/* GList of DB_graph objects
					   displayed on this map */
  gpointer	  application;		/* Pointer to application-specific
					   data for the network.
					   The GUI application uses this
					   field to point to the gxsnmp_map 
					   object. */
}
DB_map;

/*
**  The GRAPH table is used to store graphical information about items that
**  appear in maps.
*/

enum
{
  DB_GRAPH_INVALID,			
  DB_GRAPH_HOST,			/* Possibilities for the */
  DB_GRAPH_NETWORK,			/* DB_graph.type field */
  DB_GRAPH_WIRE				
};

typedef struct _DB_graph
{
  gint            rowid;                /* Unique row identifier */
  gchar         * created;              /* Row creation date/time */
  gchar         * modified;             /* Last row modification date/time */
  gint            map;                  /* Map this row belongs to */
  gint            type;                 /* Type code of item below */
  gint		  host;			/* Rowid of associated host */
  gint            network;              /* Rowid of associated network */
  gchar         * details;              /* Widget-specific data for item */
  gdouble         x;                    /* TEMP: x location of object */
  gdouble         y;                    /* TEMP: y location of object */
  gchar		* pixmap;		/* TEMP: pixmap filename */
  gpointer        g_sqldb_private;      /* For use by the g_sqldb library */

  DB_map	* DB_map;		/* Pointer to associated map */
  struct _DB_host	* DB_host;		/* Pointer to associated host */
  DB_network 	* DB_network;		/* Pointer to associated network */

  gpointer        application;          /* Pointer to application-specific
					   data for the map item.
                                           The GUI application uses this
                                           field to point to either the
					   gxsnmp_host object, the
					   gxsnmp_network object, or the
					   gxsnmp_wire object, depending
					   on the value of DB_graph.type. */
}
DB_graph;
/****************************************************************************
 * The inventory of hardware on a given node.
 ***************************************************************************/
typedef struct _DB_hinventory 
{
  gint         rowid;                    /* unique row identifier */
  gchar        *created;
  gchar        *modified;
  gint         item;                     /* Rowid of the associated item */
  gint         host;                     /* Rowid of the associated host */
  gchar        *serial;                  /* serial # of this item */
  gchar        *installed;               /* date/time of installation */
  gchar        *removed;                 /* date/time of de-installation */
  gpointer     g_sqldb_private;
}
DB_hinventory;
/****************************************************************************
 * The Hardware item table
 ***************************************************************************/
typedef struct _DB_hitem
{
  gint         rowid;
  gchar        *group;                   /* group of items this item is in*/
  gchar        *created;
  gchar        *modified;
  gchar        *name;
  gchar        *description;
  gchar        *model;
  gchar        *vendor;
  gpointer     g_sqldb_private;
}
DB_hitem;

/****************************************************************************
**
**  The application programming interface for the GXSNMP database
**
****************************************************************************/

void		table_load_topology	();
void		table_load_graphical    ();



/* OBJECT DATABASE DEFINITIONS 
 *
 * This should be a single file, but when everybody includes tables.h
 * I use this for now.
 */


typedef struct _gxdb_tables {
   G_sqldb_table * snmp_sqldb;
   G_sqldb_table * host_sqldb;
   G_sqldb_table * interface_sqldb;
   G_sqldb_table * network_sqldb;
   G_sqldb_table * map_sqldb;
   G_sqldb_table * graph_sqldb;
   G_sqldb_table * hiventory_sqldb;
   G_sqldb_table * hitem_sqldb;
} gxdb_tables;

/* This enumeration defines what gxobject.type to
   assign, the type tells us what type gxsobject.obj is */

typedef enum {
  OBJ_DB = 1
} GxSNMP_OBJECT_CONTEXT;

typedef enum {
  OBJ_DB_NULL = 1,		/* empty object */
  OBJ_DB_HOST,			/* DB_host struct */
  OBJ_DB_INTERFACE,		/* DB_interface struct */
  OBJ_DB_MAP,			/* DB_map struct */
  OBJ_DB_NETWORK,		/* DB_network struct */
  OBJ_DB_SNMP,			/* DB_snmp struct */
  OBJ_DB_HITEM,			/* DB_hitem struct */
  OBJ_DB_GRAPH, 		/* DB_graph struct */
  OBJ_DB_HOST_LIST,		/* pointer to a glist of DB_host struct's */
  OBJ_DB_INTERFACE_LIST		/* pointer to a glist of DB_host struct's */
} GxSNMP_OBJECT_Types;

typedef enum {
  OBJ_DB_TABLE_GRAPH_ROWID = 4, /* WARNING, this must not overlap with GxSNMP_PDU_Types */
  OBJ_DB_TABLE_GRAPH_CREATED,
  OBJ_DB_TABLE_GRAPH_MODIFIED,
  OBJ_DB_TABLE_GRAPH_MAP,
  OBJ_DB_TABLE_GRAPH_TYPE,
  OBJ_DB_TABLE_GRAPH_HOST,
  OBJ_DB_TABLE_GRAPH_NETWORK,
  OBJ_DB_TABLE_GRAPH_DETAILS,
  OBJ_DB_TABLE_GRAPH_X,
  OBJ_DB_TABLE_GRAPH_Y,
  OBJ_DB_TABLE_GRAPH_PIXMAP,

  OBJ_DB_TABLE_HITEM_ROWID,
  OBJ_DB_TABLE_HITEM_GROUP,
  OBJ_DB_TABLE_HITEM_CREATED,
  OBJ_DB_TABLE_HITEM_MODIFIED,
  OBJ_DB_TABLE_HITEM_NAME,
  OBJ_DB_TABLE_HITEM_DESCRIPTION,
  OBJ_DB_TABLE_HITEM_MODEL,
  OBJ_DB_TABLE_HITEM_VENDOR,

  OBJ_DB_TABLE_HOST_ROWID,
  OBJ_DB_TABLE_HOST_CREATED,
  OBJ_DB_TABLE_HOST_MODIFIED,
  OBJ_DB_TABLE_HOST_DNS_NAME,
  OBJ_DB_TABLE_HOST_NAME,
  OBJ_DB_TABLE_HOST_DESCRIPTION,
  OBJ_DB_TABLE_HOST_CONTACT,

  OBJ_DB_TABLE_INTERFACE_ROWID,
  OBJ_DB_TABLE_INTERFACE_CREATED,
  OBJ_DB_TABLE_INTERFACE_MODIFIED,
  OBJ_DB_TABLE_INTERFACE_HOST,
  OBJ_DB_TABLE_INTERFACE_SNMP,
  OBJ_DB_TABLE_INTERFACE_TRANSPORT,
  OBJ_DB_TABLE_INTERFACE_ADDRESS,
  OBJ_DB_TABLE_INTERFACE_NETMASK,
  OBJ_DB_TABLE_INTERFACE_NAME,

  OBJ_DB_TABLE_MAP_ROWID,
  OBJ_DB_TABLE_MAP_CREATED,
  OBJ_DB_TABLE_MAP_MODIFIED,
  OBJ_DB_TABLE_MAP_NAME,
  OBJ_DB_TABLE_MAP_TAB,
  OBJ_DB_TABLE_MAP_DESCRIPTION,

  OBJ_DB_TABLE_NETWORK_ROWID,
  OBJ_DB_TABLE_NETWORK_CREATED,
  OBJ_DB_TABLE_NETWORK_MODIFIED,
  OBJ_DB_TABLE_NETWORK_ADDRESS,
  OBJ_DB_TABLE_NETWORK_NETMASK,
  OBJ_DB_TABLE_NETWORK_SPEED,
  OBJ_DB_TABLE_NETWORK_NAME,
  OBJ_DB_TABLE_NETWORK_DESCRIPTION,
  OBJ_DB_TABLE_NETWORK_CONTACT,

  OBJ_DB_TABLE_SNMP_ROWID,
  OBJ_DB_TABLE_SNMP_CREATED,
  OBJ_DB_TABLE_SNMP_MODIFIED,
  OBJ_DB_TABLE_SNMP_NAME,
  OBJ_DB_TABLE_SNMP_VERSION,
  OBJ_DB_TABLE_SNMP_PORT,
  OBJ_DB_TABLE_SNMP_TIMEOUT,
  OBJ_DB_TABLE_SNMP_RETRIES,
  OBJ_DB_TABLE_SNMP_READ_C,
  OBJ_DB_TABLE_SNMP_WRITE_C

} GxSNMP_OBJECT_TABLE_Fields;

/* database find filter */

typedef enum {
  OBJ_FIND_MULTI = 1,
  OBJ_FIND_SINGLE
} GxSNMP_OBJECT_FILTER;


typedef struct _gxobject_db_filter  {
  gint method;			/* match method */
  gchar **args;			/* a setup of filters */
} gxobject_db_filter;

typedef struct _gxobject_ctx {
  gint  type;			/* type of table to use (same as name of table)  */
  gint  sock;			/* socket to use if we contact database daemon */
  gxdb_tables tables;		/* pointer to all tables loaded in memory */
  gpointer user;		/* various information held here, filter etc.. */
} gxobject_ctx;


/* This is the gxobject structure */

typedef struct _gxobject {
  gint objnum;			/* unique object number */
  gint type;			/* type of object */
  gpointer ctx;			/* additional context information */
  gpointer obj;			/* object */
} gxobject;


/* CONFIGURATION */

typedef struct {
        gchar base[200];
        gchar tabplug[200];		/* table-plugins directory */
        gchar dbplug[200];		/* database plugins */
        char  ddhost[200];		/* database daemon host */
        gint  ddport;			/* database daemon port */
        gchar dbtype[40];
        gchar dbhost[40];
        gint  dbport;
        gchar dbuser[80];
        gchar dbpass[80];
        
}gxdbconfig;

extern gxdbconfig dbconfig;

#endif
/* EOF */
