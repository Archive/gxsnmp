/* -*- Mode: C -*-
 * $Id$
 *
 * Copyright 1998,1999 comstar.net, inc.
 * Author: Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *

  This is the Client API include file

*/

/*****************************
 *
 * g_sqldb API interface
 *
 *****************************/

extern int g_sqldb_row_add            (int, G_sqldb_table *, gpointer);
extern gboolean g_sqldb_table_load    (int, G_sqldb_table *);
extern gpointer g_sqldb_row_find      (G_sqldb_table *, gchar *, gpointer);
extern gpointer g_sqldb_table_lookup  (gchar *);
extern gboolean g_sqldb_row_add       (int sock, G_sqldb_table * table, gpointer row);

/*****************************
 *
 * object API interface
 *
 *****************************/

extern gxobject * gxobject_db_create  (gint type);
extern gint       gxobject_db_destroy (gxobject *obj);
extern gint       gxobject_db_add     (gxobject *obj);
extern gint       gxobject_db_update  (gxobject *obj);
extern gint       gxobject_db_delete  (gxobject *obj);
extern gpointer   gxobject_db_find    (gxobject *obj);
extern gboolean   gxobject_db_load    (gxobject *obj);

/*****************************
 *
 * low level interface
 *
 *****************************/

extern gchar *    db_ip2name(gint sock, gchar *ip);


/* EOF */
