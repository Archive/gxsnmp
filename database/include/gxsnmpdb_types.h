#ifndef __GXSNMPDB_TYPES_H__
#define __GXSNMPDB_TYPES_H__

typedef enum {
  PDUMEMTYPE_NULL = 1,
  PDUMEMTYPE_INT, 		/*Packet encode: MemberType Integer */
  PDUMEMTYPE_STR		/*Packet encode: MemberType ASCIIZ */
}GxSNMP_PDU_Types;

typedef enum {		/*This is all table-plugins SQL strings it can build */
  SQL_SELECT = 1,		/* Select based on field value */
  SQL_SELECT_PRI,		/* Select a row using primary key */
  SQL_SELECT_ALL,		/* Select all rows */
  SQL_INSERT,			/* Insert a row */
  SQL_UPDATE,			/* Update a row using primary key */
  SQL_DELETE			/* Delete row at primary key */
} GxSNMP_SQL_Types;

typedef enum {		/* Used by API/Daemon in packet to identify action in packet */
  AUTH = 1,
  NACK,
  NOTIFY,
  ROW_ADD,
  ROW_DELETE,
  ROW_UPDATE,
  ROW_READ,
  TABLE_LOAD,
  TABLE_LOAD_FILTERED,	/* Load a table using memberrecords as a filter see section C*/
  RESULT		/* generic result of a database question */
} GxSNMP_APICMD_Types;

typedef enum {		/*Queries that can be used to ask a table plugin about table info*/
  NUM_FIELDS = 1,		/*Number of fields in table, count is from zero*/
  PRI_KEY_FIELD,		/*What is your primary key field number*/
  TABLE_ID,			/*What is your unique table ID */
  FIELD_NAME,			/*return asciiz of field name, needs a numerical pointer to field*/
  TABLE_POINTER			/*given a table identifier(gint) it returns a pointer into db_tables */
} GxSNMP_TABLEINFO_Types;


/* database I/O control values*/

#define DBCTL_RW           0x00000001    /* read_write access, reseted we have read_only access */
#define DBCTL_NOTIFY_BRIEF 0x00000002    /* notify brief          */
#define DBCTL_NOTIFY_FULL  0x00000004    /* notify full           */
#define DBCTL_AUTH         0x00000008    /* if set user is authenticated */

#endif

