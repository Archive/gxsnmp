/*
**  $Id$
**  GXSNMP -- An snmp management application
**  Copyright (C) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  plugin-table.h -- Plugin handling code, shamelessly copied from gnumeric :)
*/

#include <gmodule.h>

/*
**  Control block for keeping track of the various plugins
*/

typedef struct _PluginData
{
  GModule    * handle;		/* Identifies the loaded module */
  gchar      * filename;        /* Filename of plugin module */
  gchar      * name;		/* Plugin-assigned name of plugin */
  gint         type;            /* Plugin-assigned type of plugin */
  gint         refcount;        /* Reference cound for plugin */

/*
 * load_plugin   -- Called when we load the plugin(s).
 *
 * unload_plugin -- Called when we unlocad the plugin.
 *
 * start_plugin  -- Called when we want the plugin to do something.
 *
 */
  int       (* load_plugin)	(struct _PluginData *);
  void      (* unload_plugin)  	(struct _PluginData *);
  void      (* start_plugin) 	(struct _PluginData *);
}
PluginData;

extern GList * plugin_list;

/******************************************************************************
**
**  The API for the plugin interface
**
******************************************************************************/

void		plugins_table_load	(gchar *);
void            plugins_table_unload	(void);
void		plugins_table_start   ();
