/* This is a collection of filter that does the same as
   the previous find functions found in DB_* files

   Include this file to get access to the filters.

*/
extern gxobject_db_filter db_find_by_rowid;
extern gxobject_db_filter db_graph_find_host;
extern gxobject_db_filter db_graph_find_network;
extern gxobject_db_filter db_graph_find_wire;
extern gxobject_db_filter db_host_find_by_name;
extern gxobject_db_filter db_network_find_by_address;
