#ifndef __GXSNMP_DBLIB__
#define __GXSNMP_DBLIB__

#include "tables.h"

#include "gxsnmpdb_types.h"

#include "gxsnmpdb_DB_host.h"
#include "gxsnmpdb_DB_interface.h"
#include "gxsnmpdb_DB_graph.h"
#include "gxsnmpdb_DB_hitem.h"
#include "gxsnmpdb_DB_snmp.h"
#include "gxsnmpdb_DB_network.h"

#include "gxsnmpdb_util.h"

gint dcsock;	/*include it here for now, till we can work it away */

extern gxdb_tables db_tables;

#endif
