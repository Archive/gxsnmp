
typedef struct _profile {      /* holds a connected user profile */
  gint value;			/* connection profiles, eg notify read-write */
  GIOChannel *source;		/* Client IO stream */
} profile;

typedef struct _tranrec {      /* holds a transaction record from the Client API */
  gint action;			/* this is what action to perform on table */
  gint table;			/* table to access */
  gpointer members;		/* pointer to all MemberRecords */
  gint mlen;			/* length of MemberRecords */
  GIOChannel *source;		/* Client IO stream, its also identifies whitch profile to use*/
  profile *prf;
} tranrec;

void add_guint(gint *, void*, guint);
void add_asciiz(gint *, void *, void *);
gint decode_member(gint *, void *, gchar *, gint *);
gint decode_member_asciiz(gint *, void *, gchar *);

