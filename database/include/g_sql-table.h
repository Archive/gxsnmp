/* -*- Mode: C -*-
 *  $Id$
 *
 *  gsql -- A simplified, unified interface to various SQL packages.
 *  Copyright (C) 1999 John Schulien
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  g_sql.h  --  Abstract SQL interface API
 */

#include <glib.h>

/*
**  Structure to define an SQL database engine backend.  This structure is
**  ONLY used by g_sql-table.c and by the database engine backends.
*/

typedef struct _G_sql_table
{
  gchar    *name;					/* Name of the table*/
  gint     type;
  gboolean (* tableinfo)           (guint, gpointer, gpointer);
  gboolean (* encode_sql)          (gpointer, gchar *, gint, gint *, gint);
  gboolean (* decode_sql)          (gpointer *, G_sql_query *, guint, guint);
  gint     (* encode_members)      (gpointer, gpointer);
  gpointer (* decode_members)      (gpointer, gint *, gint);
  gboolean (* add_hash)            (GHashTable *hash, gpointer data);
  gboolean (* remove_hash)         (GHashTable *hash, gpointer data);
  gboolean (* obj_create)          ();
  gboolean (* obj_destroy)         (gpointer);
  gboolean (* obj_add)             (gpointer);
  gboolean (* obj_update)          (gpointer);
  gboolean (* obj_delete)          (gpointer);
  gboolean (* obj_match)           (gpointer);
  gboolean (* obj_load )           (gpointer);
}
G_sql_table; 

/***************************************************************************
 *  Functions available in the API.
 **/
gboolean           g_sql_register_table    	(G_sql_table     *dbt);
gboolean           g_sql_unregister_table  	(G_sql_table     *dbt);
G_sql_table        *g_sql_table_lookup		(gchar         	 *tablename);
gboolean           g_sql_encode_sql 	  	(G_sql_table     *dbt, 
						 gpointer        packet, 
						 gchar           *sqlstr, 
						 gint            querytype, 
						 gint            *n, 
						 gint            tpl);
gboolean           g_sql_decode_sql          	(G_sql_table     *dbt, 
						 G_sql_query     *sql, 
						 gpointer        packet, 
						 guint           numfields, 
						 guint           *n);
gpointer           g_sql_decode_members         (G_sql_table     *dbt,
						 gpointer        packet, 
						 gint            *n, 
						 gint            plen);
gint               g_sql_encode_members         (G_sql_table     *dbt, 
						 gpointer        packet, 
						 gpointer        data);
gpointer           g_tableinfo                  (G_sql_table     *dbt,
						 guint           query,
						 gpointer        answer);
gboolean           g_sql_add_hash               (G_sql_table     *dbt, 
						 GHashTable      *hash, 
						 gpointer        data);
gboolean           g_sql_remove_hash            (G_sql_table     *dbt, 
						 GHashTable      *hash, 
						 gpointer        data);



