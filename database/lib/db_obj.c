/*  $Id$
 *
 *  db_obj.c -- Copyright 1999 Larry Liimatainen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  db_obj.c
 *
 */


#include <stdio.h>
#include <glib.h>
#include "tables.h"
#include "gxsnmpdb_types.h"
#include "g_sql-table.h"

/* gxobject_db_create()
 *
 * create a object
 *
 *
 */

gxobject *
gxobject_db_create(gint type)
{
G_sql_table *dbh;
gxobject_ctx *ctx;

  dbh = db_table_lookup(type);
  return dbh->obj_create(type);
}

/* gxobject_db_destroy()
 *
 * destroy a object
 * 
 */

gint
gxobject_db_destroy(gxobject *obj)
{
G_sql_table *dbh;
gxobject_ctx *ctx;

  ctx = obj->ctx;
  dbh = db_table_lookup(ctx->type);
  return dbh->obj_destroy(obj);

}

/* gxobject_db_add()
 *
 * Add a object to a database
 *
 *
 */

gint
gxobject_db_add(gxobject *obj)
{
G_sql_table *dbh;
gxobject_ctx *ctx;

  g_assert(obj);
  g_assert(obj->ctx);

  ctx = obj->ctx;
  dbh = db_table_lookup(ctx->type);

fprintf(stderr,"plugin type: %d\n", ctx->type);
fprintf(stderr,"db handler is : %p\n", dbh);

  dbh->obj_add(obj);
}

/* gxobject_db_update()
 *
 * update a object in database
 *
 */

gint 
gxobject_db_update(gxobject *obj)
{
G_sql_table *dbh;
gxobject_ctx *ctx;

  g_assert(obj);

  ctx = obj->ctx;
  if(!(dbh = db_table_lookup(ctx->type))){
    fprintf(stderr,"gxobject_db_update(): No table plugin found!\n");
    return 0;
  }

  fprintf(stderr,"obj_update_call\n");
  dbh->obj_update(obj);
  fprintf(stderr,"obj_update_done\n");
}

/* gxobject_db_delete()
 *
 * delete object in database
 *
 */

gint 
gxobject_db_delete(gxobject *obj)
{
G_sql_table *dbh;
gxobject_ctx *ctx;

  g_assert(obj);

  ctx = obj->ctx;
  if(!(dbh = db_table_lookup(ctx->type))){
    fprintf(stderr,"gxobject_db_delete(): No table plugin found!\n");
    return 0;
  }

fprintf(stderr,"obj_update_call\n");
  dbh->obj_delete(obj);
fprintf(stderr,"obj_update_done\n");
}

/* gxobject_db_find()
 *
 * searches a database table for a object
 *
 * returns a GList if user has specified OBJ_DB_FIND_MULTI
 * else it returns the row matching.
 * On failure return NULL.
 *
 */
gpointer
gxobject_db_find(gxobject *obj)
{
gxobject_ctx *ctx;
gxobject_db_filter *filter;
G_sql_table *dbh;
G_sqldb_table *table;
GList *gl;
gpointer row;
GList *ret = NULL;

  ctx = obj->ctx;
  filter = ctx->user;
  dbh = db_table_lookup(ctx->type);			/* get table plugin to use*/
  dbh->tableinfo(TABLE_POINTER, 0, &table);		/* get pointer to G_sqldb_table this object uses*/

  gl = g_sqldb_table_list(table);			/* get a list of all rows in this table */
  while(gl)
    {
      obj->obj = gl->data;
      if(dbh->obj_match(obj))				/* for each row call plugin to see if we have a match against the objects row */
        {
          fprintf(stderr,"match!\n");
          if(filter->method == OBJ_FIND_MULTI)		/* append info to a glist. */
            ret = g_list_append(ret, obj->obj);
          else return obj->obj;				/* else user just wanted the matching row in answer */
        }
      gl = gl->next;
    }
  return ret;
}

/* gxobject_db_load() - loads a table into memory
 *
 * User can specify a filter.
 *
 */
gboolean
gxobject_db_load(gxobject *obj)
{
G_sql_table *dbh;
gxobject_ctx *ctx;

  g_assert(obj->ctx);

  ctx = obj->ctx;
  if(!(dbh = db_table_lookup(ctx->type))){
    fprintf(stderr,"gxobject_db_update(): No table plugin found!\n");
    return 0;
  }
  if(!(dbh->obj_load(obj))){
    return 0;
  }
  return 1;
}

/* EOF */
