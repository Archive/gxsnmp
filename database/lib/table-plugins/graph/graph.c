/* -*- Mode: C -*-
 *  gsql -- A simplified, unified interface to various SQL packages.
 *  Copyright (C) 1999 John Schulien
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  interface.c -- the database interface table support plugin
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "debug.h"

#include "gxsnmpdb_types.h"
#include "plugin.h"

#include "plugins-table.h"
#include "g_sql.h"
#include "g_sql-table.h"
#include "tables.h"

extern gxdb_tables db_tables;
extern void debug(); /*this is for gdb*/

#define GRAPH_NUM_FIELDS 12		/*TAGS is unused */


/****************************************************************************
 *  Forward references
 */
static gboolean           tableinfo           (gint query, gpointer indata, void **outdata);
static gboolean           encode_sql          (gpointer, 
					       gchar *, 
					       gint, 
					       gint *, 
					       gint);
static gboolean           decode_sql          (gpointer, 
					       G_sql_query *, 
					       gint, 
					       gint *n);
static gint               encode_members      (gpointer, 
					       gpointer);
static gpointer           decode_members      (gpointer, 
					       gint *, 
					       gint);
static gboolean           add_hash            (GHashTable *hash, 
					       gpointer);
static gboolean           remove_hash         (GHashTable *hash, 
					       gpointer);
static gxobject         * obj_create          ();
static void               obj_destroy         (gxobject *obj);
static void               obj_add             (gxobject *obj);
static void               obj_update          (gxobject *obj);
static void               obj_delete          (gxobject *obj);
static gboolean           obj_match           (gxobject *obj);
static gboolean           obj_load            (gxobject *obj);


/*
**  The database backend control block for the host table engine
*/

static struct _G_sql_table backend =
{
  "graph",			/* Name of the table */
  OBJ_DB_GRAPH,
  tableinfo,
  encode_sql,      
  decode_sql,		
  encode_members,
  decode_members,
  add_hash,
  remove_hash,
  obj_create,
  obj_destroy,
  obj_add,
  obj_update,
  obj_delete,
  obj_match,
  obj_load
};

/**
 * tableinfo()
 *
 * Arguments:
 * query - is what info user requests.
 * indata is input for query
 * outdata carries answer
 */
gboolean
tableinfo(gint query, gpointer indata, void **outdata)
{
  gint i = GRAPH_NUM_FIELDS;

  fprintf(stderr,"tableinfo()\n");
  switch(query)
    {
    case NUM_FIELDS:	   			/* number of fields available, zero is invalid*/
      *((gint *) outdata) = i;
      return TRUE;
    case TABLE_POINTER:				/* pointer to this table */
      fprintf(stderr,"address to outdata is: %p\n", *outdata);
      *outdata = graph_sqldb;
      return TRUE;
    default:    				/*user has asked a question I don't know */
      return FALSE;
    }
}


/*****************************************************************************
**
** encode_sql()
**
** Build a SQL query out of member records found in the packet.
** 
** Arguments:
** packet    is a pointer to beginning of the packet
** sqlstr    is a SQL string we are building
** querytype is what type of SQL string to build
** *n        is how far into the packet we have decoded
** tpl       is total packet length, so we know when to stop decoding Members
**
** Returns:
** TRUE  --  If the operation succeeded
** FALSE --  If the operation failed
**
*****************************************************************************/

static gboolean 
encode_sql(gpointer packet, gchar *sqlstr, gint querytype, gint *n, gint tpl)
{
static gchar mds[1000];
static gchar mds2[1000];
static gint a,b,mdi, mdi2, i, rowid;

 switch(querytype){
    case SQL_INSERT: /*this method should support all tables*/
      sprintf(sqlstr, "INSERT INTO %s VALUES(", backend.name);
      while(*n < tpl){
        switch(get_member(n, packet, mds, &mdi)){
          case PDUMEMTYPE_INT:
            sprintf(mds2, "%d,", mdi);
            strcat(sqlstr, mds2);
            break;
          case PDUMEMTYPE_STR:
            sprintf(mds2, "'%s',", mds);
            strcat(sqlstr, mds2);
            break;
          default:
            fprintf(stderr,"FATAL: APP_ERR generic packet decode error, at packetpointer %d.\n", *n);
            return FALSE;
        }
      }
      i = strlen(sqlstr);
      *((char *)sqlstr+(--i)) = 0;              /*remove last comma*/
      strcat(sqlstr, ")");
    break;
    case SQL_UPDATE:
      sprintf(sqlstr, "UPDATE %s SET ", backend.name);

      get_member(n, packet, mds, &mdi);	/* build ROWID */
      rowid = mdi;

      get_member(n, packet, mds, &mdi);     /* build CREATED */
      sprintf(mds2, "created = '%s', ", mds);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build MODIFIED */
      sprintf(mds2, "modified = '%s', ", mds);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build MAP */
      sprintf(mds2, "map = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build TYPE */
      sprintf(mds2, "type = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build HOST */
      sprintf(mds2, "host = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build NETWORK */
      sprintf(mds2, "network = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);         /* build DETAILS */
      sprintf(mds2, "details = '%s', ", mds);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);         /* build X */
      sprintf(mds2, "x = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);         /* build Y */
      sprintf(mds2, "y = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);         /* build PIXMAP*/
      sprintf(mds2, "pixmap = '%s' ", mds);
      strcat(sqlstr, mds2);
      get_member(n, packet, mds, &mdi);         /* build TAGS */
#if 0
      sprintf(mds2, "tags = '%s', ", mds); /* unused tags */
      strcat(sqlstr, mds2);
#endif
      sprintf(mds2, " WHERE _rowid = %d ", rowid);
      strcat(sqlstr, mds2);

    break;
    case SQL_DELETE:
      get_member(n, packet, mds, &mdi);	/* fetch ROWID */
      sprintf(sqlstr, "DELETE FROM %s WHERE _ROWID = %d", backend.name, mdi);
    break;
    case SQL_SELECT_PRI:
      get_member(n, packet, mds, &mdi);	/* fetch ROWID */
      sprintf(sqlstr, "SELECT * FROM %s WHERE _ROWID = %d", backend.name, mdi);
    break;
    case SQL_SELECT_ALL:
      sprintf(sqlstr, "SELECT * FROM %s", backend.name);
    break;
    case SQL_SELECT:
      sprintf(sqlstr, "SELECT * FROM %s WHERE ", backend.name);
      while(*n < tpl){
        a = get_member(n, packet, mds, &mdi);           /* get the field identifier */
        b = get_member(n, packet, mds2, &mdi2);         /* get the field value */
        switch(a){
          case PDUMEMTYPE_INT:
          case -1:
            break;
          break;
          default:
            fprintf(stderr,"Error in packet.\n");
            return FALSE;
        }
        switch(mdi){                                    /* check out what field it identifies */
          case OBJ_DB_TABLE_GRAPH_ROWID:            /* rest is table field identifier */
            strcat(sqlstr, "_rowid = ");
            break;
          case OBJ_DB_TABLE_GRAPH_CREATED:
            strcat(sqlstr, "created = ");
            break;
          case OBJ_DB_TABLE_GRAPH_MODIFIED:
            strcat(sqlstr, "modified = ");
            break;
          case OBJ_DB_TABLE_GRAPH_MAP:
            strcat(sqlstr, "map = ");
            break;
          case OBJ_DB_TABLE_GRAPH_TYPE:
            strcat(sqlstr, "type = ");
            break;
          case OBJ_DB_TABLE_GRAPH_HOST:
            strcat(sqlstr, "host = ");
            break;
          case OBJ_DB_TABLE_GRAPH_NETWORK:
            strcat(sqlstr, "network = ");
            break;
          case OBJ_DB_TABLE_GRAPH_DETAILS:
            strcat(sqlstr, "details = ");
            break;
          case OBJ_DB_TABLE_GRAPH_X:
            strcat(sqlstr, "x = ");
            break;
          case OBJ_DB_TABLE_GRAPH_Y:
            strcat(sqlstr, "y = ");
            break;
          case OBJ_DB_TABLE_GRAPH_PIXMAP:
            strcat(sqlstr, "pixmap = ");
            break;

          default:
            fprintf(stderr,"FATAL: APP_ERR generic packet decode error, at packetpointer %d.\n", *n);
            return FALSE;
        }
        switch(b){
          case PDUMEMTYPE_INT:                          /* its the value of the field */
            sprintf(mds, "%d AND ", mdi2);
            strcat(sqlstr, mds);
            break;
          case PDUMEMTYPE_STR:                          /* its the value of the field */
            sprintf(mds, "'%s' AND ", mds2);
            strcat(sqlstr, mds);
            break;
          case -1:
            break;
          default:
            fprintf(stderr,"Error in packet.\n");
        }
      } /*end of while decoding member records */
      i = strlen(sqlstr);
      *((char *)sqlstr+i-5) = 0;              /*remove last ' AND '*/
    break;
    default:
      fprintf(stderr,"FATAL: APP_ERR packet decode error, undefined SQL command\n"); /*try give some output even it may be worthless*/
      return FALSE;
  }
  /* SQL string is built, it is now ready to send to SQL database */
  fprintf(stderr,"SQL STRING: %s\n", (char *) sqlstr);
  return TRUE;
}

/*****************************************************************************
**
**  decode_sql()
**
** takes a SQL result and builds membersRecords. 
**
** arguments:
** packet, here we put resulting memberrecords
** sql,    this is the SQL result to use.
** numfields, is how many fields per row to expect
** *n,     is packet offset
** tpl,    total packet length.
** 
**
** returns:
**
**  TRUE  --  If the operation succeeded
**  FALSE --  If the operation failed
**
*****************************************************************************/

static gboolean
decode_sql(gpointer packet, G_sql_query *sql, gint numfields, gint *n)
{
gchar *data;
gchar nulstr[] = "";
gint nulint = 0;

  if(!(data = g_sql_field_pos(sql, 0))) data = &nulint;   /*rowid */
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 1))) data = nulstr;
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 2))) data = nulstr;   /*modified */
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 3))) data = &nulint;
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 4))) data = &nulint;  /*type */
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 5))) data = &nulint;
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 6))) data = &nulint; /*network */
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 7))) data = nulstr;   /*details*/
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 8))) data = &nulint; /*x */
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 9))) data = &nulint; /*y*/
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 10))) data = nulstr;   /*pixmap*/
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 11))) data = nulstr;   /*tags*/
  add_asciiz(n, packet, data);

  return TRUE;

}

/* encode_members()
 *
 * This function takes a DB_host table and makes
 * memberrecords of all structure members.
 * remember, to compile a valid packet to send
 * to database daemon, one must create a header
 * and after calling this function, set total
 * packet length.
 *
 * Arguments:
 * packet     This is destination of all members created
 * data       data this is a pointer to a DB_host table
 * 
 * Returns:
 * how much data that has been encoded
 *
 */

static gint 
encode_members(gpointer packet, gpointer data)
{
  gint n=0;

  add_guint (&n, packet, ((DB_graph *)data)->rowid);
  add_asciiz(&n, packet, ((DB_graph *)data)->created);
  add_asciiz(&n, packet, ((DB_graph *)data)->modified);
  add_guint (&n, packet, ((DB_graph *)data)->map);
  add_guint (&n, packet, ((DB_graph *)data)->type);
  add_guint (&n, packet, ((DB_graph *)data)->host);
  add_guint (&n, packet, ((DB_graph *)data)->network);
  add_asciiz(&n, packet, ((DB_graph *)data)->details);
  add_guint (&n, packet, ((DB_graph *)data)->x);
  add_guint (&n, packet, ((DB_graph *)data)->y);
  add_asciiz(&n, packet, ((DB_graph *)data)->pixmap);
  add_asciiz(&n, packet, "abc");

  return n;
}

static gpointer
decode_members(gpointer packet, gint *n, gint plen)
{
  static gchar member[2049];
  static guint memint;
  static gpointer table;
  
  if(*n >= plen) return 0;
  if(!(table = g_new0(DB_graph, 1)))
    {
      fprintf(stderr,"ERR: couldn't alloc()\n");
      return 0;
    }
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->rowid = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->created = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->modified = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->map = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->type = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->host = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->network = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->details = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->x = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->y = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_graph *) table)->pixmap = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint); /*decode tags, even though we dont use it*/

  fprintf(stderr,"done reading a row from members\n");

  return table;
}

/* add_hash()
 *
 * this functions will add data 
 * to an hash table, where it is nessecery not to
 * have a duplicate of the primary key in host_table.
 *
 *
 */
static gboolean 
add_hash(GHashTable *hash, gpointer data)
{
  
  if (g_hash_table_lookup (hash, ((DB_graph *) data)->rowid))
    {
      /*value of primary key was found in hash table, so its
        illegal to add.
      */
      return FALSE;
    }
  g_hash_table_insert (hash, ((DB_graph *) data)->rowid, data);

  return TRUE;
}

/* remove_hash()
 *
 * this functions will remove data 
 * from an hash table, where it is nessecery not to
 * have a duplicate of the primary key in host_table.
 *
 *
 */
static gboolean 
remove_hash (GHashTable *hash, gpointer data)
{
  
  if (g_hash_table_lookup (hash, ((DB_graph *) data)->rowid))
    {
      /*value of primary key was not found in hash table.*/
      return FALSE;
    }
  g_hash_table_remove (hash, ((DB_graph *) data)->rowid);
  
  return TRUE;
}


/* Object oriented section */

static gxobject *
obj_create()
{
DB_graph *dbg;
gxobject_ctx *ctx;
gxobject *obj;

  dbg = g_new0 (DB_graph, 1);
  ctx = g_new0 (gxobject_ctx,1);
  obj = g_new0 (gxobject,1);

  ctx->tables = db_tables;
  obj->objnum = 0;
  obj->type = OBJ_DB_GRAPH;
  obj->ctx = ctx;
  obj->obj = dbg;

  if (dbg)
    {
      dbg->created  = g_strdup ("(not in database)");
      dbg->modified = g_strdup (dbg->created);
      return obj;
    }
  g_warning ("graph_create memory allocation failed, expect trouble.");
  return NULL;
}

static void
obj_destroy(gxobject *obj)
{
DB_graph *dbg;

  dbg = obj->obj;

  if (dbg)
    {
      g_assert (dbg->rowid == 0);
      if (dbg->created)  g_free (dbg->created);
      if (dbg->modified) g_free (dbg->modified);
      if (dbg->details)  g_free (dbg->details);
      if (dbg->pixmap)   g_free (dbg->pixmap);
      g_free (dbg);
      dbg = NULL;
    }
  g_free (obj->ctx);
  g_free (obj);
}

static void
obj_add(gxobject *obj)
{
DB_graph *dbg;
gxobject_ctx *ctx;
static gchar nulstr[] = "";

  ctx = obj->ctx;
  dbg = obj->obj;


  g_return_if_fail (dbg != NULL);
  g_assert (dbg->rowid == 0);
  g_assert (dbg->DB_map == NULL);
  g_assert (dbg->DB_host == NULL);
  g_assert (dbg->DB_network == NULL);

  switch (dbg->type)                    /* do some application test */
    {
      case DB_GRAPH_HOST:
        g_assert (dbg->host != 0);
        g_assert (dbg->network == 0);
        break;
      case DB_GRAPH_NETWORK:
        g_assert (dbg->host == 0);
        g_assert (dbg->network != 0);
        break;
      case DB_GRAPH_WIRE:
        g_assert (dbg->host != 0);
        g_assert (dbg->network != 0);
        break;
      default:
        g_assert_not_reached();
    }
/* ok, for application, this row is OK, now we must check so
   the row is ok for functionality, ie pointers can't be zero */

  if(!dbg->details) dbg->details = &nulstr;
  if(!dbg->pixmap) dbg->pixmap = &nulstr;
  fprintf(stderr,"pixmap: '%s'\n", dbg->pixmap);

  g_assert (dbg->map != 0);             /* Must be assigned to a map */

  dbg->DB_map = g_sqldb_row_find (ctx->tables.map_sqldb, "_rowid", &dbg->map);
  dbg->DB_map->DB_graphs = g_list_append (dbg->DB_map->DB_graphs, dbg);

  if (dbg->host)
    {
      dbg->DB_host =
                g_sqldb_row_find (ctx->tables.host_sqldb, "_rowid", &dbg->host);
      dbg->DB_host->DB_graphs =
                g_list_append (dbg->DB_host->DB_graphs, dbg);
    }

  if (dbg->network)
    {
      dbg->DB_network =
                g_sqldb_row_find (ctx->tables.network_sqldb, "_rowid", &dbg->network);
      dbg->DB_network->DB_graphs =
                g_list_append (dbg->DB_network->DB_graphs, dbg);
    }

  if (dbg->created)
    g_free (dbg->created);
  dbg->created  = db_timestamp ();

  if (dbg->modified)
    g_free (dbg->modified);
  dbg->modified = g_strdup (dbg->created);

  dbg->rowid = g_sqldb_highest_rowid (ctx->tables.graph_sqldb, "_rowid") + 1;
  g_sqldb_row_add (ctx->sock, ctx->tables.graph_sqldb, dbg);
}

static void
obj_update(gxobject *obj)
{
gxobject_ctx *ctx;
DB_graph *row;

  ctx = obj->ctx;
  row = obj->obj;

  g_return_if_fail (row != NULL);
  g_assert (row->rowid != 0);
  switch (row->type)
    {
      case DB_GRAPH_HOST:
        g_assert (row->host != 0);
        g_assert (row->network == 0);
        break;
      case DB_GRAPH_NETWORK:
        g_assert (row->host == 0);
        g_assert (row->network != 0);
        break;
      case DB_GRAPH_WIRE:
        g_assert (row->host != 0);
        g_assert (row->network != 0);
        break;
      default:
        g_assert_not_reached();
    }
  g_assert (row->map != 0);             /* Must be assigned to a map */

/* Functionality test. Test that the object
   is not causing trouble.
*/

/* Application test. Check that the object
   is sane from application view.
*/

/* end of test */
  

  if (row->DB_map)                              /* If previously assigned */
    if (row->DB_map->rowid != row->map)         /* to a different map ... */
      {                                         /* then unassign it */
        row->DB_map->DB_graphs =
                        g_list_remove (row->DB_map->DB_graphs, row);
        row->DB_map = NULL;
      }

  if (!row->DB_map)            /* Assign to a new map if necessary */
    {
      row->DB_map = g_sqldb_row_find (ctx->tables.map_sqldb, "_rowid", &row->map);
      row->DB_map->DB_graphs = g_list_append (row->DB_map->DB_graphs, row);
    }

  if (row->DB_host)                             /* If previously attached to */
    if (row->DB_host->rowid != row->host)       /* a different host object */
      {                                         /* then unassign it */
        row->DB_host->DB_graphs = g_list_remove (row->DB_host->DB_graphs, row);
        row->DB_host = NULL;
      }

  if (row->host)                /* Assign graph object to a host? */
    if (!row->DB_host)          /* Yes -- add to host GList if necessary */
      {
        row->DB_host = g_sqldb_row_find (ctx->tables.host_sqldb, "_rowid", &row->host);
        row->DB_host->DB_graphs = g_list_append (row->DB_host->DB_graphs, row);
      }

  if (row->DB_network)                          /* If previously attached to */
    if (row->DB_network->rowid != row->network) /* a different network object */
      {                                         /* then unassign it */
        row->DB_network->DB_graphs = 
                g_list_remove (row->DB_network->DB_graphs, row);
        row->DB_network = NULL;
      }

  if (row->network)             /* Assign graph object to a network? */
    if (!row->DB_network)       /* Yes -- add to network GList if necessary */
      {
        row->DB_network = 
                g_sqldb_row_find (ctx->tables.network_sqldb, "_rowid", &row->network);
        row->DB_network->DB_graphs = 
                g_list_append (row->DB_network->DB_graphs, row);
      }

  if (row->modified) 
    g_free (row->modified);
  row->modified = db_timestamp ();

  g_sqldb_row_update (ctx->sock, ctx->tables.graph_sqldb, row);
}

static void
obj_delete(gxobject *obj)
{
gxobject_ctx *ctx;
DB_graph *dbg;

  ctx = obj->ctx;
  dbg = obj->obj;

  g_assert (dbg->rowid != 0);
  g_return_if_fail (dbg != NULL);
  if (dbg->DB_map)
    {
      dbg->DB_map->DB_graphs =  g_list_remove (dbg->DB_map->DB_graphs, dbg);
    }
  dbg->DB_map = NULL;
  if (dbg->DB_host)
    {
      dbg->DB_host->DB_graphs = g_list_remove (dbg->DB_host->DB_graphs, dbg);
    }
  dbg->DB_host = NULL;

  if (dbg->DB_network)
    {
      dbg->DB_network->DB_graphs = g_list_remove (dbg->DB_network->DB_graphs, 
                                                  dbg);
    }
  dbg->DB_network = NULL;

  g_assert (g_sqldb_row_delete (ctx->sock, ctx->tables.graph_sqldb, dbg) == TRUE);

  dbg->rowid = 0;
}

static gboolean
obj_match(gxobject *obj)
{
gxobject_db_filter *filter;
gxobject_ctx *ctx;
guint n=0;
guint t=1;
DB_graph *row;
void *a;
char *b;

  ctx = obj->ctx;
  filter = ctx->user;
  row = obj->obj;

  for(;filter->args[n];n++,n++)
    {
      a = filter->args[n];				/*get field name */
      b = filter->args[n+1];				/* pointer to field data */
fprintf(stderr,"Checking filter pair type: %d\n", *((gint *)a));
fprintf(stderr,"   filter value (int or pointer): %d\n", *((gint *)b));
      switch(*((gint *)a))						/* test type of field name */
        {
          case OBJ_DB_TABLE_GRAPH_ROWID:
fprintf(stderr,"   matching ROWID\n");
            if(*((gint *)b) != row->rowid) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_CREATED:
fprintf(stderr,"   matching CREATED\n");
            if(strcmp(b, row->created)) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_MODIFIED:
fprintf(stderr,"   matching MODIFIED\n");
            if(strcmp(b, row->modified)) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_MAP:
fprintf(stderr,"   matching MAP\n");
            if(*((gint *)b) != row->map) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_TYPE:
fprintf(stderr,"   matching TYPE\n");
            if(*((gint *)b) != row->type) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_HOST:
fprintf(stderr,"   matching HOST\n");
            if(*((gint *)b) != row->host) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_NETWORK:
fprintf(stderr,"   matching NETWORK\n");
            if(*((gint *)b) != row->network) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_DETAILS:
fprintf(stderr,"   matching DETAILS\n");
            if(strcmp(b, row->details)) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_X:
fprintf(stderr,"   matching X\n");
            if(*((gint *)b) != row->x) t = 0;
          break;
          case OBJ_DB_TABLE_GRAPH_Y:
fprintf(stderr,"   matching Y\n");
            if(*((gint *)b) != row->y) t = 0;
          break;
        }
    }

  if(t) return TRUE;
  return FALSE;
}


/* obj_load() - loads a database table into memory
 *
 * This can load a database filter using a filter
 *
 * It will return result in a glist of rows in obj->obj
 *
 * If unsuccessful or a empty table it returns NULL
 *
 */

static gboolean
obj_load(gxobject *obj)
{
gxobject_db_filter *filter;
gxobject_ctx       *ctx;
guint              n=0;
DB_interface       *row;
void               *a;
char               *b;

gchar              *query;            /* Text of SQL query */
gint               plen,i,h;
gint               rlen;
gchar packet[20000];
static GList              *gl = NULL;
gpointer tableres;


  ctx = obj->ctx;
  filter = ctx->user;

  i = set_pdu_header(packet, TABLE_LOAD_FILTERED, OBJ_DB_GRAPH);

  for(;filter->args[n];n++,n++)
    {
fprintf(stderr,"loading data from filter, at position %d\n", n);
      a = filter->args[n];                              /*get field name */
      b = filter->args[n+1];                            /* pointer to field data */
fprintf(stderr,"Checking filter pair type: %d\n", *((gint *)a));
fprintf(stderr,"   filter value (int or pointer): %d\n", *((gint *)b));
      switch(*((gint *)a))                              /* test type of field name */
        {
          case OBJ_DB_TABLE_GRAPH_ROWID:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_ROWID);
            add_guint(&i, packet, *((gint *)b));
          break;
          case OBJ_DB_TABLE_GRAPH_CREATED:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_CREATED);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_MODIFIED:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_MODIFIED);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_MAP:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_MAP);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_TYPE:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_TYPE);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_HOST:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_HOST);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_NETWORK:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_NETWORK);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_DETAILS:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_DETAILS);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_X:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_X);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_Y:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_Y);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_GRAPH_PIXMAP:
            add_guint(&i, packet, OBJ_DB_TABLE_GRAPH_PIXMAP);
            add_asciiz(&i, packet, b);
          break;
        }
    }

  memcpy(packet+sizeof(gint), &i, sizeof(gint));                /*write total packet length*/
  dd_write(ctx->sock, i, &packet);                              /*send packet to database daemon*/

  if((n = dd_read_header(ctx->sock, packet)) == -1){
        fprintf(stderr,"  dd_read_header() error\n");
         return -1;
  }

  memcpy(&plen,packet+sizeof(gint), sizeof(guint));                     /*get packet length*/
  if(plen <= 8) return FALSE;                                           /*table is empty*/
  if((query = dd_read_rest(ctx->sock, plen, packet)) == -1) {
    fprintf(stderr,"  dd_read_rest() error\n");
    return -1;
  }
  /*ADD: cmp rlen to plen  ie what length packet says it has and what length readed*/

  n = sizeof(gint) * 2;                                                 /* header size */

  while((tableres = decode_members(query, &n, plen))){       /*we have the first row in an unknown table in tableres*/
    gl = g_list_append(gl, tableres);
  }
  obj->obj = gl;
  return;
}


/* 
 *  service functions needed by all table plugins
 *
 *
 */

/******************************************************************************
**
**  Subroutine to load the plugin.  Set the plugin type to PLUGIN_DATABASE.
**
******************************************************************************/

int 
load_plugin (PluginData * pd)
{
  D_FUNC_START;
/* enable if we merge into ordinary plugin utility
  pd->type = PLUGIN_TABLES;
*/
  pd->name = g_strdup ("interface table plugin");
  D_FUNC_END;
  return 0;
}

/******************************************************************************
**
** Subroutine to unload the plugin
**
******************************************************************************/

void
unload_plugin (PluginData * pd)
{
  D_FUNC_START;
  g_sql_unregister_table(&backend);
  D_FUNC_END;
}

/******************************************************************************
**
**  Subroutine to start the plugin
**
******************************************************************************/

void
start_plugin (PluginData * pd)
{

  fprintf(stderr,"registering host table plugin\n");

  if(g_sql_register_table(&backend)){ /*must have a test, but this function is a void so it doesn't matter now*/
    fprintf(stderr,"start_plugin(): successfully registered plugin!\n");
    return;
  }
  fprintf(stderr,"start_plugin(): couldn't register plugin..\n");
}

/* EOF */

