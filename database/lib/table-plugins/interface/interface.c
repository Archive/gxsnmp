/* -*- Mode: C -*-
 *  gsql -- A simplified, unified interface to various SQL packages.
 *  Copyright (C) 1999 John Schulien
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  interface.c -- the database interface table support plugin
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "debug.h"

#include "gxsnmpdb_types.h"
#include "plugin.h"

#include "plugins-table.h"
#include "g_sql.h"
#include "g_sql-table.h"
#include "tables.h"

extern gxdb_tables db_tables;
extern void debug(); /*this is for gdb*/

#define INTERFACE_NUM_FIELDS 9 		/*TAGS is unused */


/****************************************************************************
 *  Forward references
 */
static gboolean           tableinfo           (gint query, gpointer indata, void **outdata);
static gboolean           encode_sql          (gpointer, 
					       gchar *, 
					       gint, 
					       gint *, 
					       gint);
static gboolean           decode_sql          (gpointer, 
					       G_sql_query *, 
					       gint, 
					       gint *n);
static gint               encode_members      (gpointer, 
					       gpointer);
static gpointer           decode_members      (gpointer, 
					       gint *, 
					       gint);
static gboolean           add_hash            (GHashTable *hash, 
					       gpointer);
static gboolean           remove_hash         (GHashTable *hash, 
					       gpointer);
static gxobject         * obj_create          ();
static void               obj_destroy         (gxobject *obj);
static void               obj_add             (gxobject *obj);
static void               obj_update          (gxobject *obj);
static void               obj_delete          (gxobject *obj);
static gboolean           obj_match           (gxobject *obj);
static gboolean           obj_load            (gxobject *obj);



/*
**  The database backend control block for the host table engine
*/

static struct _G_sql_table backend =
{
  "interface",			/* Name of the table */
  OBJ_DB_INTERFACE,
  tableinfo,
  encode_sql,      
  decode_sql,		
  encode_members,
  decode_members,
  add_hash,
  remove_hash,
  obj_create,
  obj_destroy,
  obj_add,
  obj_update,
  obj_delete,
  obj_match,
  obj_load
};

/**
 * tableinfo:
 *
 * Arguments:
 * query - is what info user requests.
 * gpointer is a piece of memory, minimum 64 bytes
 * carrying answer.
 */
gboolean
tableinfo(gint query, gpointer indata, void **outdata)
{
  gint i = INTERFACE_NUM_FIELDS;

  switch(query)
    {
    case NUM_FIELDS:                            /* number of fields available, zero is invalid*/
      *((gint *) outdata) = i;
      return TRUE;
    case TABLE_POINTER:                         /* pointer to this table */
      fprintf(stderr,"address to outdata is: %p\n", *outdata);
      *outdata = db_tables.interface_sqldb;
      return TRUE;
    default:                                    /*user has asked a question I don't know */
      return FALSE;
    }
}



/*****************************************************************************
**
** encode_sql()
**
** Build a SQL query out of member records found in the packet.
** 
** Arguments:
** packet    is a pointer to beginning of the packet
** sqlstr    is a SQL string we are building
** querytype is what type of SQL string to build
** *n        is how far into the packet we have decoded
** tpl       is total packet length, so we know when to stop decoding Members
**
** Returns:
** TRUE  --  If the operation succeeded
** FALSE --  If the operation failed
**
*****************************************************************************/

static gboolean 
encode_sql(gpointer packet, gchar *sqlstr, gint querytype, gint *n, gint tpl)
{
static gchar mds[1000];
static gchar mds2[1000];
static gint a,b,mdi,mdi2,i, rowid;

 switch(querytype){
    case SQL_INSERT: /*this method should support all tables*/
      sprintf(sqlstr, "INSERT INTO %s VALUES(", backend.name);
      while(*n < tpl){
        switch(get_member(n, packet, mds, &mdi)){
          case PDUMEMTYPE_INT:
            sprintf(mds2, "%d,", mdi);
            strcat(sqlstr, mds2);
            break;
          case PDUMEMTYPE_STR:
            sprintf(mds2, "'%s',", mds);
            strcat(sqlstr, mds2);
            break;
          default:
            fprintf(stderr,"FATAL: APP_ERR generic packet decode error, at packetpointer %d.\n", *n); /*try give some output even it may be worthless*/
            return FALSE;
        }
      }
      i = strlen(sqlstr);
      *((char *)sqlstr+(--i)) = 0;              /*remove last comma*/
      strcat(sqlstr, ")");
    break;
    case SQL_UPDATE:
      sprintf(sqlstr, "UPDATE %s SET ", backend.name);

      get_member(n, packet, mds, &mdi);	/* build ROWID */
      rowid = mdi;

      get_member(n, packet, mds, &mdi);     /* build CREATED */
      sprintf(mds2, "created = '%s', ", mds);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build MODIFIED */
      sprintf(mds2, "modified = '%s', ", mds);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build HOST */
      sprintf(mds2, "host = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build SNMP */
      sprintf(mds2, "snmp = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build TRANSPORT */
      sprintf(mds2, "transport = %d, ", mdi);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build ADDRESS */
      sprintf(mds2, "address = '%s', ", mds);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build NETMASK */
      sprintf(mds2, "netmask = '%s', ", mds);
      strcat(sqlstr, mds2);

      get_member(n, packet, mds, &mdi);     /* build NAME */
      sprintf(mds2, "NAME = '%s' ", mds);
      strcat(sqlstr, mds2);

      sprintf(mds2, " WHERE _rowid = %d ", rowid);
      strcat(sqlstr, mds2);

    break;
    case SQL_DELETE:
      get_member(n, packet, mds, &mdi);	/* fetch ROWID */
      sprintf(sqlstr, "DELETE FROM %s WHERE _ROWID = %d", backend.name, mdi);
    break;
    case SQL_SELECT_PRI:
      get_member(n, packet, mds, &mdi);	/* fetch ROWID */
      sprintf(sqlstr, "SELECT * FROM %s WHERE _ROWID = %d", backend.name, mdi);
    break;
    case SQL_SELECT_ALL:
      sprintf(sqlstr, "SELECT * FROM %s", backend.name);
    break;
    case SQL_SELECT:
      sprintf(sqlstr, "SELECT * FROM %s WHERE ", backend.name);
      while(*n < tpl){
        a = get_member(n, packet, mds, &mdi);		/* get the field identifier */
        b = get_member(n, packet, mds2, &mdi2);		/* get the field value */
        switch(a){
          case PDUMEMTYPE_INT:
          break;
          case -1:					/* Fatal error in packet, can't decode further */
            return -1;
          default:
            fprintf(stderr,"Error in packet.\n");
            return FALSE;
        }
        switch(mdi){					/* check out what field it identifies */
          case OBJ_DB_TABLE_INTERFACE_ROWID:		/* rest is table field identifier */
            strcat(sqlstr, "_rowid = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_CREATED:
            strcat(sqlstr, "created = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_MODIFIED:
            strcat(sqlstr, "modified = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_HOST:
            strcat(sqlstr, "host = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_SNMP:
            strcat(sqlstr, "snmp = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_TRANSPORT:
            strcat(sqlstr, "transport = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_ADDRESS:
            strcat(sqlstr, "address = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_NETMASK:
            strcat(sqlstr, "netmask = ");
            break;
          case OBJ_DB_TABLE_INTERFACE_NAME:
            strcat(sqlstr, "name = ");
            break;

          default:
            fprintf(stderr,"FATAL: APP_ERR generic packet decode error, at packetpointer %d.\n", *n);
            return FALSE;
        }
        switch(b){
          case PDUMEMTYPE_INT:				/* its the value of the field */
            sprintf(mds, "%d AND ", mdi2);
            strcat(sqlstr, mds);
            break;
          case PDUMEMTYPE_STR:				/* its the value of the field */
            sprintf(mds, "'%s' AND ", mds2);
            strcat(sqlstr, mds);
            break;
          case -1:					/*Fatal error on packet, can't decode further */
            return -1;
          default:
            fprintf(stderr,"Error in packet.\n");
        }
      } /*end of while decoding member records */
      i = strlen(sqlstr);
      *((char *)sqlstr+i-5) = 0;              /*remove last ' AND '*/
    break;
    default:
      fprintf(stderr,"FATAL: APP_ERR packet decode error, undefined SQL command\n"); /*try give some output even it may be worthless*/
      return FALSE;
  }
  /* SQL string is built, it is now ready to send to SQL database */
  fprintf(stderr,"SQL STRING: %s\n", (char *) sqlstr);
  return TRUE;
}

/*****************************************************************************
**
**  decode_sql()
**
** takes a SQL result and builds membersRecords. 
**
** arguments:
** packet, here we put resulting memberrecords
** sql,    this is the SQL result to use.
** numfields, is how many fields per row to expect
** *n,     is packet offset
** tpl,    total packet length.
** 
**
** returns:
**
**  TRUE  --  If the operation succeeded
**  FALSE --  If the operation failed
**
*****************************************************************************/

static gboolean
decode_sql(gpointer packet, G_sql_query *sql, gint numfields, gint *n)
{
gchar *data;
gchar nulstr[] = "";
gint nulint = 0;
guint i;

  if(!(data = g_sql_field_pos(sql, 0))) data = &nulint;     /* ROWID */
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 1))) data = nulstr;      /* CREATED */
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 2))) data = nulstr;      /* modified */
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 3))) data = &nulint;     /*host */
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 4))) data = &nulint;     /*snmp */
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 5))) data = &nulint;
  add_guint(n, packet, strtol(data,0,0));

  if(!(data = g_sql_field_pos(sql, 6))) data = nulstr;      /*address */
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 7))) data = nulstr;
  add_asciiz(n, packet, data);

  if(!(data = g_sql_field_pos(sql, 8))) data = nulstr;     /*name */
  add_asciiz(n, packet, data);

  return TRUE;
}

/* encode_members()
 *
 * This function takes a DB_host table and makes
 * memberrecords of all structure members.
 * remember, to compile a valid packet to send
 * to database daemon, one must create a header
 * and after calling this function, set total
 * packet length.
 *
 * Arguments:
 * packet     This is destination of all members created
 * data       data this is a pointer to a DB_host table
 * 
 * Returns:
 * how much data that has been encoded
 *
 */

static gint 
encode_members(gpointer packet, gpointer data)
{
  gint n=0;

  fprintf(stderr,"interface_plugin: encoding members\n");
  
  add_guint (&n, packet, ((DB_interface *)data)->rowid);
  add_asciiz(&n, packet, ((DB_interface *)data)->created);
  add_asciiz(&n, packet, ((DB_interface *)data)->modified);
  add_guint (&n, packet, ((DB_interface *)data)->host);
  add_guint (&n, packet, ((DB_interface *)data)->snmp);
  add_guint (&n, packet, ((DB_interface *)data)->transport);
  add_asciiz(&n, packet, ((DB_interface *)data)->address);
  add_asciiz(&n, packet, ((DB_interface *)data)->netmask);
  add_asciiz(&n, packet, ((DB_interface *)data)->name);
  return n;
}

static gpointer
decode_members(gpointer packet, gint *n, gint plen)
{
  static gchar member[2049];
  static guint memint;
  static gpointer table;
  
  if(*n >= plen) return 0;
  fprintf(stderr,"allocating memory: %d bytes.\n", sizeof(DB_interface));
  if(!(table = g_new0 (DB_interface, 1)))
    {
      fprintf(stderr,"ERR: couldn't alloc()\n");
      return 0;
    }
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->rowid = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->created = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->modified = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->host = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->snmp = memint;

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->transport = memint;


  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->address = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->netmask = g_strdup(member);

  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_interface *) table)->name = g_strdup(member);

  return table;
}

/* add_hash()
 *
 * this functions will add data 
 * to an hash table, where it is nessecery not to
 * have a duplicate of the primary key in host_table.
 *
 *
 */
static gboolean 
add_hash(GHashTable *hash, gpointer data)
{
  
  if (g_hash_table_lookup (hash, ((DB_interface *) data)->rowid))
    {
      /*value of primary key was found in hash table, so its
        illegal to add.
      */
      return FALSE;
    }
  g_hash_table_insert (hash, ((DB_interface *) data)->rowid, data);

  return TRUE;
}

/* remove_hash()
 *
 * this functions will remove data 
 * from an hash table, where it is nessecery not to
 * have a duplicate of the primary key in host_table.
 *
 *
 */
static gboolean 
remove_hash (GHashTable *hash, gpointer data)
{
  
  if (g_hash_table_lookup (hash, ((DB_interface *) data)->rowid))
    {
      /*value of primary key was not found in hash table.*/
      return FALSE;
    }
  g_hash_table_remove (hash, ((DB_interface *) data)->rowid);
  
  return TRUE;
}



/* Object oriented section */

static gxobject *
obj_create()
{
DB_interface *dbi;
gxobject_ctx *ctx;
gxobject *obj;

  dbi = g_new0 (DB_interface, 1);
  ctx = g_new0 (gxobject_ctx,1);
  obj = g_new0 (gxobject,1);

  ctx->tables = db_tables;
  obj->objnum = 0;
  obj->type = OBJ_DB_INTERFACE;
  obj->ctx = ctx;
  obj->obj = dbi;

  if (dbi)
    {
      dbi->created  = g_strdup ("(not in database)");
      dbi->modified = g_strdup (dbi->created);
      return obj;
    }
  return NULL;
}

static void
obj_destroy(gxobject *obj)
{
DB_interface *dbi;

  dbi = obj->obj;

  g_assert (dbi->rowid == 0);
  if (dbi->created)  g_free (dbi->created);
  if (dbi->modified) g_free (dbi->modified);
  if (dbi->address)  g_free (dbi->address);
  if (dbi->netmask)  g_free (dbi->netmask);
  if (dbi->name)     g_free (dbi->name);
  g_free (dbi);

  g_free (obj->ctx);
  g_free (obj);
}

static void
obj_add(gxobject *obj)
{
gxobject_ctx *ctx;
DB_interface *dbi;

  ctx = obj->ctx;
  dbi = obj->obj;

fprintf(stderr,"a");

  g_assert (dbi->rowid   == 0);
  g_assert (dbi->DB_host == NULL);
  g_assert (dbi->DB_snmp == NULL);
fprintf(stderr,"b");

  dbi->rowid = g_sqldb_highest_rowid (ctx->tables.interface_sqldb, "_rowid") + 1;
fprintf(stderr,"c");

  if (dbi->host)
    {
      dbi->DB_host = g_sqldb_row_find (ctx->tables.host_sqldb, "_rowid", &dbi->host);
      dbi->DB_host->DB_interfaces =
                        g_list_append (dbi->DB_host->DB_interfaces, dbi);
    }
fprintf(stderr,"d");
  if (dbi->snmp)
    {
      dbi->DB_snmp = g_sqldb_row_find (ctx->tables.snmp_sqldb, "_rowid", &dbi->snmp);
      dbi->DB_snmp->DB_interfaces =
                        g_list_append (dbi->DB_snmp->DB_interfaces, dbi);
    }

fprintf(stderr,"e");
  if (dbi->created) g_free (dbi->created);
  dbi->created = db_timestamp ();

fprintf(stderr,"f");
  if (dbi->modified) g_free (dbi->modified);
  dbi->modified = g_strdup (dbi->created);

fprintf(stderr,"g");
  g_sqldb_row_add (ctx->sock, ctx->tables.interface_sqldb, dbi);
fprintf(stderr,"h");
}

static void
obj_update(gxobject *obj)
{
gxobject_ctx *ctx;
DB_interface *dbi;

  ctx = obj->ctx;
  dbi = obj->obj;

  g_assert (dbi->rowid != 0);
 
  if (dbi->DB_host)                             /* If previously assigned */
    if (dbi->DB_host->rowid != dbi->host)       /* to a different host ... */
      {                                         /* then unassign it */
        dbi->DB_host->DB_interfaces =
                        g_list_remove (dbi->DB_host->DB_interfaces, dbi);
        dbi->DB_host = NULL;
      }

  if (dbi->host)                /* Is the interface assigned to a host? */
    if (!dbi->DB_host)          /* Yes -- add to host GList if necessary */
      {
        dbi->DB_host = g_sqldb_row_find (ctx->tables.host_sqldb, "_rowid", &dbi->host);
        dbi->DB_host->DB_interfaces =
                        g_list_append (dbi->DB_host->DB_interfaces, dbi);
      }

  if (dbi->DB_snmp)                             /* If previously using a */
    if (dbi->DB_snmp->rowid != dbi->snmp)       /* different SNMP setting */
      {                                         /* then unassign it */
        dbi->DB_snmp->DB_interfaces =
                        g_list_remove (dbi->DB_snmp->DB_interfaces, dbi);
        dbi->DB_snmp = NULL;
      }

  if (dbi->snmp)                /* Is the interface assigned SNMP settings */
    if (!dbi->DB_snmp)          /* Yes -- add to snmp GList if necessary */
      {
        dbi->DB_snmp = g_sqldb_row_find (ctx->tables.snmp_sqldb, "_rowid", &dbi->snmp);
        dbi->DB_snmp->DB_interfaces =
                        g_list_append (dbi->DB_snmp->DB_interfaces, dbi);
      }

  if (dbi->modified) g_free (dbi->modified);
  dbi->modified = db_timestamp ();

  g_sqldb_row_update (ctx->sock, ctx->tables.interface_sqldb, dbi);
}

static void
obj_delete(gxobject *obj)
{
gxobject_ctx *ctx;
DB_interface *dbi;

  ctx = obj->ctx;
  dbi = obj->obj;

  g_assert (dbi->rowid != 0);

  if (dbi->DB_host)                             /* If assigned to a host */
        dbi->DB_host->DB_interfaces =           /* then unassign it */
                        g_list_remove (dbi->DB_host->DB_interfaces, dbi);
  dbi->DB_host = NULL;

  if (dbi->DB_snmp)                             /* If assigned SNMP settings */
        dbi->DB_snmp->DB_interfaces =           /* then unassign them */
                        g_list_remove (dbi->DB_snmp->DB_interfaces, dbi);
  dbi->DB_snmp = NULL;

  g_assert (g_sqldb_row_delete (ctx->sock, ctx->tables.interface_sqldb, dbi) == TRUE);

  dbi->rowid = 0;

}

static gboolean
obj_match(gxobject *obj)
{
gxobject_db_filter *filter;
gxobject_ctx *ctx;
guint n=0;
guint t=1;
DB_interface *row;
void *a;
char *b;

  ctx = obj->ctx;
  filter = ctx->user;
  row = obj->obj;

  for(;filter->args[n];n++,n++)
    {
fprintf(stderr,"loading data from filter, at position %d\n", n);
      a = filter->args[n];                              /*get field name */
      b = filter->args[n+1];                            /* pointer to field data */
fprintf(stderr,"Checking filter pair type: %d\n", *((gint *)a));
fprintf(stderr,"   filter value (integer or pointer): %d\n", *((gint *)b));
      switch(*((gint *)a))				/* test type of field name */
        {
          case OBJ_DB_TABLE_INTERFACE_ROWID:
fprintf(stderr,"   matching ROWID\n");
            if(*((gint *)b) != row->rowid) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_CREATED:
fprintf(stderr,"   matching CREATED\n");
            if(strcmp(b, row->created)) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_MODIFIED:
fprintf(stderr,"   matching MODIFIED\n");
            if(strcmp(b, row->modified)) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_HOST:
fprintf(stderr,"   matching HOST\n");
            if(*((gint *)b) != row->host) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_SNMP:
fprintf(stderr,"   matching SNMP\n");
            if(*((gint *)b) != row->snmp) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_TRANSPORT:
fprintf(stderr,"   matching TRANSPORT\n");
            if(*((gint *)b) != row->transport) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_ADDRESS:
fprintf(stderr,"   matching ADDRESS\n");
            if(strcmp(b, row->address)) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_NETMASK:
fprintf(stderr,"   matching NETMASK\n");
            if(strcmp(b, row->netmask)) t = 0;
          break;
          case OBJ_DB_TABLE_INTERFACE_NAME:
fprintf(stderr,"   matching NAME\n");
            if(strcmp(b, row->name)) t = 0;
          break;
        }
    }

  if(t) return TRUE;
  return FALSE;
}

/* obj_load() - loads a database table into memory
 *
 * This can load a database filter using a filter
 *
 * It will return result in a glist of rows in obj->obj
 * 
 * If unsuccessful or a empty table it returns NULL
 *
 */

static gboolean
obj_load(gxobject *obj)
{
gxobject_db_filter *filter;
gxobject_ctx       *ctx;
guint              n=0;
void               *a;
char               *b;

gchar              *query;            /* Text of SQL query */
gint               plen,i,h;
gint               rlen;
gchar packet[20000];
static GList              *gl = NULL;
gpointer tableres;


  ctx = obj->ctx;
  filter = ctx->user;

  /* initialize packet header */

  i = set_pdu_header(packet, TABLE_LOAD_FILTERED, OBJ_DB_INTERFACE);

  /* go through the filter, read a pair (two) at a time,
     and append them to the packet */

  for(;filter->args[n];n++,n++)
    {
fprintf(stderr,"loading data from filter, at position %d\n", n);
      a = filter->args[n];                              /*get field name */
      b = filter->args[n+1];                            /* pointer to field data */
fprintf(stderr,"Checking filter pair type: %d\n", *((gint *)a));
fprintf(stderr,"   filter value (integer or pointer): %d\n", *((gint *)b));
      switch(*((gint *)a))				/* test type of field name */
        {
          case OBJ_DB_TABLE_INTERFACE_ROWID:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_ROWID);
            add_guint(&i, packet, *((gint *)b));
          break;
          case OBJ_DB_TABLE_INTERFACE_CREATED:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_CREATED);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_INTERFACE_MODIFIED:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_MODIFIED);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_INTERFACE_HOST:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_HOST);
            add_guint(&i, packet, *((gint *)b));
          break;
          case OBJ_DB_TABLE_INTERFACE_SNMP:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_SNMP);
            add_guint(&i, packet, *((gint *)b));
          break;
          case OBJ_DB_TABLE_INTERFACE_TRANSPORT:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_TRANSPORT);
            add_guint(&i, packet, *((gint *)b));
          break;
          case OBJ_DB_TABLE_INTERFACE_ADDRESS:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_ADDRESS);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_INTERFACE_NETMASK:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_NETMASK);
            add_asciiz(&i, packet, b);
          break;
          case OBJ_DB_TABLE_INTERFACE_NAME:
            add_guint(&i, packet, OBJ_DB_TABLE_INTERFACE_NAME);
            add_asciiz(&i, packet, b);
          break;
        }
    }

  memcpy(packet+sizeof(gint), &i, sizeof(gint));		/*write total packet length*/
  dd_write(ctx->sock, i, packet);				/*send packet to database daemon*/

  if((n = dd_read_header(ctx->sock, packet)) == -1){
        fprintf(stderr,"  dd_read_header() error\n");
         return -1;
  }

  memcpy(&plen,packet+sizeof(gint), sizeof(guint));                     /*get packet length*/
  if(plen <= 8) return FALSE;                                           /*table is empty*/
  if((query = dd_read_rest(ctx->sock, plen, packet)) == -1) {
    fprintf(stderr,"  dd_read_rest() error\n");
    return -1;
  }

  /*ADD: cmp rlen to plen  ie what length packet says it has and what length readed*/

  n = sizeof(gint) * 2;                                                 /* header size */

  while((tableres = decode_members(query, &n, plen))){       /*we have the first row in an unknown table in tableres*/
    gl = g_list_append(gl, tableres);
  }
  obj->obj = gl;
  return;
}


/* 
 *  service functions needed by all table plugins
 *
 *
 */

/******************************************************************************
**
**  Subroutine to load the plugin.  Set the plugin type to PLUGIN_DATABASE.
**
******************************************************************************/

int 
load_plugin (PluginData * pd)
{
  D_FUNC_START;
/* enable if we merge into ordinary plugin utility
  pd->type = PLUGIN_TABLES;
*/
  pd->name = g_strdup ("interface table plugin");
  D_FUNC_END;
  return 0;
}

/******************************************************************************
**
** Subroutine to unload the plugin
**
******************************************************************************/

void
unload_plugin (PluginData * pd)
{
  D_FUNC_START;
  g_sql_unregister_table(&backend);
  D_FUNC_END;
}

/******************************************************************************
**
**  Subroutine to start the plugin
**
******************************************************************************/

void
start_plugin (PluginData * pd)
{

  fprintf(stderr,"registering host table plugin\n");

  if(g_sql_register_table(&backend)){ /*must have a test, but this function is a void so it doesn't matter now*/
    fprintf(stderr,"start_plugin(): successfully registered plugin!\n");
    return;
  }
  fprintf(stderr,"start_plugin(): couldn't register plugin..\n");
}

/* EOF */

