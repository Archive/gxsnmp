/* -*- Mode: C -*-
 *  gsql -- A simplified, unified interface to various SQL packages.
 *  Copyright (C) 1999 John Schulien
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License. 
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  map.c -- the database table support plugin
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "gxsnmpdb_types.h"
#include "plugin.h"

#include "plugins-table.h"
#include "g_sql.h"
#include "g_sql-table.h"
#include "tables.h"


extern gxdb_tables db_tables;
extern void debug();                     /*this is for gdb*/

#define MAP_NUM_FIELDS 7 		/*TAGS is unused */


/****************************************************************************
 *  Forward references
 */
static gboolean           tableinfo           (gint query, 
					       gpointer indata, 
					       void **outdata);
static gboolean           encode_sql          (gpointer, 
					       gchar *, 
					       gint, 
					       gint *, 
					       gint);
static gboolean           decode_sql          (gpointer, 
					       G_sql_query *, 
					       gint, 
					       gint *n);
static gint               encode_members      (gpointer, 
					       gpointer);
static gpointer           decode_members      (gpointer, 
					       gint *, 
					       gint);
static gboolean           add_hash            (GHashTable *hash, 
					       gpointer);
static gboolean           remove_hash         (GHashTable *hash, 
					       gpointer);

static gxobject         * obj_create          ();
static void               obj_destroy         (gxobject *obj);
static void               obj_add             (gxobject *obj);
static void               obj_update          (gxobject *obj);
static void               obj_delete          (gxobject *obj);
static gboolean           obj_match           (gxobject *obj);
static gboolean           obj_load            (gxobject *obj);


/*
**  The database backend control block for the host table engine
*/

static struct _G_sql_table backend =
{
  "map",			/* Name of the table */
  OBJ_DB_MAP,
  tableinfo,
  encode_sql,      
  decode_sql,		
  encode_members,
  decode_members,
  add_hash,
  remove_hash,
  obj_create,
  obj_destroy,
  obj_add,
  obj_update,
  obj_delete,
  obj_match,
  obj_load
};

/**
 * tableinfo:
 *
 * Arguments:
 * query - is what info user requests.
 * gpointer is a piece of memory, minimum 64 bytes
 * carrying then answer.
 **/
gboolean
tableinfo(gint query, gpointer indata, void **outdata)
{
  gint i = MAP_NUM_FIELDS;

  switch(query)
    {
    case NUM_FIELDS:          /* number of fields available, zero is invalid*/
      *((gint *) outdata) = i;
      return TRUE;
    case TABLE_POINTER:                         /* pointer to this table */
      fprintf(stderr,"address to outdata is: %p\n", *outdata);
      *outdata = map_sqldb;
      return TRUE;
    default:                   /*user has asked a question I don't know */
      return FALSE;
    }
}



/*****************************************************************************
**
** encode_sql()
**
** Build a SQL query out of member records found in the packet.
** 
** Arguments:
** packet    is a pointer to beginning of the packet
** sqlstr    is a SQL string we are building
** querytype is what type of SQL string to build
** *n        is how far into the packet we have decoded
** tpl       is total packet length, so we know when to stop decoding Members
**
** Returns:
** TRUE  --  If the operation succeeded
** FALSE --  If the operation failed
**
*****************************************************************************/

static gboolean 
encode_sql(gpointer packet, gchar *sqlstr, gint querytype, gint *n, gint tpl)
{
  static gchar mds[1000];
  static gchar mds2[1000];
  static gint a,b,mdi,mdi2,i, rowid;
  
  switch(querytype)
    {
    case SQL_INSERT: /*this method should support all tables*/
      sprintf(sqlstr, "INSERT INTO %s VALUES(", backend.name);
      while(*n < tpl)
	{
	  switch(get_member(n, packet, mds, &mdi))
	    {
	    case PDUMEMTYPE_INT:
	      sprintf(mds2, "%d,", mdi);
	      strcat(sqlstr, mds2);
	      break;
	    case PDUMEMTYPE_STR:
	      sprintf(mds2, "'%s',", mds);
	      strcat(sqlstr, mds2);
	      break;
	    default:
	      fprintf(stderr,"FATAL: APP_ERR generic packet decode error, at packetpointer %d.\n", *n); /*try give some output even it may be worthless*/
	      return FALSE;
	    }
	}
      i = strlen(sqlstr);
      *((char *)sqlstr+(--i)) = 0;              /*remove last comma*/
      strcat(sqlstr, ")");
      break;
    case SQL_UPDATE:
      sprintf(sqlstr, "UPDATE %s SET ", backend.name);
      
      get_member(n, packet, mds, &mdi);	/* build ROWID */
      rowid = mdi;
      
      get_member(n, packet, mds, &mdi);     /* build CREATED */
      sprintf(mds2, "created = '%s', ", mds);
      strcat(sqlstr, mds2);
      
      get_member(n, packet, mds, &mdi);     /* build MODIFIED */
      sprintf(mds2, "modified = '%s', ", mds);
      strcat(sqlstr, mds2);
      
      get_member(n, packet, mds, &mdi);     /* build NAME */
      sprintf(mds2, "name = '%s', ", mds);
      strcat(sqlstr, mds2);
      
      get_member(n, packet, mds, &mdi);     /* build TAB */
      sprintf(mds2, "tab = '%s', ", mds);
      strcat(sqlstr, mds2);
      
      get_member(n, packet, mds, &mdi);     /* build DESCRIPTION */
      sprintf(mds2, "description = '%s', ", mds);
      strcat(sqlstr, mds2);
      
      get_member(n, packet, mds, &mdi);     /* build TAGS */
      sprintf(mds2, "tags = '%s' ", mds);
      strcat(sqlstr, mds2);
      
      sprintf(mds2, " WHERE _rowid = %d ", rowid);
      strcat(sqlstr, mds2);
      
      break;
    case SQL_DELETE:
      get_member(n, packet, mds, &mdi);	/* fetch ROWID */
      sprintf(sqlstr, "DELETE FROM %s WHERE _ROWID = %d", backend.name, mdi);
      break;
    case SQL_SELECT_PRI:
      get_member(n, packet, mds, &mdi);	/* fetch ROWID */
      sprintf(sqlstr, "SELECT * FROM %s WHERE _ROWID = %d", backend.name, mdi);
    break;
    case SQL_SELECT_ALL:
      sprintf(sqlstr, "SELECT * FROM %s", backend.name);
    break;
    case SQL_SELECT:
      sprintf(sqlstr, "SELECT * FROM %s WHERE ", backend.name);
      while(*n < tpl)
	{
	  a = get_member(n, packet, mds, &mdi);           /* get the field identifier */
	  b = get_member(n, packet, mds2, &mdi2);         /* get the field value */
	  switch(a)
	    {
	    case PDUMEMTYPE_INT:
	      break;
	    case -1:
	      break;
	    default:
	      fprintf(stderr,"Error in packet.\n");
	      return FALSE;
	    }
	  switch(mdi)
	    {                                    /* check out what field it identifies */
	    case OBJ_DB_TABLE_MAP_ROWID:            /* rest is table field identifier */
	      strcat(sqlstr, "_rowid = ");
	      break;
	    case OBJ_DB_TABLE_MAP_CREATED:
	      strcat(sqlstr, "created = ");
	      break;
	    case OBJ_DB_TABLE_MAP_MODIFIED:
	      strcat(sqlstr, "modified = ");
	      break;
	    case OBJ_DB_TABLE_MAP_NAME:
	      strcat(sqlstr, "name = ");
	      break;
	    case OBJ_DB_TABLE_MAP_TAB:
	      strcat(sqlstr, "tab = ");
	      break;
	    case OBJ_DB_TABLE_MAP_DESCRIPTION:
	      strcat(sqlstr, "description = ");
	      break;
	    default:
	      fprintf(stderr,"FATAL: APP_ERR generic packet decode error, at packetpointer %d.\n", *n);
	      return FALSE;
	    }
	  switch(b)
	    {
	    case PDUMEMTYPE_INT:                          /* its the value of the field */
	      sprintf(mds, "%d AND ", mdi2);
	      strcat(sqlstr, mds);
	      break;
	    case PDUMEMTYPE_STR:                          /* its the value of the field */
	      sprintf(mds, "'%s' AND ", mds2);
	      strcat(sqlstr, mds);
	      break;
	    case -1:
	      break;
	    default:
	      fprintf(stderr,"Error in packet.\n");
	    }
	} /*end of while decoding member records */
      i = strlen(sqlstr);
      *((char *)sqlstr+i-5) = 0;              /*remove last ' AND'*/
      break;
    default:
      fprintf(stderr,"FATAL: APP_ERR packet decode error, undefined SQL command\n"); /*try give some output even it may be worthless*/
      return FALSE;
    }
  /* SQL string is built, it is now ready to send to SQL database */
  fprintf(stderr,"SQL STRING: %s\n", (char *) sqlstr);
  return TRUE;
}

/*****************************************************************************
**
**  decode_sql()
**
** takes a SQL result and builds membersRecords. 
**
** arguments:
** packet, here we put resulting memberrecords
** sql,    this is the SQL result to use.
** numfields, is how many fields per row to expect
** *n,     is packet offset
** tpl,    total packet length.
** 
**
** returns:
**
**  TRUE  --  If the operation succeeded
**  FALSE --  If the operation failed
**
*****************************************************************************/

static gboolean
decode_sql(gpointer packet, G_sql_query *sql, gint numfields, gint *n)
{
  gchar *data;
  gchar nulstr[] = "";
  gint nulint = 0;
  guint i;
  
  if(!(data = g_sql_field_pos(sql, 0))) data = &nulint;     /* ROWID */
  add_guint(n, packet, strtol(data,0,0));
  
  if(!(data = g_sql_field_pos(sql, 1))) data = nulstr;      /* CREATED */
  add_asciiz(n, packet, data);
  
  if(!(data = g_sql_field_pos(sql, 2))) data = nulstr;      /* modified */
  add_asciiz(n, packet, data);
  
  if(!(data = g_sql_field_pos(sql, 3))) data = nulstr;      /* name */
  add_asciiz(n, packet, data);
  
  if(!(data = g_sql_field_pos(sql, 4))) data = nulstr;      /* tab */
  add_asciiz(n, packet, data);
  
  if(!(data = g_sql_field_pos(sql, 5))) data = nulstr;      /* description */
  add_asciiz(n, packet, data);
  
  if(!(data = g_sql_field_pos(sql, 6))) data = nulstr;      /* tags */
  add_asciiz(n, packet, data);
  
  return TRUE;
}
/**
 * encode_members
 *
 * This function takes a DB_host table and makes
 * memberrecords of all structure members.
 * remember, to compile a valid packet to send
 * to database daemon, one must create a header
 * and after calling this function, set total
 * packet length.
 *
 * Arguments:
 * packet     This is destination of all members created
 * data       data this is a pointer to a DB_host table
 * 
 * Returns:
 * how much data that has been encoded
 *
 **/

static gint 
encode_members(gpointer packet, gpointer data)
{
  gint n=0;

  fprintf(stderr,"map_plugin(): encoding members\n");
  
  fprintf(stderr,"1");
  add_guint (&n, packet, ((DB_map *)data)->rowid);
  g_assert(((DB_map *)data)->created);
  fprintf(stderr,"2");
  add_asciiz(&n, packet, ((DB_map *)data)->created);
  g_assert(((DB_map *)data)->modified);
  fprintf(stderr,"3");
  add_asciiz(&n, packet, ((DB_map *)data)->modified);
  g_assert(((DB_map *)data)->name);
  fprintf(stderr,"4");
  add_asciiz(&n, packet, ((DB_map *)data)->name);
  g_assert(((DB_map *)data)->tab);
  fprintf(stderr,"5");
  add_asciiz(&n, packet, ((DB_map *)data)->tab);
  g_assert(((DB_map *)data)->description);
  fprintf(stderr,"6");
  add_asciiz(&n, packet, ((DB_map *)data)->description);
  fprintf(stderr,"MAP_done\n");
  fprintf(stderr,"7");
  add_asciiz(&n, packet, "abc");
  fprintf(stderr,"MAP_done\n");
  return n;
}

static gpointer
decode_members(gpointer packet, gint *n, gint plen)
{
  static gchar member[2049];
  static guint memint;
  static gpointer table;
  
  if(*n >= plen) return 0;
  fprintf(stderr,"allocating memory: %d bytes.\n", sizeof(DB_map));
  if(!(table = g_new0(DB_map, 1)))
    {
      fprintf(stderr,"ERR: couldn't alloc()\n");
      return 0;
    }
  get_member(n, packet, member, &memint);
  ((DB_map *) table)->rowid = memint;
  
  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_map *) table)->created = g_strdup(member);
  
  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_map *) table)->modified = g_strdup(member);
  
  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_map *) table)->name = g_strdup(member);
  
  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_map *) table)->tab = g_strdup(member);
  
  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint);
  ((DB_map *) table)->description = g_strdup(member);
  
  if(*n >= plen) return 0;
  get_member(n, packet, member, &memint); /*read in tags, unused*/
  
  return table;
}
/**
 * add_hash
 *
 * this functions will add data 
 * to an hash table, where it is nessecery not to
 * have a duplicate of the primary key in host_table.
 *
 *
 **/
static gboolean 
add_hash(GHashTable *hash, gpointer data)
{
  
  if (g_hash_table_lookup (hash, ((DB_map *) data)->rowid))
    {
      /*value of primary key was found in hash table, so its
        illegal to add.
      */
      return FALSE;
    }
  g_hash_table_insert (hash, ((DB_map *) data)->rowid, data);

  return TRUE;
}
/**
 * remove_hash
 *
 * this functions will remove data 
 * from an hash table, where it is nessecery not to
 * have a duplicate of the primary key in host_table.
 *
 *
 **/
static gboolean 
remove_hash (GHashTable *hash, gpointer data)
{
  
  if (g_hash_table_lookup (hash, ((DB_map *) data)->rowid))
    {
      /*value of primary key was not found in hash table.*/
      return FALSE;
    }
  g_hash_table_remove (hash, ((DB_map *) data)->rowid);
  
  return TRUE;
}


/* Object oriented section */

static gxobject *
obj_create()
{
  DB_map *dbm;
  gxobject_ctx *ctx;
  gxobject *obj;
  
  dbm = g_new0 (DB_map, 1);
  ctx = g_new0 (gxobject_ctx,1);
  obj = g_new0 (gxobject,1);
  
  ctx->tables = db_tables;
  obj->objnum = 0;
  obj->type = OBJ_DB_MAP;
  obj->ctx = ctx;
  obj->obj = dbm;
  
  if (dbm)
    {
      dbm->created  = g_strdup ("(not in database)");
      dbm->modified = g_strdup (dbm->created);
      return obj;
    }
  return NULL;
}

static void
obj_destroy(gxobject *obj)
{
  DB_map *dbm;
  
  dbm = obj->obj;
  
  g_return_if_fail (dbm != NULL);
  if (dbm->rowid != 0)
    {
      g_warning ("map_destroy: attempt to free a map instance that is "
                 "still in use.");
      D_FUNC_END;
      return;
    }
  if (dbm)
    {
      if (dbm->created)        g_free (dbm->created);
      if (dbm->modified)       g_free (dbm->modified);
      if (dbm->name)           g_free (dbm->name);
      if (dbm->tab)            g_free (dbm->tab);
      if (dbm->description)    g_free (dbm->description);
      g_free (dbm);
      dbm = NULL;
    }
  
  
  g_free (obj->ctx);
  g_free (obj);
}

static void
obj_add(gxobject *obj)
{
  DB_map *dbm;
  gxobject_ctx *ctx;
  
  ctx = obj->ctx;
  dbm = obj->obj;
  
  g_return_if_fail (dbm != NULL);
  if (dbm->rowid != 0)
    {
      g_warning ("Attempted to re add a map which is in the DB already.");
      return ;
    }
  dbm->rowid = g_sqldb_highest_rowid (ctx->tables.map_sqldb, "_rowid") + 1;
  if (dbm->created)
    g_free (dbm->created);
  dbm->created = db_timestamp ();
  if (dbm->modified)
    g_free (dbm->modified);
  dbm->modified = g_strdup (dbm->created);
  d_print (DEBUG_DUMP, "Adding %s\n", dbm->name);
  if (g_sqldb_row_add (ctx->sock, ctx->tables.map_sqldb, dbm))
    {
      return;
    }
  dbm->rowid = 0;
}

static void
obj_update (gxobject *obj)
{
  DB_map *dbm;
  gxobject_ctx *ctx;
  
  ctx = obj->ctx;
  dbm = obj->obj;
  
  g_return_if_fail (dbm != NULL);
  g_return_if_fail (dbm->rowid != 0);
  if (dbm->modified)
    g_free (dbm->modified);
  dbm->modified = db_timestamp ();
  if (g_sqldb_row_update (ctx->sock, ctx->tables.map_sqldb, dbm))
    {
      D_FUNC_END;
      return;
    }
  g_warning ("map_update: failed to update the database..");
}

static void
obj_delete(gxobject *obj)
{
  DB_map *dbm;
  gxobject_ctx *ctx;
  GList        * gl;
  
  ctx = obj->ctx;
  dbm = obj->obj;
  
  g_return_if_fail (dbm != NULL);
  if (dbm->rowid == 0)
    {
      g_warning ("map_delete: attempt to delete a map that is marked as "
                 "deleted already.");
      return;
    }
  
  while ((gl = dbm->DB_graphs))         /* Assignment intended */
    {
      graph_delete  ((DB_graph *) gl->data);
      graph_destroy ((DB_graph *) gl->data);
    }
  g_slist_free (dbm->DB_graphs);
  dbm->DB_graphs = NULL;
  if (g_sqldb_row_delete (ctx->sock, ctx->tables.map_sqldb, dbm))
    {
      dbm->rowid = 0;
      return;
    }
  g_warning ("map_delete: failed.");
}

static gboolean
obj_match(gxobject *obj)
{
  gxobject_db_filter *filter;
  gxobject_ctx *ctx;
  guint n=0;
  guint t=1;
  DB_map *row;
  void *a;
  char *b;
  
  ctx = obj->ctx;
  filter = ctx->user;
  row = obj->obj;
  
  for(;filter->args[n];n++,n++)
    {
      a = filter->args[n];                              /*get field name */
      b = filter->args[n+1];                            /* pointer to field data */
      switch(*((gint *)a))                                              /* test type of field name */
        {
	case OBJ_DB_TABLE_MAP_ROWID:
	  if(*((gint *)b) != row->rowid) t = 0;
          break;
	case OBJ_DB_TABLE_MAP_CREATED:
	  if(strcmp(b, row->created)) t = 0;
          break;
	case OBJ_DB_TABLE_MAP_MODIFIED:
	  if(strcmp(b, row->modified)) t = 0;
          break;
	case OBJ_DB_TABLE_MAP_NAME:
	  if(strcmp(b, row->name)) t = 0;
          break;
	case OBJ_DB_TABLE_MAP_TAB:
	  if(strcmp(b, row->tab)) t = 0;
          break;
	case OBJ_DB_TABLE_MAP_DESCRIPTION:
	  if(strcmp(b, row->description)) t = 0;
          break;
        }
    }
  
  if(t) return TRUE;
  return FALSE;
}

/**
 * obj_load() - loads a database table into memory
 *
 * This can load a database filter using a filter
 *
 * It will return result in a glist of rows in obj->obj
 *
 * If unsuccessful or a empty table it returns NULL
 *
 **/
static gboolean
obj_load(gxobject *obj)
{
  gxobject_db_filter *filter;
  gxobject_ctx       *ctx;
  guint              n=0;
  DB_interface       *row;
  void               *a;
  char               *b;
  
  gchar              *query;            /* Text of SQL query */
  gint               plen,i,h;
  gint               rlen;
  gchar packet[20000];
  static GList              *gl = NULL;
  gpointer tableres;
  
  
  ctx = obj->ctx;
  filter = ctx->user;
  
  i = TABLE_LOAD_FILTERED;
  memcpy(packet, &i, sizeof(gint));                             /*set action type*/
  i = OBJ_DB_MAP;
  memcpy(packet+(sizeof(gint)*2), &i, sizeof(gint));            /*set table name*/
  i += sizeof(gint) * 3;                                        /*header length*/
  
  i = set_pdu_header(packet, TABLE_LOAD_FILTERED, OBJ_DB_MAP);
  
  for(;filter->args[n];n++,n++)
    {
      fprintf(stderr,"loading data from filter, at position %d\n", n);
      a = filter->args[n];                              /*get field name */
      b = filter->args[n+1];                            /* pointer to field data */
      fprintf(stderr,"Checking filter pair type: %d\n", *((gint *)a));
      fprintf(stderr,"   filter value (int or pointer): %d\n", *((gint *)b));
      switch(*((gint *)a))                              /* test type of field name */
        {
	case OBJ_DB_TABLE_MAP_ROWID:
	  add_guint(&i, packet, OBJ_DB_TABLE_MAP_ROWID);
	  add_guint(&i, packet, *((gint *)b));
          break;
	case OBJ_DB_TABLE_MAP_CREATED:
	  add_guint(&i, packet, OBJ_DB_TABLE_MAP_CREATED);
	  add_asciiz(&i, packet, b);
          break;
	case OBJ_DB_TABLE_MAP_MODIFIED:
	  add_guint(&i, packet, OBJ_DB_TABLE_MAP_MODIFIED);
	  add_asciiz(&i, packet, b);
          break;
	case OBJ_DB_TABLE_MAP_NAME:
	  add_guint(&i, packet, OBJ_DB_TABLE_MAP_NAME);
	  add_asciiz(&i, packet, b);
          break;
	case OBJ_DB_TABLE_MAP_TAB:
	  add_guint(&i, packet, OBJ_DB_TABLE_MAP_TAB);
	  add_asciiz(&i, packet, b);
          break;
	case OBJ_DB_TABLE_MAP_DESCRIPTION:
	  add_guint(&i, packet, OBJ_DB_TABLE_MAP_DESCRIPTION);
	  add_asciiz(&i, packet, b);
          break;
        }
    }
  
  memcpy(packet+sizeof(gint), &i, sizeof(gint));                /*write total packet length*/
  dd_write(ctx->sock, i, &packet);                              /*send packet to database daemon*/
  
  if((n = dd_read_header(ctx->sock, packet)) == -1){
    fprintf(stderr,"  dd_read_header() error\n");
    return -1;
  }
  
  memcpy(&plen,packet+sizeof(gint), sizeof(guint));                     /*get packet length*/
  fprintf(stderr,"  OBJ_MR_TPL: %d\n", plen);
  if(plen <= 8) return FALSE;                                           /*table is empty*/
  if((query = dd_read_rest(ctx->sock, plen, packet)) == -1) {
    fprintf(stderr,"  dd_read_rest() error\n");
    return -1;
  }
  fprintf(stderr,"  done_read\n");
  /*ADD: cmp rlen to plen  ie what length packet says it has and what length readed*/
  
  n = sizeof(gint) * 2;                                                 /* header size */
  
  while((tableres = decode_members(query, &n, plen))){       /*we have the first row in an unknown table in tableres*/
    gl = g_list_append(gl, tableres);
  }
  obj->obj = gl;
  return;
}



/* 
 *  service functions needed by all table plugins
 *
 *
 */

/******************************************************************************
**
**  Subroutine to load the plugin.  Set the plugin type to PLUGIN_DATABASE.
**
******************************************************************************/

int 
load_plugin (PluginData * pd)
{
  D_FUNC_START;
  /* enable if we merge into ordinary plugin utility
     pd->type = PLUGIN_TABLES;
  */
  pd->name = g_strdup ("interface table plugin");
  D_FUNC_END;
  return 0;
}

/******************************************************************************
 **
 ** Subroutine to unload the plugin
 **
 ******************************************************************************/

void
unload_plugin (PluginData * pd)
{
  D_FUNC_START;
  g_sql_unregister_table(&backend);
  D_FUNC_END;
}

/******************************************************************************
 **
 **  Subroutine to start the plugin
 **
 ******************************************************************************/

void
start_plugin (PluginData * pd)
{
  
  fprintf(stderr,"registering host table plugin\n");
  
  if(g_sql_register_table(&backend))
    { /*must have a test, but this function is a void so it doesn't matter now*/
      fprintf(stderr,"start_plugin(): successfully registered plugin!\n");
      return;
    }
  fprintf(stderr,"start_plugin(): couldn't register plugin..\n");
}

/* EOF */

