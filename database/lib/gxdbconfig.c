/* configuration code
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#include "tables.h"

gxdbconfig dbconfig;

char version[] = "$Id$"; /*usefull to 'strings' the binary*/


static void restrap(char *line) 
{
  if(!(strncmp(line,"\"",1)))
  {
    if (!(strncmp(line+(strlen(line)-1),"\"",1)))
    {
      strncpy(line,line+1,strlen(line)-2);
      strncpy(line+strlen(line)-2,"\0",1);
    }   
  }
}


static char *wordcpy(char *dst, char *src, int w){ /*copy space separated word 'w' to 'dst' from 'src'*/
int i=0,j=0;

  w--;
  *dst = '\0';
  while(1){
    if(!w) break;
      if((*(src+i) == '\0' || *(src+i) == '\n')) return 0; /*leave dst empty*/
      if(*(src+i) == '\"'){
        i++;
        while(*(src+i) != '\"' && !(*(src+i) == '\0' || *(src+i) == '\n')) i++;
        if(!(*(src+i) == '\0' || *(src+i) == '\n')) i++;
        continue;
      }
      if(*(src+i) == ' '){w--;i++; continue;}
      i++;
    }
    if(*(src+i) == '\"'){
    i++;
    while( !(*(src+i) == '\0' || *(src+i) == '\n') && !(*(src+i) == '\"') ){ *(dst+j) = *(src+i);i++;j++;}   /*move each character*/
    *(dst+j) = '\0';
    return dst;
  }
  while( !(*(src+i) == '\0' || *(src+i) == '\n') && !(*(src+i) == ' ') ){ *(dst+j) = *(src+i);i++;j++;}   /*move each character*/
  *(dst+j) = '\0';
  return dst;
}

static char *wordcpyt(char *dst, char *src, char *tmp, int w){ /*copy space separated word 'w' to 'dst' from 'src'*/
int i=0,j=0;

  w--;
  *dst = 0;
  while(1){
    if(!w) break;
    if((*(src+i) == '\0' || *(src+i) == '\n')) return 0; /*leave dst empty*/
    if(*(src+i) == *tmp){w--;i++; continue;}
    i++;
  }
  while( !(*(src+i) == '\0' || *(src+i) == '\n') && !(*(src+i) == *tmp) ){ *(dst+j) = *(src+i);i++;j++;}   /*move each character*/
  *(dst+j) = '\0';
  return dst;
}

int db_load_enviroment(){
char line[200],entity[200],value[200];
char *env;
FILE *fp;

	env = getenv("GXDB_BASE");
	if(!env) {
		fprintf(stderr,"Error: enviroment variable: GXDB_BASE is not set.\n");
		fprintf(stderr,"It should contain directory to gxdb.conf file.\n");
		fprintf(stderr,"example: 'GXDB_BASE=/usr/local/gxsnmp/conf'\n");
		return -1;
	}

	dbconfig.ddport = 4000; /*default port to use */

	strcpy(line,env);
	strcat(line,"/gxdb.conf");

	if(!(fp = fopen(line, "r"))) return -1;
	while(fgets(line,200,fp)){
		if(line[0] == '#') continue;
		wordcpyt(entity,line,"=",1);
		wordcpyt(value,line,"=",2);
		restrap(value);
		if(!strcmp("DATABASE_DAEMON_PORT",entity)) dbconfig.ddport = atoi(value);
		else if(!strcmp("TABLE_PLUGINS",entity)) strcpy(dbconfig.tabplug, value);
		else if(!strcmp("DATABASE_PLUGINS",entity)) strcpy(dbconfig.dbplug, value);
		else if(!strcmp("DATABASE_DAEMON_HOST",entity)) strcpy(dbconfig.ddhost, value);
		else if(!strcmp("DATABASE_PORT",entity)) dbconfig.dbport = atoi(value);
		else if(!strcmp("DATABASE_ENGINE",entity)) strcpy(dbconfig.dbtype, value);
		else if(!strcmp("DATABASE_HOST",entity)) strcpy(dbconfig.dbhost, value);
		else if(!strcmp("DATABASE_USER",entity)) strcpy(dbconfig.dbuser, value);
		else if(!strcmp("DATABASE_PASSWORD",entity)) strcpy(dbconfig.dbpass, value);
	}

	fclose(fp);
	return 0;
}
