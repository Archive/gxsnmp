/* -*- Mode: C -*-
 *  $Id$
 *  Copyright (C) 1999 Larry Liimatainen
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  Predefined filters for loaded database tables.
 */

/* This is a collection of filter that does the same as
   the previous find functions found in DB_* files

   Currently predefined filters:

   db_find_by_rowid
     
      set args[0] to the table field rowid
      set args[1] to rowid to search for

   db_graph_find_host

      set args[3] to map rowid
      set args[5] to host rowid
      
   db_graph_find_network

      set args[3] to map rowid
      set args[5] to network rowid

   db_graph_find_wire

      set args[3] to map rowid
      set args[5] to network rowid

   db_host_find_by_name

      set args[0] to point to host name

   db_network_find_by_address
  
      set args[1] to point to address
      set args[3] to point to netmask
 */

#include <glib.h>
#include "tables.h"

static gint aa = 0, /* this entry user has to insert the ROWID field name */
            ab = 0; /* and here insert ROWID value to search for */
static void *aarg[4] = {
  &aa,
  &ab,
  0,
  0
};
gxobject_db_filter db_find_by_rowid = {
  OBJ_FIND_SINGLE,
  aarg
};

static gint ba = OBJ_DB_TABLE_GRAPH_TYPE, 
            bb = DB_GRAPH_HOST,
            bc = OBJ_DB_TABLE_GRAPH_MAP,
            bd = 0, 				/* user input of map_row */
            be = OBJ_DB_TABLE_GRAPH_HOST,
            bf = 0; 				/* user input of host_row */
static void *barg[8] = {
  &ba,
  &bb,
  &bc,
  &bd,
  &be,
  &bf,
  0,
  0
};
gxobject_db_filter db_graph_find_host = {
  OBJ_FIND_SINGLE,
  barg
};

static gint ca = OBJ_DB_TABLE_GRAPH_TYPE, 
            cb = DB_GRAPH_NETWORK,
            cc = OBJ_DB_TABLE_GRAPH_MAP,
            cd = 0,				/* user input of map_row */
            ce = OBJ_DB_TABLE_GRAPH_NETWORK,
            cf = 0;                             /* user input of network_row */
static void *carg[8] = {
  &ca,
  &cb,
  &cc,
  &cd,
  &ce,
  &cf,
  0,
  0
};
gxobject_db_filter db_graph_find_network = {
  OBJ_FIND_SINGLE,
  carg
};

static gint da = OBJ_DB_TABLE_GRAPH_TYPE, 
            db = DB_GRAPH_WIRE,
            dc = OBJ_DB_TABLE_GRAPH_MAP,
            dd = 0,				/* user input of map_row */
            de = OBJ_DB_TABLE_GRAPH_NETWORK,
            df = 0;				/* user input of network_row */
static void *darg[8] = {
  &da,
  &db,
  &dc,
  &dd,
  &de,
  &df,
  0,
  0
};
gxobject_db_filter db_graph_find_wire = {
  OBJ_FIND_SINGLE,
  darg
};

static gint ea = OBJ_DB_TABLE_HOST_NAME;
static gchar *eb= 0;				/* user provided pointer to host_table->name */
static void *earg[4] = {
  &ea,
  &eb,
  0,
  0
};
gxobject_db_filter db_host_find_by_name = {
  OBJ_FIND_SINGLE,
  earg
};

static gchar fa = OBJ_DB_TABLE_NETWORK_ADDRESS,
             fc = OBJ_DB_TABLE_NETWORK_NETMASK;
static gint *fb = 0,				/* user provided pointer to network_table->address */
            *fd = 0;				/* user provided pointer to network_table->netmask */
static void *farg[6] = {
  &fa,
  &fb,
  &fc,
  &fd,
  0,
  0
};
gxobject_db_filter db_network_find_by_address = {
  OBJ_FIND_SINGLE,
  farg
};


