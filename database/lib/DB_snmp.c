/* -*- Mode: C -*-
 *  $Id$
 *
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

/* 
 * snmp.c containes utility functions related to the creation and destruction
 * of DB_snmp items, and functions to manage the DB_snmp database.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>
#include "g_sqldb.h"
#include "tables.h"
#include "gxsnmpdb.h"

#include "debug.h"

extern gint dcsock;

/****************************************************************************
 * snmp_create ()
 *
 * create a new snmp db instance.
 ***************************************************************************/
DB_snmp *
snmp_create (void)
{
  DB_snmp *dbs;
  D_FUNC_START;
  dbs           = g_new0 (DB_snmp, 1);
  if (dbs)
    {
      dbs->created  = g_strdup ("(not in database)");
      dbs->modified = g_strdup (dbs->created);
      d_print (DEBUG_TRACE, "Memory allocated, returning valid structure.\n");
      D_FUNC_END;
      return dbs;
    }
  d_print (DEBUG_TRACE, "Memory allocation failed. Returning NULL pointer.");
  D_FUNC_END;
  return NULL;
}
/****************************************************************************
 * snmp_destroy ()
 *
 ***************************************************************************/
void 
snmp_destroy (DB_snmp *dbs)
{
  D_FUNC_START;
  if (dbs)
    {
      if (dbs->rowid != 0)
	{
	  g_warning ("snmp_destroy: attempt to destroy an object still in "
		     "use.");
	  D_FUNC_END;
	  return;
	}
      d_print (DEBUG_TRACE, "valid structure, freeing data\n");
      if (dbs->created)        g_free (dbs->created);
      if (dbs->modified)       g_free (dbs->modified);
      if (dbs->name)           g_free (dbs->name);
      if (dbs->read_c)         g_free (dbs->read_c);
      if (dbs->write_c)        g_free (dbs->write_c);
      d_print (DEBUG_TRACE, "freeing structure.\n");
      g_free (dbs);
    }
  D_FUNC_END;
}
/****************************************************************************
 * snmp_add ()
 *
 ***************************************************************************/
void
snmp_add (DB_snmp *dbs)
{
  D_FUNC_START;
  g_return_if_fail (dbs != NULL);
  if (dbs->rowid != 0)
    {
      g_warning ("Attempted to re-add a snmp instance.");
      D_FUNC_END;
      return;
    }
  dbs->rowid = g_sqldb_highest_rowid (snmp_sqldb, "_rowid") + 1;
  d_print (DEBUG_DUMP, "dbs->rowid (%d), assigned to dbs->name (%s)\n",
	   dbs->rowid, dbs->name);
  if (dbs->created) 
    g_free (dbs->created);
  dbs->created = db_timestamp ();
  if (dbs->modified) 
    g_free (dbs->modified);
  dbs->modified = g_strdup (dbs->created);
  g_sqldb_row_add (dcsock, snmp_sqldb, dbs);
  D_FUNC_END;
}
/****************************************************************************
 * snmp_update ()
 *
 ***************************************************************************/
void 
snmp_update (DB_snmp *dbs)
{
  D_FUNC_START;
  g_assert (dbs->rowid != 0);
  if (dbs->modified) g_free (dbs->modified);
  dbs->modified = db_timestamp ();
  d_print (DEBUG_DUMP, "Updating dbs->name (%s)\n", dbs->name);
  g_sqldb_row_update (dcsock, snmp_sqldb, dbs);
  D_FUNC_END;
}
/****************************************************************************
 * snmp_delete ()
 ***************************************************************************/
void
snmp_delete (DB_snmp *dbs)
{
  D_FUNC_START;
  d_print (DEBUG_DUMP, "Deleting dbs->name (%s)\n", dbs->name);
  g_assert (g_sqldb_row_delete (dcsock, snmp_sqldb, dbs) == TRUE);
  dbs->rowid = 0;
  D_FUNC_END;
}

GList *
snmp_list (void)
{
  d_print (DEBUG_TRACE, "\n");
  return g_sqldb_table_list (snmp_sqldb);
}

DB_snmp *
snmp_find_by_rowid (guint rowid)
{
  d_print (DEBUG_TRACE, "\n");
  return g_sqldb_row_find (snmp_sqldb, "_rowid", &rowid);
}
/* EOF */



