/*
 *  $Id$
 *
 *  g_sql -- A simplified, unified interface to various SQL packages.
 *  Copyright 1999 John Schulien
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  g_sql-table.c  
 *  
 *  This file is an interface to the table-plugins
 *  and is serving SERVER and CLIENT 
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <time.h>
#include <stdlib.h>
#include <stdio.h> /*added for debugging*/
#include <glib.h>

#define G_SQL_INTERFACE
#include "g_sql.h"
#include "g_sql-table.h"

#include "debug.h"

static GList * tables      = NULL;	/* GList of tables */
static GList * table_names = NULL;	/* GList of table names */

/**
 *
 *  g_sql_enumerate_tables:    
 *
 *   Enumerate available database table support.
 *
 *  Obtain a linked list of table names
 *
 *  Return Values:
 *
 *  The return value is a GList.  Each data field in the GList points to
 *  a character string containing the name of a table.
 *
 **/
GList * 	
g_sql_enumerate_tables(void)
{
  return table_names;
}

/**********

backend uses register

***********/
/**
 * g_sql_register_table:
 *
 **/
gboolean        
g_sql_register_table(G_sql_table *dbb)
{

  D_FUNC_START;
  g_return_val_if_fail (dbb != NULL, FALSE);
  tables      = g_list_append (tables, dbb);
  table_names = g_list_append (table_names, dbb->name);
  d_print (DEBUG_TRACE, "initializing plugin (%s)\n", dbb->name);
  D_FUNC_END;
  return TRUE;
}

/**
 * 
 * g_sql_unregister_table
 *
 **/
gboolean
g_sql_unregister_table(G_sql_table *dbb)
{
  GList    * gl;
  GList    * gln;

  D_FUNC_START;
  g_return_val_if_fail (dbb != NULL, FALSE);
  d_print (DEBUG_TRACE, "Unregister %s\n", dbb->name);
  if (!(gl = g_list_find (tables, dbb)))
    {
      D_FUNC_END;
      return FALSE;
    }

  if (!(gln = g_list_find (table_names, dbb->name)))
    {
      d_print (DEBUG_TRACE, "backend not found.\n");
      D_FUNC_END;
      return FALSE;
    }

  tables      = g_list_remove_link (tables,      gl);
  table_names = g_list_remove_link (table_names, gln);

  g_list_free_1 (gl);
  g_list_free_1 (gln);
  D_FUNC_END;
  return TRUE;
}

/*  db_table_lookup()  -- get a handler to your table
 *
 *  looks up the rigth plugin to use for a table name call this first
 *  if you encounter a unknown table type and then you can start
 *  using all functions associated with the table
 *
 *  Return Values:
 *
 *  The return value is a handle to a private structure, owned by the 
 *  table backend that identifies the table, or NULL if the operation
 *  failed.
 *
 */
G_sql_table *
db_table_lookup(gint type)
{
  GList            * gl;

  gl = tables;
  while (gl)
    {
      if ((G_sql_table *)gl->data)
	{
	  if (((G_sql_table *)gl->data)->type == type)
	    {
              return (G_sql_table *)gl->data;  /*return plugin backend to use*/
	    }
	}
      gl = gl->next;
    }
  return 0;
}

/* this function is obsolete - please use db_table_lookup()
 *  g_sql_table_lookup()  -- get a handler to your table
 *
 *  looks up the rigth plugin to use for a table name call this first
 *  if you encounter a unknown table type and then you can start
 *  using all functions associated with the table
 *
 *  Return Values:
 *
 *  The return value is a handle to a private structure, owned by the 
 *  table backend that identifies the table, or NULL if the operation
 *  failed.
 *
 */
G_sql_table *
g_sql_table_lookup(gchar *tablename)
{
  GList            * gl;
  g_return_val_if_fail (tablename != NULL, NULL);

  gl = tables;
  while (gl)
    {
      if ((G_sql_table *)gl->data)
	{
	  if (!strcmp(((G_sql_table *)gl->data)->name, tablename))
	    {
              return (G_sql_table *)gl->data;  /*return plugin backend to use*/
	    }
	}
      gl = gl->next;
    }
  return 0;
}

/**
 *
 *  g_sql_encode_sql()
 *
 **/

gboolean	
g_sql_encode_sql(G_sql_table *tbh, gpointer packet, gchar *sqlstr, 
		 gint querytype, gint *n, gint tpl)
{
  return tbh->encode_sql(packet, sqlstr, querytype, n, tpl);
}

/**
 *
 *  g_sql_decode_sql()
 *
 **/

gboolean
g_sql_decode_sql (G_sql_table *tph, G_sql_query *sql, gpointer packet, 
		  guint numfields, guint *n)
{
  return tph->decode_sql(packet, sql, numfields, n);
}

/**
 *  g_sql_encode_members:
 *
 * arguments:
 * tph is which plugin to use
 * packet is result holding member records
 * data is pointer to a table like DB_host, input of data
 *
 **/
gint 
g_sql_encode_members(G_sql_table *tph, gpointer packet, gpointer data)
{
  gint n;
  D_FUNC_START;
  if(!(n = tph->encode_members(packet, data)))
    {
      D_FUNC_END;
      return 0;
    }
  D_FUNC_END;
  return n;
}
/**
 * g_sql_decode_members:
 *
 **/
gpointer 
g_sql_decode_members(G_sql_table *tph, gpointer packet, gint *n, gint plen)
{
  gpointer row;	/*holds an unknown table row*/
  D_FUNC_START;
  
  if(!(row = tph->decode_members(packet, n, plen)))
    {
      D_FUNC_END;
      return 0;
    }
  D_FUNC_END;
  return row;
}

/* g_tableinfo()
 *
 * ask question to table-plugin
 *
 * see GxSNMP_TABLEINFO_Types in gxsnmpdb_types.h
 *
 */

gpointer 
g_tableinfo(G_sql_table *tph, guint query, gpointer answer)
{
gpointer ptr;
  return tph->tableinfo(query, 0, ptr);
}

/* g_sql_add_hash()
 *
 * add data to a hashtable where the table
 * name is used as hash key.
 *
 */
gboolean
g_sql_add_hash(G_sql_table *tph, GHashTable *hash, gpointer data)
{
  return tph->add_hash(hash, data);
}

/* g_sql_remove_hash()
 *
 * remove data from a hashtable where the table
 * name is used as hash key.
 *
 */
gboolean
g_sql_remove_hash(G_sql_table *tph, GHashTable *hash, gpointer data)
{
  return tph->remove_hash(hash, data);
}

/* EOF */
