/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- An snmp management application
 *  Copyright (c) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

/* 
 * DB_hitem.c containes utility functions related to the creation and 
 * destruction of DB_hitems, and functions to manage the DB_hitem 
 * database.
 */
#include <config.h>
#include <gnome.h>
#include "g_sqldb.h"
#include "tables.h"
#include "gxsnmpdb.h"

#include "debug.h"

extern gint dcsock;

/****************************************************************************
 * hitem_create ()
 ***************************************************************************/
DB_hitem *
hitem_create (void)
{
  DB_hitem *dbi;
  D_FUNC_START;
  dbi           = g_new0 (DB_hitem, 1);
  if (dbi)
    {
      DPRT ("hitem_create: memory allocated initlizing structre.");
      dbi->created  = g_strdup ("(not in database)");
      dbi->modified = g_strdup (dbi->created);
      D_FUNC_END;
      return dbi;
    }
  DPRT ("hitem_create: unable to alloc memory for new hitem object.");
  D_FUNC_END;
  g_warning ("hitem_create unable to allocate memory, expect trouble.\n");
  return NULL;

}
/****************************************************************************
 * hitem_destroy ()
 ***************************************************************************/
void 
hitem_destroy (DB_hitem *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  if (dbi->rowid != 0)
    {
      g_warning ("hitem_destroy: attempt to destroy an object still in "
		 "use.");
      D_FUNC_END;
      return;
    }
  if (dbi)
    {
      DPRT ("hitem_destroy: valid structure, freeing data.");
      if (dbi->created)        g_free (dbi->created);
      if (dbi->modified)       g_free (dbi->modified);
      if (dbi->name)           g_free (dbi->name);
      if (dbi->group)          g_free (dbi->group);
      if (dbi->description)    g_free (dbi->description);
      if (dbi->model)          g_free (dbi->model);
      if (dbi->vendor)         g_free (dbi->vendor);
      DPRT ("hitem_destroy: freeing structure.");
      g_free (dbi);
    }
  D_FUNC_END;
}
/****************************************************************************
 * hitem_add ()
 ***************************************************************************/
void
hitem_add (DB_hitem *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  if (dbi->rowid != 0)
    {
      g_warning ("Attempted to re add a hitem with a rowid of 0");
      return ;
    }
  dbi->rowid = g_sqldb_highest_rowid (hitem_sqldb, "_rowid") + 1;
  if (dbi->created) 
    g_free (dbi->created);
  dbi->created = db_timestamp ();
  if (dbi->modified) 
    g_free (dbi->modified);
  dbi->modified = g_strdup (dbi->created);

  g_sqldb_row_add (dcsock, hitem_sqldb, dbi);
  D_FUNC_END;
}
/****************************************************************************
 * hitem_update ()
 ***************************************************************************/
void 
hitem_update (DB_hitem *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  g_assert (dbi->rowid != 0);
  if (dbi->modified) 
    g_free (dbi->modified);
  dbi->modified = db_timestamp ();
  g_sqldb_row_update (dcsock, hitem_sqldb, dbi);
  D_FUNC_END;
}
/****************************************************************************
 * hitem_delete ()
 ***************************************************************************/
void
hitem_delete (DB_hitem *dbi)
{
  D_FUNC_START;
  g_return_if_fail (dbi != NULL);
  g_return_if_fail (dbi->rowid != 0);
  g_assert (g_sqldb_row_delete (dcsock, hitem_sqldb, dbi) == TRUE);
  dbi->rowid = 0;
  D_FUNC_END;
}
/****************************************************************************
 * hitem_list ()
 ***************************************************************************/
GList *
hitem_list (void)
{
  return g_sqldb_table_list (hitem_sqldb);
}
/****************************************************************************
 * hitem_find_by_rowid ()
 ***************************************************************************/
DB_hitem *
hitem_find_by_rowid (guint rowid)
{
  g_return_val_if_fail (rowid != 0, NULL);
  return g_sqldb_row_find (hitem_sqldb, "_rowid", &rowid);
}

/* EOF */

