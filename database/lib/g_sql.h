/*
**  $Id$
**
**  gsql -- A simplified, unified interface to various SQL packages.
**  Copyright (C) 1999 John Schulien
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  g_sql.h  --  Abstract SQL interface API
*/

#ifndef __G_SQL_H__
#define __G_SQL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib.h>

/******************************************************************************
**
**  All the different types of fields that can be stored by g_sql
**  The type is stored in the two low order bytes of a G_sql_column_type.
**
******************************************************************************/

typedef enum
{
  G_SQL_INT,
  G_SQL_UINT,
  G_SQL_DOUBLE,
  G_SQL_STRING,
  G_SQL_BLOB
} 
G_sql_column_type;

/******************************************************************************
**
**  The two high order bytes of a G_sql_column_type contain flags:
**
******************************************************************************/

#define G_SQL_TYPE_MASK 0x0000ffff	/* Low 16 bits are the column type */
#define G_SQL_KEY       0x00010000	/* This field is a keyed field */
#define G_SQL_PRIMARY   0x00020000	/* This key is the primary key */
#define G_SQL_HIGHEST   0x00040000	/* Maintain highest value for col */ 

/*
**  Structure passed to g_sql_connect when connecting to a database 
*/

typedef struct _G_sql
{
  gchar *	engine;			/* Name of database engine */
  gchar *	host;			/* Host that the server resides on */
  gint          port;			/* Network port to connect to */
  gchar *	user;			/* Username to connect as */
  gchar * 	password;		/* Password to access database */
}
G_sql;

/*
**  Definition of the database access objects G_sql_connection and
**  G_sql_query.
**
**  From the application perspective, the two objects are completely
**  opaque.
*/

#ifndef G_SQL_BACKEND
#ifndef G_SQL_INTERFACE

typedef void G_sql_connection;          /* Opaque type for connection */
typedef void G_sql_query;               /* Opaque type for query */

#endif  /* G_SQL_INTERFACE */
#endif  /* G_SQL_BACKEND   */

/*
**  From the interface perspective, the objects contain only the    
**  pointer to the parent structure and the last error number.
*/

#ifndef G_SQL_BACKEND		/* Backends define their own objects */
#ifdef  G_SQL_INTERFACE         /* The interface module sees the following:  */

typedef struct _G_sql_connection	/* Open SQL connection structure */
{
  struct _G_sql_backend * backend;	/* Pointer to backend structure */
//  gint			  errno;        /* Last error on this connection */
}
G_sql_connection;

typedef struct _G_sql_query		/* Open SQL query structure */
{
  struct _G_sql_connection * connection;  /* Pointer to connection block */ 
//  gint			     errno;	  /* Last error on this query */
}
G_sql_query;

#endif /* G_SQL_INTERFACE */
#endif /* G_SQL_BACKEND */

/*
**  Structure to define an SQL database engine backend.  This structure is
**  ONLY used by g_sql.c and by the database engine backends.
*/

#if defined (G_SQL_BACKEND) || defined (G_SQL_INTERFACE)

typedef struct _G_sql_backend
{
  gchar            *    name;				/* Name of db engine */

  void	           *    private;			/* Pointer for use by */
							/* the backend        */

  gboolean           (* initialize)  			/* Initialization */  
			(struct _G_sql_backend * dbb);  /* subroutine */

  gboolean           (* terminate)			/* Termination */
			(struct _G_sql_backend * dbb);	/* subroutine */

  G_sql_connection * (* connect) 			/* Database Connect */
			(G_sql        	       * db);	/* subroutine */

  gboolean	     (* disconnect)			/* Disconnect */
			(G_sql_connection      * dbc);	/* subroutine */

  GList   	   * (* enumerate_databases)		/* List available */
		        (G_sql_connection      * dbc);  /* databases */

  gboolean	     (* select) 			/* Database select */
			(G_sql_connection      * dbc,	     /* subroutine */
		         gchar                 * database);

  G_sql_query      * (* query)   			/* Database query */
			(G_sql_connection      * dbc,       /* subroutine */
                         gchar                 * query, 
			 gint		         querylen);

  gboolean           (* free_query)			/* Free-Query */
			(G_sql_query  	       * dbq);  /* subroutine */

  gboolean           (* next_row)			/* Get next row */
			(G_sql_query	       * dbq);	/* subroutine */

  gchar            * (* field_pos)			/* Get next row */
			(G_sql_query	       * dbq,	/* subroutine */
                         gint            fieldpos);

  gchar            * (* get_field)			/* Get field */
			(G_sql_query  	       * dbq,	/* subroutine */
			 void		     * * accel,
                         gchar        	       * field,
                         gint         	       * length);
}
G_sql_backend; 
#endif

/*
**  Query accelerator structure
*/

typedef gpointer G_sql_accelerator;

/*
**  Possible error conditions
*/

enum {
  G_SQL_ERROR_NONE,
  G_SQL_ERROR_BACKEND_NOT_FOUND,
  G_SQL_INVALID_HOST,
  G_SQL_CONNECTION_REFUSED,
  G_SQL_INVALID_USER,
  G_SQL_INVALID_PASSWORD,
  G_SQL_BACKEND_INTERNAL
};

gint g_sql_errno;	/* Used for register/unregister/connect only */

/*
**  Functions available in the API.
*/

#if defined (G_SQL_BACKEND) || defined (G_SQL_INTERFACE)
gboolean           g_sql_register_backend    	(G_sql_backend     * dbb);
gboolean           g_sql_unregister_backend  	(G_sql_backend     * dbb);
#endif

GList            * g_sql_enumerate_backends  	(void);
G_sql_connection * g_sql_connect		(G_sql         	   * db);
gboolean           g_sql_disconnect 	  	(G_sql_connection  * dbc);
gboolean           g_sql_select              	(G_sql_connection  * dbc, 
				           	 gchar         	   * database);
G_sql_query      * g_sql_query               	(G_sql_connection  * dbc, 
					   	 gchar             * query,
						 gint		     querylen);
gint		   g_sql_connection_errno       (G_sql_connection  * dbc);
gint		   g_sql_query_errno		(G_sql_query       * dbq);
gboolean           g_sql_free_query          	(G_sql_query       * dbq);
gboolean           g_sql_next_row            	(G_sql_query       * dbq);
gchar            * g_sql_field_pos            	(G_sql_query       * , int );
gchar            * g_sql_field               	(G_sql_query       * dbq, 
						 G_sql_accelerator * accel,
					   	 gchar		   * field, 
					   	 gint              * length);
gboolean           g_sql_field_string        	(G_sql_query       * dbw, 
						 G_sql_accelerator * accel,
					   	 gchar 	           * field,
					   	 gchar		 * * stryng);
gboolean           g_sql_field_int           	(G_sql_query       * dbq, 
						 G_sql_accelerator * accel,
					   	 gchar             * field,
					   	 gint              * integer);
gboolean	   g_sql_field_double        	(G_sql_query       * dbq,
						 G_sql_accelerator * accel,
					   	 gchar             * field,
					   	 gdouble           * dubble);
gchar             *g_sql_timestamp              (void);
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __G_SQL_H__ */
/* EOF */
