/*
 * Common utility functions.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <glib.h>
#include <sys/time.h>
/* socket related fun */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "gxsnmpdb_types.h"
#include "debug.h"

/**
 * wordcpyt:
 * 
 * Copy a space seperated word 'w' to destination 'dst' from 'src'
 *
 **/
gchar *
wordcpyt (gchar *dst, gchar *src, gchar *tmp, gint w)
{
static int i, j;
                
  i = j = 0;
  *dst = 0;
  for(w--;w;)           /*count forward w words */
    {
      if ( *(src + i) == 0 || *(src + i) == '\n'){ /* not enough words */
        *dst = 0;
        return 0; /* Leave destination empty */
      }
      if ( *(src + i) == *tmp) w--;
      i++;
    }

  while(  ( *(src + i) != 0 && (*(src + i) != '\n')) 
       && ( *(src + i) != *tmp) )
    {
      *(dst + j) = *(src + i);    /* move each character */
      i++;
      j++;
    }
  *(dst+j) = 0;
  return dst;
}

/**
 * db_timestamp:
 *
 *  Construct a standard database timestamp.
 *
 *  This subroutine stores a timestamp in a common format suitable for
 *  inclusion in database records.
 *
 *  The format is yyyy.mm.dd hh:mm:ss\0, for a total of 20 bytes, including
 *  the NULL.
 *
 *  Return values:
 *
 *  The return value is a pointer to the buffer.
 **/

gchar *
db_timestamp ()
{
  gchar         * buf;
  struct tm     * tim;
  time_t          gm_time;
  D_FUNC_START;
  buf     = g_malloc (20);
  gm_time = time (NULL);
  tim     = localtime (&gm_time);
  strftime (buf, 20, "%Y.%m.%d %H:%M:%S", tim);
  D_FUNC_END;
  return buf;
}

/* Dummy functions -- Kludgy
 *
 * I put them here for now. so each client
 * doesn't have to define them.
 *
 */

int
gxsnmp_network_new (gpointer dummy)
{
  return 0;
}

int
gxsnmp_host_new (gpointer dummy)
{
  return 0;
}


/* EOF */
