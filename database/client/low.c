/*  $Id$
**  (c) 2000 copyright larry liimatainen
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  This is a primitive interface to the database daemon. It is provided
**  so small binaries can access the database, without having to use
**  much memory.
**  See the event display for an usage example.
** 
**  
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <string.h>
#include <stdio.h>
#include "gxsnmpdb.h"
#include "gxsnmpdbc.h"
#include "debug.h"


/* db_ip2name()
 *
 *  Convert an IP address using database info.
 *  It will first try 'Name' then 'DNS_name'
 *  Answer will be kept until next call
 *  FIX: use systems gethostbyname ?
 *  FIX: if hostname is a router maybe we should append interface info.
 *      eg: router1.domain.com:ethernet0
 *
 *  Return Values:
 *
 *  Good  --  Pointer to hostname
 *  FAIL  --  NULL
 *
 */

gchar *
db_ip2name(gint sock, gchar *ip)
{
gxobject obj;
gxobject_ctx ctx;
gxobject_db_filter filter;
void *arg[10];
gint xa, xb;
gint host_rowid;
GList *gl;
DB_interface        *dbip;
DB_host      *dbhp;
static gchar hostname[200];
gchar hostie[200];
gchar *hostie_ptr;

  hostname[0] = 0;

/*setup context */

  ctx.type = OBJ_DB_INTERFACE;
  ctx.sock = sock;
  ctx.tables = db_tables;

/* setup object */

  obj.type = OBJ_DB_INTERFACE;
  obj.ctx = &ctx;
  obj.obj = 0;

  filter.method = OBJ_FIND_SINGLE;
  filter.args = &arg;

  strcpy(hostie, ip); /* this is needed for 'gxevents', who will else bus error */
  hostie_ptr = hostie;

  xa = OBJ_DB_TABLE_INTERFACE_ADDRESS;
  arg[0] = &xa;
  arg[1] = hostie_ptr;
  arg[2] = 0;

  ctx.user = &filter;
  gxobject_db_load(&obj);

  /* OK, we got our answer in obj->obj , lets check it out */
fprintf(stderr,"A");
  xb = 0;
  gl = obj.obj;
  if(!gl){
    fprintf(stderr,"Nothing passed through the filter.\n");
    return 0;
  }
  else while(gl){
fprintf(stderr,"B");
    dbip = gl->data;
fprintf(stderr,"C");
//   printf("Data: %s\n", dbip->name);
fprintf(stderr,"D");
    xb = dbip->host;
fprintf(stderr,"E");
    gl = gl->next;
fprintf(stderr,"F");
  }
fprintf(stderr,"G");
  gl = obj.obj;
  while(gl){
    free(gl->data);
    gl = gl->next;
  }

  if(!xb){
    fprintf(stderr,"WARNING: this code, shouldn't be reached!\n");
    return 0; 			/* no IP matched, shouldn't happen */
  }

  ctx.type = OBJ_DB_HOST;
  obj.type = OBJ_DB_HOST;
  xa = OBJ_DB_TABLE_HOST_ROWID;
  arg[0] = &xa;
  arg[1] = &xb;
  arg[2] = 0;
  obj.obj = 0;

  gxobject_db_load(&obj);               /* fetch host, that has rowid in 'xb' */

  gl = obj.obj;
  if(!gl) return 0;			/* no hostname matched */
  else while(gl){
    dbhp = gl->data;
    if(dbhp->name[0] != 0)
      strcpy(hostname, dbhp->name);
    else if(dbhp->dns_name[0] != 0)
      strcpy(hostname, dbhp->name);
    gl = gl->next;
  }
  gl = obj.obj;
  while(gl){
    free(gl->data);
    gl = gl->next;
  }

return hostname;
}


/* EOF */
