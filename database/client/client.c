/* -*- Mode: C -*-
 * $Id$
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 

#include <stdio.h>
#include <glib.h>
#include "debug.h"
#include "gxsnmpdb.h"		/* include Client API */
#include "gxsnmpdbc.h"		/* include Client API */
#include "db_filters.h"
extern gxdb_tables db_tables;

int debug_level;

void
debug ()
{
}

int dcsock;

int
main (int argc, char **argv)
{
  DB_host host, *dbhp;
  DB_graph dbg;
  DB_interface dbi, *dbip;
  gchar *ip = "130.100.31.239";
  gchar *cre = "2001";
  gchar *mod = "2002";
  gchar *dns = "eremit.ericsson.se";
  gchar *ifname = "FastEthernet 0/0/0";
  gchar *name = "130.100.31.250";
  gchar *mask = "255.255.255.192";
  gchar *des = "eremit";
  gchar *con = "none";
  int sock;
  GList *gl;
  gchar *hostname;

  gxobject obj;
  gxobject_ctx ctx;
  gxobject_db_filter filter;
  gxobject_db_filter *fil;

  void *arg[10];
  void **xarg;
  int xa, xb, xc, xd;

  void *a;

  if(db_load_enviroment()){
    fprintf(stderr,"Couldn't load configuration, can't start!\n");
    exit(0);
  }

  fprintf(stderr,"Configuration listing:\n");
  fprintf(stderr,"Database daemon port: %d\n", dbconfig.ddport);
  fprintf(stderr,"Database daemon host: %s\n", dbconfig.ddhost);
  fprintf(stderr,"Table-plugin directory: %s\n", dbconfig.tabplug);

  fprintf (stderr, "loading table plugins\n");
  plugins_table_load (dbconfig.tabplug);	/*dir contains database table funcs ( shared object files ) */
  fprintf (stderr, "starting database plugins\n");
  plugins_start ();
  fprintf (stderr, "starting table plugins\n");
  plugins_table_start ();


  if ((sock = dd_connect ("127.0.0.1", dbconfig.ddport)) == -1)
    {				/*connect to database server at localhost */
      fprintf (stderr, "Couldn't connect to database daemon\n");
      exit (0);
    }

/* ROW setups */

/*
  dbg.rowid = 20345;
  dbg.created = cre;
  dbg.modified = mod;
  dbg.map = 1;
  dbg.type = 2;
  dbg.network = 3;
  dbg.details = dns;
  dbg.pixmap = nam;
  dbg.x = 1;
  dbg.y = 1;
*/

  dbi.rowid = 2;
  dbi.created = cre;
  dbi.modified = mod;
  dbi.host = 5;
  dbi.snmp = 2;
  dbi.transport = 1;
  dbi.address = ip;
  dbi.netmask = mask;
  dbi.name = ifname;
  
/* following code is trying to fetch the hostname
   using a IP address.

   1 - lookup IP in interface table.
   2 - previous returned a host rowid. use it to fetch the host.
   3 - all done

*/
  
  hostname = db_ip2name(sock, ip);
  fprintf(stderr,"Hostname: %s\n", hostname);
  hostname = db_ip2name(sock, ip);
  fprintf(stderr,"Hostname: %s\n", hostname);
  hostname = db_ip2name(sock, ip);
  fprintf(stderr,"Hostname: %s\n", hostname);
  hostname = db_ip2name(sock, ip);
  fprintf(stderr,"Hostname: %s\n", hostname);
  hostname = db_ip2name(sock, ip);
  fprintf(stderr,"Hostname: %s\n", hostname);

  goto quit;

/*setup context */

  ctx.type = OBJ_DB_INTERFACE;
  ctx.sock = sock;
  ctx.tables = db_tables;

/*setup object */

  obj.objnum = 0;
  obj.type = OBJ_DB_INTERFACE;
  obj.ctx = &ctx;
 
  filter.method = OBJ_FIND_SINGLE;
  filter.args = &arg;

  xa = OBJ_DB_TABLE_INTERFACE_ADDRESS;
  arg[0] = &xa;
  arg[1] = ip;
  arg[2] = 0;

  /* *((gint *) db_graph_find_network.args[0]) = 32; */

  ctx.user = &filter;
  gxobject_db_load(&obj);

  /* OK, we got our answer in obj->obj , lets check it out */

  xb = 0;
  gl = obj.obj;
  if(!gl){
    fprintf(stderr,"Nothing passed through the filter.\n");
  }
  else while(gl){
    dbip = gl->data;
    printf("Data: %s\n", dbip->name);
    xb = dbip->host;
    gl = gl->next;
  }
  if(!xb) exit(-1); /*IP not found in interface table, or error */
  ctx.type = OBJ_DB_HOST;
  obj.type = OBJ_DB_HOST;

  xa = OBJ_DB_TABLE_HOST_ROWID;
  arg[0] = &xa;
  arg[1] = &xb;
  arg[2] = 0;
  obj.obj = 0;

fprintf(stderr,"A");
  gxobject_db_load(&obj);		/* fetch host, that has rowid in 'xb' */

fprintf(stderr,"B");

  if(!obj.obj){
    fprintf(stderr,"Nothing passed through the filter.\n");
  }
  else{ 
    gl = obj.obj;
    while(gl){
      dbhp = gl->data;
      printf("name    : %s\n", dbhp->name);
      printf("dns_name: %s\n", dbhp->dns_name);
      gl = gl->next;
    }
  }

quit:

exit(0);
  dd_disconnect (sock);
  return 0;
}
