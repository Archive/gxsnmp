/*
**  $Id$
**
**  GXSNMP -- An snmp mangament application
**  Copyright (C) 1998 Gregory McLean
** 
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
** 
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Database interface utility functions
*/
#include <config.h>
#include <stdlib.h>
#include <glib.h>
#include <time.h>
#include "debug.h"
#define COMPILING_DB_INTERFACE_C 
#include "db_interface.h"


/****************************************************************************
**
**  db_timestamp ()  --  Construct a standard database timestamp
**
**  This subroutine stores a timestamp in a common format suitable for
**  inclusion in database records.
**
**  The format is yyyy.mm.dd hh:mm:ss\0, for a total of 20 bytes, including
**  the NULL.
** 
**  Return values:
**
**  The return value is a pointer to the buffer.
**
*****************************************************************************/

gchar *
db_timestamp ()
{
  gchar		* buf;
  struct tm     * tim;
  time_t          gm_time;
  D_FUNC_START;
  buf     = g_malloc (20);
  gm_time = time (NULL);
  tim     = localtime (&gm_time);
  strftime (buf, 20, "%Y.%m.%d %H:%M:%S", tim);
  D_FUNC_END;
  return buf;
}

/* EOF */


