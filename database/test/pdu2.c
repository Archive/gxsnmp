#include <stdio.h>
#include <glib.h>
#include "gxdd.h"

void debug(){
}


static void decode_member(gint *n, void *packet, char *buffer){
gint ml, mt, md;
 
  memcpy(&ml, packet+*n, sizeof(gint));     /*get MemberLen*/
  *n += sizeof(gint);

  memcpy(&mt, packet+*n, sizeof(gint));     /*get MemberType*/
  *n += sizeof(gint);
  switch(mt){
    case PDUMEMTYPE_INT:
      memcpy(&md, packet+*n, sizeof(gint));
      *n += sizeof(gint);
      sprintf(buffer, "%d", md);
      break;
    case PDUMEMTYPE_STR:
      ml -= sizeof(gint) * 2;
      memcpy(buffer, packet+*n, &ml);
      buffer[ml] = 0;
      break;
    default:     /*decode error*/
      fprintf(stderr,"FATAL: APP_ERR packet decode error, undefined struct member\n"); /*try give some output even it may be worthless*/
      exit(0); /*exit, else we could loop forever*/
  }
}

void decode_to_sql(void * packet, void *sqlstr, gint querytype){
gint plen,n,i,j, mdi;
gchar mds[2000];


  memcpy(plen, packet, sizeof(gint));			/*get total packet length*/
debug();
fprintf(stderr,"Total packet len: %d\n", plen);
  n = sizeof(gint);

  switch(querytype){
    case SQL_INSERT:
      while(n <= plen){
        decode_member(&n, packet, mds);
      }
      
      break;
  }
}
