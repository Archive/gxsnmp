/*
**  $Id$
**
**  GXSNMP -- An snmp management application
**  Copyright (c) 1998 Gregory McLean
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
*/

/*
**  DB_host.c contains utility functions related to the creation and destruction
**  of DB_host items, and functions used to manage the DB_host database.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>
#include "g_sqldb.h"
#include "tables.h"
#include "DB_host.h"
#include "db_interface.h"
#include "interface.h"
#include "graph.h"
#include "debug.h"
/****************************************************************************
 * host_create ()
 ***************************************************************************/
DB_host * 
host_create (void)
{
  DB_host * dbh;
  D_FUNC_START;
  dbh		= g_new0 (DB_host, 1);
  if (dbh)
    {
      dbh->created  = g_strdup ("(not in database)");
      dbh->modified = g_strdup (dbh->created);
      DPRT ("host_create: valid host instance created.");
      D_FUNC_END;
      return dbh;
    }
  DPRT ("host_create: NULL being returned.");
  D_FUNC_END;
  return NULL;
}

/******************************************************************************
**
**  Function to destroy a DB_host object.  The rowid must be zero, which
**  marks it as being "not in the database"
**
******************************************************************************/

void
host_destroy (DB_host * dbh)
{
  D_FUNC_START;
  if (dbh)
   {
     DPRT ("host_destroy: valid dbh.");
     g_assert (dbh->rowid == 0);
     DPRT ("host_destroy: freeing datum.");
     if (dbh->created)     g_free (dbh->created);
     if (dbh->modified)    g_free (dbh->modified);
     if (dbh->dns_name)    g_free (dbh->dns_name);
     if (dbh->name)        g_free (dbh->name);
     if (dbh->description) g_free (dbh->description);
     if (dbh->contact)     g_free (dbh->contact);
     DPRT ("host_destroy: freeing structure.");
     g_free (dbh);
   }
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to add a DB_host object to the database.  The rowid must be
**  zero, meaning that the item is not already in the database.
**
******************************************************************************/

void
host_add (DB_host * dbh)
{
  D_FUNC_START;
  if (!(dbh))
    {
      DPRT ("host_add: got handed a NULL.");
      D_FUNC_END;
      return;
    }
  if (dbh->rowid != 0)
    {
      g_warning ("Attempting to re-add a host which is in the db.");
      D_FUNC_END;
      return;
    }
  dbh->rowid = g_sqldb_highest_rowid (host_sqldb, "_rowid") + 1;

  if (dbh->created) g_free (dbh->created);
  dbh->created  = db_timestamp ();

  if (dbh->modified) g_free (dbh->modified);
  dbh->modified = g_strdup (dbh->created);

  g_sqldb_row_add (host_sqldb, dbh);
/*
  if(mysql_gxsnmp_add_host(dbh) == -1){
    g_warning("Couldn't add object");
    return;
  }
*/

  D_FUNC_END;
}

/******************************************************************************
**
**  Function to update a DB_host object in the database.  The rowid must be
**  non-zero, meaning that the item is already in the database.
**
******************************************************************************/

void
host_update (DB_host * dbh)
{
  D_FUNC_START;
  if (dbh)
    {
      g_assert (dbh->rowid != 0);
      
      if (dbh->modified) g_free (dbh->modified);
      dbh->modified = db_timestamp ();
      
      g_sqldb_row_update (host_sqldb, dbh);
    }
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to delete a DB_host object from the database.  The rowid must
**  be non-zero, meaning that the item is already in the database.
**
**  When we delete a DB_host object from the database, we must also delete 
**  any DB_graph entries which describe this host on a map.  Interfaces
**  owned by the host are unassigned and become orphaned ... unassigned to
**  any host.
**
**  When we finished, we set the rowid to zero, meaning that the item is
**  no longer in the database, and return.  The caller is responsible for
**  making a call to host_destroy to free the memory associated with the
**  DB_host block.
**
******************************************************************************/

void
host_delete (DB_host * dbh)
{
  GList        * gl;
  DB_interface * dbi;
  DB_graph     * dbg;
  D_FUNC_START;
  g_return_if_fail (dbh != NULL);
  g_assert (dbh->rowid != 0);
  
  while ((gl = dbh->DB_interfaces))	/* Assignment intended */
    {
      dbi = (DB_interface *) gl->data;
      g_assert (dbi->host == dbh->rowid);
      interface_delete (dbi);
      interface_update (dbi);
    }

  while ((gl = dbh->DB_graphs))		/* Assignment intended */
    {
      dbg = (DB_graph *) gl->data;
      g_assert (dbg->type == DB_GRAPH_HOST);
      g_assert (dbg->host == dbh->rowid);
/*
      graph_delete  (dbg);
      graph_destroy (dbg);
*/
    }

  g_assert (g_sqldb_row_delete (host_sqldb, dbh) == TRUE); 

  dbh->rowid = 0;
  D_FUNC_END;
}

/******************************************************************************
**
**  Function to return the master host GList
**
******************************************************************************/

GList *
host_list (void)
{
  return g_sqldb_table_list (host_sqldb);
}

/******************************************************************************
**
**  Function to locate a host by the database rowid
**
******************************************************************************/

DB_host *
host_find_by_rowid (guint rowid)
{
  return g_sqldb_row_find (host_sqldb, "_rowid", &rowid);
}

/******************************************************************************
**
**  Function to locate a host by map name
**
**  Since g_sqldb_row_find is broken for string keys, just do a quick search
**  for now.
**
******************************************************************************/

DB_host *
host_find_by_name (gchar * name)
{
  GList * gl;
  D_FUNC_START;
  g_return_val_if_fail (name != NULL, NULL);
  gl = g_sqldb_table_list (host_sqldb);
  while (gl)
    {
      DB_host * dbh;
 
      dbh = (DB_host *) gl->data;
      if (!strcmp (dbh->name, name))
	{
	  DPRT ("host_find_by_name: FOUND!");
	  D_FUNC_END;
	  return dbh;
	}
      gl = gl->next;
    }
  DPRT ("host_find_by_name: NOT FOUND!");
  D_FUNC_END;
  return NULL;
}

/* EOF */

