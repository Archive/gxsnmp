#include "g_sqldb.h"
#include "tables.h"

extern int dd_connect(char *);
extern void dd_disconnect(int);
extern gint dd_host_ip2name(int, DB_host *, gchar *);
extern gint host_add(int, DB_host *);

extern int dd_write(int, int, char *);

extern int g_sqldb_row_add(int, G_sqldb_table *, gpointer);
extern gboolean g_sqldb_table_load(G_sqldb_table *);
extern gpointer g_sqldb_row_find(G_sqldb_table *, gchar *, gpointer);
