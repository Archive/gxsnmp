/*   
 * These functions compiles/decodes SQL tables to/from structures 
 * to send as packets over a network
 *
 * protocol structure of each struct member in the packet:
 *
 * MemberLen MemberType MemberData
 *
 */
#include <stdio.h>
#include <glib.h>
#include "g_sqldb.h"
#include "tables.h"

#include "gxdd.h"

/* compile_struct
 * adds members in a struct to a packet
 * returns how long into the packet is has processed
 */

static void add_guint(gint *n, void *packet, guint data){
gint tmp;

      tmp = sizeof(guint) * 3;				/* MemberLength is set to Len of itself + MemberType + MemberData*/
      memcpy(packet+*n, &tmp, sizeof(guint));		/* cpy MemberLength */
      *n += sizeof(guint);
      tmp = PDUMEMTYPE_INT;
      memcpy(packet+*n, &tmp, sizeof(guint));		/* cpy MemberType */
      *n += sizeof(guint);
      memcpy(packet+*n, &data, sizeof(guint));		/* cpy MemberData */
      *n += sizeof(guint);
}

static void add_asciiz(gint *n, void *packet, void *str){
gint j, tmp;

fprintf(stderr,"ASCIIZ: %s\n", str);
      j = strlen(str);
      tmp = sizeof(guint) * 2;				/* MemberLength is set to Len of itself + MemberType */
      tmp += j;
      memcpy(packet+*n, &tmp, sizeof(guint));		/* cpy MemberLength */
      *n += sizeof(guint);
      tmp = PDUMEMTYPE_STR;
      memcpy(packet+*n, &tmp, sizeof(guint));		/* cpy MemberType */
      *n += sizeof(guint);
      memcpy(packet+*n, str, j);			/* cpy MemberData */
      *n += j;
}

/*
 * packet is space to hold packet, this is offset to members 
 * data is where members is held in structures
 * tabletype is structure type data is
 * len is offset so far, so we can compile total packet length
 */

gint compile_struct(void *packet, void *data, gint tabletype, gint len){
gint n, t;

  n = 0;             /*this is rigth after packet header: "TAR host "*/
  n += sizeof(gint); /*add space to hold total length*/
  switch(tabletype){
    case TBL_DB_HOST:
      add_guint(&n, packet, ((DB_host *)data)->rowid);
      add_asciiz(&n, packet, ((DB_host *)data)->created);
      add_asciiz(&n, packet, ((DB_host *)data)->modified);
      add_asciiz(&n, packet, ((DB_host *)data)->dns_name);
      add_asciiz(&n, packet, ((DB_host *)data)->name);
      add_asciiz(&n, packet, ((DB_host *)data)->description);
      add_asciiz(&n, packet, ((DB_host *)data)->contact);
      add_asciiz(&n, packet, "");				/* SQL struct TAGS is set to empty */
      break;
  }
  len += n; 
  memcpy(packet, &len, sizeof(gint)); 				/*set total packet length*/
  return n;
}
