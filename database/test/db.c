#define G_SQL_INTERFACE

#include <glib.h>
#include <stdio.h>
#include "g_sql.h"
#include "plugins.h"


extern G_sql_connection *dbhandler;

gint db_host_ip2name(gchar *ip, gchar *name){
G_sql_query *dbq;
gint accel = 0;
G_sql_accelerator *acc = &accel;
gchar *data;
gint len;


  dbq = g_sql_query(dbhandler, "SELECT * FROM host", 18);
  for(;;){
    if(!g_sql_next_row(dbq)){
      g_free(dbq);
      return -1;
    }
    data = dbq->connection->backend->get_field(dbq, acc, "name", &len);
    if(!strcmp(data,ip)){
      fprintf(stderr,"ok we got a match!\n");
      accel = 0;
      data = dbq->connection->backend->get_field(dbq, acc, "dns_name", &len);
      strcpy(name, data);
      g_free(dbq);
      return 0;
    }
  }
  return -1; /*shouldn't get here*/
}
