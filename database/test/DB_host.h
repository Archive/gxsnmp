/*
**  $Id$
**  GXSNMP -- An snmp management application
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
**
**  Functions for dealing with DB_host objects.
*/

#ifndef __DB_HOST_H__
#define __DB_HOST_H__


/******************************************************************************
**
**  The public API for manipulating DB_host objects
**
******************************************************************************/

DB_host  * host_create  	(void);
void       host_destroy 	(DB_host * dbh);
void       host_add     	(DB_host * dbh);
void       host_update  	(DB_host * dbh);
void       host_delete  	(DB_host * dbh);
GList	 * host_list		(void);
DB_host  * host_find_by_rowid 	(guint     rowid);
DB_host  * host_find_by_name    (gchar   * name);

#endif

/* EOF */
