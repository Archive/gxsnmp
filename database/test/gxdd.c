#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>
#include <gmodule.h>
#include <mysql.h>

#define G_SQL_INTERFACE

#include "dae.h"
#include "g_sql.h"
#include "g_sqldb.h"
#include "tables.h"
#include "plugins.h"

gboolean             tcp_new_connection           (GIOChannel     *source,  GIOCondition   condition, gpointer       data);
gboolean             tcp_client_activity          (GIOChannel     *source, GIOCondition   condition, gpointer       data);

char version[] = "$Id$"; /*usefull to 'strings' the binary*/

/*  GLOBALS	*/

gchar bufme[1024];

glb_enviroment enviroment;
int currt;						/*holds current time() */
FILE *fpgxdd;
aclist *trapfilter;
G_sql_connection *dbhandler;

int main(){
char raw[4000];
char foo[1000],foo2[80];
int evdpysrvdsock;
GMainLoop    *loop;

/*FOOBARS*/
GList *gl;
G_sql_query *dbq;
G_sql_query *q1;
G_sql_query *q2;
char footmp[200];
char *foot;
G_sql_accelerator acc;
int accel = 0;
gchar *data;
int len;

G_sql dbsinfo = {
  "MySQL",
  "130.100.31.248",
  3306,
  "root",
  ""
};



/*daemonize*/
/*
  if(fork() != 0) _exit(0);
  if(setsid() == -1) _exit(0);
  if(fork() != 0) _exit(0);
  umask(777);
  if(chdir("/")<0) _exit(0);
  for(i=sysconf(_SC_OPEN_MAX),j=0;j<i;) close(j++);
  open("/dev/null",O_RDWR); dup(0); dup(0);
*/
/***********/

  load_enviroment();
  fpgxdd = logopen("gxdd.log");					/*start logging*/
//  unlink(enviroment.gxdd);			/*currently we are only listening on a file socket*/
//  if(!(evdpysrvdsock = createsocket(enviroment.gxdd,0))) _exit(0);	/*trap input socket*/
  fprintf(stderr, "gxdd started.\n");
  logstr(fpgxdd,"Gxdd started.\n");
fprintf(stderr,"loading plugins\n");
  plugins_load("/root/plug/"); /*this directory contains database backends ( shared object files )*/
fprintf(stderr,"starting plugins\n");
  plugins_start();

  dbhandler = g_sql_connect(&dbsinfo); /*libgxsnmp function takes a *G_sql and returns a *G_sql_connection */
  if(!dbhandler){
    fprintf(stderr,"Failed to connect to database\n");
    exit(0);
  }
  fprintf(stderr,"connected to database with handler: %d\n", dbhandler);  /*((G_sql_backend *)(dbhandler))->connect (db)*/
  g_sql_select(dbhandler, "gxsnmp");

  tcp_socket_init (4000);
  loop = g_main_new (TRUE);
  while (g_main_is_running (loop)) g_main_run (loop);
  g_main_destroy (loop);
  return 0;
}
