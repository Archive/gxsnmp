/*
 * $Id$
 * 
 * Main function
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

int 
main (int argc, char **argv)
{
  GMainLoop    *loop;

  tcp_socket_init (4000);
  loop = g_main_new (TRUE);

  while (g_main_is_running (loop))
    g_main_run (loop);

  g_main_destroy (loop);
  return 0;
}
