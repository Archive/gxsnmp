/* -*- Mode: C -*-
 * $Id$
 *
 * GXSNMP -- An snmp mangament application
 * Copyright 1998,1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */ 
#ifndef __GXSNMP_REQUEST_H__
#define __GXSNMP_REQUEST_H__
BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros
 **/
#define GXSNMP_TYPE_REQUEST               (gxsnmp_request_get_type ())
#define GXSNMP_REQUEST(obj)               (GTK_CHECK_CAST ((obj), GXSNMP_TYPE_REQUEST, GxSNMPRequest))
#define GXSNMP_REQUEST_CLASS(klass)       (GTK_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_REQUEST, GxSNMPRequestClass))
#define GXSNMP_IS_REQUEST(obj)            (GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_REQUEST))
#define GXSNMP_IS_REQUEST_CLASS(klass)    (GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_REQUEST))
/****************************************************************************
 * Control Blocks
 **/
typedef struct _GxSNMPRequest             GxSNMPRequest;
typedef struct _GxSNMPRequestClass        GxSNMPRequestClass;
struct _GxSNMPRequest
{
  GtkObject                object;
};
struct _GxSNMPRequestClass
{
  GtkObjectClass            class;
};

GtkWidget *
gxsnmp_request_new (gchar *target, gchar *subtarget, gint transport,
                    gint version, gpointer security, gint timeout,
		                        gint retries, gpointer data);
END_GNOME_DECLS
#endif
/* EOF */

