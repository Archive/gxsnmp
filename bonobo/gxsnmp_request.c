/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The SNMP request object
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "debug.h"
#include "gxsnmp_request.h"

/****************************************************************************
 * Static data
 ***************************************************************************/

/****************************************************************************
 * Local Struct
 ***************************************************************************/

/****************************************************************************
 * Forward declarations and callback functions  
 ***************************************************************************/
static void	 request_class_init	  (GxSNMPRequestClass	     *klass);
static void	 request_init		  (GxSNMPRequest	     *request);
/****************************************************************************
 * Class registration
 ***************************************************************************/
/**
 * gxsnmp_request_get_type:
 *
 * Registers class with GTK and returns handle ("type") of class. The handle
 * is cached in a static class variable for repeated calls of this function.
 *
 * Return value: unique type
 **/
GtkType
gxsnmp_request_get_type ()
{
  static GtkType list_type = 0;
  D_FUNC_START;
  if (!list_type)
    {
      GtkTypeInfo list_info = 
      {
	"GxSNMPRequest",
	sizeof (GxSNMPRequest),
	sizeof (GxSNMPRequest),
	(GtkClassInitFunc) request_class_init,
	(GtkObjectInitFunc) request_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      list_type =gtk_type_unique(gtk_object_get_type(), &list_info);
    }
  D_FUNC_END;
  return list_type;
}
/****************************************************************************
 * Static Helper functions
 ***************************************************************************/

/****************************************************************************
 * Class methods
 ***************************************************************************/
/**
 * request_class_init:
 * @klass: self 
 *
 * Constructor of this class.
 *
 **/
static void
request_class_init (GxSNMPRequestClass *klass)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * Object methods
 ***************************************************************************/
/**
 * request_init:
 * @dialog: self 
 *
 * Constructor of this object.
 *
 **/
static void
request_init (GxSNMPRequest *request)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * Object signals
 ***************************************************************************/
/****************************************************************************
 * Public API
 ***************************************************************************/
/**
 * gxsnmp_request_new:
 *
 * Creates new SNMP request object
 *
 * Return value: SNMP request object
 **/
GtkWidget *
gxsnmp_request_new (gchar *target, gchar *subtarget, gint transport,
		    gint version, gpointer security, gint timeout,
		    gint retries, gpointer data)
{
  D_FUNC_START;
  D_FUNC_END;
  return NULL;
}
/* EOF */
