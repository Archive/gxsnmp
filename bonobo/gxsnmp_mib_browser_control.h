/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 * 
 * The mib browser panel.
 */

#ifndef __GXSNMP_MIB_BROWSER_H__
#define __GXSNMP_MIB_BROWSER_H__
#include <gnome.h>
#include <bonobo.h>
#include "gxsnmp_request.h"

BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros 
 **/
#define GXSNMP_TYPE_MIB_BROWSER            (gxsnmp_mib_browser_get_type ())
#define GXSNMP_MIB_BROWSER(obj)            (GTK_CHECK_CAST ((obj), GXSNMP_TYPE_BROWSER, GxSNMPMibBrowser))
#define GXSNMP_MIB_BROWSER_CLASS(klass)    (GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_BROWSER, GxSNMPMibBrowserClass))
#define GXSNMP_IS_MIB_BROWSER(obj)         (GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_MIB_BROWSER)
#define GXSNMP_IS_MIB_BROWSER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MIB_BROWSER)
/****************************************************************************
 * Control blocks for the widget class and for widget instances 
 **/
typedef struct _GxSNMPMibBrowser             GxSNMPMibBrowser;
typedef struct _GxSNMPMibBrowserClass        GxSNMPMibBrowserClass;

struct _GxSNMPMibBrowser
{
  GtkVBox        vbox;           /* Parent Object */
  GtkWidget     *root;           /* root of the MIB tree */
  GtkWidget     *mib_desc;       /* the description of the mib */
  GtkWidget     *mib_type;       /* Type of this mib */
  GtkWidget     *mib_module;     /* Module this oid is defined in */
  GtkWidget     *mib_status;     /* Status of this oid */
  GtkWidget     *mib_oid;        /* the oid of this mib */
  GtkWidget     *mib_label;      /* the textual representation of a mib */
  GtkWidget     *mib_value;      /* value for SNMP read / write operation */
  GtkWidget     *wbutton;        /* WALK button */
  GtkWidget     *tbutton;        /* TABLE button */
  GtkWidget     *gbutton;        /* GET button */
  GtkWidget     *pbutton;        /* PUT button */
  gint           dragged_row;	 /* Dragged Row */
  GxSNMPRequest *request;	 /* SNMP Request */
};

struct _GxSNMPMibBrowserClass
{
  GtkVBoxClass    parent_class;
};
/****************************************************************************
 * Public Widget functions
 ***************************************************************************/
GtkType        gxsnmp_mib_browser_get_type               (void);
GtkWidget      *gxsnmp_mib_browser_new                   (void);

#endif

/* EOF */
