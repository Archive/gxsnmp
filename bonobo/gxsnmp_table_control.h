/* -*- Mode: C -*-
 *  $Id$
 *  GXSNMP -- An snmp managament application
 *  Copyright (C) 1998 Gregory McLean
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  SNMP table dialog
 */
#ifndef __GXSNMP_TABLE_DIALOG_H__
#define __GXSNMP_TABLE_DIALOG_H__

#include <glib.h>
#include <gnome.h>
#include <smi.h>

/*
 * DOC: gxsnmp SNMP table dialog
 */

BEGIN_GNOME_DECLS
/****************************************************************************
 *  Standard widget macros
 **/
#define GXSNMP_TYPE_TABLE_DIALOG            (gxsnmp_table_dialog_get_type())
#define GXSNMP_TABLE_DIALOG(obj)            (GTK_CHECK_CAST ((obj), GXSNMP_TYPE_TABLE_DIALOG, GxSNMPTableDialog))
#define GXSNMP_TABLE_DIALOG_CLASS(klass)    (GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_TABLE_DIALOG, GxSNMPTableDialogClass))
#define GXSNMP_IS_TABLE_DIALOG(obj)         (GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_TABLE_DIALOG))
#define GXSNMP_IS_TABLE_DIALOG_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_TABLE_DIALOG))
/*****************************************************************************
 *  Control blocks for the widget class and for widget instances
 **/
typedef struct _GxSNMPTableDialogClass   GxSNMPTableDialogClass;
typedef struct _GxSNMPTableDialog 	 GxSNMPTableDialog;
struct _GxSNMPTableDialog
{
  GnomeDialog       dialog;		/* The GxSNMPTableDialog base widget */
  GtkWidget        *clist;		/* The clist widget */
  GtkWidget        *scrolled_win;	/* Scroll widget (for clist) */
  GtkWidget        *label;		/* Title of table */
  GSList	   *colinfo;		/* The column definitions */
  gchar		   *title;		/* The title */
}; 
struct _GxSNMPTableDialogClass
{
  GnomeDialogClass  dialog_class;   	/* The GxSNMPTableDialog base class */

  void (* update)(GxSNMPTableDialog *dialog);
};

/****************************************************************************
 *
 *  Public widget manipulation functions
 *
 **/
GtkType	           gxsnmp_table_get_type		(void);
GtkWidget          *gxsnmp_table_new			(GSList *colinfo,
							 gchar *title);
GtkWidget          *gxsnmp_table_new_from_node		(SmiNode *node);

END_GNOME_DECLS

#endif

/* EOF */

