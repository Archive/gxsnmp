//#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo.h>
#include <smi.h>

#include "gxsnmp_mib_browser_control.h"
#include "gxsnmp_table_control.h"

/***************************************************************************
 * Local functions.
 **/
void              debug_file_cb           (poptContext        con,
                                           enum poptCallbackReason reason,
                                           const struct poptOption * opt,
                                           const char         *arg,
                                           const void         *data);
/**
 * popt table
 **/
poptContext      pctx;
int              debug_level  = 0;
GHashTable      *debug_files = NULL;
static gchar    *dummy       = NULL;
struct poptOption options[] = {
  { NULL, '\0', POPT_ARG_CALLBACK, debug_file_cb, NULL, NULL, NULL },
  { "debug-level", '\0', POPT_ARG_INT, &debug_level, 0,
    N_("Specify the level of debugging messages that will be output."),
    N_("DEBUG_LEVEL") },
  { "debug-filename", '\0', POPT_ARG_STRING, &dummy, 0,
    N_("Specify the filename for which debugging messages will be output (can "
       "be specified more than once)."),
    N_("DEBUG_FILENAME") },
  { NULL, '\0', 0, NULL, 0 }
};

void
debug_file_cb (poptContext con, enum poptCallbackReason reason,
               const struct poptOption *opt, const char *arg,
               const void *data)
{
  if (!strcmp(opt->longName, "debug-filename"))
    {
      if (!debug_files)
	debug_files = g_hash_table_new(g_str_hash, g_str_equal);
      g_hash_table_insert(debug_files, arg, data);
    }
}

static void
init_bonobo (int argc, char **argv)
{ 
  CORBA_Environment ev;
  CORBA_ORB orb;

  CORBA_exception_init (&ev);
  gnome_CORBA_init_with_popt_table ("gxsnmp-control-factory", "0.0",
    &argc, argv, options, 0, &pctx, GNORBA_INIT_SERVER_FUNC, &ev);
  orb = gnome_CORBA_ORB ();
  poptFreeContext (pctx);
  if (bonobo_init (orb, NULL, NULL) == FALSE)
    g_error (_("Could not initialize Bonobo"));
}

int
main (int argc, char **argv)
{
  init_bonobo (argc, argv);
  gxsnmp_mib_browser_factory_init ();
  gxsnmp_table_factory_init ();
  smiInit ("gxsnmp");
  bonobo_main ();
  return 0;
}
