/*  GTK - The GIMP Toolkit
**
**  gtk_database.c -- A gtk object type for database-backed data objects.
**
**  Copyright 1999 John Schulien & The University of Illinois at Chicago
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Library General Public
**  License as published by the Free Software Foundation; either
**  version 2 of the License, or (at your option) any later version.
**
**  This library is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
**  Library General Public License for more details.
**
**  You should have received a copy of the GNU Library General Public
**  License along with this library; if not, write to the
**  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
**  Boston, MA 02111-1307, USA.
*/

#ifndef __GTK_DATABASE_H__
#define __GTK_DATABASE_H__

#include <gtk/gtkobject.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_TYPE_DATABASE	            (gtk_database_get_type ())
#define GTK_DATABASE(obj)	            (GTK_CHECK_CAST ((obj), \
				             GTK_TYPE_DATABASE, \
				             GtkDatabase))
#define GTK_DATABASE_CLASS(klass)           (GTK_CHECK_CLASS_CAST ((klass), \
				             GTK_TYPE_DATABASE, \
				             GtkDatabaseClass))
#define GTK_IS_DATABASE(obj)	            (GTK_CHECK_TYPE ((obj), \
				             GTK_TYPE_DATABASE))
#define GTK_IS_DATABASE_CLASS(klass)        (GTK_CHECK_CLASS_TYPE ((klass), \
				             GTK_TYPE_DATABASE))

#define GTK_TYPE_DATABASE_ITEM              (gtk_database_item_get_type ())
#define GTK_DATABASE_ITEM(obj)              (GTK_CHECK_CAST ((obj), \
                                             GTK_TYPE_DATABASE_ITEM, \
					     GtkDatabaseItem))
#define GTK_DATABASE_ITEM_CLASS(klass)	    (GTK_CHECK_CLASS_CAST ((klass), \
                                             GTK_TYPE_DATABASE_ITEM, \
				             GtkDatabaseItemClass))
#define GTK_IS_DATABASE_ITEM(obj)           (GTK_CHECK_TYPE ((obj), \
                                       	     GTK_TYPE_DATABASE_ITEM))
#define GTK_IS_DATABASE_ITEM_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), \
                                             GTK_TYPE_DATABASE_ITEM))

typedef struct _GtkDatabase          GtkDatabase;
typedef struct _GtkDatabaseClass     GtkDatabaseClass;
typedef struct _GtkDatabaseItem      GtkDatabaseItem;
typedef struct _GtkDatabaseItemClass GtkDatabaseItemClass;

struct _GtkDatabase
{
  GtkObject object;
};

struct _GtkDatabaseClass
{
  GtkObjectClass parent_class;
};

struct _GtkDatabaseItem
{
  GtkObject             object;
  struct _GtkDatabase * database;
};

struct _GtkDatabaseItemClass
{
  GtkObjectClass parent_class;
  gboolean       (* add)    (GtkDatabaseItem * item);
  gboolean	 (* update) (GtkDatabaseItem * item);
  gboolean	 (* delete) (GtkDatabaseItem * item);
};

GtkType gtk_database_get_type (void);
GtkType gtk_database_item_get_type (void);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_DATABASE_H__ */
