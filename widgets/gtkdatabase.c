/*  GTK - The GIMP Toolkit
**
**  gtk_database.c -- A gtk object type for database-backed data objects.
**
**  Copyright 1999 John Schulien & The University of Illinois at Chicago
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Library General Public
**  License as published by the Free Software Foundation; either
**  version 2 of the License, or (at your option) any later version.
**
**  This library is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
**  Library General Public License for more details.
**
**  You should have received a copy of the GNU Library General Public
**  License along with this library; if not, write to the
**  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
**  Boston, MA 02111-1307, USA.
*/

#include "gtkdatabase.h"
#include <gtk/gtksignal.h>

enum {
  DATABASE_LAST_SIGNAL
};

enum {
  DATABASE_ITEM_LAST_SIGNAL
};

static void gtk_database_class_init      (GtkDatabaseClass     * klass);
static void gtk_database_init		 (GtkDatabase          * database);

static void gtk_database_item_class_init (GtkDatabaseItemClass * klass);
static void gtk_database_item_init	 (GtkDatabaseItem      * item);

static guint database_signals[DATABASE_LAST_SIGNAL]           = { 0 };
static guint database_item_signals[DATABASE_ITEM_LAST_SIGNAL] = { 0 };

/******************************************************************************
**
**  _get_type subroutine for GtkDatabase objects
**
******************************************************************************/

GtkType
gtk_database_get_type (void)
{
  static GtkType database_type = 0;

  if (!database_type)
    {
      static const GtkTypeInfo database_info =
      {
	"GtkDatabase",
	sizeof (GtkDatabase),
	sizeof (GtkDatabaseClass),
	(GtkClassInitFunc) gtk_database_class_init,
	(GtkObjectInitFunc) gtk_database_init,
	/* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };
      database_type = gtk_type_unique (gtk_object_get_type (), &database_info);
    }
  return database_type;
}

/******************************************************************************
**
**  _get_type subroutine for GtkDatabaseItem objects
**
******************************************************************************/

GtkType
gtk_database_item_get_type (void)
{
  static GtkType database_item_type = 0;

  if (!database_item_type)
    {
      static const GtkTypeInfo database_item_info =
      {
        "GtkDatabaseItem",
        sizeof (GtkDatabaseItem),
        sizeof (GtkDatabaseItemClass),
        (GtkClassInitFunc) gtk_database_item_class_init,
        (GtkObjectInitFunc) gtk_database_item_init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      database_item_type = gtk_type_unique (gtk_object_get_type (), 
					    &database_item_info);
    }
  return database_item_type;
}

/******************************************************************************
**
**  Class initialization routine for GtkDatabase objects
**
******************************************************************************/

static void
gtk_database_class_init (GtkDatabaseClass *class)
{
  GtkObjectClass *object_class;
  object_class = (GtkObjectClass*) class;
}

/******************************************************************************
**
**  Object initialization routine for GtkDatabase objects
**
******************************************************************************/

static void
gtk_database_init (GtkDatabase * database)
{

}

/******************************************************************************
**
**  Class initialization routine for GtkDatabaseItem objects
**
******************************************************************************/

static void
gtk_database_item_class_init (GtkDatabaseItemClass *class)
{
  GtkObjectClass *object_class;
  object_class = (GtkObjectClass*) class;
}

/******************************************************************************
**
**  Object initialization routine for GtkDatabaseItem objects
**
******************************************************************************/

static void
gtk_database_item_init (GtkDatabaseItem * database_item)
{

}


