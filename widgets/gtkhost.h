/*
 * $Id$
 * GTKEXT - Extensions to The GIMP Toolkit
 * Copyright (C) 1998 Gregory McLean
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 * Host Widget
 */
#ifndef __GTKHOST_H__
#define __GTKHOST_H__

#include <gdk/gdk.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkcontainer.h>
#include <gtk/gtkbox.h>

#ifdef __cplusplus
extern "C" {
#endif

#define GTK_TYPE_HOST         (gtk_host_get_type ())
#define GTK_HOST(obj)         (GTK_CHECK_CAST (obj, GTK_TYPE_HOST, GtkHost))
#define GTK_HOST_CLASS(klass) (GTK_CHECK_CLASS_CAST (klass, GTK_TYPE_HOST, GtkHostClass))
#define GTK_IS_HOST(obj)      (GTK_CHECK_TYPE (obj, GTK_TYPE_HOST))
#define GTK_IS_HOST_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_HOST))

typedef struct _GtkHost       GtkHost;
typedef struct _GtkHostClass  GtkHostClass;

struct _GtkHost 
{
  GtkContainer container;

  GtkWidget    *child;

  GtkWidget    *vbox;
  GtkWidget    *label;
  GtkWidget    *button;
  GtkWidget    *pixmap;

  gboolean     snmp_enabled;         /* This host responds to snmp */
  gint         host_state;           /* Up down etc.. */
  gint         snmp_request;         /* Running request */
  gpointer     host_datum;           /* Pointer to app specific info */
};

struct _GtkHostClass
{
  GtkContainerClass    parent_class;

  void (* enter)       (GtkHost *host);
  void (* leave)       (GtkHost *host);
  void (* pressed)     (GtkHost *host);
  void (* released)    (GtkHost *host);
};

guint        gtk_host_get_type       (void);
GtkWidget*   gtk_host_new            (void);
GtkWidget*   gtk_host_new_with_label (const gchar *label);
void         gtk_host_enter          (GtkHost     *host);
void         gtk_host_leave          (GtkHost     *host);
void         gtk_host_pressed        (GtkHost     *host);
void         gtk_host_released       (GtkHost     *host);

void         gtk_host_set_pixmap     (GtkWidget   *widget, 
                                      GtkWidget   *pixmap);

void         gtk_host_set_label      (GtkWidget   *host, 
                                      const gchar *label);

#ifdef __cplusplus
}
#endif

#endif

/* EOF */
