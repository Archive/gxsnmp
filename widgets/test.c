#include <gnome.h>
#include "gtkledbar.h"

static int count = 0;
static GtkWidget *led_bar = NULL;
static GtkWidget *scanner = NULL;
static GtkWidget *percent_test = NULL;
static GtkWidget *dial = NULL;

int
time_cb (gpointer data)
{
  if (count >= 10)
   {
      led_bar_clear (led_bar);
      count = 0;
      return TRUE;
   }
  else
   {
      count++;
      led_bar_light_segments (led_bar, count);
   }
  return TRUE;
}

int
scan_cb (gpointer data)
{
   gfloat    r;

   r = (100.0 * rand()/(RAND_MAX+1.0));
   r /= 100.0;
   led_bar_light_percent (percent_test, r);
   led_bar_sequence_step (scanner);
   return TRUE;
}


int 
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *vbox;
  int       i;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  gtk_window_set_title (GTK_WINDOW (window), "Dial");
  
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_exit), NULL);
  
  gtk_container_border_width (GTK_CONTAINER (window), 10);

  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_widget_show(vbox);

  led_bar = led_bar_new (15);
  g_print ("Bar has %d segments\n", led_bar_get_num_segments (led_bar));
  gtk_box_pack_start (GTK_BOX (vbox), led_bar, TRUE, TRUE, 0);
  gtk_widget_show (led_bar);
  scanner = led_bar_new (15);
  gtk_box_pack_start (GTK_BOX (vbox), scanner, TRUE, TRUE, 0);
  gtk_widget_show (scanner);
  percent_test = led_bar_new (15);
  gtk_box_pack_start (GTK_BOX (vbox), percent_test, TRUE, TRUE, 0);
  gtk_widget_show (percent_test);
  i = gtk_timeout_add (100, time_cb, NULL);
  i = gtk_timeout_add (110, scan_cb, NULL);
  gtk_widget_show (window);

  gtk_main ();
  
  return 0;
}
