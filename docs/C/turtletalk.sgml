<chapter>
  <title>Turtle I/O Design</title>
<sect1 id="introduction">
  <title>Introduction</title>
  <para>
  With the separation of the network and host discovering daemon, called turtle
  with the rest of the processes there is a need for a communication protocol
  between each turtle ``slave'' and the central ``turtle master''.  This
  document describes this procotol and has been written to foster discussion.
  </para>
  <para>
  The author felt that the way the protocol looks is similar in style to BGP
  and so has based this protocol upon BGP-4, as described in RFC 1771 by the
  IETF.  It may be beneficial to read this RFC first as background reading
  to this documents ancestor.
  </para>
</sect1>
<sect1 id="general-overview">
  <title>General Overview</title>
  <para>
    The communication is done using TCP over a defined port.  The connection
    establishment is from the master to the slave.  The master must have a list
    of slaves, but the slave may, as an option, have a list of masters for
    purposes such as security.  With the current protocol, there is one active
    master per slave.
  </para>
  <para>
    The master establishes the connection and then both entites can exchange
    messages.  There may be more than one message per TCP datagram.  The first
    message is sent by the master, and is the OPEN messagem, which establishes
    certain parameters for the slave.
  </para>
</sect1>
<sect1 id="message-syntax">
  <title>Message syntax</title>
  <para>
    Every message sent has the same sort of header.  The header consists of:
  </para>
  <itemizedlist>
    <listitem><para>
      A 4-octet counter; the counter is initialised by the OPEN message
      can be set to anything.  After that each entity tracks the counter.
    </para></listitem>
    <listitem><para>
      A 2-octet length.  This is the total length of this message in octects
      including this message header.
    </para></listitem>
    <listitem><para>
      An 1-octet message type, the definition of the message types are 
      described below.
    </para></listitem>
  </itemizedlist>
  <para>
    The receiver checks each of these fields and sends a NOTIFICATION if there
    is a problem with the header.
  </para>
</sect1>
<sect1 id="messages">
  <title>Messages</title>
  <sect2 id="open-message">
  <title>OPEN Message</title>
  <para>
    The OPEN message can only be sent by the master to the slave.  It is the
    first message that should be sent by any entity.  If another message is
    received first, a state-machine NOTIFICATION is sent.
  </para>
  <para>
    The message type for an OPEN message is 0x01.  The formation of the OPEN
    message is:
  </para>
  <itemizedlist>
    <listitem><para>
      1-octet version number, current this is 1.
    </para></listitem>
    <listitem><para>
      2-octet keepalive time.
    </para></listitem>
    <listitem><para>
      1-octet optional parameter length.
    </para></listitem>
  </itemizedlist>
  <para>
    The keepalive time is the time in seconds that the master expects to 
    see at least one packet from the slave and also the interval that the
    slave should expect a message from the master. If the either entity has
    nothing at the moment to send, then it sends a KEEPALIVE message.  This 
    value cannot be less than 3. It is assumed that they  will send a message 
    at least once per one-third the keepalive time.  That is if the keepalive 
    time is set for 60 seconds, both entities will send one message at least
    every 20 seconds, however they will not consider a link to be lost until
    they have not received a message for 60 seconds.
  </para>
  <para>
    The optional parameter length gives the length of all the optional
    parameters for this OPEN message.  If there are no options, this field
    is set to 0.
  </para>
<sect3>
  <title>OPEN optional parameters</title>
  <para>
    Each OPEN optional parameter follows the following format:
  </para>
  <itemizedlist>
    <listitem><para>
      1-octet parameter type (see below for list)
    </para></listitem>
    <listitem><para>
      1-octect parameter length of parameter data, may be 0.
    </para></listitem>
    <listitem><para>
      A variable length field specific to the parameter type.
    </para></listitem>
  </itemizedlist>
  <para>
    Currently the only optional parameter for the OPEN is the authentication
    option, which is type 1.  The authentication option consists of the
    following three fields:
  </para>
  <itemizedlist>
    <listitem><para>
      1-octet Authentication type (described below)
    </para></listitem>
    <listitem><para>
      1-octect variable data length, may be 0.
    </para></listitem>
    <listitem><para>
      Variable length authentication data field.
    </para></listitem>
  </itemizedlist>
  <para>
    Currently the only authentication type is type 1, which is clear-text
    password.  The data-length field contains the length of the string and
    the variable length field contains the cleartext password itself.
  </para>
  <para>
    A NOTIFICATION will be sent with a type "OPEN error" subtype 
    "Authentication Failed" if the cleartext password is incorrect.
  </para>
</sect3>
</sect2>
<sect2 id="manage-message">
  <title>MANAGE Message</title>
  <para>
    The MANAGE message is sent my the master to the slave to let the slave
    know what networks it should be managaing.  In addition, extra details
    about the network can be added in these messages.  There can be multiple
    networks per MANAGE message.  The MANAGE message is type 2. The format
    of the MANAGE message is:
  </para>
  <itemizedlist>
    <listitem><para>
      4-octet Address Family, using the standard Unix AF_* types.
    </para></listitem>
    <listitem><para>
      1-octet showing the length of the network address (the next field).
      In an IPv4 network, this length will be 8.
    </para></listitem>
    <listitem><para>
      A variable length field that stores the description of the network. 
      For a given address family, described in the first field, this
      field will be a known length.
      </para><para>
      For an address family of IPV4, this variable field will be 8-octets
      long.  The first 4 octets hold the IPv4 network address and the second
      hold the network mask of the network.
    </para></listitem>
    <listitem><para>
      1-octet field should the length of network attributes, in octets.  If
      there are no attributes this field is 0.
    </para></listitem>
  </itemizedlist>
  <para>
    The network attributes can be different for each network in the same
    MANAGE message.  The format of the network attributes is:
  </para>
  <itemizedlist>
    <listitem><para>
      1-octet attribute type (see below).
    </para></listitem>
    <listitem><para>
      1-octet length of variable data, for attributes that have no variable
      data, this field would be 0.
    </para></listitem>
    <listitem><para>
      A variable length attribute data field, the format of this field is
      dependent on the attribute used.
    </para></listitem>
  </itemizedlist>
  <para>
    Attribute type 1 is SNMP community.  The variable length data field holds
    the SNMP community name in cleartext.  There can be multiple instances
    of this attribute for one network if they are contained in the same
    network list.  However if a network is mentioned in the same or
    even different MANAGE messages the SNMP communities are overwritten by
    the last received network description.  The length field is the string
    length of the community.
  </para>
</sect2>
<sect2 id="keepalive-message">
  <title>KEEPALIVE Message</title>
  <para>
    KEEPALIVE messages are used by both the master and slave when there is
    nothing to send but a message must be sent to reset the keepalive 
    timeout on the remote end.  KEEPALIVE messages are type 3 and have no
    other fields except the generic message headers.
  </para>
</sect2>
<sect2 id="notification-messsage">
  <title>NOTIFICATION Message</title>
  <para>
    NOTIFICATION messages are the protocol's way of alerting the administrator
    or the peer system there is a problem.  They are message type 4 and can
    be sent by either entity though some NOTIFICATION message types can only
    be sent by one entity. The format of a NOTIFICATION message is:
  </para>
  <itemizedlist>
    <listitem><para>
      1-octet Error Code
    </para></listitem>
    <listitem><para>
      1-octet Error Sub-Code
    </para></listitem>
    <listitem><para>
      1-octet giving the length of the variable data field
    </para></listitem>
    <listitem><para>
      A variable length data field, the format is dependent of the Error Code
      and Error Sub-Code.
    </para></listitem>
  </itemizedlist>
  <sect3>
    <title>Message Header NOTIFICATION</title>
    <para>
      These NOTIFICATION messages are sent when there is a problem with the
      message header.  They are NOTIFICATION type 1. The following are
      the various Error Sub-Codes that can be used:
    </para>
    <itemizedlist>
      <listitem><para>
        Sub-Code 1: Connection not synchronized.  This means that the expected
	counter in the message header was not received and there is a
	missing message or one or both of the entities has gotten very mixed
	up.  The solution is to reset the session.  The data field has the
	erronous message counter.
     </para></listitem>
     <listitem><para>
       Sub-Code 2: Bad Message Length.  The length of the message is either
       out of the range expected for all messages or the range expected for
       a particular message type (if known).  The data field has the erronous
       length field.
     </para></listitem>
     <listitem><para>
       Sub-Code 3: Bad Message Type.  The type of message is not recognised
       by the receiver.  While this points to a problem, it is not a fatal
       one.  The data field contains the unkown message type.
     </para></listitem>
   </itemizedlist>
 </sect3>
 <sect3>
   <title>OPEN Error NOTIFICATION message</title>
   <para>
     Type 2 NOTIFICATION messages are ones that have an error in the OPEN
     message.  As the OPEN message is only sent by the master to the slave,
     this NOTIFICATION can only be sent by the slave. The following are the
     sub-codes that can be used.
   </para>
   <itemizedlist>
     <listitem><para>
       Sub-Code: 1 Bad Version.  The slave does not know about the given
       version number in the OPEN message.  The data field has the most
       recent (largest) protocol version that the slave can handle.  If the
       master has backward compatibility to that version, it can, at
       the administrator's option, shift down to an older version, or it
       can abort that connection to the slave.
     </para></listitem>
     <listitem><para>
       Sub-Code: 2 Bad KEEPALIVE time.  The value for the KEEPALIVE time has
       been rejected by the slave.  The slave may have limits that have
       been set by its administrator or the time given is below 3 seconds
       and therefore must be rejected.  There is no data in the data field.
     </para></listitem>
     <listitem><para>
       Sub-Code: 3 Unsupported optional parameter.  One of the optional
       parameters is not supported by the slave.  The data field contains
       the parameter code that was rejected.  The master may either ignore
       this NOTIFICATION or terminate the session.
     </para></listitem>
     <listitem><para>
       Sub-Code: 4 Authentication Failed.  The slave rejected whatever 
       method of authentication was used for the OPEN message.  The slave
       will not accept any other messages except for another OPEN
       mesage or it may teminate the session.  The failure should be
       logged by both entities.
     </para></listitem>
   </itemizedlist>
  </sect3>
</sect2>
<sect2 id="host-message">
  <title>HOST Message</title>
  <para>
    Blah
  </para>
</sect2>
<sect2 id="network-message">
  <title>NETWORK Message</title>
  <para>
    Blah
  </para>
</sect2>
<sect2 id="poll-message">
  <title>POLL Message</title>
  <para>
    Blah
  </para>
</sect2>
<sect2 id="reset-message">
  <title>RESET Message</title>
  <para>
    Blah
  </para>
</sect2>
</sect1>
</chapter>
